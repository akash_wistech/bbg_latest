<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->id();
            $table->string('reference_no',100)->nullable();
            $table->integer('membership_type_id')->default(0)->nullable()->index();
            $table->integer('company_id')->default(0)->nullable()->index();
            $table->tinyInteger('is_active')->default(0)->nullable()->index();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('no_of_months')->default(0)->nullable();
            $table->integer('no_of_days')->default(0)->nullable();
            $table->tinyInteger('activated')->default(0)->nullable()->index();
            $table->timestamp('expiry_alert_sent')->nullable()->index();
            $table->integer('renewed_subscription')->default(0)->nullable();
            $table->integer('no_of_nominee')->default(0)->nullable();
            $table->integer('no_of_additional')->default(0)->nullable();
            $table->integer('no_of_named_associate')->default(0)->nullable();
            $table->integer('no_of_alternate')->default(0)->nullable();
            $table->integer('no_of_news_per_month')->default(0)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });

        Schema::table('users', function($table) {
            $table->tinyInteger('active_subscription_id')->after('user_type')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription');
    }
}

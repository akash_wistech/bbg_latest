<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_type', function (Blueprint $table) {
            $table->id();
            $table->integer('service_id')->default(0)->index();
            $table->tinyInteger('sort_order')->default(0)->nullable();
            $table->integer('no_of_nominee')->default(0)->nullable();
            $table->integer('no_of_additional')->default(0)->nullable();
            $table->integer('no_of_named_associate')->default(0)->nullable();
            $table->integer('no_of_alternate')->default(0)->nullable();
            $table->integer('no_of_news_per_month')->default(0)->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();


            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_type');
    }
}

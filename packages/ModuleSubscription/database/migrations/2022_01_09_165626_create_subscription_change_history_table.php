<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionChangeHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_change_history', function (Blueprint $table) {
            $table->id();
            $table->integer('subscription_id')->default(0)->nullable()->index();
            $table->timestamp('old_start_date')->nullable();
            $table->timestamp('old_end_date')->nullable();
            $table->timestamp('new_start_date')->nullable();
            $table->timestamp('new_end_date')->nullable();
            $table->timestamp('old_no_of_nominee')->nullable();
            $table->timestamp('new_no_of_nominee')->nullable();
            $table->timestamp('old_no_of_additional')->nullable();
            $table->timestamp('new_no_of_additional')->nullable();
            $table->timestamp('old_no_of_named_associate')->nullable();
            $table->timestamp('new_no_of_named_associate')->nullable();
            $table->timestamp('old_no_of_alternate')->nullable();
            $table->timestamp('new_no_of_alternate')->nullable();
            $table->timestamp('old_no_of_news_per_month')->nullable();
            $table->timestamp('new_no_of_news_per_month')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_change_history');
    }
}

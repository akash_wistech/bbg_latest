<?php
return [
  'heading' => 'Subscriptions',
  'heading' => 'Subscriptions',
  'subscription_detail' => 'Subscription - :reference_no',
  'create' => 'Create New Subscription',
  'renew' => 'Renew Subscription',
  'assign_users' => 'Assign users for :company :dates',
  'create_heading_for' => 'Create Subscription for :company',
  'renew_heading_for' => 'Renew Subscription for :company',
  'saved' => 'Renew Subscription for :company',
  'membership_type' => 'Membership Type',
  'generated_invoice_for' => 'Generated Invoice of subscription for :company :dates',
  'user_updated' => 'Updated user successfully',
  'quota_full' => ':type quota full for the subscription',
  'user_type' => 'User Type',
  'change_user_type' => 'Change User Type for :member',

  'no_of_nominee' => 'No of Nominees',
  'no_of_additional' => 'No of Additionals',
  'no_of_named_associate' => 'No of Named Associates',
  'no_of_alternate' => 'No of Alternates',
  'no_of_news_per_month' => 'No of News',
];

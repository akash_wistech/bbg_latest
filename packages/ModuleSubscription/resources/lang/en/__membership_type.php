<?php
return [
	'heading' => 'Membership Types',
	'saved' => 'Membership type saved successfully',
	'notsaved' => 'Membership type not saved',
	'deleted' => 'Deleted Successfully',

	'sort_order' => 'Sort Order',
	'no_of_nominee' => 'No of Nominee',
	'no_of_additional' => 'No of Additional',
	'no_of_named_associate' => 'No of Named Associate',
	'no_of_alternate' => 'No of Alternate',
	'description' => 'Description',



	'first_name' => 'First Name',
	'last_name' => 'Last Name',
	'phone_number' => 'Mobile',
	'email' => 'Email',
	'country_code' => 'Your Office Based Country',
	'city' => 'Your Office Based City',
	'other_country' => 'Please specify Country Name',
	'other_city' => 'Please specify City Name',
	'address' => 'Physical Mailing Address ',
	'secondry_email' => 'Secondary Email',
	'designation' => 'Corporate Title',
	'nationality' => 'Nationality',
	'company_type' => 'Category',
	'howDidYouHear' => 'How did you hear about us?',
	'referred_by' => 'Referred By',
	'tellUsAboutYourself' => 'Tell us about yourself in a few words',
	'purposeOfJoining' => 'Reasons for joining the BBG',

	'compnay_name' => 'Company Name',
	'company_type' => 'Sector',
	'compnay_url' => 'Company Url',
	'vat_number' => 'VAT TRN',
	'company_phone' => 'Telephone',
	'postal_zip' => 'P.O. Box/Zip',
	'fax' => 'Fax ',
	'emirates' => 'UAE/UK Head Office',
	'other_emirates' => 'Please Specify',
	'comapny_address' => 'Address',
	'about_company' => 'About Company (Use Keywords)',

	'user_name' => 'User Name',
	'password' => 'Password',
	'repassword' => 'Retype password',
	'verifyCode' => 'Verify Code',

	'name' => 'Name'

];

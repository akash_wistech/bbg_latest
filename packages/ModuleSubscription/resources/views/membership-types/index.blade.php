@php
$btnsList = [];
if(checkActionAllowed('create')){
$btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/membership-type/create', 'method'=>'post'];
}

$statuArr = getStatusIconArr();
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('nam-subscription::membership_type.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('nam-subscription::membership_type.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <div class="overlay-wrapper">
      @if($results->isNotEmpty())
      <table class="table">
        <thead>
          <tr>
            <th>{{__('common.id')}}</th>
            <th>{{__('common.title')}}</th>
            <th>{{__('common.status')}}</th>
            <th width="100">{{__('common.action')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($results as $result)
          <tr id="result_id_{{ $result->id }}">
            <td>{{ $result->id }}</td>
            <td>{{ $result->service->title }}</td>

            <td>{!! isset($statuArr[$result->status]) ? $statuArr[$result->status] : '' !!}</td>

            <td>
              @if(checkActionAllowed('update'))
              <a href="{{ url('/membership-type/update',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon edit' title='{{__('common.update')}}' data-id="{{$result->id}}">
                <i class="text-dark-50 flaticon-edit"></i>
              </a>
              @endif
              @if(checkActionAllowed('delete'))
              <a href="{{ url('/membership-type/delete',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon act-confirmation' title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
              </a>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <div class="text-center">{{ __('common.noresultfound') }}</div>
      @endif
    </div>
  </div>
</div>
@endsection

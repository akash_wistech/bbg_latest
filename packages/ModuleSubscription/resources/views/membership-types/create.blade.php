@extends('layouts.app')
@section('title', __('nam-subscription::membership_type.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'membership-type'=>__('nam-subscription::membership_type.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('nam-subscription::membership-types._form')
@endsection

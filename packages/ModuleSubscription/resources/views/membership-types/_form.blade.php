{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom multi-tabs">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('service_id',__('common.title')) }}<span class="text-danger">*</span>
          {{ Form::select('service_id', getServicesListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('sort_order', __('nam-subscription::membership_type.sort_order'))}}<span class="text-danger">*</span>
          {{ Form::number('sort_order', null, ['class'=> 'form-control'])}}
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('no_of_nominee', __('nam-subscription::membership_type.no_of_nominee'))}}
          {{ Form::text('no_of_nominee', null, ['class'=> 'form-control nominee', 'placeholder'=> 'No of Nominee'])}}
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('no_of_additional', __('nam-subscription::membership_type.no_of_additional'))}}
          {{ Form::text('no_of_additional', null, ['class'=> 'form-control additional', 'placeholder'=> 'No of Additional'])}}
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('no_of_named_associate', __('nam-subscription::membership_type.no_of_named_associate'))}}
          {{ Form::text('no_of_named_associate', null, ['class'=> 'form-control named-associate', 'placeholder'=> 'No of Named Associate'])}}
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('no_of_alternate', __('nam-subscription::membership_type.no_of_alternate'))}}
          {{ Form::text('no_of_alternate', null, ['class'=> 'form-control alternate', 'placeholder'=> 'No of Alternate'])}}
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('no_of_news_per_month', __('nam-subscription::membership_type.no_of_news_per_month'))}}
          <div class="input-group igb mb-3">
            {{ Form::text('no_of_news_per_month', null, ['class'=> 'form-control news_limit', 'placeholder'=> 'No of News'])}}
            <div class="input-group-append">
              <span class="input-group-text ctv">{{__('common.per_month')}}</span>
            </div>
          </div>
        </div>
      </div>

      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('status', __('common.status'))}}<span class="text-danger">*</span>
          {{ Form::select('status', getMembershipTypeStatusArr(), null, ['class'=> 'form-control'])}}
        </div>
      </div>

    </div>

    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('description', __('nam-subscription::membership_type.description'))}}<span class="text-danger">*</span>
          {{
            Form::textarea('description', null, [
            'class'   => 'form-control editor',
            ])
          }}
        </div>
      </div>
    </div>


  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/membership-type') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}


@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
  tinymce.init({
    selector: '.editor',
    menubar: false,
    toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
    plugins : 'advlist autolink link image lists charmap code table',
    image_advtab: true ,
  });

  $('.nominee, .additional, .named-associate, .alternate').keypress(function (e) {
    var charCode = (e.which) ? e.which : event.keyCode
    if (String.fromCharCode(charCode).match(/[^0-9]/g))
      return false;
  });

</script>

@endpush

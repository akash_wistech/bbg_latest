@extends('frontend.app')
@section('content')

<?php
use App\Models\MemberContacts;
$no_image = asset('assets/images/dummy-image-1.jpg');
if (Auth::check()) {?>
    <x-contact-banner/>
    <?php
// dd("here");
}
?>
{!! Form::model($model, ['files' => true]) !!}
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                    <!-- error div here -->
                    <div class="col-md-12 ">
                        <div class="Heading text-left">
                            <h3>Personal Information</h3><br/>
                        </div>


                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <label class="docs_upload_label control-label" for="signupform-image">Profile
                                Picture</label>
                                <div class="form-group">
                                    <a href="javascript:;"
                                    id="upload-document4"
                                    onclick="uploadAttachment(4)"
                                    data-toggle="tooltip"
                                    class="img-thumbnail"
                                    title="Upload Document">
                                    <i></i>
                                    <img src="<?= ($contactModel->image<>null) ? $contactModel->image : $no_image; ?>"
                                    width="100" alt="" title="" data-placeholder="no_image.png"/>
                                    <br/>
                                </a>
                                <p class="image_upload_label">click Image to upload</p>
                                {{ Form::hidden('image', '', array('id' => 'input-attachment4', 'maxlength' => true)) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
                            {{ Form::label('name', __('nam-subscription::membership_type.name'))}}<span class="text-danger">*</span>
                            {{ Form::text('name', $contactModel->name, ['class'=> 'form-control'])}}
                            {{ Form::label('email', __('nam-subscription::membership_type.email'))}}<span class="text-danger">*</span>
                            {{ Form::text('email', $contactModel->email, ['class'=> 'form-control'])}}
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
                            {{ Form::label('phone', __('nam-subscription::membership_type.phone_number'))}}<span class="text-danger">*</span>
                            {{ Form::text('phone', $contactModel->phone, ['class'=> 'form-control'])}}
                            {{ Form::label('emails[1]', __('nam-subscription::membership_type.secondry_email'))}}<span class="text-danger">*</span>
                            {{ Form::text('emails[1]', $contactModel->emails, ['class'=> 'form-control'])}}
                        </div>


                        @if(function_exists('saveCustomField'))
                        <x-customfields-frontend-nonta-fields :model="$contactModel"/>
                        @endif

                        <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                            {{ Form::label('designation_id', __('nam-subscription::membership_type.designation'))}}
                            {{ Form::select('designation_id', [''=>__('Select')]+JobTitlesListArr(), $contactModel->designation_id, ['class'=> 'form-control'])}}
                        </div>

                        <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                            {{ Form::label('referred_by', __('nam-subscription::membership_type.referred_by'))}}
                            {{ Form::text('referred_by', $contactModel->referred_by, ['class'=> 'form-control'])}}
                        </div>

                        @if(function_exists('saveCustomField'))
                        <x-customfields-frontend-onlyta-fields :model="$contactModel"/>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                    <button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
{!! Form::close() !!}


@endsection

@push('css')
<style>
    .mysubmitbtn_parent_btn{
        margin-bottom: 3%;
    }
    .mysubmitbtn{
        font-size: x-large;
    }
    .textarea.form-control{
        font-size: 14px;
        height: 112px;
    }

</style>
@endpush
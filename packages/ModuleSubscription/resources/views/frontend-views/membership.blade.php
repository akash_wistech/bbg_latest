<?php
use NaeemAwan\ModuleSubscription\Models\MembershipType;
?>


@extends('frontend.app')
@section('content')
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
               <!--  cms wali working here abhi nhi pata uska -->
            </div>


            <?php
            $joining_fee_id= getSetting('joining_fee_id');
            $joining_fee = DB::table('predefined_list_price')->where('list_id', '=', $joining_fee_id)->first();

            foreach ($membershipTypes as $key => $type) {
                if ($type!=null) {
                    ?>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                        <div class="type-bg">
                            <div class="TypeHeading">
                                <h2><?= $type->service->title; ?></h2>
                                <!-- <h3>(One-time Joining Fee AED <?/*= ($type <> null) ? $type->joining_fee : ""; */?>)</h3>-->
                            </div>
                            <div class="Type applyNowBoxes">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Member Type</th>
                                            <th>Membership Fee</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        if ($joining_fee) {
                                            ?>
                                            <tr>
                                                <td>One-time Joining Fee</td>
                                                <td><?= ($joining_fee->rate > 0) ? "AED  " . $joining_fee->rate . " + VAT"  : "0"; ?></td>
                                            </tr>
                                            <?php
                                        } ?>




                                        <?php
                                        if ($type->no_of_nominee) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    if($type->no_of_nominee > 0 ){ ?>

                                                        <?= $type->no_of_nominee; ?> named representatives

                                                        <?php
                                                    }else{?>
                                                        Nominee
                                                        <?php
                                                    }
                                                    ?>

                                                </td>
                                                <td><?= ($type->rate->rate > 0) ? "AED  " . $type->rate->rate . " + VAT" : "Free"; ?></td>
                                            </tr>
                                            <?php
                                        } ?>

                                        <?php
                                        if ($type->no_of_alternate) {
                                            ?>
                                            <tr>
                                                <td>Alternate</td>
                                                <td><?= ($type->rate->rate > 0) ? "AED  " . $type->rate->rate  . " + VAT" : "Free"; ?></td>
                                            </tr>
                                            <?php
                                        } ?>

                                        <?php
                                        if ($type->no_of_additional) {
                                            ?>
                                            <tr>
                                                <td>Additional Members</td>
                                                <td><?= ($type->rate->rate > 0) ? "AED  " . $type->rate->rate  . " + VAT" : "Free"; ?></td>
                                            </tr>
                                            <?php
                                        } ?>

                                        <?php
                                        if ($type->no_of_named_associate) {
                                            ?>
                                            <tr>
                                                <td>Named Associates</td>
                                                <td><?= ($type->rate->rate > 0) ? "AED  " . $type->rate->rate  . " + VAT" : "Free"; ?></td>
                                            </tr>
                                            <?php
                                        } ?>

                                        <tr>
                                            <td colspan="2"><span class="color-docs-heading">Document Required</span></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <ol class="list_numbering">
                                                    <?= $type->description; ?>
                                                </ol>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="fullbtn"><a href="<?= url('signup/'.$type->id) ?>">Apply Now</a></div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>


        </div>
    </div>
</section>
@endsection

@push('css')
<style>
    .list_numbering{
        margin: 5px 10px;
    }
    .color-docs-heading{
        color: #1d355f !important;
        font-weight: bold;
    }
</style>
@endpush

<?php
$placeHolderTellUsAbout = "E.g. educated in the UAE and studied in the Netherlands I have lived, worked and studied in a cosmopolitan environment. Through this experience I was able to achieve a wide-ranging background. Through my travels I was able to gain knowledge in the following areas public speaking, event management, creative writing, marketing and production.";
$model = new App\Models\Contact;
$modelComp = new App\Models\Company;
$no_image = asset('assets/images/dummy-image-1.jpg');
?>

@extends('frontend.app')
@section('content')
{!! Form::open(array('url' => 'register-now', 'method' => 'post', 'id' => '','enableClientScript' => false)) !!}
{{ Form::hidden('id', $id, ['class'=> 'form-control'])}}
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                    <!-- error div here -->
                    <div class="col-md-12 ">
                        <div class="Heading text-left">
                            <h3>Personal Information</h3><br/>
                        </div>


                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('name', __('nam-subscription::membership_type.name'))}}<span class="text-danger">*</span>
                                {{ Form::text('name', null, ['class'=> 'form-control'])}}
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('phone', __('nam-subscription::membership_type.phone_number'))}}<span class="text-danger">*</span>
                                {{ Form::text('phone', null, ['class'=> 'form-control'])}}
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('email', __('nam-subscription::membership_type.email'))}}<span class="text-danger">*</span>
                                {{ Form::text('email', null, ['class'=> 'form-control'])}}
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('emails[1]', __('nam-subscription::membership_type.secondry_email'))}}<span class="text-danger">*</span>
                                {{ Form::text('emails[1]', null, ['class'=> 'form-control'])}}
                            </div>

                            @if(function_exists('saveCustomField'))
                            <x-customfields-frontend-nonta-fields :model="$model"/>
                            @endif

                            <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('designation_id', __('nam-subscription::membership_type.designation'))}}
                                {{ Form::select('designation_id', [''=>__('Select')]+JobTitlesListArr(), null, ['class'=> 'form-control'])}}
                            </div>

                            <div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('referred_by', __('nam-subscription::membership_type.referred_by'))}}
                                {{ Form::text('referred_by', null, ['class'=> 'form-control'])}}
                            </div>

                            @if(function_exists('saveCustomField'))
                            <x-customfields-frontend-onlyta-fields :model="$model"/>
                            @endif
                        </div>

                        <div class="Heading text-left">
                            <h3>Company Info</h3><br/>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('compnay_name', __('nam-subscription::membership_type.compnay_name'))}}<span class="text-danger">*</span>
                                {{ Form::text('compnay_name', null, ['class'=> 'form-control'])}}
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('compnay_email', __('common.email'))}}<span class="text-danger">*</span>
                                {{ Form::text('compnay_email', null, ['class'=> 'form-control'])}}
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
                                {{ Form::label('vat_number', __('nam-subscription::membership_type.vat_number'))}}<span class="text-danger">*</span>
                                {{ Form::text('vat_number', null, ['class'=> 'form-control'])}}
                            </div>

                            @if(function_exists('saveCustomField'))
                            <x-customfields-frontend-nonta-fields :model="$modelComp"/>
                            @endif

                            @if(function_exists('saveCustomField'))
                            <x-customfields-frontend-onlyta-fields :model="$modelComp"/>
                            @endif
                        </div>




                        <div class="Heading text-left">
                            <h3>Upload Required Documents</h3><br>
                        </div>
                        <div class="row ">

                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <label class="docs_upload_label control-label" for="signupform-trade_licence">
                                Copy of UAE Trade Licence.</label>
                                <div class="form-group">
                                    <a href="javascript:;"
                                    id="upload-document1"
                                    onclick="uploadAttachment(1)"
                                    data-toggle="tooltip"
                                    class="img-thumbnail"
                                    title="Upload Document">
                                    <i></i>
                                    <img src="<?= $no_image ?>"
                                    width="100" alt="" title="" data-placeholder="no_image.png"/>
                                    <br/>
                                </a>
                                <p class="image_upload_label">click Image to upload</p>
                                {{ Form::hidden('trade_licence', '', array('id' => 'input-attachment1', 'maxlength' => true)) }}
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <label class="docs_upload_label control-label" for="signupform-vat_document">
                            {{__('Vat Document')}}.</label>
                            <div class="form-group">
                                <a href="javascript:;"
                                id="upload-document6"
                                onclick="uploadAttachment(6)"
                                data-toggle="tooltip"
                                class="img-thumbnail"
                                title="Upload Document">
                                <i></i>
                                <img src="<?= $no_image ?>"
                                width="100" alt="" title="" data-placeholder="no_image.png"/>
                                <br/>
                            </a>
                            <p class="image_upload_label">click Image to upload</p>
                            {{ Form::hidden('vat_document', '', array('id' => 'input-attachment6', 'maxlength' => true)) }}
                        </div>
                    </div>

                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                        <label class="docs_upload_label control-label" for="signupform-passport_copy">
                        Copy of passport</label>
                        <div class="form-group">
                            <a href="javascript:;"
                            id="upload-document2"
                            onclick="uploadAttachment(2)"
                            data-toggle="tooltip"
                            class="img-thumbnail"
                            title="Upload Document">
                            <i></i>
                            <img src="<?= $no_image; ?>"
                            width="100" alt="" title="" data-placeholder="no_image.png"/>
                            <br/>
                        </a>
                        <p class="image_upload_label">click Image to upload</p>
                        {{ Form::hidden('passport_copy', '', array('id' => 'input-attachment2', 'maxlength' => true)) }}
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                    <label class="docs_upload_label control-label" for="signupform-residence_visa">Copy of
                    residence visa</label>
                    <div class="form-group">
                        <a href="javascript:;"
                        id="upload-document3"
                        onclick="uploadAttachment(3)"
                        data-toggle="tooltip"
                        class="img-thumbnail"
                        title="Upload Document">
                        <i></i>
                        <img src="<?= $no_image ?>"
                        width="100" alt="" title="" data-placeholder="no_image.png"/>
                        <br/>
                    </a>
                    <p class="image_upload_label">click Image to upload</p>
                    {{ Form::hidden('residence_visa', '', array('id' => 'input-attachment3', 'maxlength' => true)) }}
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <label class="docs_upload_label control-label" for="signupform-logo">Company
                Logo.</label>
                <div class="form-group">
                    <a href="javascript:;"
                    id="upload-document5"
                    onclick="uploadAttachment(5)"
                    data-toggle="tooltip"
                    class="img-thumbnail"
                    title="Upload Document">
                    <i></i>
                    <img src="<?= $no_image; ?>"
                    width="100" alt="" title="" data-placeholder="no_image.png"/>
                    <br/>
                </a>
                <p class="image_upload_label">click Image to upload</p>
                {{ Form::hidden('logo', '', array('id' => 'input-attachment5', 'maxlength' => true)) }}
            </div>
        </div>

        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
            <label class="docs_upload_label control-label" for="signupform-image">Profile
            Picture (upload as JPEG)</label>
            <div class="form-group">
                <a href="javascript:;"
                id="upload-document4"
                onclick="uploadAttachment(4)"
                data-toggle="tooltip"
                class="img-thumbnail"
                title="Upload Document">
                <i></i>
                <img src="<?= $no_image; ?>"
                width="100" alt="" title="" data-placeholder="no_image.png"/>
                <br/>
            </a>
            <p class="image_upload_label">click Image to upload</p>
            {{ Form::hidden('image', '', array('id' => 'input-attachment4', 'maxlength' => true)) }}
        </div>
    </div>




</div>
<!-- document area here -->

<div class="Heading text-left">
    <h3>BBG Profile Info</h3><br/>
</div>
<div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
        {{ Form::label('password', __('nam-subscription::membership_type.password'))}}<span class="text-danger">*</span>
        {{ Form::password('password', null, ['class'=> 'form-control'])}}
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
        {{ Form::label('repassword', __('nam-subscription::membership_type.repassword'))}}
        {{ Form::password('repassword', null, ['class'=> 'form-control'])}}
    </div>

    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-12">
        {!! htmlFormSnippet() !!}
    </div>


    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-2">
        <label class="checkbox checkbox-lg">
            {{ Form::checkbox('newsletter', '1', false,  ['class' => 'larger']) }}
            <span class="label">I want to subscribe to the BBG Newsletter.</span>
        </label>
    </div>

    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-4">
        <label class="checkbox checkbox-lg">
            {{ Form::checkbox('terms', '1', false,  ['class' => 'larger']) }}
            <span class="label">I agree to the BBG Terms and Conditions.</span>
        </label>
    </div>

    <style>
        input.larger {
            width: 25px;
            height: 25px;
        }

    </style>

    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
        <span>
            <a target="_blank" href="{{url('/slug-pages?slug=terms-and-conditions')}}"
            style="font-size: 10px;font-weight: 800;font-style: italic;">Read Terms & Condition here</a>
        </span>
    </div>

</div>
</div>
<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
    <button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o" name="LOGIN TO YOUR ACCOUNT">Sign Up</button>
</div>
</div>
</div>
</div>

</section>
{!! Form::close() !!}
@endsection

@push('css')


<style>
    form div.required label.control-label:after {
        content: " * ";
        color: #a94442;
    }

    textarea#signupform-tellusaboutyourself {
        min-height: 100px;
    }

    textarea#signupform-purposeofjoining {
        min-height: 100px;
    }

    textarea#signupform-purposeofjoining::-webkit-input-placeholder,
    textarea#signupform-tellusaboutyourself::-webkit-input-placeholder {
        font-size: 14px;
    }
</style>
@endpush


@push('js')
<script>
    var uploadAttachment = function (attachmentId) {
        $('#form-upload').remove();
        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="hidden" name="_token" value="{{ csrf_token() }}" /><input type="file" name="file" value="" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: '{{url('member/file-manager/uploadFrontend')}}?parent_id=1',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $('#upload-document' + attachmentId + ' img').hide();
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                        $('#upload-document' + attachmentId).prop('disabled', true);
                    },
                    complete: function () {
                        $('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
                        $('#upload-document' + attachmentId).prop('disabled', false);
                        $('#upload-document' + attachmentId + ' img').show();
                    },
                    success: function (json) {
                        if (json['error']) {
                            alert(json['error']);
                        }

                        if (json['success']) {
                            console.log(json);
                            $('#upload-document' + attachmentId + ' img').prop('src', json.href);
                            $('#input-attachment' + attachmentId).val(json.href);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    }
</script>
@endpush

@extends('layouts.blank')
{!! Form::model($model, ['class' => 'simple-ajax-submit', 'data-pjxcntr' => $pjxcntr, 'data-blockcntr' => $pjxcntr]) !!}
<div class="row">
  <div class="col-12 col-sm-12">
    <div class="form-group">
      {{ Form::label('user_type',__('nam-subscription::subscription.user_type'),['class'=>'required']) }}
      {{ Form::select('user_type', getSubscriptionUserTypeListArr(), null, ['class'=>'form-control']) }}
    </div>
  </div>
</div>
<div class="card-footer text-right">
  <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
  <a href="javascript:;" data-dismiss="modal" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
{!! Form::close() !!}

@extends('layouts.app')
@section('title', __('Subscription'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'subscription'=> __('Subscription'),
  __('common.update')
]])
@endsection
@section('content')

<?php
      $company=  getCompanyInfo($model->company_id);
?>
{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif


   <div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
        {{ Form::label('reference_no',__('ID')) }}
        {{ Form::text('reference_no',$model->reference_no, ['class'=>'form-control form-control-solid bg-mute','readonly'=>'readonly']) }}
       </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('company_id',__('Company')) }}
          {{ Form::text('company_id', $company->title, ['class'=>'form-control form-control-solid','readonly'=>'readonly']) }}
          {{ Form::hidden('company_id', $company->id, ['class'=>'form-control form-control-solid','readonly'=>'readonly']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
      <div class="form-group">
        {{ Form::label('membership_type_id',__('nam-subscription::subscription.membership_type')) }}
        {{ Form::select('membership_type_id', getMembershipTypesListArr(), $model->membership_type_id, ['class'=>'form-control']) }}
      </div>
      </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('start_date',__('Start Date')) }}
          <div class="input-group">
          {{ Form::text('start_date', date("Y-m-d", strtotime($model->start_date)) , ['class'=>'form-control dtpicker']) }}
          {{ Form::hidden('start_old_date',date("Y-m-d", strtotime($model->start_date)),['class'=>'form-control start_old_date ']) }}

          <div class="input-group-append">
            <span class="input-group-text">
              <i class="la la-calendar"></i>
             </span>
          </div>
        </div>
        </div>
      </div>

        <div class="form-group col-xs-12 col-sm-4">
          <div class="form-group">
        {{ Form::label('end_date',__('End Date')) }}
        <div class="input-group">
         {{ Form::text('end_date', date("Y-m-d", strtotime($model->end_date)) , ['class'=>'form-control dtpicker','placeholder'=>'expiry date',]) }}
         <div class="input-group-append">
           <span class="input-group-text">
             <i class="la la-calendar"></i>
            </span>
         </div>
         {{ Form::hidden('expiry_old_date',date("Y-m-d", strtotime($model->end_date)),['class'=>'form-control expiry_old_date ']) }}
        </div>
        </div>
       </div>
       <div class="col-12 col-sm-4">
         <div class="form-group">
           {{ Form::label('no_of_nominee',__('nam-subscription::subscription.no_of_nominee'),['class'=>'required']) }}
           {!! Form::text('no_of_nominee', $model->no_of_nominee, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
         </div>
       </div>
       <div class="col-12 col-sm-4">
         <div class="form-group">
           {{ Form::label('no_of_additional',__('nam-subscription::subscription.no_of_additional'),['class'=>'required']) }}
           {!! Form::text('no_of_additional', $model->no_of_additional, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
         </div>
       </div>
       <div class="col-12 col-sm-4">
         <div class="form-group">
           {{ Form::label('no_of_named_associate',__('nam-subscription::subscription.no_of_named_associate'),['class'=>'required']) }}
           {!! Form::text('no_of_named_associate', $model->no_of_named_associate, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
         </div>
       </div>
       <div class="col-12 col-sm-4">
         <div class="form-group">
           {{ Form::label('no_of_alternate',__('nam-subscription::subscription.no_of_alternate'),['class'=>'required']) }}
           {!! Form::text('no_of_alternate', $model->no_of_alternate, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
         </div>
       </div>
       <div class="col-12 col-sm-4">
         <div class="form-group">
           {{ Form::label('no_of_news_per_month',__('nam-subscription::subscription.no_of_news_per_month'),['class'=>'required']) }}
           <div class="input-group date">
             {!! Form::text('no_of_news_per_month', $model->no_of_news_per_month, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
             <div class="input-group-append">
               <div class="input-group-text">{{__('common.per_month')}}</div>
             </div>
           </div>
         </div>
       </div>

       <div class="col-12">
         <div class="form-group">
           {{ Form::label('expiry_reason',__('Expiry Reason')) }}
           {{ Form::hidden('old_expiry_reason', $model->expiry_reason, ['class'=>'form-control','id'=>'expiry_reason','rows'=>'4']) }}
            <?php $model->expiry_reason ='';?>
           {{ Form::textarea('expiry_reason', null, ['class'=>'form-control','id'=>'expiry_reason','rows'=>'4']) }}
         </div>
       </div>
    </div>



  </div>
  <div class="card-footer">
  	<a href="{{ url('/subscription') }}" class="btn btn-default">{{__('common.cancel')}}</a>
    <button type="submit" class="btn btn-success">{{__('Save')}}</button>
  </div>
</div>

{!! Form::close() !!}


@if($results)
<div class="row shadow-none rounded-0 border">
	<div class="col-12">
		<!--begin::Card-->
		<div class="card card-custom gutter-b card-stretch shadow-none border-0 rounded-0">
			<!--begin::Body-->
			<div class="card-body shadow-none border-0 rounded-0">
				<!--begin::Content-->
				<div class="row">
					<div class="col-2">
						<span class="d-block font-weight-bold mb-2" style="">Created</span>
					</div>
					<div class="col-2">
						<span class="d-block font-weight-bold mb-2 " style="">Information</span>
					</div>
				</div>
				<?php
        foreach ($results as  $result) { ?>
					<div class="row mt-2">
						<div class="col-2">
							<span class=""><?= formatDateTime($result->created_at); ?></span>
						</div>
						<div class="col-10">
							<div class="row">
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old Start Date: </span> <span><?= formatDate($result->old_start_date); ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New Start Date: </span><span><?= formatDate($result->new_start_date); ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old End Date: </span> <span><?= formatDate($result->old_end_date); ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New End Date: </span><span><?= formatDate($result->new_end_date); ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old No of Nominees: </span> <span><?= $result->old_no_of_nominee; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New No of Nominees: </span><span><?= $result->new_no_of_nominee; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old No of Additional: </span> <span><?= $result->old_no_of_additional; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New No of Additional: </span><span><?= $result->new_no_of_additional; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old No of Named Associates: </span> <span><?= $result->old_no_of_named_associate; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New No of Named Associates: </span><span><?= $result->new_no_of_named_associate; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old No of Alternates: </span> <span><?= $result->old_no_of_alternate; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New No of Alternates: </span><span><?= $result->new_no_of_alternate; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">Old No of News: </span> <span><?= $result->old_no_of_news_per_month; ?></span>
								</div>
								<div class="col-3 mb-2">
									<span class="font-weight-bold">New No of News: </span><span><?= $result->new_no_of_news_per_month; ?></span>
								</div>
								<div class="col-12 pt-1">
									<span class="font-weight-bold">Reason: </span><span><?= $result->reason; ?></span>
								</div>
							</div>
						</div>
					</div>
					<hr>
				<?php }
         ?>
				<!--end::Content-->
			</div>
			<!--end::Body-->
			<!--begin::Footer-->
			<div class="card-footer d-flex align-items-center">
        {{$results->links()}}
			</div>
			<!--end::Footer-->
		</div>
		<!--end::Card-->
	</div>
</div>
@endif


@endsection

@push('js-script')


<!-- <script> -->

$('body').on('click','.add_p', function(){
  $('#type').val('+');
})

$('body').on('click','.add_m', function(){
  $('#type').val('-');
})


$('body').on('click','.expiry_adjustment', function(){




  ExpiryAdjustmentDays =  $('#expiry_adjustment_days').val();
  ExpiryReason =  $('#expiry_reason').val();
  type =  $('#type').val();


  if (!ExpiryAdjustmentDays) {
      Swal.fire("Error!", "Enter No OF Days", "error");
      return false;
  }
  if (!ExpiryReason) {
    Swal.fire("Error!", "Enter Date change Reason!", "error");
    return false;
  }
   ExpiryActualDate =   $('.expiry_old_date').val();

  data = {ExpiryAdjustmentDays:ExpiryAdjustmentDays,ExpiryReason:ExpiryReason,ExpiryActualDate:ExpiryActualDate,type:type, company_id:{{ $model->company_id }}};

  $.ajax({
    url:"<?= url('company/expiry-adjustment')?>",
    type:"POST",
    data: {
      "_token": "<?= csrf_token() ?>",
      data: data,
   },
   success:function (data) {

     if(data==1){
       if(Swal.fire("Good job!", "Expiry Changed successfully.", "success")){
         setTimeout(function () {
       location.reload(true);
     }, 2000);
       }
     }
     if(data==0){
       	if(Swal.fire("Error!", "Something went wrong!", "error")){
          location.reload(true);
        }
     }
   }
 });
});

@endpush

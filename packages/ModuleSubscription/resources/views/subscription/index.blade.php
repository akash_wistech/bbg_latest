@php


$btnsList = [];

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];
$advSearchCols = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('Membership Type'),'data'=>'membership_type_id','name'=>'membership_type_id'];
$dtColsArr[]=['title'=>__('Company'),'data'=>'company_id','name'=>'company_id'];
$dtColsArr[]=['title'=>__('Reference No'),'data'=>'reference_no','name'=>'reference_no'];
$dtColsArr[]=['title'=>__('Start Date'),'data'=>'start_date','name'=>'start_date'];
$dtColsArr[]=['title'=>__('End Date'),'data'=>'end_date','name'=>'end_date'];
$dtColsArr[]=['title'=>__('Nominees'),'data'=>'no_of_nominee','name'=>'no_of_nominee'];
$dtColsArr[]=['title'=>__('Additional'),'data'=>'no_of_additional','name'=>'no_of_additional'];
$dtColsArr[]=['title'=>__('Named Associates'),'data'=>'no_of_named_associate','name'=>'no_of_named_associate'];
$dtColsArr[]=['title'=>__('Alternates'),'data'=>'no_of_alternate','name'=>'no_of_alternate'];
$dtColsArr[]=['title'=>__('News'),'data'=>'no_of_news_per_month','name'=>'no_of_news_per_month'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])

<?php
if ($request->get('type')) {?>
  @section('title', __('Upcoming Renewal'))
  @section('breadcrumbs')
  @include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
    __('Upcoming Renewal')
  ]])
  @endsection
  <?php
}else { ?>
  @section('title', __('Subscription'))
  @section('breadcrumbs')
  @include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  __('Subscription')
  ]])
  @endsection
<?php } ?>





@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('Subscription')
]])
@endsection

<?php

$data='';
if ($request->get('type')) {
 $data= '?type='.$request->get('type');
}




 ?>

@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :showStats="true" :controller="'subscription'" :jsonUrl="'subscription/data-table-data'.$data" :dtColsArr="$dtColsArr" :advSearch="true" :exportMenu="true" :advSrchColArr="$advSearchCols" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>

@endsection

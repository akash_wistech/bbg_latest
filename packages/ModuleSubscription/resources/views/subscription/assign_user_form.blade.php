@extends('layouts.blank')
{!! Form::model($model, ['class' => 'simple-ajax-submit', 'data-pjxcntr' => $pjxcntr, 'data-blockcntr' => $pjxcntr]) !!}
<input type="hidden" name="gi" value="{{$gi}}" />
@if($usersToBeAdded->isNotEmpty())
<div class="row">
  @foreach($usersToBeAdded as $user)
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ $user->name }}
      {!! Form::select('user_type['.$user->id.']', $userTypesList, null, ['id'=>'user_id', 'class' => 'form-control', 'required'=>'true']) !!}
    </div>
  </div>
  @endforeach
</div>
<div class="card-footer text-right">
  <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
  <a href="javascript:;" data-dismiss="modal" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
@endif
{!! Form::close() !!}

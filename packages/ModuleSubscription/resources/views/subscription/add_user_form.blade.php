@extends('layouts.blank')
{!! Form::model($model, ['class' => 'simple-ajax-submit', 'data-pjxcntr' => $pjxcntr, 'data-blockcntr' => $pjxcntr]) !!}
@if($usersToBeAdded->isNotEmpty())
<div class="row">
  @foreach($usersToBeAdded as $user)
  <div class="col-12 col-sm-6">
    <div class="form-group">
      <div class="checkbox-inline">
        <label class="checkbox">
          {!! Form::checkbox('user_id[]', $user->id, false, ['class'=>'cbmsel']) !!}
          <span></span>{{$user->name}}
        </label>
      </div>
    </div>
  </div>
  @endforeach
</div>
<div class="card-footer text-right">
  <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
  <a href="javascript:;" data-dismiss="modal" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
@endif
{!! Form::close() !!}
@push('jsScripts')
var cbAmlimit = {{$allowedCount}};
$("body").on("change", "input.cbmsel", function (evt) {
  if($("input.cbmsel:checked").length >= cbAmlimit) {
    $("input.cbmsel").not(":checked").attr("disabled",true);
    $("input.cbmsel").not(":checked").parent("label.checkbox").addClass("checkbox-disabled");
  }else{
    $("input.cbmsel").not(":checked").removeAttr('disabled');
    $("input.cbmsel").not(":checked").parent("label.checkbox").removeClass("checkbox-disabled");
  }
});
@endpush

@extends('layouts.app')
@section('title', __('nam-subscription::subscription.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'subscription'=>__('nam-subscription::subscription.heading'),
'subscription/update/'.$model->id=>__('common.update_rec',['name'=>$model->reference_no]),
__('common.view')
]])
@endsection
@section('content')
<div id="grid-table">
  @include('nam-subscription::subscription._view_detail')
</div>
@endsection

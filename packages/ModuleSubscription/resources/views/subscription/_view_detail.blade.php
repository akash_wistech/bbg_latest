@php
$rq='m';
$pjxcntr = 'grid-table';
$pjxurl = '';
if($request->ajax()){
  $rq='a';
  $pjxcntr = 'subscription-info';
  $pjxurl = url('/subscription/view',['id'=>$model->id]);
}
@endphp
<div class="card card-custom shadow-none">
  <div class="card-header">
    <div class="card-title">
      <span class="card-icon">
        <i class="flaticon2-chat-1 text-primary"></i>
      </span>
      <h3 class="card-label">
        {{__('nam-subscription::subscription.subscription_detail',['reference_no'=>$model->reference_no.' ('.formatDate($model->start_date).' - '.formatDate($model->end_date).')'])}}
      </h3>
    </div>
  </div>
  <div class="card-body">
    <div class="row mb-5">
      <div class="col-12 col-sm-4">
        <strong>{{__('nam-subscription::subscription.membership_type')}}:</strong> {{$model->membershipType->service->title}}
      </div>
      <div class="col-12 col-sm-4">
        <strong>{{__('common.start_date')}}:</strong> {{formatDate($model->start_date)}}
      </div>
      <div class="col-12 col-sm-4">
        <strong>{{__('common.end_date')}}:</strong> {{formatDate($model->end_date)}}
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-12 col-sm-4">
        <strong>{{__('nam-subscription::subscription.no_of_nominee')}}:</strong> {{$model->nomineeCount}}
      </div>
      <div class="col-12 col-sm-4">
        <strong>{{__('nam-subscription::subscription.no_of_additional')}}:</strong> {{$model->additionalCount}}
      </div>
      <div class="col-12 col-sm-4">
        <strong>{{__('nam-subscription::subscription.no_of_named_associate')}}:</strong> {{$model->namedAssosiateCount}}
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-12 col-sm-4">
        <strong>{{__('nam-subscription::subscription.no_of_alternate')}}:</strong> {{$model->alternateCount}}
      </div>
      <div class="col-12 col-sm-4">
        <strong>{{__('nam-subscription::subscription.no_of_news_per_month')}}:</strong> {{$model->newsCount}}
      </div>
    </div>
  </div>
</div>
<div class="card card-custom shadow-none">
  <div class="card-header">
    <div class="card-title">
      <span class="card-icon">
        <i class="flaticon-users text-primary"></i>
      </span>
      <h3 class="card-label">
        {{__('Members')}}
      </h3>
    </div>
    <div class="card-toolbar">
      <div class="dropdown dropdown-inline" data-toggle="tooltip" title="{{__('common.add_user')}}">
        <a href="#" class="btn btn-icon btn-circle btn-sm btn-light-primary mr-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="flaticon2-plus-1"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right" style="">
          <ul class="navi navi-hover py-5">
            @foreach(getSubscriptionUserTypeListArr() as $key=>$val)
            <li class="navi-item">
              <a href="{{url('subscription/add-subscription-user/'.$model->id.'/'.$key.'?rq='.$rq)}}" class="navi-link poplink" data-heading="{{__('common.add_user_type',['type'=>$val])}}">
                <span class="navi-text">{{$val}}</span>
              </a>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body">
    <table class="table table-head-custom table-vertical-center">
      <thead>
        <tr>
          <th>{{__('common.member')}}</th>
          <th width="200">{{__('common.type')}}</th>
          <th width="200">{{__('common.added')}}</th>
          <th width=100>{{__('common.action')}}</th>
        </tr>
      </thead>
      <tbody>
        @if($model->subscriptionUsers->isNotEmpty())
        @foreach($model->subscriptionUsers as $user)
        @if($user->user!=null)
        <tr>
          <td>{{$user->user->name}}</td>
          <td>{{$user->user_type_label}}</td>
          <td>{{formatDateTime($user->created_at)}}</td>
          <td>
            @if(checkActionAllowed('update-subscription-user'))
            <a href="{{url('subscription/update-subscription-user/'.$user->id.'?rq='.$rq)}}" class="btn btn-sm btn-clean btn-icon poplink" data-heading="{{__('nam-subscription::subscription.change_user_type',['member'=>$user->user->name])}}" data-toggle="tooltip" title="{{__('common.update')}}">
              <i class="text-dark-50 flaticon-edit"></i>
            </a>
            @endif
            @if(checkActionAllowed('delete-subscription-user'))
            <a href="{{url('subscription/delete-subscription-user/'.$user->id)}}" class="btn btn-sm btn-clean btn-icon act-confirmation" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="{{$pjxcntr}}" data-pjxurl="{{$pjxurl}}" data-toggle="tooltip" title="{{__('common.delete')}}">
              <i class="text-dark-50 flaticon-delete"></i>
            </a>
            @endif
          </td>
        </tr>
          @endif
        @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>

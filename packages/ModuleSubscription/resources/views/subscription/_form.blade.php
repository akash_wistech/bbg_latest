@extends('layouts.blank')

@if($member_exists==1)
{!! Form::model($model, ['class' => 'simple-ajax-submit']) !!}
<div class="form-group">
  {{ Form::label('membership_type_id',__('nam-subscription::subscription.membership_type')) }}
  {{ Form::select('membership_type_id', getMembershipTypesListArr(), $model->membership_type_id, ['class'=>'form-control']) }}
</div>
<div class="row">
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('start_date',__('common.start_date'),['class'=>'required']) }}
      <div class="input-group date">
        {!! Form::text('start_date', $model->start_date, ['class' => 'form-control popdtpicker', 'autocomplete'=>'off', 'required'=>'true']) !!}
        <div class="input-group-append">
          <div class="input-group-text button-date-select"><i class="la la-calendar-check-o"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('end_date',__('common.end_date'),['class'=>'required']) }}
      <div class="input-group date">
        {!! Form::text('end_date', $model->end_date, ['class' => 'form-control popdtpicker', 'autocomplete'=>'off', 'required'=>'true']) !!}
        <div class="input-group-append">
          <div class="input-group-text button-date-select"><i class="la la-calendar-check-o"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('no_of_nominee',__('nam-subscription::subscription.no_of_nominee'),['class'=>'required']) }}
      {!! Form::text('no_of_nominee', $model->no_of_nominee, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('no_of_additional',__('nam-subscription::subscription.no_of_additional'),['class'=>'required']) }}
      {!! Form::text('no_of_additional', $model->no_of_additional, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('no_of_named_associate',__('nam-subscription::subscription.no_of_named_associate'),['class'=>'required']) }}
      {!! Form::text('no_of_named_associate', $model->no_of_named_associate, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('no_of_alternate',__('nam-subscription::subscription.no_of_alternate'),['class'=>'required']) }}
      {!! Form::text('no_of_alternate', $model->no_of_alternate, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="form-group">
      {{ Form::label('no_of_news_per_month',__('nam-subscription::subscription.no_of_news_per_month'),['class'=>'required']) }}
      <div class="input-group date">
        {!! Form::text('no_of_news_per_month', $model->no_of_news_per_month, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
        <div class="input-group-append">
          <div class="input-group-text">{{__('common.per_month')}}</div>
        </div>
      </div>
    </div>
  </div>
  @if($model->invoice_generated==false)
  <div class="col-12 col-sm-12">
    <div class="form-group">
      <div class="checkbox-inline">
        <label class="checkbox">
          {!! Form::checkbox('generate_invoice', 1, true) !!}
          <span></span>{{__('common.generate_invoice')}}
        </label>
      </div>
    </div>
  </div>
  @endif
</div>
@else
<div class="">
  <p style="font-size:16px">Please At least add one Member in the Company before creating subscrption.</p>

</div>
@endif
<div class="card-footer text-right">
  @if($member_exists==1)
  <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
  @endif
  <a href="javascript:;" data-dismiss="modal" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
{!! Form::close() !!}
@push('jsScripts')
var memberShipOptions = {!!json_encode(getMembershipOptions())!!};

arr_list = memberShipOptions[$("#membership_type_id").val()];
$("#no_of_nominee").val(arr_list['no_of_nominee']);
$("#no_of_additional").val(arr_list['no_of_additional']);
$("#no_of_named_associate").val(arr_list['no_of_named_associate']);
$("#no_of_alternate").val(arr_list['no_of_alternate']);
$("#no_of_news_per_month").val(arr_list['no_of_news_per_month']);



$('.popdtpicker').datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
  orientation: "bottom left",
  todayBtn: "linked",
  clearBtn: true,
  }).on('change', function(){
      $('.datepicker').hide();
  });

$('.button-date-select').click(function (e) {
    $(this).parents('.date').find('.popdtpicker').datepicker('show');

   });

$("body").on("change", "#membership_type_id", function (e) {
  arr_list = memberShipOptions[$(this).val()];
  $("#no_of_nominee").val(arr_list['no_of_nominee']);
  $("#no_of_additional").val(arr_list['no_of_additional']);
  $("#no_of_named_associate").val(arr_list['no_of_named_associate']);
  $("#no_of_alternate").val(arr_list['no_of_alternate']);
  $("#no_of_news_per_month").val(arr_list['no_of_news_per_month']);
});
@endpush()

<?php
namespace NaeemAwan\ModuleSubscription\Models;

use App;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use App\Models\User;

class SubscriptionUsers extends FullTables
{
  use Blameable, SoftDeletes;

  protected $table = 'subscription_users';
  protected $moduleTypeId = 'subscription_user';

  protected $fillable = [
    'subscription_id',
    'user_id',
  ];

  /**
  * Get subscription
  */
  public function subscription()
  {
    return $this->belongsTo(Subscription::class,'subscription_id');
  }

  /**
  * Get nominee user
  */
  public function user()
  {
    return $this->belongsTo(User::class,'user_id','id');
  }

  /**
  * Get user type label
  */
  public function getUserTypeLabelAttribute()
  {
    $label = '';
    $result = getPredefinedItem($this->user_type);
    if($result!=null){
      $label = $result->title;
    }
    return $label;
  }
}

<?php
namespace NaeemAwan\ModuleSubscription\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use App\Models\Company;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\News\Models\News;


class Subscription extends FullTables
{
  public $previous_id,$activate_id=1;
  public $generate_invoice=0;
  use HasFactory, Blameable, SoftDeletes;

  public $moduleTypeId = 'subscription';
  protected $table = 'subscription';

  protected $fillable = [
    'previous_id',
    'membership_type_id',
    'company_id',
    'start_date',
    'end_date',
    'no_of_months',
    'invoice_id',
  ];

  /**
  * Get the membership type
  */
  public function membershipType()
  {
    return $this->belongsTo(MembershipType::class,'membership_type_id');
  }

  /**
  * Get the company
  */
  public function company()
  {
    return $this->belongsTo(Company::class,'company_id');
  }

  /**
  * Get the renewed subscription
  */
  public function renewedSubscription()
  {
    return $this->belongsTo(Subscription::class,'renewed_subscription_id');
  }

  /**
  * Get the invoice
  */
  public function invoice()
  {
    return $this->belongsTo(Invoice::class,'company_id');
  }

  /**
  * Get the invoice
  */
  public function subscriptionUsers()
  {
    return $this->hasMany(SubscriptionUsers::class);
  }

  /**
  * return if invoice is generated
  */
  public function getInvoiceGeneratedAttribute()
  {
    $generated = false;
    $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->first();
    if($moduleInvoiceRow!=null){
      $generated = true;
    }
    return $generated;
  }

  /**
  * return generated invoice id
  */
  public function getGeneratedInvoiceIdAttribute()
  {
    $id = 0;
    $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->first();
    if($moduleInvoiceRow!=null){
      $id = $moduleInvoiceRow->invoice_id;
    }
    return $id;
  }

  /**
  * return nominee count
  */
  public function getNomineeCountAttribute()
  {
    $countStr='';
    $addedCount = SubscriptionUsers::where(['subscription_id'=>$this->id,'user_type'=>getNomineeUserTypeId()])->count('id');
    $countStr.=$addedCount.' / '.$this->no_of_nominee;
    return $countStr;
  }

  /**
  * return additional count
  */
  public function getAdditionalCountAttribute()
  {
    $countStr='';
    $addedCount = SubscriptionUsers::where(['subscription_id'=>$this->id,'user_type'=>getAdditionalUserTypeId()])->count('id');
    $countStr.=$addedCount.' / '.$this->no_of_additional;
    return $countStr;
  }

  /**
  * return named associate count
  */
  public function getNamedAssosiateCountAttribute()
  {
    $countStr='';
    $addedCount = SubscriptionUsers::where(['subscription_id'=>$this->id,'user_type'=>getNamedAssociateUserTypeId()])->count('id');
    $countStr.=$addedCount.' / '.$this->no_of_named_associate;
    return $countStr;
  }

  /**
  * return alternate count
  */
  public function getAlternateCountAttribute()
  {
    $countStr='';
    $addedCount = SubscriptionUsers::where(['subscription_id'=>$this->id,'user_type'=>getAlternateUserTypeId()])->count('id');
    $countStr.=$addedCount.' / '.$this->no_of_alternate;
    return $countStr;
  }

  /**
  * return news count
  */
  public function getNewsCountAttribute()
  {
    $countStr='';
    $subscriptionId = $this->id;
    $company_id = $this->company_id;
    // $addedCount = News::whereIn('created_by',function($query) use ($subscriptionId){
    //   $query->select('user_id')
    //   ->from(with(new SubscriptionUsers)->getTable())
    //   ->where('subscription_id', $subscriptionId);
    // })
    // ->where(function($query){
    //   $query->whereMonth('created_at',date("m"))
    //   ->whereYear('created_at',date("Y"));
    // })
    // ->count('id');
    $addedCount = News::where(function($query) use ($subscriptionId,$company_id){
      $query->where(function($query) use ($subscriptionId,$company_id){
        $query->where('related_to','company')
        ->where('rel_id',$company_id);
      })
      ->orWhere(function($query) use($subscriptionId,$company_id){
        $query->where('related_to','member')
        ->whereIn('rel_id',function($query) use($company_id){
          $query->select('module_id')
          ->from(with(new ModuleCompany)->getTable())
          ->where('module_type','contact')
          ->where('company_id', $company_id);
        });
      });
    })
    ->where(function($query){
      $query->whereMonth('created_at',date("m"))
      ->whereYear('created_at',date("Y"));
    })
    ->count('id');
    $countStr.=$addedCount.' / '.$this->no_of_news_per_month;
    return $countStr;
  }
}

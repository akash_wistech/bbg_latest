<?php
namespace NaeemAwan\ModuleSubscription\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use NaeemAwan\PredefinedLists\Models\PredefinedList;
use NaeemAwan\PredefinedLists\Models\PredefinedListPrice;

class MembershipType extends FullTables
{
  use HasFactory, Blameable, SoftDeletes;

  protected $table = 'membership_type';
  protected $moduleTypeId = 'membership_type';

  protected $fillable = [
    'service_id',
    'sort_order',
    'no_of_nominee',
    'no_of_additional',
    'no_of_named_associate',
    'no_of_alternate',
    'description',
    'status',
  ];

  /**
  * Get the membership type
  */
  public function service()
  {
    return $this->belongsTo(PredefinedList::class,'service_id');
  }

  public function rate()
  {
    return $this->belongsTo(PredefinedListPrice::class,'service_id','list_id');
  }
}

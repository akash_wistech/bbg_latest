<?php
namespace NaeemAwan\ModuleSubscription\Models;

use App;
use App\Traits\Blameable;
use App\Models\BlameableOnlyTables;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class SubscriptionChangeHistory extends BlameableOnlyTables
{
  use Blameable;

  protected $table = 'subscription_change_history';

  protected $fillable = [
    'subscription_id',
    'old_start_date',
    'old_end_date',
    'new_start_date',
    'new_end_date',
    'old_no_of_nominee',
    'new_no_of_nominee',
    'old_no_of_additional',
    'new_no_of_additional',
    'old_no_of_named_associate',
    'new_no_of_named_associate',
    'old_no_of_alternate',
    'new_no_of_alternate',
    'old_no_of_news_per_month',
    'new_no_of_news_per_month',
    'reason',
  ];

  /**
  * Get the subscription
  */
  public function subscription()
  {
    return $this->belongsTo(Subscription::class,'subscription_id');
  }
}

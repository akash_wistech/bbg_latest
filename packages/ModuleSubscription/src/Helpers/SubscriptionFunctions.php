<?php
use NaeemAwan\ModuleSubscription\Models\Subscription;
use NaeemAwan\ModuleSubscription\Models\MembershipType;
use NaeemAwan\ModuleSubscription\Models\SubscriptionUsers;
use Illuminate\Support\Facades\DB;

if(!function_exists('getCompanySubscriptionCount')){
  /**
  * Get company subscription count
  */
  function getCompanySubscriptionCount($id)
  {
    return Subscription::where('company_id',$id)->count('id');
  }
}

if(!function_exists('getServicesListArr')){
  /**
  * Get services from predefinedlists
  */
  function getServicesListArr()
  {
    $servicesList = getSetting('service_list_id');
    return getPredefinedListOptionsArr($servicesList);
  }
}

if(!function_exists('getMembershipOptions')){
  /**
  * Get membership options
  */
  function getMembershipOptions()
  {
    $options = [];
    $results = MembershipType::where('status',1)->get();
    if($results!=null){
      foreach($results as $result){
        $options[$result->id] = [
          'no_of_nominee' => $result->no_of_nominee,
          'no_of_additional' => $result->no_of_additional,
          'no_of_named_associate' => $result->no_of_named_associate,
          'no_of_alternate' => $result->no_of_alternate,
          'no_of_news_per_month' => $result->no_of_news_per_month,
        ];
      }
    }
    return $options;
  }
}

if(!function_exists('getSubscriptionInfo')){
  /**
  * Get services from predefinedlists
  */
  function getSubscriptionInfo($id)
  {
    $start = '';
    $end = '';
    $result = Subscription::where('id',$id)->first();
    if($result!=null){
      $start = $result->start_date;
      $end = $result->end_date;
    }
    return ['start_date'=>$start,'end_date'=>$end];
  }
}

if(!function_exists('getSubscriptionInfos')){
  /**
  * Get services from predefinedlists
  */
  function getSubscriptionInfos($id)
  {
    return Subscription::where('company_id',$id)
    ->where('is_active',1)
    ->whereNotIn('id',function($query) use ($id){
      $query->select('renewed_subscription')
      ->from(with(new Subscription)->getTable())
      ->where('company_id', $id)
      ->where('is_active',1);
    })->get();
  }
}

if(!function_exists('getFutureSubscriptionInfo')){
  /**
  * Get services from predefinedlists
  */
  function getFutureSubscriptionInfo($id)
  {
    return Subscription::where('company_id',$id)->whereDate('start_date','>',date("Y-m-d"))->where('is_active',0)->orderBy('id','asc')->first();
  }
}

if(!function_exists('getLastExpiredSubscriptionInfo')){
  /**
  * Get services from predefinedlists
  */
  function getLastExpiredSubscriptionInfo($id)
  {
    return Subscription::where('company_id',$id)->whereDate('end_date','<',date("Y-m-d"))->orderBy('id','desc')->first();
  }
}

if(!function_exists('getMembershipTypesList')){
  /**
  * Get membership types
  */
  function getMembershipTypesListArr()
  {
    $arr = [];
    $results = MembershipType::where('status',1)->get();
    if($results!=null){
      foreach($results as $result){
        $arr[$result->id] = $result->service->title;
      }
    }
    return $arr;
  }
}

if(!function_exists('getMembershipTypeNames')){
  /**
  * Get membership type names
  */
  function getMembershipTypeNames()
  {
    $arr = [];
    $results = MembershipType::get();
    if($results!=null){
      foreach($results as $result){
        $arr[] = $result->service->title;
      }
    }
    return $arr;
  }
}



if (!function_exists('getMembershipTypeStatusArr')) {
  /**
  * return status array
  */
  function getMembershipTypeStatusArr()
  {
    return [
      '1' => __('Active'),
      '0' => __('In-active'),
    ];
  }
}

if (!function_exists('getMembershipTypeStatusLabelArr')) {
  /**
  * return status array
  */
  function getMembershipTypeStatusLabelArr()
  {
    return [
      '1' => '<span class="label label-inline label-light-success font-weight-bold">'.__('Active').'</span>',
      '2' => '<span class="label label-inline label-light-danger font-weight-bold">'.__('In-Active').'</span>',
    ];
  }
}


if (!function_exists('getTitleArrFend')) {
  /**
  * return status array
  */
  function getTitleArrFend()
  {
    return [
      'Mr' => 'Mr',
      'Ms' => 'Ms',
      'Miss' => 'Miss',
      'Dr' => 'Dr',
      'Mrs' => 'Mrs',
      'HMCG' => 'HMCG',
      'H.E' => 'H.E',
      'Sir' => 'Sir'
    ];
  }
}

if (!function_exists('getOfferVasedCityArr')) {
  /**
  * return status array
  */
  function getOfferVasedCityArr()
  {
    return [
      'Abu Dhabi' => 'Abu Dhabi',
      'Dubai' => 'Dubai',
      'Ajman' => 'Ajman',
      'Sharjah' => 'Sharjah',
      'Fujairah' => 'Fujairah',
      'Ras Al Khaimah' => 'Ras Al Khaimah',
      'Umm Al Quwain' => 'Umm Al Quwain',
      'Other' => 'Other'
    ];
  }
}

if (!function_exists('getOfferBasedCountryArr')) {
  /**
  * return status array
  */
  function getOfferBasedCountryArr()
  {
    return [
      'uae' => 'United Arab Emirates',
      'uk' => 'United Kingdom',
      'other' => 'Other',
    ];
  }
}

if(!function_exists('getMemType')){
  /**
  * Get membership types
  */
  function getMemType($id)
  {
    $arr = [];
    $result = MembershipType::where('id',$id)->first();
    if ($result) {
      return $result->service->title;
    }
    return '';
  }
}

if (!function_exists('getSubscriptionUserTypeListArr')) {
  /**
  * return user types
  */
  function getSubscriptionUserTypeListArr()
  {
    $arr = [];
    $parent = getSetting('contactroles_list_id');
    $allowed = getSubscriptionAllowedUserTypes();
    $results = getPredefinedListItems($parent);
    if($results!=null){
      foreach($results as $result){
        if(in_array($result->id,$allowed)){
          $arr[$result->id] = $result->title;
        }
      }
    }
    return $arr;
  }
}

if (!function_exists('getUserTypeColName')) {
  /**
  * return count column name
  */
  function getUserTypeColName()
  {
    return [
      38=>'no_of_nominee',
      39=>'no_of_alternate',
      40=>'no_of_additional',
      41=>'no_of_named_associate'
    ];
  }
}

if (!function_exists('getSubscriptionAllowedUserTypes')) {
  /**
  * return nominee id
  */
  function getSubscriptionAllowedUserTypes()
  {
    return [38,39,40,41];
  }
}

if (!function_exists('getNomineeUserTypeId')) {
  /**
  * return nominee id
  */
  function getNomineeUserTypeId()
  {
    return 38;
  }
}

if (!function_exists('getAdditionalUserTypeId')) {
  /**
  * return additional id
  */
  function getAdditionalUserTypeId()
  {
    return 40;
  }
}

if (!function_exists('getNamedAssociateUserTypeId')) {
  /**
  * return name associate id
  */
  function getNamedAssociateUserTypeId()
  {
    return 41;
  }
}

if (!function_exists('getAlternateUserTypeId')) {
  /**
  * return alternate id
  */
  function getAlternateUserTypeId()
  {
    return 39;
  }
}

if (!function_exists('getSubscriptionColCount')) {
  /**
  * return count
  */
  function getSubscriptionColCount($model,$type,$id)
  {
    $countCol = getUserTypeColName()[$type];
    $currentCount = SubscriptionUsers::where('subscription_id',$id)
    ->where('user_type',$type)
    ->where('user_id','!=',$model->id)
    ->count('id');
    $currentDbCol = $model->subscription->{$countCol};
  }
}

if (!function_exists('generateFirstSubscription')) {
  /**
  * generates first subscription
  */
  function generateFirstSubscription($model,$request)
  {
    $membershipPackage = MembershipType::where('id',$model->reg_package_id)->first();
    $startDate=date("Y-m-d");
    $endDate = date("Y-m-d",strtotime("+1 year",strtotime($startDate)));
    $modelSubscription = new Subscription;
    $modelSubscription->activate_id = 0;
    $modelSubscription->generate_invoice = 1;
    $modelSubscription->company_id = $model->main_company_id;
    $totalMonths = getDataInDates('m',$startDate,$endDate);
    $totalDays = getDataInDates('d',$startDate,$endDate);
    $modelSubscription->membership_type_id = $model->reg_package_id;
    $modelSubscription->start_date = $startDate;
    $modelSubscription->end_date = $endDate;
    $modelSubscription->no_of_months = round($totalMonths);
    $modelSubscription->no_of_days = round($totalDays);

    $modelSubscription->no_of_nominee = $membershipPackage->no_of_nominee;
    $modelSubscription->no_of_additional = $membershipPackage->no_of_additional;
    $modelSubscription->no_of_named_associate = $membershipPackage->no_of_named_associate;
    $modelSubscription->no_of_alternate = $membershipPackage->no_of_alternate;
    $modelSubscription->no_of_news_per_month = $membershipPackage->no_of_news_per_month;
    if($modelSubscription->save()){

      DB::table($model->getTable())
      ->where('id', $model->id)
      ->update([
        'active_subscription_id' => $modelSubscription->id
      ]);

      if(function_exists('logAction')){
        $viewUrl = url('company/view/'.$model->main_company_id);
        $message = '<a href="'.url('user/view/'.$model->id).'">'.$model->full_name.'</a>';
        $message.= ' bought subscription: ';
        $message.= '<a href="'.$viewUrl.'">'.$modelSubscription->reference_no.'</a>';
        $logdata = [
          'module_type' => $modelSubscription->moduleTypeId,
          'module_id' => $modelSubscription->id,
          'message' => $message,
          'target_url' => $viewUrl,
          'action_type' => 'success',
        ];
        logAction($request,$logdata);
      }
    }
  }
}

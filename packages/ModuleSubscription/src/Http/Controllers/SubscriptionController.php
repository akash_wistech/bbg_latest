<?php
namespace NaeemAwan\ModuleSubscription\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\ExpiryReason;
use Validator;
use Auth;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use App\Models\User;
use NaeemAwan\ModuleSubscription\Models\Subscription;
use NaeemAwan\ModuleSubscription\Models\SubscriptionChangeHistory;
use NaeemAwan\ModuleSubscription\Models\SubscriptionUsers;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\ModuleInvoice;

class SubscriptionController extends Controller
{
  public $folderName='nam-subscription::subscription';
  public $controllerId='subscription';


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request,$id)
  {


    $model = $this->newModel();
    $model->company_id = $id;
    $startDate = date("Y-m-d");
    $previousSubscription = Subscription::where(['company_id'=>$model->company_id])->orderBy('end_date','desc')->first();
    if($previousSubscription!=null){
      // $startDate = date("Y-m-d",strtotime("+1 day",strtotime($previousSubscription->end_date)));
    }

    $model->start_date = $startDate;
    $model->end_date = date("Y-m-d",strtotime("+1 year",strtotime($model->start_date)));


    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'membership_type_id'=>'required|integer',
        'start_date'=>'required|date',
        'end_date'=>'required|date',
        'generate_invoice'=>'nullable|integer',
      ]);
      if(!$validator->fails()){
        $totalMonths = getDataInDates('m',$request->start_date,$request->end_date);
        $totalDays = getDataInDates('d',$request->start_date,$request->end_date);
        $model->membership_type_id = $request->membership_type_id;
        $model->start_date = $request->start_date;
        $model->end_date = $request->end_date;
        $model->no_of_months = round($totalMonths);
        $model->no_of_days = round($totalDays);
        $model->no_of_nominee = $request->no_of_nominee;
        $model->no_of_additional = $request->no_of_additional;
        $model->no_of_named_associate = $request->no_of_named_associate;
        $model->no_of_alternate = $request->no_of_alternate;
        $model->no_of_news_per_month = $request->no_of_news_per_month;
        $model->generate_invoice = $request->generate_invoice ?? 0;

                // dd($model->generate_invoice);
        if ($model->save()) {
          //If already started. update company.
          if(function_exists('logAction')){
            // $viewUrl = url('subscription/view/'.$model->id);
            $viewUrl = url('company/view/'.$model->company_id);
            $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
            $message.= ' created a subscription: ';
            $message.= '<a href="'.$viewUrl.'">'.$model->reference_no.'</a>';
            $logdata = [
              'module_type' => $model->moduleTypeId,
              'module_id' => $model->id,
              'message' => $message,
              'target_url' => $viewUrl,
              'action_type' => 'success',
            ];
            logAction($request,$logdata);
          }
          if($request->ajax()){
            //Select Subscription Users

            $subscriptionDates = formatDate($model->start_date).' - '.formatDate($model->end_date);
            $popHeading = __('nam-subscription::subscription.assign_users',['company'=>$model->company->title,'dates'=>$subscriptionDates]);
            return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('subscription/assign-subscription-user/'.$model->id.'?gi='.$model->generate_invoice),'heading'=>$popHeading]]);

            // if($model->generate_invoice==1){
            //   $generatedInvoiceId = $model->generated_invoice_id;
            //   if($generatedInvoiceId){
            //     $subscriptionDates = formatDate($model->start_date).' - '.formatDate($model->end_date);
            //     $popHeading = __('nam-subscription::subscription.generated_invoice_for',['company'=>$model->company->title,'dates'=>$subscriptionDates]);
            //     return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('invoices/update/'.$generatedInvoiceId),'heading'=>$popHeading]]);
            //   }
            //   return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.saved')]]);
            // }else{
            //   return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.saved')]]);
            // }
          }else{
            return redirect($this->controllerId)->with('success', __('nam-subscription::subscription.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::subscription.notsaved'),]]);
          }else{
            return redirect($this->controllerId)->with('error', __('nam-subscription::subscription.notsaved'));
          }
        }
      }else{
        return redirect($this->controllerId.'/create/'.$id)->withErrors($validator)->withInput($request->all());
      }
    }
    $controllerId = $this->controllerId;
    if($request->ajax()){
      if ($id) {
        $member_exists=0;
        $Members = \NaeemAwan\MultipleCompany\Models\ModuleCompany::where(['module_type'=>'contact','company_id'=>$id])->first();
        if ($Members) {
          $member_exists=1;
        }
      }
      $viewFile = $this->folderName.'._form';
    }else{
      $viewFile = $this->folderName.'.create';
    }
    return view($viewFile,compact('model','controllerId','request','member_exists'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function renew(Request $request,$id)
  {
    $previousSubscription = $this->findModel($id);
    if($previousSubscription!=null){
      $model = $this->newModel();
      $model->previous_id = $previousSubscription->id;
      $model->company_id = $previousSubscription->company_id;
      $startDate = date("Y-m-d",strtotime("+1 day",strtotime($previousSubscription->end_date)));

      $model->start_date = $startDate;
      $model->end_date = date("Y-m-d",strtotime("+1 year",strtotime($model->start_date)));


      if($request->isMethod('post')){
        $validator = Validator::make($request->all(), [
          'membership_type_id'=>'required|integer',
          'start_date'=>'required|date',
          'end_date'=>'required|date',
          'generate_invoice'=>'nullable|integer',
        ]);
        if(!$validator->fails()){
          $totalMonths = getDataInDates('m',$request->start_date,$request->end_date);
          $totalDays = getDataInDates('d',$request->start_date,$request->end_date);
          $model->membership_type_id = $request->membership_type_id;
          $model->start_date = $request->start_date;
          $model->end_date = $request->end_date;
          $model->no_of_months = round($totalMonths);
          $model->no_of_days = round($totalDays);
          $model->no_of_nominee = $request->no_of_nominee;
          $model->no_of_additional = $request->no_of_additional;
          $model->no_of_named_associate = $request->no_of_named_associate;
          $model->no_of_alternate = $request->no_of_alternate;
          $model->no_of_news_per_month = $request->no_of_news_per_month;
          $model->generate_invoice = $request->generate_invoice ?? 0;

                  // dd($model->generate_invoice);
          if ($model->save()) {
            //If already started. update company.
            if(function_exists('logAction')){
              // $viewUrl = url('subscription/view/'.$model->id);
              $viewUrl = url('company/view/'.$model->company_id);
              $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
              $message.= ' created a subscription: ';
              $message.= '<a href="'.$viewUrl.'">'.$model->reference_no.'</a>';
              $logdata = [
                'module_type' => $model->moduleTypeId,
                'module_id' => $model->id,
                'message' => $message,
                'target_url' => $viewUrl,
                'action_type' => 'success',
              ];
              logAction($request,$logdata);
            }
            if($request->ajax()){
              //Select Subscription Users

              $subscriptionDates = formatDate($model->start_date).' - '.formatDate($model->end_date);
              $popHeading = __('nam-subscription::subscription.assign_users',['company'=>$model->company->title,'dates'=>$subscriptionDates]);
              return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('subscription/assign-subscription-user/'.$model->id.'?gi='.$model->generate_invoice),'heading'=>$popHeading]]);

              // if($model->generate_invoice==1){
              //   $generatedInvoiceId = $model->generated_invoice_id;
              //   if($generatedInvoiceId){
              //     $subscriptionDates = formatDate($model->start_date).' - '.formatDate($model->end_date);
              //     $popHeading = __('nam-subscription::subscription.generated_invoice_for',['company'=>$model->company->title,'dates'=>$subscriptionDates]);
              //     return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('invoices/update/'.$generatedInvoiceId),'heading'=>$popHeading]]);
              //   }
              //   return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.saved')]]);
              // }else{
              //   return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.saved')]]);
              // }
            }else{
              return redirect($this->controllerId)->with('success', __('nam-subscription::subscription.saved'));
            }
          }else{
            if($request->ajax()){
              return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::subscription.notsaved'),]]);
            }else{
              return redirect($this->controllerId)->with('error', __('nam-subscription::subscription.notsaved'));
            }
          }
        }else{
          return redirect($this->controllerId.'/create/'.$id)->withErrors($validator)->withInput($request->all());
        }
      }
      $controllerId = $this->controllerId;
      if($request->ajax()){
        $viewFile = $this->folderName.'._form';
      }else{
        $viewFile = $this->folderName.'.create';
      }
      return view($viewFile,compact('model','controllerId','request'));
    }
  }

  /**
  * select users for the subscription
  *
  * @return \Illuminate\Http\Response
  */
  public function assignUsersToSubscription(Request $request,$id)
  {
    $pjxcntr='grid-table';
    $generate_invoice = 0;
    if($request->has('rq') && $request->input('rq')=='a')$pjxcntr='subscription-info';
    if($request->has('gi'))$generate_invoice=$request->input('gi');
    $model = $this->findModel($id);
    if($model!=null){
      $userTypesList = getSubscriptionUserTypeListArr();
      if($request->isMethod('post')){
        $validatedData = $request->validate([
          'user_type.*' => 'required|integer',
         ]);
         if($request->user_type!=null){
           foreach ($request->user_type as $userId => $userTypeId) {

            $countCol = getUserTypeColName()[$userTypeId];
            $currentDbCol = $model->{$countCol};

             // $currentCount = SubscriptionUsers::where('subscription_id',$model->id)
             // ->where('user_type',$userTypeId)
             // ->count('id');
             // if($currentCount>=$currentDbCol){
             //   return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::subscription.quota_full',['type'=>$userTypesList[$userTypeId]]),]]);
             // }
             //Save User
             $subscriptionUser = new SubscriptionUsers;
             $subscriptionUser->subscription_id = $model->id;
             $subscriptionUser->user_type = $userTypeId;
             $subscriptionUser->user_id = $userId;
             if($subscriptionUser->save()){
               //Update users active_subscription_id col
               //Save to history
               $history = new SubscriptionChangeHistory;
               $history->subscription_id = $model->id;
               $history->reason = 'Added user ('.$subscriptionUser->user->name.') as '.$userTypesList[$userTypeId].' in subscription '.$subscriptionUser->subscription->reference_no.'';
               $history->save();
               if($model->start_date<=date("Y-m-d")){
                 DB::table((new User)->getTable())
                 ->where('id', $userId)->update(['active_subscription_id' => $model->id]);
               }

               if(function_exists('logAction')){
                 //Log Action
                 $viewUrl = url('subscription/view/'.$subscriptionUser->subscription_id);
                 $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
                 $message.= ' added new '.$userTypesList[$userTypeId].' member in subscription: ';
                 $message.= '<a href="'.$viewUrl.'">'.$subscriptionUser->subscription->reference_no.'</a>';
                 $logdata = [
                   'module_type' => $subscriptionUser->subscription->moduleTypeId,
                   'module_id' => $subscriptionUser->subscription_id,
                   'message' => $message,
                   'target_url' => $viewUrl,
                   'action_type' => 'success',
                 ];
                 logAction($request,$logdata);
               }
             }
           }
         }
         if($generate_invoice==1){
           $generatedInvoiceId = $model->generated_invoice_id;
           if($generatedInvoiceId){
             $subscriptionDates = formatDate($model->start_date).' - '.formatDate($model->end_date);
             $popHeading = __('nam-subscription::subscription.generated_invoice_for',['company'=>$model->company->title,'dates'=>$subscriptionDates]);
             return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('invoices/update/'.$generatedInvoiceId),'heading'=>$popHeading]]);
           }
           return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.saved')]]);
         }else{
           return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.saved')]]);
         }
      }

      $usersToBeAdded = User::whereIn('id',function($query) use($model){
        $query->select('module_id')
        ->from((new ModuleCompany)->getTable())
        ->where('module_type', 'contact')
        ->where('company_id', $model->company_id);
      })
      ->whereNotIn('id',function($query) use ($model){
        //Users which are not subscribed to any active subscrption
        $query->select('user_id')
        ->from((new SubscriptionUsers)->getTable())
        ->whereIn('subscription_id',function($query) use ($model){
          $query->select('id')
          ->from((new Subscription)->getTable())
          ->where('is_active', 1)
          ->where('company_id', $model->company_id);
        })
        ->where('subscription_id', '!=', $model->id);
      })
      ->get();

      $userTypesList = getSubscriptionUserTypeListArr();

     return view('nam-subscription::subscription.assign_user_form', [
       'model'=>$model,
       'pjxcntr'=>$pjxcntr,
       'usersToBeAdded'=>$usersToBeAdded,
       'userTypesList'=>$userTypesList,
       'gi'=>$generate_invoice,
     ]);
    }
  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Subscription;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Subscription::where('id', $id)->first();
  }

  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view('nam-subscription::subscription.index',compact('moduleTypeId','request'));
  }

  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;

    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
    $id;
      if ($request->has('id')) {
      $id = (int)$request->input('id');
      $query->where('company_id', $id);
      }
      if ($request->has('company_id')) {
      $company_id = (int)$request->input('company_id');
      // dd($company_id);
      $query->where('company_id', $company_id);
      }
      if ($request->has('type')) {
        $time = strtotime(date("Y/m/d"));
        $one_month = date("Y-m-d", strtotime("+1 month", $time));
        $query->where('renewed_subscription',0);
        $query->whereDate('end_date', '>', date("Y-m-d"))->whereDate('end_date', '<', $one_month);
      }


    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where(function($query) use ($request){
      $query->where(function($query) use ($request){
        $query->where('p_reference_number','like','%'.$request->reference_no.'%')
        ->whereNull('reference_number');
      })
      ->orWhere('reference_number','like','%'.$request->reference_no.'%');
    })
    ;
    if($request->invoice_type!='')$query->where('invoice_type',$request->invoice_type);
    if($request->payment_status!='')$query->where('payment_status',$request->payment_status);
    if($request->admin_note!='')$query->where('admin_note','like','%'.$request->admin_note.'%');
    if($request->client_note!='')$query->where('client_note','like','%'.$request->client_note.'%');
    if($request->terms_text!='')$query->where('terms_text','like','%'.$request->terms_text.'%');

    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $invoiceTypes = getInvoiceTypesListArr();

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){

        $thisData=[];
        //Colors column
        $thisData['cb_col']='';

        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;
         $thisData['membership_type_id']=getMemType($model->membership_type_id);
        $company = getCompanyInfo($model->company_id);
        $thisData['company_id']=$company->title;
        $thisData['reference_no']=$model->reference_no;
        $thisData['start_date']=formatDate($model->start_date);
        $thisData['end_date']=formatDate($model->end_date);
        $thisData['no_of_nominee']=$model->no_of_nominee;
        $thisData['no_of_additional']=$model->no_of_additional;
        $thisData['no_of_named_associate']=$model->no_of_named_associate;
        $thisData['no_of_alternate']=$model->no_of_alternate;
        $thisData['no_of_news_per_month']=$model->no_of_news_per_month;


        // $thisData['name']=$model->name;
        // $companyNames = getRowMultipleCompanies($moduleTypeId,$model->id);
        // $thisData['company_name']=$companyNames;
        // $thisData['email']=$model->email;
        // $thisData['phone']=$model->phone;
        //
        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];


        if($model->renewed_subscription>0){
        }else{
          $subscriptionTooltip=__('nam-subscription::subscription.renew');
          $subscriptionPopupHeading=__('nam-subscription::subscription.renew_heading_for',['company'=>$company->title]);
          $actBtns[]='
          <a href="'.url('/subscription/renew',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit poplink" data-heading="'.$subscriptionPopupHeading.'" data-toggle="tooltip" title="'.$subscriptionTooltip.'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-layer"></i>
          </a>';
        }


        $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$this->newModel()->moduleTypeId,'module_id'=>$model->id])
        ->first();
        if($moduleInvoiceRow!=null){
          $invoiceId=$moduleInvoiceRow->invoice_id;
          $invoiceUrl = url('/invoices/view-invoice/'.$invoiceId);

          $tooltip = __('sales::invoices.view_invoice');
          $invoice = Invoices::where('id',$invoiceId)->first();
          if($invoice!=null && $invoice->invoice_no==null){
            $tooltip = __('sales::invoices.view_porforma_invoice');
          }
            $actBtns[]='
            <a href="'.$invoiceUrl.'" class="btn btn-sm btn-clean btn-icon view poplink" data-toggle="tooltip" title="'.$tooltip.'" data-id="'.$model->id.'">
            <i class="text-dark-50 flaticon-doc"></i>
            </a>';
            if($invoice!=null && $invoice->invoice_no==null){
              $tooltip = __('sales::invoices.generate_tax_invoice');
              $taxInvoiceUrl = url('/invoices/generate-tax-invoice/'.$invoice->id);
              $actBtns[]='
              <a href="'.$taxInvoiceUrl.'" class="btn btn-sm btn-clean btn-icon view act-confirmation" data-confirmationmsg="'.__('sales::invoices.confirm_generate_tax_invoice').'" data-pjxcntr="grid-table" data-toggle="tooltip" title="'.$tooltip.'" data-id="'.$model->id.'">
              <i class="text-dark-50 fas fa-file-invoice"></i>
              </a>';
            }
        }else{
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/generate-invoice',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-confirmationmsg="'.__('event_lang::invoice.confirm_generate_invoice').'" data-pjxcntr="grid-table" data-toggle="tooltip" title="'.__('common.generate_invoice').'" data-id="'.$model->id.'">
          <i class="text-dark-50 fas fa-file-invoice-dollar"></i>
          </a>';
        }
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="javascript:;" data-url="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon inline-detail" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'" data-heading="'.__('nam-subscription::subscription.subscription_detail',['reference_no'=>$model->reference_no.' ('.formatDateTime($model->start_date).' - '.formatDateTime($model->end_date).')']).'" data-target="subscription-info">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        // if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        // }
        // if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        // }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * detail view of model
  *
  * @return \Illuminate\Http\Response
  */
  public function show(Request $request,$id)
  {
    $model = $this->findModel($id);
    $viewFile = $this->folderName.'.view';
    if($request->ajax()){
      $viewFile = $this->folderName.'._view_detail';
    }
    return view($viewFile, compact('model','request'));
  }

  /**
  * Add a new member to subscription.
  *
  * @return \Illuminate\Http\Response
  */
  public function addSubscriptionUser(Request $request, $id, $type)
  {
    $pjxcntr='grid-table';
    if($request->has('rq') && $request->input('rq')=='a')$pjxcntr='subscription-info';
    $model = new SubscriptionUsers;
    $model->subscription_id = $id;
    $model->user_type = $type;
    $subscription = $model->subscription;

    $userTypesList = getSubscriptionUserTypeListArr();
    if($request->isMethod('post')){

      $validatedData = $request->validate([
        'user_id.*' => 'required|integer',
       ]);

       $newUserType = $type;
       $countCol = getUserTypeColName()[$newUserType];

       $currentDbCol = $subscription->{$countCol};

       if($request->user_id!=null){
         foreach ($request->user_id as $key => $val) {
           $currentCount = SubscriptionUsers::where('subscription_id',$subscription->id)
           ->where('user_type',$newUserType)
           ->count('id');
           if($currentCount>=$currentDbCol){
             return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::subscription.quota_full',['type'=>$userTypesList[$newUserType]]),]]);
           }
           //Save User
           $subscriptionUser = new SubscriptionUsers;
           $subscriptionUser->subscription_id = $id;
           $subscriptionUser->user_type = $type;
           $subscriptionUser->user_id = $val;
           if($subscriptionUser->save()){
             //Update users active_subscription_id col
             //Save to history
             $history = new SubscriptionChangeHistory;
             $history->subscription_id = $id;
             $history->reason = 'Added user ('.$subscriptionUser->user->name.') as '.$userTypesList[$newUserType].' in subscription '.$subscriptionUser->subscription->reference_no.'';
             $history->save();

             if($subscription->start_date<=date("Y-m-d")){
               DB::table((new User)->getTable())
               ->where('id', $val)->update(['active_subscription_id' => $subscription->id]);
             }

             if(function_exists('logAction')){
               //Log Action
               $viewUrl = url('subscription/view/'.$subscriptionUser->subscription_id);
               $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
               $message.= ' added new '.$userTypesList[$newUserType].' member in subscription: ';
               $message.= '<a href="'.$viewUrl.'">'.$subscriptionUser->subscription->reference_no.'</a>';
               $logdata = [
                 'module_type' => $subscriptionUser->subscription->moduleTypeId,
                 'module_id' => $subscriptionUser->subscription_id,
                 'message' => $message,
                 'target_url' => $viewUrl,
                 'action_type' => 'success',
               ];
               logAction($request,$logdata);
             }
           }
         }
       }

       return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.user_updated'),]]);
     }

     $usersToBeAdded = User::whereIn('id',function($query) use($id,$type,$subscription){
       $query->select('module_id')
       ->from((new ModuleCompany)->getTable())
       ->where('module_type', 'contact')
       ->where('company_id', $subscription->company_id);
     })
     ->whereNotIn('id',function($query) use ($id,$type){
       $query->select('user_id')
       ->from((new SubscriptionUsers)->getTable())
       ->where('subscription_id', $id)
       ->whereNull('deleted_at');
     })
     ->get();

     $allowedCount = 0;

     $countCol = getUserTypeColName()[$type];
     $currentCount = SubscriptionUsers::where('subscription_id',$subscription->id)
     ->where('user_type',$type)
     ->count('id');
     $currentDbCol = $subscription->{$countCol};
     $allowedCount = $currentDbCol-$currentCount;

    return view('nam-subscription::subscription.add_user_form', [
      'model'=>$model,
      'pjxcntr'=>$pjxcntr,
      'allowedCount'=>$allowedCount,
      'usersToBeAdded'=>$usersToBeAdded,
    ]);
  }

  /**
  * Updates the specified resource from storage.
  *
  * @return \Illuminate\Http\Response
  */
  public function updateSubscriptionUser(Request $request, $id)
  {
    $pjxcntr='grid-table';
    if($request->has('rq') && $request->input('rq')=='a')$pjxcntr='subscription-info';
    $model = $this->findSubscriptionUserModel($id);
    $userTypesList = getSubscriptionUserTypeListArr();
    $oldUserType = $model->user_type;
    if($request->isMethod('post')){

      $validatedData = $request->validate([
        'user_type' => 'required|integer',
       ]);
       $newUserType = $request->user_type;

       $countCol = getUserTypeColName()[$request->user_type];
       $currentCount = SubscriptionUsers::where('subscription_id',$model->subscription_id)
       ->where('user_type',$newUserType)
       ->where('user_id','!=',$model->user_id)
       ->count('id');
       $currentDbCol = $model->subscription->{$countCol};
       if($currentCount>=$currentDbCol){
         return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::subscription.quota_full',['type'=>$userTypesList[$newUserType]]),]]);
       }
       $model->user_type = $request->user_type;
       $model->save();


       if($model->subscription->start_date<=date("Y-m-d")){
         DB::table((new User)->getTable())
         ->where('id', $model->user_id)->update(['active_subscription_id' => $model->subscription->id]);
       }

       $history = new SubscriptionChangeHistory;
       $history->subscription_id = $model->subscription_id;
       $history->reason = 'Changed user ('.$model->user->name.') from '.$userTypesList[$oldUserType].' to '.$userTypesList[$newUserType].' in subscription '.$model->subscription->reference_no.'';
       $history->save();

       if(function_exists('logAction')){
         $viewUrl = url('subscription/view/'.$model->subscription_id);
         $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
         $message.= ' changed user type in subscription: ';
         $message.= '<a href="'.$viewUrl.'">'.$model->subscription->reference_no.'</a>';
         $logdata = [
           'module_type' => $model->subscription->moduleTypeId,
           'module_id' => $model->subscription_id,
           'message' => $message,
           'target_url' => $viewUrl,
           'action_type' => 'success',
         ];
         logAction($request,$logdata);
       }
       return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::subscription.user_updated'),]]);
     }
     return view('nam-subscription::subscription.update_user_form', [
       'model'=>$model,
       'pjxcntr'=>$pjxcntr,
     ]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @return \Illuminate\Http\Response
  */
  public function destroySubscriptionUser(Request $request, $id)
  {
    $model = $this->findSubscriptionUserModel($id);
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    $history = new SubscriptionChangeHistory;
    $history->subscription_id = $model->subscription_id;
    $history->reason = 'Removed user ('.$model->user->name.') from subscription '.$model->subscription->reference_no;
    $history->save();

    DB::table((new User)->getTable())
    ->where('id', $model->user_id)->update(['active_subscription_id' => 0]);

   if(function_exists('logAction')){
     $viewUrl = url('subscription/view/'.$model->subscription_id);
     $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
     $message.= ' removed user ('.$model->user->name.') from subscription: ';
     $message.= '<a href="'.$viewUrl.'">'.$model->subscription->reference_no.'</a>';
     $logdata = [
       'module_type' => $model->subscription->moduleTypeId,
       'module_id' => $model->subscription_id,
       'message' => $message,
       'target_url' => $viewUrl,
       'action_type' => 'success',
     ];
     logAction($request,$logdata);
   }
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findSubscriptionUserModel($id)
  {
    return SubscriptionUsers::where('id', $id)->first();
  }

  public function generateInvoice(Request $request, $id){
    $model = $this->findModel($id);
    $model->generate_invoice = 1;
    $model->updated_at = date("Y-m-d H:i:s");
    if ($model->update()) {
      $generatedInvoiceId = $model->generated_invoice_id;
      if($generatedInvoiceId){
        $subscriptionDates = formatDate($model->start_date).' - '.formatDate($model->end_date);
        $popHeading = __('nam-subscription::subscription.generated_invoice_for',['company'=>$model->company->title,'dates'=>$subscriptionDates]);
        return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('invoices/update/'.$generatedInvoiceId),'heading'=>$popHeading]]);
      }
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::subscription.notsaved'),]]);
    }
  }


  public function update(Request $request, $id){

    $model = Subscription::where('id', $id)->first();
    $old_start_date = $model->start_date;
    $old_end_date = $model->end_date;
    $old_no_of_nominee = $model->no_of_nominee;
    $old_no_of_additional = $model->no_of_additional;
    $old_no_of_named_associate = $model->no_of_named_associate;
    $old_no_of_alternate = $model->no_of_alternate;
    $old_no_of_news_per_month = $model->no_of_news_per_month;
    if($request->isMethod('post')){

      $validatedData = $request->validate([
        'expiry_reason' => 'required|string',
        'start_date' => 'required|date',
        'start_date' => 'required|date',
        'end_date' => 'required|date',
       ]);

       $model->membership_type_id = $request->membership_type_id;
       $model->start_date = $request->start_date;
       $model->end_date = $request->end_date;
       $model->no_of_nominee = $request->no_of_nominee;
       $model->no_of_additional = $request->no_of_additional;
       $model->no_of_named_associate = $request->no_of_named_associate;
       $model->no_of_alternate = $request->no_of_alternate;
       $model->no_of_news_per_month = $request->no_of_news_per_month;
       $model->save();

       $expiry = new SubscriptionChangeHistory;
       $expiry->subscription_id = $model->id;
       $expiry->old_start_date = $old_start_date;
       $expiry->old_end_date = $old_end_date;
       $expiry->new_start_date = $request->start_date;
       $expiry->new_end_date = $request->end_date;
       $expiry->old_no_of_nominee = $old_no_of_nominee;
       $expiry->new_no_of_nominee = $request->no_of_nominee;
       $expiry->old_no_of_additional = $old_no_of_additional;
       $expiry->new_no_of_additional = $request->no_of_additional;
       $expiry->old_no_of_named_associate = $old_no_of_named_associate;
       $expiry->new_no_of_named_associate = $request->no_of_named_associate;
       $expiry->old_no_of_alternate = $old_no_of_alternate;
       $expiry->new_no_of_alternate = $request->no_of_alternate;
       $expiry->old_no_of_news_per_month = $old_no_of_news_per_month;
       $expiry->new_no_of_news_per_month = $request->no_of_news_per_month;
       $expiry->reason = $request->expiry_reason;
       $expiry->save();

       return redirect('subscription/update/'.$model->id)->with('success', __('Subscription has been changed.'));
    }

    $results = SubscriptionChangeHistory::where('subscription_id',$model->id)->orderBy('created_at','desc')->paginate(10);

    return view('nam-subscription::subscription.record_subscription', [
      'model'=>$model,
      'request'=>$request,
      'results'=>$results,
    ]);

  }

}

<?php

namespace NaeemAwan\ModuleSubscription\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\ModuleSubscription\Models\MembershipType;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class MembershipTypeController extends Controller
{
  public $folderName = 'membership-types';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    // dd("hello");
    $results=MembershipType::get();
    return view('nam-subscription::'.$this->folderName.'.index', compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    // dd($request->all());
    $model = $this->newModel();
    if($request->isMethod('post')){
      $data=$request->all();

      $validatedData = $request->validate([
        'service_id'=>'required|integer',
        'description'=>'required',
      ]);

      $model->service_id=$request->service_id;
      $model->sort_order=$request->sort_order;
      $model->no_of_nominee=$request->no_of_nominee;
      $model->no_of_additional=$request->no_of_additional;
      $model->no_of_named_associate=$request->no_of_named_associate;
      $model->no_of_alternate=$request->no_of_alternate;
      $model->no_of_news_per_month=$request->no_of_news_per_month;
      $model->status=$request->status;
      $model->description=$request->description;
      $model->created_by = Auth::user()->id;

      if ($model->save()) {
        if(function_exists('logAction')){
          $viewUrl = url('membership-type/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' created a membership type: ';
          $message.= '<a href="'.$viewUrl.'">'.$model->service->title.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'success',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::membership_type.saved'),]]);
        }else{
          return redirect('membership-type')->with('success', __('nam-subscription::membership_type.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.notsaved'),]]);
        }else{
          return redirect('membership-type')->with('error', __('nam-subscription::membership_type.notsaved'));
        }
      }
    }
    return view('nam-subscription::'.$this->folderName.'.create',compact('model'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \NaeemAwan\ModuleSubscription\Models\MembershipType  $membershipType
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = $this->findModel($id);
    if($request->isMethod('post')){
      $data=$request->all();

      $validatedData = $request->validate([
        'service_id'=>'required|integer',
        'description'=>'required',
      ]);

      $model->service_id=$request->service_id;
      $model->sort_order=$request->sort_order;
      $model->no_of_nominee=$request->no_of_nominee;
      $model->no_of_additional=$request->no_of_additional;
      $model->no_of_named_associate=$request->no_of_named_associate;
      $model->no_of_alternate=$request->no_of_alternate;
      $model->no_of_news_per_month=$request->no_of_news_per_month;
      $model->status=$request->status;
      $model->description=$request->description;
      $model->updated_at = date("Y-m-d H:i:s");

      if ($model->update()) {
        if(function_exists('logAction')){
          $viewUrl = url('membership-type/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' updated membership type: ';
          $message.= '<a href="'.$viewUrl.'">'.$model->service->title.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'info',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::membership_type.saved'),]]);
        }else{
          return redirect('membership-type')->with('success', __('nam-subscription::membership_type.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.notsaved'),]]);
        }else{
          return redirect('membership-type')->with('error', __('nam-subscription::membership_type.notsaved'));
        }
      }
    }
    return view('nam-subscription::'.$this->folderName.'.update', [
      'model'=>$model,
    ]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \NaeemAwan\ModuleSubscription\Models\MembershipType  $membershipType
  * @return \Illuminate\Http\Response
  */
  public function delete(Request $request, $id)
  {
    $model = $this->findModel($id);
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    if(function_exists('logAction')){
      $viewUrl = url('membership-type/view/'.$model->id);
      $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
      $message.= ' trashed membership type: ';
      $message.= ''.$model->service->title.'';
      $logdata = [
        'module_type' => $model->moduleTypeId,
        'module_id' => $model->id,
        'message' => $message,
        'target_url' => $viewUrl,
        'action_type' => 'danger',
      ];
      logAction($request,$logdata);
    }
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::membership_type.deleted'),]]);
  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new MembershipType;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return MembershipType::where('id', $id)->first();
  }



}

<?php

namespace NaeemAwan\ModuleSubscription\Http\Controllers\FrontendControllers;

use NaeemAwan\ModuleSubscription\Models\MembershipType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Contact;
use App\Models\Company;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use App\Rules\DuplicateUserCheckFrontend;
use Illuminate\Auth\Events\Registered;

class MembershipTypeController extends Controller
{
  public $folderName = 'frontend-views';

  public function newModel()
  {
    return new MembershipType;
  }

  public function findModel($id)
  {
    return MembershipType::where('id', $id)->first();
  }

  public function index()
  {
    $membershipTypes = MembershipType::where(['status'=>1])->orderBy('sort_order', 'asc')->get();
    return view('nam-subscription::'.$this->folderName.'.membership',compact('membershipTypes'));
  }



  public function SignUp(Request $request,$id)
  {
    return view('nam-subscription::'.$this->folderName.'.signupformembership',compact('id'));
  }

  public function registerNow(Request $request)
  {
    if($request->isMethod('post')){

      $validator = Validator::make($request->all(), [
        'id'=>'required|integer',
        'name'=>'required|string',
        'phone'=>'required',
        'email'=>'required|email',
        'emails.*'=>'required|email',
        'compnay_name'=>'required|string',
        'compnay_email'=>'required|email',
        'password'=>'required|min:6|required_with:repassword|same:repassword',
        'terms'=>'required|integer',
      'g-recaptcha-response' => 'recaptcha',
        'duplicatecheck'=>new DuplicateUserCheckFrontend('contact',['email'=>$request->email,'phone'=>$request->phone])
      ]);

      if(!$validator->fails()){
        $duplicateRow = DuplicateUserCheckFrontend('contact',['email'=>$request->email]);
        if($duplicateRow!=null){
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=> __('Email is already registered')]]);
          }else{
            return redirect('signup/'.$request->id)->withErrors($validator->errors())->withInput($request->all())->with('error', __('Email is already registered'));
          }
        }
        $duplicateRow = DuplicateUserCheckFrontend('contact',['phone'=>$request->phone]);
        if($duplicateRow!=null){
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('Phone number is already registered'),]]);
          }else{
            return redirect('signup/'.$request->id)->withErrors($validator->errors())->withInput($request->all())->with('error', __('Phone number is already registered'));
          }
        }


        $jobTitleId = $request->designation_id;
        $model = new Contact;
        $model->name = $request->name;
        $model->email = $request->email;
        $model->phone = $request->phone;
        $model->image = trim($request->image);
        $model->passport_copy = trim($request->passport_copy);
        $model->residence_visa = trim($request->residence_visa);
        $model->permission_group_id = getSetting('clients_permission_group');
        $model->newsletter = $request->newsletter?? 0;
        $model->reg_package_id = $request->id;
        $model->referred_by = $request->referred_by;
        $model->password = $request->password!='' ? Hash::make($request->password) : null;
        $model->status = 10;
        if ($model->save()) {
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          if($request->newsletter==1){
            if(function_exists('subscribeUser')){
              subscribeUser($model);
            }
          }

          saveMultipleEmailNumber($model,$request,'form');
          saveCustomField($model,$request,'form');

          $company = new Company;
          $company->title = trim($request->compnay_name);
          $company->email = trim($request->compnay_email);
          $company->vat_number = trim($request->vat_number);
          $company->logo = trim($request->logo);
          $company->trade_licence = trim($request->trade_licence);
          $company->vat_document = trim($request->vat_document);
          $company->parent_company_id = $request->parent_company_id ?? 0;
          if ($company->save()) {
            if($request->input_field!=null){
              foreach($request->input_field as $key=>$val){
                $company->input_field[$key]=$val;
              }
            }
            saveCustomField($company,$request,'form');
            //Save User Company Info
            $modelCompany=ModuleCompany::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'company_id'=>$company->id])->first();
            if($modelCompany==null){
              $modelCompany=new ModuleCompany;
              $modelCompany->module_type=$model->moduleTypeId;
              $modelCompany->module_id=$model->id;
              $modelCompany->company_id=$company->id;
              $modelCompany->role_id=0;
              $modelCompany->job_title_id=$jobTitleId;
              $modelCompany->save();
            }

          }

          event(new Registered($model));

          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::membership_type.saved'),]]);
          }else{
            return redirect('/')->with('success', __('nam-subscription::membership_type.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.notsaved'),]]);
          }else{
            return redirect('signup/'.$request->id)->withErrors($validator)->withInput($request->all())->with('error', __('nam-subscription::membership_type.notsaved'));
          }
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['errors' => $validator->errors(),'error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.notsaved'),]]);
        }else{
          return redirect('signup/'.$request->id)->withErrors($validator)->withInput($request->all());
        }
      }
    }
  }


  public function MembershipFendUpdate(Request $request, $id=null)
  {
    // dd($request->all());
    $model = $this->findModel(Auth::user()->id);

    $contactModel =  Contact::where(['id'=>$id])->first();
    // dd($contactModel);

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'name'=>'required|string',
        'phone'=>'required',
        'email'=>'required|email',
        'emails.*'=>'required|email',
      ]);



      if(!$validator->fails()){

        // dd(trim($request->image));
        $contactModel =  Contact::where(['id'=>$id])->first();
        $contactModel->name = $request->name;
        $contactModel->email = $request->email;
        $contactModel->phone = $request->phone;
        $contactModel->image = trim($request->image);
        // $contactModel->passport_copy = trim($request->passport_copy);
        // $contactModel->residence_visa = trim($request->residence_visa);
        $contactModel->permission_group_id = getSetting('clients_permission_group');
        $contactModel->referred_by = $request->referred_by;
        $contactModel->status = 10;

        if ($contactModel->save()) {
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $contactModel->input_field[$key]=$val;
            }
          }

          saveMultipleEmailNumber($contactModel,$request,'form');
          saveCustomField($contactModel,$request,'form');

          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::membership_type.saved_profile'),]]);
          }else{
            return redirect('membershipFend/update/'.$id)->with('success', __('nam-subscription::membership_type.saved_profile'));
          }

        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.error_profile'),]]);
          }else{
            return redirect('membershipFend/update/'.$id)->withErrors($validator)->withInput($request->all())->with('error', __('nam-subscription::membership_type.error_profile'));
          }
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['errors' => $validator->errors(),'error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.error_profile'),]]);
        }else{
          return redirect('membershipFend/update/'.$id)->withErrors($validator)->withInput($request->all());
        }
      }
    }

    return view('nam-subscription::'.$this->folderName.'.update',compact('model','contactModel'));
  }


  public function paySubscription()
  {
    return redirect('/pay-tabs/'.Auth::user()->activeSubscription->generated_invoice_id);
  }



}

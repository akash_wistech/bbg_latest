<?php

namespace NaeemAwan\ModuleSubscription\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\ModuleSubscription\Models\Subscription;
use NaeemAwan\ModuleSubscription\Observers\SubscriptionObserver;

class SubscriptionServiceProvider extends ServiceProvider
{
  public function boot()
  {
    Subscription::observe(SubscriptionObserver::class);
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'nam-subscription');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'nam-subscription');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'nam-subscription-migrations');
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

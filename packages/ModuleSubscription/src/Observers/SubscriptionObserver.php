<?php
namespace NaeemAwan\ModuleSubscription\Observers;

use NaeemAwan\ModuleSubscription\Models\Subscription;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Sales\Models\Invoices;
use Illuminate\Support\Facades\DB;

class SubscriptionObserver
{
  /**
  * Handle the Subscription "created" event.
  *
  * @param  \NaeemAwan\ModuleSubscription\Models\Subscription  $subscription
  * @return void
  */
  public function created(Subscription $subscription)
  {
    if ($subscription->id) {
      $reference='MS'.'-'.date('y').'-'.sprintf('%03d', $subscription->id);

      DB::table($subscription->getTable())
      ->where('id', $subscription->id)
      ->update(['reference_no' => $reference]);

      $this->checkAndPerformAdditionalTask($subscription);
    }
  }

  /**
  * Handle the Subscription "updated" event.
  *
  * @param  \App\Models\Subscription  $subscription
  * @return void
  */
  public function updated(Subscription $subscription)
  {
    $this->checkAndPerformAdditionalTask($subscription);
  }

  /**
  * Handle the Subscription "deleted" event.
  *
  * @param  \App\Models\Subscription  $subscription
  * @return void
  */
  public function deleted(Subscription $subscription)
  {
    //
  }

  /**
  * Handle the Subscription "restored" event.
  *
  * @param  \App\Models\Subscription  $subscription
  * @return void
  */
  public function restored(Subscription $subscription)
  {
    //
  }

  /**
  * Handle the Subscription "force deleted" event.
  *
  * @param  \App\Models\Subscription  $subscription
  * @return void
  */
  public function forceDeleted(Subscription $subscription)
  {
    //
  }

  public function checkAndPerformAdditionalTask($subscription)
  {
    $totalSubscriptions = Subscription::where(['company_id'=>$subscription->company_id])->count('id');
    if($subscription->activate_id==1 && $subscription->activated==0){
      //Check if date is already started
      if($subscription->start_date<=date("Y-m-d") || $totalSubscriptions<=1){
        //Update Company
        // DB::table($subscription->company->getTable())
        // ->where('id', $subscription->company_id)
        // ->update(['active_subscription_id' => $subscription->id]);

        //Activate It
        DB::table($subscription->getTable())
        ->where('id', $subscription->id)
        ->update(['is_active'=>1,'activated' => 1]);

        //Updates subscription users with active subscription_id
      }
    }


    //Update Previous subscription with renewed one
    if($subscription->previous_id>0){
      $previousSubscription = Subscription::where('id',$subscription->previous_id)->first();
      if($previousSubscription!=null && $previousSubscription->renewed_subscription<=0){
        DB::table($previousSubscription->getTable())
        ->where('id', $previousSubscription->id)
        ->update(['renewed_subscription' => $subscription->id]);
      }
    }

    //Check & Generate Invoice
    if($subscription->generate_invoice==1){
      $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$subscription->moduleTypeId,'module_id'=>$subscription->id])
      ->first();
      if($moduleInvoiceRow==null){
        $invoice = new Invoices;
        $invoice->company_id = $subscription->company_id;
        $invoice->user_id = $subscription->company->first_primary_contact_id;
        $invoice->invoice_type = 1;
        $invoice->p_invoice_date = date("Y-m-d");
        $invoice->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($invoice->p_invoice_date))));
        $invoice->currency_id = getSetting('currency');
        $invoice->is_recurring = 0;
        $invoice->discount_calculation = 'aftertax';
        $item_type = [];
        $item_id = [];
        $title = [];
        $descp = [];
        $qty = [];
        $rate = [];
        $discount = [];
        $discount_type = [];
        $tax_id = [];
        if($totalSubscriptions<=1){
          //New add Joining Fees
          $serviceJoiningItem = getPredefinedItem(getSetting('joining_fee_id'));
          $item_type[] = 'service';
          $item_id[] = $serviceJoiningItem->id;
          $title[] = $serviceJoiningItem->title;
          $descp[] = $serviceJoiningItem->description;
          $qty[] = 1;
          $rate[] = $serviceJoiningItem->pricing->rate;
          $discount[] = 0;
          $discount_type[] = 'percentage';
          $tax_id[] = $serviceJoiningItem->pricing->tax_id;
        }

        //Package Fees
        $packageItem = getPredefinedItem($subscription->membershipType->service_id);
        $description = $packageItem->description;
        $description.= ($description!='' ? "\n\r" : '');
        $description.= 'Start date: '.formatDate($subscription->start_date);
        $description.= "\n\r";
        $description.= 'End date: '.formatDate($subscription->end_date);
        $item_type[] = 'service';
        $item_id[] = $packageItem->id;
        $title[] = $packageItem->title;
        $descp[] = $description;
        $qty[] = 1;
        $rate[] = $packageItem->pricing->rate;
        $discount[] = 0;
        $discount_type[] = 'percentage';
        $tax_id[] = $packageItem->pricing->tax_id;

        $invoice->item_type = $item_type;
        $invoice->item_id = $item_id;
        $invoice->title = $title;
        $invoice->descp = $descp;
        $invoice->qty = $qty;
        $invoice->rate = $rate;
        $invoice->discount = $discount;
        $invoice->discount_type = $discount_type;
        $invoice->tax_id = $tax_id;

        $invoice->terms_text = getInvoicesTermsContent();
        if($invoice->save()){
          $invoiceModule = new ModuleInvoice;
          $invoiceModule->module_type = $subscription->moduleTypeId;
          $invoiceModule->module_id = $subscription->id;
          $invoiceModule->invoice_id = $invoice->id;
          $invoiceModule->save();
        }
      }
    }
  }
}

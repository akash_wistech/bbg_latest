<?php

use Illuminate\Support\Facades\Route;
use NaeemAwan\ModuleSubscription\Http\Controllers\SubscriptionController;

Route::get('/subscription', [SubscriptionController::class,'index']);
Route::get('/subscription/data-table-data', [SubscriptionController::class,'datatableData']);
Route::match(['get', 'post'], '/subscription/create/{id}', [SubscriptionController::class,'create']);
Route::match(['get', 'post'], '/subscription/renew/{id}', [SubscriptionController::class,'renew']);
// Route::match(['get', 'post'], '/subscription/create', [SubscriptionController::class,'create']);
Route::match(['get', 'post'], '/subscription/update/{id}', [SubscriptionController::class,'update']);
Route::match(['post','get'],'/subscription/view/{id}', [SubscriptionController::class,'show']);
Route::post('/subscription/delete/{id}', [SubscriptionController::class,'destroy']);

Route::match(['get', 'post'], '/subscription/assign-subscription-user/{id}', [SubscriptionController::class,'assignUsersToSubscription']);
Route::match(['get', 'post'], '/subscription/add-subscription-user/{id}/{type}', [SubscriptionController::class,'addSubscriptionUser']);
Route::match(['get', 'post'], '/subscription/update-subscription-user/{id}', [SubscriptionController::class,'updateSubscriptionUser']);
Route::post('/subscription/delete-subscription-user/{id}', [SubscriptionController::class,'destroySubscriptionUser']);

Route::post('/subscription/generate-invoice/{id}', [SubscriptionController::class,'generateInvoice']);

<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\ModuleSubscription\Http\Controllers\FrontendControllers\MembershipTypeController;
use App\Http\Controllers\Auth\VerificationController;

Route::get('applyformembership', [MembershipTypeController::class, 'index'])->name('applyformembership');
Route::match(['get', 'post'], '/signup/{id}', [MembershipTypeController::class,'SignUp']);
Route::match(['get', 'post'], '/register-now', [MembershipTypeController::class,'registerNow'])->name('register-now');

Route::match(['get', 'post'], '/membershipFend/update/{id}', [MembershipTypeController::class,'MembershipFendUpdate']);

Route::get('/subscription-payment', [MembershipTypeController::class,'paySubscription'])->name('subscrption.payment');

Route::get('/email/verify', [VerificationController::class,'show'])->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', [VerificationController::class,'verify'])->name('verification.verify')->middleware(['signed']);
Route::post('/email/resend', [VerificationController::class,'resend'])->name('verification.resend');

// Route::get('/email/verify', function () {
//     return view('auth.verify-email');
// })->name('verification.notice');
// Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
//     $request->fulfill();
//
//     return redirect('/home');
// })->middleware(['auth', 'signed'])->name('verification.verify');

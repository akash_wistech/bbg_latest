<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\ModuleSubscription\Http\Controllers\SubscriptionController;


Route::group(['middleware' => ['web','auth']], function () {
	include('membershipTypes.php');
	include('subscriptions.php');

});

Route::group(['middleware' => ['web']], function () {
	include('MembershipTypesFrontendRoutes.php');
});

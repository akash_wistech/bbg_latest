<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\ModuleSubscription\Http\Controllers\MembershipTypeController;

Route::get('/membership-type', [NaeemAwan\ModuleSubscription\Http\Controllers\MembershipTypeController::class, 'index'])->name('membership_types');
Route::match(['post','get'],'/membership-type/create', [NaeemAwan\ModuleSubscription\Http\Controllers\MembershipTypeController::class, 'create'])->name('membership_types.create');
Route::match(['post','get'],'/membership-type/update/{id}', [NaeemAwan\ModuleSubscription\Http\Controllers\MembershipTypeController::class, 'update'])->name('membership_types.update.{id}');
Route::post('/membership-type/delete/{id}', [NaeemAwan\ModuleSubscription\Http\Controllers\MembershipTypeController::class, 'delete'])->name('membership_types.destroy.{id}');


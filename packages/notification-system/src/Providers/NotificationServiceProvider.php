<?php

namespace Wisdom\Notification\Providers;

use Illuminate\Support\ServiceProvider;
use Wisdom\Notification\Models\NotificationTemplate;
use Wisdom\Notification\Observers\NotificationTemplateObserver;
use Wisdom\Notification\Providers\EventServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
  public function boot()
  {
    NotificationTemplate::observe(NotificationTemplateObserver::class);
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'notification');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'notification');
    $this->publishes([
      __DIR__.'/../../config/notification.php' => config_path('notification.php'),
    ],'notification-config');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/notification'),
    ],'notification-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'notification-migrations');
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
    $this->mergeConfigFrom(
      __DIR__.'/../../config/notification.php',
      'notification'
    );
    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
    $loader->alias('NotificationListHelper', 'Wisdom\Notification\Helpers\NotificationListHelper');
    $this->app->register(EventServiceProvider::class);
  }
}

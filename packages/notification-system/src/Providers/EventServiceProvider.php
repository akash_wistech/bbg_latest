<?php
namespace Wisdom\Notification\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Event;
use Mail;
use Log;
use App\Models\User;
use Wisdom\Notification\Models\NotificationTrigger;
use Wisdom\Notification\Models\NotificationTemplate;
use Wisdom\Notification\Models\NotificationTemplateDetail;
use Wisdom\Notification\Mail\NotificationMail;
use NaeemAwan\StaffManager\Models\ModuleManager;
// use Wisdom\Notification\Notifications\SystemNotification;
// use Notification;
// use Wisdom\Notification\Events\NotificationEvents;
// use Wisdom\Notification\Listeners\UpdatePostTitle;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen('notify.*', function ($eventName, array $data) {
          // Log::debug($eventName);
          // Log::debug($data);
          $eventId = str_replace("notify.","",$eventName);
          $dbTriggers = NotificationTrigger::where('event_id',$eventId)->get();

          $replacements = isset($data['replacements']) ? $data['replacements'] : null;
          $attachments = isset($data['attachments']) ? $data['attachments'] : null;
          // Log::debug($eventId);
          if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
              $userTypes = json_decode($dbTrigger['user_type']);
              $template = NotificationTemplate::find($dbTrigger['template_id']);
              $notificationTypes = json_decode($dbTrigger['notification_triggers']);

              if($userTypes!=null){
                foreach($userTypes as $key=>$val){
                  if($val==1){
                    //Admin
                    $users = [];
                    $adminIdz[1] = 1;
                    $moduleManagers = ModuleManager::select('staff_id')->where('module_type',$data['manager_info']['module_type'])->where('module_id',$data['manager_info']['module_id'])->get();
                    if($moduleManagers!=null){
                      foreach($moduleManagers as $moduleManager){
                        $adminIdz[$moduleManager->staff_id]=$moduleManager->staff_id;
                      }
                    }
                    $staffUsers = User::whereIn('id',$adminIdz)->get();
                    if($staffUsers!=null){
                      foreach ($staffUsers as $staffUser) {
                        $users[]=['name'=>$staffUser->name,'email'=>$staffUser->email];
                      }
                    }
                  }else{
                    $toUser = $data['user'];
                  }


                  if($notificationTypes!=null){
                    foreach($notificationTypes as $ntKey=>$ntVal){

                      //Email
                      if($ntVal=='email'){
                        $emailBody = $template->lang->email_body;
                        $emailSubject = $template->lang->subject;
                        if($replacements!=null){
                          foreach($replacements as $rKey=>$rVal){
                            $emailBody = str_replace($rKey,$rVal,$emailBody);
                            $emailSubject = str_replace($rKey,$rVal,$emailSubject);
                          }
                        }

                        if($users!=null){
                          foreach($users as $user){
                            $emailBody = str_replace("{staffName}",$user['name'],$emailBody);
                            $emailSubject = str_replace("{staffName}",$user['name'],$emailSubject);

                            $mail = Mail::to($user['email'])->send(new NotificationMail($data,$emailSubject,$emailBody,$attachments));
                          }
                        }

                      }
                      //Sms
                      // if($ntVal=='sms'){
                      //   $smsBody = $template->lang->sms_body;
                      //   if($replacements!=null){
                      //     foreach($replacements as $rKey=>$rVal){
                      //       $smsBody = str_replace($rKey,$rVal,$smsBody);
                      //     }
                      //   }
                      //   $smsBody = str_replace("{name}",$toUser->name,$smsBody);
                      // }
                      //Push
                      // if($ntVal=='push'){
                      //   $pushBody = $template->lang->push_body;
                      //   if($replacements!=null){
                      //     foreach($replacements as $rKey=>$rVal){
                      //       $pushBody = str_replace($rKey,$rVal,$pushBody);
                      //     }
                      //   }
                      //   $pushBody = str_replace("{name}",$toUser->name,$pushBody);
                      // }
                    }
                  }
                }
              }
            }
          }
        });
        parent::boot();
    }
}

<?php

namespace Wisdom\Notification\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationTemplate extends Model
{
  public $subject,$email_body,$push_body,$sms_body;

  use Blameable, SoftDeletes;

  protected $table = 'notification_templates';

  protected $dates = ['created_at','updated_at','deleted_at'];

  protected $fillable = [
    'keyword',
    'subject',
    'email_body',
    'push_body',
    'sms_body',
  ];

  /**
  * locale condition
  */
  public function scopeLocale($query,$lang='en')
  {
    $pdl = (new NotificationTemplateDetail)->getTable();
    return $query->join($pdl,$pdl.'.notification_template_id','=',$this->getTable().'.id')
    ->where('lang', $lang);
  }

  /**
  * Get language details.
  */
  public function langDetails()
  {
    return $this->hasMany(NotificationTemplateDetail::class,'notification_template_id');
  }

  /**
  * Get single language detail.
  */
  public function lang()
  {
    $locale = App::currentLocale();
    return $this->hasOne(NotificationTemplateDetail::class,'notification_template_id')->where('lang',$locale);
  }

  /**
  * Get single language detail.
  */
  public function forLang($locale)
  {
    return NotificationTemplateDetail::where('notification_template_id',$this->id)->where('lang',$locale)->first();
  }

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }
}

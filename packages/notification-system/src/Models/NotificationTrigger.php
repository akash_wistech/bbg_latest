<?php

namespace Wisdom\Notification\Models;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use NotificationListHelper;

class NotificationTrigger extends Model
{
  use Blameable, SoftDeletes;

  protected $table = 'notification_triggers';

  protected $dates = ['created_at','updated_at','deleted_at'];

  protected $fillable = [
    'event_id',
    'user_type',
    'template_id',
    'notification_triggers',
    'status',
  ];

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }

  /**
  * Get User types.
  */
  public function getUserTypeLabels()
  {
    $html='';
    if($this->user_type!=null){
      $systemUserTypes = NotificationListHelper::getSystemUserTypes();
      $types=json_decode($this->user_type);
      foreach($types as $key=>$val){
        $html.=$html!='' ? ' ' : '';
        $html.='<span class="label label-sm label-primary label-inline font-weight-bold">'.$systemUserTypes[$val].'</span>';
      }
    }
    return $html;
  }

  /**
  * Get notifications services.
  */
  public function getNotificationTypeLabels()
  {
    $html='';
    if($this->notification_triggers!=null){
      $systemNotificationTypes = NotificationListHelper::getNotificationTypes();
      $types=json_decode($this->notification_triggers);
      foreach($types as $key=>$val){
        $html.=$html!='' ? ' ' : '';
        $html.='<span class="label label-sm label-info label-inline font-weight-bold">'.$systemNotificationTypes[$val].'</span>';
      }
    }
    return $html;
  }
}

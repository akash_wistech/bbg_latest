<?php

namespace Wisdom\Notification\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationTemplateDetail extends Model
{
  protected $table = 'notification_templates_detail';

  public $timestamps = false;

  protected $fillable = [
    'notification_template_id',
    'lang',
    'subject',
    'email_body',
    'push_body',
    'sms_body',
  ];

  /**
  * Get Main row.
  */
  public function mainRow()
  {
    return $this->belongsTo(NotificationTemplate::class,'notification_template_id');
  }

}

<?php
// 'src/Mail/WelcomeMail.php'

namespace Wisdom\Notification\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Log;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $subject;
    public $email_body;
    public $attFiles=null;

    public function __construct($data,$subject,$email_body,$attFiles=null)
    {
        $this->data = $data;
        $this->subject = $subject;
        $this->email_body = $email_body;
        if($attFiles!=null){
          $this->attFiles = $attFiles;
        }
    }

    public function build()
    {
      $mail = $this->subject($this->subject)
      ->view('notification::emails.general',['body'=>$this->email_body])
      ->text('notification::emails.general_text',['body'=>$this->email_body]);
      // Log::debug($this->attFiles);
      if($this->attFiles!=null){
        foreach($this->attFiles as $attFile){
          $mail->attach($attFile);
        }
      }

      return $mail;
    }
}

<?php

namespace Wisdom\Notification\Http\Controllers;

use Wisdom\Notification\Models\NotificationTemplate;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Wisdom\Notification\Http\Requests\NotificationTemplateRequest;
use Illuminate\Support\Str;

class NotificationTemplateController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $results=NotificationTemplate::get();
    return view('notification::notification-template.index', compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $model = new NotificationTemplate;
    return view('notification::notification-template.create',compact('model'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(NotificationTemplateRequest $request)
  {
    $data= new NotificationTemplate();
    $data->keyword=$request->keyword;
    $data->subject=$request->subject;
    $data->email_body=$request->email_body ?? '';
    $data->sms_body=$request->sms_body ?? '';
    $data->push_body=$request->push_body ?? '';
    if ($data->save()) {
      if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('notification::notification.saved'),]]);
      }else{
        Toastr::success(__('notification::notification.saved'), __('common.success'));
        return redirect('notification-template');
      }
    }else{
      if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('notification::notification.notsaved'),]]);
      }else{
        Toastr::success(__('notification::notification.notsaved'), __('common.error'));
        return redirect('notification-template');
      }
    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\NotificationTemplate  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, $id)
  {
    $model = NotificationTemplate::where('id', $id)->first();
    $langDetails = $model->langDetails;
    if($langDetails!=null){
      foreach($langDetails as $langDetail){
        $model->subject[$langDetail->lang] = $langDetail->subject;
        $model->email_body[$langDetail->lang] = $langDetail->email_body;
        $model->sms_body[$langDetail->lang] = $langDetail->sms_body;
        $model->push_body[$langDetail->lang] = $langDetail->push_body;
      }
    }
    return view('notification::notification-template.edit', [
      'model'=>$model,
    ]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\NotificationTemplate  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function update(NotificationTemplateRequest $request)
  {
    $data= NotificationTemplate::find($request->id);
    $data->keyword=$request->keyword;
    if(isset($request->subject))$data->subject=$request->subject;
    if(isset($request->email_body))$data->email_body=$request->email_body;
    if(isset($request->sms_body))$data->sms_body=$request->sms_body;
    if(isset($request->push_body))$data->push_body=$request->push_body;
    $data->updated_at = date("Y-m-d H:i:s");
    if ($data->update()) {
      if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('notification::notification.saved'),]]);
      }else{
        Toastr::success(__('notification::notification.saved'), __('common.success'));
        return redirect('notification-template');
      }
    }else{
      if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('notification::notification.notsaved'),]]);
      }else{
        Toastr::success(__('notification::notification.notsaved'), __('common.error'));
        return redirect('notification-template');
      }
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\NotificationTemplate  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $model= NotificationTemplate::where('id',$id)->first();
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('notification::notification.deleted'),]]);
  }
}

<?php

namespace Wisdom\Notification\Http\Controllers;

use App\Models\Setting;
use Wisdom\Notification\Models\NotificationTrigger;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Wisdom\Notification\Http\Requests\NotificationSettingsForm;
use Validator;
use Session;
use App;
use Auth;
use NotificationListHelper;

class SystemController extends Controller
{
  /**
  * System Settings
  *
  * @return \Illuminate\Http\Response
  */
  public function notificationSettings(Request $request)
  {
    $model = new NotificationSettingsForm;

    $settings=Setting::get();
    foreach($settings as $record){
      $colName = $record['config_name'];
      $model->$colName=$record['config_value'];
    }
    return view('notification::system.notification',['model'=>$model,'tab'=>'notification']);
  }

  /**
  * Update notification setting
  *
  * @param  \App\Models\Setting
  * @return \Illuminate\Http\Response
  */
  public function saveNotification(Request $request, $id)
  {
    $model = Setting::where('config_name', $id)->first();
    if($model!=null && $model->config_value==0){
      $val=1;
    }else{
      $val=0;
    }
    if($model!=null){
      DB::table($model->getTable())
      ->where('config_name', $id)
      ->update(['config_value' => $val]);
    }else{
      DB::table((new Setting)->getTable())->insert([
        'config_name'=>$id,'config_value'=>$val
      ]);
    }

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.statusUpdated'),]]);
  }
}

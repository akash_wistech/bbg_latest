<?php

namespace Wisdom\Notification\Http\Controllers;

use Wisdom\Notification\Models\NotificationTrigger;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Wisdom\Notification\Http\Requests\NotificationTriggerRequest;
use Illuminate\Support\Str;

class NotificationTriggerController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $results=NotificationTrigger::get();
    return view('notification::notification-trigger.index', compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $model = new NotificationTrigger;
    return view('notification::notification-trigger.create',compact('model'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(NotificationTriggerRequest $request)
  {
    $model= new NotificationTrigger();
    $model->event_id=$request->event_id;
    $model->user_type=$request->user_type ? json_encode($request->user_type) : '';
    $model->template_id=$request->template_id ?? '';
    $model->notification_triggers=$request->notification_triggers ? json_encode($request->notification_triggers) : '';
    $model->status=1;
    if ($model->save()) {
      if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('notification::notification_trigger.saved'),]]);
      }else{
        Toastr::success(__('notification::notification_trigger.saved'), __('common.success'));
        return redirect('notification-trigger');
      }
    }else{
      if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('notification::notification_trigger.notsaved'),]]);
      }else{
        Toastr::success(__('notification::notification_trigger.notsaved'), __('common.error'));
        return redirect('notification-trigger');
      }
    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\NotificationTrigger  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, $id)
  {
    $model = NotificationTrigger::where('id', $id)->first();
    $model->user_type = json_decode($model->user_type);
    $model->notification_triggers = json_decode($model->notification_triggers);
    return view('notification::notification-trigger.edit', [
      'model'=>$model,
    ]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\NotificationTrigger  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function update(NotificationTriggerRequest $request)
  {
    $model= NotificationTrigger::find($request->id);
    $model->event_id=$request->event_id;
    $model->user_type=$request->user_type ? json_encode($request->user_type) : '';
    $model->template_id=$request->template_id ?? '';
    $model->notification_triggers=$request->notification_triggers ? json_encode($request->notification_triggers) : '';
    $model->updated_at = date("Y-m-d H:i:s");
    if ($model->update()) {
      if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('notification::notification_trigger.saved'),]]);
      }else{
        Toastr::success(__('notification::notification_trigger.saved'), __('common.success'));
        return redirect('notification-trigger');
      }
    }else{
      if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('notification::notification_trigger.notsaved'),]]);
      }else{
        Toastr::success(__('notification::notification_trigger.notsaved'), __('common.error'));
        return redirect('notification-trigger');
      }
    }
  }

  /**
  * Update status of model
  *
  * @param  \App\Models\ContentPage  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function status(Request $request, $id)
  {
    $model = NotificationTrigger::where('id', $id)->first();
    if($model->status==0){
      $status=1;
    }else{
      $status=0;
    }
    DB::table($model->getTable())
    ->where('id', $model->id)
    ->update(['status' => $status]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.statusUpdated'),]]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Models\NotificationTrigger  $contentPage
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $model= NotificationTrigger::where('id',$id)->first();
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('notification::notification_trigger.deleted'),]]);
  }
}

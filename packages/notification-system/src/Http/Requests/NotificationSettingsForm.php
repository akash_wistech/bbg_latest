<?php

namespace Wisdom\Notification\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;
use App\Models\Setting;

class NotificationSettingsForm
{
  public $enable_email_notification,$enable_sms_notification,$enable_inapp_notification;
  /**
  * save the settings data.
  */
  public function save()
  {
    $post=Request::post();
    foreach($post as $key=>$val){
      if($key!='_token'){
        $setting=Setting::where('config_name',$key)->first();
        if($setting==null){
          $setting=new Setting;
          $setting->config_name=$key;
        }
        // if($key=='feedback_staff'){
        //   $val=implode(",",$this->feedback_staff);
        // }
        $setting->config_value=$val;
        $setting->save();
      }
    }
    return true;
  }
}

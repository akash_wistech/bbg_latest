<?php

namespace Wisdom\Notification\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationTriggerRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      'event_id'=>'required|string',
      'user_type'=>'required',
      'template_id'=>'required|integer',
      'notification_type'=>'required',
    ];
  }
}

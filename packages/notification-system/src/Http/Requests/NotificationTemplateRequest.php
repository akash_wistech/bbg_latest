<?php

namespace Wisdom\Notification\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationTemplateRequest extends FormRequest
{
  /**
  * Determine if the user is authorized to make this request.
  *
  * @return bool
  */
  public function authorize()
  {
    return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    return [
      "keyword"=>"required|string",
      "subject"=>"array",
      "subject.*"=>"required|string",
      "email_body"=>"array",
      "email_body.*"=>"required|string",
      "push_body"=>"array",
      "push_body.*"=>"required|string",
      "sms_body"=>"array",
      "sms_body.*"=>"required|string",
    ];
  }
}

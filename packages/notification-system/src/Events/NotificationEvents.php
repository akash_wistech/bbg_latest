<?php

namespace Wisdom\Notification\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
// use JohnDoe\BlogPackage\Models\Post;

class NotificationEvents
{
    use Dispatchable, SerializesModels;

    public $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }
}

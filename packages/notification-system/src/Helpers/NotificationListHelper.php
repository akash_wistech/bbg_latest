<?php
namespace Wisdom\Notification\Helpers;

use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Wisdom\Notification\Models\Setting;
use Wisdom\Notification\Models\NotificationTemplate;
use DB;

/**
* This is a helper class for notification lists
*/
class NotificationListHelper
{
  /**
  * return all languages
  */
  public static function getSystemLanguages()
  {
    return [
      'en'=>'English',
      // 'ar'=>'Arabic',
    ];
  }

  /**
  * return all rtl languages
  */
  public static function getSystemRtlLanguages()
  {
    return [
      'ar',
    ];
  }

  /**
  * return all user types
  */
  public static function getSystemUserTypes()
  {
    return [
      1=>'Admin',
      2=>'Prospect',
      3=>'Contact',
    ];
  }

  /**
  * return user types with number options
  */
  public static function getSystemUserTypesWithNumbers()
  {
    return [0];
  }

  /**
  * return events
  */
  public static function getSystemNotificationEvents()
  {
    return [
      'Dynamic Forms'=>[
        'dynamic-form.submitted'=>'Submitted',
      ],
    ];
  }

  /**
  * return events titles
  */
  public static function getSystemNotificationEventLabels()
  {
    return [
      'dynamic-form.submitted'=>'Dynamic Form Submitted',
    ];
  }

  /**
  * return notification services
  */
  public static function getNotificationTypes()
  {
    $allowedServices=[];
    $settings=Setting::whereIn('config_name',['enable_email_notification','enable_sms_notification','enable_inapp_notification'])->get();
    foreach($settings as $record){
      if($record['config_name']=='enable_email_notification' && $record['config_value']==1)$allowedServices['email']='Email';
      if($record['config_name']=='enable_sms_notification' && $record['config_value']==1)$allowedServices['sms']='SMS';
      if($record['config_name']=='enable_inapp_notification' && $record['config_value']==1)$allowedServices['push']='Push Notification';
    }
    return $allowedServices;
  }
  /**
  * return events
  */
  public static function getNotificationTemplates()
  {
    return NotificationTemplate::select((new NotificationTemplate)->getTable().'.id',DB::raw("CONCAT(subject,' (',keyword,')') AS tmpsubject"))->locale()->get()->pluck('tmpsubject', 'id')->toArray();
  }

  /**
  * return front user types
  */
  public static function getSysUserId()
  {
    return [2,3];
  }
}

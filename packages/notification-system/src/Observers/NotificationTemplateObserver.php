<?php

namespace Wisdom\Notification\Observers;

use NotificationListHelper;
use Wisdom\Notification\Models\NotificationTemplate;
use Wisdom\Notification\Models\NotificationTemplateDetail;

class NotificationTemplateObserver
{
  /**
  * Handle the NotificationTemplate "created" event.
  *
  * @param  \App\Models\NotificationTemplate  $notificationTemplate
  * @return void
  */
  public function created(NotificationTemplate $notificationTemplate)
  {
    if ($notificationTemplate->id) {
      $sysLangs = NotificationListHelper::getSystemLanguages();
      foreach ($sysLangs as $key=>$val) {
        if(isset($notificationTemplate->subject[$key])){
          $subject = $notificationTemplate->subject[$key];
          $email_body = isset($notificationTemplate->email_body[$key]) ? $notificationTemplate->email_body[$key] : '';
          $push_body = isset($notificationTemplate->push_body[$key]) ? $notificationTemplate->push_body[$key] : '';
          $sms_body = isset($notificationTemplate->sms_body[$key]) ? $notificationTemplate->sms_body[$key] : '';

          $langDetail = NotificationTemplateDetail::where('notification_template_id',$notificationTemplate->id)->where('lang',$key)->first();
          if($langDetail==null){
            $langDetail = new NotificationTemplateDetail();
            $langDetail->notification_template_id = $notificationTemplate->id;
            $langDetail->lang = $key;
          }
          $langDetail->subject = $subject;
          $langDetail->email_body = $email_body;
          $langDetail->push_body = $push_body;
          $langDetail->sms_body = $sms_body;
          $langDetail->save();
        }
      }
    }
  }

  /**
  * Handle the NotificationTemplate "updated" event.
  *
  * @param  \App\Models\NotificationTemplate  $notificationTemplate
  * @return void
  */
  public function updated(NotificationTemplate $notificationTemplate)
  {
    $sysLangs = NotificationListHelper::getSystemLanguages();
    foreach ($sysLangs as $key=>$val) {
      if(isset($notificationTemplate->subject[$key])){
        $subject = $notificationTemplate->subject[$key];
        $email_body = isset($notificationTemplate->email_body[$key]) ? $notificationTemplate->email_body[$key] : '';
        $push_body = isset($notificationTemplate->push_body[$key]) ? $notificationTemplate->push_body[$key] : '';
        $sms_body = isset($notificationTemplate->sms_body[$key]) ? $notificationTemplate->sms_body[$key] : '';

        $langDetail = NotificationTemplateDetail::where('notification_template_id',$notificationTemplate->id)->where('lang',$key)->first();
        if($langDetail==null){
          $langDetail = new NotificationTemplateDetail();
          $langDetail->notification_template_id = $notificationTemplate->id;
          $langDetail->lang = $key;
        }
        $langDetail->subject = $subject;
        $langDetail->email_body = $email_body;
        $langDetail->push_body = $push_body;
        $langDetail->sms_body = $sms_body;
        $langDetail->save();
      }
    }
  }

  /**
  * Handle the NotificationTemplate "deleted" event.
  *
  * @param  \App\Models\NotificationTemplate  $notificationTemplate
  * @return void
  */
  public function deleted(NotificationTemplate $notificationTemplate)
  {
    //
  }

  /**
  * Handle the NotificationTemplate "restored" event.
  *
  * @param  \App\Models\NotificationTemplate  $notificationTemplate
  * @return void
  */
  public function restored(NotificationTemplate $notificationTemplate)
  {
    //
  }

  /**
  * Handle the NotificationTemplate "force deleted" event.
  *
  * @param  \App\Models\NotificationTemplate  $notificationTemplate
  * @return void
  */
  public function forceDeleted(NotificationTemplate $notificationTemplate)
  {
    //
  }
}

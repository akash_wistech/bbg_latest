<?php
return [
  'heading' => 'Notification Triggers',
  'triggers' => 'Triggers',
  'saved' => 'Notification trigger saved succefully',
  'notsaved' => 'Error while saving notification trigger',
  'deleted' => 'Record deleted successfully',
  'model_class' => 'Origin',
  'event_id' => 'Event',
  'user_type' => 'User',
  'template_id' => 'Template',
  'notification_type' => 'Notification Type',
  'email_template_id' => 'Email Template',
  'sms_template_id' => 'SMS Template',
  'push_template_id' => 'Push Template',
];

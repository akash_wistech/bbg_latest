@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/notification-trigger/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('notification::notification_trigger.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('notification::notification.heading'),
__('notification::notification_trigger.triggers')
]])
@endsection
@php
$eventLabels = NotificationListHelper::getSystemNotificationEventLabels();
$notificationTemplates=NotificationListHelper::getNotificationTemplates();
@endphp

@section('content')
<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card card-custom">
      <div id="grid-table" class="card card-body">
        <div class="overlay-wrapper">
          <table class="table">
            <thead>
              <tr>
                <th width="50">#</th>
                <th>{{__('notification::notification_trigger.event_id')}}</th>
                <th>{{__('notification::notification_trigger.user_type')}}</th>
                <th>{{__('notification::notification_trigger.template_id')}}</th>
                <th>{{__('notification::notification_trigger.notification_type')}}</th>
                <th width="100">{{__('common.status')}}</th>
                <th width="100">{{__('common.action')}}</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; ?>
              @foreach($results as $result)
              @php
              if($result->status==1){
                $confrmMsg=__('app.general.confirmDisable');
              }else{
                $confrmMsg=__('app.general.confirmEnable');
              }
              @endphp
              <tr>
                <td>{{$i}}</td>
                <td>{{$eventLabels[$result->event_id]}}</td>
                <td>{!!$result->getUserTypeLabels()!!}</td>
                <td>{{$notificationTemplates[$result->template_id]}}</td>
                <td>{!!$result->getNotificationTypeLabels()!!}</td>
                <td>{!!getEnableDisableIconWithLink($result->status,'notification-trigger/status/'.$result->id,$confrmMsg,'grid-table')!!}</td>
                <td>
                  <a href="{{ url('notification-trigger/edit/'.$result->id)}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}" data-id="{{$result->id}}">
                    <i class="text-dark-50 flaticon-edit"></i>
                  </a>
                  <a href="{{ url('notification-trigger/delete/'.$result->id)}}" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="{{__('common.delete')}}" data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                  </a>
                </td>
              </tr>
              <?php $i++; ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@extends('layouts.app')
@section('title', __('notification::notification_trigger.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'notification-trigger'=>__('notification::notification_trigger.heading'),
  __('common.create')
]])
@endsection
@section('content')
<div class="d-flex flex-column-fluid">
  <div class="container">
    {!! Form::model($model, ['method' => 'POST', 'route' => ['notification-trigger.store'], 'id' => 'form']) !!}
    @include('notification::notification-trigger._form')
    {!! Form::close() !!}
  </div>
</div>

@endsection

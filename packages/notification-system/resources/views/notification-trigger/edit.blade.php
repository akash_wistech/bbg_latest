@extends('layouts.app')
@section('title', __('notification::notification_trigger.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'notification-trigger'=>__('notification::notification_trigger.heading'),
__('common.update')
]])
@endsection

@section('content')
<div class="container">
  {!! Form::model($model, ['method' => 'POST', 'route' => ['notification-trigger.update',$model->id], 'id' => 'form']) !!}
  @include('notification::notification-trigger._form')
  {!! Form::close() !!}
</div>
@endsection

@php
$notificationEvents=NotificationListHelper::getSystemNotificationEvents();
$userTypes=NotificationListHelper::getSystemUserTypes();
$templates=NotificationListHelper::getNotificationTemplates();
$notificationServices=NotificationListHelper::getNotificationTypes();
@endphp
<div class="card card-custom tabs-card mb-2">
  <div class="card-body">
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('event_id', __('notification::notification_trigger.event_id')) !!}
          {!! Form::select('event_id', $notificationEvents, null, ['class' => 'form-control selectpicker show-tick', 'data-live-search'=>'true', 'title'=>__('notification::notification.select')]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('user_type[]', __('notification::notification_trigger.user_type')) !!}
          {!! Form::select('user_type[]', $userTypes, null, ['class' => 'form-control selectpicker show-tick', 'multiple'=>'multiple', 'title'=>__('notification::notification.select')]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('template_id', __('notification::notification_trigger.template_id')) !!}
          {!! Form::select('template_id', $templates, null, ['class' => 'form-control selectpicker show-tick', 'data-live-search'=>'true', 'title'=>__('notification::notification.select')]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('notification_triggers[]', __('notification::notification_trigger.notification_type')) !!}
          {!! Form::select('notification_triggers[]', $notificationServices, null, ['class' => 'form-control selectpicker show-tick', 'multiple'=>'multiple', 'title'=>__('notification::notification.select')]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    {!! Form::submit(trans('common.save'), ['class' => 'btn btn-success']) !!}
  </div>
</div>

@push('jsScripts')
  $('select.selectpicker').selectpicker();
@endpush

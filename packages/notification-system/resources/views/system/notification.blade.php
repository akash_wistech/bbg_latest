@extends('layouts.app')
@section('title', __('notification::notification.notification_heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  __('app.settings.heading'),
  __('notification::notification.notification_heading'),
]])
@endsection

@section('content')
<div class="card card-custom">
  {!! Form::model($model, ['method' => 'POST', 'route' => ['system.settings.notification'], 'id' => 'form']) !!}
  <div class="card-body">

    <div class="form-group row">
      <div class="col-6">
      </div>
      <div class="col-2">
        {{__('notification::notification.email')}}
      </div>
      <div class="col-2">
        {{__('notification::notification.sms')}}
      </div>
      <div class="col-2">
        {{__('notification::notification.push_notification')}}
      </div>
    </div>
    <div class="form-group row">
      <div class="col-6">
        {{__('notification::notification.enable_notifications')}}
      </div>
      <div class="col-2">
        <span class="switch switch-outline switch-icon switch-success">
          <label>
            <input type="checkbox" class="notifyCb" value="1"{!!$model->enable_email_notification==1 ? ' checked="checked"' : ''!!} name="enable_email_notification"/>
            <span></span>
          </label>
        </span>
      </div>
      <div class="col-2">
        <span class="switch switch-outline switch-icon switch-success">
          <label>
            <input type="checkbox" class="notifyCb" value="1"{!!$model->enable_sms_notification==1 ? ' checked="checked"' : ''!!} name="enable_sms_notification"/>
            <span></span>
          </label>
        </span>
      </div>
      <div class="col-2">
        <span class="switch switch-outline switch-icon switch-success">
          <label>
            <input type="checkbox" class="notifyCb" value="1"{!!$model->enable_inapp_notification==1 ? ' checked="checked"' : ''!!} name="enable_inapp_notification"/>
            <span></span>
          </label>
        </span>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
$("body").on("change", ".notifyCb", function (e) {
  val=0;
  if($(this).is(":checked")){
    val=1;
  }else{
    val=0;
  }
  fld=$(this).attr("name");
  console.log(fld+" is "+val);
  url = "{{url('/system/save-notification')}}/"+fld;
  makeSilentAjaxRequest(url,"settings-card")
});
</script>
@endpush

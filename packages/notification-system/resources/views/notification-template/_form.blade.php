@php
$sysLanguages=NotificationListHelper::getSystemLanguages();
$rtlLangs=NotificationListHelper::getSystemRtlLanguages();
$colWidth=6;
$langCount = count($sysLanguages);
if($langCount==1)$colWidth=12;
@endphp
<div class="card card-custom tabs-card mb-2">
  <div class="card-body">
    <div class="form-group">
      {!! Form::label('keyword', __('notification::notification.keyword') . '*') !!}
      {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => trans('notification::notification.keyword_hint'), 'required']) !!}
      <div class="help-block with-errors"></div>
    </div>
    <div class="row">
      @foreach($sysLanguages as $key=>$val)
      <div class="col-12 col-sm-{{$colWidth}}">
        <div class="form-group">
          {!! Form::label('subject['.$key.']', __('notification::notification.subject_in',['lang'=>$val]) . '*') !!}
          {!! Form::text('subject['.$key.']', null, ['class' => 'form-control'.(in_array($key,$rtlLangs) ? ' text-rtl' : ''), 'placeholder' => trans('notification::notification.subject_in_hint',['lang'=>$val]), 'required']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      @endforeach
    </div>
    <h3>Emails</h3>
    <div class="row">
      @foreach($sysLanguages as $key=>$val)
      <div class="col-12 col-sm-{{$colWidth}}">
        <div class="form-group">
          <!--{!! Form::label('email_body['.$key.']', __('notification::notification.email_body_in',['lang'=>$val])) !!}-->
          <div class="tinymce">
            {!! Form::textarea('email_body['.$key.']', null, ['class' => 'form-control  tox-target'.(in_array($key,$rtlLangs) ? ' textEditor-rtl' : ' textEditor'), 'rows'=>4, 'placeholder' => trans('notification::notification.email_body_in_hint',['lang'=>$val])]) !!}
          </div>
          <div class="help-block with-errors"></div>
        </div>
      </div>
      @endforeach
    </div>
    @if(config('notification.enable_sms')==true)
    <h3>SMS</h3>
    <div class="row">
      @foreach($sysLanguages as $key=>$val)
      <div class="col-12 col-sm-{{$colWidth}}">
        <div class="form-group">
          <!--{!! Form::label('sms_body['.$key.']', __('notification::notification.sms_body_in',['lang'=>$val])) !!}-->
          <div class="tinymce">
            {!! Form::textarea('sms_body['.$key.']', null, ['class' => 'form-control'.(in_array($key,$rtlLangs) ? ' text-rtl' : ''), 'rows'=>6, 'placeholder' => trans('notification::notification.sms_body_in_hint',['lang'=>$val])]) !!}
          </div>
          <div class="help-block with-errors"></div>
        </div>
      </div>
      @endforeach
    </div>
    @endif
    @if(config('notification.enable_apppush')==true)
    <h3>Push Notification</h3>
    <div class="row">
      @foreach($sysLanguages as $key=>$val)
      <div class="col-12 col-sm-{{$colWidth}}">
        <div class="form-group">
          <!--{!! Form::label('push_body['.$key.']', __('notification::notification.push_body_in',['lang'=>$val])) !!}-->
          <div class="tinymce">
            {!! Form::textarea('push_body['.$key.']', null, ['class' => 'form-control'.(in_array($key,$rtlLangs) ? ' text-rtl' : ''), 'rows'=>6, 'placeholder' => trans('notification::notification.push_body_in_hint',['lang'=>$val])]) !!}
          </div>
          <div class="help-block with-errors"></div>
        </div>
      </div>
      @endforeach
    </div>
    @endif
  </div>
  <div class="card-footer">
    {!! Form::submit(trans('common.save'), ['class' => 'btn btn-success']) !!}
  </div>
</div>
@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
@endpush
@push('jsScripts')
tinymce.init({
    selector: '.textEditor',
    menubar: false,
    toolbar: ['styleselect fontselect fontsizeselect | link image',
        'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code',
        'advlist | autolink | lists'],
    plugins : 'advlist autolink link image lists charmap code'
});
tinymce.init({
    selector: '.textEditor-rtl',
    directionality : "rtl",
    menubar: false,
    toolbar: ['styleselect fontselect fontsizeselect | link image',
        'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code',
        'advlist | autolink | lists'],
    plugins : 'advlist autolink link image lists charmap code'
});
@endpush

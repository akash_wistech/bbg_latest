@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/notification-template/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('notification::notification.notification_templates'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('notification::notification.heading'),
__('notification::notification.templates')
]])
@endsection

@section('content')
<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card card-custom">
      <div id="grid-table" class="card card-body">
        <div class="overlay-wrapper">
          <table class="table">
            <thead>
              <tr>
                <th width="50">#</th>
                <th>{{__('notification::notification.subject')}}</th>
                <th width="100">{{__('common.action')}}</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; ?>
              @foreach($results as $result)
              <tr>
                <td>{{$i}}</td>
                <td>{{$result->lang->subject}}</td>
                <td>
                  <a href="{{ url('notification-template/edit/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon edit' data-toggle="tooltip" title='{{__('common.update')}}' data-id="{{$result->id}}">
                    <i class="text-dark-50 flaticon-edit"></i>
                  </a>
                  <a href="{{ url('notification-template/delete/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon act-confirmation' data-toggle="tooltip" title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                  </a>
                </td>
              </tr>
              <?php $i++; ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@extends('layouts.app')
@section('title', __('notification::notification.notification_templates'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('notification::notification.heading'),
'notification-template'=>__('notification::notification.templates'),
__('app.general.update')
]])
@endsection

@section('content')
<div class="container">
  {!! Form::model($model, ['method' => 'POST', 'route' => ['notification-template.update',$model->id], 'id' => 'form']) !!}
  @include('notification::notification-template._form')
  {!! Form::close() !!}
</div>
@endsection

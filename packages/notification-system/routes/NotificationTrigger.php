<?php
Route::get('/notification-trigger', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'index'])->name('notification-trigger');
Route::get('/notification-trigger/create', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'create'])->name('notification-trigger.create');
Route::post('/notification-trigger/store', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'store'])->name('notification-trigger.store');
Route::match(['post','get'],'/notification-trigger/status/{id}', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'status'])->name('notification-trigger.status.{id}');
Route::get('/notification-trigger/edit/{id}', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'edit'])->name('notification-trigger.edit.{id}');
Route::post('/notification-trigger/update/{id}', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'update'])->name('notification-trigger.update');
Route::post('/notification-trigger/delete/{id}', [Wisdom\Notification\Http\Controllers\NotificationTriggerController::class, 'destroy'])->name('notification-trigger.destroy.{id}');

<?php
Route::get('/notification-template', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'index'])->name('notification-template');
Route::get('/notification-template/create', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'create'])->name('notification-template.create');
Route::post('/notification-template/store', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'store'])->name('notification-template.store');
Route::match(['post','get'],'/notification-template/status/{id}', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'status'])->name('notification-template.status.{id}');
Route::get('/notification-template/edit/{id}', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'edit'])->name('notification-template.edit.{id}');
Route::post('/notification-template/update/{id}', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'update'])->name('notification-template.update');
Route::post('/notification-template/delete/{id}', [Wisdom\Notification\Http\Controllers\NotificationTemplateController::class, 'destroy'])->name('notification-template.destroy.{id}');

<?php

Route::group(['middleware' => ['web','auth']], function () {
  include('NotificationTemplate.php');
  include('NotificationTrigger.php');

  Route::get('/notification/settings', [Wisdom\Notification\Http\Controllers\SystemController::class, 'notificationSettings'])->name('system.settings.notification');
  Route::match(['post','get'],'/system/save-notification/{id}', [Wisdom\Notification\Http\Controllers\SystemController::class, 'saveNotification'])->name('system.save-settings.{id}');
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationTemplateDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_templates_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('notification_template_id')->nullable();
            $table->string('lang',10)->nullable();
            $table->string('subject',150)->nullable();
            $table->longText('email_body')->nullable();
            $table->text('push_body')->nullable();
            $table->text('sms_body')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_templates_detail');
    }
}

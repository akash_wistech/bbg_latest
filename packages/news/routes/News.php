<?php
use Illuminate\Support\Facades\Route;


use Wisdom\News\Http\Controllers\NewsController;



Route::get('news', [NewsController::class,'index']);
Route::match(['get', 'post'],'news/create', [NewsController::class,'create']);
Route::match(['get','post'],'news/update/{id}',[NewsController::class, 'update'])->name('news.update');
Route::match(['get','post'],'news/delete/{id}',[NewsController::class, 'destroy'])->name('news.delete');
Route::match(['get','post'],'news/view/{id}',[NewsController::class, 'view'])->name('news.view');
Route::match(['get','post'],'news/data-table-data',[NewsController::class, 'datatableData']);
Route::match(['get','post'],'news/getRelToMemCom',[NewsController::class, 'getRelToMemCom']);


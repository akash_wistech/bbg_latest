<?php

use Illuminate\Support\Facades\Route;
use Wisdom\News\Http\Controllers\NewsController;


Route::group(['middleware' => ['web','auth']], function () {
	include('News.php');
});




Route::group(['middleware' => ['web']], function () {
// FrontEnd Routes
	Route::get('news-fend', [NewsController::class, 'News'])->name('news-fend');
	Route::match(['get', 'post'], '/news-details/{id}', [NewsController::class,'NewsDetails']);

	Route::match(['get', 'post'], '/subscribe-newsletter', [NewsController::class,'SubscribeNewsletter']);
	Route::match(['get', 'post'], 'add-news', [NewsController::class,'AddNews']);


	
	Route::match(['get', 'post'], 'GetMyCompanyNews', [NewsController::class,'GetMyCompanyNews']);
	Route::match(['get', 'post'], 'MyNews/edit/{id}', [NewsController::class,'MyNewsEdit']);
	Route::match(['get', 'post'], 'MyNews/delete/{id}', [NewsController::class,'MyNewsDelete']);
});

<?php
return [
	'heading' => 'News',
	//form fields
	'title' => 'Title',
	'category' => 'Category',
	'link' => 'Link',
	'sort_order' => 'Sort Order',
	'isPublished' => 'publish',
	'homepage_show' => 'Show On HomePage',
	'related_to' => 'Retated Type',
	'rel_id' => 'Related To',
	'short_description' => 'Short Description',
	'description' => 'Description',
	're_post' => 'Re Post',
	'id' => 'Id',
	'news_type' => 'News Type',
	'image' => 'Main Image',
	'date' => 'Date',

	'meta_title' => 'Meta Title',
	'meta_description' => 'Meta Description',
	'slug' => 'Slug',
	'created' => 'Created',

	'saved' => 'News Saved Successfully',
	'updated' => 'News Updated Successfully',

];

@extends('layouts.app')

@section('title', __('news_lang::news-lang.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'news'=>__('news_lang::news-lang.heading'),
__('common.view')
]])
@endsection


@section('content')
<div class="card card-custom pb-0" data-card="true">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">Information</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','news'))
			{
				?>
				<a href="{{url('news/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			}	
			?>	
		</div>
	</div>
	<div class="card-body">
		<div class="row" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.created')}}:</strong>{{formatDateTime($model->created_at)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.news_type')}}:</strong>{{$model->news_type}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.homepage_show')}}:</strong>{{getNewsPublishArr()[$model->homepage_show]}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.related_to')}}:</strong>{{$model->related_to}}
				</div>
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.slug')}}:</strong>{{$model->slug}}
				</div>
			</div>
			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.title')}}: </strong>{{$model->title}}
				</div>
				<div class="col mt-3">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.link')}}:</strong>{{$model->link}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.sort_order')}}: </strong>{{$model->sort_order}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.rel_id')}}: </strong>{{$model->rel_id}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.meta_description')}}: </strong>{{$model->meta_description}}
				</div>
			</div>
			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.category')}}:</strong>{{$model->category}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.isPublished')}}:</strong>{{getNewsPublishArr()[$model->isPublished]}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.re_post')}}:</strong>{{getNewsPublishArr()[$model->re_post]}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.meta_title')}}: </strong>{!! $model->meta_title !!}
				</div>
				<div class="col-12">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.short_description')}}: </strong>{!! $model->short_description !!}
				</div>
			</div>
		</div>	


		<div class="row py-2" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.image')}}:</strong>{!!getImageTag($model->image)!!}
				</div>
			</div>
			<div class="col-8">			
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('news_lang::news-lang.description')}}: </strong>{!! $model->description !!}
				</div>
			</div>
		</div>



	</div>
</div>

@endsection
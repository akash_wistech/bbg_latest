@extends('layouts.app')

@section('title', __('news_lang::news-lang.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'news'=> __('news_lang::news-lang.heading'),
__('common.create')
]])
@endsection

@section('content')

@include('news::news._form')

@endsection


@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/wizard/wizard-3.css')}}">
@endpush

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/custom/wizard/wizard-3.js')}}"></script>

<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table',
		image_advtab: true ,
	});
</script>

<script>

	var rel_id = "{{ $model->rel_id }}";

	// console.log(rel_id);

	if (rel_id) {
		AB(rel_id);
	}
	

	$('body').on('change','#relatedType', function(){
		if ($(this).val()== 'general') {
			// console.log("helo general");
			$('#target-rel-id').html('');
			$('#target-rel-id').html('<option value="">Select</option>');
		}else{
			// console.log("hello world funxt run");
			AB();
		}
	});

	

	function AB(rel_id=null) {
		console.log("hellow world");
		console.log(rel_id);
		value = $("#relatedType").val();
		data = {data_key:value, rel_id:rel_id};

		$.ajax({
			url:"<?= url('news/getRelToMemCom')?>",
			type:"POST",
			data: {
				"_token": "<?= csrf_token() ?>",
				data: data,
			},
			success:function (data) {
				$('#target-rel-id').html(data);
				// console.log(data);
			}
		});
	}
</script>
@endpush






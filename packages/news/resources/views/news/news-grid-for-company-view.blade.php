<?php
$btnsList = [];
if(checkActionAllowed('create','news')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'news/create', 'method'=>'post'];
}

$dtColsArr = [];
$advSearchCols=[];

$dtColsArr[]=['title'=>__('news_lang::news-lang.image'),'data'=>'image','name'=>'image','orderable'=>false,'width'=>80];
$dtColsArr[]=['title'=>__('news_lang::news-lang.id'),'data'=>'id','name'=>'id','orderable'=>false,'width'=>5];
$dtColsArr[]=['title'=>__('news_lang::news-lang.title'),'data'=>'title','name'=>'title','orderable'=>false,'width'=>0];
$dtColsArr[]=['title'=>__('news_lang::news-lang.category'),'data'=>'category','name'=>'category','orderable'=>false,'width'=>40];
$dtColsArr[]=['title'=>__('news_lang::news-lang.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false,'width'=>200];
$dtColsArr[]=['title'=>__('news_lang::news-lang.description'),'data'=>'description','name'=>'description','orderable'=>false,'width'=>200];
$dtColsArr[]=['title'=>__('news_lang::news-lang.news_type'),'data'=>'news_type','name'=>'news_type','orderable'=>false,'width'=>95];
$dtColsArr[]=['title'=>__('news_lang::news-lang.link'),'data'=>'link','name'=>'link','orderable'=>false,'width'=>40];
$dtColsArr[]=['title'=>__('news_lang::news-lang.homepage_show'),'data'=>'homepage_show','name'=>'homepage_show','orderable'=>false,'width'=>2];
$dtColsArr[]=['title'=>__('news_lang::news-lang.sort_order'),'data'=>'sort_order','name'=>'sort_order','orderable'=>false,'width'=>90];
$dtColsArr[]=['title'=>__('news_lang::news-lang.related_to'),'data'=>'related_to','name'=>'related_to','orderable'=>false,'width'=>90];
$dtColsArr[]=['title'=>__('news_lang::news-lang.rel_id'),'data'=>'rel_id','name'=>'rel_id','orderable'=>false,'width'=>2];
$dtColsArr[]=['title'=>__('news_lang::news-lang.re_post'),'data'=>'re_post','name'=>'re_post','orderable'=>false,'width'=>90];
$dtColsArr[]=['title'=>__('news_lang::news-lang.isPublished'),'data'=>'isPublished','name'=>'isPublished','orderable'=>false,'width'=>1];

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

$moduleTypeId='';
?>


<div id="grid-table-news">
	<x-data-table-grid :sid="'news'" :request="$request"  :controller="'news'" :cbSelection="false" :exportMenu="true" :jsonUrl="'news/data-table-data?company_id='.$model->id" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
</div>


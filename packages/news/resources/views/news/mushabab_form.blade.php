
{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">

<div class=" mt-10 ml-10">
  <h3 >News</h3>
</div>
  


  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif

<div class="row px-0">

    <div class="col-md-4 col-lg-5 px-0">
      <div class="col-12">
        <div class="form-group">
          {{ Form::label('title',__('membership_lang::membership.title')) }}
          {{ Form::select('title',[''=>__('common.select')] + getMembershipTitle(), $model->title, ['class'=>'form-control']) }}
      
        </div>
      </div>
      <div class="col-12">
        <div class="form-group">
        {{ Form::label('last_name',__('membership_lang::membership.last_name')) }}
        {{ Form::text('last_name', $model->last_name, ['class'=>'form-control', 'placeholder'=>'last name']) }}
          </div>
        </div>
    </div> 


    <div class="col-md-4 col-lg-5  px-0 ">
      <div class="col-12">
        <div class="form-group">
         {{ Form::label('first_name',__('membership_lang::membership.first_name')) }}
         {{ Form::text('first_name', $model->first_name, ['class'=>'form-control', 'placeholder'=>'first name']) }}
      </div>
      </div>
      <div class="col-12">
        <div class="form-group">
          {{ Form::label('email',__('membership_lang::membership.email')) }}
          {{ Form::text('email', $model->email, ['class'=>'form-control', 'placeholder'=>'email']) }}
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column">
       <?php //if ($model->logo) { ?>
      <!-- <div class="image-input image-input-empty image-input-outline" id="kt_image_5" style="background-image: url('{{ asset("images/".$model->logo)  }}')"> -->
       <?php //}else{ ?>
      <div class="image-input image-input-empty image-input-outline" id="kt_image_5" style="background-image: url(assets/media/users/blank.png)">
     <?php //} ?> 

                        
       <div class="image-input-wrapper"></div>
       <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
        <i class="fa fa-pen icon-sm text-muted"></i>
        <input type="file" value="{{ $model->logo }}" name="logo" accept=".png, .jpg, .jpeg"/>
        <input type="hidden" name="profile_avatar_remove"/>
       </label>

       <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
        <i class="ki ki-bold-close icon-xs text-muted"></i>
       </span>

       <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
        <i class="ki ki-bold-close icon-xs text-muted"></i>
       </span>
      </div>
      <span class="label label-xl label-muted font-weight-bold label-inline mt-3">Company Logo</span>
    </div>



</div>
    

    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('secondry_email',__('membership_lang::membership.secondry_email')) }}
          {{ Form::text('secondry_email', $model->secondry_email, ['class'=>'form-control', 'placeholder'=>'secondary email']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('phone',__('membership_lang::membership.phone')) }}
          {{ Form::text('phone', $model->phone, ['class'=>'form-control', 'placeholder'=>'phone no']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('gender',__('membership_lang::membership.gender')) }}
          {{ Form::select('gender',[''=>__('common.select')] +  getGenderArr(), $model->gender, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>

    <div class="row">
      
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('designation',__('membership_lang::membership.designation')) }}
          {{ Form::text('designation', $model->designation, ['class'=>'form-control', 'placeholder'=>'designation']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('nationality',__('membership_lang::membership.nationality')) }}
          {{ Form::text('nationality', $model->nationality, ['class'=>'form-control', 'placeholder'=>'nationalality']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('country_code',__('membership_lang::membership.country_code')) }}
          {{ Form::select('country_code',[''=>__('common.select')] + getPredefinedListItemsArr(2), $model->country_code, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>

<div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('city',__('membership_lang::membership.city')) }}
          {{ Form::text('city', $model->city, ['class'=>'form-control', 'placeholder'=>'city']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('vat_number',__('membership_lang::membership.vat_number')) }}
          {{ Form::text('vat_number', $model->vat_number, ['class'=>'form-control', 'placeholder'=>'vat number']) }}
        </div>
      </div>
       <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('address',__('membership_lang::membership.address')) }}
          {{ Form::text('address', $model->address, ['class'=>'form-control', 'placeholder'=>'address']) }}
        </div>
      </div>
    </div>



    <div class="row">
     
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('linkedin',__('membership_lang::membership.linkedin')) }}
          {{ Form::text('linkedin', $model->linkedin, ['class'=>'form-control', 'placeholder'=>'linkedin']) }}
        </div>
      </div>
       <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('twitter',__('membership_lang::membership.twitter')) }}
          {{ Form::text('twitter', $model->twitter, ['class'=>'form-control', 'placeholder'=>'twitter']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('howDidYouHear',__('membership_lang::membership.howDidYouHear')) }}
             {{ Form::select('howDidYouHear',[''=>__('common.select')] + getPredefinedListItemsArr(11), $model->howDidYouHear, ['class'=>'form-control']) }}
         </div>
      </div>
    </div>

     <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('tellUsAboutYourself',__('membership_lang::membership.tellUsAboutYourself')) }}
          {{ Form::textarea('tellUsAboutYourself', $model->tellUsAboutYourself, ['class'=>'form-control', 'rows'=>'3']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('purposeOfJoining',__('membership_lang::membership.purposeOfJoining')) }}
          {{ Form::textarea('purposeOfJoining', $model->purposeOfJoining, ['class'=>'form-control', 'rows'=>'3']) }}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('company_id',__('membership_lang::membership.company_id')) }}
          {{ Form::select('company_id',[''=>__('common.select')] + getCompanyArr(), $model->company_id, ['class'=>'form-control selectpicker' , 'data-live-search'=>'true']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group pt-10">
          <button type="button" id="btn_create_company" class="btn btn-primary float-right">Create Company</button>
        </div>
      </div>
  </div>



<div class="company_form"  style="display:none;">

  <div class=" mt-9 mb-6 " >
  <h3 >Company Information</h3>
</div>

    <div class="row " >
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[name]',__('membership_lang::membership.company_name')) }}
          {{ Form::text('CompanyData[name]', $model->name, ['class'=>'form-control', 'placeholder'=>'company name']) }}
        </div>
        </div>
        <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[category]',__('c_lang::company.sector')) }}
          {{ Form::select('CompanyData[category]', [''=>__('common.select')] + getPredefinedListItemsArr(7), $model->category, ['class'=>'form-control']) }}
        </div>
        </div>
         <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[phonenumber]',__('common.phone')) }}
          {{ Form::text('CompanyData[phonenumber]', $model->phonenumber, ['class'=>'form-control', 'placeholder'=>'phone no']) }}
        </div>
      </div>
      
    </div>



    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[fax]',__('c_lang::company.fax')) }}
          {{ Form::text('CompanyData[fax]', $model->fax, ['class'=>'form-control', 'placeholder'=>'fax']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[postal_code]',__('c_lang::company.postal_code')) }}
          {{ Form::text('CompanyData[postal_code]', $model->postal_code, ['class'=>'form-control', 'placeholder'=>'postal code']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[emirates_number]',__('c_lang::company.emirates_number')) }}
          {{ Form::text('CompanyData[emirates_number]', $model->emirates_number, ['class'=>'form-control', 'placeholder'=>'emirate number']) }}
        </div>
      </div>
    </div>


     <div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[url]',__('c_lang::company.url')) }}
          {{ Form::text('CompanyData[url]', $model->url, ['class'=>'form-control', 'placeholder'=>'company url']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[vat_number]',__('c_lang::company.vat_number')) }}
          {{ Form::text('CompanyData[vat_number]', $model->vat_number, ['class'=>'form-control', 'placeholder'=>'vat number']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('CompanyData[address]',__('c_lang::company.address')) }}
          {{ Form::text('CompanyData[address]', $model->address, ['class'=>'form-control', 'placeholder'=>'address']) }}
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('CompanyData[show_in_directory]',__('c_lang::company.show_in_directory')) }}
          {{ Form::select('CompanyData[show_in_directory]',[''=>__('common.select')] + getDirectoryArr(), $model->show_in_directory, ['class'=>'form-control']) }}
        </div>
      </div>
       <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('CompanyData[is_active]',__('c_lang::company.is_active')) }}
          {{ Form::select('CompanyData[is_active]', getStatusArrCompany(), $model->is_active, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="form-group">
          {{ Form::label('CompanyData[about_company]',__('c_lang::company.about_company')) }}
          {{ Form::textarea('CompanyData[about_company]', $model->about_company, ['class'=>'form-control','rows'=>'3']) }}
        </div>
      </div>
    </div>

</div>


<div class=" mt-9 mb-6  ">
  <h3 >Account Information</h3>
</div>


    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('status',__('membership_lang::membership.status')) }}
          {{ Form::select('status', getStatusArrMembership(), $model->status, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        {{ Form::label('membership_type_id',__('membership_lang::membership.membership_type_id')) }}
        {{ Form::select('membership_type_id',getMembershipTypeArr(), $model->membership_type_id, ['class'=>'form-control']) }}
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('account_type',__('membership_lang::membership.account_type')) }}
          {{ Form::select('account_type', getAccountTypes(), $model->account_type, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
          {{ Form::label('expiry_date',__('membership_lang::membership.expiry_date')) }}
          {{ Form::text('expiry_date', $model->expiry_date, ['class'=>'form-control','placeholder'=>'expiry date', 'id'=>'kt_datepicker_3','readonly']) }}
      </div>



    </div>

      <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('parent_id',__('membership_lang::membership.parent_id')) }}
          {{ Form::select('parent_id',[''=>__('common.select')] + getMembers(), $model->parent_id, ['class'=>'form-control selectpicker' , 'data-live-search'=>'true']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
          {{ Form::label('user_industry',__('membership_lang::membership.user_industry')) }}
            {{ Form::select('user_industry', [''=>__('common.select')] + getPredefinedListItemsArr(7), $model->user_industry, ['class'=>'form-control']) }}
      </div>
    </div>








<div class="mt-9 mb-6">
  <h3 >User Docs</h3>
</div>


<div class="row py-5">
     <!--Trade Licence  -->
    <div class="col-xs-12 col-sm-4 d-flex align-items-start flex-column">
    <div class="image-input image-input-empty image-input-outline" id="kt_image_5" style="background-image: url(assets/media/users/blank.png)">          
     <div class="image-input-wrapper"></div>
     <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
      <i class="fa fa-pen icon-sm text-muted"></i>
      <input type="file" value="{{ $model->logo }}" name="logo" accept=".png, .jpg, .jpeg"/>
      <input type="hidden" name="profile_avatar_remove"/>
     </label>

     <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
      <i class="ki ki-bold-close icon-xs text-muted"></i>
     </span>

     <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
      <i class="ki ki-bold-close icon-xs text-muted"></i>
     </span>
    </div>
    <span class="label label-xl label-muted font-weight-bold label-inline mt-3">Trade Licence</span>
  </div>

 </div>
    



  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
    <a href="{{ url('/company') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}

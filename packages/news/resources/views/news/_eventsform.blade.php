@extends('layouts.app')
@section('content')


{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
	<div class=" mt-10 ml-10">
		<h3 >Create Events</h3>
	</div>

	<div class="card-body">
		@if ($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
			<div>{{ $error }}</div>
			@endforeach
		</div>
		@endif




		<div class="row px-0">

			<div class="col-4">
				<div class="form-group">
					{{ Form::label('title',__('Title')) }}
					{{ Form::text('title', $model->title, ['class'=>'form-control']) }}
				</div>
			</div>
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('type',__('Type')) }}
					{{ Form::text('type', $model->type, ['class'=>'form-control']) }}
				</div>
			</div>
			<div class="col-4">
				<div class="form-group">
					{{ Form::label('venue',__('Venue')) }}
					{{ Form::text('venue', $model->venue, ['class'=>'form-control']) }}
				</div>
			</div>

		</div>



		<div class="row px-0">

			<div class="col-3">
				<div class="form-group">
					{{ Form::label('address',__('Adderss')) }}
					{{ Form::text('address', $model->address, ['class'=>'form-control']) }}
				</div>
			</div>
			<div class="col-3">
				<div class="form-group">
					{{ Form::label('map',__('Map')) }}
					{{ Form::text('map', $model->map, ['class'=>'form-control']) }}
				</div>
			</div>
			<div class="col-3">
				<div class="form-group">
					{{ Form::label('facebook_gallery',__('Facebook Gallery')) }}
					{{ Form::text('facebook_gallery', $model->facebook_gallery, ['class'=>'form-control']) }}
				</div>
			</div>
			<div class="col-3">
				<div class="form-group">
					{{ Form::label('group_size',__('Group Size')) }}
					{{ Form::text('group_size', $model->group_size, ['class'=>'form-control']) }}
				</div>
			</div>

		</div>


		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="form-group">
					{{ Form::label('short_description',__('Short Description')) }}
					{{ Form::textarea('short_description', $model->short_description, ['class'=>'form-control ']) }}
				</div>
			</div>
		</div>

		<div class="mt-4 mb-10 ">
			<h3 >Timings</h3>
		</div>

		<div class="row">
			<div class="col-6">
				<div class="row">
					<div class="col-4">
						<div class="form-group">
							{{ Form::label('registration_start',__('Registration Start')) }}
							{{ Form::text('registration_start', $model->registration_start, ['class'=>'form-control']) }}
						</div>
					</div>
					<div class="col-4">
						<div class="form-group">
							{{ Form::label('registration_start',__('Registration End')) }}
							{{ Form::text('registration_start', $model->registration_start, ['class'=>'form-control']) }}
						</div>
					</div>
					<div class="col-4">
						<div class="form-group">
							{{ Form::label('cancellation_date',__('Cancellation Date')) }}
							{{ Form::text('cancellation_date', $model->cancellation_date, ['class'=>'form-control']) }}
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							{{ Form::label('event_start_date',__('Event Start Date')) }}
							{{ Form::text('event_start_date', $model->event_start_date, ['class'=>'form-control']) }}
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							{{ Form::label('event_end_date',__('Event End Date')) }}
							{{ Form::text('event_end_date', $model->event_end_date, ['class'=>'form-control']) }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<div class="form-group">
							{{ Form::label('event_start_time',__('Event Start Time')) }}
							{{ Form::text('event_start_time', $model->event_start_time, ['class'=>'form-control']) }}
						</div>
					</div>
					<div class="col-6">
						<div class="form-group">
							{{ Form::label('event_end_time',__('Event End Time')) }}
							{{ Form::text('event_end_time', $model->event_end_time, ['class'=>'form-control']) }}
						</div>
					</div>
				</div>
			</div>
			<div class="col-6">
				
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="form-group">
					{{ Form::label('description',__('news_lang::news-lang.description')) }}
					{{ Form::text('description', $model->description, ['class'=>'form-control editor']) }}
				</div>
			</div>
		</div>







	</div>




	<div class="card-footer">
		<button type="submit" class="btn btn-success">{{__('common.save')}}</button>
		<a href="{{ url('/news') }}" class="btn btn-default">{{__('common.cancel')}}</a>
	</div>
</div>
{!! Form::close() !!}

@endsection

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
@endpush


@push('js-script')

tinymce.init({
selector: '.editor',
menubar: false,
toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
plugins : 'advlist autolink link image lists charmap code table',
image_advtab: true ,
});

@endpush
<div class="container">
  <div class="card card-custom">
    <div class="card-header card-header-tabs-line">
      <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#kt_builder_themes">News</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#kt_builder_page">Seo</a>
        </li>
      </ul>
    </div>
    {!! Form::model($model, ['files' => true]) !!}
    <!--begin::Body-->
    <div class="card-body">
      <div class="tab-content pt-3">
        <!--begin::Tab Pane-->
        <div class="tab-pane active" id="kt_builder_themes">
          @if ($errors->any())
          <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
            @endforeach
          </div>
          @endif

          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                {{ Form::label('title',__('news_lang::news-lang.title')) }}
                {{ Form::text('title', $model->title, ['class'=>'form-control']) }}
              </div>
              <div class="form-group">
                {{ Form::label('category',__('news_lang::news-lang.category')) }}
                {{ Form::select('category', [''=>__('common.select')] + getPredefinedListItemsArr($category_list_id), $model->category, ['class'=>'form-control']) }}
              </div>
            </div>

            

          </div>            

          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                {{ Form::label('link',__('news_lang::news-lang.link')) }}
                {{ Form::text('link', $model->link, ['class'=>'form-control']) }}
              </div>
            </div>

            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                {{ Form::label('sort_order',__('news_lang::news-lang.sort_order')) }}
                {{ Form::number('sort_order', $model->sort_order, ['class'=>'form-control']) }}
              </div>
            </div>
          </div>


          <div class="row">
           <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('isPublished',__('news_lang::news-lang.isPublished')) }}
              {{ Form::select('isPublished', [''=>__('common.select')] + getNewsPublishArr(),  $model->isPublished, ['class'=>'form-control']) }}
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('homepage_show',__('news_lang::news-lang.homepage_show')) }}
              {{ Form::select('homepage_show', [''=>__('common.select')] + getNewsPublishArr(), $model->homepage_show, ['class'=>'form-control']) }}
            </div>
          </div>

        </div>


        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('related_to',__('news_lang::news-lang.related_to')) }}
              {{ Form::select('related_to', [''=>__('common.select')] + getReleatedTypeArr(), $model->related_to, ['class'=>'form-control']) }}
            </div>
          </div>

          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('rel_id',__('news_lang::news-lang.rel_id')) }}
              {{ Form::select('rel_id', [''=>__('common.select')], $model->rel_id, ['class'=>'form-control']) }}
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label class="checkbox checkbox-success">
                {{ Form::checkbox('re_post', '1', $model->re_post==1 ? true : false, array('class' => 'rePost')) }}
                <span class="mx-2"></span>{{__('news_lang::news-lang.re_post')}}</label>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                {{ Form::label('short_description',__('news_lang::news-lang.short_description')) }}
                {{ Form::text('short_description', $model->short_description, ['class'=>'form-control editor']) }}
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                {{ Form::label('description',__('news_lang::news-lang.description')) }}
                {{ Form::text('description', $model->description, ['class'=>'form-control editor']) }}
              </div>
            </div>
          </div>

          <input type="input" name="news_type" value="member" class="d-none">



        </div>
        <!--end::Tab Pane-->
        <!--begin::Tab Pane-->
        <div class="tab-pane" id="kt_builder_page">
          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                {{ Form::label('meta_title',__('news_lang::news-lang.meta-title')) }}
                {{ Form::text('meta_title', $model->meta_title, ['class'=>'form-control']) }}
              </div>
            </div>

            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                {{ Form::label('slug',__('news_lang::news-lang.slug')) }}
                {{ Form::text('slug', $model->slug, ['class'=>'form-control']) }}
              </div>
            </div>

            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                {{ Form::label('meta_description',__('news_lang::news-lang.meta-description')) }}
                {{ Form::text('meta_description', $model->meta_description, ['class'=>'form-control editor']) }}
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--end::Tab Pane-->
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
    <a href="{{ url('/news') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
  {!! Form::close() !!}
</div>
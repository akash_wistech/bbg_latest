<?php
$query = DB::table('settings')
->select('config_value')
->where('config_name', '=', 'news_categories_list')
->first();
$category_list_id = $query->config_value;

?>

<section>



  <!--begin::Entry-->
  <div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
      <div class="card card-custom">
        <div class="card-body p-0">
          <!--begin: Wizard-->
          <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first" data-wizard-clickable="true">
            <!--begin: Wizard Nav-->
            <div class="wizard-nav">
              <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-3">
                <!--begin::Wizard Step 1 Nav-->
                <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                  <div class="wizard-label">
                    <h3 class="wizard-title">
                    General</h3>
                    <div class="wizard-bar"></div>
                  </div>
                </div>
                <!--end::Wizard Step 1 Nav-->
                <!--begin::Wizard Step 2 Nav-->
                <div class="wizard-step" data-wizard-type="step">
                  <div class="wizard-label">
                    <h3 class="wizard-title">
                    SEO</h3>
                    <div class="wizard-bar"></div>
                  </div>
                </div>
                <!--end::Wizard Step 2 Nav-->
              </div>
            </div>
            <!--end: Wizard Nav-->
            <!--begin: Wizard Body-->
            <div class="row justify-content-center px-8">
              <div class="col">
                <!--begin: Wizard Form-->
                {!! Form::model($model, ['files' => true,'id'=>'kt_form']) !!}

                <!--begin: Wizard Step 1-->
                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                  <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                      <div>{{ $error }}</div>
                      @endforeach
                    </div>
                    @endif

                    <div class="row px-0">

                      <div class="col-md-4 col-lg-5 px-0">
                        <div class="col-12">
                          <div class="form-group">
                            {{ Form::label('title',__('news_lang::news-lang.title')) }}
                            {{ Form::text('title', $model->title, ['class'=>'form-control']) }}
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-group">
                            {{ Form::label('category',__('news_lang::news-lang.category')) }}
                            {{ Form::select('category', [''=>__('common.select')] + getPredefinedListItemsArr($category_list_id), $model->category, ['class'=>'form-control']) }}
                          </div>
                        </div>
                      </div> 


                      <div class="col-md-4 col-lg-5  px-0 ">
                        <div class="col-12">
                          <div class="form-group">
                            {{ Form::label('link',__('news_lang::news-lang.link')) }}
                            {{ Form::text('link', $model->link, ['class'=>'form-control']) }}
                          </div>
                        </div>
                        <div class="col-12">
                          <div class="form-group">
                            {{ Form::label('sort_order',__('news_lang::news-lang.sort_order')) }}
                            {{ Form::number('sort_order', $model->sort_order, ['class'=>'form-control']) }}
                          </div>
                        </div>
                      </div>


                      <!-- image here -->
                      <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-4">
                        <div id="images" class="form-group pb-0 mb-2">
                          <a href="" data-thumbid="thumb-image" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
                            <img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
                            alt="" width="145" height="125" title=""
                            data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
                          </a>
                          {{ Form::text('image', $model->image, ['maxlength' => true, 'data-targetid' => 'input-image','class'=>'form-control d-none']) }}
                        </div>
                        <span class="label label-xl label-muted font-weight-bold label-inline mx-6">Main Image</span>
                      </div>
                      <!-- image end -->



                    </div>




                    <div class="row">
                     <div class="col-xs-12 col-sm-5">
                      <div class="form-group">
                        {{ Form::label('isPublished',__('news_lang::news-lang.isPublished')) }}
                        {{ Form::select('isPublished', [''=>__('common.select')] + getNewsPublishArr(),  $model->isPublished, ['class'=>'form-control']) }}
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                      <div class="form-group">
                        {{ Form::label('homepage_show',__('news_lang::news-lang.homepage_show')) }}
                        {{ Form::select('homepage_show', [''=>__('common.select')] + getNewsPublishArr(), $model->homepage_show, ['class'=>'form-control']) }}
                      </div>
                    </div>

                  </div>


                  <div class="row">
                    <div class="col-xs-12 col-sm-5">
                      <div class="form-group">
                        {{ Form::label('related_to',__('news_lang::news-lang.related_to')) }}
                        {{ Form::select('related_to', [''=>__('common.select')] + getReleatedTypeArr(), $model->related_to, ['class'=>'form-control','id'=>'relatedType']) }}
                      </div>
                    </div>

                    <div class="col-xs-12 col-sm-5">
                      <div class="form-group">
                        {{ Form::label('rel_id',__('news_lang::news-lang.rel_id')) }}
                        {{ Form::select('rel_id', ['' => 'Select'], $model->rel_id, ['class'=>'form-control select2','id' => 'target-rel-id']) }}
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-xs-12 col-sm-5">
                     <div class="form-group">
                      {{ Form::label('date',__('news_lang::news-lang.date')) }}<span class="text-danger"><b>*</b></span>
                      <div class="input-group date">
                        {{ Form::text('date',  $model->date, ['class'=>'form-control dtpicker', 'readonly']) }}
                        <div class="input-group-append">
                          <span class="input-group-text">
                            <i class="la la-calendar"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 my-10">
                    <div class="form-group">
                      <label class="checkbox checkbox-success">
                        {{ Form::checkbox('re_post', '1', $model->re_post==1 ? true : false, array('class' => 'rePost')) }}
                        <span class="mx-2"></span>{{__('news_lang::news-lang.re_post')}}</label>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                        {{ Form::label('short_description',__('news_lang::news-lang.short_description')) }}
                        {{ Form::textarea('short_description', $model->short_description, ['class'=>'form-control', 'rows' => 4]) }}
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12 col-sm-12">
                      <div class="form-group">
                        {{ Form::label('description',__('news_lang::news-lang.description')) }}
                        {{ Form::textarea('description', $model->description, ['class'=>'form-control editor']) }}
                      </div>
                    </div>
                  </div>

                  <input type="input" name="news_type" value="member" class="d-none">





                </div>
              </div>
              <!--end: Wizard Step 1-->


              <!--begin: Wizard Step 2-->
              <div class="pb-5" data-wizard-type="step-content">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('meta_title',__('news_lang::news-lang.meta_title')) }}
                      {{ Form::text('meta_title', $model->meta_title, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('slug',__('news_lang::news-lang.slug')) }}
                      {{ Form::text('slug', $model->slug, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('meta_description',__('news_lang::news-lang.meta_description')) }}
                      {{ Form::textarea('meta_description', $model->meta_description, ['class'=>'form-control', 'rows' => '3']) }}
                    </div>
                  </div>
                </div>
              </div>
              <!--end: Wizard Step 2-->


              <!--begin: Wizard Actions-->
              <div class="d-flex justify-content-between border-top">
                    <!-- <div class="mr-2">
                      <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Previous</button>
                    </div>
                    <div>
                      <button type="button" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-submit">Submit</button>
                      <button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-next">Next</button>
                    </div> -->

                    <div class="card-footer border-0">
                      <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
                      <a href="{{ url('news') }}" class="btn btn-default">{{__('common.cancel')}}</a>
                    </div>

                  </div>
                  <!--end: Wizard Actions-->
                  {!! Form::close() !!}
                  <!--end: Wizard Form-->
                </div>
              </div>
              <!--end: Wizard Body-->
            </div>
            <!--end: Wizard-->
          </div>
        </div>
      </div>
      <!--end::Container-->
    </div>
    <!--end::Entry-->

  </section>




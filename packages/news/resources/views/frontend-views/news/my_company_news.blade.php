@extends('frontend.app')
@section('content')

<?php
use App\Models\Contact;
$no_image = asset('assets/images/dummy-image-1.jpg');
if ($model<>null) {
	?>
	<x-contact-banner/>
	<?php
}

// dd($results);
?>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-6">
						<h3 class="">News</h3>
					</div>
					<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-6 float-right" style="padding: 10px;">
						<?php
						if ($totalNewsPerYear<=12) {?>
							<a href="{{url('add-news')}}"><button type="button" class="Mybtn float-right mx-2">Add News</button></a>
							<?php
						}else{?>
							<button type="button" class="Mybtn float-right mx-2" style=" background-color: #f0ad4e; color: white;"><i class="fa fa-lock mx-2"></i>Closed</button>
							<?php
						}
						?>
					</div>
				</div>
				<br>
				<table class="table table-responsive">
					<thead>

						<tr>
							<th>Image</th>
							<th>Title</th>
							<th>Category</th>
							<th>Link</th>
							<th>Show on Home Page</th>
							<th>Status</th>
							<th>From</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($results<>null) {
							foreach ($results as $result) {
								// dd($result->contactName->name);
								$userName = Contact::where(['id'=>$result->created_by])->first();
								// dd($userName->name);
								?>
								<tr id="<?= $result->id ?>">
									<th class="text-center"><img src="<?= ($result->image<>null) ? $result->image : $no_image ?>" width="50px;"></th>
									<td class="text-center"><?= $result->title ?></td>
									<td class="text-center"><?= getCatForNews($result->category) ?></td>
									<td class="text-center"><?= $result->link ?></td>
									<td class="text-center"><?= getNewsHomePageLabelFend()[$result->homepage_show] ?></td>
									<td class="text-center"><?= getNewsPublishLabelFend()[$result->isPublished] ?></td>
									<td class="text-center"><?= $userName->name ?></td>
									<td class="invoicelink text-center">
										<a class="btn-sm Mybtn" href="<?= url('news-details',['id'=>$result->id]) ?>" target="_blank" title="View Details" style="color:#fff !important" data-method="post"><i class="fa fa-eye"></i></a>
										<?php
										if ($result->created_by==Auth::user()->id) {?>
											<a class="btn-sm Mybtn" href="<?= url('MyNews/edit',['id'=>$result->id]) ?>"  title="Edit" style="color:#fff !important" data-method="post"><i class="fa fa-pencil"></i></a>
											<a class="btn-sm Mybtn  confirmation" href="javascript:;" title="Delete" style="color:#fff !important" data-method="post"><i class="fa fa-trash"></i></a>
											<?php
										}
										?>

									</td>
								</tr>
								<?php
							}
						}
						?>
					</tbody>
				</table>
				<br>
				<div class="col-md-12 text-center pull-right">
					{{$results->links()}}
				</div>
			</div>

		</div>
	</div>
</section>

@endsection

@push('js')
<script>


	$(".confirmation").click(function(e) {
		var id = $(this).parents("tr").attr("id");


		swal({
			title: 'Are you Sure!',
			text: 'You Wont be able to revert this action',
			type: 'warning',
			timer: 5000,
			confirmButtonText: 'OK'
		}).then((result) => {
			if (result.value) {
				$.ajax({

					url: "{{url('MyNews/delete')}}/"+id,
					type: 'POST',
					data : {
						"_token": "{{ csrf_token() }}"
					},

					success: function (data) {
                    // var data = JSON.parse(data);
                    // console.log(data);

                    if(data == 1){
                    	$("#"+id).remove();
                    	swal({
                    		title: 'Success!',
                    		text: 'Deleted Successfully',
                    		type: 'success',
                    		timer: 5000,
                    		confirmButtonText: 'OK'
                    	});
                    }
                },
                error: function (e) {

                	swal({
                		title: 'Error!',
                		text: 'Something Went Wrong!',
                		type: 'error',
                		timer: 5000,
                		confirmButtonText: 'OK'
                	});
                }
            });
			}
		})


	});

</script>
@endpush

@extends('frontend.app')
@section('content')


<section class="MainArea">
    <div class="container">


     <div class="row ">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea ">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft ">
  
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-md-8 col-md-offset-2 loginFormstyle">
                        <div class="Heading text-center">
                            <h3>Newsletter Subscription</h3>
                        </div>
                        {!! Form::model($model, ['files' => true]) !!}
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <p>I would like to receive emails from the British Business Group Dubai & Northern Emirates</p>
                                <div class="SiteText">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        {{ Form::text('first_name', '', ['class'=>'form-control', 'autofocus'=>true, 'placeholder'=>'Enter First Name']) }}
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        {{ Form::text('last_name', '', ['class'=>'form-control', 'autofocus'=>true, 'placeholder'=>'Enter Last Name']) }}
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        {{ Form::text('phone', '', ['class'=>'form-control', 'autofocus'=>true, 'placeholder'=>'Enter Mobile No.']) }}
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        {{ Form::text('email', '', ['class'=>'form-control', 'autofocus'=>true, 'placeholder'=>'Enter Email']) }}

                                        {!! Form::submit('Subscribe', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-2"></div>
                </div>

            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea ">
            @include('frontend.blocks.RightSideBarWidget')
        </div>
    </div>
</div>
</section>


@endsection
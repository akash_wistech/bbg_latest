@extends('frontend.app')
@section('content')
<?php  
$query = DB::table('settings')
->select('config_value')
->where('config_name', '=', 'news_categories_list')
->first();
$category_list_id = $query->config_value;
?>

<style>
	input.larger {
		width: 25px;
		height: 25px;
	}
</style>
<section class="MainArea">
	<div class="container LeftArea">
		{!! Form::model($model, ['files' => true]) !!}

		<input type="input" name="news_type" value="member" class="d-none">
		<div class="row px-4">

			<div class="col-md-4 col-lg-5 px-0">
				<div class="col-12">
					<div class="form-group">
						{{ Form::label('title',__('news_lang::news-lang.title')) }}
						{{ Form::text('title', $model->title, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						{{ Form::label('category',__('news_lang::news-lang.category')) }}
						{{ Form::select('category', [''=>__('common.select')] + getPredefinedListItemsArr($category_list_id), $model->category, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					{{ Form::label('isPublished',__('news_lang::news-lang.isPublished')) }}
					{{ Form::select('isPublished', [''=>__('common.select')] + getNewsPublishArr(),  $model->isPublished, ['class'=>'form-control']) }}
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					{{ Form::label('date',__('news_lang::news-lang.date')) }}<span class="text-danger"><b>*</b></span>
					<div class="input-group date">
						{{ Form::text('date',  $model->date, ['class'=>'form-control dtpicker', 'readonly']) }}
					</div>
				</div>
			</div> 


			<div class="col-md-4 col-lg-5  px-0 ">
				<div class="col-12">
					<div class="form-group">
						{{ Form::label('link',__('news_lang::news-lang.link')) }}
						{{ Form::text('link', $model->link, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-12">
					<div class="form-group">
						{{ Form::label('sort_order',__('news_lang::news-lang.sort_order')) }}
						{{ Form::number('sort_order', $model->sort_order, ['class'=>'form-control','style'=>'height:39px;']) }}
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					{{ Form::label('homepage_show',__('news_lang::news-lang.homepage_show')) }}
					{{ Form::select('homepage_show', [''=>__('common.select')] + getNewsPublishArr(), $model->homepage_show, ['class'=>'form-control']) }}
				</div>
				<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 py-5">
					<label class="checkbox checkbox-lg ">
						<input class="larger" name="re_post" type="checkbox" value="1">
						<span class="label">Re Post</span>
					</label>
				</div>
			</div>


			<!-- image here -->
			<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 py-4">
				<div class="form-group">
					<a href="javascript:;" id="upload-document1" onclick="uploadAttachment(1)" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">
						<i></i>
						<img src="http://bbg_events.local/assets/images/dummy-image-1.jpg" width="100" alt="" title="" data-placeholder="no_image.png" style="">
						<br>
					</a>
					<p class="image_upload_label my-2 mx-2">click Image to upload</p>
					<input id="input-attachment1" maxlength="" name="image" type="hidden" value="">
				</div>
			</div>
			<!-- image end -->
		</div>


		<div class="row px-4">			
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				{{ Form::label('short_description',__('news_lang::news-lang.short_description')) }}
				{{ Form::textarea('short_description', $model->short_description, ['class'=>'form-control', 'rows' => 4]) }}
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  py-4 px-4">
				{{ Form::label('description',__('news_lang::news-lang.description')) }}<span class="text-danger"><b>*</b></span>
				{{ Form::textarea('description',  $model->description, ['class'=>'form-control editor']) }}
			</div>
		</div>

		<div class="row px-4">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  py-4 px-4">
				<button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Save</button>
			</div>
		</div>
		{!! Form::close() !!}



	</div>
</section>
@endsection


@push('css')
<link rel="stylesheet" type="text/css" media="all"
href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css"    />

<style>
	form div.required label.control-label:after {
		content: " * ";
		color: #a94442;
	}
</style>
@endpush()



@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table',
		image_advtab: true ,
	});
</script>

<script>
	$('.dtpicker').datepicker({ 
		dateFormat: 'yy-mm-dd',
		todayHighlight: true,
		orientation: "bottom left",
		todayBtn: "linked",
		clearBtn: true,
		autoclose: true,
	});
</script>
@endpush
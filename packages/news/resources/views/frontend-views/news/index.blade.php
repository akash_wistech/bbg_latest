@extends('frontend.app')
@section('content')

<?php 
$login=0;
if (Auth::check()) {
	$login=1;
}
?>

<style>
	.col-xl-3,.col-xl-4,.col-xl-2{
		float: left;
	}
	input.Mybtn{
		padding: 10px 32px;
	}
	.Mybtn{
		padding: 10px 32px;
	}
	.bg-infoo{
		padding-top:10px;
	}
</style>
<section class="MainArea">
	<div class="container">

		<div class="row" >
			<div class="memberS mx-2 row" style="padding: 15px;border: 1px solid #ddd;margin-bottom: 5px;width: 99% !important;">
				<div class="col-<?=$login=='1'?'10':'12'?> bg-infoo" style="background-color: #EEEEEE;">
					{!! Form::open(array('url' => 'news-fend', 'method' => 'get', 'id' => 'member-searcher','enableClientScript' => false)) !!}
					<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
						{{ Form::select('month', [''=>__('Select Month')] + getMonthArr(), $month, ['class'=>'form-control']) }}
					</div>

					<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">

						{{ Form::select('year', [''=>__('Select Year')] + getYears(), $year, ['class'=>'form-control']) }}
					</div>

					<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-4">
						{{ Form::select('category', [''=>__('Select Category')] +  getPredefinedListOptionsArr(getSetting('news_categories_list')), $type, ['class'=>'form-control']) }}
					</div>

					<div class="col-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
						{!! Form::submit(__('Search'), ['class' => 'Mybtn']) !!}
					</div>
					{!! Form::close() !!}
				</div>
				<?php  
				if (Auth::check()) {
					?>
					<div class="col-2"  style="padding: 10px; background-color: #EEEEEE;">
						<?php  
						if ($totalNewsPerYear<=12) {
							?>
							<a href="{{url('add-news')}}"><button type="button" class=" Mybtn">Add News</button></a>
							<?php
						}else{?>
							<button type="button" class="Mybtn float-right mx-2" style=" background-color: #f0ad4e; color: white;"><i class="fa fa-lock mx-2"></i>Closed</button>
							<?php
						}
						?>
					</div>
					<?php
				}
				?>
			</div>
		</div>


		<div class="row ">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<div class="Heading">
						<h3><?= $title; ?></h3>
					</div>
					<?php
					if (isset($news) && $news <> null && count($news) > 0) {
						$dummyImg = asset('assets/images/dummy-image-1.jpg');
						foreach ($news as $new) {
							?>
							<div class="Event1">
								<div class="row">
									<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
										<img src="<?= (!empty($new->image)) ? $new->image : $dummyImg ?>" class="img-fluid" alt="">
									</div>
									<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
										<div class="EventText">
											<h3><i class="fa fa-calendar"
												aria-hidden="true"></i> <?= formatDate($new->created_at); ?>
											</h3>
											<h4><?= $new->title; ?></h4>
											<p><?= $new->short_description; ?></p>
											<span class="EventDetail">
												<a href="<?= url('news-details',['id'=>$new->id]) ?>" id="<?= $new->id ?>">News Detail</a>
											</span>
										</div>
									</div>
								</div>
							</div>
							<?php
						}
					}else{
						?>
						<!--<div class="alert alert-warning">There isn't any news yet,you hear soon</div>-->
						<div class="row">
							<div class="col-12 col-sm-12">
								<div class="EventText text-justify">
									<p>As part of the BBG's commitment to provide the latest information on market intelligence and research to its members and wider audience, we are delighted to share information on Expo 2020. We are working closely with the Department for International Trade Expo 2020 team and across industry sectors, to ensure you are up to date on this exciting international event for creativity, innovation and cultural diversity.</p>
								</div>
							</div>
						</div>
						<?php
					}
					?>
					<div class="col-md-12 text-center">
						{{ $news->links() }}
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>
@endsection
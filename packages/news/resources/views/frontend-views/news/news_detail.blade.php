<?php 
$title = 'News Details';
?>

@extends('frontend.app')
@section('content')
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<?php
					if (isset($news) && $news <> null) {
						$dummyImg = asset('assets/images/dummy-image-1.jpg');
						
						$title = $news->title;
						?>
						<div class="Heading">
							<h3><?= $title; ?></h3>
							<h4><?= $news->title; ?></h4>
							<p>
								<span class="BoldText">Date Posted:</span> <?= formatDate($news->created_at); ?>
							</p>
						</div>
						<div class="EventDetail">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<img src="<?= (!empty($news->image)) ? $news->image : $dummyImg ?>" class="img-fluid" alt="<?= $news->title; ?>">
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="DescriptionHeading text-justify"><br/> <br/> <br/>
										<h4><?= $news->short_description; ?></h4>
										<?= $news->description; ?>
									</div>

									<div id="share" class=""></div>

								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>
@endsection

@push('js')
<script>
	var id = "{{$news->id}}";
	var title = "{{$news->title}}"
	var url = "{{url('news-details')}}/"+id;

	$("#share").jsSocials({
		url : url,
		text: title,
		shareIn: "popup",
		showLabel: false,
		showCount: false,
		shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
	});
</script>
@endpush
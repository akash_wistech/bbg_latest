<?php

namespace Wisdom\News\Providers;

use Illuminate\Support\ServiceProvider;

class NewsServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
		$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'news');
		$this->loadViewsFrom(__DIR__ . '/../../resources/views/frontend-views', 'frontend-views');
		$this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'news_lang');
		$this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
	}
}
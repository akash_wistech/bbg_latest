<?php
use Illuminate\Support\Facades\DB;
use App\Models\Company;
use App\Models\User;

if (!function_exists('getNewsPublishArr')) {
  /**
  * return status array
  */
  function getNewsPublishArr()
  {
  	return [
  		'0' => __('No'),
  		'1' => __('Yes'),
  	];
  }
}


if (!function_exists('getNewsPublishLabel')) {
  /**
  * return status array
  */
  function getNewsPublishLabel()
  {
    return [
      '0' => '<span class="label label-lg font-weight-boldlabel-light-primary label-inline">No</span>',
      '1' => '<span class="label label-lg font-weight-bold label-light-success label-inline">Yes</span>',
    ];
  }
}

if (!function_exists('getRePostLabel')) {
  /**
  * return status array
  */
  function getRePostLabel($rePostVal)
  {
    if ($rePostVal==1) {
      return '<span class="label label-lg font-weight-bold label-inline label-light-primary">On</span>';
    }else{
      return '<span class="label label-lg font-weight-bold label-inline label-light-info">Off</span>';
    }
  }
}



if (!function_exists('getReleatedTypeArr')) {
  function getReleatedTypeArr()
  {
  	return [
  		'general' => __('General'),
  		'company' => __('Company'),
  		'member' => __('Member'),
  	];
  }
}



if (!function_exists('getCatForNews')) {
  function getCatForNews($id)
  {
    $result = DB::table('predefined_list_detail')
    ->select('title')
    ->where('id', '=', $id)
    ->first();
    if ($result<>null) {
      return $result->title;
    }else{
      return false;
    }
  }
}



if (!function_exists('getRelToForNews')) {
  function getRelToForNews($rel_type,$id)
  {
    if ($rel_type!=null && $rel_type=='company') {
      if ($id!=null) {
        $result = Company::select(['title'])->where(['id'=>$id])->first();
        if ($result<>null) {
          return $result->title;
        }else{
          return false;
        }
      }
    }

    if ($rel_type!=null && $rel_type=='member') {
      if ($id!=null) {
        $result = User::select(['name'])->where(['id'=>$id])->first();
        if ($result<>null) {
          return $result->name;
        }else{
          return false;
        }
      }
    }
  }
}






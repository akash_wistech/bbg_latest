<?php

namespace Wisdom\News\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use App\Traits\Sluggable;
use App\Models\Contact;

class News extends FullTables
{
	use HasFactory, Blameable, Sluggable, SoftDeletes;

	public $moduleTypeId = 'news';


	// public function ContactName()
	// {
	// 	return $this->hasOne(Contact::class, 'created_by', 'id');
	// }
}

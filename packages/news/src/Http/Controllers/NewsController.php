<?php

namespace Wisdom\News\Http\Controllers;

use Wisdom\News\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Company;
use App\Models\Prospect;
use App\Models\Contact;
use Illuminate\Database\Eloquent\SoftDeletes;
use NaeemAwan\ModuleSubscription\Models\SubscriptionUsers;
use Carbon\Carbon;

class NewsController extends Controller
{

  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view('news::news.index',compact('moduleTypeId','request'));
  }

  public function newModel()
  {
    return new News;
  }


  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');

    if ($request->has('company_id')) {
      $query->where('company_id', $request->company_id);
    }


// permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'membership',$query);



    if($request->search['value']!=''){
      $keyword = $request->search['value'];

      // dd($keyword);
      $srchFlds[]='title';
      $srchFlds[]='short_description';
      $srchFlds[]='description';
      $srchFlds[]='news_type';
      $srchFlds[]='link';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }



    // if($request->created_at!=''){
    //   if(strpos($request->created_at," - ")){
    //     $query->where(function($query) use($request){
    //       list($start_date,$end_date)=explode(" - ",$request->created_at);
    //       $query->where('created_at','>=',$start_date)
    //       ->where('created_at','<=',$end_date);
    //     });
    //   }else{
    //     $query->where('created_at',$request->created_at);
    //   }
    // }
    // if($request->name!='')$query->where('name','like','%'.$request->name.'%');
    // if($request->full_name!='')$query->where('full_name','like','%'.$request->full_name.'%');
    // if($request->company_name!='')$query->where('company_name','like','%'.$request->company_name.'%');
    // if($request->email!='')$query->where('email','like','%'.$request->email.'%');
    // if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
    // if($request->input_field!=''){
    //   advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    // }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

// $gridViewColumns=getGridViewColumns($moduleTypeId);


    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
// dd($models);

    if($models!=null){
      foreach($models as $model){
        $thisData=[];
//Colors column
// $colorArr = getActivityColors($moduleTypeId,$model->id);
// $thisData['cs_col']=implode("",$colorArr);

        $ispublish = getNewsPublishLabel()[$model->isPublished];
        $rePost = getRePostLabel($model->re_post);
        $showOnHomePage = getNewsPublishLabel()[$model->homepage_show];
        $link = '<span class="font-weight-bold text-primary">'.$model->link.'</span>';

        $thisData['id']=$model->id;
        $thisData['title']=$model->title;
        $thisData['category']=getCatForNews($model->category);
        $thisData['short_description']=$model->short_description;
        $thisData['description']=$model->description;
        $thisData['news_type']=$model->news_type;
        $thisData['link']=$link;
        $thisData['isPublished']=$ispublish;
        $thisData['homepage_show']=$showOnHomePage;
        $thisData['sort_order']=$model->sort_order;
        $thisData['re_post']=$rePost;
        $thisData['related_to']=$model->related_to;
        $thisData['rel_id']=getRelToForNews($model->related_to,$model->rel_id);
        $thisData['image']=getImageTag($model->image);
        $thisData['cb_col']='';

// if($gridViewColumns!=null){
//   foreach($gridViewColumns as $gridViewColumn){
//     $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
//   }
// }
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->name;

        $actBtns=[];
        if(checkActionAllowed('view','news')){
          $actBtns[]='
          <a href="'.url('news/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update','news')){
          $actBtns[]='
          <a href="'.url('news/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete','news')){
          $actBtns[]='
          <a href="'.url('news/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }

        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }


    $dtInputColsArr = [];
    $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
// $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
// if($gridViewColumns!=null){
//   foreach($gridViewColumns as $gridViewColumn){
//     $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
//   }
// }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);

  }

  public function create(Request $request)
  {
    // dd($request->input());
    $model = new News;

    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|string',
        'category' => 'required|integer',
        'description' => 'required|string',
        'isPublished' => 'required|integer',
        'homepage_show' => 'required|integer',
      ]);




      $model = new News;
      $model->title = $request->title;
      $model->category = $request->category;
      $model->link = $request->link;
      $model->sort_order = $request->sort_order;
      $model->isPublished = $request->isPublished;
      $model->homepage_show = $request->homepage_show;
      $model->related_to = $request->related_to;
      $model->rel_id = $request->rel_id;
      $model->short_description = $request->short_description;
      $model->description = $request->description;
      $model->re_post = $request->re_post!=''?$request->re_post:0;
      $model->news_type = $request->news_type;
      $model->image = $request->image;
      $model->date = $request->date;
      $model->meta_title = $request->meta_title;
      $model->slug = $request->slug;
      $model->meta_description = $request->meta_description;
      $model->created_by = Auth::user()->id;

      if ($request->related_to=="company") {
        $model->company_id = $request->rel_id;
      }
      if ($request->related_to=="member") {
        $model_contact = Contact::where(['id'=>$request->rel_id])->first();
        $model->company_id = $model_contact->main_company_id;
      }

      if ($model->save()) {
        return redirect('news')->with('success', __('news_lang::news-lang.saved'));
      }

    }
    return view('news::news.create',['model'=>$model]);
  }

  public function view(News $news, $id)
  {
    $model=News::where('id',$id)->first();
// dd($model);
    return view('news::news.view',compact('model'));
  }

  public function update(Request $request, News $news, $id)
  {
// dd($request->all());
    $model=News::where('id',$id)->first();

    // dd($model);

    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|string',
        'category' => 'required|integer',
        'description' => 'required|string',
        'isPublished' => 'required|integer',
        'homepage_show' => 'required|integer',
      ]);

      $newsSaved=News::where('id',$id)->first();
      $newsSaved->title = $request->title;
      $newsSaved->category = $request->category;
      $newsSaved->link = $request->link;
      $newsSaved->sort_order = $request->sort_order;
      $newsSaved->isPublished = $request->isPublished;
      $newsSaved->homepage_show = $request->homepage_show;
      $newsSaved->related_to = $request->related_to;
      $newsSaved->rel_id = $request->rel_id;
      $newsSaved->short_description = $request->short_description;
      $newsSaved->description = $request->description;
      $newsSaved->re_post = $request->re_post!=''?$request->re_post:0;
      $newsSaved->news_type = $request->news_type;
      $newsSaved->image = $request->image;
      $newsSaved->date = $request->date;
      $newsSaved->meta_title = $request->meta_title;
      $newsSaved->slug = $request->slug;
      $newsSaved->meta_description = $request->meta_description;

      if ($request->related_to=="company") {
        $model->company_id = $request->rel_id;
      }
      if ($request->related_to=="member") {
        $model_contact = Contact::where(['id'=>$request->rel_id])->first();
        $model->company_id = $model_contact->main_company_id;
      }

      if ($newsSaved->save()) {
        return redirect('news')->with('success', __('news_lang::news-lang.updated'));
      }

    }

    return view('news::news.create',['model'=>$model]);
  }

  public function findModel($id)
  {
    return News::where('id', $id)->first();
  }

  public function getRelToMemCom(Request $request)
  {
    $data_key =  $request->post('data')['data_key'];
    $rel_id = $request->post('data')['rel_id'];

    // dd($data_key);


    if ($data_key=='member') {
      $users = User::get()->toArray();
      $html='<option value="" selected="selected">Select</option>';
      foreach ($users as $key => $user) {
        if ($rel_id == $user['id']) {
          $html.='<option value="'.$user['id'].'" selected>'.$user['name'].'.</option>';
        }else{
          $html.='<option value="'.$user['id'].'">'.$user['name'].'.</option>';
        }
      }
      return $html;

    }
    if ($data_key=='company') {
      $companies = Company::get()->toArray();
      $html='<option value="" selected="selected">Select</option>';
      foreach ($companies as $key => $company) {
        if ($rel_id ==$company['id']) {
          $html.='<option value="'.$company['id'].'" selected>'.$company['title'].'.</option>';
        }else{
          $html.='<option value="'.$company['id'].'">'.$company['title'].'.</option>';
        }
      }
      return $html;
    }
  }



























  public function News(Request $request)
  {
    // dd("here");
    $title = "News";
    $model = '';
    $totalNewsPerYear = '';

    $type='';
    $month='';
    $year='';

    if ($request->has('type')) {
      $type = $request->input('type');
    }

    if ($request->has('category')) {
      $type = $request->input('category');
    }

    if ($request->has('month')) {
      $month = $request->input('month');
    }

    if ($request->has('year')) {
      $year = $request->input('year');
    }

    // dd($type);

    $news = $this->newModel()::where(['isPublished'=>1])

    ->when($type, function ($query, $type){
      return $query->where('category', $type);
    })

    ->when($month, function ($query, $month){
      return $query->whereRaw("MONTH(date) = $month");
    })

    ->when($year, function ($query, $year){
      return $query->whereRaw("YEAR(date) = $year");
    })

    ->orderBy('created_at', 'ASC')
    ->paginate(10);

    // dd($news);

    if (Auth::check()) {
      $model = Contact::where(['id'=>Auth::user()->id])->first();

      $totalNewsPerYear = News::whereIn('created_by', function($query) use ($model){
        $query->select('user_id')
        ->from(with(new SubscriptionUsers)->getTable())
        ->where('subscription_id', $model->active_subscription_id)
        ->whereBetween('date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
      })->orderBy('id', 'desc')->count();
    }



    return view('frontend-views::news.index',compact('news','title','type','month','year','model','totalNewsPerYear'));
  }



  public function NewsDetails($id='')
  {
    $news =  $this->findModel($id);
    return view('frontend-views::news.news_detail',compact('news'));
  }

  public function SubscribeNewsletter(Request $request)
  {
    // dd("hello");
    $model = new Prospect;

    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'email' => ['required', 'string', 'email', 'max:255', 'unique:prospect'],
        'phone' => 'required|integer',
      ]);

      $model = new Prospect;
      $model->full_name = $request->first_name.' '.$request->last_name;
      $model->email = $request->email;
      $model->phone = $request->phone;

      if ($model->save()) {
        if(function_exists('subscribeUser')){
          subscribeUser($model);
        }
        return redirect('/');
      }

    }
    return view('frontend-views::news.subscribe',compact('model'));

  }

  public function AddNews(Request $request)
  {
    // dd($request->all());
    $model = new News;

    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|string',
        'category' => 'required|integer',
        'description' => 'required|string',
        'isPublished' => 'required|integer',
        'homepage_show' => 'required|integer',
      ]);

      $model = new News;
      $model->title = $request->title;
      $model->category = $request->category;
      $model->link = $request->link;
      $model->sort_order = $request->sort_order;
      $model->isPublished = $request->isPublished;
      $model->homepage_show = $request->homepage_show;
      $model->related_to = 'member';
      $model->rel_id = Auth::user()->id;
      $model->short_description = $request->short_description;
      $model->description = $request->description;
      $model->re_post = $request->re_post!=''?$request->re_post:0;

      $model->news_type = $request->news_type;
      $model->image = $request->image;
      $model->date = $request->date;
      $model->created_by = Auth::user()->id;


      $Newmodel = Contact::where(['id'=>Auth::user()->id])->first();
      $model->company_id = $Newmodel->main_company_id;


      if ($model->save()) {
        return redirect('GetMyCompanyNews');
      }

    }
    return view('frontend-views::news.add_news_form',compact('model'));
  }


  public function GetMyCompanyNews(Request $request)
  {
    $model = Contact::where(['id'=>Auth::user()->id])->first();
    // dd($model->active_subscription_id);

    $results = News::whereIn('created_by', function($query) use ($model){
      $query->select('user_id')
      ->from(with(new SubscriptionUsers)->getTable())
      ->where('subscription_id', $model->active_subscription_id);
    })->orderBy('id', 'desc')->paginate(15);

    $totalNewsPerYear = News::whereIn('created_by', function($query) use ($model){
      $query->select('user_id')
      ->from(with(new SubscriptionUsers)->getTable())
      ->where('subscription_id', $model->active_subscription_id)
      ->whereBetween('date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    })->orderBy('id', 'desc')->count();

    // dd($totalNewsPerYear);

    // SELECT * FROM `news` WHERE created_by in (select user_id from subscription_users where subscription_id=3);

    // $results = $this->newModel()::where(['created_by'=>$model->id])->orWhere('company_id', $model->main_company_id)->paginate(15);
    // dd($results);

    return view('frontend-views::news.my_company_news',compact('model','results','totalNewsPerYear'));
  }

  public function MyNewsEdit(Request $request, $id)
  {
    // dd($id);
    $model=News::where('id',$id)->first();

    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|string',
        'category' => 'required|integer',
        'description' => 'required|string',
        'isPublished' => 'required|integer',
        'homepage_show' => 'required|integer',
      ]);

      $model = News::where('id',$id)->first();
      $model->title = $request->title;
      $model->category = $request->category;
      $model->link = $request->link;
      $model->sort_order = $request->sort_order;
      $model->isPublished = $request->isPublished;
      $model->homepage_show = $request->homepage_show;
      $model->related_to = 'member';
      $model->rel_id = Auth::user()->id;
      $model->short_description = $request->short_description;
      $model->description = $request->description;
      $model->re_post = $request->re_post!=''?$request->re_post:0;

      $model->news_type = $request->news_type;
      $model->image = $request->image;
      $model->date = $request->date;
      $model->updated_by = Auth::user()->id;


      $Newmodel = Contact::where(['id'=>Auth::user()->id])->first();
      $model->company_id = $Newmodel->main_company_id;

      if ($model->save()) {
        return redirect('GetMyCompanyNews');
      }

    }

    return view('frontend-views::news.add_news_form',compact('model'));
  }

  public function MyNewsDelete($id)
  {
    $model = $this->findModel($id);
    if ($model->delete()) {
      return true;
    }
  }

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->integer('category')->nullable();
            $table->text('short_description')->nullable();
            $table->text('image')->nullable();
            $table->text('description')->nullable();
            $table->string('news_type')->nullable();
            $table->string('link')->nullable();
            $table->tinyInteger('isPublished')->default(1)->nullable();
            $table->tinyInteger('homepage_show')->default(1)->nullable();
            $table->tinyInteger('sort_order')->nullable();
            $table->tinyInteger('re_post')->nullable();
            $table->string('related_to')->nullable();
            $table->integer('rel_id')->nullable();
            $table->timestamp('date')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('slug')->nullable();
            $table->text('meta_description')->nullable();

            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}

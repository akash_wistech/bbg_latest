<?php
namespace NaeemAwan\DynamicFormsGenerator\Observers;

use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormData;
use Illuminate\Support\Facades\DB;

class DynamicFormDataObserver
{
  /**
  * Handle the DynamicFormData "created" event.
  *
  * @param  \NaeemAwan\DynamicFormDatas\Models\DynamicFormData  $dynamicFormData
  * @return void
  */
  public function created(DynamicFormData $dynamicFormData)
  {
    if ($dynamicFormData->id) {
      $reference='DFD'.'-'.date('y').'-'.sprintf('%03d', $dynamicFormData->id);

      DB::table($dynamicFormData->getTable())
      ->where('id', $dynamicFormData->id)
      ->update(['reference_no' => $reference]);
    }
  }

  /**
  * Handle the DynamicFormData "updated" event.
  *
  * @param  \App\Models\DynamicFormData  $dynamicFormData
  * @return void
  */
  public function updated(DynamicFormData $dynamicFormData)
  {
    //
  }

  /**
  * Handle the DynamicFormData "deleted" event.
  *
  * @param  \App\Models\DynamicFormData  $dynamicFormData
  * @return void
  */
  public function deleted(DynamicFormData $dynamicFormData)
  {
    //
  }

  /**
  * Handle the DynamicFormData "restored" event.
  *
  * @param  \App\Models\DynamicFormData  $dynamicFormData
  * @return void
  */
  public function restored(DynamicFormData $dynamicFormData)
  {
    //
  }

  /**
  * Handle the DynamicFormData "force deleted" event.
  *
  * @param  \App\Models\DynamicFormData  $dynamicFormData
  * @return void
  */
  public function forceDeleted(DynamicFormData $dynamicFormData)
  {
    //
  }
}

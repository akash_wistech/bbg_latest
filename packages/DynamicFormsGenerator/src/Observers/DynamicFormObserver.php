<?php
namespace NaeemAwan\DynamicFormsGenerator\Observers;

use NaeemAwan\DynamicFormsGenerator\Models\DynamicForms;
use Illuminate\Support\Facades\DB;

class DynamicFormObserver
{
  /**
  * Handle the DynamicForms "created" event.
  *
  * @param  \NaeemAwan\DynamicFormss\Models\DynamicForms  $dynamicForm
  * @return void
  */
  public function created(DynamicForms $dynamicForm)
  {
    if ($dynamicForm->id) {
      $reference='DF'.'-'.date('y').'-'.sprintf('%03d', $dynamicForm->id);

      DB::table($dynamicForm->getTable())
      ->where('id', $dynamicForm->id)
      ->update(['reference_no' => $reference]);
    }
  }

  /**
  * Handle the DynamicForms "updated" event.
  *
  * @param  \App\Models\DynamicForms  $dynamicForm
  * @return void
  */
  public function updated(DynamicForms $dynamicForm)
  {
    //
  }

  /**
  * Handle the DynamicForms "deleted" event.
  *
  * @param  \App\Models\DynamicForms  $dynamicForm
  * @return void
  */
  public function deleted(DynamicForms $dynamicForm)
  {
    //
  }

  /**
  * Handle the DynamicForms "restored" event.
  *
  * @param  \App\Models\DynamicForms  $dynamicForm
  * @return void
  */
  public function restored(DynamicForms $dynamicForm)
  {
    //
  }

  /**
  * Handle the DynamicForms "force deleted" event.
  *
  * @param  \App\Models\DynamicForms  $dynamicForm
  * @return void
  */
  public function forceDeleted(DynamicForms $dynamicForm)
  {
    //
  }
}

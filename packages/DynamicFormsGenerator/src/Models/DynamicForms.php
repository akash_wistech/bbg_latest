<?php
namespace NaeemAwan\DynamicFormsGenerator\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use Illuminate\Support\Facades\DB;
use App\Traits\Sluggable;

class DynamicForms extends FullTables
{
  use HasFactory, Blameable, Sluggable, SoftDeletes;
  public $selected_field,$manager_id;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'dynamic_forms';
  public $moduleTypeId = 'dynamic_forms';

  protected $fillable = [
    'reference_no',
    'title',
  ];

  public function getSluggableString()
  {
      return $this->title;
  }

  public function getFullTitleAttribute()
  {
    return "{$this->title}";
  }
}

<?php
namespace NaeemAwan\DynamicFormsGenerator\Models;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Traits\Blameable;

class DynamicFormData extends Model
{
  use Blameable;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'dynamic_form_data';
  public $moduleTypeId = 'dynamic_form_data';

  protected $fillable = [
    'form_id',
  ];
}

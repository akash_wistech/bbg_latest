<?php
namespace NaeemAwan\DynamicFormsGenerator\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use Illuminate\Support\Facades\DB;

class DynamicFormFields extends FullTables
{
  use HasFactory, Blameable, SoftDeletes;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'dynamic_form_fields';

  protected $fillable = [
    'form_id',
    'field_type',
    'custom_field_id',
    'input_type',
    'title',
    'short_title',
    'is_required',
    'show_in_grid',
    'predefined_list_id',
    'col_width',
    'selection_type',
    'hint_text',
    'descp',
    'sort_order',
    'adv_filter',
    'enable_stats',
  ];
}

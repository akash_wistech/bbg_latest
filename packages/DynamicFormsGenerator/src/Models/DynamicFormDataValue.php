<?php
namespace NaeemAwan\DynamicFormsGenerator\Models;

use App;
use App\Models\ChildTables;
use Illuminate\Support\Facades\DB;

class DynamicFormDataValue extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'dynamic_form_data_value';

  protected $fillable = [
    'form_id',
    'form_field_id',
    'input_value',
    'input_value_id',
  ];
}

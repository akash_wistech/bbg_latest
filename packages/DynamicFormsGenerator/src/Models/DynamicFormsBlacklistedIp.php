<?php
namespace NaeemAwan\DynamicFormsGenerator\Models;

use App;
use App\Models\ChildTables;
use Illuminate\Support\Facades\DB;

class DynamicFormsBlacklistedIp extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'dynamic_forms_blacklisted_ip';

  protected $fillable = [
    'form_id',
    'ip',
  ];
}

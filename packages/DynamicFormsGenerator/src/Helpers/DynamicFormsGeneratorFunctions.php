<?php
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormFields;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormData;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormDataValue;
use Illuminate\Support\Facades\Log;
use App\Models\SysNotifications;

if (!function_exists('saveSelectedFormFields')) {
  /**
  * return status array
  */
  function saveSelectedFormFields($model,$request)
  {
    $formFields = getLeadFormFields();
    $selectedFields = $request->input('selected_field');
    // echo '<pre>';
    // print_r($selectedFields);
    // echo '</pre>';
    $frmSavedIds=[];
    if($selectedFields!=null){
      foreach($selectedFields as $key=>$val){
        list($ftype,$rawFid)=explode("-",$val);
        if($ftype=='dynamic'){
          $fid = str_replace("input_field[","",$rawFid);
          $fid = str_replace("]","",$fid);
          $customFieldId = $fid;
          $fid = 'input_field-'.$fid;
        }else{
          $fid = $rawFid;
          $customFieldId = 0;
        }
        // echo $ftype.' => '.$fid;

        $thisFieldItem = $formFields[$fid];
        // echo '<pre>';print_r($thisFieldItem);echo '</pre>';

        $row = DynamicFormFields::where('form_id',$model->id)->where('field_type',$ftype)
        ->where('field_full_name',$val)->where('custom_field_id',$fid)->first();
        if($row==null){
          $row = new DynamicFormFields;
          $row->form_id = $model->id;
          $row->field_type = $ftype;
          $row->field_full_name = $val;
        }
        $row->field_name = $rawFid;
        $row->custom_field_id = $customFieldId;
        $row->input_type = $thisFieldItem['input_type'];
        $row->title = $thisFieldItem['title'];
        $row->short_title = isset($thisFieldItem['short_title']) ? $thisFieldItem['short_title'] : $thisFieldItem['title'];
        $row->is_required = $thisFieldItem['required'];
        $row->show_in_grid = isset($thisFieldItem['show_in_grid']) ? $thisFieldItem['show_in_grid'] : 0;
        $row->predefined_list_id = isset($thisFieldItem['predefined_list_id']) ? $thisFieldItem['predefined_list_id'] : 0;
        $row->col_width = $thisFieldItem['colwidth'];
        $row->selection_type = isset($thisFieldItem['selection_type']) ? $thisFieldItem['selection_type'] : 1;
        $row->hint_text = isset($thisFieldItem['hint_text']) ? $thisFieldItem['hint_text'] : '';
        $row->descp = isset($thisFieldItem['descp']) ? $thisFieldItem['descp'] : '';
        $row->save();
        $frmSavedIds[]=$row->id;
        // echo '<br />';
      }
    }
    if($frmSavedIds!=null){
      DynamicFormFields::where([
        'form_id'=>$model->id,
      ])
      ->whereNotIn('id',$frmSavedIds)->delete();
    }
  }
}

if (!function_exists('getFFSelection')) {
  /**
  * return checked str for selection
  */
  function getFFSelection($model,$formField)
  {
    $str='';
    $row = DynamicFormFields::where('form_id',$model->id)->where('field_type',$formField['ft'])
    ->where('field_full_name',$formField['ft'].'-'.$formField['name'])->first();
    if($row!=null){
      $str=' checked="checked"';
    }
    return $str;
  }
}

if (!function_exists('getDynamicFormSubmissionCount')) {
  /**
  * return checked str for selection
  */
  function getDynamicFormSubmissionCount($id)
  {
    $totalInquiries = DynamicFormData::where('form_id',$id)->count('id');
    $pendingInquiries = DynamicFormData::where(['form_id'=>$id,'status'=>0])->count('id');
    $convertedInquiries = DynamicFormData::where(['form_id'=>$id,'status'=>[1,2]])->count('id');
    $spamInquiries = DynamicFormData::where(['form_id'=>$id,'status'=>10])->count('id');
    $str = '<a href="'.url('/dynamic-forms/view/'.$id).'" class="label label-rounded label-primary mr-2" data-toggle="tooltip" title="'.__('common.total').'">'.$totalInquiries.'</a>';
    $str.= '<a href="'.url('/dynamic-forms/view/'.$id).'?s=0" class="label label-rounded label-info mr-2" data-toggle="tooltip" title="'.__('common.pending').'">'.$pendingInquiries.'</a>';
    $str.= '<a href="'.url('/dynamic-forms/view/'.$id).'?s=1,2" class="label label-rounded label-success mr-2" data-toggle="tooltip" title="'.__('common.converted').'">'.$convertedInquiries.'</a>';
    $str.= '<a href="'.url('/dynamic-forms/view/'.$id).'?s=10" class="label label-rounded label-danger mr-2" data-toggle="tooltip" title="'.__('common.spam').'">'.$spamInquiries.'</a>';
    return $str;
  }
}

if (!function_exists('getDynamicFormGridFields')) {
  /**
  * return dynamic form fields
  */
  function getDynamicFormGridFields($id)
  {
    return DynamicFormFields::where('form_id',$id)->where('show_in_grid',1)->get();
  }
}

if (!function_exists('getDynamicFormFields')) {
  /**
  * return dynamic form fields
  */
  function getDynamicFormFields($id)
  {
    return DynamicFormFields::where('form_id',$id)->get();
  }
}

if (!function_exists('getIncomingIpCount')) {
  /**
  * return count for a specific incoming ip
  */
  function getIncomingIpCount($formid,$ip)
  {
    $countSpam = DynamicFormData::where('ip',$ip)->where('form_id',$formid)->where('status',10)->count('id');

    return $countSpam>0 ? (' <a href="'.url('/incoming-data/auto-spam',['formid'=>$formid,'ip'=>$ip]).'" class="label label-warning label-inline font-weight-normal act-confirmation" data-confirmationmsg="'.__('common.automarkspamconfirmation').'" data-pjxcntr="grid-table">'.(__('multiformgen::dynamicform.auto_spam')).'</a>') : '' ;
  }
}

if (!function_exists('getDFInquiryCount')) {
  /**
  * return count for inquiries based on status
  */
  function getDFInquiryCount($status,$formid)
  {
    return DynamicFormData::where('form_id',$formid)->whereIn('status',$status)->count('id');
  }
}

if (!function_exists('getDynamicFormDataValue')) {
  /**
  * return saved value for a column
  */
  function getDynamicFormDataValue($formid,$submissionId,$fieldId)
  {
    $value = '';
    $row = DynamicFormDataValue::where(['form_id'=>$formid,'form_data_id'=>$submissionId,'form_field_id'=>$fieldId])->first();
    if($row!=null){
      $value = $row->input_value;
    }
    return $value;
  }
}

if (!function_exists('updateDFNotifications')) {
  /**
  * return saved value for a column
  */
  function updateDFNotifications($formid)
  {
    $pendingInquiries = DynamicFormData::where(['form_id'=>$formid,'status'=>0])->count('id');
    if($pendingInquiries<=0){
      SysNotifications::where('url','dynamic-forms/view/'.$formid.'?s=0')->delete();
    }
  }
}

if (!function_exists('saveDynamicFormData')) {
  /**
  * save dynamic form data
  */
  function saveDynamicFormData($dynamicForm,$dynamicFormData,$request)
  {
    // Log::debug($request->all());
    $dynamicFormFields = DynamicFormFields::where('form_id',$dynamicForm->id)->get();
    if($dynamicFormFields!=null){
      foreach($dynamicFormFields as $dynamicFormField){
        $inputValueId=0;
        $inputValue='';
        //Static Fields Data
        if($dynamicFormField['field_type']=='static' && $request->has($dynamicFormField['field_name'])){

          $inputValue = $request->input($dynamicFormField['field_name']);

          if($dynamicFormField['field_name']=='service_type'){
            $inputValueId = $inputValue;
            $serviceItem = getPredefinedItem($inputValue);
            if($serviceItem!=null){
              $inputValue = $serviceItem->lang->title;
            }
          }
        }

        //Dynamic Fields Data
        if($dynamicFormField['field_type']=='dynamic' && $request->has('input_field')){
          $inputFieldValue = $request->input('input_field');

          $inputValueId=0;
          $inputValue = $inputFieldValue[$dynamicFormField['custom_field_id']];

          if(in_array($dynamicFormField->input_type,['select','checkbox','radio'])){
            if(is_array($inputValue)){

            }else{
              $inputValueId = $inputValue;
              $inputValue = getOptionTitleById($inputValueId,$dynamicFormField);
            }
          }
        }

        if($inputValue!=''){
          $row = new DynamicFormDataValue;
          $row->form_id=$dynamicFormData->form_id;
          $row->form_data_id=$dynamicFormData->id;
          $row->form_field_id=$dynamicFormField->id;
          $row->input_value=$inputValue;
          $row->input_value_id=$inputValueId;
          $row->save();
        }
      }
    }
    return true;
  }
}

if (!function_exists('dfInputTypes')) {
  /**
  * return status array
  */
  function dfInputTypes()
  {
    return [
      ['type'=>'text', 'title' => 'Text Field', 'icon'=>'icon-text'],
      ['type'=>'textarea', 'title' => 'Text Area', 'icon'=>'icon-textarea'],
      ['type'=>'select', 'title' => 'Drop Down', 'icon'=>'icon-select'],
      ['type'=>'checkbox', 'title' => 'Checkbox', 'icon'=>'icon-checkbox-group'],
      ['type'=>'radio', 'title' => 'Radio Buttons', 'icon'=>'icon-radio-group'],
      ['type'=>'numberinput', 'title' => 'Number Input', 'icon'=>'icon-number'],
      ['type'=>'websiteinput', 'title' => 'Url Input', 'icon'=>'icon2-link'],
      ['type'=>'emailinput', 'title' => 'Email Input', 'icon'=>'icon2-envelop'],
      ['type'=>'date', 'title' => 'Date Input', 'icon'=>'icon-date'],
    ];
  }
}

if (!function_exists('loadIncomingDataToModel')) {
  /**
  * Loads incoming data to form fields
  */
  function loadIncomingDataToModel($model,$dynamicForm,$idid)
  {
    if($dynamicForm!=null){
      $model->title = 'Lead from '.$dynamicForm->title;
      $dynamicFormFields = getDynamicFormFields($dynamicForm->id);
      if($dynamicFormFields!=null){
        foreach($dynamicFormFields as $dynamicFormField){
          $incomingDataRow = DynamicFormDataValue::where(['form_id'=>$dynamicForm->id,'form_data_id'=>$idid,'form_field_id'=>$dynamicFormField->id])->first();
          if($incomingDataRow!=null){
            //Assigning static fields
            if($dynamicFormField->field_type=='static'){
              if($dynamicFormField->field_name=='service_type'){
                $model->service_type=$incomingDataRow->input_value_id;
              }else{
                $model->{$dynamicFormField->field_name}=$incomingDataRow->input_value;
              }
            }
            //Assigning dynamic fields
            if($dynamicFormField->field_type=='dynamic'){
              if($dynamicFormField->input_type=='select' || $dynamicFormField->input_type=='radio'){
                $model->input_field[$dynamicFormField->custom_field_id]=$incomingDataRow->input_value_id;
              }else{
                $model->input_field[$dynamicFormField->custom_field_id]=$incomingDataRow->input_value;
              }
            }
          }
        }
      }
    }
  }
}

if (!function_exists('getLeadFormFields')) {
  /**
  * return status array
  */
  function getLeadFormFields()
  {
    $leadFields=[];
    $leadFields['full_name']=['ft'=>'static','id'=>'full_name','name'=>'full_name','show_in_grid'=>1,'input_type'=>'text','title'=>__('common.name'),'required'=>true,'colwidth'=>'6'];
    $leadFields['company_name']=['ft'=>'static','id'=>'company_name','name'=>'company_name','input_type'=>'text','title'=>__('common.company_name'),'required'=>true,'colwidth'=>'6'];
    $leadFields['email']=['ft'=>'static','id'=>'email','name'=>'email','show_in_grid'=>1,'input_type'=>'emailinput','title'=>__('common.email'),'required'=>true,'colwidth'=>'6'];
    $leadFields['phone']=['ft'=>'static','id'=>'phone','name'=>'phone','show_in_grid'=>1,'input_type'=>'numberinput','title'=>__('common.phone'),'required'=>true,'colwidth'=>'6'];
    $leadFields['title']=['ft'=>'static','id'=>'title','name'=>'title','input_type'=>'text','title'=>__('common.title'),'required'=>false,'colwidth'=>'6'];
    $servicesList = getPredefinedListOptionsArr(getSetting('service_list_id'));
    $leadFields['service_type']=['ft'=>'static','id'=>'service_type','name'=>'service_type','input_type'=>'select','title'=>__('common.service_type'),'required'=>true,'predefined_list_id'=>0,'colwidth'=>'6','subOptions'=>$servicesList];
    $leadFields['expected_close_date']=['ft'=>'static','id'=>'expected_close_date','name'=>'expected_close_date','input_type'=>'date','title'=>__('common.expected_close_date'),'required'=>false,'colwidth'=>'6'];
    $leadFields['quote_amount']=['ft'=>'static','id'=>'quote_amount','name'=>'quote_amount','input_type'=>'numberinput','title'=>__('common.quote_amount'),'required'=>false,'colwidth'=>'6'];
    $leadFields['descp']=['ft'=>'static','id'=>'descp','name'=>'descp','input_type'=>'textarea','title'=>__('common.descp'),'required'=>true,'colwidth'=>'12'];
    $customFields = getCustomFieldsByModule(['opportunity','prospect']);
    if($customFields!=null){
      foreach($customFields as $customField){
        $customFieldSubOptions=[];
        if(in_array($customField->input_type,['select','checkbox','radio'])){
          if($customField->predefined_list_id > 0){
            $customFieldSubOptions = getPredefinedList($customField->predefined_list_id);
          }else{
            $customFieldSubOptions = getMultipleOptions($customField->id);
          }
        }
        $leadFields['input_field-'.$customField['id']]=[
          'ft'=>'dynamic','did'=>$customField['id'],'id'=>'input_field-'.$customField['id'],'name'=>'input_field['.$customField['id'].']',
          'short_title'=>$customField['short_title'],'input_type'=>$customField['input_type'],'predefined_list_id'=>$customField['predefined_list_id'],
          'title'=>$customField['title'],'required'=>($customField['is_required']==1 ? true : false),
          'colwidth'=>$customField['colum_width'],'show_in_grid'=>$customField['show_in_grid'],'subOptions'=>$customFieldSubOptions
        ];
      }
    }
    return $leadFields;
  }
}

if (!function_exists('generateJsCode')) {
  /**
  * return js code for embeded form
  */
  function generateJsCode($formId,$formFields)
  {
    $jsCode = "var jsCode = '';\n";
    // $jsCode = "jsCode = '<script>';\n";

    $jsCode.= "jsCode+= 'var dffsSpinner = $(\"#dffsloader\");';\n";
    $jsCode.= "jsCode+= '$(document).ready(function() {';\n";

    $jsCode.= "jsCode+= '$.getJSON(\"https://api.ipify.org/?format=json\", function(e) {';\n";
    $jsCode.= "jsCode+= '$(\"#dfuip\").val(e.ip);';\n";
    $jsCode.= "jsCode+= '});';\n";

    $jsCode.= "jsCode+= '$(\"body\").on(\"submit\", \"form#".$formId."\", function (e) {';\n";
    $jsCode.= "jsCode+= 'e.preventDefault();';\n";
    $jsCode.= "jsCode+= 'frm = $(this);';\n";
    $jsCode.= "jsCode+= 'frmId = frm.attr(\"id\");';\n";

    $jsCode.= "jsCode+= '$(\"#\"+frmId+\"-msg-container\").html(\"\");';\n";

    $jsCode.= "jsCode+= 'var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;';\n";


    $jsCode.= "jsCode+= '$(\"#\"+frmId+\" .isrequired\").each(function () {';\n";
    $jsCode.= "jsCode+= 'if($(this).val()==\"\"){';\n";
    $jsCode.= "jsCode+= 'alert(\"Please fill all required fields\");';\n";
    $jsCode.= "jsCode+= 'return false;';\n";
    $jsCode.= "jsCode+= '}';\n";
    $jsCode.= "jsCode+= '});';\n";


    foreach($formFields as $formField){
      if($formField['input_type']=='emailinput'){
        $jsCode.= "jsCode+= 'inputVal = frm.find(\"#".$formField['id']."\").val();';\n";
        $jsCode.= "jsCode+= 'if (testEmail.test(inputVal)){';\n";
        $jsCode.= "jsCode+= '}else{';\n";
        $jsCode.= "jsCode+= 'alert(\"Please provide valid email address\");';\n";
        $jsCode.= "jsCode+= 'return false;';\n";
        $jsCode.= "jsCode+= '}';\n";
      }
    }
    $jsCode.= "jsCode+= 'dffsSpinner.show();';\n";
    $jsCode.= "jsCode+= 'frm.find(\"input[type=submit]\").attr(\"disabled\",true);';\n";
    $jsCode.= "jsCode+= 'var formData = new FormData(frm[0]);';\n";
    $jsCode.= "jsCode+= '$.ajax({';\n";
    $jsCode.= "jsCode+= 'url: frm.attr(\"action\"),';\n";
    $jsCode.= "jsCode+= 'type: \"POST\",';\n";
    $jsCode.= "jsCode+= 'data: formData,';\n";
    $jsCode.= "jsCode+= 'cache: false,';\n";
    $jsCode.= "jsCode+= 'contentType: false,';\n";
    $jsCode.= "jsCode+= 'processData: false,';\n";
    $jsCode.= "jsCode+= 'success: function (response) {';\n";
    $jsCode.= "jsCode+= 'dffsSpinner.hide();';\n";
    $jsCode.= "jsCode+= 'frm.find(\"input[type=submit]\").attr(\"disabled\",false);';\n";
    $jsCode.= "jsCode+= 'msgHtml=\"\";';\n";
    $jsCode.= "jsCode+= 'if(response[\"success\"]){';\n";
    $jsCode.= "jsCode+= 'frm.trigger(\"reset\");';\n";
    $jsCode.= "jsCode+= 'msgHtml = \'<div class=\"alert alert-success\">\'+response[\"success\"][\"msg\"]+\'</div>\';';\n";
    $jsCode.= "jsCode+= '}else{';\n";
    $jsCode.= "jsCode+= 'msgHtml = \'<div class=\"alert alert-danger\">An error occured while performing the action, Please try again in a while.</div>\';';\n";
    $jsCode.= "jsCode+= '}';\n";
    $jsCode.= "jsCode+= 'if(msgHtml!=\"\"){';\n";
    $jsCode.= "jsCode+= '$(\"#\"+frmId+\"-msg-container\").html(msgHtml).show();';\n";
    // $jsCode.= "jsCode+= '$(\"html, body\").animate({';\n";
    // $jsCode.= "jsCode+= 'scrollTop: $(\"#\"+frmId+\"-msg-container\").offset().top';\n";
    // $jsCode.= "jsCode+= '}, 2000);';\n";
    $jsCode.= "jsCode+= '}';\n";
    $jsCode.= "jsCode+= '},';\n";
    $jsCode.= "jsCode+= 'error: function(xhr, ajaxOptions, thrownError){';\n";
    $jsCode.= "jsCode+= 'alert(xhr.responseText);';\n";
    $jsCode.= "jsCode+= '}';\n";
    $jsCode.= "jsCode+= '});';\n";
    $jsCode.= "jsCode+= 'return false;';\n";
    $jsCode.= "jsCode+= '});';\n";
    $jsCode.= "jsCode+= '});';\n";
    // $jsCode.= "jsCode+= '</script>';";
    return $jsCode;
  }
}

if (!function_exists('searchInDFDataFields')) {
  /**
  * return status array
  */
  function searchInDFDataFields($moduleTypeId,$query,$keyword,$srchFlds)
  {
    $customFields = getCustomFieldsByModule($moduleTypeId);
    $query->where(function($query) use ($customFields,$moduleTypeId,$keyword,$srchFlds){
      if($srchFlds!=null){
        $nn=1;
        foreach($srchFlds as $key=>$val){
          if($nn==1){
            $query->orWhere($val,'like','%'.$keyword.'%');
          }else{
            $query->orWhere($val,'like','%'.$keyword.'%');
          }
          $nn++;
        }
      }
      if($moduleTypeId=='opportunity'){
        //Search In Prospects
        $moduleSourceNameFieldArr = getModuleSourceNameField();
        $oppSourceModulesArr = getModulesHavingOpportunity();
        if($oppSourceModulesArr!=null){
          foreach($oppSourceModulesArr as $key=>$val){
            $nameCol = $moduleSourceNameFieldArr[$val];
            $subQuerymodelClass = getModelClass()[$val];
            $query->orWhere(function($query) use ($val, $subQuerymodelClass, $keyword, $nameCol){
              $query->where('module_type',$val)
              ->whereIn('module_id',function($query) use ($val, $subQuerymodelClass, $keyword, $nameCol){

                $query->from((new $subQuerymodelClass)->getTable())
                ->selectRaw('id')
                ->where($nameCol, 'like', '%'.$keyword.'%')
                ->orWhere('email', 'like', '%'.$keyword.'%')
                ->orWhere('phone', 'like', '%'.$keyword.'%');
                if($val!='contact'){
                  $query->orWhere('company_name', 'like', '%'.$keyword.'%');
                }
              });
            });
          }
        }
      }

      if($customFields!=null){
        foreach($customFields as $customField){
          $query->orWhereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){
            if(($customField->input_type=='select' && $customField->selection_type==2) || $customField->input_type=='checkbox' || $customField->input_type=='radio'){
              $query->from((new CustomFieldDataMultiple)->getTable())
              ->selectRaw('module_id')
              ->where('module_type',$moduleTypeId)
              ->where('input_field_id',$customField->id)
              ->where('option_id_value', 'like', '%'.$keyword.'%');
            }else{
              $query->from((new CustomFieldData)->getTable())
              ->selectRaw('module_id')
              ->where('module_type',$moduleTypeId)
              ->where('input_field_id',$customField->id)
              ->where(function ($query) use ($keyword){
                $query->where('input_value', 'like', '%'.$keyword.'%')
                ->orWhere('input_org_value', 'like', '%'.$keyword.'%');
              });
            }
          });
        }
      }
    });
  }
}

<?php
namespace NaeemAwan\DynamicFormsGenerator\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Event;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Rules\DuplicateCheck;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicForms;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormData;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormsBlacklistedIp;
use App\Models\Opportunity;

class IncomingDataController extends Controller
{
  public $folderName='dynamic-forms';
  public $controllerId='dynamicforms';


  /**
  * returns data for submission datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function submissionDatatableData(Request $request,$id)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    $folderName = $this->folderName;
    $s = $request->s;
    $query = DB::table($this->newModel()->getTable())->where(['form_id'=>$id])->whereIn('status',explode(",",$s));

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='referral_url';
      $srchFlds[]='ip';
      searchInDFDataFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->name!='')$query->where('referral_url','like','%'.$request->name.'%');
    if($request->name!='')$query->where('ip','like','%'.$request->name.'%');

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();

    $gridViewColumns = getDynamicFormGridFields($id);

    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $colValue=getDynamicFormDataValue($id,$model->id,$gridViewColumn['id']);
            $thisData['col_'.$gridViewColumn['id']]=$colValue;
          }
        }
        $thisData['referral_url']=$model->referral_url;
        $ipInfo = $model->ip;
        if($s!=10)$ipInfo.=getIncomingIpCount($id,$model->ip);
        $thisData['ip']=$ipInfo;

        $thisData['created_at']=formatDateTime($model->created_at);

        $actBtns=[];
          $actBtns[]='
          <a href="'.url('/'.$folderName.'/view-submission',['id'=>$model->id,'formid'=>$id]).'" class="btn btn-sm btn-clean btn-icon view poplink" data-heading="'.$model->reference_no.'" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        if($s==0){
          $actBtns[]='
          <a href="'.url('/incoming-data/lead-form',['formid'=>$id,'idid'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon convert" data-toggle="tooltip" title="'.__('common.convert').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-paper-plane"></i>
          </a>';
          $actBtns[]='
          <a href="'.url('/incoming-data/mark-spam',['id'=>$model->id,'formid'=>$id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('multiformgen::dynamicform.mark_spam').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.markspamconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-warning-sign"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Update status of model
  *
  * @return \Illuminate\Http\Response
  */
  public function markSpam(Request $request, $id, $formid)
  {
    $model = $this->findModel($id,$formid);

    DB::table($model->getTable())
    ->where('id', $model->id)
    ->update(['status' => 10,'updated_at'=>date("Y-m-d H:i:s"),'updated_by'=>Auth::user()->id]);

    updateDFNotifications($formid);

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.statusUpdated'),]]);
  }

  /**
  * Update status of model
  *
  * @return \Illuminate\Http\Response
  */
  public function autoMarkSpam(Request $request, $formid, $ip)
  {
    DB::table($this->newModel()->getTable())
    ->where('form_id', $formid)
    ->where('ip', $ip)
    ->update(['status' => 10]);

    $blockedIp = DynamicFormsBlacklistedIp::where('form_id',$formid)->where('ip',$ip)->first();
    if($blockedIp==null){
      $blockedIp=new DynamicFormsBlacklistedIp;
      $blockedIp->form_id=$formid;
      $blockedIp->ip=$ip;
      $blockedIp->save();
    }

    updateDFNotifications($formid);

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('multiformgen::dynamicform.ip_blocked_successfully'),]]);
  }

  /**
  * Convert to lead
  *
  * @return \Illuminate\Http\Response
  */
  public function saveLead(Request $request, $formid, $idid)
  {
    $model = new Opportunity;
    $module='prospect';
    $subModel = getModelClass()[$module];
    $model->subModel=new $subModel;
    $dynamicForm = DynamicForms::where('id',$formid)->first();
    loadIncomingDataToModel($model,$dynamicForm,$idid);

    $di = $request->di ?? 0;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required|string',
        'full_name'=>'required|string',
        'company_name'=>'nullable|string',
        'email'=>'required|string',
        'phone'=>'required|string',
        'service_type'=>'required|integer',
        'expected_close_date'=>'string|nullable',
        'quote_amount'=>'numeric|nullable',
        'descp'=>'string|nullable',
        'duplicatecheck'=>new DuplicateCheck($model->subModel->moduleTypeId,$request,$request->di)
      ]);
      if(!$validator->fails()){
        $diId = 0;
        $crequest=['email'=>$request->email,'phone'=>$request->phone];
        if($di==1){
          $diId = getModuleDuplicateId($model->subModel->moduleTypeId,$crequest);
        }
        if($diId>0){
          $subModel = $subModel::where('id',$diId)->first();
        }else{
          $subModel = new $subModel;
        }

        if($subModel->moduleTypeId=='prospect'){
          $subModel->full_name = $request->full_name;
          $subModel->company_name = $request->company_name;
          $subModel->email = $request->email;
          $subModel->phone = $request->phone;
          $subModel->save();
          foreach($request->input_field as $key=>$val){
            $subModel->input_field[$key]=$val;
          }
          saveCustomField($subModel,$request);
          saveModuleManagerField($subModel,$request);
          $module_id=$subModel->id;
        }

        $model= new Opportunity;
        $model->rec_type = 'l';
        $model->title = $request->title;
        $model->service_type = $request->service_type;
        $model->module_type = $module;
        $model->module_id = $module_id;
        $model->expected_close_date = $request->expected_close_date;
        $model->quote_amount = $request->quote_amount;
        $model->descp = $request->descp;
        if ($model->save()) {
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          saveCustomField($model,$request,'form');
          saveModuleManagerField($model,$request,'form');
          saveWorlflowField($model,$request);

          DB::table((new DynamicFormData)->getTable())
          ->where(['form_id'=>$formid,'id'=>$idid])
          ->update(['status' => 1]);

          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.lead.saved')]]);
          }else{
            return redirect('dynamic-forms/view/'.$formid)->with('success', __('app.lead.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.lead.notsaved'),]]);
          }else{
            return redirect('dynamic-forms/view/'.$formid)->with('error', __('app.lead.notsaved'));
          }
        }
      }else{
        $errors = $validator->errors();
        if($errors->first('duplicatecheck')){
          return redirect('incoming-data/lead-form/'.$formid.'/'.$idid.'?di=1')->withErrors($validator)->withInput($request->all());
        }else{
          return redirect('incoming-data/lead-form/'.$formid.'/'.$idid.'')->withErrors($validator)->withInput($request->all());
        }
      }
    }
    return view('lead.create',compact('model','di'));
  }

  /**
  * Create a submission model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new DynamicFormData;
  }

  /**
  * Find a submission model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id,$formid)
  {
    return DynamicFormData::where('id', $id)->where('form_id',$formid)->first();
  }
}

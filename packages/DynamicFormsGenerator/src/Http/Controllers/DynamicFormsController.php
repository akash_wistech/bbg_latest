<?php
namespace NaeemAwan\DynamicFormsGenerator\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Event;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicForms;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormData;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormsBlacklistedIp;
use App\Models\SysNotifications;

class DynamicFormsController extends Controller
{
  public $folderName='dynamic-forms';
  public $controllerId='dynamicforms';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    $routeId = $this->folderName;
    return view('multiformgen::'.$this->folderName.'.index',compact('moduleTypeId','routeId','controllerId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    $folderName = $this->folderName;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='title';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->name!='')$query->where('title','like','%'.$request->name.'%');
    // if($request->input_field!=''){
    //   advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    // }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;
        $thisData['title']=$model->title;
        $thisData['submissions']=getDynamicFormSubmissionCount($model->id);
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/'.$folderName.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/'.$folderName.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$folderName.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    $dtInputColsArr[]=['data'=>'title','name'=>'title','searchable'=>true,'orderable'=>true];
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * detail view of model
  *
  * @return \Illuminate\Http\Response
  */
  public function show(Request $request,$id)
  {
    $model = $this->findModel($id);
    $routeId = $this->folderName;
    $folderName = $this->folderName;
    $controllerId = $this->controllerId;
    $s = 0;
    if($request->input('s'))$s=$request->s;

    return view('multiformgen::'.$this->folderName.'.view', compact('model','request','controllerId','folderName','routeId','s'));
  }

  /**
  * detail view of model
  *
  * @return \Illuminate\Http\Response
  */
  public function viewSubmission(Request $request,$id,$formid)
  {
    $model = $this->findDataModel($id,$formid);

    return view('multiformgen::'.$this->folderName.'.view-submission', compact('model','id','formid'));
  }

  public function saveRemoteForm(Request $request)
  {
    if($request->has('fid')){
      $dynamicForm = $this->findModel($request->input('fid'));

      header('Access-Control-Allow-Origin: '.$dynamicForm->allowed_domains);

      if($request->input('dfuip')!=null && $request->input('dfuip')!=''){
        $reqStatus = 0;
        $blockedIp=DynamicFormsBlacklistedIp::where(['form_id'=>$dynamicForm->id,'ip'=>$request->input('dfuip')]);
        if($blockedIp->exists()){
          $reqStatus = 10;
        }
        $dynamicFormData = new DynamicFormData;
        $dynamicFormData->form_id = $dynamicForm->id;
        $dynamicFormData->status = $reqStatus;
        $dynamicFormData->referral_url = request()->headers->get('referer');
        $dynamicFormData->ip = $request->input('dfuip');
        $dynamicFormData->save();
        $status = saveDynamicFormData($dynamicForm,$dynamicFormData,$request);

        $replacements=[
          '{formName}' => $dynamicForm->title,
          '{formLink}' => '<a href="'.url('dynamic-forms/view/'.$dynamicForm->id).'" target="_blank">'.__('multiformgen::dynamicform.go_to_form_list').'</a>',
        ];
        $manager_info = ['module_type'=>$dynamicForm->moduleTypeId,'module_id'=>$dynamicForm->id];
        Event::dispatch('notify.dynamic-form.submitted',['model'=>$dynamicFormData,'manager_info'=>$manager_info,'user'=>[],'replacements'=>$replacements]);

        $assignedUsers = getSavedModuleManagerIdz($dynamicForm);
        Log::info($assignedUsers);
        if($assignedUsers!=null){
          $sysNotification = new SysNotifications;
          $sysNotification->rec_type='pending-action';
          $sysNotification->icon='bell';
          $sysNotification->title='You have pending inquiries for '.$dynamicForm->title;
          $sysNotification->url=$this->folderName.'/view/'.$dynamicForm->id.'?s=0';
          $sysNotification->cls_name='danger';
          $sysNotification->user_id=$assignedUsers;
          $sysNotification->save();
        }

        if($status==true){
          if($dynamicForm->confirmation_msg!=''){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>$dynamicForm->confirmation_msg,]]);
          }else{
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('multiformgen::dynamicform.data_saved'),]]);
          }
        }else{
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('multiformgen::dynamicform.data_not_saved'),]]);
        }
      }else{
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('multiformgen::dynamicform.data_not_saved'),]]);
      }
    }
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = $this->newModel();
    $model->title = 'untitled form';
    $model->save();
    return redirect($this->folderName.'/update/'.$model->id);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = $this->findModel($id);

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required|string',
      ]);
      $model= $this->findModel($request->id);
      $model->title = trim($request->form_title);
      $model->allowed_domains = trim($request->allowed_domains);
      $model->input_labels = $request->input_labels ? $request->input_labels : 0;
      $model->confirmation_msg = trim($request->confirmation_msg);
      $model->html_code = trim($request->html_code);
      $model->js_code = trim($request->js_code);
      $model->iframe_code = trim($request->iframe_code);
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        saveSelectedFormFields($model,$request);
        saveTags($model,$request,'form');
        saveModuleManagerField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('multiformgen::dynamicform.saved'),]]);
        }else{
          return redirect($this->folderName)->with('success', __('multiformgen::dynamicform.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('multiformgen::dynamicform.notsaved'),]]);
        }else{
          return redirect($this->folderName)->with('error', __('multiformgen::dynamicform.notsaved'));
        }
      }
    }
    $folderName = $this->folderName;

    return view('multiformgen::'.$this->folderName.'.update', [
      'model'=>$model,
      'request'=>$request,
      'folderName'=>$folderName
    ]);

  }

  /**
  * Create a submission model
  *
  * @return \Illuminate\Http\Response
  */
  public function newSubmissionModel()
  {
    return new DynamicFormData;
  }

  /**
  * Find a submission model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findDataModel($id,$formid)
  {
    return DynamicFormData::where('id', $id)->where('form_id',$formid)->first();
  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new DynamicForms;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return DynamicForms::where('id', $id)->first();
  }
}

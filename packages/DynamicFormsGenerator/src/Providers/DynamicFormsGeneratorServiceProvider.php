<?php

namespace NaeemAwan\DynamicFormsGenerator\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\DynamicFormsGenerator\View\Components\DynamicFields;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicForms;
use NaeemAwan\DynamicFormsGenerator\Observers\DynamicFormObserver;
use NaeemAwan\DynamicFormsGenerator\Models\DynamicFormData;
use NaeemAwan\DynamicFormsGenerator\Observers\DynamicFormDataObserver;

class DynamicFormsGeneratorServiceProvider extends ServiceProvider
{
  public function boot()
  {
    DynamicForms::observe(DynamicFormObserver::class);
    DynamicFormData::observe(DynamicFormDataObserver::class);
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'multiformgen');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'multiformgen');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/multiformgen'),
    ],'multiformgen-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'multiformgen-migrations');

    $this->loadViewComponentsAs('multiformgen', [
        DynamicFields::class,
    ]);

  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

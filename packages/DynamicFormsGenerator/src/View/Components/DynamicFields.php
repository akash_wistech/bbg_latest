<?php
namespace NaeemAwan\DynamicFormsGenerator\View\Components;

use Illuminate\View\Component;

class DynamicFields extends Component
{
  /**
  * model.
  *
  * @var string
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $model = $this->model;

    $formFields = getLeadFormFields();
    $rows=1;
    $fldRank=[];

    $formId = 'dlrf'.$model->id;

    return view('multiformgen::components.dynamic-fields',compact('model','formFields','fldRank','formId'));
  }
}

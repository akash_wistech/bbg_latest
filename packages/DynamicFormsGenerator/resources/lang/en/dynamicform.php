<?php
return [
  'heading' => 'Dynamic Forms',
  'saved' => 'Form saved successfully',
  'notsaved' => 'Form not saved',
  'data_saved' => 'Information sent successfully',
  'data_not_saved' => 'Sorry, system encountered a problem while sending your information, Please try again in a while',
  'allowed_domains' => 'Allowed Domains',
  'allowed_domains:' => 'Allowed Domains:',
  'confirmation_msg' => 'Confirmation Message',
  'form_fields' => 'Form Fields',
  'select_fields' => 'Select Fields',
  'embed_code' => 'Embed Code',
  'submission_heading' => 'Inquiries',
  'inquiries' => 'Inquiries',
  'go_to_form_list' => 'Check Inquiry',
  'labels' => 'Input Labels',
  'mark_spam' => 'Mark Spam',
  'auto_spam' => 'Auto Spam',
  'ip_blocked_successfully' => 'Ip blocked successfully',

  'inquiries_with_count' => 'Inquiries (:count)',
  'converted_with_count' => 'Converted (:count)',
  'spam_with_count' => 'Spam (:count)',
];

@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>$routeId.'/create', 'method'=>'post'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
$advSearchCols[]=['title'=>__('common.name'),'name'=>'title'];

$dtColsArr[]=['title'=>__('multiformgen::dynamicform.inquiries'),'data'=>'submissions','name'=>'submissions'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('multiformgen::dynamicform.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('multiformgen::dynamicform.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="$controllerId" :jsonUrl="$routeId.'/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="true" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>
@endsection

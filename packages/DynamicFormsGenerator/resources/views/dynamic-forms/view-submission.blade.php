@extends('layouts.blank')
@php
$gridViewColumns = getDynamicFormFields($formid);
@endphp
@section('content')
<div class="row">
  @foreach($gridViewColumns as $gridViewColumn)
  @php
  $colWidth=6;
  if($gridViewColumn['col_width']==12)$colWidth=12;

  $colValue=getDynamicFormDataValue($formid,$id,$gridViewColumn['id']);
  @endphp
  <div class="col-12 col-sm-{{$colWidth}} mb-2">
    <strong>{{$gridViewColumn['title']}}:</strong> {{$colValue}}
  </div>
  @endforeach
</div>
@endsection

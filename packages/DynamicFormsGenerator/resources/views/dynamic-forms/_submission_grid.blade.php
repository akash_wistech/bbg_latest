@php
$gridViewColumns = getDynamicFormGridFields($model->id);

$dtColsArr=[];
$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.reference_no'),'data'=>'reference_no','name'=>'reference_no'];

if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}

$dtColsArr[]=['title'=>__('common.referer_url'),'data'=>'referral_url','name'=>'referral_url'];
$dtColsArr[]=['title'=>__('common.ip'),'data'=>'ip','name'=>'ip'];

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>85];
@endphp

<ul class="nav nav-tabs nav-bold nav-tabs-line">
	<li class="nav-item">
		<a class="nav-link{{$s==0 ? ' active' : ''}}" href="{{url($routeId.'/view/'.$model->id.'?s=0')}}">
			<span class="nav-text">{{ __('multiformgen::dynamicform.inquiries_with_count',['count'=>getDFInquiryCount([0],$model->id)]) }}</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link{{$s=='1,2' ? ' active' : ''}}" href="{{url($routeId.'/view/'.$model->id.'?s=1,2')}}">
			<span class="nav-text">{{ __('multiformgen::dynamicform.converted_with_count',['count'=>getDFInquiryCount([1,2],$model->id)]) }}</span>
		</a>
	</li>
	<li class="nav-item">
		<a class="nav-link{{$s==10 ? ' active' : ''}}" href="{{url($routeId.'/view/'.$model->id.'?s=10')}}">
			<span class="nav-text">{{ __('multiformgen::dynamicform.spam_with_count',['count'=>getDFInquiryCount([10],$model->id)]) }}</span>
		</a>
	</li>
</ul>
<section class="card card-custom card-border mb-3" data-card="true">
  <div id="grid-table" class="card-body">
    <x-data-table-grid :request="$request" :controller="$controllerId" :jsonUrl="'/incoming-data/single-table-data/'.$model->id.'?s='.$s" :dtColsArr="$dtColsArr" :moduleTypeId="''"/>
  </div>
</section>
@push('jsScripts')
@endpush()

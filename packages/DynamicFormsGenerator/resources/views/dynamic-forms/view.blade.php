@extends('layouts.app')
@section('title', __('multiformgen::dynamicform.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
$routeId=>__('multiformgen::dynamicform.heading'),
$routeId.'/update/'.$model->id=>__('common.update_rec',['name'=>$model->reference_no]),
__('common.view')
]])
@endsection

@section('content')
<section class="card card-custom card-border mb-3" data-card="true">
  <header class="card-header">
    <div class="card-title">
      <h3 class="card-label">{{ __('common.info') }}</h3>
    </div>
    <div class="card-toolbar">
      @if(checkActionAllowed('update'))
        <a href="{{ url($routeId.'/update',['id'=>$model->id]) }}" class="btn btn-icon btn-sm btn-hover-light-primary">
          <i class="flaticon-edit-1"></i>
        </a>
      @endif
      <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
        <i class="ki ki-arrow-down icon-nm"></i>
      </a>
    </div>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-3">
        <strong>{{ __('common.created:') }}</strong> {{ formatDateTime($model->created_at) }}
      </div>
      <div class="col-sm-3">
        <strong>{{ __('common.reference_no:') }}</strong> {{ $model->reference_no }}
      </div>
      <div class="col-sm-3">
        <strong>{{ __('common.title:') }}</strong> {{ $model->title }}
      </div>
      <div class="col-sm-3">
        <strong>{{ __('multiformgen::dynamicform.allowed_domains:') }}</strong> {{ $model->allowed_domains }}
      </div>
    </div>
    <x-customtags-custom-tags-field-details :model="$model"/>
  </div>
</section>
@include('multiformgen::'.$folderName.'._submission_grid')
@endsection

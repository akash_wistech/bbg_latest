@extends('layouts.app')
@section('title', __('multiformgen::dynamicform.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'company'=>__('multiformgen::dynamicform.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('multiformgen::'.$folderName.'._form')
@endsection

<div class="row mb-4">
  <div class="col-12 col-sm-3">
    <label class="checkbox">
      <input type="checkbox" name="input_labels" value="1" id="input_labels"{{$model->input_labels==1 ? ' checked="checked"' : ''}} />
      <span></span>&nbsp;{{__('multiformgen::dynamicform.labels')}}
    </label>
  </div>
  <div class="col-12 col-sm-3">
    <div class="form-group">
      <select class="form-control" name="msg_type" id="msg_type">
        <option value="inline" selected="selected">Inline</option>
        <option value="simplepopup">Popup</option>
      </select>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 col-sm-3">
    <div class="card card-custom card-border bg-light p-2">
      <div class="card-header p-1">
        <div class="card-title">
          <h3 class="card-label">{{__('multiformgen::dynamicform.select_fields')}}</h3>
        </div>
      </div>
      <div class="card-body p-0">
        <div class="checkbox-list">
          <ul id="frmFlds">
            @foreach($formFields as $formField)
            @php
            $checked = getFFSelection($model,$formField);

            @endphp
            <li class="p-2 border border-secondary m-2">
              <label class="checkbox">
                <input type="checkbox" class="input-options" name="selected_field[]" value="{{$formField['ft'].'-'.$formField['name']}}" data-id="{{$formField['id']}}" data-name="{{$formField['name']}}" data-colwidth="{{$formField['colwidth']}}" data-title="{{$formField['title']}}"{{$checked}} />
                <span></span>{{$formField['title']}}
              </label>
            </li>
            @endforeach
          </ul>
        </div>
        <input type="hidden" name="fieldRank" id="fieldRank" value="" />
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="card card-custom card-border bg-light p-2">
      <div class="card-header p-1">
        <div class="card-title">
          <h3 class="card-label">{{__('common.preview')}}</h3>
        </div>
      </div>
      <div id="previewHtml" class="card-body p-0">
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-3">
    <div class="card card-custom card-border bg-light p-2">
      <div class="card-header p-1">
        <div class="card-title">
          <h3 class="card-label">{{__('multiformgen::dynamicform.embed_code')}}</h3>
        </div>
      </div>
      <div class="card-body p-1">
        <ul class="nav nav-tabs nav-bold nav-tabs-line">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#dft-htmlcode">Html Code</a>
					</li>
					<!--li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#dft-iframecode">Iframe</a>
					</li-->
				</ul>
        <div class="tab-content pt-3">
          <div class="tab-pane fade show active" id="dft-htmlcode" role="tabpanel">
            <textarea id="ta-htmlcode" name="html_code" class="form-control" rows="10"></textarea>
            <h5 class="mt-3 mb-1">Js Code</h5>
            <textarea id="ta-jscode" name="js_code" class="form-control" rows="10"></textarea>
          </div>
          <div class="tab-pane fade" id="dft-iframecode" role="tabpanel">
            <textarea id="ta-iframecode" name="iframe_code" class="form-control" rows="10"></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@push('js')
<script src="{{asset('assets/plugins/custom/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
@endpush
@push('jsScripts')
$("#frmFlds").sortable({
  revert: 100,
  placeholder: "placeholder",
  update: function (event, ui) {
    var data = $(this).sortable("toArray");
    $("#fieldRank").val(data.join(","));
    updateFormOutput();
  }
});
$("body").on("change", "input:checkbox", function (e) {
  updateFormOutput();
});
updateFormOutput();
@endpush
@push('jsFunc')
var formFieldOptions = {!!json_encode($formFields)!!};
function updateFormOutput()
{
  if($('#input_labels').is(':checked')){
    addLabels = 1;
  }else{
    addLabels = 0;
  }
  html = '<div id="{{$formId}}-container" style="position:relative;">';
  html+= '  <div id="dffsloader" style="display:none;position:fixed;top:0;left:0;right:0;bottom:0;width:100%;background:rgba(0,0,0,0.75) url({{asset('images/loading.gif')}}) no-repeat center center;z-index:10000;"></div>';
  html+= '  <form id="{{$formId}}" action="{{url('lead-form')}}" method="post">';
  html+= '    <input type="hidden" name="fid" id="fid" value="{{$model->id}}" />';
  html+= '    <input type="hidden" name="dfuip" id="dfuip" value="" />';
  html+= '    <div class="row">';
    $('input[type=checkbox].input-options').each(function () {
      inputId = $(this).data('id');
      thisOption = formFieldOptions[inputId];

      isRequired = thisOption['required'];
      title = thisOption['title'];
      inputName = thisOption['name'];
      inputType = thisOption['input_type'];

      colwidth = thisOption['colwidth'];
      colwidth = 6;
      if(inputType=='textarea')colwidth = 12;

      if(this.checked){
        html+= '<div class="col-12 col-sm-'+colwidth+'">';
          html+= '  <div class="form-group">';
            if(addLabels==1){
              html+= '    <label for="title">'+title+(isRequired==true ? ' <span style="color:red">*</span>' : '')+'</label>';
            }
            if(inputType=='text'){
              html+= '    <input class="form-control'+(isRequired==true ? ' isrequired' : '')+'" autocomplete="off" name="'+inputName+'" placeholder="'+title+'" type="text" id="'+inputId+'" />';
            }
            if(inputType=='websiteinput'){
              html+= '    <input class="form-control'+(isRequired==true ? ' isrequired' : '')+'" autocomplete="off" name="'+inputName+'" placeholder="'+title+'" type="url" id="'+inputId+'" />';
            }
            if(inputType=='emailinput'){
              html+= '    <input class="form-control'+(isRequired==true ? ' isrequired' : '')+'" autocomplete="off" name="'+inputName+'" placeholder="'+title+'" type="email" id="'+inputId+'" />';
            }
            if(inputType=='numberinput'){
              html+= '    <input class="form-control'+(isRequired==true ? ' isrequired' : '')+'" autocomplete="off" name="'+inputName+'" placeholder="'+title+'" type="number" id="'+inputId+'" />';
            }
            if(inputType=='date'){
              html+= '    <input class="form-control'+(isRequired==true ? ' isrequired' : '')+' dfdtpicker" autocomplete="off" name="'+inputName+'" placeholder="'+title+'" type="text" id="'+inputId+'" />';
            }
            if(inputType=='textarea'){
              html+= '    <textarea class="form-control'+(isRequired==true ? ' isrequired' : '')+'" name="'+inputName+'" placeholder="'+title+'" id="'+inputId+'" rows="6"></textarea>';
            }
            if(inputType=='select'){
              html+= '    <select class="form-control'+(isRequired==true ? ' isrequired' : '')+'" name="'+inputName+'" id="'+inputId+'">';
              html+= '      <option value="">{{__('common.select')}}</option>';
              inputOptions = thisOption['subOptions'];
              if(inputOptions!=undefined){
                $.each(inputOptions, function(index, item) {
                  html+= '      <option value="'+index+'">'+item+'</option>';
                });
              }
              html+= '    </select>';
            }
            html+= '  </div>';
            html+= '</div>';
          }
        });
        html+= '    </div>';
        html+= '  <div id="{{$formId}}-msg-container" style="display:none;background-color: #67d467;color: white;padding: 5px;margin-bottom: 20px; text-align: center;"></div>';
        html+= '    <div class="row">';
        html+= '      <div class="col-12 col-sm-12">';
        html+= '        <button type="submit" class="btn btn-primary mr-2">Submit</button>';
        html+= '      </div>';
        html+= '    </div>';
        html+= '  </form>';
        html+= '</div>';
        {!!generateJsCode($formId,$formFields)!!}

        $("#previewHtml").html(html);

        $("#ta-htmlcode").html(html);
        $("#ta-jscode").html(jsCode);
      }
      @endpush

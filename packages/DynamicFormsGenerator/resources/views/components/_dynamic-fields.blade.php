<div id="dynamic-field-container" class="row">
  <div class="col-12 col-sm-10">
    <div class="card card-custom card-border bg-light">
      <div class="card-header p-2 h-2">
				<div class="card-title">
					<h3 class="card-label">
            {{__('multiformgen::dynamicform.form_fields')}}
             <small><a href="javascript:;" onclick="javascript:updateWidgets('col');">{{__('common.collapse_all')}}</a> | <a href="javascript:;" onclick="javascript:updateWidgets('exp');">{{__('common.expand_all')}}</a></small>
          </h3>
				</div>
			</div>
      <div class="card-body p-3">
        <ul id="form-fields">
        </ul>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-2">
    <div data-spy="affix" data-offset-top="500">
      <div class="card card-custom card-border">
        <div class="card-body p-1">
          <ul class="navi navi-hover navi-link-rounded-lg">
            @foreach($inputTypes as $inputType)
            <li class="navi-item">
              <a class="navi-link" href="javascript:;" onclick="javascript:addInput('{{$inputType['type']}}');">
                <span class="navi-icon custom-icomoon">
                  <i class="{{$inputType['icon']}}"></i>
                </span>
                <span class="navi-text">{{$inputType['title']}}</span>
              </a>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<input id="fldRank" name="inputfieldrank" type="hidden" value="{{implode(",",$fldRank)}}" />
@push('css')
<link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('jsScripts')
	/*$("#inputOpts").slimScroll({
    height: "415px",
		size: "3px",
  });*/
  $("body").on("click", ".removeTmpSubOption", function (e) {
		$(this).closest("tr.subOptRow").remove();
	});
  $("body").on("click", ".collapse-it", function (e) {
		$(this).removeClass("collapse-it").addClass("un-collapse-it");
		$(this).html("<i class=\"ki ki-arrow-down icon-nm\"></i>");
		$(this).closest(".card.card-custom").addClass("card-collapsed");
	});
  $("body").on("click", ".un-collapse-it", function (e) {
		$(this).removeClass("un-collapse-it").addClass("collapse-it");
		$(this).html("<i class=\"ki ki-arrow-up icon-nm\"></i>");
		$(this).closest(".card.card-custom").removeClass("card-collapsed");
	});
  $("body").on("keyup", ".field-title", function (e) {
    $(this).parents(".card.card-custom").find(".fldTitle").html(" - "+$(this).val());
  });
  $("body").on("click", ".change-field-status", function (e) {
		id=$(this).data("id");
		stype=$(this).data("stype");
		msgText="You want to enable this?";
		if(stype=="disable"){
			msgText="You want to disable this?";
		}
    swal({
      title: "'.Yii::t('app','Are you sure?').'",
      text: msgText,
      type: "info",
      showCancelButton: true,
      confirmButtonText: "'.Yii::t('app','Confirm').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'"
    }).then((result) => {
      if (result.value) {
        _targetContainer=$("#box-container-"+id);
        App.blockUI({
          message: "'.Yii::t('app','Please wait...').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });
        $.ajax({
          url: "'.Url::to(['change-field-status','media_type'=>$model->id,'id'=>'']).'"+id,
          dataType: "html",
          type: "post",
          success: function(data) {
            if(data=="success"){
              toastr.success("'.Yii::t('app','Updated!').'", "'.Yii::t('app','Field updated successfully').'");
              App.unblockUI($(_targetContainer));
              $.pjax.reload({container: "#box-container-"+id, timeout: 2000});
            }
          },
          error: bbAlert
        });
      }
    });
  });
	$(document).delegate(".remove-input-field", "click", function() {
		_this=$(this);
		field_id=$(this).data("field_id");
    swal({
      title: "'.Yii::t('app','Are you sure?').'",
      text: "'.Yii::t('app','You will not not be able to revert this.').'",
      type: "info",
      showCancelButton: true,
      confirmButtonText: "'.Yii::t('app','Confirm').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'"
    }).then((result) => {
      if (result.value) {
        _targetContainer=$("#frmFld-"+field_id);
        App.blockUI({
          message: "'.Yii::t('app','Please wait...').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });
        $.ajax({
          url: "'.Url::to(['delete-input-field','media_type_id'=>$model->id,'id'=>'']).'"+field_id,
          dataType: "json",
          type: "post",
          success: function(response) {
						if(response["success"]){
							_this.closest("li").remove();
							swal({title: response["success"]["heading"], text: response["success"]["msg"], type: "success", timer: 5000});
						}else{
							swal({title: response["error"]["heading"], text: response["error"]["msg"], type: "error", timer: 5000});
						}
						App.unblockUI($(_targetContainer));
          },
          error: bbAlert
        });
      }
    });
  });
	$(document).delegate(".change-field-option-status", "click", function() {
		id=$(this).data("id");
		field_id=$(this).data("field_id");
		stype=$(this).data("stype");
		msgText="You want to enable this?";
		if(stype=="disable"){
			msgText="You want to disable this?";
		}
    swal({
      title: "'.Yii::t('app','Are you sure?').'",
      text: msgText,
      type: "info",
      showCancelButton: true,
      confirmButtonText: "'.Yii::t('app','Confirm').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'"
    }).then((result) => {
      if (result.value) {
        _targetContainer=$("#tbl-"+field_id+"-container-"+id);
        App.blockUI({
          message: "'.Yii::t('app','Please wait...').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });
        $.ajax({
          url: "'.Url::to(['change-field-option-status','media_type'=>$model->id,'field_id'=>'']).'"+field_id+"&id="+id,
          dataType: "html",
          type: "post",
          success: function(data) {
            if(data=="success"){
              toastr.success("'.Yii::t('app','Updated!').'", "'.Yii::t('app','Field updated successfully').'");
              App.unblockUI($(_targetContainer));
              $.pjax.reload({container: "#tbl-"+field_id+"-container-"+id, timeout: 2000});
            }
          },
          error: bbAlert
        });
      }
    });
  });
	$(document).delegate(".remove-input-field-option", "click", function() {
		_this=$(this);
		id=$(this).data("option_id");
		field_id=$(this).data("field_id");
    swal({
      title: "'.Yii::t('app','Are you sure?').'",
      text: "'.Yii::t('app','You will not not be able to revert this.').'",
      type: "info",
      showCancelButton: true,
      confirmButtonText: "'.Yii::t('app','Confirm').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'"
    }).then((result) => {
      if (result.value) {
        _targetContainer=$("#frmFld-"+field_id);
        App.blockUI({
          message: "'.Yii::t('app','Please wait...').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });
        $.ajax({
          url: "'.Url::to(['delete-input-field-option','media_type_id'=>$model->id,'field_id'=>'']).'"+field_id+"&id="+id,
          dataType: "json",
          type: "post",
          success: function(response) {
						if(response["success"]){
							_this.closest(".subOptRow").remove();
							swal({title: response["success"]["heading"], text: response["success"]["msg"], type: "success", timer: 5000});
						}else{
							swal({title: response["error"]["heading"], text: response["error"]["msg"], type: "error", timer: 5000});
						}
						App.unblockUI($(_targetContainer));
          },
          error: bbAlert
        });
      }
    });
  });
	/*$("#form-fields").sortable({
    revert: 100,
    placeholder: "placeholder",
		update: function (event, ui) {
      var data = $(this).sortable("toArray");
			console.log(data);
			$("#fldRank").val(data.join(","));
    }
  });*/
@endpush

@push('jsFunc')
var row={{$rows}};
var inputOptionsIconArr=[];
var inputOptionsLabelArr=[];
@foreach($inputTypes as $inputType)
inputOptionsIconArr['{{$inputType['type']}}'] = '<i class="{{$inputType['icon']}}"></i>';
inputOptionsLabelArr['{{$inputType['type']}}'] = '{{$inputType['title']}}';
@endforeach
function addInput(inputType)
{
	col1='8';
	col2='2';
	col3='';
	if(inputType=="select"){
		col1='6';
		col2='2';
		col3='2';
	}
	html = '';
	html+= '<li>';
	html+= '<div id="tmpFld-'+row+'" class="card card-custom card-border mb-2">';
	html+= '	<input type="hidden" name="input_field['+row+'][input_type]" value="'+inputType+'" />';
	html+= '	<div class="card-header">';
	html+= '	  <div class="card-title">';
	html+= '		 <span class="card-icon">'+inputOptionsIconArr[inputType]+'</span>';
	html+= '		 <h3 class="card-label">'+inputOptionsLabelArr[inputType]+'<span class="fldTitle"></span></h3>';
	html+= '	  </div>';
	html+= '		<div class="card-toolbar">';
	html+= '			<a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary mr-1 box-action collapse-it"><i class="ki ki-arrow-up icon-nm"></i></a>';
	html+= '			<a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" onclick="removeField('+row+');"><i class="ki ki-close icon-nm"></i></a>';
	html+= '		</div>';
	html+= '	</div>';
	html+= '	<div class="card-body">';
  html+= '		<div class="row">';
  html+= '			<div class="col-12 col-sm-'+col1+'">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" for="input-field-title-'+row+'">Title</label>';
  html+= '					<input type="text" id="input-field-title-'+row+'" class="form-control field-title" name="input_field['+row+'][title]" value="" placeholder="Title">';
  html+= '				</div>';
  html+= '			</div>';
  html+= '			<div class="col-12 col-sm-'+col2+'">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" for="input-field-show_in_grid-'+row+'">Show in Grid</label>';
  html+= '					<select type="text" id="input-field-show_in_grid-'+row+'" class="form-control" name="input_field['+row+'][show_in_grid]">';
  html+= '						<option value="1">Yes</option>';
  html+= '						<option value="0" selected="selected">No</option>';
  html+= '					</select>';
  html+= '				</div>';
  html+= '			</div>';
  html+= '			<div class="col-12 col-sm-'+col2+'">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" for="input-field-is_required-'+row+'">Required</label>';
  html+= '					<select type="text" id="input-field-is_required-'+row+'" class="form-control" name="input_field['+row+'][is_required]">';
  html+= '						<option value="1">Yes</option>';
  html+= '						<option value="0">No</option>';
  html+= '					</select>';
  html+= '				</div>';
  html+= '			</div>';
  if(inputType=="select"){
  html+= '			<div class="col-12 col-sm-'+col3+'">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" for="input-field-selection_type-'+row+'">Selection</label>';
  html+= '					<select type="text" id="input-field-selection_type-'+row+'" class="form-control" name="input_field['+row+'][selection_type]">';
  html+= '						<option value="1">Single</option>';
  html+= '						<option value="2">Multiple</option>';
  html+= '					</select>';
  html+= '				</div>';
  html+= '			</div>';
  }
  html+= '		</div>';
  html+= '		<div class="row">';
  html+= '		  <div class="col-12 col-sm-6">';
  html+= '		    <div class="form-group">';
  html+= '		      <label class="control-label" for="input-field-short_name-'+row+'">Short Name</label>';
  html+= '		      <input type="text" id="input-field-short_name-'+row+'" class="form-control" name="input_field['+row+'][short_name]" value="" placeholder="Short Name">';
  html+= '		    </div>';
  html+= '		  </div>';
  html+= '		  <div class="col-12 col-sm-6">';
  html+= '		    <div class="form-group">';
  html+= '		      <label class="control-label" for="input-field-colum_width-'+row+'">Columns</label>';
  html+= '		      <select type="text" id="input-field-colum_width-'+row+'" class="form-control" name="input_field['+row+'][colum_width]">';
  @for($cn=1;$cn<=12;$cn++)
  html+= '		        <option value="{{$cn}}"{{($cn==4 ? ' selected' : '')}}>{{$cn}} Columns</option>';
  @endfor
  html+= '		      </select>';
  html+= '		    </div>';
  html+= '		  </div>';
  html+= '		</div>';
  html+= '		<div class="row">';
  html+= '			<div class="col-12 col-sm-6">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" for="input-field-hint_text-'+row+'">Hint / Information Text (Frontend)</label>';
  html+= '					<textarea rows="4" id="input-field-hint_text-'+row+'" class="form-control" name="input_field['+row+'][hint_text]"></textarea>';
  html+= '				</div>';
  html+= '			</div>';
  html+= '			<div class="col-12 col-sm-6">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" for="input-field-descp-'+row+'">Description (Backend)</label>';
  html+= '					<textarea rows="4" id="input-field-descp-'+row+'" class="form-control" name="input_field['+row+'][descp]"></textarea>';
  html+= '				</div>';
  html+= '			</div>';
  html+= '		</div>';
  if(inputType=="select" || inputType=="checkbox" || inputType=="radio"){
  html+= '		<div class="row">';
  html+= '			<div class="col-12 col-sm-12">';
  html+= '				<div class="form-group">';
  html+= '					<label class="control-label" onclick="showTmpSubOptions(\'p\','+row+')" for="input-field-sub-opt-selection-'+row+'-up"><input type="radio" id="input-field-sub-opt-selection-'+row+'-up" name="input_field['+row+'][suboptselection]" value="usepredefined" checked="checked" /> Use Predefined list</label>&nbsp;&nbsp;&nbsp;';
  html+= '					<label class="control-label" onclick="showTmpSubOptions(\'c\','+row+')" for="input-field-sub-opt-selection-'+row+'-uc"><input type="radio" id="input-field-sub-opt-selection-'+row+'-uc" name="input_field['+row+'][suboptselection]" value="usecustom" /> Add New Options</label>';
  html+= '				</div>';
  html+= '				<div id="predefinedOpts'+row+'">';
  @foreach($predefinedListsArr as $key=>$val)
  html+= '					<label class="dd-pdtypes" for="input-field-predefined-selection-'+row+'-{{$key}}"><input type="radio" id="input-field-predefined-selection-'+row+'-{{$key}}" name="input_field['+row+'][predefined_link]" value="{{$key}}"> {{$val}}</label>';
  @endforeach
  html+= '				</div>';
  html+= '				<div id="customOpts'+row+'" style="display:none;">';
  html+= '					<table id="customOptsTable'+row+'" class="table table-bordered table-striped">';
  html+= '						<thead>';
  html+= '						<tr>';
  html+= '							<th>Title</th>';
  html+= '							<th width="10"><a href="javascript:;" class="btn btn-xs btn-success" onclick="javascript:addSubOption('+row+');"><i class="fa fa-plus"></i></a></th>';
  html+= '						</tr>';
  html+= '						</thead>';
  html+= '						<tbody>';
  for(g=1;g<=3;g++){
  html+= '						<tr class="subOptRow">';
  html+= '							<td>';
  html+= '								<input type="text" class="form-control" name="input_field['+row+'][suboption_title][]" />';
  html+= '							</td>';
  html+= '							<td><a href="javascript:;" class="btn btn-xs btn-danger removeTmpSubOption"><i class="fa fa-times"></a></td>';
  html+= '						</tr>';
  }
  html+= '						</tbody>';
  html+= '					</table>';
  html+= '				</div>';
  html+= '			</div>';
  html+= '		</div>';
  }
	html+= '	</div>';
	html+= '</div>';
	html+= '</li>';
	$("#form-fields").append(html);
	row++;
}
function showSubOptions(type,id)
{
	if(type=='p'){
		$("#frmFld-"+id).find("#predefinedOpts"+id).show();
		$("#frmFld-"+id).find("#customOpts"+id).hide();
	}
	if(type=='c'){
		$("#frmFld-"+id).find("#predefinedOpts"+id).hide();
		$("#frmFld-"+id).find("#customOpts"+id).show();

	}
}
function showTmpSubOptions(type,id)
{
	if(type=='p'){
		$("#tmpFld-"+id).find("#predefinedOpts"+id).show();
		$("#tmpFld-"+id).find("#customOpts"+id).hide();
	}
	if(type=='c'){
		$("#tmpFld-"+id).find("#predefinedOpts"+id).hide();
		$("#tmpFld-"+id).find("#customOpts"+id).show();

	}
}
function addSubOption(id)
{
	html = '';
	html+= '<tr class="subOptRow">';
	html+= '	<td>';
	html+= '		<input type="text" class="form-control" name="input_field['+id+'][suboption_title][]" />';
	html+= '	</td>';
	html+= '	<td><a href="javascript:;" class="btn btn-xs btn-danger removeTmpSubOption"><i class="fa fa-times"></a></td>';
	html+= '</tr>';
	$("#customOptsTable"+id).find("tbody").append(html);
}
function removeField(id)
{
	$("#tmpFld-"+id).remove();
}
function updateWidgets(act)
{
	$("#form-fields .card.card-custom").each(function( index ) {
    if(act=='col'){
			$(this).addClass("card-collapsed");
			$(this).find(".box-action").removeClass("collapse-it").addClass("un-collapse-it").html("<i class=\"ki ki-arrow-down icon-nm\"></i>");
		}else{
			$(this).removeClass("card-collapsed");
			$(this).find(".box-action").removeClass("un-collapse-it").addClass("collapse-it").html("<i class=\"ki ki-arrow-up icon-nm\"></i>");
		}
	});
}
@endpush

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDynamicFormDataMultiple extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dynamic_form_data_value_multiple', function (Blueprint $table) {
        $table->integer('form_id')->index();
        $table->integer('form_field_id')->index();
        $table->integer('input_field_option_id')->index();
        $table->text('input_field_option_id_value')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('dynamic_form_data_value_multiple');
    }
}

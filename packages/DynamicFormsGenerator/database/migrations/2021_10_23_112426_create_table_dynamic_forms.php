<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTableDynamicForms extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('dynamic_forms', function (Blueprint $table) {
      $table->id();
      $table->string('slug',100)->nullable();
      $table->string('reference_no',100)->nullable();
      $table->string('title',100);
      $table->text('allowed_domains')->nullable();
      $table->tinyInteger('input_labels')->default(1)->nullable();
      $table->text('html_code')->nullable();
      $table->text('js_code')->nullable();
      $table->text('iframe_code')->nullable();
      $table->string('msg_type',25)->nullable();
      $table->text('confirmation_msg')->nullable();
      $table->timestamps();
      $table->timestamp('deleted_at')->nullable();
      $table->integer('created_by')->nullable();
      $table->integer('updated_by')->nullable();
      $table->integer('deleted_by')->nullable();
    });

    // Insert some stuff
    DB::table('admin_menu')->insert([
      'parent' => 0,
      'icon' => 'flaticon-apps',
      'title' => 'Dynamic Forms',
      'route' => 'dynamic-forms',
      'controller_id' => 'dynamicforms',
      'action_id' => 'index',
      'show_in_menu' => 1,
      'ask_list_type' => 0,
      'rank' => 8
    ]);
    $menuId = DB::getPdo()->lastInsertId();
    DB::table('admin_menu')->insert([
      'parent'=>$menuId,'title'=>'Create','route'=>'dynamic-forms','controller_id'=>'dynamicforms','action_id'=>'create','rank'=>1
    ]);
    DB::table('admin_menu')->insert([
      'parent'=>$menuId,'title'=>'Update','route'=>'dynamic-forms','controller_id'=>'dynamicforms','action_id'=>'update','rank'=>2
    ]);
    DB::table('admin_menu')->insert([
      'parent'=>$menuId,'title'=>'View','route'=>'dynamic-forms','controller_id'=>'dynamicforms','action_id'=>'view','rank'=>3
    ]);
    DB::table('admin_menu')->insert([
      'parent'=>$menuId,'title'=>'Change Status','route'=>'dynamic-forms','controller_id'=>'dynamicforms','action_id'=>'status','rank'=>4
    ]);
    DB::table('admin_menu')->insert([
      'parent'=>$menuId,'title'=>'Delete','route'=>'dynamic-forms','controller_id'=>'dynamicforms','action_id'=>'delete','rank'=>5
    ]);
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('dynamic_forms');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDynamicFormFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dynamic_form_fields', function (Blueprint $table) {
        $table->id();
        $table->integer('form_id')->index();
        $table->string('input_type',25)->index();
        $table->string('field_type',10);
        $table->string('field_full_name',50);
        $table->string('field_name',50);
        $table->integer('custom_field_id')->default(0)->nullable();
        $table->string('title',100);
        $table->string('short_title',50)->nullable();
        $table->tinyInteger('is_required')->default(1)->nullable();
        $table->tinyInteger('show_in_grid')->default(1)->nullable();
        $table->tinyInteger('predefined_list_id')->default(0)->nullable();
        $table->tinyInteger('col_width')->default(4)->nullable();
        $table->tinyInteger('selection_type')->default(1)->nullable()->comment('1=>Single,2=>Multiple');
        $table->text('hint_text')->nullable();
        $table->text('descp')->nullable();
        $table->integer('sort_order')->nullable();
        $table->tinyInteger('adv_filter')->default(0)->nullable();
        $table->tinyInteger('enable_stats')->default(0)->nullable();
        $table->tinyInteger('status')->default(1)->nullable();
        $table->timestamps();
        $table->timestamp('deleted_at')->nullable();
        $table->integer('created_by')->nullable();
        $table->integer('updated_by')->nullable();
        $table->integer('deleted_by')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('dynamic_form_fields');
    }
}

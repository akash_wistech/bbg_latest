<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDynamicFormData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dynamic_form_data', function (Blueprint $table) {
        $table->id();
        $table->string('reference_no',100)->nullable();
        $table->integer('form_id')->index();
        $table->tinyInteger('status')->default(0)->index();
        $table->text('referral_url')->nullable();
        $table->string('ip',50)->nullable();
        $table->timestamps();
        $table->integer('created_by')->nullable();
        $table->integer('updated_by')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('dynamic_form_data');
    }
}

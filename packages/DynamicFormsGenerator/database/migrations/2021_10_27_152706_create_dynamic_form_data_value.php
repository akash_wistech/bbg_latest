<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicFormDataValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dynamic_form_data_value', function (Blueprint $table) {
        $table->integer('form_id')->index();
        $table->integer('form_data_id')->index();
        $table->integer('form_field_id')->index()->nullable();
        $table->text('input_value')->nullable();
        $table->integer('input_value_id')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('dynamic_form_data_value');
    }
}

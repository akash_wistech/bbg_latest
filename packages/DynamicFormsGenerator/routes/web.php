<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\DynamicFormsGenerator\Http\Controllers\DynamicFormsController;
use NaeemAwan\DynamicFormsGenerator\Http\Controllers\IncomingDataController;

Route::post('/lead-form', [DynamicFormsController::class,'saveRemoteForm']);

Route::group(['middleware' => ['web','auth']], function () {
  Route::get('/dynamic-forms', [DynamicFormsController::class,'index']);
  Route::get('/dynamic-forms/data-table-data', [DynamicFormsController::class,'datatableData']);
  Route::match(['get', 'post'], '/dynamic-forms/create', [DynamicFormsController::class,'create']);
  Route::match(['get', 'post'], '/dynamic-forms/view/{id}', [DynamicFormsController::class,'show']);
  Route::match(['get', 'post'], '/dynamic-forms/view-submission/{id}/{formid}', [DynamicFormsController::class,'viewSubmission']);
  Route::match(['get', 'post'], '/dynamic-forms/update/{id}', [DynamicFormsController::class,'update']);
  Route::match(['get', 'post'], '/dynamic-forms/status/{id}', [DynamicFormsController::class,'status']);
  Route::post('/dynamic-forms/delete/{id}', [DynamicFormsController::class,'destroy']);

  Route::get('/incoming-data/single-table-data/{id}', [IncomingDataController::class,'submissionDatatableData']);
  Route::match(['get', 'post'], '/incoming-data/mark-spam/{id}/{formid}', [IncomingDataController::class,'markSpam']);
  Route::match(['get', 'post'], '/incoming-data/auto-spam/{formid}/{ip}', [IncomingDataController::class,'autoMarkSpam']);
  Route::get('/incoming-data', [IncomingDataController::class,'index']);
  Route::get('/incoming-data/data-table-data', [IncomingDataController::class,'datatableData']);
  Route::match(['get', 'post'], '/incoming-data/lead-form/{formid}/{idid}', [IncomingDataController::class,'saveLead']);
});

{!! Form::model(null,['id'=>'attfrm','block1cntr'=>'alc','class'=>'simp1le-ajax-submit']) !!}
<div id="hidden-files" class="d-none1"></div>
<div class="d-none">
  <button type="submit" class="btn btn-sm btn-success">{{__('common.save')}}</button>
</div>
{!! Form::close() !!}
<div class="dropzone dropzone-default dropzone-success mb-3" id="att_dz">
  <div class="dropzone-msg dz-message needsclick">
    <h3 class="dropzone-msg-title">
      {{__('actionlog::activity.dropzone_msg')}}
    </h3>
    <span class="dropzone-msg-desc">(png,jpg,jpeg,gif,doc,docx,xls,xlsx,pdf)</span>
  </div>
</div>
@if($results!=null)
<div class="d-flex align-items-center mt-5">
  @foreach($results as $result)
  @php
  $otherFilesArr=['doc','docx','xls','xlsx','pdf'];
  if($result['file_name']!=null && file_exists(attachmentFilePath().'/'.$result['file_name'])){
    $fpInfo=pathinfo(attachmentFilePath().'/'.$result['file_name']);

    $fileLink=getAttachmentImagePath($result['file_name'],'');
    if(in_array($fpInfo['extension'],$otherFilesArr)){
      $icon=asset('assets/media/icons/'.$fpInfo['extension'].'.jpg');
    }else{
      $icon=getAttachmentImagePath($result['file_name'],'icon');
    }
  }
  @endphp
  <div class="symbol symbol-20 mr-3">
      <a href="{{$fileLink}}" target="_blank"><img alt="{{$result->file_title}}" src="{{$icon}}" width="100" /></a>
  </div>
  @endforeach
</div>
@endif
@push('jsScripts')
$('#att_dz').dropzone({
    url: "{{url('action-log/drop-upload')}}", // Set the url for your upload script location
    headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    paramName: "file", // The name that will be used to transfer the file
    maxFiles: 10,
    maxFilesize: 10, // MB
    addRemoveLinks: true,
    acceptedFiles: "image/*,application/pdf",
    accept: function(file, done) {
        if (file.name == "justinbieber.jpg") {
            done("Naha, you don't.");
        } else {
            done();
        }
    },
    success: function (file, response) {
      var fileName = response;
      $("#hidden-files").append("<input type=\"hid1den\" name=\"attachment_file[]\" class=\"form-control\" value=\""+fileName+"\" />");
      file.previewElement.classList.add("dz-success");
      file.previewElement.querySelector(".dz-progress").style.opacity = 0;
      file.previewElement.querySelector(".dz-success-mark").style.opacity = 1;
    },
    removedfile: function(file){
      $("input[value=\""+file.name+"\"]").remove();
      $(document).find(file.previewElement).remove();
    },
    queuecomplete: function (file, response) {
      submitFiles();
    }
});
@endpush
<script>
function submitFiles()
{
	var form = $("#attfrm");
	$.ajax({
   type: "POST",
   url: "{{url('action-log/save-attachment/'.$model->moduleTypeId.'/'.$model->id)}}",
   data: form.serialize(), // serializes the form's elements.
	 dataType: 'json',
   success: function(response){
		 if(response["success"]){
			 $("#hidden-files").html("");
			 if($("#al-lv-container").length){
				 // $.pjax.reload({container: "#al-lv-container", timeout: 2000});
			 }
			 // reloadAlHistory();
			 toastr.success(response["success"]["msg"]);
		 }else{
			 Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error"});
		 }
   }
 });
}
</script>

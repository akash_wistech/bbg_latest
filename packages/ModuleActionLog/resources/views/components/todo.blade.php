<div class="row mt-3 mb-5">
  <div class="col-xl-12">
    <div class="card card-custom card-stretch" id="kt_todo_list">
      <div class="card-body p-0 pt-4">
        <div class="table-responsive">
          <div class="list list-hover min-w-500px" data-inbox="list">
            @foreach($todoTabs as $key=>$todoTab)
            <div class="card card-custom" data-card="true" data-card-tooltips="false" id="todoTab{{$key}}">
              <div class="card-header">
                <div class="card-title">
                  <h3 class="card-label">{{$todoTab['title']}} (<span id="todoCount{{$key}}">{{getTodoCount($todoTab['from'],$todoTab['to'])}}</span>)</h3>
                </div>
                <div class="card-toolbar">
                  <a href="#" class="btn btn-icon btn-sm btn-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="{{__('common.toggle')}}">
                    <i class="ki ki-arrow-down icon-nm"></i>
                  </a>
                </div>
              </div>
              <div class="card-body" id="todoContainer{{$key}}" style="min-height:40px;">
              </div>
            </div>
            @push('jsScripts')
              var todoContent{{$key}}='';
              var url{{$key}}="{!!url('calendar/todo-items/'.$todoTab['from'].'/'.$todoTab['to'])!!}";

              _targetContainer{{$key}} = 'todoContainer{{$key}}';

              myApp.blockUIProgress('#'+_targetContainer{{$key}}, {
               overlayColor: '#000000',
               state: 'danger',
               message: "{{__('common.please_wait')}}"
              });

              sBCG(url{{$key}},_targetContainer{{$key}},'todoContainer{{$key}}',todoContent{{$key}},inittodoPopOver);
            @endpush
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@push('jsScripts')

$("body").on("click", ".todo-item-c", function (e) {
  _t=$(this);
  $("#general-modal h5").html(_t.data("title"));
  descp = _t.find(".descp-box").html();
  managersHtml = _t.find(".manager-box").html();
  popupBtns = _t.find(".btns-box").html();
  html = '<div id="calTaskShow" class="row">';
  html+= '  <div id="calTaskDetail" class="col-12 col-sm-12">';
  html+= '    '+descp+"";
  html+= '    '+managersHtml;
  html+= '  </div>';
  html+= '</div>';

  if(popupBtns!=''){
    html+='<hr />';
    html+='<div class="align-items-center">';
    html+=' <div class="d-flex align-items-center justify-content-between">';
    html+='   '+popupBtns+"";
    html+=' </div>';
    html+='</div>';
  }
  $("#general-modal .modalContent").html(html);
  $("#general-modal").modal();
  $('[data-toggle="tooltip"]').tooltip();
});
@endpush
@push('jsFunc')
function inittodoPopOver(params)
{
  /*setTimeout(function(){
    if($(".extendedPopOver").length>0){
      $(".extendedPopOver").popover({
          html: true,
          placement: "auto",
          trigger: "click",
          content: function () {
            //return $(this).parents(".todo-item-c").find(".description-box").html();
            return $(this).find(".description-box").html();
          }
      });
    }
  }, 3000);*/
}
@endpush

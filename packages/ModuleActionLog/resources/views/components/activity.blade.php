<div id="alc" class="activity-logger-card">
  <ul class="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
    <li class="nav-item mb-2" data-toggle="tooltip"  title="{{ __('actionlog::activity.comment') }}" data-placement="left">
      <a class="btn btn-sm btn-icon btn-bg-light btn-icon-success btn-hover-success" data-toggle="modal" href="#tab-comment">
        <i class="far fa-comment-dots"></i>
      </a>
    </li>
    <li class="nav-item mb-2" data-toggle="tooltip" title="{{ __('actionlog::activity.task') }}" data-placement="left">
      <a class="btn btn-sm btn-icon btn-bg-light btn-icon-primary btn-hover-primary" data-toggle="modal" href="#tab-action">
        <i class="fas fa-tasks"></i>
      </a>
    </li>
    <li class="nav-item mb-2" data-toggle="tooltip" title="{{ __('actionlog::activity.log_call') }}" data-placement="left">
      <a class="btn btn-sm btn-icon btn-bg-light btn-icon-warning btn-hover-warning" data-toggle="modal" href="#tab-log-call">
        <i class="fas fa-phone-alt"></i>
      </a>
    </li>
    <li class="nav-item mb-2" data-toggle="tooltip" title="{{ __('actionlog::activity.log_time') }}" data-placement="left">
      <a class="btn btn-sm btn-icon btn-bg-light btn-icon-info btn-hover-info" data-toggle="modal" href="#tab-log-time">
        <i class="far fa-clock"></i>
      </a>
    </li>
    <li class="nav-item" data-toggle="tooltip" title="{{ __('actionlog::activity.calendar') }}" data-placement="left">
      <a class="btn btn-sm btn-icon btn-bg-light btn-icon-danger btn-hover-danger" data-toggle="modal" href="#tab-calendar-event">
        <i class="far fa-calendar-alt"></i>
      </a>
    </li>
  </ul>
  <div class="d-n1one">
    {!!getModalOpenHtml('tab-comment',__('actionlog::activity.comment'))!!}
    <div class="comment-form">
      {!! Form::model($commentForm,['url'=>['action-log/save','module'=>$commentForm->module_type,'type'=>$commentForm->rec_type,'id'=>$commentForm->model->id],'blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
      @include('actionlog::action-log._comment_form')
      {!! Form::close() !!}
    </div>
    {!!getModalCloseHtml()!!}

    {!!getModalOpenHtml('tab-action',__('actionlog::activity.task'))!!}
    <div class="action-form">
      {!! Form::model($actionForm,['url'=>['action-log/save','module'=>$actionForm->module_type,'type'=>$actionForm->rec_type,'id'=>$actionForm->model->id],'blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
      @include('actionlog::action-log._action_form')
      {!! Form::close() !!}
    </div>
    {!!getModalCloseHtml()!!}

    {!!getModalOpenHtml('tab-log-call',__('actionlog::activity.log_call'))!!}
    <div class="call-form">
      @php
      $callNoteResponse = [''=>'-'];
      @endphp
      {!! Form::model($callForm,['url'=>['action-log/save','module'=>$callForm->module_type,'type'=>$callForm->rec_type,'id'=>$callForm->model->id],'blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
      @include('actionlog::action-log._call_form')
      {!! Form::close() !!}
    </div>
    {!!getModalCloseHtml()!!}

    {!!getModalOpenHtml('tab-log-time',__('actionlog::activity.log_time'))!!}
    <div class="log-time-form">
      {!! Form::model($timerForm,['url'=>['action-log/save','module'=>$timerForm->module_type,'type'=>$timerForm->rec_type,'id'=>$timerForm->model->id],'blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
      @include('actionlog::action-log._time_form')
      {!! Form::close() !!}
    </div>
    {!!getModalCloseHtml()!!}

    {!!getModalOpenHtml('tab-calendar-event',__('actionlog::activity.calendar'))!!}
    <div class="calendar-event-form">
      {!! Form::model($calendarForm,['url'=>['action-log/save','module'=>$calendarForm->module_type,'type'=>$calendarForm->rec_type,'id'=>$calendarForm->model->id],'blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
      @include('actionlog::action-log._calendar_form')
      {!! Form::close() !!}
    </div>
    {!!getModalCloseHtml()!!}
  </div>
</div>

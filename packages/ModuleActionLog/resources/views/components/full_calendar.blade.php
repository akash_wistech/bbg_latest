<div class="card card-custom">
  <div class="card-body{{$cardBodyPadding}}">
    <div id="script-warning">
      <code>{{__('common.sorry')}}</code> {{__('common.something_broke')}}
    </div>
    <div id="calendar" class="fc fc-ltr fc-unthemed"></div>
  </div>
</div>
@push('cssStyles')
#script-warning {
  display: none;
  background: #eee;
  border-bottom: 1px solid #ddd;
  padding: 0 10px;
  line-height: 40px;
  text-align: center;
  font-weight: bold;
  font-size: 12px;
  color: red;
}
.fc-unthemed .fc-event .fc-content, .fc-unthemed .fc-event-dot .fc-content{padding-left:0.55rem;}
.fc-unthemed .fc-event .fc-content:before, .fc-unthemed .fc-event-dot .fc-content:before{content:none;}
#calHC{height:310px;overflow-y: auto;}
#calHistoryMain .card.card-custom > .card-header{min-height: 30px;}
@endpush
@push('css')
<link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>
<script>
var calendar;
document.addEventListener('DOMContentLoaded', function() {
  var todayDate = moment().startOf('day');
  var YM = todayDate.format('YYYY-MM');
  var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
  var TODAY = todayDate.format('YYYY-MM-DD');
  var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');
  var eventsDataSource = {
    url: "{{url('calendar/data-items')}}",
    extraParams: function() { // a function that returns an object
      chkdVals='';
      $(".cb").each(function () {
         if(this.checked){
           chkdVals+=(chkdVals!="" ? ',' : '')+$(this).val();
         }
      });
      chkdStaffVals = '';
      $('#assigned_to :selected').each(function(){
        chkdStaffVals+=(chkdStaffVals!="" ? ',' : '')+$(this).val();
      });
      return {
        calender_id: chkdVals,
        staff_id: chkdStaffVals,
      };
    },
    failure: function() {
      document.getElementById('script-warning').style.display = 'block'
    }
  }

  var fcOptions = {!! json_encode($fcOptions) !!};
  console.log(fcOptions);
  fcOptions.events = eventsDataSource;
  fcOptions.loading = loadingFunc;
  fcOptions.eventRender = eventRenderFunc;
  fcOptions.eventClick = eventClickFunc;
  fcOptions.contentHeight = 740;

  var calendarEl = document.getElementById('calendar');
  calendar = new FullCalendar.Calendar(calendarEl, fcOptions);

  calendar.render();

    var elements = document.getElementsByClassName("cb");
    var elementsAll = document.getElementsByClassName("select-on-check-all");

    var reloadCalendar = function() {
        calendar.refetchEvents();
    };

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', reloadCalendar, false);
    }
    for (var i = 0; i < elementsAll.length; i++) {
        elementsAll[i].addEventListener('click', reloadCalendar, false);
    }
});
function loadingFunc(bool) {
  if(bool){
    html = '<div class="overlay-layer bg-dark-o-10">';
    html+= ' <button type="button" class="btn btn-light-success spinner spinner-darker-success spinner-left mr-3">';
    html+= '   {{__('common.loading')}}';
    html+= ' </button>';
    html+= '</div>';
    $("#calendar").addClass("overlay").addClass("overlay-block");
    $("#calendar").append(html);
  }else{
    $("#calendar").removeClass("overlay").removeClass("overlay-block")
    $("#calendar").find(".overlay-layer").remove();
  }
  // document.getElementById('loading').style.display = bool ? 'block' : 'none';
}
function eventRenderFunc(info) {
  var element = $(info.el);
  if (info.event.extendedProps && info.event.extendedProps.description) {
    // element.data('mid', info.event.extendedProps.id);
    if (element.hasClass('fc-day-grid-event')) {
      element.data('content', info.event.extendedProps.description);
      element.data('placement', 'right');
      element.data('html', true);
      // element.data('trigger', 'click');
      KTApp.initPopover(element);
    } else if (element.hasClass('fc-time-grid-event')) {
      element.find('.fc-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
    } else if (element.find('.fc-list-item-title').lenght !== 0) {
      element.find('.fc-list-item-title').append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');
    }
    if (info.event.extendedProps.imageurl) {
        element.find("div.fc-content").prepend("" + info.event.extendedProps.imageurl +"");
    }
  }
}
function eventClickFunc(info) {
  $("#general-modal h5").html(info.event.title);
  html = '<div id="calTaskShow" class="row">';
  html+= '  <div id="calTaskDetail" class="col-12 col-sm-12">';
  html+= '    '+info.event.extendedProps.popdescription+"";
  html+= '    '+info.event.extendedProps.popmanagers;
  html+= '  </div>';
  html+= '</div>';
  popupBtns = info.event.extendedProps.popupBtns;
  if(popupBtns!=''){
    html+='<hr />';
    html+='<div class="align-items-center">';
    html+=' <div class="d-flex align-items-center justify-content-between">';
    html+='   '+popupBtns+"";
    html+=' </div>';
    html+='</div>';
  }
  $("#general-modal .modalContent").html(html);
  $("#general-modal").modal();
  $('[data-toggle="tooltip"]').tooltip();
}
function loadCalendar()
{
  calendar.refetchEvents();
}
var historyItem='';
function loadActivityHistory(module,id)
{
  if($("#calHistoryMain").length<=0){
    $("#calTaskDetail").removeClass("col-sm-12").addClass("col-sm-6");
    html ='<div id="calHistoryMain" class="col-12 col-sm-6">';
    html+=' <div class="card card-custom card-fit card-border p-1" data-card="true">';
    html+='   <div class="card-header p-1">';
    html+='     <div class="card-title">';
    html+='       <h3 class="card-label">{{__('actionlog::activity.history')}}</h3>';
    html+='     </div>';
    html+='     <div class="card-toolbar">';
    html+='       <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary remove-hc">';
    html+='         <i class="ki ki-close icon-nm"></i>';
    html+='       </a>';
    html+='     </div>';
    html+='   </div>';
    html+='   <div id="calHC" class="card-body p-0 pt-2">';
    html+='   </div>';
    html+=' </div>';
    html+='</div>';
    $("#calTaskShow").append(html);
    _targetContainer = 'calHistoryMain';

    myApp.blockUIProgress('#'+_targetContainer, {
     overlayColor: '#000000',
     state: 'danger',
     message: "{{__('common.please_wait')}}"
    });
    url = "{{url('action-log/history')}}/"+module+"/"+id+"";
    sBCG(url,_targetContainer,'calHC',historyItem,null);
  }
}
</script>
@endpush

@push('jsScripts')
$("body").on("change","#assigned_to",function(e){
  loadCalendar();
});
$("body").on("click",".remove-hc",function(e){
  swal({
    title: "{{__('common.confirmation')}}",
    html: "{{__('actionlog::activity.remove_history_msg')}}",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "{{__('common.confirm')}}",
    cancelButtonText: "{{__('common.cancel')}}",
  }).then((result) => {
    if (result.value) {
      $("#calHistoryMain").remove();
      $("#calTaskDetail").removeClass("col-sm-6").addClass("col-sm-12");
    }else{
      return false;
    }
  });
});
@endpush

<div id="al-lv-container">
  @if($results!=null)
  <div id="alh-list-view" class="card-comments mCustomS1crollbar" data-mcs-theme="dark" data-pjax>
    <div class="card-comments com-cnt">
      @foreach($results as $model)
        @include('actionlog::action-log._log_item')
      @endforeach
      <div class="al-pager d-non1e">
        {{ $results->links('vendor.pagination.custom-simple') }}
      </div>
      <div id="alh-spinner" class="text-center{{count($results)<=25 ? ' d-none' : ''}}">
        <div class="btn btn-secondary spinner spinner-dark spinner-right">
          <span>{{__('common.loading')}}</span>
        </div>
      </div>
    </div>
  </div>
  @else
  <div class="alert aler-info">
    {{__('common.noresultfound')}}
  </div>
  @endif
</div>
@push('jsFunc')
function reloadAlHistory(){
  $('#alh-list-view').infiniteScroll({
    path: 'a.pg-next',
    append: '.post',
    history: false,
  });

  //var alhNL=false;

  //let alhIAS = new InfiniteAjaxScroll('.com-cnt', {
  //  scrollContainer: '#alh-list-view',
  //  logger: false,
  //  item: '.alh-item',
  //  next: 'a.pg-next',
  //  pagination: '.al-pager',
  //  negativeMargin: 0,
  //  spinner: document.getElementById('alh-spinner'),
  //});

  //alhIAS.on('next', function(event) {
  //  alhNL=true;
  //});
  //alhIAS.on('appended', function(event) {
  //  alhNL=false;
  //});

  //$("#alh-list-view").mCustomScrollbar({
  //  scrollbarPosition: "outside",
  //  autoHideScrollbar: true,
  //  callbacks:{
  //    onTotalScroll: function(){
        //if(alhNL==false)alhIAS.next();
  //    }
  //  }
  //});

}
@endpush
@once
@push('jsScripts')
//reloadAlHistory();
$.pjax.defaults.timeout = 5000
$(document).pjax('[data-pjax] .al-pager a, a[data-pjax]', '#alh-list-view')
@endpush
@endonce
<style>
#alh-list-vie1w{height: 600px; overflow-y: auto;}
.att-img-block{
margin: 1px 2px;
display: inline-block;
padding: 1px;
border:1px solid #ccc;
}
.alh-toolbar a.btn{padding:1px;}
</style>

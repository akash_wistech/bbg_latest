@extends('layouts.app')
@section('title', __('actionlog::calendar.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'calendar'=>__('actionlog::calendar.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('actionlog::calendar._form')
@endsection

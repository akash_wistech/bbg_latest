@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/calendar/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('actionlog::calendar.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('actionlog::calendar.heading')
]])
@endsection


@section('content')
<ul class="nav nav-tabs nav-bold nav-tabs-line">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#calTab">Calendar</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#todoTab">Todo</a>
  </li>
</ul>
<div class="tab-content">
  <div class="tab-pane fade show active" id="calTab" role="tabpanel">
    <div class="calender-index row pt-3">
      <div class="col-sm-8">
        <x-actionlog-full-calendar/>
      </div>
      <div class="col-sm-4">
        <div class="card card-custom mb-3">
          <div id="grid-table" class="card card-body p-1">
            <div class="overlay-wrapper">
              <table class="table" id="dt-list">
                <thead>
                  <tr>
                    <th width="10"></th>
                    <th>{{__('common.title')}}</th>
                    <th width="90">{{__('common.action')}}</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($results as $result)
                  <tr>
                    <td><input type="checkbox" name="cb{{$result->id}}" value="{{$result->id}}" class="cb" /></td>
                    <td>{{$result->title}}</td>
                    <td>
                      @if(checkActionAllowed('update'))
                      <a href="{{ url('calendar/update/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon edit' data-toggle="tooltip" title='{{__('common.update')}}' data-id="{{$result->id}}">
                        <i class="text-dark-50 flaticon-edit"></i>
                      </a>
                      @endif
                      @if(checkActionAllowed('delete'))
                      <a href="{{ url('calendar/delete/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon act-confirmation' data-toggle="tooltip" title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                        <i class="text-dark-50 flaticon-delete"></i>
                      </a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        @if(Auth::user()->permission_group_id==1)
        <div class="card card-custom">
          <div class="card card-body">
            <div class="form-group">
              <label for="users">{{__('common.select_assigned_to')}}</label>
              <select id="assigned_to" name="users" class="form-control selectpicker" multiple="multiple">
                @foreach(getStaffMemberListArr() as $key=>$val)
                <option value="{{$key}}">{{$val}}</option>
                @endforeach
              </select>
            </div>

          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="todoTab" role="tabpanel">
    <x-actionlog-todo/>
  </div>
</div>
@endsection
@push('jsScripts')
var _alTargetContainer="#al-calendar-form";
$("body").on("beforeSubmit", "form#al-calendar-form", function () {
  App.blockUI({
    iconOnly: true,
    target: _alTargetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

  var form = $(this);
  // return false if form still have some validation errors
  if (form.find(".has-error").length) {
    return false;
  }
  url="{{url('action-log/save',['ltype'=>'calendar'])}}&mtype="+$("#alcalendareventform-module_type").val()+"&mid="+$("#alcalendareventform-module_id").val();
  // submit form
  $.ajax({
    url: url,
    type: "post",
    data: form.serialize(),
    dataType: "json",
    success: function (response) {
      if(response["success"]){
        toastr.success(response["success"]["msg"]);
        form.trigger("reset");
        window.closeModal();
        window.location.reload();
      }else{
        Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error"});
      }

      App.unblockUI($(_alTargetContainer));
    }
  });
  return false;
});
@endpush

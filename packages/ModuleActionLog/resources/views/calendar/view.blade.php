@extends('layouts.app')
@section('title', __('app.prospect.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'prospect'=>__('app.prospect.heading'),
'prospect/update/'.$model->id=>__('common.update_rec',['name'=>$model->reference_no]),
__('common.view')
]])
@endsection

@section('content')
<section class="card card-custom with-activity">
  <header class="card-header">
    <div class="card-title">
      <h3 class="card-label">{{ $model->reference_no }}</h3>
    </div>
    @if(checkActionAllowed('update'))
    <div class="card-toolbar">
      <a href="{{ url('/prospect/update',['id'=>$model->id]) }}" class="btn btn-icon btn-sm btn-hover-light-primary">
        <i class="flaticon-edit-1"></i>
      </a>
    </div>
    @endif
  </header>
  <div class="card-body multi-cards multi-tabs">
    <div class="row">
      <div class="col-sm-8">
        <section class="card card-custom card-border mb-3" data-card="true">
          <header class="card-header">
            <div class="card-title">
              <h3 class="card-label">{{ __('common.info') }}</h3>
            </div>
            <div class="card-toolbar">
              <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
                <i class="ki ki-arrow-down icon-nm"></i>
              </a>
            </div>
          </header>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <strong>{{ __('common.created:') }}</strong> {{ formatDateTime($model->created_at) }}
              </div>
              <div class="col-sm-6">
                <strong>{{ __('common.reference_no:') }}</strong> {{ $model->reference_no }}
              </div>
            </div>
            <x-customfields-custome-field-details :model="$model"/>
          </div>
        </section>
        <x-actionlog-attachment :model="$model"/>
      </div>
      <div class="col-sm-4">
        <x-actionlog-activity :model="$model"/>
      </div>
    </div>
  </div>
</section>
@endsection

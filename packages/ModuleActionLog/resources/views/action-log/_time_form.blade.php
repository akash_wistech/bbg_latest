<div class="form-group">
  {!! Form::textarea('comments', null, ['class' => 'form-control form-control-sm', 'rows'=>3, 'placeholder' => '']) !!}
</div>
<label>{{__('actionlog::activity.duration')}}</label>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        {{ Form::text('hours', null, ['maxlength'=>true,'placeholder'=>__('actionlog::activity.hours'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append">
          <span class="input-group-text">{{__('actionlog::activity.hours')}}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        {{ Form::text('minutes', null, ['maxlength'=>true,'placeholder'=>__('actionlog::activity.minutes'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append">
          <span class="input-group-text">{{__('actionlog::activity.minutes')}}</span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="tf-start_dt-input{{$timerForm->model->id.$timerForm->id}}" data-target-input="nearest">
        {{ Form::text('start_dt', null, ['maxlength'=>true,'data-target'=>'#tf-start_dt-input'.$timerForm->model->id.$timerForm->id,'placeholder'=>__('actionlog::activity.start_dt'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#tf-start_dt-input{{$timerForm->model->id.$timerForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="tf-end_dt-input{{$timerForm->model->id.$timerForm->id}}" data-target-input="nearest">
        {{ Form::text('end_dt', null, ['maxlength'=>true,'data-target'=>'#tf-end_dt-input'.$timerForm->model->id.$timerForm->id,'placeholder'=>__('actionlog::activity.end_dt'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#tf-end_dt-input{{$timerForm->model->id.$timerForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.visibility')}}</span>
        </div>
        {{ Form::select('visibility', getVisibilityListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-6 text-right">
    <button type="submit" class="btn btn-sm btn-success">{{__('common.save')}}</button>
  </div>
</div>

@push('jsScripts')
$("#tf-start_dt-input{{$timerForm->model->id.$timerForm->id}},#tf-end_dt-input{{$timerForm->model->id.$timerForm->id}}").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("#tf-start_dt-input{{$timerForm->model->id.$timerForm->id}}").on("change.datetimepicker", function (e) {
  $("#tf-end_dt-input{{$timerForm->model->id.$timerForm->id}}").datetimepicker("minDate", e.date);
});
$("#tf-end_dt-input{{$timerForm->model->id.$timerForm->id}}").on("change.datetimepicker", function (e) {
  $("#tf-start_dt-input{{$timerForm->model->id.$timerForm->id}}").datetimepicker("maxDate", e.date);
});
@endpush()

@extends('layouts.blank')
<div id="update-log">
  @if($model->rec_type=='comment')
    @php
      $commentForm->comments=$model->comments;
      $commentForm->visibility=$model->visibility;
    @endphp
    {!! Form::model($commentForm,['blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
    @include('actionlog::action-log._comment_form')
    {!! Form::close() !!}
  @endif

  @if($model->rec_type=='action')
    @php
    $actionForm->id = $model->id;
    $actionForm->model = $model->refModel();
    if($model->actionInfo()!=null){
      $actionForm->subject=$model->actionInfo()['subject'];
      $actionForm->due_date=$model->actionInfo()['due_date'];
    }
    $actionForm->priority=$model->priority;
    $actionForm->comments=$model->comments;
    $actionForm->visibility=$model->visibility;
    $actionForm->assign_to=$model->managerIdzArr();
    if($model->reminderInfo()!=null){
      $actionForm->reminder=1;
      $actionForm->reminder_to=$model->reminderInfo()['notify_to'];
      $actionForm->remind_time=$model->reminderInfo()['notify_time'];
    }
    if($model->calendarInfo()!=null){
      $actionForm->calendar_id=$model->calendarInfo()['calendar_id'];
    }
    @endphp
    {!! Form::model($actionForm,['blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit action-form']) !!}
    @include('actionlog::action-log._action_form')
    {!! Form::close() !!}
  @endif


  @if($model->rec_type=='call')
    @php
    $callForm->id = $model->id;
    $callForm->model = $model->refModel();
    $callForm->comments=$model->comments;
    $callNoteResponse = [''=>'-'];
    if($model->noteInfo()!=null){
      $callForm->qnoteAction=$model->noteInfo()['qa'];
      $callForm->qnoteResponse=$model->noteInfo()['qr'];
      $callNoteResponse = [''=>'-'] + getCallQuickNoteResponse()[$callForm->qnoteAction];
    }

    if($model->timeInfo()!=null){
      $callForm->hours=$model->timeInfo()['hours'];
      $callForm->minutes=$model->timeInfo()['minutes'];
      $callForm->start_dt=$model->timeInfo()['start_dt'];
      $callForm->end_dt=$model->timeInfo()['end_dt'];
    }
    $callForm->visibility=$model->visibility;
    @endphp
    {!! Form::model($callForm,['blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
    @include('actionlog::action-log._call_form')
    {!! Form::close() !!}
  @endif


  @if($model->rec_type=='timer')
    @php
      $timerForm->id = $model->id;
      $timerForm->model = $model->refModel();
      $timerForm->comments=$model->comments;
      if($model->timeInfo()!=null){
        $timerForm->hours=$model->timeInfo()['hours'];
        $timerForm->minutes=$model->timeInfo()['minutes'];
        $timerForm->start_dt=$model->timeInfo()['start_dt'];
        $timerForm->end_dt=$model->timeInfo()['end_dt'];
      }
      $timerForm->visibility=$model->visibility;
    @endphp
    {!! Form::model($timerForm,['blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit']) !!}
    @include('actionlog::action-log._time_form')
    {!! Form::close() !!}
  @endif


  @if($model->rec_type=='calendar')
    @php
      $calendarForm->id = $model->id;
      $calendarForm->model = $model->refModel();
      $calendarForm->priority=$model->priority;
      $calendarForm->comments=$model->comments;
      $calendarForm->assign_to=$model->managerIdzArr();
      if($model->calendarEventInfo()!=null){
        $calendarForm->event_start=$model->calendarEventInfo()['start_date'];
        $calendarForm->event_end=$model->calendarEventInfo()['end_date'];
        $calendarForm->color=$model->calendarEventInfo()['color_code'];
        $calendarForm->allday=$model->calendarEventInfo()['all_day'];
        $calendarForm->sub_type=$model->calendarEventInfo()['sub_type'];
        $calendarForm->status=$model->calendarEventInfo()['status'];
      }
      if($model->reminderInfo()!=null){
        $calendarForm->reminder=1;
        $calendarForm->reminder_to=$model->reminderInfo()['notify_to'];
        $calendarForm->remind_time=$model->reminderInfo()['notify_time'];
      }
      if($model->calendarInfo()!=null){
        $calendarForm->calendar_id=$model->calendarInfo()['calendar_id'];
      }
    @endphp
    {!! Form::model($calendarForm,['blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit calendar-event-form']) !!}
    @include('actionlog::action-log._calendar_form')
    {!! Form::close() !!}
  @endif
</div>

@push('jsScripts')
$(".popselect2").select2({
	allowClear: true,
	width: "100%",
});
@endpush

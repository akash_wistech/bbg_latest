<div class="form-group">
  {!! Form::label('comments', __('actionlog::activity.comment')) !!}
  {!! Form::textarea('comments', null, ['class' => 'form-control form-control-sm', 'rows'=>3, 'placeholder' => '']) !!}
</div>
<div class="row">
  <div class="col-sm-8">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.visibility')}}</span>
        </div>
        {{ Form::select('visibility', getVisibilityListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-4 text-right">
    <button type="submit" class="btn btn-sm btn-success">{{__('common.save')}}</button>
  </div>
</div>

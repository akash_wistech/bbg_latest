@extends('layouts.blank')
<div id="add-log">
  {!! Form::model($frmModel,['url'=>$saveUrl,'blockcntr'=>'alc','data-pjxcntr'=>'al-lv-container','class'=>'simple-ajax-submit'.$efcls]) !!}
  <input type="hidden" name="parent_id" value="{{$parent}}" />
  @if($type=='comment')
    @include('actionlog::action-log._comment_form')
  @endif

  @if($type=='action')
    @include('actionlog::action-log._action_form')
  @endif

  @if($type=='call')
    @php
    $callNoteResponse = [''=>'-'];
    @endphp
    @include('actionlog::action-log._call_form')
  @endif

  @if($type=='timer')
    @include('actionlog::action-log._time_form')
  @endif

  @if($type=='calendar')
    @include('actionlog::action-log._calendar_form')
  @endif

  {!! Form::close() !!}
</div>

@push('jsScripts')
$(".select2").select2({
	allowClear: true,
	width: "100%",
});
@endpush

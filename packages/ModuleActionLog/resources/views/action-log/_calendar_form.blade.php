<div class="form-group">
  {!! Form::textarea('comments', null, ['id'=>'alcallform-comments'.$calendarForm->model->id.$calendarForm->id,'class' => 'form-control form-control-sm', 'rows'=>3, 'placeholder' => __('actionlog::activity.comment')]) !!}
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="cef-start_dt-input{{$calendarForm->model->id.$calendarForm->id}}" data-target-input="nearest">
        {{ Form::text('event_start', null, ['maxlength'=>true,'data-target'=>'#cef-event_start-input'.$calendarForm->model->id.$calendarForm->id,'placeholder'=>__('actionlog::activity.event_start'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#cef-start_dt-input{{$calendarForm->model->id.$calendarForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="cef-end_dt-input{{$calendarForm->model->id.$calendarForm->id}}" data-target-input="nearest">
        {{ Form::text('event_end', null, ['maxlength'=>true,'data-target'=>'#cef-event_end-input'.$calendarForm->model->id.$calendarForm->id,'placeholder'=>__('actionlog::activity.event_end'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#cef-end_dt-input{{$calendarForm->model->id.$calendarForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.priority')}}</span>
        </div>
        {{ Form::select('priority', getPriorityListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-6 text-right">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.color')}}</span>
        </div>
        {{ Form::select('color', getCalendarColorListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.sub_type')}}</span>
        </div>
        {{ Form::select('sub_type', [''=>__('common.select')] + getEventSubTypesListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-6 text-right">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.event_status')}}</span>
        </div>
        {{ Form::select('status', [''=>__('common.select')] + getEventStatusListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-8">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.calendar_id')}}</span>
        </div>
        {{ Form::select('calendar_id', [''=>__('actionlog::activity.none')] + getMyCalendarListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
    	<div class="checkbox-inline">
    		<label class="checkbox">
    		{!! Form::checkbox('allday', 1, $calendarForm->allday==1 ? true : false, ['class'=>'form-group mb-0 lbl-mb-0']) !!}
    		<span></span>
        {{__('actionlog::activity.all_day')}}
      </label>
    	</div>
    </div>
  </div>
</div>
<div class="form-group">
  {!! Form::label('assign_to', __('actionlog::activity.assign_to')) !!}
  {{ Form::select('assign_to[]', getStaffMemberListArr(), null, ['id'=>'alactionform-assign_to'.$calendarForm->model->id.$calendarForm->id,'class'=>'form-control '.($calendarForm->id==null ? 'select2' : 'popselect2'),'multiple'=>'multiple']) }}
</div>
<div class="form-group">
	<div class="checkbox-inline">
		<label class="checkbox">
		{!! Form::checkbox('reminder', 1, $calendarForm->reminder==1 ? true : false, ['class'=>'form-group cbReminder2 mb-0 lbl-mb-0']) !!}
		<span></span>
    {{__('actionlog::activity.reminder')}}
  </label>
	</div>
</div>
<div class="calendarReminderInfo mb-3 {{$calendarForm->reminder==1 ? '' : 'd-none'}}">
  {{ __('actionlog::activity.create_notification_reminder_for') }}
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group input-group-sm mb-0">
        {{ Form::select('reminder_to', getRemindToListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
    <div class="col-sm-6 text-right">
      <div class="form-group input-group-sm mb-0">
        {{ Form::select('remind_time', getRemindTimeListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  {{ __('actionlog::activity.before_action_is_due') }}
</div>
  <div class="d-none">
    {{ Form::text('cmodule_id', null, ['id'=>'cef-module_id','maxlength'=>true,'class'=>'form-control','autocomplete'=>'off']) }}
  </div>
  <div class="form-group" style="display:none;">
    {!! Form::label('cmodule_type', __('actionlog::activity.association_type')) !!}
    {{ Form::select('cmodule_type', [''=>__('common.select')] + getModulesListArr(), null, ['id'=>'cef-module_type','class'=>'form-control']) }}
  </div>
  <div id="rlfld" style="display:none;">
    <div class="form-group cef-lookup">
      {!! Form::label('ref_lookup', __('actionlog::activity.ref_lookup')).'<span class="label-info"></span>' !!}
      {{ Form::text('ref_lookup', null, ['id'=>'ref_lookup-keyword','maxlength'=>true,'placeholder'=>__('actionlog::activity.keyword'),'class'=>'form-control form-control-sm','autocomplete'=>'off']) }}
    </div>
  </div>
<button type="submit" class="btn btn-sm btn-success">{{__('common.save')}}</button>

@if($calendarForm->model->module_type=='' || $calendarForm->model->module_type==null)
  @push('jsScripts')
  $("#ref_lookup-keyword").autocomplete({
    serviceUrl: "{{url('look-up-suggestion')}}",
    noCache: true,
    params: {
      "type": function(){
        return $("#cef-module_type").val();
      }
    },
    onSelect: function(suggestion) {
      if($("#cef-module_id").val()!=suggestion.data){
        $("#cef-module_id").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
      $("#cef-module_id").val("0");
    }
  });
  $("body").on("change", "#cef-module_type", function () {
    if($(this).val()!=""){
      var associationTypes={!!json_encode(getModulesListArr())!!};
      $(".cef-lookup .label-info").html(" "+associationTypes[$(this).val()]);
      $("#rlfld").show();
    }else{
      $("#cef-module_id").val("0");
      $("#rlfld").hide();
    }
  });
  @endpush()
@endif
@push('jsScripts')
$("#cef-start_dt-input{{$calendarForm->model->id.$calendarForm->id}},#cef-end_dt-input{{$calendarForm->model->id.$calendarForm->id}}").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("#cef-start_dt-input{{$calendarForm->model->id.$calendarForm->id}}").on("change.datetimepicker", function (e) {
  $("#cef-end_dt-input{{$calendarForm->model->id.$calendarForm->id}}").datetimepicker("minDate", e.date);
});
$("#cef-end_dt-input{{$calendarForm->model->id.$calendarForm->id}}").on("change.datetimepicker", function (e) {
  $("#cef-start_dt-input{{$calendarForm->model->id.$calendarForm->id}}").datetimepicker("maxDate", e.date);
});

$("body").on("click", ".cbReminder2", function () {
  if($(this).prop("checked")){
    $(this).parents(".calendar-event-form").find(".calendarReminderInfo").removeClass("d-none");
  }else{
    $(this).parents(".calendar-event-form").find(".calendarReminderInfo").addClass("d-none");
  }
});
@endpush()

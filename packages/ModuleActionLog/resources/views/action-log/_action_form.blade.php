
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      {{ Form::text('subject', null, ['maxlength'=>true,'placeholder'=>__('actionlog::activity.subject'),'class'=>'form-control form-control-sm']) }}
    </div>
  </div>
  <div class="col-sm-6 text-right">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="af-due_date-input{{$actionForm->model->id.$actionForm->id}}" data-target-input="nearest">
        {{ Form::text('due_date', null, ['maxlength'=>true,'placeholder'=>__('actionlog::activity.due_date'),'data-target'=>'#af-due_date-input'.$actionForm->model->id.$actionForm->id,'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#af-due_date-input{{$actionForm->model->id.$actionForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="ki ki-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  {!! Form::textarea('comments', null, ['class' => 'form-control form-control-sm', 'rows'=>3, 'placeholder' => '']) !!}
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.priority')}}</span>
        </div>
        {{ Form::select('priority', getPriorityListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-6 text-right">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.visibility')}}</span>
        </div>
        {{ Form::select('visibility', getVisibilityListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  {!! Form::label('comments', __('actionlog::activity.assign_to')) !!}
  {{ Form::select('assign_to[]', getStaffMemberListArr(), null, ['class'=>'form-control '.($actionForm->id==null ? 'select2' : 'popselect2'),'multiple'=>'multiple']) }}
</div>
<div class="form-group">
	<div class="checkbox-inline">
		<label class="checkbox">
		{!! Form::checkbox('reminder', 1, $actionForm->reminder==1 ? true : false, ['class'=>'form-group cbReminder mb-0 lbl-mb-0']) !!}
		<span></span>
    {{__('actionlog::activity.reminder')}}
  </label>
	</div>
</div>
<div class="actionReminderInfo mb-3 {{$actionForm->reminder==1 ? '' : 'd-none'}}">
  {{ __('actionlog::activity.create_notification_reminder_for') }}
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group input-group-sm mb-0">
        {{ Form::select('reminder_to', getRemindToListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
    <div class="col-sm-6 text-right">
      <div class="form-group input-group-sm mb-0">
        {{ Form::select('remind_time', getRemindTimeListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  {{ __('actionlog::activity.before_action_is_due') }}
</div>
<div class="mt-2">
  <div class="form-group">
    <div class="input-group input-group-sm">
      <div class="input-group-prepend">
        <span class="input-group-text">{{__('actionlog::activity.calendar_id')}}</span>
      </div>
      {{ Form::select('calendar_id', [''=>__('actionlog::activity.none')] + getMyCalendarListArr(), null, ['class'=>'form-control']) }}
    </div>
  </div>
</div>
<button type="submit" class="btn btn-sm btn-success">{{__('common.save')}}</button>

@push('jsScripts')

$("#af-due_date-input{{$actionForm->model->id.$actionForm->id}}").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});

$("body").on("click", ".cbReminder", function () {
  if($(this).prop("checked")){
    $(this).parents(".action-form").find(".actionReminderInfo").removeClass("d-none");
  }else{
    $(this).parents(".action-form").find(".actionReminderInfo").addClass("d-none");
  }
});
@endpush()

<div class="form-group">
  <div class="input-group input-group-sm">
    <div class="input-group-prepend">
      <span class="input-group-text">{{__('actionlog::activity.qnoteAction')}}</span>
    </div>
    {{ Form::select('qnoteAction', [''=>''] + getCallQuickNoteAction(), null, ['id'=>'alcallform-qnoteaction'.$callForm->model->id.$callForm->id,'class'=>'form-control']) }}
    &nbsp;:&nbsp;
    {{ Form::select('qnoteResponse', $callNoteResponse, null, ['id'=>'alcallform-qnoteresponse'.$callForm->model->id.$callForm->id,'class'=>'form-control mb-0']) }}
  </div>
</div>
<div class="form-group">
  {!! Form::textarea('comments', null, ['id'=>'alcallform-comments'.$callForm->model->id.$callForm->id,'class' => 'form-control form-control-sm', 'rows'=>3, 'placeholder' => __('actionlog::activity.comment')]) !!}
</div>
<!--label>{{__('actionlog::activity.duration')}}</label-->
<div class="row call-duration-r">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        {{ Form::text('hours', null, ['maxlength'=>true,'placeholder'=>__('actionlog::activity.hours'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append">
          <span class="input-group-text">{{__('actionlog::activity.hours')}}</span>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        {{ Form::text('minutes', null, ['maxlength'=>true,'placeholder'=>__('actionlog::activity.minutes'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append">
          <span class="input-group-text">{{__('actionlog::activity.minutes')}}</span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="cf-start_dt-input{{$callForm->model->id.$callForm->id}}" data-target-input="nearest">
        {{ Form::text('start_dt', null, ['maxlength'=>true,'data-target'=>'#cf-start_dt-input'.$callForm->model->id.$callForm->id,'placeholder'=>__('actionlog::activity.start_dt'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#cf-start_dt-input{{$callForm->model->id.$callForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm date" id="cf-end_dt-input{{$callForm->model->id.$callForm->id}}" data-target-input="nearest">
        {{ Form::text('end_dt', null, ['maxlength'=>true,'data-target'=>'#cf-end_dt-input'.$callForm->model->id.$callForm->id,'placeholder'=>__('actionlog::activity.end_dt'),'class'=>'form-control','autocomplete'=>'off']) }}
        <div class="input-group-append" data-target="#cf-end_dt-input{{$callForm->model->id.$callForm->id}}" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <div class="input-group input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">{{__('actionlog::activity.visibility')}}</span>
        </div>
        {{ Form::select('visibility', getVisibilityListArr(), null, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
    	<div class="checkbox-inline">
    		<label class="checkbox">
    		{!! Form::checkbox('follow_up', 1, $actionForm->follow_up==1 ? true : false, ['class'=>'form-group cbFollowUp mb-0 lbl-mb-0']) !!}
    		<span></span>
        {{__('actionlog::activity.follow_up')}}
      </label>
    	</div>
    </div>
  </div>
</div>
<div class="followUpInfoRow{{$actionForm->follow_up==1 ? '' : ' d-none'}}">
  <hr />
  <strong>{{__('actionlog::activity.follow_up_info')}}</strong>
  <div class="form-group">
    {!! Form::textarea('followup_comments', null, ['class' => 'form-control form-control-sm', 'rows'=>3, 'placeholder' => __('actionlog::activity.followup_comment')]) !!}
  </div>
  <div class="form-group">
    <div class="input-group input-group-sm date" id="ccef-start_dt-input{{$actionForm->model->id.$actionForm->id}}" data-target-input="nearest">
      {{ Form::text('followup_date', null, ['maxlength'=>true,'data-target'=>'#ccef-event_start-input'.$actionForm->model->id.$actionForm->id,'placeholder'=>__('actionlog::activity.followup_start'),'class'=>'form-control','autocomplete'=>'off']) }}
      <div class="input-group-append" data-target="#ccef-start_dt-input{{$actionForm->model->id.$actionForm->id}}" data-toggle="datetimepicker">
          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <div class="input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text">{{__('actionlog::activity.priority')}}</span>
          </div>
          {{ Form::select('followup_priority', getPriorityListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="col-sm-6 text-right">
      <div class="form-group">
        <div class="input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text">{{__('actionlog::activity.color')}}</span>
          </div>
          {{ Form::select('followup_color', getCalendarColorListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <div class="input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text">{{__('actionlog::activity.sub_type')}}</span>
          </div>
          {{ Form::select('followup_sub_type', [''=>__('common.select')] + getEventSubTypesListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="col-sm-6 text-right">
      <div class="form-group">
        <div class="input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text">{{__('actionlog::activity.event_status')}}</span>
          </div>
          {{ Form::select('followup_status', [''=>__('common.select')] + getEventStatusListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-8">
      <div class="form-group">
        <div class="input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text">{{__('actionlog::activity.calendar_id')}}</span>
          </div>
          {{ Form::select('followup_calendar_id', [''=>__('actionlog::activity.none')] + getMyCalendarListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="form-group">
      	<div class="checkbox-inline">
      		<label class="checkbox">
      		{!! Form::checkbox('followup_allday', 1, $actionForm->allday==1 ? true : false, ['class'=>'form-group mb-0 lbl-mb-0']) !!}
      		<span></span>
          {{__('actionlog::activity.all_day')}}
        </label>
      	</div>
      </div>
    </div>
  </div>
  <div class="form-group">
    {!! Form::label('followup_assign_to', __('actionlog::activity.assign_to')) !!}
    {{ Form::select('followup_assign_to[]', getStaffMemberListArr(), null, ['id'=>'alactionform-followup_assign_to'.$actionForm->model->id.$actionForm->id,'class'=>'form-control '.($actionForm->id==null ? 'select2' : 'popselect2'),'multiple'=>'multiple']) }}
  </div>
  <div class="form-group">
  	<div class="checkbox-inline">
  		<label class="checkbox">
  		{!! Form::checkbox('followup_reminder', 1, $actionForm->reminder==1 ? true : false, ['class'=>'form-group cbFollowUpReminder mb-0 lbl-mb-0']) !!}
  		<span></span>
      {{__('actionlog::activity.reminder')}}
    </label>
  	</div>
  </div>
  <div class="calendarReminderInfo mb-3 {{$actionForm->reminder==1 ? '' : 'd-none'}}">
    {{ __('actionlog::activity.create_notification_reminder_for') }}
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group input-group-sm mb-0">
          {{ Form::select('followup_reminder_to', getRemindToListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-sm-6 text-right">
        <div class="form-group input-group-sm mb-0">
          {{ Form::select('followup_remind_time', getRemindTimeListArr(), null, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    {{ __('actionlog::activity.before_action_is_due') }}
  </div>
</div>
<button type="submit" class="btn btn-sm btn-success">{{__('common.save')}}</button>

@push('jsScripts')
$("#cf-start_dt-input{{$callForm->model->id.$callForm->id}},#cf-end_dt-input{{$callForm->model->id.$callForm->id}}").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("#cf-start_dt-input{{$callForm->model->id.$callForm->id}}").on("change.datetimepicker", function (e) {
  $("#cf-end_dt-input{{$callForm->model->id.$callForm->id}}").datetimepicker("minDate", e.date);
});
$("#cf-end_dt-input{{$callForm->model->id.$callForm->id}}").on("change.datetimepicker", function (e) {
  $("#cf-start_dt-input{{$callForm->model->id.$callForm->id}}").datetimepicker("maxDate", e.date);
});

{!!getCallActionResponseList()!!}
$("body").on("change", "#alcallform-qnoteaction{{$callForm->model->id.$callForm->id}}", function () {
  if($(this).val()=="2"){
    $(this).parents("form").find(".call-duration-r").addClass("d-none");
    $(this).parents("form").find(".call-duration-r input").val(0);
  }else{
    $(this).parents("form").find(".call-duration-r").removeClass("d-none");
    $(this).parents("form").find(".call-duration-r input").val("");
  }
  html="<option value=\"\">-</option>";
  resOptions=alLogCallActionResponses[$(this).val()];
  jQuery.each(resOptions, function(i, val){
    html+="<option value=\""+i+"\">"+val+"</option>";
  });
  $("#alcallform-qnoteresponse{{$callForm->model->id.$callForm->id}}").html(html);
});
$("body").on("change", "#alcallform-qnoteresponse{{$callForm->model->id.$callForm->id}}", function () {
  selValue=resOptions=alLogCallActionResponses[$("#alcallform-qnoteaction{{$callForm->model->id.$callForm->id}}").val()][$(this).val()];
  $("#alcallform-comments{{$callForm->model->id.$callForm->id}}").val(selValue);
});
$("body").on("click", ".cbFollowUp", function () {
  if($(this).prop("checked")){
    $(this).parents("form").find(".followUpInfoRow").removeClass("d-none");
  }else{
    $(this).parents("form").find(".followUpInfoRow").addClass("d-none");
  }
});

$("#ccef-start_dt-input{{$callForm->model->id.$callForm->id}}").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD hh:mm:ss",
  icons: {
    time: "fa fa-clock",
  },
});
$("body").on("click", ".cbFollowUpReminder", function () {
  if($(this).prop("checked")){
    $(this).parents("form").find(".calendarReminderInfo").removeClass("d-none");
  }else{
    $(this).parents("form").find(".calendarReminderInfo").addClass("d-none");
  }
});
@endpush()

<?php
return [
  'heading' => 'Calendar',
  'saved' => 'Calendar saved successfully',
  'notsaved' => 'Error while saving calendar',
  'assign_task_to' => 'Assign Task'
];

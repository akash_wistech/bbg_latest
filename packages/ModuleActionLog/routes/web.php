<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\ModuleActionLog\Http\Controllers\ActionLogController;
use NaeemAwan\ModuleActionLog\Http\Controllers\CalendarController;
use NaeemAwan\ModuleActionLog\Http\Controllers\TodoController;


Route::group(['middleware' => ['web','auth']], function () {
  Route::get('/look-up-suggestion', [ActionLogController::class, 'lookUp']);
  Route::match(['get','post'],'/action-log/history/{module}/{id}/', [ActionLogController::class, 'loadActivityHistory']);
  Route::match(['get','post'],'/action-log/save/{module}/{type}/{id}', [ActionLogController::class, 'save']);
  Route::match(['get','post'],'/action-log/update/{id}', [ActionLogController::class, 'update']);
  Route::post('/action-log/completed/{id}', [ActionLogController::class, 'completed']);
  Route::post('/action-log/drop-upload', [ActionLogController::class, 'dropUpload']);
  Route::post('/action-log/save-attachment/{type}/{id}', [ActionLogController::class, 'saveALAttachment']);
  Route::post('/action-log/destroy/{id}', [ActionLogController::class, 'destroy']);

  Route::get('/activity-log/{module}/{type}/{id}', [ActionLogController::class, 'loadActivityForm']);

  Route::match(['get', 'post'], 'assign-task/{id}', [ActionLogController::class,'assignTask']);


  Route::get('/calendar', [CalendarController::class,'index']);
  Route::get('/calendar/data-table-data', [CalendarController::class,'datatableData']);
  Route::match(['get', 'post'], '/calendar/create', [CalendarController::class,'create']);
  Route::match(['get', 'post'], '/calendar/update/{id}', [CalendarController::class,'update']);
  Route::post('/calendar/delete/{id}', [CalendarController::class,'destroy']);
  Route::match(['get', 'post'], '/calendar/data-items', [CalendarController::class,'calJsonData']);
  Route::match(['get', 'post'], '/calendar/todo-items/{start}/{end}', [TodoController::class,'todoData']);
  Route::match(['get', 'post'], '/calendar/data-items-show', [CalendarController::class,'showJsonData']);
});

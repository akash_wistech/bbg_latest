<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogAttachment extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_attachment', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->text('file_name')->nullable();
      $table->text('file_title')->nullable();
      
      $table->index('action_log_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_attachment');
  }
}

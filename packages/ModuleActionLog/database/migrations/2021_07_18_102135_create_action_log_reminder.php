<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogReminder extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_reminder', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->char('notify_to',10)->nullable();
      $table->integer('notify_time')->default(0)->nullable();
      $table->timestamp('final_due_at')->nullable();
      $table->tinyInteger('notified')->default(0)->nullable();

      $table->index('action_log_id');
      $table->index('notify_to');
      $table->index('notify_time');
      $table->index('final_due_at');
      $table->index('notified');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_reminder');
  }
}

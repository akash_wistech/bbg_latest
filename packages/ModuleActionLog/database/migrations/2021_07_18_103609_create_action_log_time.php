<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogTime extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_time', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->timestamp('start_dt')->nullable();
      $table->timestamp('end_dt')->nullable();
      $table->integer('hours')->default(0)->nullable();
      $table->char('minutes',2)->nullable();

      $table->index('action_log_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_time');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogManager extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_manager', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->integer('staff_id')->default(0)->nullable();

      $table->index('action_log_id');
      $table->index('staff_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_manager');
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLog extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log', function (Blueprint $table) {
      $table->id();
      $table->char('module_type',15)->nullable();
      $table->integer('module_id')->default(0)->nullable();
      $table->char('rec_type',15)->nullable();
      $table->text('comments')->nullable();
      $table->tinyInteger('priority')->default(1)->nullable();
      $table->tinyInteger('visibility')->default(1)->nullable();
      $table->timestamps();
      $table->timestamp('deleted_at')->nullable();
      $table->integer('created_by')->nullable();
      $table->integer('updated_by')->nullable();
      $table->integer('deleted_by')->nullable();

      $table->index('module_type');
      $table->index('module_id');
      $table->index('rec_type');
      $table->index('deleted_at');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log');
  }
}

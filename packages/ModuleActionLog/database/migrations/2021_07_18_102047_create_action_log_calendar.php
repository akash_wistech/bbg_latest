<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogCalendar extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_calendar', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->integer('calendar_id')->default(0)->nullable();

      $table->index('action_log_id');
      $table->index('calendar_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_calendar');
  }
}

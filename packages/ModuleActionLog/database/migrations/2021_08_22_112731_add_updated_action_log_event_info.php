<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUpdatedActionLogEventInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('action_log_event_info', function (Blueprint $table) {
          $table->tinyInteger('updated')->default(0)->nullable()->index();
          $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('action_log_event_info', function (Blueprint $table) {
          $table->dropColumn('updated');
          $table->dropColumn('updated_at');
        });
    }
}

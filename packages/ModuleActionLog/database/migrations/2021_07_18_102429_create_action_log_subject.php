<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogSubject extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_subject', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->string('subject',255)->nullable();
      $table->timestamp('due_date')->nullable();
      $table->tinyInteger('status')->default(0)->nullable();
      $table->timestamp('completed_at')->nullable();

      $table->index('action_log_id');
      $table->index('status');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_subject');
  }
}

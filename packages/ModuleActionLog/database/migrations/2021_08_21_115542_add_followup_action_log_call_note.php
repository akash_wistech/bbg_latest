<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFollowupActionLogCallNote extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::table('action_log_call_note', function (Blueprint $table) {
      $table->tinyInteger('follow_up')->default(0)->nullable()->index();
      $table->integer('follow_up_id')->default(0)->nullable()->index();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    if (Schema::hasColumn('action_log_call_note', 'follow_up'))
    {
      Schema::table('action_log_call_note', function (Blueprint $table)
      {
        $table->dropColumn('follow_up');
      });
    }
    if (Schema::hasColumn('action_log_call_note', 'follow_up_id'))
    {
      Schema::table('action_log_call_note', function (Blueprint $table)
      {
        $table->dropColumn('follow_up_id');
      });
    }
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogEventInfo extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('action_log_event_info', function (Blueprint $table) {
      $table->integer('action_log_id')->default(0)->nullable();
      $table->timestamp('start_date')->nullable();
      $table->timestamp('end_date')->nullable();
      $table->char('color_code',10)->nullable();
      $table->tinyInteger('all_day')->default(0)->nullable();
      $table->integer('sub_type')->default(0)->nullable();
      $table->tinyInteger('status')->default(0)->nullable();

      $table->index('action_log_id');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('action_log_event_info');
  }
}

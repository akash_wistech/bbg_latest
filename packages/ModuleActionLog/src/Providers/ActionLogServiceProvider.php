<?php

namespace NaeemAwan\ModuleActionLog\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Observers\ActionLogObserver;
use NaeemAwan\ModuleActionLog\View\Components\Activity;
use NaeemAwan\ModuleActionLog\View\Components\ActivityHistory;
use NaeemAwan\ModuleActionLog\View\Components\Attachment;
use NaeemAwan\ModuleActionLog\View\Components\FullCalendar;
use NaeemAwan\ModuleActionLog\View\Components\Todo;

class ActionLogServiceProvider extends ServiceProvider
{
  public function boot()
  {
    ActionLog::observe(ActionLogObserver::class);

    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'actionlog');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'actionlog');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/actionlog'),
    ],'actionlog-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'actionlog-migrations');

    $this->loadViewComponentsAs('actionlog', [
        Activity::class,
        ActivityHistory::class,
        Attachment::class,
        FullCalendar::class,
        Todo::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

<?php
namespace NaeemAwan\ModuleActionLog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class ALActionForm extends ALForms
{
  public $subject,$due_date,$assign_to,$priority,$reminder,$reminder_to,$remind_time,$calendar_id;

  /**
  * Save Action Log Action Info
  */
  public function save()
  {
    if ($this->validate()) {
      $checkAlready=$this->checkAlready;
      if(!$checkAlready->exists()){
        if($this->id>0){
          $comment = ActionLog::findOne($this->id);
          $logMsg=''.Yii::$app->user->identity->name.' update a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id.' with id '.$this->id;
        }else{
          $comment = new ActionLog;
          $comment->module_type=$this->module_type;
          $comment->module_id=$this->module_id;
          $comment->rec_type=$this->rec_type;
          $logMsg=''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id;
        }
        $comment->subject=$this->subject;
        $comment->due_date=$this->due_date;
        $comment->comments=$this->comments;
        $comment->assign_to=$this->assign_to;
        $comment->priority=$this->priority;
        $comment->visibility=$this->visibility;
        $comment->calendar_id=$this->calendar_id;
        if($comment->save()){
          if($this->reminder==1){
            $final_due_at=date("Y-m-d H:i:s",strtotime("-".$this->remind_time." Minute",strtotime($this->due_date)));
            $reminderInfo=ActionLogReminder::find()->where(['action_log_id'=>$comment->id])->one();
            if($reminderInfo==null){
              $reminderInfo=new ActionLogReminder;
              $reminderInfo->action_log_id=$comment->id;
            }
            $reminderInfo->notify_to=$this->reminder_to;
            $reminderInfo->notify_time=$this->remind_time;
            $reminderInfo->final_due_at=$final_due_at;
            $reminderInfo->save();
          }else{
            $reminderInfo=ActionLogReminder::find()->where(['action_log_id'=>$comment->id])->one();
            if($reminderInfo!=null){
              $reminderInfo->delete();
            }
          }
        }else{
          if($comment->hasErrors()){
            foreach($comment->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  $this->addError('',$val);
                  return false;
                }
              }
            }
          }
        }
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, $logMsg);
      }else{
        return true;
      }
      return true;
    }
    return false;
  }
}

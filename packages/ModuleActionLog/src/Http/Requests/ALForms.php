<?php
namespace NaeemAwan\ModuleActionLog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Request;
use NaeemAwan\ModuleActionLog\Models\ActionLog;

class ALForms
{
  public $model,$module_type,$rec_type,$id,$subject,$due_date,$comments,$priority,$visibility,$assign_to;
  public $qnoteAction,$qnoteResponse,$hours,$minutes,$start_dt,$end_dt;
  public $follow_up,$followup_comments,$followup_date,$followup_priority,$followup_color,$followup_sub_type,$followup_status;
  public $followup_calendar_id,$followup_allday,$followup_assign_to,$followup_reminder,$followup_reminder_to,$followup_remind_time;
  public $reminder,$reminder_to,$remind_time,$calendar_id;
  public $event_start,$event_end,$color,$sub_type,$status,$allday;

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
  public function rules()
  {
    //Comments
    if($this->rec_type=='comment'){
      $rules['comments'] = 'required|string';
      $rules['visibility'] = 'required|integer';
    }
    if($this->rec_type=='action'){
      $rules['subject'] = 'required|string|max:255';
    }
    if($this->rec_type=='call'){
      $rules['qnoteAction'] = 'required|integer';
      $rules['qnoteResponse'] = 'required|integer';
      $rules['hours'] = 'required|string';
      $rules['minutes'] = 'required|string';
      $rules['start_dt'] = 'required|string';
      $rules['end_dt'] = 'required|string';
    }
    if($this->rec_type=='timer'){
      $rules['comments'] = 'required|string';
      $rules['hours'] = 'required|string';
      $rules['minutes'] = 'required|string';
      $rules['start_dt'] = 'required|string';
      $rules['end_dt'] = 'required|string';
    }
    if($this->rec_type=='calendar'){
      $rules['comments'] = 'required|string';
    }
    return $rules;
  }
}

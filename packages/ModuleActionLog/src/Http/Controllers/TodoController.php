<?php
namespace NaeemAwan\ModuleActionLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Models\ActionLogCalendar;
use NaeemAwan\ModuleActionLog\Models\ActionLogManager;
use NaeemAwan\ModuleActionLog\Models\ActionLogEventInfo;
use NaeemAwan\ModuleActionLog\Models\ActionLogSubject;
use NaeemAwan\ModuleActionLog\Models\Calendar;

class TodoController extends Controller
{
  /**
  * Show the form for editing the specified resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function todoData(Request $request,$start,$end)
  {
    if($start=='-n')$start='01-01-1970';

    $limit=20;
    if($request->limit)$limit=$request->limit;

    $html='';

    $query = getActivityQuery($start,$end,$request);
    $results = $query->paginate($limit);

    $totalRecords = $results->total();
    $totalPages = $results->lastPage();
    $pagesDone = $results->currentPage();
    $nextPageUrl = $results->nextPageUrl();

    $html='';
    if($results!=null){
      $calenderSubTypeIconsArr = getEventSubTypesListIconArr();
      $calenderSubTypeHeadingArr = getEventSubTypesHeadingListArr();
      foreach($results as $result){
        $html.= getActivityItemOutput($result,$calenderSubTypeIconsArr,$calenderSubTypeHeadingArr,'html');
      }
    }

    $response['append']['contentHtml'] = $html;
    if($totalPages>0){
      $percentage=round(($pagesDone/$totalPages)*100);
    }else{
      $percentage=100;
    }
    $response['progress']=['totalRecords'=>$totalRecords,'totalPages'=>$totalPages,'pagesDone'=>$pagesDone,'percentage'=>$percentage];
    if($nextPageUrl!=null){
      $response['progress']['url']=$nextPageUrl;
    }
    return new JsonResponse($response);
  }
}

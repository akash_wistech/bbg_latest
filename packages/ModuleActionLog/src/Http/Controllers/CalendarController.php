<?php
namespace NaeemAwan\ModuleActionLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Models\ActionLogCalendar;
use NaeemAwan\ModuleActionLog\Models\ActionLogManager;
use NaeemAwan\ModuleActionLog\Models\ActionLogEventInfo;
use NaeemAwan\ModuleActionLog\Models\ActionLogSubject;
use NaeemAwan\ModuleActionLog\Models\Calendar;

class CalendarController extends Controller
{
  /**
  * Display listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $query = DB::table((new Calendar)->getTable());
    permissionListTypeFilter((new Calendar)->moduleTypeId,(new Calendar)->getTable(),'calendar',$query);
    $results = $query->get();
    return view('actionlog::calendar.index',compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Calendar;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title' => 'required|string,max:100',
      ]);
      $model= new Calendar();
      $model->title = $request->title;
      if ($model->save()) {
        saveModuleManagerField($model,$request,'form');
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::calendar.saved')]]);
        }else{
          return redirect('calendar')->with('success', __('actionlog::calendar.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('actionlog::calendar.notsaved'),]]);
        }else{
          return redirect('calendar')->with('error', __('actionlog::calendar.notsaved'));
        }
      }
    }
    return view('actionlog::calendar.create',compact('model'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Calendar  $calendar
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = Calendar::where('id', $id)->first();

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required|string,max:100',
      ]);

      $model= Calendar::find($request->id);
      $model->title = $request->title;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        saveModuleManagerField($model,$request,'form');
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::calendar.saved'),]]);
        }else{
          return redirect('calendar')->with('success', __('actionlog::calendar.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('actionlog::calendar.notsaved'),]]);
        }else{
          return redirect('calendar')->with('error', __('actionlog::calendar.notsaved'));
        }
      }
    }

    return view('actionlog::calendar.update', [
      'model'=>$model,
    ]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function calJsonData(Request $request)
  {
    $start=parseDateTime($request->start);
    $end=parseDateTime($request->end);
    $start=stripTime($request->start);
    $end=stripTime($request->end);
    $output_arrays=[];

    $query = getActivityQuery($start,$end,$request);
    $results = $query->get();

    if($results!=null){
      $calenderSubTypeIconsArr = getEventSubTypesListIconArr();
      $calenderSubTypeHeadingArr = getEventSubTypesHeadingListArr();
      foreach($results as $result){
        $arr = getActivityItemOutput($result,$calenderSubTypeIconsArr,$calenderSubTypeHeadingArr,'arr');
        if($arr!=null){
          $output_arrays[] = $arr;
        }

        // dd($output_arrays);

      }
    }

    // dd($output_arrays);
    // Send JSON to the client.
    return new JsonResponse($output_arrays);
  }

  public function showJsonData(Request $request){
    $output_arrays=[];

    $query = DB::table((new ActionLog)->getTable());

    $query->select([
      (new ActionLog)->getTable().'.id',
      (new ActionLog)->getTable().'.module_type',
      (new ActionLog)->getTable().'.module_id',
      (new ActionLog)->getTable().'.rec_type',
      (new ActionLog)->getTable().'.comments',
      (new ActionLog)->getTable().'.priority',
      (new ActionLog)->getTable().'.visibility',
      (new ActionLogEventInfo)->getTable().'.start_date',
      (new ActionLogEventInfo)->getTable().'.end_date',
      (new ActionLogEventInfo)->getTable().'.color_code',
      (new ActionLogEventInfo)->getTable().'.all_day',
      (new ActionLogEventInfo)->getTable().'.sub_type',
      (new ActionLogEventInfo)->getTable().'.status',
      (new ActionLogSubject)->getTable().'.subject',
      (new ActionLogSubject)->getTable().'.due_date',
    ]);

    $query->leftJoin((new ActionLogEventInfo)->getTable(),(new ActionLogEventInfo)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id");


    $query->leftJoin((new ActionLogSubject)->getTable(),(new ActionLogSubject)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id");

    $query->whereNull('deleted_at')->whereIn((new ActionLog)->getTable().'.rec_type',['action','calendar']);
    $user = Auth::user()->user_type;
    if ($user == 20) {
      $query->whereIn('id',function($query){
        $query->from((new ActionLogManager)->getTable())
        ->selectRaw('action_log_id');
        // dd($query);
      });
      }
      else{
       $query->whereIn('id',function($query){
        $query->from((new ActionLogManager)->getTable())
        ->selectRaw('action_log_id')
        ->where('staff_id', Auth::user()->id);
      });
     }


     if($request->calender_id!=null){
      $calendarId = $request->calender_id;
      $query->whereIn('id',function($query) use ($calendarId){
        $query->from((new ActionLogCalendar)->getTable())
        ->selectRaw('action_log_id')
        ->whereIn('calendar_id', explode(",",$calendarId));
      });
    }
    $results = $query->get();

    if($results!=null){
      $calenderSubTypeIconsArr = getEventSubTypesListIconArr();
      $calenderSubTypeHeadingArr = getEventSubTypesHeadingListArr();
      foreach($results as $result){
        $model = getModuleRow($result->module_type,$result->module_id);
        if($model!=null){
          $moduleName = getModuleNameFieldValue($model);

          $description = getModuleDetailViewForCalendar($result,$model,12);
          $descriptionPop = getModuleDetailViewForCalendar($result,$model,6);
          $managersHtml = getCalendarAssignedUsersSymbol($result->id,40);

          $popupBtns='';

          $btnsArr=[];
          $btnsArr[]=['icon'=>'<i class="fa fa-edit"></i>','url'=>getModelUrlPre()[$result->module_type].'/update/'.$model->id,'title'=>__('common.update')];
          $btnsArr[]=['icon'=>'<i class="fa fa-table"></i>','url'=>getModelUrlPre()[$result->module_type].'/view/'.$model->id,'title'=>__('common.view')];
          $btnsArr = getCalendarAssignModuleBtn($btnsArr,$result);
          $btnsArr = getActivityBtns($btnsArr,$model,$result);
          $btnsArr[]=['icon'=>'<i class="fa fa-history"></i>','url'=>'','title'=>__('common.history'),'otherOpts'=>'onclick="loadActivityHistory(\''.$result->module_type.'\','.$model->id.')"'];

          foreach($btnsArr as $key=>$val){
            $url = ($val['url']!='' ? url($val['url']) : 'javascript:;');
            $classes = 'btn btn-clean btn-icon btn-md mr-1'.(isset($val['class'])!='' ? ' '.$val['class'] : '');
            $otherAtrs = (isset($val['otherOpts'])!='' ? ' '.$val['otherOpts'] : '');
            $tooltip = (isset($val['title']) ? ' data-toggle="tooltip" data-placement="top" data-original-title="'.$val['title'].'"' : '');

            $popupBtns.='      <a href="'.$url.'" class="'.$classes.'" data-mid="secondary-modal"'.$otherAtrs.$tooltip.'>';
            $popupBtns.='        '.$val['icon'];
            $popupBtns.='      </a>';
          }


          $viewUrl = url(getModelUrlPre()[$result->module_type].'/view/'.$result->module_id);
          if($result->rec_type=='action'){
            $output_arrays[] = [
              'id'=>$result->id,
              'title'=>trim($result->subject),
              'start'=>trim($result->due_date),
              'className'=>'',
              'description'=>$description,
              'popdescription'=>$descriptionPop,
              'popmanagers'=>$managersHtml,
              'popupBtns'=>$popupBtns,
              'viewUrl'=>$viewUrl,
              // 'url'=>$url,
            ];
          }
          elseif($result->rec_type=='calendar'){
            $title = trim($result->comments);

            if(isset($calenderSubTypeHeadingArr[$result->sub_type])){
              $subType = $calenderSubTypeHeadingArr[$result->sub_type];
              $title = __('actionlog::activity.'.$subType,['name'=>$moduleName]);
            }

            $output_arrays[] = [
              'id'=>$result->id,
              'imageurl'=>'<i class="'.$calenderSubTypeIconsArr[$result->sub_type].' text-white"></i> ',
              'title'=>$title,
              'start'=>trim($result->start_date),
              'end'=>trim($result->end_date),
              'color'=>trim($result->color_code),
              'allDay'=>trim($result->all_day),
              'className'=>'',
              'description'=>$description,
              'popdescription'=>$descriptionPop,
              'popmanagers'=>$managersHtml,
              'popupBtns'=>$popupBtns,
              'viewUrl'=>$viewUrl,
              // 'url'=>$url,
            ];
          }
        }
      }
    }

    return response()->json([
      'calendarData' => $output_arrays,

    ]);

  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Calendar  $calendar
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Calendar::where('id', $id)->first();
  }
}

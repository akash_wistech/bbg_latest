<?php
namespace NaeemAwan\ModuleActionLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Validator;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Models\Opportunity;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Http\Requests\ALForms;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCommentForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALActionForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCallForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALTimerForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCalendarEventForm;
use NaeemAwan\ModuleActionLog\Models\ActionLogSubject;

class ActionLogController extends Controller
{
  /**
  * Show activity form
  *
  * @return \Illuminate\Http\Response
  */
  public function loadActivityForm(Request $request,$module,$type,$id)
  {
    $modelClass = getModelClass()[$module];
    $model = $modelClass::where('id',$id)->first();

    $commentForm=new ALCommentForm;
    $actionForm=new ALActionForm;
    $actionForm->model = $model;
    $callForm=new ALCallForm;
    $callForm->model = $model;
    $timerForm=new ALTimerForm;
    $timerForm->model = $model;
    $calendarForm=new ALCalendarEventForm;
    $calendarForm->model = $model;

    $efcls = '';
    if($type=='comment')$frmModel = $commentForm;
    if($type=='action'){
      $frmModel = $actionForm;
      $efcls=' action-form';
    }
    if($type=='call')$frmModel = $callForm;
    if($type=='timer')$frmModel = $timerForm;
    if($type=='calendar'){
      $frmModel = $calendarForm;
      $efcls=' calendar-event-form';
    }

    $saveUrl = url('/action-log/save/'.$module.'/'.$type.'/'.$model->id);

    return view('actionlog::action-log.load_form', [
      'model'=>$model,
      'module'=>$module,
      'parent'=>$request->parent,
      'type'=>$type,
      'commentForm'=>$commentForm,
      'actionForm'=>$actionForm,
      'callForm'=>$callForm,
      'timerForm'=>$timerForm,
      'calendarForm'=>$calendarForm,
      'frmModel'=>$frmModel,
      'saveUrl'=>$saveUrl,
      'efcls'=>$efcls,
    ]);
  }

  /**
  * Show and save the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function save(Request $request,$module,$type,$id)
  {
    if($type=='comment'){
      $validator = $request->validate([
        'comments' => 'required|string',
        'visibility' => 'required|integer',
      ]);
    }
    if($type=='action'){
      $validator = $request->validate([
        'subject' => 'required|string|max:255',
      ]);
    }
    if($type=='call'){
      $validator = $request->validate([
        'qnoteAction' => 'required|integer',
        'qnoteResponse' => 'required|integer',
        'follow_up' => 'integer',
        'hours' => 'required|string',
        'minutes' => 'required|string',
        'start_dt' => 'required|string',
        'end_dt' => 'required|string',
      ]);
    }
    if($type=='timer'){
      $validator = $request->validate([
        'comments' => 'required|string',
        'hours' => 'required|string',
        'minutes' => 'required|string',
        'start_dt' => 'required|string',
        'end_dt' => 'required|string',
      ]);
    }
    if($type=='calendar'){
      $validator = $request->validate([
        'comments' => 'required|string',
      ]);
    }

    $checkTimeArr=getStartEndDateTimeFromNow("-5 second");
    $checkAlready = ActionLog::where([
      'module_type'=>$module,
      'module_id'=>$id,
      'rec_type'=>$type,
      'created_by'=>Auth::user()->id,
    ])
    ->whereBetween('created_at',[$checkTimeArr['start'],$checkTimeArr['end']])
    ->first();

    if($checkAlready!=null){
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('actionlog::activity.slowdown'),]]);
    }else{
      $model = new ActionLog;
      $model->module_type=$module;
      $model->module_id=$id;
      $model->rec_type=$type;

      $model->parent_id=$request->parent_id ?? 0;

      $model->subject=$request->subject ?? '';
      $model->due_date=$request->due_date ?? '';
      $model->comments=$request->comments ?? '';
      $model->assign_to=$request->assign_to ?? '';
      $model->priority=$request->priority ?? 1;
      $model->visibility=$request->visibility ?? 1;
      $model->calendar_id=$request->calendar_id ?? '';
      $model->hours=$request->hours ?? '';
      $model->minutes=$request->minutes ?? '';
      $model->start_dt=$request->start_dt ?? '';
      $model->end_dt=$request->end_dt ?? '';
      $model->reminder=$request->reminder ?? '';
      $model->reminder_to=$request->reminder_to ?? '';
      $model->remind_time=$request->remind_time ?? '';
      $model->qnoteAction=$request->qnoteAction ?? '';
      $model->qnoteResponse=$request->qnoteResponse ?? '';
      $model->cmodule_id=$request->cmodule_id ?? '';
      $model->cmodule_type=$request->cmodule_type ?? '';

      $model->event_start=$request->event_start ?? '';
      $model->event_end=$request->event_end ?? '';
      $model->color=$request->color ?? '';
      $model->sub_type=$request->sub_type ?? 0;
      $model->status=$request->status ?? 0;
      $model->allday=$request->allday ?? 0;


      //followup
      $model->follow_up=$request->follow_up ?? 0;
      $model->followup_comments=$request->followup_comments ?? 0;
      $model->followup_date=$request->followup_date ?? '';
      $model->followup_priority=$request->followup_priority ?? 0;
      $model->followup_color=$request->followup_color ?? 0;
      $model->followup_sub_type=$request->followup_sub_type ?? 0;
      $model->followup_status=$request->followup_status ?? 0;
      $model->followup_calendar_id=$request->followup_calendar_id ?? 0;
      $model->followup_allday=$request->followup_allday ?? 0;
      $model->followup_assign_to=$request->followup_assign_to ?? 0;
      $model->followup_reminder=$request->followup_reminder ?? 0;
      $model->followup_reminder_to=$request->followup_reminder_to ?? 0;
      $model->followup_remind_time=$request->followup_remind_time ?? 0;

      if($model->save()){
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::activity.saved')]]);
      }else{
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('actionlog::activity.notsaved'),]]);
      }
    }
  }

  /**
  * Show and save the form for creating a already resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request,$id)
  {
    $modelLog = $this->findModel($id);

    if($request->isMethod('post')){
      if($modelLog->rec_type=='comment'){
        $validator = Validator::make($request->all(), [
          'comments' => 'required|string',
          'visibility' => 'required|integer',
        ]);
      }
      if($modelLog->rec_type=='action'){
        $validator = Validator::make($request->all(), [
          'subject' => 'required|string|max:255',
        ]);
      }
      if($modelLog->rec_type=='call'){
        $validator = Validator::make($request->all(), [
          'qnoteAction' => 'required|integer',
          'qnoteResponse' => 'required|integer',
          'hours' => 'required|string',
          'minutes' => 'required|string',
          'start_dt' => 'required|string',
          'end_dt' => 'required|string',
        ]);
      }
      if($modelLog->rec_type=='timer'){
        $validator = Validator::make($request->all(), [
          'comments' => 'required|string',
          'hours' => 'required|string',
          'minutes' => 'required|string',
          'start_dt' => 'required|string',
          'end_dt' => 'required|string',
        ]);
      }
      if($modelLog->rec_type=='calendar'){
        $validator = Validator::make($request->all(), [
          'comments' => 'required|string',
        ]);
      }

      $modelLog->subject=$request->subject ?? '';
      $modelLog->due_date=$request->due_date ?? '';
      $modelLog->comments=$request->comments ?? '';
      $modelLog->assign_to=$request->assign_to ?? '';
      $modelLog->priority=$request->priority ?? 1;
      $modelLog->visibility=$request->visibility ?? 1;
      $modelLog->calendar_id=$request->calendar_id ?? '';
      $modelLog->hours=$request->hours ?? '';
      $modelLog->minutes=$request->minutes ?? '';
      $modelLog->start_dt=$request->start_dt ?? '';
      $modelLog->end_dt=$request->end_dt ?? '';
      $modelLog->reminder=$request->reminder ?? '';
      $modelLog->reminder_to=$request->reminder_to ?? '';
      $modelLog->remind_time=$request->remind_time ?? '';
      $modelLog->qnoteAction=$request->qnoteAction ?? '';
      $modelLog->qnoteResponse=$request->qnoteResponse ?? '';
      $modelLog->cmodule_id=$request->cmodule_id ?? '';
      $modelLog->cmodule_type=$request->cmodule_type ?? '';

      $modelLog->event_start=$request->event_start ?? '';
      $modelLog->event_end=$request->event_end ?? '';
      $modelLog->color=$request->color ?? '';
      $modelLog->sub_type=$request->sub_type ?? 0;
      $modelLog->status=$request->status ?? 0;
      $modelLog->allday=$request->allday ?? 0;
      $modelLog->updated_at=date("Y-m-d H:i:s");

      if($modelLog->update()){
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::activity.saved')]]);
      }else{
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('actionlog::activity.notsaved'),]]);
      }
    }

    $commentForm=new ALCommentForm;
    $actionForm=new ALActionForm;
    $callForm=new ALCallForm;
    $timerForm=new ALTimerForm;
    $calendarForm=new ALCalendarEventForm;

    return view('actionlog::action-log.update', [
      'model'=>$modelLog,
      'commentForm'=>$commentForm,
      'actionForm'=>$actionForm,
      'callForm'=>$callForm,
      'timerForm'=>$timerForm,
      'calendarForm'=>$calendarForm,
    ]);
  }

  public function completed($id)
  {
    $model = $this->findModel($id);

    DB::table((new ActionLogSubject)->getTable())
    ->where('action_log_id', $model->id)
    ->update(['status' => 1, 'completed_at'=>date("Y-m-d H:i:s")]);

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::activity.completed_successfully')]]);
  }

  /**
  * Suggests records depending on the module type
  *
  * @return \Illuminate\Http\Response
  */
  public function lookUp(Request $request)
  {
    // header('Content-type: application/json');
    $output_arrays=[];
    $results = Prospect::where();
    if($type=='client'){
      $results=Company::find()
      ->select(['id','title'])
      ->where(['status'=>1,'trashed'=>0])
      ->andFilterWhere(['like','title',$query])
      ->asArray()->all();
    }
    if($type=='contact'){
      $results=User::find()
      ->select(['id','title'=>'CONCAT(firstname, " ", lastname)'])
      ->where(['user_type'=>0,'status'=>1,'trashed'=>0])
      ->andFilterWhere([
        'or',
        ['like','firstname',$query],
        ['like','lastname',$query],
      ])
      ->asArray()->all();
    }
    if($type=='lead'){
      $results=Lead::find()
      ->select(['id','title'=>'CONCAT(firstname, " ", lastname)'])
      ->where(['rec_type'=>1,'trashed'=>0])
      ->andFilterWhere([
        'or',
        ['like','firstname',$query],
        ['like','lastname',$query],
      ])
      ->asArray()->all();
    }
    if($type=='opportunity'){
      $results=Lead::find()
      ->select(['id','title'=>'CONCAT(firstname, " ", lastname)'])
      ->where(['rec_type'=>2,'trashed'=>0])
      ->andFilterWhere([
        'or',
        ['like','firstname',$query],
        ['like','lastname',$query],
      ])
      ->asArray()->all();
    }

    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * Uploads attachment
  *
  * @return \Illuminate\Http\Response
  */
  public function dropUpload(Request $request)
  {
    $allowedFileTypes=[
      'jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','DOC','docx','DOCX',
      'xls','XLS','xlsx','XLSX','pdf','PDF'
    ];
    if ($request->hasFile('file')) {
      $image = $request->file('file');
      if (in_array($image->getClientOriginalExtension(),$allowedFileTypes)) {
        $content = file_get_contents($request->file('file'));
        if (preg_match('/\<\?php/i', $content)) {
        }else{
          $image->move(temp_upload_path(),$image->getClientOriginalName());
          echo $image->getClientOriginalName();
        }
      }
    }
    exit;
  }

  /**
  * Save attachment for module
  *
  * @return \Illuminate\Http\Response
  */
  public function saveALAttachment(Request $request,$type,$mid)
  {
    $attachment = new ActionLog;
    $attachment->module_type=$type;
    $attachment->module_id=$mid;
    $attachment->rec_type='attachment';
    $attachment->attachment_file=$request->attachment_file;
    $attachment->save();
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::activity.attachment_saved'),]]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @return \Illuminate\Http\Response
  */
  public function delete($id)
  {
    $model = $this->findModel($id);

    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  }

  /**
  * Assign a task to staff
  *
  * @return \Illuminate\Http\Response
  */
  public function assignTask(Request $request,$id)
  {
    $model = $this->findModel($id);
    if($model!=null){
      if($request->isMethod('post')){
        $validatedData = $request->validate([
          // 'permission_group_id' => 'required|numeric',
        ]);
        $model->updated_at = date("Y-m-d H:i:s");
        $model->assign_to = $request->assign_to;
        $model->update();
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('actionlog::activity.assignment_successfully'),]]);
        exit;
      }
      $model->assign_to = $model->managerIdzArr();
      return view('actionlog::action-log.assign_task', [
        'model'=>$model,
      ]);
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('common.notfound'),]]);
    }
  }

  public function loadActivityHistory(Request $request,$module,$id)
  {
    $limit=20;
    if($request->length)$limit=$request->length;

    $mainModelClass = getModelClass()[$module];
    $mainModel = $mainModelClass::where('id',$id)->first();

    $query = getModuleActivityForUnion($module,$id);

    $query = getActivityUnionQueries($query,$module,$id,$mainModel);

    // if($module=='opportunity'){
    //   $parentActivity = getModuleActivityForUnion($mainModel->module_type,$mainModel->module_id);
    //   $query->union($parentActivity);
    // }

    // Check own & assigned task
    // permissionListTypeFilter($request->module,$this->newModel()->getTable(),'opportunity',$query);

    $models = $query->orderBy('created_at','desc')->paginate($limit);

    $models = $query->paginate($limit);
    $totalRecords = $models->total();
    $totalPages = $models->lastPage();
    $pagesDone = $models->currentPage();
    $nextPageUrl = $models->nextPageUrl();

    $thisFinalContent='';
    if($models!=null){
      foreach($models as $model){
        $actionLog = $this->findModel($model->id);
        $opheading=true;
        // if($module!='opportunity')$opheading = true;
        $thisFinalContent.=getActivityItemHtml($module,$actionLog,false,$opheading);
      }
    }

    $response['append']['contentHtml'] = $thisFinalContent;
    if($totalPages>0){
      $percentage=round(($pagesDone/$totalPages)*100);
    }else{
      $percentage=100;
    }
    $response['progress']=['totalRecords'=>$totalRecords,'totalPages'=>$totalPages,'pagesDone'=>$pagesDone,'percentage'=>$percentage];
    if($nextPageUrl!=null){
      $response['progress']['url']=$nextPageUrl;
    }
    return new JsonResponse($response);
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\AdminGroup  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return ActionLog::where('id', $id)->first();
  }
}

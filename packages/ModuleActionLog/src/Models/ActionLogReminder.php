<?php

namespace NaeemAwan\ModuleActionLog\Models;
use App\Models\ChildTables;

class ActionLogReminder extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'action_log_reminder';
}

<?php

namespace NaeemAwan\ModuleActionLog\Models;
use App\Models\BlameableOnlyTables;

class ActionLogCalAction extends BlameableOnlyTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'action_log_cal_action';
}

<?php

namespace NaeemAwan\ModuleActionLog\Models;
use App\Models\FullTables;

class Calendar extends FullTables
{
  public $manager_id;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'calendar';
  public $moduleTypeId = 'calendar';
}

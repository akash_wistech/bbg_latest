<?php

namespace NaeemAwan\ModuleActionLog\Models;
use App\Models\ChildTables;

class ActionLogAttachment extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'action_log_attachment';
}

<?php

namespace NaeemAwan\ModuleActionLog\Models;
use App\Models\FullTables;

class ActionLog extends FullTables
{
	public $parent_id,$subject,$due_date,$assign_to;
	public $qnoteAction,$qnoteResponse,$hours,$minutes,$start_dt,$end_dt;
  public $reminder,$reminder_to,$remind_time,$calendar_id;
  public $event_start,$event_end,$color,$sub_type,$status,$allday;
	public $cmodule_id,$cmodule_type;
	public $attachment_file;

  public $follow_up,$followup_comments,$followup_date,$followup_priority,$followup_color,$followup_sub_type,$followup_status;
  public $followup_calendar_id,$followup_allday,$followup_assign_to,$followup_reminder,$followup_reminder_to,$followup_remind_time;

  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'action_log';



	/**
	* returns subject/due date info if any
	*/
	public function actionInfo()
	{
		$str=[];
		$sbjRow=ActionLogSubject::select(['subject','due_date','status'])->where(['action_log_id'=>$this->id])->first();
		if($sbjRow!=null)$str=['subject'=>$sbjRow['subject'],'due_date'=>$sbjRow['due_date'],'status'=>$sbjRow['status']];
		return $str;
	}

	/**
	* returns reminder info if any
	*/
	public function reminderInfo()
	{
		$arr=[];
		$reminderRow=ActionLogReminder::select(['notify_to','notify_time'])->where(['action_log_id'=>$this->id])->first();
		if($reminderRow!=null)$arr=['notify_to'=>$reminderRow['notify_to'],'notify_time'=>$reminderRow['notify_time']];
		return $arr;
	}

	/**
	* returns quick note info string if any
	*/
	public function noteInfo()
	{
		$str=[];
		$timeRow=ActionLogCallNote::select(['call_action','call_response'])->where(['action_log_id'=>$this->id])->first();
		if($timeRow!=null)$str=[
			'qa'=>$timeRow['call_action'],
			'qr'=>$timeRow['call_response'],
		];
		return $str;
	}

	/**
	* returns timer info string if any
	*/
	public function timeInfo()
	{
		$str=[];
		$timeRow=ActionLogTime::select(['start_dt','end_dt','hours','minutes'])->where(['action_log_id'=>$this->id])->first();
		if($timeRow!=null)$str=[
			'start_dt'=>$timeRow['start_dt'],
			'end_dt'=>$timeRow['end_dt'],
			'hours'=>$timeRow['hours'],
			'minutes'=>$timeRow['minutes'],
		];
		return $str;
	}

	/**
	* returns event info string if any
	*/
	public function calendarEventInfo()
	{
		$str=[];
		$eventRow=ActionLogEventInfo::select(['start_date','end_date','color_code','all_day','sub_type','status'])->where(['action_log_id'=>$this->id])->first();
		if($eventRow!=null)$str=[
			'start_date'=>$eventRow['start_date'],
			'end_date'=>$eventRow['end_date'],
			'color_code'=>$eventRow['color_code'],
			'all_day'=>$eventRow['all_day'],
			'sub_type'=>$eventRow['sub_type'],
			'status'=>$eventRow['status'],
		];
		return $str;
	}

	/**
	* returns reminder info if any
	*/
	public function calendarInfo()
	{
		$arr=[];
		$calendarRow=ActionLogCalendar::select(['calendar_id'])->where(['action_log_id'=>$this->id])->first();
		if($calendarRow!=null)$arr=['calendar_id'=>$calendarRow['calendar_id']];
		return $arr;
	}

	/**
	* returns attachments info if any
	*/
	public function attachmentInfo()
	{
		$arrFiles=[];
		$results=ActionLogAttachment::where(['action_log_id'=>$this->id])->get();
		if($results!=null){
			$otherFilesArr=['doc','docx','xls','xlsx','pdf'];
			foreach($results as $attachment){
				if($attachment['file_name']!=null && file_exists(attachmentFilePath().'/'.$attachment['file_name'])){
					$fpInfo=pathinfo(attachmentFilePath().'/'.$attachment['file_name']);
					$fileLink=getAttachmentImagePath($attachment['file_name'],'');
					if(in_array($fpInfo['extension'],$otherFilesArr)){
						$icon=asset('assets/media/icons/'.$fpInfo['extension'].'.jpg');
					}else{
						$icon=getAttachmentImagePath($attachment['file_name'],'icon');
					}
					$arrFiles[]=[
						'icon'=>$icon,
						'title'=>$attachment['file_title'],
						'link'=>$fileLink,
					];
				}
			}
		}
		return $arrFiles;
	}

	/**
	* returns event info string if any
	*/
	public function managerIdz()
	{
		return ActionLogManager::select(['staff_id'])->where(['action_log_id'=>$this->id])->get();
	}

	/**
	* returns event info string if any
	*/
	public function managerIdzArr()
	{
    return ActionLogManager::select('staff_id')->where(['action_log_id'=>$this->id])->get()->pluck('staff_id', 'staff_id')->toArray();
	}

  /**
  * Reference model
  */
  public function refModel()
  {
		$modelName = getModelClass()[$this->module_type];
    return $modelName::where('id',$this->module_id)->first();
  }

  /**
  * Reference model
  */
  public function getParentModel()
  {
		$modelName = getModelClass()[$this->module_type];
    return $modelName::where('id',$this->module_id)->first();
  }
}

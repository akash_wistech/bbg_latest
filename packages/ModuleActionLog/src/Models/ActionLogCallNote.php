<?php

namespace NaeemAwan\ModuleActionLog\Models;
use App\Models\ChildTables;

class ActionLogCallNote extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'action_log_call_note';
}

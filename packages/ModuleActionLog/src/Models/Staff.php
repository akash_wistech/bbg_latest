<?php
namespace NaeemAwan\ModuleActionLog\Models;

use App\Models\FullTables;

class Staff extends FullTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'users';

  public function scopeEnabled($query)
  {
      return $query->where('status', 1);
  }

  public function scopeStaff($query)
  {
      return $query->where('user_type', 10);
  }
}

<?php
// use Auth;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Models\ActionLogManager;
use NaeemAwan\ModuleActionLog\Models\ActionLogEventInfo;
use NaeemAwan\ModuleActionLog\Models\ActionLogCalendar;
use NaeemAwan\ModuleActionLog\Models\ActionLogSubject;
use NaeemAwan\ModuleActionLog\Models\ActionLogReminder;
use NaeemAwan\ModuleActionLog\Models\ActionLogCallNote;
use NaeemAwan\ModuleActionLog\Models\Staff;
use NaeemAwan\ModuleActionLog\Models\Calendar;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCommentForm;
use Wisdom\Event\Models\Event;


if (!function_exists('getVisibilityListArr')) {
  /**
  * return visibility list
  */
  function getVisibilityListArr()
  {
    return [
      '1'=>__('actionlog::activity.public'),
      '2'=>__('actionlog::activity.private'),
    ];
  }
}

if (!function_exists('getMyCalendarListArr')) {
  /**
  * return my calendars
  */
  function getMyCalendarListArr()
  {
    if(Auth::user()->id>1){
      return Calendar::select('id','title')->where('created_by',Auth::user()->id)->get()->pluck('title', 'id')->toArray();
    }else{
      return Calendar::select('id','title')->get()->pluck('title', 'id')->toArray();
    }
  }
}

if (!function_exists('getStaffMemberListArr')) {
  /**
  * return staff members
  */
  function getStaffMemberListArr()
  {
    return Staff::select('id','name')->enabled()->staff()->get()->pluck('name', 'id')->toArray();;
  }
}

if (!function_exists('getPriorityListArr')) {
  /**
  * return priority list
  */
  function getPriorityListArr()
  {
    return [
      '1'=>__('actionlog::activity.low'),
      '2'=>__('actionlog::activity.medium'),
      '3'=>__('actionlog::activity.high'),
    ];
  }
}

if (!function_exists('getRemindToListArr')) {
  /**
  * return remider to list
  */
  function getRemindToListArr()
  {
    return [
      'me'=>__('actionlog::activity.me'),
      'assigned'=>__('actionlog::activity.assigned_users'),
      'both'=>__('actionlog::activity.me_and_assigned_users'),
    ];
  }
}

if (!function_exists('getRemindTimeListArr')) {
  /**
  * return reminder time list
  */
  function getRemindTimeListArr()
  {
    return [
      '1'=>__('actionlog::activity.one_minute'),
      '5'=>__('actionlog::activity.five_minutes'),
      '10'=>__('actionlog::activity.ten_minutes'),
      '15'=>__('actionlog::activity.fifteen_minutes'),
      '30'=>__('actionlog::activity.thirty_minutes'),
      '60'=>__('actionlog::activity.one_hour'),
      '1440'=>__('actionlog::activity.one_day'),
      '10080'=>__('actionlog::activity.one_week'),
    ];
  }
}

if (!function_exists('getCallQuickNoteAction')) {
  /**
  * return quick note list
  */
  function getCallQuickNoteAction()
  {
    return [
      '1'=>__('actionlog::activity.contacted'),
      '2'=>__('actionlog::activity.not_Contacted'),
    ];
  }
}

if (!function_exists('getCallQuickNoteResponse')) {
  /**
  * returns array of Log Call Quick Note action response list
  */
  function getCallQuickNoteResponse()
  {
    return [
      '1'=>[
        '1'=>__('actionlog::activity.not_interested'),
        '2'=>__('actionlog::activity.requested_follow_up_call'),
        '3'=>__('actionlog::activity.contact_made'),
      ],
      '2'=>[
        '1'=>__('actionlog::activity.no_answer'),
        '2'=>__('actionlog::activity.wrong_number'),
        '3'=>__('actionlog::activity.left_voicemail'),
      ],
    ];
  }
}

if (!function_exists('getCalendarColorListArr')) {
  /**
  * returns array of Calendar Colors
  */
  function getCalendarColorListArr()
  {
    return [
      '#6389de'=>__('actionlog::activity.blue'),
      '#a9c1fd'=>__('actionlog::activity.light_blue'),
      '#5de1e5'=>__('actionlog::activity.turquoise'),
      '#82e7c2'=>__('actionlog::activity.light_green'),
      '#6bc664'=>__('actionlog::activity.green'),
      '#fddb68'=>__('actionlog::activity.yellow'),
      '#ffbc80'=>__('actionlog::activity.orange'),
      '#ff978c'=>__('actionlog::activity.pink'),
      '#e74046'=>__('actionlog::activity.red'),
      '#d9adfb'=>__('actionlog::activity.purple'),
      '#dedddd'=>__('actionlog::activity.gray'),
    ];
  }
}

if (!function_exists('getEventSubTypesListArr')) {
  /**
  * returns array of Event Sub Types
  */
  function getEventSubTypesListArr()
  {
    return [
      '1'=>__('actionlog::activity.meeting'),
      '2'=>__('actionlog::activity.appointment'),
      '3'=>__('actionlog::activity.call'),
    ];
  }
}

if (!function_exists('getEventSubTypesHeadingListArr')) {
  /**
  * returns array of Event Sub Types
  */
  function getEventSubTypesHeadingListArr()
  {
    return [
      '1'=>'meeting_with',
      '2'=>'appointment_with',
      '3'=>'call_to',
    ];
  }
}

if (!function_exists('getEventSubTypesListIconArr')) {
  /**
  * returns array of Event Sub Types
  */
  function getEventSubTypesListIconArr()
  {
    return [
      '1'=>'fas fa-meeting',
      '2'=>'fas fa-appointment',
      '3'=>'fas fa-phone',
    ];
  }
}

if (!function_exists('getEventStatusListArr')) {
  /**
  * returns array of Event Status
  */
  function getEventStatusListArr()
  {
    return [
      '1'=>__('actionlog::activity.confirmed'),
      '2'=>__('actionlog::activity.cancelled'),
    ];
  }
}

if (!function_exists('getCallActionResponseList')) {
  /**
  * return my calendars
  */
  function getCallActionResponseList()
  {
    $responses=getCallQuickNoteResponse();
    $txtJScript='var alLogCallActionResponses = '.json_encode($responses).';';
    return $txtJScript;
  }
}

if (!function_exists('getStartEndDateTimeFromNow')) {
  /**
  * return time dif
  */
  function getStartEndDateTimeFromNow($diff)
  {
		$date=date("Y-m-d H:i:s");
		return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
  }
}

if (!function_exists('attachmentFilePath')) {
  /**
  * return attachments path
  */
  function attachmentFilePath()
  {
    return public_path('attachments');
  }
}

if (!function_exists('getAttachmentImagePath')) {
  /**
  * return attachments path
  */
  function getAttachmentImagePath($image,$size)
  {
    return asset('attachments/'.$image);
  }
}

if (!function_exists('parseDateTime')) {
  /**
  * parse date for calendar
  */
  function parseDateTime($string, $timeZone=null)
  {
    $date = new \DateTime(
	    $string,
	    $timeZone ? $timeZone : new DateTimeZone('Asia/Dubai')
	      // Used only when the string is ambiguous.
	      // Ignored if string has a timeZone offset in it.
	  );
	  if ($timeZone) {
	    // If our timeZone was ignored above, force it.
	    $date->setTimezone($timeZone);
	  }
	  return $date;
  }
}

if (!function_exists('stripTime')) {
  /**
  * parse date for calendar
  */
  function stripTime($datetime)
  {
	  return $datetime;//->format('Y-m-d');
  }
}

if (!function_exists('getActivityBtns')) {
  /**
  * activity btns for kanban
  */
  function getActivityBtns($btnsArr,$model,$result,$name='')
  {
    if(in_array($model->moduleTypeId,getModulesHavingOpportunity())){
      $btnsArr[]=['icon'=>'<i class="fa fa-plus-circle"></i>','url'=>'','title'=>__('app.general.create_opportunity'),'class'=>'load-modal','otherOpts'=>'data-url="'.(url('/opportunity/create-for',['module'=>$model->moduleTypeId,'id'=>$result->module_id])).'" data-heading="'.__('app.general.create_opportunity_for',['module'=>$name]).'"'];
    }
    $btnsArr[]=['icon'=>'<i class="fa fa-comments"></i>','url'=>'','title'=>__('actionlog::activity.comment'),'class'=>'load-modal','otherOpts'=>'data-url="'.(url('activity-log/'.$model->moduleTypeId.'/comment/'.$model->id).'?parent='.$result->id).'" data-heading="'.__('actionlog::activity.add_comment_for',['module'=>$model->title]).'"'];
    $btnsArr[]=['icon'=>'<i class="fa fa-tasks"></i>','url'=>'','title'=>__('actionlog::activity.task'),'class'=>'load-modal','otherOpts'=>'data-url="'.(url('activity-log/'.$model->moduleTypeId.'/action/'.$model->id).'?parent='.$result->id).'" data-heading="'.__('actionlog::activity.add_action_for',['module'=>$model->title]).'"'];
    $btnsArr[]=['icon'=>'<i class="fa fa-phone"></i>','url'=>'','title'=>__('actionlog::activity.log_call'),'class'=>'load-modal','otherOpts'=>'data-url="'.(url('activity-log/'.$model->moduleTypeId.'/call/'.$model->id).'?parent='.$result->id).'" data-heading="'.__('actionlog::activity.add_logcall_for',['module'=>$model->title]).'"'];
    $btnsArr[]=['icon'=>'<i class="fa fa-clock"></i>','url'=>'','title'=>__('actionlog::activity.log_time'),'class'=>'load-modal','otherOpts'=>'data-url="'.(url('activity-log/'.$model->moduleTypeId.'/timer/'.$model->id).'?parent='.$result->id).'" data-heading="'.__('actionlog::activity.add_logtime_for',['module'=>$model->title]).'"'];
    $btnsArr[]=['icon'=>'<i class="fa fa-calendar"></i>','url'=>'','title'=>__('actionlog::activity.calendar'),'class'=>'load-modal','otherOpts'=>'data-url="'.(url('activity-log/'.$model->moduleTypeId.'/calendar/'.$model->id).'?parent='.$result->id).'" data-heading="'.__('actionlog::activity.add_calendar_for',['module'=>$model->title]).'"'];
	  return $btnsArr;
  }
}

if (!function_exists('getModuleDetailViewForCalendar')) {
  /**
  * parse date for calendar
  */
  function getModuleDetailViewForCalendar($result,$model,$col)
  {
	  $html = '<div class="row">';
    if($model->moduleTypeId=='opportunity'){
      $html.= $model->getCalendarInfoHtml($col);
    }
    if($model->moduleTypeId=='prospect'){
      $html.= '<div class="mb-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.full_name:').'</strong> '.$model->full_name;
      $html.= '</div>';
      $html.= '<div class="mb-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.company_name:').'</strong> '.$model->company_name;
      $html.= '</div>';
      $html.= '<div class="mb-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.email:').'</strong> '.$model->email;
      $html.= '</div>';
      $html.= '<div class="mb-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.phone:').'</strong> '.$model->phone;
      $html.= '</div>';
    }
    if($model->moduleTypeId=='events'){

      // dd($model);

      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.registration_start').'</strong> '.formatDate($model->registration_start);
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.registration_end').'</strong> '.formatDate($model->registration_end);
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.event_startDate').'</strong> '.formatDate($model->event_startDate);
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.event_endDate').'</strong> '.formatDate($model->event_endDate);
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.event_startTime').'</strong> '.$model->event_startTime;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.event_startTime').'</strong> '.$model->event_endTime;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.cancellation_tillDate').'</strong> '.formatDate($model->cancellation_tillDate);
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.group_size').'</strong> '.$model->group_size;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.member_fee').'</strong> '.(($model->member_fee <>null) ? $model->member_fee : ' N/A') ;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.nonmember_fee').'</strong> '.(($model->nonmember_fee<>null) ? $model->nonmember_fee : ' N/A') ;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.committee_fee').'</strong> '.(($model->committee_fee<>null) ? $model->committee_fee : ' N/A') ;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.honourary_fee').'</strong> '.(($model->honourary_fee<>null) ? $model->honourary_fee : ' N/A') ;
      $html.= '</div>';

      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.sponsor_fee').'</strong> '.(($model->sponsor_fee<>null) ? $model->sponsor_fee : ' N/A') ;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.focus_chair_fee').'</strong> '.(($model->focus_chair_fee<>null) ? $model->focus_chair_fee : ' N/A') ;
      $html.= '</div>';
      $html.= '<div class="my-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.event.honourary_fee').'</strong> '.(($model->honourary_fee<>null) ? $model->honourary_fee : ' N/A') ;
      $html.= '</div>';


    }
    $customeFields = getCustomFieldsByModule($model->moduleTypeId);
    if($customeFields!=null){
      foreach($customeFields as $customeField){

        $value = getInputFielSavedValueForDetail($model,$customeField);
        if($customeField['input_type']=='textarea'){
          $col=12;
          // $value = nl2br(truncateText($value,150));
        }
    	  $html.= '<div class="mb-3 col-sm-'.$col.''.($customeField['input_type']=='textarea' ? ' descp-truncate text-truncate' : '').'">';
    	  $html.= '  <strong>'.$customeField->title.':</strong>'.($customeField['input_type']=='textarea' ? '<br />' : ' ').''.$value;
    	  $html.= '</div>';
      }
    }
	  $html.= '</div>';
	  $html.= '<div class="row">';
    $html.= ' <div class="mb-3 col-sm-12 descp-truncate text-truncate">';
	  // $html.= '  <strong>Task Comments:</strong> '.nl2br(truncateText($result->comments,150));
	  $html.= '  <strong>Task Comments:</strong><br />'.nl2br($result->comments).'';
	  $html.= '  </div>';
	  $html.= '</div>';
    return $html;
  }
}

if (!function_exists('getColorLegends')) {
  /**
  * parse date for calendar
  */
  function getColorLegends()
  {
	  return [
      'activity'=>['color_code'=>'#26C281','title'=>__('actionlog::activity.activity_exists')],
      'pendingaction'=>['color_code'=>'#E08283','title'=>__('actionlog::activity.pending_action')],
      'reminder'=>['color_code'=>'#D91E18','title'=>__('actionlog::activity.coming_reminder')],
      'notinterested'=>['color_code'=>'#E5E5E5','title'=>__('actionlog::activity.not_interested')],
      'followup'=>['color_code'=>'#3598DC','title'=>__('actionlog::activity.have_followup')],
      'made'=>['color_code'=>'#32C5D2','title'=>__('actionlog::activity.contact_made')],
      'noanswer'=>['color_code'=>'#525E64','title'=>__('actionlog::activity.no_answer')],
      'wrongnumber'=>['color_code'=>'#C8D046','title'=>__('actionlog::activity.wrong_number')],
      'voicemail'=>['color_code'=>'#9A12B3','title'=>__('actionlog::activity.left_voicemail')],
    ];
  }
}

if (!function_exists('getActivityColors')) {
  /**
  * parse date for calendar
  */
  function getActivityColors($module,$id)
  {
    $colorArr=[];
	  //Activity Exsists
    if(DB::table((new ActionLog)->getTable())->where(['module_type'=>$module,'module_id'=>$id])->exists()){
      $colorItem = getColorLegends()['activity'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Pending Action
    if(
      DB::table((new ActionLog)->getTable())
      ->join((new ActionLogSubject)->getTable(),(new ActionLogSubject)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,'rec_type'=>'action','status'=>0])//,['due_date','>=',date("Y-m-d H:i:s")]
      ->exists()
    ){
      $colorItem = getColorLegends()['pendingaction'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Coming Reminder
    if(
      DB::table((new ActionLog)->getTable())
      ->join((new ActionLogReminder)->getTable(),(new ActionLogReminder)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,['final_due_at','>=',date("Y-m-d H:i:s")]])
      ->whereIn('rec_type',['action','calendar'])
      ->exists()
    ){
      $colorItem = getColorLegends()['reminder'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Not Interested
    if(
      DB::table((new ActionLog)->getTable())
      ->join((new ActionLogCallNote)->getTable(),(new ActionLogCallNote)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,'call_action'=>1,'call_response'=>1])
      ->exists()
    ){
      $colorItem = getColorLegends()['notinterested'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Followup
    if(
      DB::table((new ActionLog)->getTable())
      ->join((new ActionLogCallNote)->getTable(),(new ActionLogCallNote)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,'call_action'=>1,'call_response'=>2])
      ->exists()
    ){
      $colorItem = getColorLegends()['followup'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Contact Made
    $contactMade = DB::table((new ActionLog)->getTable())
    ->join((new ActionLogCallNote)->getTable(),(new ActionLogCallNote)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
    ->where(['module_type'=>$module,'module_id'=>$id,'call_action'=>1,'call_response'=>3])
    ->exists();
    if($contactMade){
      $colorItem = getColorLegends()['made'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //No Answer
    if(
      !$contactMade && DB::table((new ActionLog)->getTable())
      ->join((new ActionLogCallNote)->getTable(),(new ActionLogCallNote)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,'call_action'=>2,'call_response'=>1])
      ->exists()
    ){
      $colorItem = getColorLegends()['noanswer'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Wrong Number
    if(
      !$contactMade && DB::table((new ActionLog)->getTable())
      ->join((new ActionLogCallNote)->getTable(),(new ActionLogCallNote)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,'call_action'=>2,'call_response'=>2])
      ->exists()
    ){
      $colorItem = getColorLegends()['wrongnumber'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
	  //Left Voicemail
    if(
      !$contactMade && DB::table((new ActionLog)->getTable())
      ->join((new ActionLogCallNote)->getTable(),(new ActionLogCallNote)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id")
      ->where(['module_type'=>$module,'module_id'=>$id,'call_action'=>2,'call_response'=>3])
      ->exists()
    ){
      $colorItem = getColorLegends()['voicemail'];
      $colorArr[]='<span class="label label-dot label-xl" style="background:'.$colorItem['color_code'].';" data-toggle="tooltip" data-title="'.$colorItem['title'].'" data-original-title="'.$colorItem['title'].'" data-placement="top"></span>';
    }
    return $colorArr;
  }
}

if (!function_exists('getCalendarAssignModuleBtn')) {
  /**
  * assignment btns for calendar
  */
  function getCalendarAssignModuleBtn($btnsArr,$model)
  {
		$btnsArr[]=['icon'=>'<i class="fa fa-user-plus"></i>','url'=>'','title'=>__('actionlog::calendar.assign_task_to'),'class'=>'load-modal','otherOpts'=>'data-url="'.url('assign-task/'.$model->id).'" data-heading="'.__('actionlog::calendar.assign_task_to').'"'];
	  return $btnsArr;
  }
}

if (!function_exists('getModuleActivityForUnion')) {
  /**
  * get action log single row
  */
  function getModuleActivityForUnion($moduleTypeId,$id)
  {
    $query = DB::table((new ActionLog)->getTable());
    $query->select('id','created_at');
    $query->where(['module_type'=>$moduleTypeId]);
    if(is_array($id)){
      $query->whereIn('module_id',$id);
    }else{
      $query->where('module_id',$id);
    }

    $query->whereNull('deleted_at');
    return $query;
  }
}

if (!function_exists('getActionLogSingleItem')) {
  /**
  * get action log single row
  */
  function getActionLogSingleItem($id)
  {
		return ActionLog::find($id);
  }
}

if (!function_exists('getActivityUnionQueries')) {
  /**
  * get activity union queries
  */
  function getActivityUnionQueries($query,$module,$id,$model)
  {
    if($module=='prospect'){
      $subClass = getModelClass()['opportunity'];
      $subQueryProcpectOpIdz=$subClass::select('id')->where(['module_type'=>$module,'module_id'=>$id])->get()->pluck('id')->toArray();
      $parentActivity = getModuleActivityForUnion('opportunity',$subQueryProcpectOpIdz);
      $query->union($parentActivity);
    }

    if($module=='opportunity'){
      $parentActivity = getModuleActivityForUnion($model->module_type,$model->module_id);
      $query->union($parentActivity);
    }
    return $query;
  }
}

if (!function_exists('getActivityItemHtml')) {
  /**
  * assignment btns for calendar
  */
  function getActivityItemHtml($module_type,$model,$toolbar=true,$opheading=false)
  {

    $actionInfo=$model->actionInfo();
    $timeInfo=$model->timeInfo();
    $calendarInfo=$model->calendarEventInfo();
    $attachmentInfo=$model->attachmentInfo();
    $html = '';
    $html.= '<div class="alh-item d-flex align-items-center mb-5 border-top pt-5" data-key="'.$model->id.'">';
    $html.= ' <div class="symbol symbol-20 symbol-light-primary mr-2">';
    $html.= '   <img class="img-circle img-sm" src="'.asset('assets/media/icons/'.$model->rec_type.'.png').'" alt="'.$model->rec_type.'" width="20" />';
    $html.= ' </div>';
    $html.= ' <div class="d-flex flex-column font-weight-bold w-100">';
    $html.= '   <span class="username">';
    $html.= '     '.$model->createdBy->name;
    $html.= '     <span class="text-muted"><i class="fa fa-clock"></i> '.convertTime($model->created_at).'&nbsp;</span>';
    if($toolbar==true){
    $html.= '     <span class="float-right alh-toolbar">';
    if($model->rec_type=='action' && $actionInfo!=null && $actionInfo['due_date']>=date("Y-m-d H:i:s") && $actionInfo['status']==0){
    $html.= '       <a href="'.url('action-log/completed/'.$model->id).'" class="btn btn-xs act-confirmation" data-confirmationmsg="'.__('actionlog::activity.mark_done_confirmation').'" data-pjxcntr="al-lv-container"><i class="fa fa-check"></i></a>';
    }
    if($model->rec_type!='attachment'){
    $html.= '       <a href="javascript:;" class="btn btn-xs load-modal" data-heading="'.__('actionlog::activity.update_type',['type'=>actionLogTypesArr()[$model->rec_type]]).'" data-url="'.url('action-log/update/'.$model->id).'"><i class="fa fa-edit"></i></a>';
    }
    $html.= '       <a href="'.url('action-log/destroy/'.$model->id).'" class="btn btn-xs act-confirmation" data-confirmationmsg="'.__('actionlog::activity.delete_confirmation').'" data-pjxcntr="al-lv-container"><i class="fa fa-trash"></i></a>';
    $html.= '     </span>';
    }
    $html.= '   </span>';
    if($opheading==true && $model->module_type=='opportunity'){
      $oppModel = $model->getParentModel();
      $oppUrl = url(''.($oppModel->rec_type=='l' ? 'lead' : 'opportunity').'/view/'.$model->module_id);
      $html.= '   <a href="'.$oppUrl.'" target="_blank"><strong>'.$oppModel->title.'</strong></a>';
    }
    if($actionInfo!=null){
    $html.= '   <strong>'.$actionInfo['subject'].'</strong>';
    if($actionInfo['due_date']!=null && $actionInfo['due_date']!=''){
    $html.= '   <span class="badge badge-danger"><i class="fa fa-bell text-white"></i> '.formatDateTime($actionInfo['due_date']).'</span>';
    }
    $html.= '   <br />';
    }
    if($model->rec_type=='call' && $timeInfo!=null){
      $duration=$timeInfo['hours'].'.'.$timeInfo['minutes'];
    $html.= '   '.__('actionlog::activity.call_info',['date'=>formatDateTime($timeInfo['start_dt']),'duration'=>$duration]);
    $html.= '   <br />';
    }
    if($model->rec_type=='timer' && $timeInfo!=null){
      $duration=$timeInfo['hours'].'.'.$timeInfo['minutes'];
    $html.= '   '.__('actionlog::activity.time_info',['duration'=>$duration,'date'=>formatDateTime($timeInfo['start_dt'])]);
    $html.= '   <br />';
    }
    if($model->rec_type=='calendar' && $calendarInfo!=null){
    $html.= '   '.__('actionlog::activity.event_info',['startDate'=>formatDateTime($calendarInfo['start_date']),'endDate'=>formatDateTime($calendarInfo['end_date'])]);
    $html.= '   <br />';
    }
    if($model->rec_type=='attachment' && $attachmentInfo!=null){
    $html.= '   <div class="d-flex align-items-center justify-content-between">';
    $html.= '     <div class="mr-3">';
    foreach($attachmentInfo as $attachment){
    $html.= '   <a href="'.$attachment['link'].'" class="btn btn-clean btn-icon btn-md" target="_blank" title="'.$attachment['title'].'" data-toggle="tooltip">';
    $html.= '     <img src="'.$attachment['icon'].'" class="h-50 align-self-center" alt="" width="30" height="30" />';
    $html.= '   </a>';
    }
    $html.= '     </div>';
    $html.= '   </div>';
    }
    $html.= '   '.$model->comments;
    $html.= ' </div>';
    $html.= '</div>';
    $html.= '';
    return $html;
  }
}

if (!function_exists('getActivityQuery')) {
  /**
  * returns activity query for calender and todo
  */
  function getActivityQuery($start,$end,$request=null)
  {
    $start = calendarDateFormat($start);
    $end = calendarDateFormat($end);
    $query = DB::table((new ActionLog)->getTable());
    $query->select([
        (new ActionLog)->getTable().'.id',
        // DB::raw("'actionlog' as row_type"),
        (new ActionLog)->getTable().'.module_type',
        (new ActionLog)->getTable().'.module_id',
        (new ActionLog)->getTable().'.rec_type',
        (new ActionLog)->getTable().'.comments',
        (new ActionLog)->getTable().'.priority',
        (new ActionLog)->getTable().'.visibility',
        (new ActionLogEventInfo)->getTable().'.start_date',
        (new ActionLogEventInfo)->getTable().'.end_date',
        (new ActionLogEventInfo)->getTable().'.color_code',
        (new ActionLogEventInfo)->getTable().'.all_day',
        (new ActionLogEventInfo)->getTable().'.sub_type',
        (new ActionLogEventInfo)->getTable().'.status',
        (new ActionLogSubject)->getTable().'.subject',
        (new ActionLogSubject)->getTable().'.due_date',
      ]);
    $query->leftJoin((new ActionLogEventInfo)->getTable(),(new ActionLogEventInfo)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id");

    $query->leftJoin((new ActionLogSubject)->getTable(),(new ActionLogSubject)->getTable().".action_log_id","=",(new ActionLog)->getTable().".id");

    $query->whereNull('deleted_at')->whereIn((new ActionLog)->getTable().'.rec_type',['action','calendar']);

    $query2 = DB::table((new Event)->getTable());
    $query2->select([
        // 'id',
        DB::raw("'0' as id"),
        // DB::raw("'event' as row_type"),
        DB::raw("'event' as module_type"),
        DB::raw("id as module_id"),
        DB::raw("'main-event' as rec_type"),
        DB::raw("'' as comments"),
        DB::raw("'' as priority"),
        DB::raw("'' as visibility"),
        'event_startDate as start_date',
        DB::raw("event_endDate as end_date"),

        DB::raw("'' as color_code"),
        DB::raw("'' as all_day"),
        DB::raw("'' as sub_type"),
        DB::raw("'' as status"),
        DB::raw("'title' as subject"),
        DB::raw("'' as due_date"),

      ]);




      $query2->where(function($query2) use($start,$end){
          $query2->whereRaw('DATE(event_startDate)>=?',[$start])
          ->where('active','1')
          ->whereRaw('DATE(event_endDate)<=?',[$end]);
      });

    // dd($query2->get());




    $query->where(function($query) use($start,$end){
      $query->where(function($query) use($start,$end){
        $query->whereRaw('DATE('.(new ActionLogSubject)->getTable().'.due_date)>=?',[$start])
        ->whereRaw('DATE('.(new ActionLogSubject)->getTable().'.due_date)<=?',[$end])
        ->where((new ActionLogSubject)->getTable().'.status',0);
      })
      ->orWhere(function($query) use($start,$end){
        $query->whereRaw('DATE('.(new ActionLogEventInfo)->getTable().'.start_date)>=?',[$start])
        ->whereRaw('DATE('.(new ActionLogEventInfo)->getTable().'.end_date)<=?',[$end])
        ->where((new ActionLogEventInfo)->getTable().'.updated',0);
      });
    });
    $query = $query2->union($query);
    if(getListingTypeByController('calendar')==2){
			$query->whereIn('id',function($query){
				$query->selectRaw('action_log_id')
		    ->from((new ActionLogManager)->getTable())
		    ->where(['staff_id'=>Auth::user()->id]);
			});
    }

    if($request!=null && $request->staff_id!=null){
      $staffIdz = $request->staff_id;
      $query->whereIn('id',function($query) use ($staffIdz){
        $query->selectRaw('action_log_id')
		    ->from((new ActionLogManager)->getTable())
		    ->whereIn('staff_id',explode(",",$staffIdz));
      });
    }

    if($request!=null && $request->calender_id!=null){
      $calendarId = $request->calender_id;
      $query->whereIn('id',function($query) use ($calendarId){
        $query->from((new ActionLogCalendar)->getTable())
        ->selectRaw('action_log_id')
        ->whereIn('calendar_id', explode(",",$calendarId));
      });
    }
    return $query;
  }
}

  if (!function_exists('getActivityItemOutput')) {
  /**
  * returns activity output
  */
  function getActivityItemOutput($result,$calenderSubTypeIconsArr,$calenderSubTypeHeadingArr,$type)
  {
    $html='';
    $model = getModuleRow($result->module_type,$result->module_id);
    if($model!=null){
      $moduleName = getModuleNameFieldValue($model);

      $description = getModuleDetailViewForCalendar($result,$model,12);
      $descriptionPop = getModuleDetailViewForCalendar($result,$model,6);
      $managersHtml = '';//getCalendarAssignedUsersSymbol($result->id,40);


      $popupBtns='';


      $btnsArr=[];
      $btnsArr[]=['icon'=>'<i class="fa fa-edit"></i>','url'=>getModelUrlPre()[$result->module_type].'/update/'.$model->id,'title'=>__('common.update')];
      $btnsArr[]=['icon'=>'<i class="fa fa-eye"></i>','url'=>getModelUrlPre()[$result->module_type].'/view/'.$model->id,'title'=>__('common.view')];
      // $btnsArr = getCalendarAssignModuleBtn($btnsArr,$result);
      $btnsArr = getActivityBtns($btnsArr,$model,$result,$moduleName);
      $btnsArr[]=['icon'=>'<i class="fa fa-history"></i>','url'=>'','title'=>__('common.history'),'otherOpts'=>'onclick="loadActivityHistory(\''.$result->module_type.'\','.$model->id.')"'];

      foreach($btnsArr as $key=>$val){
        $url = ($val['url']!='' ? url($val['url']) : 'javascript:;');
        $classes = 'btn btn-clean btn-icon btn-md mr-1'.(isset($val['class'])!='' ? ' '.$val['class'] : '');
        $otherAtrs = (isset($val['otherOpts'])!='' ? ' '.$val['otherOpts'] : '');
        $tooltip = (isset($val['title']) ? ' data-toggle="tooltip" data-placement="top" data-original-title="'.$val['title'].'"' : '');

        $popupBtns.='      <a href="'.$url.'" class="'.$classes.'" data-mid="secondary-modal"'.$otherAtrs.$tooltip.'>';
        $popupBtns.='        '.$val['icon'];
        $popupBtns.='      </a>';
      }


      $viewUrl = url(getModelUrlPre()[$result->module_type].'/view/'.$result->module_id);
      $imageurl = '';
      $endDate = '';
      $color_code = '';
      $all_day='';
      if($result->rec_type=='action'){
        $title = trim($result->subject);
        $startDate = trim($result->due_date);
      }
      elseif($result->rec_type=='calendar'){
        $title = trim($result->comments);
        $startDate = trim($result->start_date);
        $endDate = trim($result->end_date);
        $color_code = trim($result->color_code);
        $all_day = trim($result->all_day);
        if(isset($calenderSubTypeHeadingArr[$result->sub_type])){
          $subType = $calenderSubTypeHeadingArr[$result->sub_type];
          $title = __('actionlog::activity.'.$subType,['name'=>$moduleName]);
        }
        if(isset($calenderSubTypeIconsArr[$result->sub_type])){
          $imageurl = '<i class="'.$calenderSubTypeIconsArr[$result->sub_type].' text-white"></i> ';
        }
      }
      elseif($result->rec_type=='main-event'){
        $title = trim($model->title);
        $startDate = trim($model->event_startDate);
        // $endDate = trim($model->event_endDate);
          $endDate =  date('Y-m-d H:i:s', strtotime("$model->event_endDate $model->event_startTime"));
        $color_code = '#6389de';
      }


      if($type=='html'){
        if($model->moduleTypeId=='prospect'){
          $title.=' ('.$model->full_name.($model->full_name!='' && $model->company_name!='' ? ' - ' : '').$model->company_name.')';
        }
      }
      $html.= '<div id="todoItem-'.$result->id.'" class="todo-item-c d-flex align-items-start list-item card-spacer-x py-4 extendedPopOver">';
      $html.= ' <div class="flex-grow-1 mt-1 mr-2" data-toggle="view">';
      $html.= '   <div class="font-weight-bolder mr-2">'.$title.'</div>';
      $html.= '   <div class="d-none">';
      $html.= '     <div class="description-box">'.$description.'</div>';
      $html.= '     <div class="descp-box">'.$descriptionPop.'</div>';
      $html.= '     <div class="manager-box">'.$managersHtml.'</div>';
      $html.= '     <div class="btns-box">'.$popupBtns.'</div>';
      $html.= '   </div>';
      $html.= ' </div>';
      $html.= ' <div class="d-flex align-items-center justify-content-end flex-wrap" data-toggle="view">';
      $html.= '   <div class="font-weight-bolder" data-toggle="view">'.formatDateTime($startDate).'</div>';
      $html.= ' </div>';
      $html.= '</div>';

      // dd($endDate);

      if($type=='arr'){
        return [
          'id'=>$result->id,
          'imageurl'=>$imageurl,
          'title'=>$title,
          'start'=>$startDate,
          'end'=>$endDate,
          'color'=>$color_code,
          'allDay'=>$all_day,
          'className'=>'',
          'description'=>$description,
          'popdescription'=>$descriptionPop,
          'popmanagers'=>$managersHtml,
          'popupBtns'=>$popupBtns,
          'viewUrl'=>$viewUrl,
          // 'url'=>$url,
        ];
      }else{
        return $html;
      }
    }






  if ('1'=='event') {

    $event = Event::where('id',$result->id)
    ->select([
        'id',
        DB::raw("image as imageurl"),
        'title',
        DB::raw("event_startDate as start"),
        DB::raw("'' as end"),
        DB::raw("'' as color"),
        DB::raw("'' as allDay"),
        DB::raw("'' as className"),
        'description',
        DB::raw("'' as popdescription"),
        DB::raw("'' as popmanagers"),
        DB::raw("'' as popupBtns"),
        DB::raw("'' as viewUrl"),
      ])->first()->toArray();
      //
      // if($type=='arr'){
      //   return [
      //     'id'=>$result->id,
      //     'imageurl'=>$imageurl,
      //     'title'=>$title,
      //     'start'=>$startDate,
      //     'end'=>$endDate,
      //     'color'=>$color_code,
      //     'allDay'=>$all_day,
      //     'className'=>'',
      //     'description'=>$description,
      //     'popdescription'=>$descriptionPop,
      //     'popmanagers'=>$managersHtml,
      //     'popupBtns'=>$popupBtns,
      //     'viewUrl'=>$viewUrl,
      //     // 'url'=>$url,
      //   ];
      // }else{
      //   return $html;
      // }

      return $event;


    // dd($event);
  }


}
}

if (!function_exists('getTodoTabs')) {
  /**
  * returns array of Event Sub Types
  */
  function getTodoTabs()
  {
    list($thisWeekStart,$thisWeekEnd) = getWeekStartEnd(date("Y-m-d"));
    list($nextWeekStart,$nextWeekEnd) = getWeekStartEnd(date("Y-m-d",strtotime("+2 day",strtotime($thisWeekEnd))));

    return [
      [
        'title'=>__('actionlog::todo.over_due'),
        'from'=>'-n',
        'to'=>date("Y-m-d",strtotime("-1 day",strtotime(date("Y-m-d")))),
      ],
      [
        'title'=>__('actionlog::todo.today'),
        'from'=>date("Y-m-d"),
        'to'=>date("Y-m-d"),
      ],
      [
        'title'=>__('actionlog::todo.this_week'),
        'from'=>$thisWeekStart,
        'to'=>$thisWeekEnd,
      ],
      [
        'title'=>__('actionlog::todo.next_week'),
        'from'=>$nextWeekStart,
        'to'=>$nextWeekEnd,
      ],
    ];
  }
}

if (!function_exists('getTodoCount')) {
  /**
  * returns array of Event Sub Types
  */
  function getTodoCount($from,$to)
  {
    $query = getActivityQuery($from,$to,$request=null);
    return $query->count('id');
  }
}

if (!function_exists('saveModuleComment')) {
  /**
  * Save a note for a module
  */
  function saveModuleComment($module,$moduleId,$comments)
  {
    $model = new ActionLog;
    $model->module_type=$module;
    $model->module_id=$moduleId;
    $model->rec_type='comment';
    $model->comments=$comments;
    $model->save();
  }
}

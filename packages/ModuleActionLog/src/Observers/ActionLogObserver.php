<?php
namespace NaeemAwan\ModuleActionLog\Observers;

use Auth;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Models\ActionLogAttachment;
use NaeemAwan\ModuleActionLog\Models\ActionLogCalendar;
use NaeemAwan\ModuleActionLog\Models\ActionLogEventInfo;
use NaeemAwan\ModuleActionLog\Models\ActionLogManager;
use NaeemAwan\ModuleActionLog\Models\ActionLogReminder;
use NaeemAwan\ModuleActionLog\Models\ActionLogSubject;
use NaeemAwan\ModuleActionLog\Models\ActionLogTime;
use NaeemAwan\ModuleActionLog\Models\ActionLogCallNote;
use NaeemAwan\ModuleActionLog\Models\ActionLogCalAction;
use Illuminate\Support\Facades\DB;
use Log;

class ActionLogObserver
{
  /**
  * Handle the ActionLog "created" event.
  *
  * @param  \NaeemAwan\ModuleActionLog\Models\ActionLog  $actionLog
  * @return void
  */
  public function created(ActionLog $actionLog)
  {
    if ($actionLog->id) {
      //Saving Parent child row
      if($actionLog->parent_id>0){
        $alPrntCldRow=ActionLogCalAction::where(['action_log_parent_id'=>$actionLog->parent_id])->first();
        if($alPrntCldRow==null){
  				$alPrntCldRow=new ActionLogCalAction;
  				$alPrntCldRow->action_log_parent_id=$actionLog->parent_id;
          $alPrntCldRow->action_log_child_id = $actionLog->id;
          $alPrntCldRow->save();
        }else{
          DB::table((new ActionLogCalAction)->getTable())
          ->where('action_log_parent_id', $actionLog->parent_id)
          ->update(['action_log_child_id'=>$actionLog->id,'updated_by'=>Auth::user()->id,'updated_at'=>date("Y-m-d H:i:s")]);
        }
        // Log::debug('Parent ID: '.$actionLog->parent_id);
        $parentRow = ActionLog::where('id',$actionLog->parent_id)->first();
        if($parentRow!=null){
        // Log::debug('Parent Row Found & type is "'.$parentRow->rec_type.'"');
          if($parentRow->rec_type=='action'){
            DB::table((new ActionLogSubject)->getTable())
            ->where('action_log_id', $parentRow->id)
            ->update(['status' => 1, 'completed_at'=>date("Y-m-d H:i:s")]);
            // Log::debug('Updating subject row for action_log_id "'.$parentRow->id.'"');
          }
          if($parentRow->rec_type=='calendar'){
            DB::table((new ActionLogEventInfo)->getTable())
            ->where('action_log_id', $parentRow->id)
            ->update(['updated' => 1, 'updated_at'=>date("Y-m-d H:i:s")]);
          }
        }
      }

      //Saving Subject of action
      if(isset($actionLog->subject) && $actionLog->subject!=''){
        $alSbjRow=ActionLogSubject::where(['action_log_id'=>$actionLog->id])->first();
        if($alSbjRow==null){
  				$alSbjRow=new ActionLogSubject;
  				$alSbjRow->action_log_id=$actionLog->id;
        }
        $alSbjRow->subject = $actionLog->subject;
        $alSbjRow->due_date = $actionLog->due_date;
        $alSbjRow->save();
      }

      //Saving Reminder
      if(in_array($actionLog->rec_type,['action','calendar'])){
        if($actionLog->reminder==1){
          if($actionLog->rec_type=='action'){
            $final_due_at=date("Y-m-d H:i:s",strtotime("-".$actionLog->remind_time." Minute",strtotime($actionLog->due_date)));
          }else{
            $final_due_at=date("Y-m-d H:i:s",strtotime("-".$actionLog->remind_time." Minute",strtotime($actionLog->event_start)));
          }

          $reminderInfo=ActionLogReminder::where(['action_log_id'=>$actionLog->id])->first();
          if($reminderInfo==null){
            $reminderInfo=new ActionLogReminder;
            $reminderInfo->action_log_id=$actionLog->id;
          }
          $reminderInfo->notify_to=$actionLog->reminder_to;
          $reminderInfo->notify_time=$actionLog->remind_time;
          $reminderInfo->final_due_at=$final_due_at;
          $reminderInfo->save();
        }else{
          $reminderInfo=ActionLogReminder::where(['action_log_id'=>$actionLog->id])->first();
          if($reminderInfo!=null){
            $reminderInfo->delete();
          }
        }
  		}

      //Saving Calendar
      if(isset($actionLog->calendar_id) && $actionLog->calendar_id!=null){
  			$alCalRow=ActionLogCalendar::where(['action_log_id'=>$actionLog->id])->first();
  			if($alCalRow==null){
  				$alCalRow=new ActionLogCalendar;
  				$alCalRow->action_log_id=$actionLog->id;
  			}
  			$alCalRow->calendar_id=$actionLog->calendar_id;
  			$alCalRow->save();
  		}

      //Saving Call Notes
      if((isset($actionLog->qnoteAction) && $actionLog->qnoteAction!='') && (isset($actionLog->qnoteResponse) && $actionLog->qnoteResponse!='')){
  			$alCalNoteRow=ActionLogCallNote::where(['action_log_id'=>$actionLog->id])->first();
  			if($alCalNoteRow==null){
  				$alCalNoteRow=new ActionLogCallNote;
  				$alCalNoteRow->action_log_id=$actionLog->id;
  			}
  			$alCalNoteRow->call_action=$actionLog->qnoteAction;
  			$alCalNoteRow->call_response=$actionLog->qnoteResponse;
  			$alCalNoteRow->follow_up=$actionLog->follow_up;
        //saving followup
        if($actionLog->follow_up==1){
          $followUpModel = new ActionLog;
          $followUpModel->module_type = $actionLog->module_type;
          $followUpModel->module_id = $actionLog->module_id;
          $followUpModel->rec_type = 'calendar';
          $followUpModel->comments = $actionLog->followup_comments;
          $followUpModel->event_start = $actionLog->followup_date;
          $followUpModel->event_end = $actionLog->followup_date;
          $followUpModel->priority = $actionLog->followup_priority;
          $followUpModel->color = $actionLog->followup_color;
          $followUpModel->sub_type = $actionLog->followup_sub_type;
          $followUpModel->status = $actionLog->followup_status;
          $followUpModel->allday = $actionLog->followup_allday;
          $followUpModel->calendar_id = $actionLog->followup_calendar_id;
          $followUpModel->assign_to = $actionLog->followup_assign_to;
          $followUpModel->reminder = $actionLog->followup_reminder;
          $followUpModel->reminder_to = $actionLog->followup_reminder_to;
          $followUpModel->remind_time = $actionLog->followup_remind_time;
          $followUpModel->save();
    			$alCalNoteRow->follow_up_id=$followUpModel->id;
        }
  			$alCalNoteRow->save();
  		}

      //Saving Event Info
      if(
        (isset($actionLog->event_start) && $actionLog->event_start!='') ||
        (isset($actionLog->event_end) && $actionLog->event_end!='') ||
        (isset($actionLog->color) && $actionLog->color!='') ||
        (isset($actionLog->allday) && $actionLog->allday!='') ||
        (isset($actionLog->sub_type) && $actionLog->sub_type!='') ||
        (isset($actionLog->status) && $actionLog->status!='')
      ){
        $eventInfo=ActionLogEventInfo::where(['action_log_id'=>$actionLog->id])->first();
        if($eventInfo==null){
          $eventInfo=new ActionLogEventInfo;
          $eventInfo->action_log_id=$actionLog->id;
        }
        $eventInfo->start_date=$actionLog->event_start;
        $eventInfo->end_date=$actionLog->event_end;
        $eventInfo->color_code=$actionLog->color;
        $eventInfo->all_day=$actionLog->allday;
        $eventInfo->sub_type=$actionLog->sub_type;
        $eventInfo->status=$actionLog->status;
        $eventInfo->save();
  		}

  		//Saving Duration
      if(isset($actionLog->start_dt) && $actionLog->start_dt!=''){
  			$endDateTime=$actionLog->end_dt;
  			if($endDateTime=='')$endDateTime=$actionLog->start_dt;
  			$alSbjRow=ActionLogTime::where(['action_log_id'=>$actionLog->id])->first();
  			if($alSbjRow==null){
  				$alSbjRow=new ActionLogTime;
  				$alSbjRow->action_log_id=$actionLog->id;
  			}
  			$alSbjRow->start_dt=$actionLog->start_dt;
  			$alSbjRow->end_dt=$endDateTime;
  			$alSbjRow->hours=$actionLog->hours;
  			$alSbjRow->minutes=$actionLog->minutes;
  			$alSbjRow->save();
  		}

  		//Saving Assigned To
      if(isset($actionLog->assign_to) && $actionLog->assign_to!=null){
  			ActionLogManager::where('action_log_id',$actionLog->id)->whereNotIn('staff_id',$actionLog->assign_to)->delete();
  			foreach($actionLog->assign_to as $key=>$val){
  				$managerRow=ActionLogManager::where(['action_log_id'=>$actionLog->id,'staff_id'=>$val])->first();
  				if($managerRow==null){
  					$managerRow=new ActionLogManager;
  					$managerRow->action_log_id=$actionLog->id;
  					$managerRow->staff_id=$val;
  					$managerRow->save();
  				}
  			}
  		}
  		$managerRow=ActionLogManager::where(['action_log_id'=>$actionLog->id,'staff_id'=>$actionLog->created_by])->first();
  		if($managerRow==null){
  			$managerRow=new ActionLogManager;
  			$managerRow->action_log_id=$actionLog->id;
  			$managerRow->staff_id=$actionLog->created_by;
  			$managerRow->save();
  		}

      //Saving Attachment
      if(isset($actionLog->attachment_file) && $actionLog->attachment_file!=null){
  			foreach($actionLog->attachment_file as $key=>$val){
  				$alAttachmentRow=new ActionLogAttachment;
  				$alAttachmentRow->action_log_id=$actionLog->id;
  				$pInfo=pathinfo($val);
  				$fileName=generateName().'.'.$pInfo['extension'];
  				$alAttachmentRow->file_name=$fileName;
  				$alAttachmentRow->file_title=$pInfo['filename'];
  				if($alAttachmentRow->save()){
  					rename(temp_upload_path().'/'.$val,attachmentFilePath().'/'.$fileName);
  				}
  			}
  		}
    }
  }

  /**
  * Handle the ActionLog "updated" event.
  *
  * @param  \App\Models\ActionLog  $actionLog
  * @return void
  */
  public function updated(ActionLog $actionLog)
  {
    //Saving Subject of action
    if(isset($actionLog->subject) && $actionLog->subject!=''){
      DB::table((new ActionLogSubject)->getTable())
      ->where('action_log_id', $actionLog->id)
      ->update(['subject' => $actionLog->subject, 'due_date' => $actionLog->due_date]);
    }

    //Saving Reminder
    if(in_array($actionLog->rec_type,['action','calendar'])){
      if($actionLog->reminder==1){
        if($actionLog->rec_type=='action'){
          $final_due_at=date("Y-m-d H:i:s",strtotime("-".$actionLog->remind_time." Minute",strtotime($actionLog->due_date)));
        }else{
          $final_due_at=date("Y-m-d H:i:s",strtotime("-".$actionLog->remind_time." Minute",strtotime($actionLog->event_start)));
        }

        DB::table((new ActionLogReminder)->getTable())
        ->where('action_log_id', $actionLog->id)
        ->update(['notify_to' => $actionLog->reminder_to, 'notify_time' => $actionLog->remind_time, 'final_due_at' => $final_due_at]);

      }else{
        DB::table((new ActionLogReminder)->getTable())->where('action_log_id', $actionLog->id)->delete();
      }
    }

    //Saving Calendar
    if(isset($actionLog->calendar_id) && $actionLog->calendar_id!=null){
      DB::table((new ActionLogCalendar)->getTable())
      ->where('action_log_id', $actionLog->id)
      ->update(['calendar_id' => $actionLog->calendar_id]);
    }

    //Saving Call Notes
    if((isset($actionLog->qnoteAction) && $actionLog->qnoteAction!='') && (isset($actionLog->qnoteResponse) && $actionLog->qnoteResponse!='')){

      DB::table((new ActionLogCallNote)->getTable())
      ->where('action_log_id', $actionLog->id)
      ->update(['call_action' => $actionLog->qnoteAction, 'call_response' => $actionLog->qnoteResponse]);
    }

    //Saving Event Info
    if(
      (isset($actionLog->event_start) && $actionLog->event_start!='') ||
      (isset($actionLog->event_end) && $actionLog->event_end!='') ||
      (isset($actionLog->color) && $actionLog->color!='') ||
      (isset($actionLog->allday) && $actionLog->allday!='') ||
      (isset($actionLog->sub_type) && $actionLog->sub_type!='') ||
      (isset($actionLog->status) && $actionLog->status!='')
    ){
      DB::table((new ActionLogEventInfo)->getTable())
      ->where('action_log_id', $actionLog->id)
      ->update([
        'start_date' => $actionLog->event_start,
        'end_date' => $actionLog->event_end,
        'color_code' => $actionLog->color,
        'all_day' => $actionLog->allday,
        'sub_type' => $actionLog->sub_type,
        'status' => $actionLog->status,
      ]);
    }

    //Saving Duration
    if(isset($actionLog->start_dt) && $actionLog->start_dt!=''){
      $endDateTime=$actionLog->end_dt;
      if($endDateTime=='')$endDateTime=$actionLog->start_dt;

      DB::table((new ActionLogTime)->getTable())
      ->where('action_log_id', $actionLog->id)
      ->update([
        'start_dt' => $actionLog->start_dt,
        'end_dt' => $endDateTime,
        'hours' => $actionLog->hours,
        'minutes' => $actionLog->minutes,
      ]);
    }

    //Saving Assigned To
    if(isset($actionLog->assign_to) && $actionLog->assign_to!=null){
      DB::table((new ActionLogManager)->getTable())->where('action_log_id', $actionLog->id)->whereNotIn('staff_id',$actionLog->assign_to)->delete();
      foreach($actionLog->assign_to as $key=>$val){
        $managerRow=ActionLogManager::where(['action_log_id'=>$actionLog->id,'staff_id'=>$val])->first();
        if($managerRow==null){
          $managerRow=new ActionLogManager;
          $managerRow->action_log_id=$actionLog->id;
          $managerRow->staff_id=$val;
          $managerRow->save();
        }
      }
    }
    $managerRow=ActionLogManager::where(['action_log_id'=>$actionLog->id,'staff_id'=>$actionLog->created_by])->first();
    if($managerRow==null){
      $managerRow=new ActionLogManager;
      $managerRow->action_log_id=$actionLog->id;
      $managerRow->staff_id=$actionLog->created_by;
      $managerRow->save();
    }

    //Saving Attachment
    if(isset($actionLog->attachment_file) && $actionLog->attachment_file!=null){
      foreach($actionLog->attachment_file as $key=>$val){
        $alAttachmentRow=new ActionLogAttachment;
        $alAttachmentRow->action_log_id=$actionLog->id;
        $pInfo=pathinfo($val);
        $fileName=generateName().'.'.$pInfo['extension'];
        $alAttachmentRow->file_name=$fileName;
        $alAttachmentRow->file_title=$pInfo['filename'];
        if($alAttachmentRow->save()){
          // rename(Yii::$app->params['temp_abs_path'].$val,Yii::$app->params['attachment_uploads_abs_path'].$fileName);
        }
      }
    }
  }

  /**
  * Handle the ActionLog "deleted" event.
  *
  * @param  \App\Models\ActionLog  $actionLog
  * @return void
  */
  public function deleted(ActionLog $actionLog)
  {
    //
  }

  /**
  * Handle the ActionLog "restored" event.
  *
  * @param  \App\Models\ActionLog  $actionLog
  * @return void
  */
  public function restored(ActionLog $actionLog)
  {
    //
  }

  /**
  * Handle the ActionLog "force deleted" event.
  *
  * @param  \App\Models\ActionLog  $actionLog
  * @return void
  */
  public function forceDeleted(ActionLog $actionLog)
  {
    //
  }
}

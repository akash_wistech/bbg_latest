<?php
namespace NaeemAwan\ModuleActionLog\View\Components;

use Illuminate\View\Component;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use Illuminate\Support\Facades\DB;

class ActivityHistory extends Component
{
  /**
  * model.
  *
  * @var string
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->module_type = $model->moduleTypeId;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $module_type=$this->module_type;

    // $results = ActionLog::where('module_type',$this->model->moduleTypeId)->where('module_id',$this->model->id)->paginate(25);
    $query = getModuleActivityForUnion($this->model->moduleTypeId,$this->model->id);

    $query = getActivityUnionQueries($query,$this->model->moduleTypeId,$this->model->id,$this->model);

    // if($this->model->moduleTypeId=='opportunity'){
    //   $parentActivity = getModuleActivityForUnion($this->model->module_type,$this->model->module_id);
    //   $query->union($parentActivity);
    // }

    $results = $query->orderBy('created_at','desc')->simplePaginate(25);

    return view('actionlog::components.activity_history',compact('module_type','results'));
  }
}

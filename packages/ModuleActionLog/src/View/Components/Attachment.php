<?php
namespace NaeemAwan\ModuleActionLog\View\Components;

use Illuminate\View\Component;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Models\ActionLogAttachment;

class Attachment extends Component
{
  /**
  * model.
  *
  * @var string
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->module_type = $model->moduleTypeId;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $model = $this->model;
    $results = ActionLogAttachment::whereIn('action_log_id',function($query) use ($model){
      $query->select('id')
      ->from((new ActionLog)->getTable())
      ->where('module_type',$model->moduleTypeId)
      ->where('module_id',$model->id);
    })->get();

    return view('actionlog::components.attachment_form',compact('results','model'));
  }
}

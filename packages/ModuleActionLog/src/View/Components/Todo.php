<?php
namespace NaeemAwan\ModuleActionLog\View\Components;

use Illuminate\View\Component;

class Todo extends Component
{
  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct()
  {
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $todoTabs = getTodoTabs();

    return view('actionlog::components.todo',[
      'todoTabs'=>$todoTabs,
    ]);
  }
}

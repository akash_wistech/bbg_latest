<?php
namespace NaeemAwan\ModuleActionLog\View\Components;

use Illuminate\View\Component;
use NaeemAwan\ModuleActionLog\Models\ActionLog;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCommentForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALActionForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCallForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALTimerForm;
use NaeemAwan\ModuleActionLog\Http\Requests\ALCalendarEventForm;
use Illuminate\Support\Facades\DB;

class Activity extends Component
{
  /**
  * model.
  *
  * @var string
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->module_type = $model->moduleTypeId;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $module_type=$this->module_type;
    //Comments Form
    $commentForm=new ALCommentForm;
    $commentForm->rec_type='comment';
    $commentForm->model=$this->model;
    $commentForm->module_type=$this->model->moduleTypeId;
    //Action Form
    $actionForm=new ALActionForm;
    $actionForm->rec_type='action';
    $actionForm->model=$this->model;
    $actionForm->module_type=$this->model->moduleTypeId;
    //Log Call Form
    $callForm=new ALCallForm;
    $callForm->rec_type='call';
    $callForm->model=$this->model;
    $callForm->module_type=$this->model->moduleTypeId;
    //Log Time Form
    $timerForm=new ALTimerForm;
    $timerForm->rec_type='timer';
    $timerForm->model=$this->model;
    $timerForm->module_type=$this->model->moduleTypeId;
    //Calendar Event Form
    $calendarForm=new ALCalendarEventForm;
    $calendarForm->rec_type='calendar';
    $calendarForm->model=$this->model;
    $calendarForm->module_type=$this->model->moduleTypeId;

    return view('actionlog::components.activity',compact('module_type','commentForm','actionForm','callForm','timerForm','calendarForm'));
  }
}

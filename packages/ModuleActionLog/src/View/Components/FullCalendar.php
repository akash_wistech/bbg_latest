<?php
namespace NaeemAwan\ModuleActionLog\View\Components;

use Illuminate\View\Component;

class FullCalendar extends Component
{
  /**
  * display.
  *
  * @var string
  */
  public $display;
  /**
  * options.
  *
  * @var array
  */
  public $options;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($display='full',$options=[])
  {
    $this->display = $display;
    $this->options = $options;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $options['plugins'] = ['bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list'];
    $options['themeSystem'] = 'bootstrap';
    $options['initialDate'] = date("Y-m-d");
    $options['defaultDate'] = date("Y-m-d");
    $options['editable'] = false;
    $options['eventLimit'] = true;
    $options['navLinks'] = true;
    $options['nowIndicator'] = true;
    $options['dayMaxEvents'] = true;
    $options['now'] = now();

    $viewOptions = [
      'dayGridMonth'=>['buttonText'=>'month'],
      'timeGridWeek'=>['buttonText'=>'week'],
      'timeGridDay'=>['buttonText'=>'day']
    ];
    $cardBodyPadding = '';

    if($this->display=='widget'){
      $headerOptions = [
        'left'=>'title',
        'right'=>'prev,next today',
      ];
      $options['defaultView'] = 'timeGrid';

      $options['height'] = 440;
      $options['contentHeight'] = 440;
      $options['aspectRatio'] = 3;
      $cardBodyPadding=' p-1';
    }else{
      $headerOptions = [
        'left'=>'prev,next today',
        'center'=>'title',
        'right'=>'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      ];
      $options['defaultView'] = 'dayGridMonth';

      $options['height'] = 800;
      $options['contentHeight'] = 780;
      $options['aspectRatio'] = 3;
    }
    $options['header'] = $headerOptions;
    $options['views'] = $viewOptions;

    // dd($viewOptions);

    return view('actionlog::components.full_calendar',[
      'fcOptions'=>$options,
      'cardBodyPadding'=>$cardBodyPadding,
    ]);
  }
}

<?php
namespace NaeemAwan\MultipleEmailPhone\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\MultipleEmailPhone\Models\ModuleEmail;
use NaeemAwan\MultipleEmailPhone\Models\ModuleNumber;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class MultipleEmailPhoneController extends Controller
{
  /**
  * Remove the specified email
  *
  * @return \Illuminate\Http\Response
  */
  public function deleteEmail($module,$id,$email)
  {
    ModuleEmail::where([
      'module_type'=>$module,
      'module_id'=>$id,
      'email'=>$email,
    ])->delete();
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  }

  /**
  * Remove the specified number
  *
  * @return \Illuminate\Http\Response
  */
  public function deleteNumber($module,$id,$number)
  {
    ModuleNumber::where([
      'module_type'=>$module,
      'module_id'=>$id,
      'phone'=>$number,
    ])->delete();
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  }
}

<?php

namespace NaeemAwan\MultipleEmailPhone\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\MultipleEmailPhone\View\Components\MultipleEmailPhoneFields;
use NaeemAwan\MultipleEmailPhone\View\Components\MultipleEmailPhoneFieldDetails;

class MultipleEmailPhoneServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'multiemailphone');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'multiemailphone');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/multiemailphone'),
    ],'multiemailphone-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'multiemailphone-migrations');

    $this->loadViewComponentsAs('multiemailphone', [
      MultipleEmailPhoneFields::class,
      MultipleEmailPhoneFieldDetails::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

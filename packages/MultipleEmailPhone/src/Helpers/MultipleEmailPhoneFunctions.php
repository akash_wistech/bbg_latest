<?php
use NaeemAwan\MultipleEmailPhone\Models\ModuleEmail;
use NaeemAwan\MultipleEmailPhone\Models\ModuleNumber;

if (!function_exists('saveMultipleEmailNumber')) {
  /**
  * return list of modules workflow can have
  */
  function saveMultipleEmailNumber($model,$request,$type='form')
  {
    if($request->emails!=null){
      foreach($request->emails as $key=>$val){
        $model->emails[$key]=$val;
      }
    }
    if($request->phone_numbers!=null){
      foreach($request->phone_numbers as $key=>$val){
        $model->phone_numbers[$key]=$val;
      }
    }
    if($model->emails!=null){
      ModuleEmail::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
      ])
      ->whereNotIn('email',$model->emails)->delete();
			foreach($model->emails as $key=>$val){
				if($val!=''){
					$childRow=ModuleEmail::where([
						'module_type'=>$model->moduleTypeId,
						'module_id'=>$model->id,
						'email'=>$val
					]);
					if(!$childRow->exists()){
						$childRow=new ModuleEmail;
						$childRow->module_type=$model->moduleTypeId;
						$childRow->module_id=$model->id;
						$childRow->email=$val;
						$childRow->save();
					}
				}
			}
		}
		if($model->phone_numbers!=null){
      ModuleNumber::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
      ])
      ->whereNotIn('phone',$model->phone_numbers)->delete();
			foreach($model->phone_numbers as $key=>$val){
				if($val!=''){
					$childRow=ModuleNumber::where([
            'module_type'=>$model->moduleTypeId,
            'module_id'=>$model->id,
            'phone'=>$val
          ]);
					if(!$childRow->exists()){
						$childRow=new ModuleNumber;
						$childRow->module_type=$model->moduleTypeId;
						$childRow->module_id=$model->id;
						$childRow->phone=$val;
	  				$childRow->save();
					}
				}
			}
		}
  }
}

if (!function_exists('getModuleSavedEmails')) {
  /**
  * get saved tags
  */
  function getModuleSavedEmails($model)
  {
    return ModuleEmail::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->get();
  }
}

if (!function_exists('getModuleSavedNumbers')) {
  /**
  * get saved tags
  */
  function getModuleSavedNumbers($model)
  {
    return ModuleNumber::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->get();
  }
}

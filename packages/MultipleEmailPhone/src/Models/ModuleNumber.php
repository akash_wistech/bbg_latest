<?php

namespace NaeemAwan\MultipleEmailPhone\Models;
use App\Models\ChildTables;

class ModuleNumber extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_number';
}

<?php

namespace NaeemAwan\MultipleEmailPhone\Models;
use App\Models\ChildTables;

class ModuleEmail extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_email';
}

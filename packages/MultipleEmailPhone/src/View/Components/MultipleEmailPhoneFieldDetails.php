<?php
namespace Naeemawan\MultipleEmailPhone\View\Components;

use Auth;
use Illuminate\View\Component;
use Naeemawan\MultipleEmailPhone\Models\ModuleEmail;
use Naeemawan\MultipleEmailPhone\Models\ModuleNumber;

class MultipleEmailPhoneFieldDetails extends Component
{
    /**
     * model
     */
    public $model;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($model)
    {
     $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
      $emails = getModuleSavedEmails($this->model);
      $numbers = getModuleSavedNumbers($this->model);

        return view('multiemailphone::components.multiple-email-phone-field-details', [
          'emails' => $emails,
          'numbers' => $numbers,
          'model'=>$this->model
        ]);
    }
}

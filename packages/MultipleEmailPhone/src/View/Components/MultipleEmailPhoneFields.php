<?php
namespace NaeemAwan\MultipleEmailPhone\View\Components;

use Illuminate\View\Component;

class MultipleEmailPhoneFields extends Component
{
  /**
  * card
  */
  public $card=false;

  /**
  * model
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($model,$card=false)
  {
    $this->card = $card;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $emails = getModuleSavedEmails($this->model);
    $numbers = getModuleSavedNumbers($this->model);

    $en = 1;
    $nn = 1;

    return view('multiemailphone::components.multiple-email-number-fields', [
      'showCard' => $this->card,
      'model'=>$this->model,
      'emails'=>$emails,
      'numbers'=>$numbers,
      'en'=>$en,
      'nn'=>$nn,
    ]);
  }
}

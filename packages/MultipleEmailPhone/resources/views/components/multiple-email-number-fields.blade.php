@if($showCard==true)
<div class="card card-custom">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('customtags::customtags.heading')}}</h3>
    </div>
  </div>
  <div class="card-body p-2">
    @endif
    <div class="row">
      <div id="rdemails" class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('email',__('common.email')) }}
          <div class="input-group input-group-md">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
            {{ Form::email('email', $model->email, ['class'=>'form-control is-unique emls']) }}
            <div class="input-group-append">
              <button type="button" class="btn btn-success btn-sm btn-flat" onclick="addEmail()">
                <i class="fa fa-plus"></i>
              </button>
            </div>
          </div>
        </div>
        @if($emails!=null)
        @foreach($emails as $email)
        @php
        $model->emails[$en]=$email['email'];
        @endphp
        <div class="form-group">
          <div class="input-group input-group-md">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
            {{ Form::email('emails['.$en.']', null, ['class'=>'form-control is-unique emls']) }}
            <div class="input-group-append">
              <a class="btn btn-danger btn-sm btn-flat act-confirmation" href="{{ url('module-action/delete-email/'.$model->moduleTypeId.'/'.$model->id.'/'.$email['email']) }}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="rdemails">
                <i class="fa fa-trash"></i>
              </a>
            </div>
          </div>
        </div>
        @php
        $en++;
        @endphp
        @endforeach
        @endif
      </div>
      <div id="rdnumbers" class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('phone',__('common.phone')) }}
          <div class="input-group input-group-md">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>
            </div>
            {{ Form::text('phone', $model->phone, ['class'=>'form-control is-unique pnmbrs']) }}
            <div class="input-group-append">
              <button type="button" class="btn btn-success btn-sm btn-flat" onclick="addPhone()">
                <i class="fa fa-plus"></i>
              </button>
            </div>
          </div>
        </div>
        @if($numbers!=null)
        @foreach($numbers as $number)
        @php
        $model->phone_numbers[$nn]=$number['phone'];
        @endphp
        <div class="form-group">
          <div class="input-group input-group-md">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>
            </div>
            {{ Form::text('phone_numbers['.$nn.']', null, ['class'=>'form-control is-unique pnmbrs']) }}
            <div class="input-group-append">
              <a class="btn btn-danger btn-sm btn-flat act-confirmation" href="{{ url('module-action/delete-number/'.$model->moduleTypeId.'/'.$model->id.'/'.$number['phone']) }}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="rdnumbers">
                <i class="fa fa-trash"></i>
              </a>
            </div>
          </div>
        </div>
        @php
        $nn++;
        @endphp
        @endforeach
        @endif
      </div>
    </div>

    @if($showCard==true)
  </div>
</div>
@endif
@push('jsScripts')
$("body").on("click", ".remove-mep-row", function () {
  $(this).parents(".new-mep-row").remove();
});
@endpush
@push('jsFunc')
var emlRows = {{$en}};
function addEmail(){
  html ='';
  html+='<div class="new-mep-row form-group field-emails-'+emlRows+'">';
  html+='  <div class="input-group input-group-md">';
  html+='    <div class="input-group-prepend">';
  html+='      <span class="input-group-text"><i class="fas fa-envelope"></i></span>';
  html+='    </div>';
  html+='    <input type="text" id="emails-'+emlRows+'" class="form-control is-unique emls" name="emails['+emlRows+']">';
  html+='    <div class="input-group-append">';
  html+='      <button type="button" class="btn btn-danger btn-sm btn-flat remove-mep-row"><i class="fa fa-trash"></i></button>';
  html+='    </div>';
  html+='  </div>';
  html+='</div>';
  emlRows++;
  $("#rdemails").append(html);
}
var nmbrRows = {{$nn}};
function addPhone(){
  html ='';
  html+='<div class="new-mep-row form-group field-phone_numbers-'+nmbrRows+'">';
  html+='  <div class="input-group input-group-md">';
  html+='    <div class="input-group-prepend">';
  html+='      <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>';
  html+='    </div>';
  html+='    <input type="text" id="phone_numbers-'+nmbrRows+'" class="form-control is-unique pnmbrs" name="phone_numbers['+nmbrRows+']">';
  html+='    <div class="input-group-append">';
  html+='      <button type="button" class="btn btn-danger btn-sm btn-flat remove-mep-row"><i class="fa fa-trash"></i></button>';
  html+='    </div>';
  html+='  </div>';
  html+='</div>';
  nmbrRows++;
  $("#rdnumbers").append(html);
}
@endpush

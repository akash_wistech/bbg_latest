<div class="row">
  <div class="mt-3 col-sm-6">
    @if($emails!=null)
      <strong>{{ __('common.addition_email:') }}</strong>
      @php
      $n=1;
      @endphp
      @foreach($emails as $email)
        {{ ($n>1 ? ', ' : '').$email['email'] }}
        @php
        $n++;
        @endphp
      @endforeach
    @endif
  </div>
  <div class="mt-3 col-sm-6">
    @if($numbers!=null)
      <strong>{{ __('common.additional_phone:') }}</strong>
      @php
      $n=1;
      @endphp
      @foreach($numbers as $number)
        {{ ($n>1 ? ', ' : '').$number['phone'] }}
        @php
        $n++;
        @endphp
      @endforeach
    @endif
  </div>
</div>

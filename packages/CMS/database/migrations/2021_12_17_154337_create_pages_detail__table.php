<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('page_id')->index();
            $table->string('slug',120)->nullable();
            $table->string('lang',10)->nullable()->default('en')->index();
            $table->string('title',150)->nullable();
            $table->longText('descp')->nullable();
            $table->string('meta_title',255)->nullable();
            $table->text('meta_descp')->nullable();
            $table->text('meta_keyword')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_detail');
    }
}

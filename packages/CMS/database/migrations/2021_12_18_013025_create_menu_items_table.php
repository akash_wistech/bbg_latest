<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->id();
            $table->integer('menu_id')->index();
            $table->integer('parent_id')->nullable()->default(0)->index();
            $table->string('reference_type',255)->nullable();
            $table->integer('reference_id')->nullable();
            $table->string('url',120)->nullable();
            $table->string('icon_font',50)->nullable();
            $table->tinyInteger('position')->nullable()->default(0)->index();
            $table->string('title',120)->nullable();
            $table->string('css_class',120)->nullable();
            $table->string('target',20)->default('_self')->nullable();
            $table->tinyInteger('has_child')->nullable()->default(0)->index();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}

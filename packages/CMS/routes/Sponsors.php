<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\SponsorsController;


Route::get('sponsors', [SponsorsController::class,'index']);
Route::match(['get', 'post'],'sponsors/create', [SponsorsController::class,'create']);
Route::match(['get','post'],'sponsors/update/{id}',[SponsorsController::class, 'update']);
Route::match(['get','post'],'sponsors/view/{id}',[SponsorsController::class, 'view']);
Route::match(['get','post'],'sponsors/delete/{id}',[SponsorsController::class, 'destroy']);
Route::match(['get','post'],'sponsors/data-table-data',[SponsorsController::class, 'datatableData']);



<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\BannersController;


Route::match(['get', 'post'],'banners', [BannersController::class,'index']);
Route::match(['get', 'post'],'banners/create', [BannersController::class,'create']);
Route::match(['get','post'],'banners/update/{id}',[BannersController::class, 'update']);
Route::match(['get','post'],'banners/view/{id}',[BannersController::class, 'view']);
Route::match(['get','post'],'banners/delete/{id}',[BannersController::class, 'delete']);
Route::match(['get','post'],'banners/data-table-data',[BannersController::class, 'datatableData']);
Route::match(['get','post'],'banners/popdescmodal',[BannersController::class, 'popdescmodal']);
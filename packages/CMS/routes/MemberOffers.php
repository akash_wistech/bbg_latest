<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\MemberOffersController;


Route::match(['get', 'post'],'member-offers', [MemberOffersController::class,'index']);
Route::match(['get', 'post'],'member-offers/create', [MemberOffersController::class,'create']);
Route::match(['get','post'],'member-offers/update/{id}',[MemberOffersController::class, 'update']);
Route::match(['get','post'],'member-offers/view/{id}',[MemberOffersController::class, 'view']);
Route::match(['get','post'],'member-offers/delete/{id}',[MemberOffersController::class, 'destroy']);
Route::match(['get','post'],'member-offers/data-table-data',[MemberOffersController::class, 'datatableData']);
Route::match(['get','post'],'member-offers/getRelToMemCom',[MemberOffersController::class, 'getRelToMemCom']);




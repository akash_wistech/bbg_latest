<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\ViewFromChairController;


Route::get('view-from-chair', [ViewFromChairController::class,'index']);
Route::match(['get', 'post'],'view-from-chair/create', [ViewFromChairController::class,'create']);
Route::match(['get','post'],'view-from-chair/update/{id}',[ViewFromChairController::class, 'update']);
Route::match(['get','post'],'view-from-chair/view/{id}',[ViewFromChairController::class, 'view']);
Route::match(['get','post'],'view-from-chair/delete/{id}',[ViewFromChairController::class, 'destroy']);
Route::match(['get','post'],'view-from-chair/data-table-data',[ViewFromChairController::class, 'datatableData']);
// Route::match(['get','post'],'view-from-chair/delete/{id}',[ViewFromChairController::class, 'delete']);


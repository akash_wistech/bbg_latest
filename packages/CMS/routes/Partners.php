<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\PartnersController;


Route::get('partners', [PartnersController::class,'index']);
Route::match(['get', 'post'],'partners/create', [PartnersController::class,'create']);
Route::match(['get','post'],'partners/update/{id}',[PartnersController::class, 'update']);
Route::match(['get','post'],'partners/view/{id}',[PartnersController::class, 'view']);
Route::match(['get','post'],'partners/delete/{id}',[PartnersController::class, 'destroy']);
Route::match(['get','post'],'partners/data-table-data',[PartnersController::class, 'datatableData']);

<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\CMS\Http\Controllers\CommitteMembersController;
use NaeemAwan\CMS\Http\Controllers\MemberOffersController;
use NaeemAwan\CMS\Http\Controllers\SponsorsController;
use NaeemAwan\CMS\Http\Controllers\ViewFromChairController;

Route::group(['middleware' => ['web','auth']], function () {
  include('CommitteMembers.php');
  include('vfc.php');
  include('Sponsors.php');
  include('Gallery.php');
  include('Categories.php');
  include('MemberOffers.php');
  include('Banners.php');
  include('Partners.php');
  Route::get('/pages', [NaeemAwan\CMS\Http\Controllers\PagesController::class, 'index'])->name('pages');
  Route::match(['post','get'],'/pages/create', [NaeemAwan\CMS\Http\Controllers\PagesController::class, 'create'])->name('pages.create');
  Route::match(['post','get'],'/pages/edit/{id}', [NaeemAwan\CMS\Http\Controllers\PagesController::class, 'edit'])->name('pages.edit.{id}');
  Route::match(['post','get'],'/pages/status/{id}', [NaeemAwan\CMS\Http\Controllers\PagesController::class, 'status'])->name('pages.status.{id}');
  Route::post('/pages/update/{id}', [NaeemAwan\CMS\Http\Controllers\PagesController::class, 'update'])->name('pages.update');
  Route::post('/pages/delete/{id}', [NaeemAwan\CMS\Http\Controllers\PagesController::class, 'destroy'])->name('pages.destroy.{id}');

  Route::get('/menus', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'index'])->name('menus');
  Route::match(['post','get'],'/menus/create', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'create'])->name('menus.create');
  Route::match(['post','get'],'/menus/edit/{id}', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'edit'])->name('menus.edit.{id}');
  Route::match(['post','get'],'/menus/status/{id}', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'status'])->name('menus.status.{id}');
  Route::match(['post','get'],'/menus/add-item', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'addItem'])->name('menus.add.item');
  Route::post('/menus/update/{id}', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'update'])->name('menus.update');
  Route::post('/menus/delete/{id}', [NaeemAwan\CMS\Http\Controllers\MenusController::class, 'destroy'])->name('menus.destroy.{id}');




});




Route::group(['middleware' => ['web']], function () {


// Frontend routes
  Route::get('board-members-info', [CommitteMembersController::class, 'boardmembersinfo'])->name('board-members-info');
  Route::match(['get', 'post'], '/committee-details/{id}', [CommitteMembersController::class,'CommitteeDetails']);

  Route::match(['get', 'post'], '/operations-info', [CommitteMembersController::class,'OperationsInfo'])->name('operations-info');
  Route::match(['get', 'post'], '/future-vision-collective', [CommitteMembersController::class,'FutureVisionCollective'])->name('future-vision-collective');
  Route::match(['get', 'post'], '/business-details/{id}', [CommitteMembersController::class,'BusinessDetails']);

// for frontend 
  Route::match(['get','post'],'membership/offers',[MemberOffersController::class, 'MemberOffers']);
  Route::match(['get','post'],'membership/offers/detail/{id}',[MemberOffersController::class, 'MemberOffersDetail']);

// Sponsors Frontend Routes
  Route::get('sponsors-fend', [SponsorsController::class, 'sponsorsfend'])->name('sponsors-fend');
  Route::match(['get', 'post'], '/sponsor-fend/sponsor-details/{id}', [SponsorsController::class,'SponsorDetails']);



// frontend routes
// Route::match(['get', 'post'], '/view-from-chair-list', [ViewFromChairController::class,'ViewFromChairList']);
  Route::match(['get', 'post'], '/view-from-chair-detail', [ViewFromChairController::class,'ViewFromChair']);


  Route::match(['get', 'post'], '/slug-pages/', [NaeemAwan\CMS\Http\Controllers\PagesController::class,'ViewBySlug']);




});

<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\GalleryController;


Route::match(['get', 'post'],'gallery', [GalleryController::class,'index']);
Route::match(['get', 'post'],'gallery/create', [GalleryController::class,'create']);
Route::match(['get','post'],'gallery/update/{id}',[GalleryController::class, 'update']);
Route::match(['get','post'],'gallery/view/{id}',[GalleryController::class, 'view']);
Route::match(['get','post'],'gallery/delete/{id}',[GalleryController::class, 'destroy']);
Route::match(['get','post'],'gallery/data-table-data',[GalleryController::class, 'datatableData']);




Route::match(['get','post'],'gallery/sendinvoice',[GalleryController::class, 'sendinvoice']);
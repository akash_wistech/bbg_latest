<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\CategoriesController;


Route::match(['get', 'post'],'categories', [CategoriesController::class,'index']);
Route::match(['get', 'post'],'categories/create', [CategoriesController::class,'create']);
Route::match(['get','post'],'categories/update/{id}',[CategoriesController::class, 'update']);
Route::match(['get','post'],'categories/view/{id}',[CategoriesController::class, 'view']);
Route::match(['get','post'],'categories/delete/{id}',[CategoriesController::class, 'destroy']);
Route::match(['get','post'],'categories/data-table-data',[CategoriesController::class, 'datatableData']);
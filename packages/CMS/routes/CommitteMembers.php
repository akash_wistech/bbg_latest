<?php
use Illuminate\Support\Facades\Route;


use NaeemAwan\CMS\Http\Controllers\CommitteMembersController;


Route::get('committe-members', [CommitteMembersController::class,'index']);
Route::match(['get', 'post'],'committe-members/create', [CommitteMembersController::class,'create']);
Route::match(['get','post'],'committe-members/update/{id}',[CommitteMembersController::class, 'update']);
Route::match(['get','post'],'committe-members/view/{id}',[CommitteMembersController::class, 'view']);
Route::match(['get','post'],'committe-members/delete/{id}',[CommitteMembersController::class, 'destroy']);
Route::match(['get','post'],'committe-members/data-table-data',[CommitteMembersController::class, 'datatableData']);



<?php
return [
	'heading' => 'View From Chair',
	//form fields
	'title' => 'Title',
	'display_on_home' => 'Show On Home Page',
	'sort_order' => 'Sort Order',
	'date' => 'Date',
	'status' => 'Publish',
	'description' => 'Description',
	'short_description' => 'Short Description',
	'image' => 'Main Image',
	'id' => 'Id',


	'saved' => 'View From Chair Saved Successfully',
	'updated' => 'View From Chair Updated Successfully',

];

<?php
return [
  'heading' => 'Pages',
  'saved' => 'Page saved successfully',
  'notsaved' => 'Error while saving page',
  'deleted' => 'Page trashed successfully',
];

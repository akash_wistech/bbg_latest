<?php
return [
	'heading' => 'Categories',
	//form fields
	'id' => 'Id',
	'parent_id' => 'Parent',
	'title' => 'Title',
	'type' => 'Type',
	'short_description' => 'Short Description',
	'description' => 'Description',
	'image' => 'Image',
	'baner_image' => 'Banner Image',
	'meta_title' => 'Meta Title',
	'meta_description' => 'Meta Description',
	'status' => 'Status',
	'created' => 'Created',
	//toast
	'saved' => 'Saved Successfully',
	'updated' => 'Updated Successfully',

];
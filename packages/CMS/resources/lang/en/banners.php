<?php
return [
	'heading' => 'Banners',
	'id' => 'Id',
	'name' => 'Name',
	'slug' => 'Slug',
	'type' => 'Type',
	'status' => 'Status',
	'created' => 'Created',
	'saved' => 'Saved Successfully',
	'updated' => 'Updated Successfully',
	'deleted' => 'Deleted Successfully',

];

<?php
return [
	'heading' => 'Sponsors',
	//form fields
	'title' => 'Title',
	'sort_order' => 'Sort Order',
	'type' => 'Type',
	'status' => 'Publish',
	'description' => 'Description',
	'short_description' => 'Short Description',
	'image' => 'Main Image',
	'id' => 'Id',


	'saved' => 'Sponsors Saved Successfully',
	'updated' => 'Sponsors Updated Successfully',

];

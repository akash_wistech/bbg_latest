<?php
return [
	'heading' => 'Committe Members',
	//form fields
	'full_name' => 'Full Name',
	'type' => 'Type',
	'email' => 'email',
	'sort_order' => 'Sort Order',
	'phonenumber' => 'Phone Number',
	'status' => 'Status',
	'description' => 'Description',
	'designation' => 'Designation',
	'id' => 'Id',
	'image' => 'Main Image',



	'saved' => 'Committe Members Saved Successfully',
	'updated' => 'Committe Members Updated Successfully',

];

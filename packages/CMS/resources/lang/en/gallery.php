<?php
return [
	'heading' => 'Gallery',
	//form fields
	'id' => 'Id',
	'name' => 'Name',
	'image' => 'Main Image',
	'video_link' => 'Video Link',
	'size' => 'Size',
	'position' => 'Position',
	'status' => 'Status',
	'description' => 'Description',
	//toast
	'saved' => 'Gallery Saved Successfully',
	'updated' => 'Gallery Updated Successfully',

];

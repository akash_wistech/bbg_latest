<?php
return [
	'heading' => 'Member Offers',
	//form fields
	'id' => 'Id',
	'category_id' => 'Category',
	'title' => 'Title',
	'short_description' => 'Short Description',
	'description' => 'Description',
	'image' => 'Image',
	'is_active' => 'Status',
	'sort_order' => 'Sort Order',
	'offer_rel' => 'Related to',
	'offer_rel_id' => 'Name',
	'created' => 'Created',
	//toast
	'saved' => 'Saved Successfully',
	'updated' => 'Updated Successfully',

];
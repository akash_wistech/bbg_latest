<?php
return [
	'heading' => 'Partners',
	'saved' => 'Saved Successfully',
	'updated' => 'Updated Successfully',
	'id' => 'Id',
	'name' => 'Name',
	'description' => 'Description',
	'created' => 'Created',
	'logo' => 'Logo',
];
{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif



    <div class="row px-0">

      <div class="col-md-4 col-lg-5 px-0">
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('title',__('managecontent::categories.title')) }}
            {{ Form::text('title', $model->title, ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group"> 
            {{ Form::label('type',__('managecontent::categories.type')) }}
            {{ Form::select('type', [''=>__('common.select')] + getCategoriesTypeArr(), $model->type,  ['class'=>'form-control']) }}
          </div>
        </div>
      </div> 


      <div class="col-md-4 col-lg-5  px-0 ">
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('parent_id',__('managecontent::categories.parent_id')) }}
            {{ Form::select('parent_id', [''=>__('common.select')] + getCategoriesArr(), $model->parent_id, ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group"> 
            {{ Form::label('status',__('managecontent::categories.status')) }}
            {{ Form::select('status', [''=>__('common.select')] + getStatusArr(), $model->status,  ['class'=>'form-control']) }}
          </div>
        </div>

      </div>


      <!-- image here -->
      <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-4">
        <div id="images" class="form-group pb-0 mb-2">
          <a href="" data-thumbid="thumb-image-image" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
            <img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
            alt="" width="145" height="125" title=""
            data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
          </a>
          {{ Form::text('image', $model->image, ['maxlength' => true, 'data-targetid' => 'input-image-image','class'=>'form-control d-none']) }}
        </div>
        <span class="label label-xl label-muted font-weight-bold label-inline mx-12">{{__('managecontent::categories.image')}}</span>
      </div>
      <!-- image end -->
    </div>



    <div class="row px-0">
      <div class="col-md-4 col-lg-5 px-0">

        <div class="col-12">
          <div class="form-group">
            {{ Form::label('meta_title',__('managecontent::categories.meta_title')) }}
            {{ Form::textarea('meta_title', $model->meta_title,  ['class'=>'form-control', 'rows'=> 3]) }}
          </div>
        </div>

      </div> 


      <div class="col-md-4 col-lg-5  px-0 ">
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('meta_description',__('managecontent::categories.meta_description')) }}
            {{ Form::textarea('meta_description', $model->meta_description, ['class'=>'form-control', 'rows'=> 3]) }}
          </div>
        </div>
      </div>
      <!-- image here -->
      <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-4">
        <div id="images" class="form-group pb-0 mb-2">
          <a href="" data-thumbid="thumb-image-baner-image" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
            <img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
            alt="" width="145" height="125" title=""
            data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
          </a>
          {{ Form::text('baner_image', $model->baner_image, ['maxlength' => true, 'data-targetid' => 'input-image-baner-image','class'=>'form-control d-none']) }}
        </div>
        <span class="label label-xl label-muted font-weight-bold label-inline mx-6">{{__('managecontent::categories.baner_image')}}</span>
      </div>
      <!-- image end -->


    </div>


    <div class="row">
      <div class="col-10">
        <div class="form-group"> 
          {{ Form::label('short_description',__('managecontent::categories.short_description')) }}
          {{ Form::textarea('short_description', $model->short_description, ['class'=>'form-control', 'rows' => 4]) }}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('description',__('managecontent::categories.description')) }}
          {{ Form::textarea('description', $model->description, ['class'=>'form-control editor']) }}
        </div>
      </div>
    </div>

  </div>




  <div class="card-footer">
    <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
    <a href="{{ url('/news') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}



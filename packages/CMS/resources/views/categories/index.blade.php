<?php
$btnsList = [];
if(checkActionAllowed('create','categories')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'categories/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];
$dtColsArr[]=['title'=>__('managecontent::categories.image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.baner_image'),'data'=>'baner_image','name'=>'baner_image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.parent_id'),'data'=>'parent_id','name'=>'parent_id','orderable'=>false,'width'=>70];
$dtColsArr[]=['title'=>__('managecontent::categories.title'),'data'=>'title','name'=>'title','orderable'=>false,'width'=>70];
$dtColsArr[]=['title'=>__('managecontent::categories.type'),'data'=>'type','name'=>'type','orderable'=>false,'width'=>70];
$dtColsArr[]=['title'=>__('managecontent::categories.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.meta_title'),'data'=>'meta_title','name'=>'meta_title','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.meta_description'),'data'=>'meta_description','name'=>'meta_description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::categories.status'),'data'=>'status','name'=>'status','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::categories.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::categories.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'categories'" :cbSelection="false" :exportMenu="true" :jsonUrl="'categories/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection


@push('js')
@endpush
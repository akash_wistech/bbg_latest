@extends('layouts.app')

@section('title', __('managecontent::categories.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'categories'=>__('managecontent::categories.heading'),
__('common.view')
]])
@endsection

<?php //`dd($model); ?>
@section('content')
<div class="card card-custom pb-0" data-card="true">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">Information</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','categories'))
			{
				?>
				<a href="{{url('categories/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			} 
			?> 
		</div>
	</div>
	<div class="card-body">
		<div class="row" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.created')}}:</strong>{{formatDateTime($model->created_at)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.type')}}:</strong>{{getCategoriesTypeArr()[$model->type]}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.meta_description')}}:</strong>{{$model->meta_description}}
				</div>
			</div>


			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.title')}}:</strong>{{$model->title}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.status')}}: </strong>{{getStatusArr()[$model->status]}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.short_description')}}: </strong>{!! $model->short_description !!}
				</div>
			</div>

			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.parent_id')}}: </strong>{{getCatParentTitle($model->parent_id)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.meta_title')}}: </strong>{{$model->meta_title}}
				</div>
			</div>
		</div>


		<div class="row" style="font-size: 14px; text-align: justify; line-height: 1.6; ">
			<div class="col px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.description')}}: </strong>{!! $model->description !!}
				</div>
			</div>
		</div>

		<div class="row py-2" style="font-size: 14px;">
			<div class="col-6 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.image')}}: </strong>{!!getImageTag($model->image)!!}
				</div>
			</div>
			<div class="col-6">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::categories.baner_image')}}:</strong>{!!getImageTag($model->baner_image)!!}
				</div>
			</div>
		</div>


	</div>

</div>
@endsection
{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif



    <div class="row px-0">

      <div class="col-md-4 col-lg-5 px-0">
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('name',__('managecontent::gallery.name')) }}
            {{ Form::text('name', $model->name, ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('video_link',__('managecontent::gallery.video_link')) }}
            {{ Form::text('video_link', $model->video_link, ['class'=>'form-control']) }}
          </div>
        </div>
      </div> 


      <div class="col-md-4 col-lg-5  px-0 ">
        <div class="col-12">
          <div class="form-group"> 
            {{ Form::label('size',__('managecontent::gallery.size')) }}
            {{ Form::select('size', getGallerySizeArr(), $model->size,  ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('position',__('managecontent::gallery.position')) }}
            {{ Form::select('position', getGalleryPositionArr(), $model->position,  ['class'=>'form-control']) }}
          </div>
        </div>
      </div>


      <!-- image here -->
      <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-2 pl-0 parent-div">
        <div id="images" class="form-group pb-0 mb-2">
          <a href="" data-thumbid="thumb-image" data-toggle="image" class="img-thumbnail shadow" data-content=""  width="20">
            <img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
            alt="" width="145" height="125" title=""
            data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
          </a>
          {{ Form::text('image', $model->image, ['maxlength' => true, 'data-targetid' => 'input-image','class'=>'form-control d-none']) }}
        </div>
        <span class="label label-xl label-muted font-weight-bold label-inline mx-8">{{__('managecontent::gallery.image')}}</span>
      </div>
      <!-- image end -->

      
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-10">
        <div class="form-group">
          {{ Form::label('status',__('managecontent::gallery.status')) }}
          {{ Form::select('status', getGalleryStatusArr(), $model->status, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('description',__('managecontent::gallery.description')) }}
          {{ Form::textarea('description', $model->description, ['class'=>'form-control editor']) }}
        </div>
      </div>
    </div>

  </div>




  <div class="card-footer">
    <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
    <a href="{{ url('/news') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}



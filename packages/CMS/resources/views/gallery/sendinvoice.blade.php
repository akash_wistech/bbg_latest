@extends('layouts.scope')
@section('content')

<button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#exampleModalSizeXl">Modal - xl</button>


<!--begin::Modal-->
<div class="modal fade" id="exampleModalSizeXl" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeXl" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Re-Send Invoice</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<section class="MainArea" style="font-family: 'Century Gothic'">
					<div class="container">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

								<div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8" id="invoice_logo">
									<a href="/"><img src="https://d2a5i90cjeax12.cloudfront.net/BBG_NEW_WEBSITE_PLACEHOLDER_RGB_200x60px5f6a012c5ec34.jpg" alt="BBG-Dubai"></a>
								</div>

								<div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4" id="invoice_date_number">
									<h1 style="color: #000;"><b>PRO-FORMA INVOICE</b></h1>
									<strong><span>Invoice date: Mon, 13 Dec, 2021</span></strong><br>
									<strong><span>Invoice number: M-0011506</span></strong>
								</div>

								<div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" id="invoice_comapny_info">
									<br><br>
									<strong><br></strong>
									<strong><br>Telehephone: +971 4 397 0303<br></strong>
									<strong>Email: info@bbgdxb.com<br></strong>
									<strong>VAT Registration no:
										<u>TRN 100001769700003</u><br></strong>
									</div>

									<div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6" id="invoice_client_info">
										<br>
										<strong>Bill To:</strong><br>
										<strong>Name: kiran arif(Member)</strong><br>
										<strong>Company: </strong><br>
										<strong>Address: Al Maktoom Road - Dubai<br></strong>
										<strong>P.OBox: <br></strong>
										<strong>Telephone: 09008766898</strong><br>
										<strong>VAT Registration no:<u></u><br><br></strong>
									</div>

									<div style="width: 100%;" class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
										<div style="width: 100%;">
											<table class="table" style="width: 100%; float: left;" cellpadding="5">
												<thead>
													<tr>
														<th class="col-md-8" style="width:70%; background: #1d355f; color: #ffffff; text-align: left;">Item
														</th>
														<th class="col-md-4" style="width:30%; background: #1d355f; color: #ffffff; text-align: center">Amount
														</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Membership Joining Fee (Essential )</td>
														<td style="text-align: right;">AED 600</td>
													</tr>
													<tr>
														<td>Essential  (Essential )</td>
														<td style="text-align: right;">AED 1000</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div style="width: 100%;border-top: #000 solid 1px;margin-top: 30px;clear: both;">
											<table class="table" style="width: 100%; float: left;" cellpadding="5">
												<tbody>
													<tr>
														<td style="width:70%;text-align: left;">
															<strong>Subtotal</strong>
															<br><strong>VAT(5%)</strong>
														</td>
														<td style="width:30%;text-align: right;">
															<strong>1600</strong>
															<br><strong>80</strong>
														</td>
													</tr>

													<tr>
														<td style="width:70%;text-align: left;">
															<br><strong>Total</strong>
														</td>
														<td style="width:30%;text-align: right;">
															<br>
															<strong>AED <span id="before_adjust_total">1680</span>
																<input type="hidden" value="1680" id="actual_total">
															</strong>

															<a id="edit_invoice_btn" href="#" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>                                
														</td>
													</tr>

												</tbody>
											</table>
										</div>

									</div>

									<div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
										<a href="https://bbgdubai.org/site/invoice-payment?invoice_id=11506">Pay Online</a><br>
										<p>&nbsp;</p>
										<p><strong>Below are the methods of payments acceptable by BBG:</strong></p>
										<p><strong>CHEQUE:</strong>&nbsp;Payable to British Business Group</p>
										<p><strong>BANK TRANSFER-(NET OF BANK CHARGES)</strong></p>
										<table width="98%" cellspacing="0" cellpadding="0" align="left">
											<tbody>
												<tr>
													<td width="25%">In favor&nbsp;of</td>
													<td width="1%">:</td>
													<td>British Business Group</td>
												</tr>
												<tr>
													<td width="25%">Bank</td>
													<td valign="top" width="1%">:</td>
													<td>HSBC Bank Middle East, P.O. Box 3766, Jumeirah 1, Dubai, UAE</td>
												</tr>
												<tr>
													<td width="25%">Account No</td>
													<td width="1%">:</td>
													<td>030-123756-001</td>
												</tr>
												<tr>
													<td width="25%">IBAN</td>
													<td width="1%">:</td>
													<td>AE880200000030123756001</td>
												</tr>
												<tr>
													<td width="25%">Swift Code</td>
													<td width="1%">:</td>
													<td>BBMEAEAD</td>
												</tr>
											</tbody>
										</table>
										<p>&nbsp;</p>
										<p style="color: red; font-size: 10px; text-align: center;"><span style="color: #ff0000;"><em><strong>*Note:&nbsp;</strong>Tax Invoice will be issued upon payment.</em></span></p>
										<p><br><br></p>                
									</div>
								</div>
							</div>


						</div>
					</section>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-info" id="member_send_invoice_to_admin">Send to admin</button>
					<button type="button" class="btn btn-default btn-primary" id="member_send_invoice">Send Invoice</button>
					<button type="button" class="btn btn-default btn-danger" data-dismiss="modal" id="closeModalBox">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--end::Modal-->






	@endsection


	<style>
		.searchResult .view a {
			font-weight: bold;
		}

		#events, #news {
			background: #ffffff;
		}

		.searchResult .view {
			padding: 5px 5px 10px;
			margin-bottom: 10px;
			border-bottom: dashed 1px #ccc;
			-webkit-transition: background 0.2s;
			transition: background 0.2s;
		}
		#invoice_logo, #invoice_comapny_info {
			width: 60%;
			float: left;
		}

		#invoice_date_number {
			width: 35%;
			float: right;
		}

		#invoice_client_info{
			width: 40%;
			float: right;
		}

		@media (max-width: 767px) {

			#invoice_logo, #invoice_comapny_info {
				width: 100% !important;
				float: left;
			}

			#invoice_date_number {
				width: 100% !important;
				float: right;
			}

			#invoice_client_info{
				width:  100% !important;
				float: right;
			}

			.modal-dialog{
				width: 95% !important;
			}
		}
	</style>
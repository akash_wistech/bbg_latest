<?php
$btnsList = [];
if(checkActionAllowed('create','gallery')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'gallery/create', 'method'=>'post'];
}

$dtColsArr = [];

$advSearchCols=[];

$dtColsArr[]=['title'=>__('managecontent::gallery.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('managecontent::gallery.image'),'data'=>'image','name'=>'image'];
$dtColsArr[]=['title'=>__('managecontent::gallery.name'),'data'=>'name','name'=>'name'];
$dtColsArr[]=['title'=>__('managecontent::gallery.video_link'),'data'=>'video_link','name'=>'video_link'];
$dtColsArr[]=['title'=>__('managecontent::gallery.size'),'data'=>'size','name'=>'size'];
$dtColsArr[]=['title'=>__('managecontent::gallery.position'),'data'=>'position','name'=>'position'];
$dtColsArr[]=['title'=>__('managecontent::gallery.description'),'data'=>'description','name'=>'description'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by',];
$dtColsArr[]=['title'=>__('managecontent::gallery.status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::gallery.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::gallery.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'gallery'" :cbSelection="false" :exportMenu="true" :jsonUrl="'active-gallery-datatable'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection
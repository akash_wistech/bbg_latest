<?php
$btnsList = [];
if(checkActionAllowed('create','sponsors')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'sponsors/create', 'method'=>'post'];
}

$dtColsArr = [];

$advSearchCols=[];

$dtColsArr[]=['title'=>__('managecontent::sponsors.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::sponsors.image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::sponsors.title'),'data'=>'title','name'=>'title','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::sponsors.type'),'data'=>'type','name'=>'type','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::sponsors.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false];
// $dtColsArr[]=['title'=>__('managecontent::sponsors.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::sponsors.sort_order'),'data'=>'sort_order','name'=>'sort_order','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('managecontent::sponsors.status'),'data'=>'status','name'=>'status','orderable'=>false];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::sponsors.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::sponsors.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'sponsors'" :cbSelection="false" :exportMenu="true" :jsonUrl="'sponsors/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection

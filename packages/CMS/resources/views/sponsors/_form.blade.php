{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif



    <div class="row px-0">

      <div class="col-md-4 col-lg-5 px-0">
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('title',__('managecontent::sponsors.title')) }}
            {{ Form::text('title', $model->title, ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('sort_order',__('managecontent::sponsors.sort_order')) }}
            {{ Form::number('sort_order', $model->sort_order, ['class'=>'form-control']) }}
          </div>
        </div>
      </div> 


      <div class="col-md-4 col-lg-5  px-0 ">
        <div class="col-12">
          <div class="form-group"> 
            {{ Form::label('type',__('managecontent::sponsors.type')) }}
            {{ Form::select('type', [''=>__('common.select')] + getSponsorTypeArr(), $model->type,  ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-12">
          <div class="form-group">
            {{ Form::label('status',__('managecontent::sponsors.status')) }}
            {{ Form::select('status', [''=>__('common.select')] + getSponsorsstatusArr(), $model->status,  ['class'=>'form-control']) }}
          </div>
        </div>

      </div>


      <!-- image here -->
      <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-4">
        <div id="images" class="form-group pb-0 mb-2">
          <a href="" data-thumbid="thumb-image" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
            <img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
            alt="" width="145" height="125" title=""
            data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
          </a>
          {{ Form::text('image', $model->image, ['maxlength' => true, 'data-targetid' => 'input-image','class'=>'form-control d-none']) }}
        </div>
        <span class="label label-xl label-muted font-weight-bold label-inline mx-6">Main Image</span>
      </div>
      <!-- image end -->
    </div>






    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('short_description',__('managecontent::sponsors.short_description')) }}
          {{ Form::textarea('short_description', $model->short_description, ['class'=>'form-control', 'rows' => 4]) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('description',__('managecontent::sponsors.description')) }}
          {{ Form::textarea('description', $model->description, ['class'=>'form-control editor']) }}
        </div>
      </div>
    </div>

  </div>




  <div class="card-footer">
    <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
    <a href="{{ url('/news') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}



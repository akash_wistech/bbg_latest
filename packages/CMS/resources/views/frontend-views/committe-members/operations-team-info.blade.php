@extends('frontend.app')
@section('content')

<section class="MainArea">
	<div class="container">
		<div class="row">

			
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<div class="Heading">
						<h3>Business Team</h3>
					</div>
					<hr/>
					<div class="committee text-center">
						<div class="row">
							<?php
							if (isset($staff) && $staff <> null) {
								foreach ($staff as $data) {
									if($data <> null) {
										$no_image = asset('assets/images/dummy-image-1.jpg');
										?>
										
										<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">

											<a class="committee_info" href="<?= url('business-details',['id'=>$data->id]) ?>" style="color: #666666 !important;" target="_blank">
												<img src="<?= ($data->image) ? $data->image : $no_image; ?>" class="business_staff_image img-fluid"
												alt="<?= $data->full_name;?>">
											</a>

											<div class="name_and_postion">
												<h5><a href="<?= url('committee-details',['id'=>$data->id]) ?>" style="color: #666666 !important;"><?= $data->full_name ?></a></h5>

												<h6><?= $data->designation; ?></h6>
											</div>
											<h3><a href="mailto:<?=$data->email?>" class="commitee_staff_email"><?= $data->email ?></a></h3>

										</h3>
									</div>
									<?php
								}
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>


		<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
				<div class="SideHeading">
					<h3>Get In Touch</h3>
				</div>
				<div class="SideCalender">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
								<i class="fa fa-map-marker blueicon" aria-hidden="true"></i>
							</div>
							<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
								<p> 
									<?= getSetting('company_address') ? nl2br(getSetting('company_address')) : ''; ?>
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
								<i class="fa fa-envelope blueicon" aria-hidden="true"></i>
							</div>
							<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
								<p>
									<strong>Email:</strong> <a href="mailto:<?= getSetting('admin_email') ?>"><?= getSetting('admin_email') ?></a>
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 contentcenter">
								<i class="fa fa-phone blueicon" aria-hidden="true"></i>
							</div>
							<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
								<p>
									<strong>Telephone:</strong> <?= getSetting('telephone') ? getSetting('telephone') : ''; ?>
								</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>

@endsection

@push('css')
<style>
	.blueicon {
		color: #1f3760 !important;
		font-size: 25px !important;
	}
</style>
@endpush
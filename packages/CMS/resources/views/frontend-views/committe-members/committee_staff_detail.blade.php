@extends('frontend.app')
@section('content')

<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">

					<?php
					if (isset($data) && $data <> null) {
						?>

						<div class="Heading">
							<h3><?= $data->full_name; ?></h3>
						</div>
						<div class="EventDetail">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-justify">
									<img width="200" align="left" style="margin-right: 10px;"
									src="<?= (!empty($data->image)) ? $data->image : asset('assets/images/dummy-image-1.jpg'); ?>"
									class="img-fluid" alt="<?= $data->full_name; ?>">
									<?= $data->description; ?>
								</div>
							</div>
						</div>


						<?php
					}
					?>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 boxContact paddingRightLeft">
					<div class="SideHeadingGetInTouch">
						<h3>Get In Touch</h3>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">

						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 contactinfo-side contentcenter">
								<i class="fa fa-user blueicon" aria-hidden="true"></i>
								<strong>
									<span class="BoldText"><?= $data->full_name; ?></span></strong> <br>

								</div>
							</div>

							<div class="row">

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 contactinfo-side contentcenter">
									<i class="fa fa-user-o blueicon" aria-hidden="true"></i>
									<strong>
										<span class="BoldText"><?= $data->designation; ?></span></strong>
										<br>

									</div>
								</div>

								<div class="row">

									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 contactinfo-side contentcenter">
										<i class="fa fa-envelope blueicon" aria-hidden="true"></i>
										<strong>
											<span class="BoldText"><a href="mailto:<?=$data->email?>" class="" style="background: none !important; color:#1d355f;"><?= $data->email ?></a></span>
										</strong> <br>

									</div>
								</div>
								<?php
								if ($data->phonenumber) {
									?>
									<div class="row">

										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 contactinfo-side contentcenter">
											<i class="fa fa-phone blueicon" aria-hidden="true"></i>
											<strong>
												<span class="BoldText"><?= $data->phonenumber; ?></span></strong>
												<br>


											</div>
										</div>
										<?php
									}
									?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			@endsection

			@push('css')
			<style>
				a.btn-floating {
					background: none !important;
				}
			</style>
			<style>
				.blueicon {
					color: #1f3760 !important;
					font-size: 16px !important;
					margin: 5px;
				}

				.SideHeadingGetInTouch {
					background: #bd1f2f !important;
					color: #fff !important;
					padding: 14px !important;
					font-size: 20px !important;
					font-weight: bold !important;
					text-align: center !important;
				}

				.boxContact{
					border: 1px solid #ddd;
				}
			</style>
			@endpush
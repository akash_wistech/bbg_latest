<?php  
$title = 'PARTNERS AND SPONSORS';
// dd($results);
?>

@extends('frontend.app')
@section('content')
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<div class="Heading">
						<h3>{{$title}}</h3>
					</div>
					<?php
					if (isset($results) && $results <> null && count($results) > 0) {

						foreach ($results as $data) {
							// dd($result);
							?>
							<div class="Event1">
								<div class="row">
									<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
										<img src="<?= $data->image; ?>" class="img-fluid" alt="">
									</div>
									<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
										<div class="EventText">
											<h4><?= $data->title; ?></h4>
											<p class="text-justify"><?= strlen($data->short_description) > 240 ? substr($data->short_description , 0, 240).'...' : $data->short_description; ?></p>
											<span class="EventDetail">
												<a href="<?= url('sponsor-fend/sponsor-details',['id'=>$data->id]) ?>">Read More...</a>
											</span>
										</div>
									</div>
								</div>
							</div>
							<?php
						}
					}
					?>
					<div class="col-md-12 text-center">
						{{ $results->links() }}
					</div>
				</div>

			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>
@endsection
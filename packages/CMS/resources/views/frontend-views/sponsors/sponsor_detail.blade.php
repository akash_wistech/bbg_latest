<?php  
$title = 'PARTNERS AND SPONSORS';
// dd($results);
?>

@extends('frontend.app')
@section('content')
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<?php
					if (isset($data) && $data <> null) {
						?>
						<div class="Heading">
							<h3><?= $title; ?></h3>
							<hr/>
						</div>
						<div class="EventDetail">
							<div class="row">
								<?php
								if(!empty($data->image)) {
									?>
									<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
										<img src="<?= $data->image; ?>" class="img-fluid"
										alt="<?= $data->title; ?>">
									</div>
									<?php
								}
								?>
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="DescriptionHeading text-justify">
										<h3>&nbsp;</h3>
										<h4><?= $data->short_description; ?></h4>
									</div>
									<div class="DescriptionHeading  text-justify">
										<?= $data->description; ?>
									</div>

								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">

			</div>
		</div>
	</div>
</section>
@endsection
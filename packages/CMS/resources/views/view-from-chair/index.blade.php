<?php
$btnsList = [];
if(checkActionAllowed('create','view-from-chair')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'view-from-chair/create', 'method'=>'post'];
}

$dtColsArr = [];

$advSearchCols=[];

$dtColsArr[]=['title'=>__('managecontent::vfc.image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::vfc.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::vfc.title'),'data'=>'title','name'=>'title','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::vfc.date'),'data'=>'date','name'=>'date','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::vfc.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false];
// $dtColsArr[]=['title'=>__('managecontent::vfc.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::vfc.sort_order'),'data'=>'sort_order','name'=>'sort_order','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('managecontent::vfc.display_on_home'),'data'=>'display_on_home','name'=>'display_on_home','orderable'=>false,'width'=>''];
$dtColsArr[]=['title'=>__('managecontent::vfc.status'),'data'=>'status','name'=>'status','orderable'=>false];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::vfc.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::vfc.heading')
]])
@endsection

<?php //dd("here"); ?>

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'view-from-chair'" :cbSelection="false" :exportMenu="true" :jsonUrl="'view-from-chair/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection

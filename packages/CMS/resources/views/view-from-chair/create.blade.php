@extends('layouts.app')

@section('title', __('managecontent::vfc.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'view-from-chair'=> __('managecontent::vfc.heading'),
__('common.create')
]])
@endsection

@section('content')
@include('view-from-chair::._form')
@endsection



@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table',
		image_advtab: true ,
	});

</script>

@endpush



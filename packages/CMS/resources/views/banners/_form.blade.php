{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif

    <div class="row">
      <div class="col-6">
        <div class="form-group"> 
          {{ Form::label('name',__('managecontent::banners.name')) }}
          {{ Form::text('name', $model->name, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-6">
        <div class="form-group"> 
          {{ Form::label('slug',__('managecontent::banners.slug')) }}
          {{ Form::text('slug', $model->slug, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-6">
        <div class="form-group"> 
          {{ Form::label('type',__('managecontent::banners.type')) }}
          {{ Form::select('type', [''=>__('common.select')] + getBannerTypeArr(), $model->type, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-6">
        <div class="form-group">
          {{ Form::label('status',__('managecontent::banners.status')) }}
          {{ Form::select('status', [''=>__('common.select')] + getStatusArr(), $model->status, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>



    <div class="row">
      <div class="col-12">
        <table class="table table-light table-bordered table-sm" id="banners">
          <thead class="thead-light">
            <tr>
              <th class="text-center">Image</th>
              <th class="">Title</th>
              <th class="">Short Description</th>
              <th class="">Href(Link)</th>
              <th class="">Sort Order</th>
              <th class="text-center">
                <a href="javascript:;" class="add-button btn btn-icon btn-success btn-xs ml-2" data-toggle="tooltip" title="" aria-haspopup="true" aria-expanded="false" data-original-title="Add">
                  <i class="flaticon2-plus"></i>
                </a>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $image_row = 0; 
            if ($model->id!=null) {
              $results = getBannerRows($model->id);
              if ($results!=null) {
                foreach ($results as $value) {
      // dd($value);
                  ?>
                  <tr class="append-area">
                    <input class="form-control d-none " name="existingId[<?= $image_row ?>]" type="text" value="<?= $value['id'] ?>">
                    <td>
                      <div id="images" class="form-group pb-0 mb-2 pt-3 mx-1 text-center">
                        <a href="javascript:;" data-thumbid="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail" data-content="" width="20">
                          <img src="<?= $value['image']!=''? $value['image'] : asset('assets/images/dummy-image.jpg') ?>" width="60"
                          data-placeholder="{{asset('assets/images/dummy-image.jpg')}}">
                        </a>
                        <input value="<?= $value['image'] ?>" data-targetid="input-image<?php echo $image_row; ?>" class="form-control d-none" name="banners[<?php echo $image_row; ?>][image]" type="text">
                      </div>
                    </td>
                    <td>
                      <div class="form-group pt-6">
                        <input class="form-control" name="banners[<?php echo $image_row; ?>][title]" type="text" value="<?= $value['title']  ?>"> 
                      </div>
                    </td>
                    <td>
                      <div class="form-group pt-6">
                        <input class="form-control description" name="banners[<?php echo $image_row; ?>][description]" type="text" value="<?= $value['description'] ?>"> 
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group pt-6">
                      <input class="form-control" name="banners[<?php echo $image_row; ?>][link]" type="text" value="<?= $value['link']  ?>"> 
                    </div>
                  </td>
                  <td>
                    <div class="form-group pt-6">
                      <input class="form-control" name="banners[<?php echo $image_row; ?>][sort_order]" type="number"value="<?= $value['sort_order']  ?>"> 
                    </div>
                  </td>
                  <td class="text-center pt-9">
                    <a href="javascript:;" class="remove-row btn btn-icon btn-danger btn-xs ml-2" data-toggle="tooltip" aria-haspopup="true" aria-expanded="false"><i class="flaticon2-cross"></i></a>
                  </td>
                </tr>

                <?php
                $image_row++;
              }
            }
          }
          ?>

        </tbody>
      </table>
    </div>
  </div>



</div>

<div class="card-footer">
  <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
  <a href="{{ url('/banners') }}" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
</div>
{!! Form::close() !!}

@push('js')

<!-- <script>
  $('.edit-desc').click(function(){
    var description = $(this).parents('.append-area').find('.description').val();
    console.log(description);
    data = {description:description};
    $.ajax({
      url: 'banners/popdescmodal',
      type: 'post',
      data: {
        "_token": "{{ csrf_token() }}",
        "data": data,
      },
      success: function(html) {
        $('body').append('<div class="modal fade" id="descriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeLg" aria-hidden="true">' + html + '</div>');
        $('#descriptionModal').modal('show');
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
</script> -->

<script>
  var image_row = <?= $image_row ?>;

  $('body').on('click', '.add-button', function(e) {
    addImage();
  });

  function addImage() {
    html  = '<tr class="append-area">';
    html += '<td><div id="images" class="form-group pb-0 mb-2 pt-3 text-center"><a href="javascript:;" data-thumbid="thumb-image'+image_row+'" data-toggle="image" class="img-thumbnail" data-content="" width="20"><img src="{{asset("assets/images/dummy-image.jpg")}}" alt="" width="75" title="" data-placeholder="http://bbg-laravel-v1.local/assets/images/dummy-image.jpg"></a><input maxlength="" data-targetid="input-image'+image_row+'" class="form-control d-none" name="banners['+image_row+'][image]" type="text"></div></td>';
    html += '<td><div class="form-group pt-9"><input class="form-control" name="banners[' + image_row + '][title]" type="text"> </div></td>';
    html += '<td><div class="form-group pt-9"><input class="form-control" name="banners[' + image_row + '][description]" type="text"> </div></td>';
    html += '<td><div class="form-group pt-9"><input class="form-control" name="banners[' + image_row + '][link]" type="text"> </div></td>';
    html += '<td><div class="form-group pt-9"><input class="form-control" name="banners[' + image_row + '][sort_order]" type="number"> </div></td>';
    html += '<td class="text-center pt-12"><a href="javascript:;"  class="remove-row btn btn-icon btn-danger btn-xs ml-2" data-toggle="tooltip" aria-haspopup="true" aria-expanded="false"><i class="fa flaticon2-cross"></i></a></td>';
    html += '</tr>';
    $('#banners tbody').append(html);
    image_row++;
  }
</script>

<!-- start delete code -->
<script>
  $('body').on('click', '.remove-row', function(e) {
    _this = $(this);
    swal({
      title: "{{__('common.confirmation')}}",
      html: "You wont be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#47a447",
      confirmButtonText: "{{__('common.confirm')}}",
      cancelButtonText: "{{__('common.cancel')}}",
    }).then((result) => {
      if (result.value) {
        _this.parents('.append-area').remove();
      }else{
        return false;
      }
    });
  });
</script>
<!-- end delete code -->

@endpush











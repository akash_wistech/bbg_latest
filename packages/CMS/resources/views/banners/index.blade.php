<?php
// dd("hello");
$btnsList = [];
if(checkActionAllowed('create','banners')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'banners/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];

$dtColsArr[]=['title'=>__('managecontent::banners.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::banners.name'),'data'=>'name','name'=>'name','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::banners.slug'),'data'=>'slug','name'=>'slug','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::banners.type'),'data'=>'type','name'=>'type','orderable'=>false];

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('managecontent::banners.status'),'data'=>'status','name'=>'status','orderable'=>false];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::banners.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::banners.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'banners'" :cbSelection="false" :exportMenu="true" :jsonUrl="'banners/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection


@push('js')
@endpush
@extends('layouts.app')
@section('title', __('managecontent::banners.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'banners'=> __('managecontent::banners.heading'),
__('common.create')
]])
@endsection

@section('content')
@include('banners::._form')
@endsection

@push('css')
@endpush










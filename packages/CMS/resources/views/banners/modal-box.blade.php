@extends('layouts.modal')
@section('content')
<?php //dd($description); ?>
<!--begin::Modal-->
<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Modal Title</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i aria-hidden="true" class="ki ki-close"></i>
			</button>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-12">
					<div class="form-group"> 
						{{ Form::label('description',__('Description')) }}
						<input class="form-control editor" name="description" value="{!! $description !!}">

					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary font-weight-bold">Save changes</button>
		</div>
	</div>
</div>
<!--end::Modal-->
@endsection

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table',
		image_advtab: true ,
	});

</script>

@endpush



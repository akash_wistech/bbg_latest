@extends('layouts.app')

@section('title', __('news_lang::news-lang.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'news'=>__('news_lang::news-lang.heading'),
__('common.view')
]])
@endsection


@section('content')
<div class="card card-custom pb-0" data-card="true">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">Information</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','banners'))
			{
				?>
				<a href="{{url('banners/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			} 
			?> 
		</div>
	</div>
	<div class="card-body">
		<div class="row" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::banners.created')}}:</strong>{{formatDateTime($model->created_at)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::banners.slug')}}:</strong>{{$model->slug}}
				</div>
			</div>


			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::banners.name')}}: </strong>{{$model->name}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::banners.status')}}: </strong>{{getStatusArr()[$model->status]}}
				</div>
			</div>

			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::banners.type')}}:</strong>{{getBannerTypeArr()[$model->type]}}
				</div>
			</div>
		</div>


		<div class="row my-4">
			<div class="col-12">
				<table class="table table-light table-bordered table-sm" id="banners">
					<thead class="thead-light">
						<tr>
							<th class="text-center">Image</th>
							<th class="">Title</th>
							<th class="">Short Description</th>
							<th class="">Href(Link)</th>
							<th class="">Sort Order</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$results = getBannerRows($model->id);
						if ($results!=null) {
							foreach ($results as $value) {
								?>
								<tr class="append-area">
									<td>
										{!! getImageTag($value['image']) !!}
									</td>
									<td>
										<div class="form-group pt-4">
											<input class="form-control" value="<?= $value['title']  ?>" readonly> 
										</div>
									</td>
									<td>
										<div class="form-group pt-4">
											<input class="form-control description" value="<?= $value['description'] ?>" readonly> 
										</div>
									</div>
								</td>
								<td>
									<div class="form-group pt-4">
										<input class="form-control" value="<?= $value['link']  ?>" readonly> 
									</div>
								</td>
								<td>
									<div class="form-group pt-4">
										<input class="form-control" value="<?= $value['sort_order']  ?>" readonly> 
									</div>
								</td>
							</tr>
							<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection
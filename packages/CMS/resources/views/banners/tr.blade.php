<tr>
  <td>
    <div class="form-group">
      <a href="" data-thumbid="thumb-image-image" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
        <img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
        alt="" width="80"  title=""
        data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
      </a>
      {{ Form::text('image', $model->image, ['maxlength' => true, 'data-targetid' => 'input-image-image','class'=>'form-control d-none']) }}
    </div>
  </td>
  <td><div class="form-group"> {{ Form::text('banners[title][]', $model->title, ['class'=>'form-control']) }} </div></td>
  <td><div class="form-group"> {{ Form::text('banners[description][]', $model->description, ['class'=>'form-control']) }}</div></td>
  <td><div class="form-group"> {{ Form::text('banners[link][]', $model->link, ['class'=>'form-control']) }}</td>
    <td><div class="form-group"> {{ Form::number('banners[sort_order][]', $model->sort_order, ['class'=>'form-control']) }}</div></td>
    <td>
      <a onclick="addImage();" href="javascript:;" class="btn btn-icon btn-success btn-xs ml-2" data-toggle="tooltip" title="" aria-haspopup="true" aria-expanded="false" data-original-title="Add">
        <i class="fa fa-plus"></i>
      </a>
    </td>
  </tr>
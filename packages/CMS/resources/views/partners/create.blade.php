@extends('layouts.app')
@section('title', __('managecontent::partners.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'partners'=> __('managecontent::partners.heading'),
__('common.create')
]])
@endsection

@section('content')
@include('partners::._form')
@endsection

@push('css')
<style>
</style>
@endpush

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table autoresize',
		image_advtab: true,
		max_height: 200,
	});
</script>
@endpush










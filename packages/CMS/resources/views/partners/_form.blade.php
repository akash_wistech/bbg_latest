{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif


    <div class="row px-0">
      <div class="col-md-4 col-lg-10 px-0">
        <div class="col-12">
         <div class="form-group"> 
          {{ Form::label('name',__('managecontent::partners.name')) }}
          {{ Form::text('name', $model->name, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12">
        <div class="form-group"> 
          {{ Form::label('description',__('managecontent::partners.description')) }}
          {{ Form::textarea('description', $model->description, ['class'=>'form-control editor']) }}
        </div>
      </div>
    </div> 


    <!-- image here -->
    <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-2 pl-0 parent-div">
      <div id="images" class="form-group pb-0 mb-2">
        <a href="" data-thumbid="thumb-image" data-toggle="image" class="img-thumbnail shadow" data-content=""  width="20">
          <img src="{{ $model->logo != '' && $model->logo <> null ? $model->logo : asset('assets/images/dummy-image.jpg') }}"
          alt="" width="145" height="125" title=""
          data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
        </a>
        {{ Form::text('logo', $model->logo, ['maxlength' => true, 'data-targetid' => 'input-image','class'=>'form-control d-none']) }}
      </div>
      <span class="label label-xl label-muted font-weight-bold label-inline mx-16">{{__('managecontent::partners.logo')}}</span>
    </div>
    <!-- image end -->
  </div>


</div>




<div class="card-footer">
  <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
  <a href="{{ url('/partners') }}" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
</div>
{!! Form::close() !!}

@push('js')

@endpush











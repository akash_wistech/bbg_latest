@extends('layouts.app')

@section('title', __('managecontent::partners.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'partners'=>__('managecontent::partners.heading'),
__('common.view')
]])
@endsection


@section('content')
<div class="card card-custom pb-0" data-card="true">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">Information</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','partners'))
			{
				?>
				<a href="{{url('partners/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			} 
			?> 
		</div>
	</div>
	<div class="card-body">
		<div class="row" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::partners.created')}}:</strong>{{formatDateTime($model->created_at)}}
				</div>
			</div>


			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::partners.name')}}:</strong>{{$model->name}}
				</div>
			</div>

			<div class="col-4">

			</div>
		</div>


		<div class="row my-4" style="font-size: 14px; text-align: justify; line-height: 1.6; ">
			<div class="col px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::partners.description')}}: </strong>{!! $model->description !!}
				</div>
			</div>
		</div>

		<div class="row py-2" style="font-size: 14px;">
			<div class="col-6 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::partners.logo')}}: </strong>{!!getImageTag($model->logo)!!}
				</div>
			</div>
		</div>


	</div>

</div>
</div>
@endsection
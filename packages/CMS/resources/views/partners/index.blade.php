<?php
// dd("hello");
$btnsList = [];
if(checkActionAllowed('create','partners')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'partners/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];

$dtColsArr[]=['title'=>__('managecontent::partners.logo'),'data'=>'logo','name'=>'logo','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::partners.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::partners.name'),'data'=>'name','name'=>'name','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::partners.description'),'data'=>'description','name'=>'description','orderable'=>false];

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::partners.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::partners.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'partners'" :cbSelection="false" :exportMenu="true" :jsonUrl="'partners/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection


@push('js')
@endpush
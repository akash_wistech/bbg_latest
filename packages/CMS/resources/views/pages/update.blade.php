@extends('layouts.app')
@section('title', __('namod-cms::cms.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'workflow'=>__('namod-cms::cms.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('namod-cms::pages._form')
@endsection

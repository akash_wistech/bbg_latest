@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/pages/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('namod-cms::cms.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('namod-cms::cms.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <div class="overlay-wrapper">
      @if($results->isNotEmpty())
      <table class="table">
        <thead>
          <tr>
            <th width="50">#</th>
            <th>{{__('common.title')}}</th>
            <th width="100">{{__('common.status')}}</th>
            <th width="130">{{__('common.action')}}</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1; ?>
          @foreach($results as $result)
          @php
          if($result->status==1){
            $confrmMsg=__('common.confirmDisable');
          }else{
            $confrmMsg=__('common.confirmEnable');
          }
          @endphp
          <tr>
            <td>{{$i}}</td>
            <td>{{$result->lang->title}}</td>
            <td>{!!getEnableDisableIconWithLink($result->status,'pages/status/'.$result->id,$confrmMsg,'grid-table')!!}</td>
            <td>
              <a href="{{ url('pages/edit/'.$result->id)}}" data-toggle="tooltip" class='btn btn-sm btn-clean btn-icon edit' title='{{__('common.update')}}' data-id="{{$result->id}}">
                <i class="text-dark-50 flaticon-edit"></i>
              </a>
              <a href="{{ url('pages/delete/'.$result->id)}}" data-toggle="tooltip" class='btn btn-sm btn-clean btn-icon act-confirmation' title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
              </a>
            </td>
          </tr>
          <?php $i++; ?>
          @endforeach
        </tbody>
      </table>
      @else
      <div class="text-center">{{ __('common.noresultfound') }}</div>
      @endif
    </div>
  </div>
</div>
@endsection

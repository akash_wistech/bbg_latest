@php
$sysLanguages=getSystemLanguages();
$n=1;
@endphp
{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom tabs-card mb-2">
  <div class="card-body">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      @foreach($sysLanguages as $key=>$val)
      <li class="nav-item">
        <a class="nav-link{{$n==1 ? ' active' : ''}}" data-toggle="tab" href="#tab{{$key}}">
          <!--span class="nav-icon">
            <i class="flaticon2-gear"></i>
          </span-->
          <span class="nav-text">{{$val}}</span>
        </a>
      </li>
      @php
      $n++;
      @endphp
      @endforeach
    </ul>
    <div class="tab-content border border-top-0">
      @php
      $n=1;
      @endphp
      @foreach($sysLanguages as $key=>$val)
      <div id="tab{{$key}}" class="tab-pane fade{{$n==1 ? ' active show' : ''}} card card-custom">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('title['.$key.']', __('common.title_in',['lang'=>$val]) . '*') !!}
                {!! Form::text('title['.$key.']', null, ['class' => 'form-control', 'placeholder' => trans('common.title_hint_in',['lang'=>$val]), 'required']) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('descp['.$key.']', __('common.descp_in',['lang'=>$val])) !!}
                <div class="tinymce">
                  {!! Form::textarea('descp['.$key.']', null, ['class' => 'form-control textEditor tox-target', 'rows'=>4, 'placeholder' => trans('common.descp_hint_in',['lang'=>$val])]) !!}
                </div>
                <div class="help-block with-errors"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                {!! Form::label('meta_title['.$key.']', __('common.meta_title_in',['lang'=>$val])) !!}
                {!! Form::text('meta_title['.$key.']', null, ['class' => 'form-control', 'placeholder' => trans('common.meta_title_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('meta_descp['.$key.']', __('common.meta_descp_in',['lang'=>$val])) !!}
                {!! Form::textarea('meta_descp['.$key.']', null, ['class' => 'form-control', 'rows'=>4, 'placeholder' => trans('common.meta_descp_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('meta_keyword['.$key.']', __('common.meta_keyword_in',['lang'=>$val])) !!}
                {!! Form::textarea('meta_keyword['.$key.']', null, ['class' => 'form-control', 'rows'=>4, 'placeholder' => trans('common.meta_keyword_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
          </div>
          @if($n==1)
          <div class="form-group">
            {!! Form::label('status', __('common.status')) !!}
            {!! Form::select('status', getStatusArr(), null, ['class' => 'form-control', 'rows'=>4]) !!}
            <div class="help-block with-errors"></div>
          </div>
          @endif
        </div>
      </div>
      @php
      $n++;
      @endphp
      @endforeach
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/pages') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}
@push('js')
<script src="assets/plugins/custom/tinymce/tinymce.bundle.js"></script>
@endpush
@push('jsScripts')
tinymce.init({
    selector: '.textEditor',
    menubar: false,
    toolbar: ['styleselect fontselect fontsizeselect | link image',
        'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code',
        'advlist | autolink | lists'],
    plugins : 'advlist autolink link image lists charmap code'
});
@endpush

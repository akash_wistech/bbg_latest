@extends('frontend.app')
@section('content')


<?php if ($model<>null) {?>
	<section class="MainArea">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
						<div class="Heading">
							<h3><?= $model->title;?></h3>
						</div>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<div class="SiteText">
									<?= $model->descp; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
					@include('frontend.blocks.RightSideBarWidget')
				</div>
			</div>
		</div>
	</section>
<?php } ?>
@endsection
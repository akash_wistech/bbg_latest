@extends('frontend.app')
@section('content')

<?php  
$no_image = asset('assets/images/dummy-image-1.jpg');
if ($model<>null) {?>
	@include('frontend._banner',['model'=>$model,'no_image'=>$no_image])
	<?php
}
?>





<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="membershipcontent">
					<div class="Heading">
						<h3>Member Offers</h3>
					</div>
				</div>
			</div>
			<?php
			if ($results <> null && count($results)>0) {
				foreach ($results as $data){

					?>
					<div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 PaddingTopBtm30px">
						<div class="offer-bg1 offer text-center">
							<img src="<?= $data->image;?>" class="img-fluid" alt="">
							<h2 style="margin-bottom: 10%;"><?= $data->title; ?></h2>
							<span class="EventDetail">
								<a href="<?= url('membership/offers/detail',['id'=>$data->id]) ?>">View Detail</a>
							</span>
						</div>
					</div>
				<?php } }else{ ?>
					<div class="col-md-12">
						<div class="col-md-12 alert alert-danger">No Record Found</div>
					</div>
				<?php }?>
			</div>
		</div>
	</section>

	@endsection
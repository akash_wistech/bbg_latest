@extends('frontend.app')
@section('content')
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <?php
                    if (isset($result) && $result <> null) {
                        ?>

                        <div class="Heading">
                            <h3>Member Offer</h3>
                            <hr/>
                            <h4><?= $result->title; ?></h4>

                        </div>
                        <div class="EventDetail">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <img src="<?= (empty($result->image)) ? Yii::$app->params['no_image'] : $result->image; ?>" class="img-fluid" alt="<?= $result->title; ?>">
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="DescriptionHeading">
                                        <br/> <br/> <br/>
                                        <h4><?= $result->short_description; ?></h4>
                                        <?= $result->description; ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <!-- right sidebar weiget -->
            </div>
        </div>
    </div>
</section>
@endsection
<?php
// dd("hello");
$btnsList = [];
if(checkActionAllowed('create','member-offers')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'member-offers/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.title'),'data'=>'title','name'=>'title','orderable'=>false,'width'=>'150'];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.category_id'),'data'=>'category_id','name'=>'category_id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.offer_rel'),'data'=>'offer_rel','name'=>'offer_rel','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.offer_rel_id'),'data'=>'offer_rel_id','name'=>'offer_rel_id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.sort_order'),'data'=>'sort_order','name'=>'sort_order','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.is_active'),'data'=>'is_active','name'=>'is_active','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::memberoffers.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::memberoffers.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'member-offers'" :cbSelection="false" :exportMenu="true" :jsonUrl="'member-offers/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection


@push('js')
@endpush
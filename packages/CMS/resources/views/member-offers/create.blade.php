@extends('layouts.app')
@section('title', __('managecontent::memberoffers.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'member-offers'=> __('managecontent::memberoffers.heading'),
__('common.create')
]])
@endsection

@section('content')
@include('member-offers::._form')
@endsection

@push('css')
@endpush

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table',
		image_advtab: true ,
	});
</script>

<script>
	var rel_id = "{{ $model->offer_rel_id }}";
	console.log(rel_id);

	if (rel_id>0) {
		AB(rel_id);
	}
	
	$('body').on('change','#relatedType', function(){
		// console.log($(this).val());
		if ($(this).val()== 'general') {
			// console.log("helo general");
			$('#target-rel-id').html('');
			$('#target-rel-id').html('<option value="">Select</option>');
		}else{
			// console.log("hello world funxt run");
			AB();
		}
	});


	function AB(rel_id=null) {
		value = $("#relatedType").val();
		data = {data_key:value, rel_id:rel_id};

		$.ajax({
			url:"<?= url('member-offers/getRelToMemCom')?>",
			type:"POST",
			data: {
				"_token": "<?= csrf_token() ?>",
				data: data,
			},
			success:function (data) {
				$('#target-rel-id').html(data);
			}
		});
	}



</script>
@endpush


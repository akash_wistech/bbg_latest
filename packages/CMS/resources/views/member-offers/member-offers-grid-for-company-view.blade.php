<?php
// dd("hello");
$btnsList = [];
if(checkActionAllowed('create','member-offers')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'member-offers/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.title'),'data'=>'title','name'=>'title','orderable'=>false,'width'=>'150'];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.category_id'),'data'=>'category_id','name'=>'category_id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.offer_rel'),'data'=>'offer_rel','name'=>'offer_rel','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.offer_rel_id'),'data'=>'offer_rel_id','name'=>'offer_rel_id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.sort_order'),'data'=>'sort_order','name'=>'sort_order','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::memberoffers.is_active'),'data'=>'is_active','name'=>'is_active','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

$moduleTypeId='';
?>


<div id="grid-table-offers">
	<x-data-table-grid :sid="'offers'" :request="$request" :controller="'member-offers'" :cbSelection="false" :exportMenu="true" :jsonUrl="'member-offers/data-table-data?company_id='.$model->id" :dtColsArr="$dtColsArr" :advSearch="false" :moduleTypeId="$moduleTypeId"/>
</div>


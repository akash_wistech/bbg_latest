@extends('layouts.app')

@section('title', __('managecontent::memberoffers.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'news'=>__('managecontent::memberoffers.heading'),
__('common.view')
]])
@endsection


@section('content')
<div class="card card-custom pb-0" data-card="true">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">Information</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','member-offers'))
			{
				?>
				<a href="{{url('member-offers/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			} 
			?> 
		</div>
	</div>
	<div class="card-body">
		<div class="row" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.created')}}:</strong>{{formatDateTime($model->created_at)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.offer_rel_id')}}:</strong>{{ $model->type }}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.is_active')}}:</strong>{{getStatusArr()[$model->is_active]}}
				</div>
			</div>


			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.title')}}:</strong>{{$model->title}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.offer_rel')}}: </strong>{{ $model->offer_rel }}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.short_description')}}: </strong>{!! $model->short_description !!}
				</div>
			</div>

			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.category_id')}}: </strong>{{getCatParentTitle($model->category_id)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.sort_order')}}: </strong>{{$model->sort_order}}
				</div>
			</div>
		</div>


		<div class="row" style="font-size: 14px; text-align: justify; line-height: 1.6; ">
			<div class="col px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.description')}}: </strong>{!! $model->description !!}
				</div>
			</div>
		</div>

		<div class="row py-2" style="font-size: 14px;">
			<div class="col-6 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('managecontent::memberoffers.image')}}: </strong>{!!getImageTag($model->image)!!}
				</div>
			</div>
		</div>


	</div>

</div>
</div>
@endsection
@extends('layouts.app')
@section('title', __('namod-cms::menu.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'workflow'=>__('namod-cms::menu.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('namod-cms::menus._form')
@endsection

@php
$pagesList = getPagesListArr();
@endphp
<style>
.dd-item .dd-collapse,.dd-item .dd-expand{display: none;}
</style>
{!! Form::model($model, ['files' => false]) !!}
<div class="card card-custom multi-tabs">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group{{$model->id!=null ? '' : ' mb-0'}}">
          {{ Form::label('title',__('common.title')) }}
          {{ Form::text('title', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {!! Form::label('status', __('common.status')) !!}
          {!! Form::select('status', getStatusArr(), null, ['class' => 'form-control', 'rows'=>4]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
    @if($model->id!=null)
    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="card card-custom card-border mb-3 card-collapse" data-card="true">
          <div class="card-header">
            <div class="card-title">
              <span class="card-icon">
                <i class="flaticon2-file text-primary"></i>
              </span>
              <h3 class="card-label">{{__('namod-cms::cms.heading')}}</h3>
            </div>
            <div class="card-toolbar">
              <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                <i class="ki ki-arrow-down icon-nm"></i>
              </a>
            </div>
          </div>
          <div class="card-body" style="display:none;">
            @if($pagesList!=null)
            <div class="form-group">
              <div class="checkbox-list">
                @foreach($pagesList as $page)
                <label class="checkbox">
                  <input type="checkbox" class="pcbs" name="page_ids[{{$page->id}}]" value="{{$page->id}}" data-id="{{$page->id}}" data-title="{{$page->lang->title}}">
                  <span></span>{{$page->lang->title}}
                </label>
                @endforeach
              </div>
            </div>
            @endif
            <div class="card-footer p-0 pb-0 pt-3 text-right">
              <button type="button" class="btn btn-sm btn-info mr-2" onclick="addPageItem();"><i class="flaticon2-plus icon-xs"></i> {{ __('namod-cms::menu.add_to_menu') }}</button>
            </div>
          </div>
        </div>
        <div class="card card-custom card-border mb-3 card-collapse" data-card="true">
          <div class="card-header">
            <div class="card-title">
              <span class="card-icon">
                <i class="flaticon2-file text-primary"></i>
              </span>
              <h3 class="card-label">{{__('namod-cms::menu.link_heading')}}</h3>
            </div>
            <div class="card-toolbar">
              <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                <i class="ki ki-arrow-down icon-nm"></i>
              </a>
            </div>
          </div>
          <div class="card-body" style="display:none;">
            <div class="form-group">
              <label for="link_title">{{__('namod-cms::menu.link_title')}}</label>
              <input class="form-control" placeholder="{{__('namod-cms::menu.link_title')}}" name="link_title" type="text" value="" id="link_title">
            </div>
            <div class="form-group">
              <label for="link_url">{{__('namod-cms::menu.url')}}</label>
              <input class="form-control" placeholder="{{__('namod-cms::menu.url_placeholder')}}" name="link_url" type="text" value="" id="link_url">
            </div>
            <div class="form-group">
              <label for="link_icon">{{__('namod-cms::menu.link_icon')}}</label>
              <input class="form-control" placeholder="{{__('namod-cms::menu.link_icon_placeholder')}}" name="link_icon" type="text" value="" id="link_icon">
            </div>
            <div class="form-group">
              <label for="link_css_class">{{__('namod-cms::menu.link_css_class')}}</label>
              <input class="form-control" placeholder="{{__('namod-cms::menu.link_css_class_placeholder')}}" name="link_css_class" type="text" value="" id="link_css_class">
            </div>
            <div class="form-group">
              <label for="link_target">{{__('namod-cms::menu.link_target')}}</label>
              <select name="link_target" class="form-control" id="link_target">
                @foreach(getUrlTargetOptions() as $key=>$val)
                <option value="{{$key}}">{{$val}}</option>
                @endforeach
              </select>
            </div>
            <div class="card-footer p-0 pb-0 pt-3 text-right">
              <button type="button" class="btn btn-sm btn-info mr-2" onclick="addLinkItem();"><i class="flaticon2-plus icon-xs"></i> {{ __('namod-cms::menu.add_to_menu') }}</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8">
        <div class="card card-custom card-border">
          <div class="card-header">
            <div class="card-title">
              <span class="card-icon">
                <i class="flaticon2-layers text-primary"></i>
              </span>
              <h3 class="card-label">{{__('namod-cms::menu.menu_structure')}}</h3>
            </div>
          </div>
          <div class="card-body">
            <div id="dd-mc" class="dd">
              @if($model->menuItems!=null)
              <ol id="ddddl" class="dd-list">
              @foreach($model->menuItems as $menuItem)
              {!!generateMenuItemHtml($menuItem)!!}
              @endforeach
              </ol>
              @endif
            </div>
          </div>
          <div class="card-footer">
            <h3 class="display-5">{{__('namod-cms::menu.display_location')}}</h3>
            <div class="form-group mt-4 mb-0">
              <div class="checkbox-list">
                @foreach(getSysMenuLocations() as $key=>$val)
                <label class="checkbox">
                  <input type="checkbox" class="hlcbs" name="location_ids[{{$key}}]" value="{{$key}}" data-id="{{$key}}">
                  <span></span>{{$val}}
                </label>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/menus') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}
@push('css')
<link href="{{asset('assets/plugins/custom/nestable/jquery.nestable.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/custom/nestable/menu.css')}}" rel="stylesheet">
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/nestable/jquery.nestable.min.js')}}"></script>
@endpush
@push('jsScripts')
initScripts();
$("body").on("click", ".dd-item .dd3-content a.show-item-details", function (event) {
  event.preventDefault();
  let parent = $(event.currentTarget).parent().parent();
  $(event.currentTarget).toggleClass('active');
  parent.toggleClass('active');
});

// Remove nodes
$("body").on("click", ".nestable-menu .item-details .btn-remove", function (event) {
  event.preventDefault();
  let current = $(event.currentTarget);
  let dd_item = current.parents('.item-details').parent();

  let $elm = $('.form-save-menu input[name="deleted_nodes"]');
  // Add id of deleted nodes to delete in controller
  $elm.val($elm.val() + ' ' + dd_item.attr('data-id'));
  let children = dd_item.find('> .dd-list').html();
  if (children !== '' && children != null) {
    dd_item.before(children.replace('<script>', '').replace('<\\/script>', ''));
  }
  dd_item.remove();
});
$("body").on("click", ".nestable-menu .item-details .btn-cancel", function (event) {
  event.preventDefault();
  let current_pa = $(event.currentTarget);
  let parent = current_pa.parents('.item-details').parent();
  parent.find('input[type="text"]').each((index, el) => {
    $(el).val($(el).attr('data-old'));
  });

  parent.find('select').each((index, el) => {
    $(el).val($(el).val());
  });

  parent.find('input[type="text"]').trigger('change');
  parent.find('select').trigger('change');
  parent.removeClass('active');
});
@endpush()

@push('jsFunc')
function addPageItem()
{
  var html=""
  $.each($("input[class=\"pcbs\"]:checked"), function(){
    generateHtml('p',$(this));
  });
}
function addLinkItem()
{
  var html=""
  generateHtml('l','');
  $("#link_title").val("");
  $("#link_url").val("");
  $("#link_icon").val("");
  $("#link_css_class").val("");
  $("#link_target").val("_self");
}

function generateHtml(type,el)
{
  html = "";
  var data;
  if(type=='p'){
    data = {mtype: 'page',menu_id: {{$model->id}},page_id: el.val()};
  }else{
    linkTitle=$("#link_title").val();
    linkUrl=$("#link_url").val();
    linkIcon=$("#link_icon").val();
    linkCssClass=$("#link_css_class").val();
    linkCssTarget=$("#link_target").val();
    if(linkTitle==""){
      toastr.error("{{__('namod-cms::menu.enter_title')}}", "{{__('common.error')}}");
      return false;
    }
    if(linkUrl==""){
      toastr.error("{{__('namod-cms::menu.enter_url')}}", "{{__('common.error')}}");
      return false;
    }
    data = {mtype: 'link',menu_id: {{$model->id}},title: linkTitle,url: linkUrl,icon: linkIcon,css_class: linkCssClass,target: linkCssTarget};
  }
  $.ajax({
    url: "{{url('menus/add-item')}}",
    type: 'POST',
    data: data,
    dataType: "html",
    success: function(response) {
      if($("#ddddl").length<=0){
        $("#dd-mc").html("<ol id=\"ddddl\"></ol>")
      }
      $("#ddddl").append(response);
      $(".dd-empty").remove();
      initScripts();
    },
  });
  return html;
}
function initScripts()
{
  $('.dd').nestable({
    callback: function(l,e){
      // l is the main container
      // e is the element that was moved
    }
  });
}
@endpush

@extends('layouts.app')

@section('title', __('managecontent::committemembers.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'committe-members'=> __('managecontent::committemembers.heading'),
__('common.view')
]])
@endsection

@section('content')

<section class="card card-custom with-activity">
	<header class="card-header heared-outline">
		<div class="card-title">
			<h3 class="card-label">{{$model->full_name}}</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','committe-members'))
			{
				?>
				<a href="{{url('committe-members/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			}	
			?>	
		</div>
	</header>
	<div class="card-body multi-cards multi-tabs">
		<div class="row">
			<div class="col-sm-12">
				<section class="card card-custom card-border mb-3" data-card="true">
					<header class="card-header heared-outline">
						<div class="card-title">
							<h3 class="card-label">Information</h3>
						</div>
						<div class="card-toolbar">
							<a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="Toggle">
								<i class="ki ki-arrow-down icon-nm"></i>
							</a>
						</div>
					</header>
					<div class="card-body" style="" kt-hidden-height="78">
						<div class="row">
							<div class="col-6">
								<div class="col-sm-12">
									<strong>Created:</strong> {{formatDateTime($model->created_at)}}
								</div>
								<div class="col-sm-12 my-2">
									<strong>Full Name: </strong> {{$model->full_name}}
								</div>
							</div>

							<div class="col-6">
								<div class="col-sm-12">
									<strong>Email:</strong> {{$model->email}}
								</div>
								<div class="col-sm-12 my-2">
									<strong>Phone Number: </strong> {{$model->phonenumber}}
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-6">
								<div class="col-sm-12">
									<strong>Designation:</strong> {{$model->designation}}
								</div>
								<div class="col-sm-12 my-2">
									<strong>Type: </strong> {{getPredefinedListTitle($model->type)}}
								</div>
							</div>

							<div class="col-6">
								<div class="col-sm-12">
									<strong>Sort Order:</strong> {{$model->sort_order}}
								</div>
								<div class="col-sm-12 my-2">
									<strong>Status: </strong> {{getCommitteMembersPublishArr()[$model->status]}}
								</div>
							</div>
						</div>

						<div class="row my-2">
							<div class="col-12">
								<div class="col-sm-12">
									<strong>Description</strong> {!! $model->description !!}
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>

@endsection

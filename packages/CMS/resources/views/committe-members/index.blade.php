<?php
$btnsList = [];
if(checkActionAllowed('create','committe-members')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'committe-members/create', 'method'=>'post'];
}

$dtColsArr = [];

$advSearchCols=[];

$dtColsArr[]=['title'=>__('managecontent::committemembers.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.full_name'),'data'=>'full_name','name'=>'full_name','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.designation'),'data'=>'designation','name'=>'designation','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.email'),'data'=>'email','name'=>'email','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.phonenumber'),'data'=>'phonenumber','name'=>'phonenumber','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.type'),'data'=>'type','name'=>'type','orderable'=>false];
$dtColsArr[]=['title'=>__('managecontent::committemembers.sort_order'),'data'=>'sort_order','name'=>'sort_order','orderable'=>false];
// $dtColsArr[]=['title'=>__('managecontent::committemembers.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by','orderable'=>false,];
$dtColsArr[]=['title'=>__('managecontent::committemembers.status'),'data'=>'status','name'=>'status','orderable'=>false,'width'=>90];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

$advSearchCols[]=['title'=>__('common.name'),'name'=>'full_name'];
?>

@extends('layouts.app',['btnsList'=>$btnsList])

@section('title', __('managecontent::committemembers.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('managecontent::committemembers.heading')
]])
@endsection
<?php //echo "string"; die(); ?>

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'committe-members'" :cbSelection="false" :exportMenu="true" :jsonUrl="'committe-members/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="true" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection

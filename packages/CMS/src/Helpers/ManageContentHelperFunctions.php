<?php
use NaeemAwan\CMS\Models\Categories;
use NaeemAwan\CMS\Models\BannerImages;
use App\Models\Company;
use App\Models\User;
use NaeemAwan\PredefinedLists\Models\PredefinedList;
use NaeemAwan\PredefinedLists\Models\PredefinedListDetail;
use NaeemAwan\PredefinedLists\Models\PredefinedCategoriesList;

if (!function_exists('getCommitteMembersPublishArr')) {
  /**
  * return status array
  */
  function getCommitteMembersPublishArr()
  {
  	return [
  		'0' => __('In-Active'),
  		'1' => __('Active'),
  	];
  }
}

if (!function_exists('getGalleryStatusArr')) {
  /**
  * return status array
  */
  function getGalleryStatusArr()
  {
    return [
      'In-Active' => __('In-Active'),
      'active' => __('Active'),
    ];
  }
}

if (!function_exists('getGalleryStatusLabelArr')) {
  /**
  * return status array
  */
  function getGalleryStatusLabelArr()
  {
    return [
      'In-Active' => '<span class="label label-inline label-light-danger font-weight-bold">In-Active</span>',
      'active' => '<span class="label label-inline label-light-success font-weight-bold">Active</span>',
    ];
  }
}

if (!function_exists('getGalleryPositionArr')) {
  /**
  * return status array
  */
  function getGalleryPositionArr()
  {
    return [
      'left' => __('Left'),
      'right' => __('Right'),
    ];
  }
}

if (!function_exists('getGallerySizeArr')) {
  /**
  * return Size array
  */
  function getGallerySizeArr()
  {
    return [
      'small' => __('Small'),
      'large' => __('Large'),
    ];
  }
}


if (!function_exists('getvfcPublishArr')) {
  /**
  * return status array
  */
  function getvfcPublishArr()
  {
    return [
      '0' => __('Un-Publish'),
      '1' => __('Publish'),
    ];
  }
}

if (!function_exists('getvfcPublishArrLable')) {
  /**
  * return status array
  */
  function getvfcPublishArrLable()
  {
    return [
      '0' => '<span class="label label-inline label-light-primary  font-weight-bold">Un-Publish</span>',
      '1' => '<span class="label label-inline label-light-success font-weight-bold">Published</span>',
    ];
  }
}

if (!function_exists('getShowHomePageLabelArr')) {
  /**
  * return status array
  */
  function getShowHomePageLabelArr()
  {
    return [
      '0' => '<span class="label label-inline label-light-primary  font-weight-bold">No</span>',
      '1' => '<span class="label label-inline label-light-success font-weight-bold">Yes</span>',
    ];
  }
}

if (!function_exists('getShowHomePageArr')) {
  /**
  * return status array
  */
  function getShowHomePageArr()
  {
    return [
      '0' => __('No'),
      '1' => __('Yes'),
    ];
  }
}


if (!function_exists('getSponsorTypeArr')) {
  /**
  * return status array
  */
  function getSponsorTypeArr()
  {
    return [
      'platinum' => __('Platinum'),
      'pearls' => __('Pearls'),
    ];
  }
}

if (!function_exists('getSponsorTypeLabelArr')) {
  /**
  * return status array
  */
  function getSponsorTypeLabelArr()
  {
    return [
      'platinum' => '<span class="label label-inline label-light-primary  font-weight-bold">Platinum</span>',
      'pearls' => '<span class="label label-inline label-light-info  font-weight-bold">Pearls</span>',
    ];
  }
}


if (!function_exists('getSponsorsstatusArr')) {
  /**
  * return status array
  */
  function getSponsorsstatusArr()
  {
    return [
      '0' => __('In-Active'),
      '1' => __('Active'),
    ];
  }
}


if (!function_exists('formatDateTime')) {
  /**
  * returns formated date time
  */
  function formatDateTime($datetime)
  {
    return date("j M, Y / h:i a",strtotime($datetime));
  }
}




if (!function_exists('getCatParentTitle')) {
  /**
  * return status confirm msg array
  */
  function getCatParentTitle($id)
  {
    if ($id!=null) {
      $result = PredefinedListDetail::select(['title'])->where(['list_id'=>$id])->first();
      return $result->title;
    }
    
  }
}

if (!function_exists('getCatForMemberOffersTitle')) {
  function getCatForMemberOffersTitle()
  {
    return Categories::select(['id','title'])
    ->where(['type'=>'offers'])
    ->get()
    ->pluck('title', 'id')
    ->toArray();
  }
}




if (!function_exists('getCategoriesTypeArr')) {
  /**
  * return status confirm msg array
  */
  function getCategoriesTypeArr()
  {
    return [
      'general' => 'General',
      'offers' => 'Offers',
    ];
    
  }
}


if (!function_exists('getCategoriesArr')) {
  /**
  * return status confirm msg array
  */
  function getCategoriesArr()
  {
    return Categories::select(['id','title'])
    ->get()
    ->pluck('title', 'id')
    ->toArray();
    
  }
}

if (!function_exists('getBannerTypeArr')) {
  function getBannerTypeArr()
  {
    return [
      'home' => 'Home', 'category' => 'Category', 'product' => 'Product', 'other' => 'Other',
    ];
  }
}

if (!function_exists('getOfferRelatedToArr')) {
  function getOfferRelatedToArr()
  {
    return [
      'general' => 'General', 'member' => 'Member', 'company' => 'Company'
    ];
  }
}

if (!function_exists('getBannerRows')) {
  function getBannerRows($id)
  {
    return BannerImages::where(['banner_id' => $id])->get()->toArray();
  }
}


if (!function_exists('getPredefinedListTitle')) {
  /**
  * returns formated date time
  */
  function getPredefinedListTitle($id)
  {
    if ($id!='') {
      $result = DB::table('predefined_list_detail')->select('title')->where('id', $id)->first();
      if ($result<>null) {
        return $result->title;
      }
    }else{
      return '';
    }
  }
}


if (!function_exists('getImageTag')) {
  /**
  * returns formated date time
  */
  function getImageTag($src)
  {
    if ($src!='') {
      return '<img src="'.$src.'" style="width:100px;">';
    }else{
      return '<img src="'.asset('assets/images/dummy-image-1.jpg').'" style="width:100px;">';
    }
  }
}


if (!function_exists('getRelToForMemOff')) {
  function getRelToForMemOff($rel_type,$id)
  {
    if ($rel_type!=null && $rel_type=='company') {
      if ($id!=null) {
        $result = Company::select(['title'])->where(['id'=>$id])->first();
        if ($result<>null) {
          return $result->title;
        }else{
          return false;
        }
      }
    }

    if ($rel_type!=null && $rel_type=='member') {
      if ($id!=null) {
        $result = User::select(['name'])->where(['id'=>$id])->first();
        if ($result<>null) {
          return $result->name;
        }else{
          return false;
        }
      }
    }
  }
}



if (!function_exists('getPredefinedListForCategoryArr')) {
  /**
  * All predefined for dropdown
  *
  * @return array
  */
  function getPredefinedListForCategoryArr($id) //PredefinedCategoriesList
  {
    return PredefinedList::select([(new PredefinedList)->getTable().'.id','title'])
    ->join((new PredefinedListDetail)->getTable(),(new PredefinedListDetail)->getTable().'.list_id','=',(new PredefinedList)->getTable().'.id')
    ->join((new PredefinedCategoriesList)->getTable(),(new PredefinedCategoriesList)->getTable().'.list_id','=',(new PredefinedList)->getTable().'.id')
    ->where(['parent_id' => $id, 'lang'=>App::currentLocale(), 'status' => 1])
    ->where(['type' => 'offers'])
    ->orderBy('title','asc')->get()->pluck('title','id')->toArray();
  }
}



if (!function_exists('getYears')) {
  function getYears()
  {
    $year = date('Y');
    $years[] = $year;
    for ($i = 0; $i < 30; $i++) {
      $year = $year - 1;
      $years[] = $year;
    }
    return $years;
  }
}
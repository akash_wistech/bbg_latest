<?php
// use App;
use NaeemAwan\CMS\Models\Pages;
//
if (!function_exists('getSysMenuLocations')) {
    /**
     * return list of system defined menu locations
     */
    function getSysMenuLocations()
    {
      return [
        'header-menu' => __('namod-cms::menu.header'),
        'main-menu' => __('namod-cms::menu.main_menu'),
        'footer-menu' => __('namod-cms::menu.footer'),
      ];
    }
}

if (!function_exists('getPagesListArr')) {
    /**
     * return list of pages
     */
    function getPagesListArr()
    {
      return Pages::get();
    }
}

if (!function_exists('getUrlTargetOptions')) {
    /**
     * return list of link targets
     */
    function getUrlTargetOptions()
    {
      return [
        '_self' => __('namod-cms::menu.open_link_directly'),
        '_blank' => __('namod-cms::menu.open_link_in_new_tab'),
      ];
    }
}

if (!function_exists('generateMenuItemHtml')) {
    /**
     * generate html for the menu item
     */
    function generateMenuItemHtml($menuItem)
    {
      $menuType='Page';
      if($menuItem->reference_type=='Link')$menuType='Custom Link';
      $html = '<li class="dd-item" data-id="'.$menuItem->id.'">';
      $html.= ' <div class="dd-handle dd3-handle"></div>';
      $html.= ' <div class="dd3-content">';
      $html.= '   <span class="text float-left" data-update="title">'.$menuItem->title.'</span>';
      $html.= '   <span class="text float-right">'.$menuType.'</span>';
      $html.= '   <a href="javascript:;" title="" class="show-item-details"><i class="fa fa-angle-down"></i></a>';
      $html.= '   <div class="clearfix"></div>';
      $html.= ' </div>';
      $html.= ' <div class="item-details">';
      $html.= '  <label class="pad-bot-5">';
      $html.= '    <span class="text pad-top-5 dis-inline-block" data-update="title">'.__('namod-cms::menu.link_title').'</span>';
      $html.= '    <input type="text" class="form-control" name="title['.$menuItem->id.']" value="'.$menuItem->title.'" data-old="'.$menuItem->title.'">';
      $html.= '  </label>';
      if($menuItem->reference_type=='Link'){
        $html.= '  <label class="pad-bot-5 dis-inline-block">';
        $html.= '    <span class="text pad-top-5" data-update="custom-url">URL</span>';
        $html.= '    <input type="text" class="form-control" name="url['.$menuItem->id.']" value="'.$menuItem->url.'" data-old="'.$menuItem->url.'">';
        $html.= '  </label>';
      }
      $html.= '  <label class="pad-bot-5 dis-inline-block">';
      $html.= '    <span class="text pad-top-5" data-update="icon-font">Icon</span>';
      $html.= '    <input type="text" class="form-control" name="icon_font['.$menuItem->id.']" value="'.$menuItem->icon_font.'" data-old="'.$menuItem->icon_font.'">';
      $html.= '  </label>';
      $html.= '  <label class="pad-bot-10">';
      $html.= '    <span class="text pad-top-5 dis-inline-block">CSS class</span>';
      $html.= '    <input type="text" class="form-control" name="css_class['.$menuItem->id.']" value="'.$menuItem->css_class.'" data-old="'.$menuItem->css_class.'">';
      $html.= '  </label>';
      $html.= '  <label class="pad-bot-10">';
      $html.= '    <span class="text pad-top-5 dis-inline-block">Target</span>';
      $html.= '    <div style="width: 228px; display: inline-block">';
      $html.= '      <select name="target['.$menuItem->id.']" class="form-control" id="target" data-old="'.$menuItem->target.'" aria-invalid="false">';
      foreach(getUrlTargetOptions() as $key=>$val){
        $html.= '        <option value="'.$key.'"'.($key==$menuItem->target ? ' selected="selected"' : '').'>'.$val.'</option>';
      }
      $html.= '      </select>';
      $html.= '    </div>';
      $html.= '  </label>';
      $html.= '  <div class="clearfix"></div>';
      $html.= '  <div class="text-right" style="margin-top: 5px">';
      $html.= '    <a href="javascript:;" class="btn btn-danger btn-remove btn-sm">Remove</a>';
      $html.= '    <a href="javascript:;" class="btn btn-primary btn-cancel btn-sm">Cancel</a>';
      $html.= '  </div>';
      $html.= ' </div>';
      $html.= '</li>';
      return $html;
    }
}

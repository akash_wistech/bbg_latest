<?php

namespace NaeemAwan\CMS\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use App\Traits\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menus extends Model
{
  public $location_ids;
  use Blameable, SoftDeletes, Sortable;

  protected $table = 'menus';

  protected $dates = ['created_at','updated_at','deleted_at'];

  protected $fillable = [
    'slug',
    'title',
    'status',
  ];

  public $sortable = ['id', 'slug', 'title', 'status'];

  /**
  * enabled condition
  */
  public function scopeEnabled($query)
  {
    return $query->where('status', 1);
  }

  /**
  * Enable / Disable the record
  * @return boolean
  */
  public function updateStatus()
  {
    $status=1;
    if($this->status==1)$status=0;

    DB::table($this->table)
    ->where('id', $this->id)
    ->update(['status' => $status]);

    return true;
  }

  /**
  * Get menu items.
  */
  public function menuItems()
  {
    return $this->hasMany(MenuItems::class,'menu_id');
  }

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }
}

<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommitteMembers extends Model
{
    use HasFactory;

    public $moduleTypeId = 'committe-members';
}

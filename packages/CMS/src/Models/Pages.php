<?php

namespace NaeemAwan\CMS\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use App\Traits\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
  public $title,$descp;
  public $meta_title,$meta_descp,$meta_keyword;

  use Blameable, SoftDeletes, Sortable;

  protected $table = 'content_pages';

  protected $dates = ['created_at','updated_at','deleted_at'];

  protected $fillable = [
    'slug',
    'title',
    'status',
    'parent_id',
    'descp',
    'meta_title',
    'meta_descp',
    'meta_keyword',
  ];

  public $sortable = ['id', 'title', 'descp', 'status'];

  /**
  * enabled condition
  */
  public function scopeEnabled($query)
  {
    return $query->where('status', 1);
  }

  /**
  * locale condition
  */
  public function scopeLocale($query,$lang='en')
  {
    $pdl = (new PagesDetail)->getTable();
    return $query->join($pdl,$pdl.'.page_id','=',$this->getTable().'.id')
    ->where('lang', $lang);
  }

  /**
  * Enable / Disable the record
  * @return boolean
  */
  public function updateStatus()
  {
    $status=1;
    if($this->status==1)$status=0;

    DB::table($this->table)
    ->where('id', $this->id)
    ->update(['status' => $status]);

    return true;
  }

  /**
  * Get language details.
  */
  public function langDetails()
  {
    return $this->hasMany(PagesDetail::class,'page_id');
  }

  /**
  * Get single language detail.
  */
  public function lang()
  {
    $locale = App::currentLocale();
    return $this->hasOne(PagesDetail::class,'page_id')->where('lang',$locale);
  }

  /**
  * Get single language detail.
  */
  public function forLang($locale)
  {
    return PagesDetail::where('page_id',$this->id)->where('lang',$locale)->first();
  }

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }
}

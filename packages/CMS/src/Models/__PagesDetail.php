<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class PagesDetail extends Model
{
  protected $table = 'content_pages_detail';

  public $timestamps = false;

  protected $fillable = [
    'page_id',
    'lang',
    'title',
    'descp',
    'meta_title',
    'meta_descp',
    'meta_keyword',
  ];

  /**
  * Get Main row.
  */
  public function mainRow()
  {
    return $this->belongsTo(Pages::class,'page_id');
  }

}

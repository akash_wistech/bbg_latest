<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberOffers extends Model
{
	use HasFactory;
	public $moduleTypeId = 'member-offers';
}

<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	use HasFactory;
	public $moduleTypeId = 'categories';
}

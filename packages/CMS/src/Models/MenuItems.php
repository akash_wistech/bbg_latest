<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItems extends Model
{
  use Blameable, SoftDeletes;
  protected $table = 'menu_items';

  protected $fillable = [
    'menu_id',
    'parent_id',
    'reference_type',
    'reference_id',
    'url',
    'icon_font',
    'position',
    'title',
    'css_class',
    'target',
    'has_child',
  ];

  /**
  * Get Menu.
  */
  public function menu()
  {
    return $this->belongsTo(Menu::class,'menu_id');
  }

  /**
  * Get Parent Menu.
  */
  public function parentMenu()
  {
    return $this->belongsTo(MenuItems::class,'parent_id');
  }

  /**
  * Get child items.
  */
  public function childItems()
  {
    return $this->hasMany(MenuItems::class,'parent_id');
  }

}

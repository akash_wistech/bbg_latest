<?php

namespace NaeemAwan\CMS\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;

class MenuLocations extends Model
{
  use Blameable;

  protected $table = 'menu_locations';

  protected $dates = ['created_at','updated_at'];

  protected $fillable = [
    'menu_id',
    'location',
  ];
}

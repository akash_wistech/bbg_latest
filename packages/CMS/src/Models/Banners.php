<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use NaeemAwan\CMS\Models\BannerImages;

class Banners extends Model
{
	use HasFactory;
	public $moduleTypeId = 'banners';
	public $banners,$existingId;

	public static function boot()
	{
		parent::boot();

		self::creating(function ($model) {
		});

		self::created(function ($model) {
			$model->saveBanners($model);
		});

		self::updating(function ($model) {
			$model->saveBanners($model);
		});

		self::updated(function ($model) {
		});

		self::deleting(function ($model) {
		});

		self::deleted(function ($model) {
		});
	}

	public function saveBanners($model)
	{
		// echo "<pre>";
		// print_r($model);
		// echo "</pre>";
		// dd("working");
		$xxx = 0;
		$savedIdz = [];
		if ($model->banners!=null) {
			foreach ($model->banners as $key => $value) {
				if (isset($model->existingId[$xxx]) && $model->existingId[$xxx] != null) {
					//print_r($model->existingId[$xxx]); echo "<br>";
					$child = BannerImages::find($model->existingId[$xxx]);
				} else {
					$child = new BannerImages;
					$child->banner_id = $model->id;
				}
				$child->title = $value['title'];
				$child->description = $value['description'];
				$child->image = $value['image'];
				$child->link = $value['link'];
				$child->sort_order = $value['sort_order'];
				$child->status = 1;
				$child->save();
				$savedIdz[$key] = $child->id;
				$xxx++;
			} 
			//dd("complete");
		}
		BannerImages::where('banner_id', $model->id)->whereNotIn('id', $savedIdz)->delete();

	}
}

<?php

namespace NaeemAwan\CMS\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewFromChair extends Model
{
	use HasFactory;
	public $moduleTypeId = 'view-from-chair';
}

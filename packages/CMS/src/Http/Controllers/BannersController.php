<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\Banners;
use NaeemAwan\CMS\Models\BannerImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class BannersController extends Controller
{
    public function index(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        return view('banners::.index',compact('moduleTypeId','request'));
    }

    public function datatableData(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        $query = DB::table($this->newModel()->getTable())
        ->whereNull('deleted_at');

        if($request->search['value']!=''){
            $keyword = $request->search['value'];

            $srchFlds[]='name';
            $srchFlds[]='slug';
            $srchFlds[]='type';
            $srchFlds[]='status';
            searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
        }

        $countQuery = clone $query;
        $totalRecords = $countQuery->count('id');
        $orderBy = 'id';
        if($request->ob)$orderBy = $request->ob;
        $orderByOrder = 'desc';
        if($request->obo)$orderByOrder = $request->obo;
        $models = $query->offset($request->start)
        ->limit($request->length)
        ->orderBy($orderBy,$orderByOrder)
        ->get();
        $dataArr=[];

        if($models!=null){
            foreach($models as $model){
                $thisData=[];
                $thisData['id']=$model->id;
                $thisData['name']=$model->name;
                $thisData['slug']=$model->slug;
                $thisData['type']=$model->type;
                $thisData['status']=getStatusIconArr()[$model->status];
                $thisData['cb_col']='';
                $thisData['created_at']=formatDateTime($model->created_at);
                $createdBy = getUserInfo($model->created_by);
                $thisData['created_by']=$createdBy->name;

                $actBtns=[];
                if(checkActionAllowed('view','banners')){
                    $actBtns[]='
                    <a href="'.url('banners/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-file-2"></i>
                    </a>';
                }
                if(checkActionAllowed('update','banners')){
                    $actBtns[]='
                    <a href="'.url('banners/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-edit"></i>
                    </a>';
                }
                if(checkActionAllowed('delete','banners')){
                    $actBtns[]='
                    <a href="'.url('banners/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                    </a>';
                }

                $thisData['action_col']=getDatatableActionTemplate($actBtns);
                $dataArr[]=$thisData;
            }
        }


        $dtInputColsArr = [];
        $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
        $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
        $inputArr = [
            'columns' => $dtInputColsArr,
            'draw' => (int)$request->draw,
            'length' => (int)$request->length,
            'order' => [
                ['column'=>0,'dir'=>'asc'],
            ],
            'search' => ['value'=>$request->search['value'],'regex'=>false],
            'start' => $request->start,
        ];

        $response = [
            'draw' => (int)$request->draw,
            'input' => $inputArr,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords,
            'data' => $dataArr,
        ];
        return new JsonResponse($response);
    }

    public function create(Request $request)
    {
        $model = new Banners;
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'name' => 'required|string',
                'slug' => 'required|string',
                'status' => 'required|integer',
            ]);
            $model = new Banners;
            $model->name=$request->name;
            $model->slug=$request->slug;
            $model->type=$request->type;
            $model->status=$request->status;
            $model->banners=$request->banners;
            $model->created_by = Auth::user()->id;
            if ($model->save()) {
                return redirect('banners')->with('success', __('managecontent::banners.saved'));
            }
        }
        return view('banners::create',['model'=>$model]);
    }

    public function view(Banners $banners, $id)
    {
        $model=Banners::where('id',$id)->first();
        return view('banners::.view',compact('model'));
    }

    public function update(Request $request, Banners $banners, $id)
    {
        $model=Banners::where('id',$id)->first();
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'name' => 'required|string',
                'slug' => 'required|string',
                'status' => 'required|integer',
            ]);
            $model=Banners::where('id',$id)->first();
            $model->name=$request->name;
            $model->slug=$request->slug;
            $model->type=$request->type;
            $model->status=$request->status;
            $model->updated_at = date('Y-m-d H:i:s', strtotime('now'));
            $model->banners=$request->banners;
            $model->existingId = $request->existingId;
            if ($model->save()) {
                return redirect('banners')->with('success', __('managecontent::banners.updated'));
            }
        }
        return view('banners::create',['model'=>$model]);
    }

    public function delete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            BannerImages::where('banner_id', $id)->delete();
        }
        DB::table($model->getTable())
        ->where('id', $id)
        ->update(['deleted_by' => Auth::user()->id]);
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
    }

    public function newModel()
    {
        return new Banners;
    }

    public function findModel($id)
    {
        return Banners::where('id', $id)->first();
    }

    public function popdescmodal(Request $request)
    {
        $description = $request->data['description'];
        echo view('banners::.modal-box',compact('description'));
    }

}

<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{

    public function index(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        return view('categories::.index',compact('moduleTypeId','request'));
    }

    public function datatableData(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        $query = DB::table($this->newModel()->getTable())
        ->whereNull('deleted_at');

        if($request->search['value']!=''){
            $keyword = $request->search['value'];
        }
        $countQuery = clone $query;
        $totalRecords = $countQuery->count('id');
        $orderBy = 'id';
        if($request->ob)$orderBy = $request->ob;
        $orderByOrder = 'desc';
        if($request->obo)$orderByOrder = $request->obo;
        $models = $query->offset($request->start)
        ->limit($request->length)
        ->orderBy($orderBy,$orderByOrder)
        ->get();
        $dataArr=[];

        if($models!=null){
            foreach($models as $model){
                $thisData=[];
                $thisData['id']=$model->id;
                $thisData['parent_id']=getCatParentTitle($model->parent_id);
                $thisData['title']=$model->title;
                $thisData['type']=$model->type;
                $thisData['short_description']=$model->short_description;
                $thisData['description']=$model->description;
                $thisData['image']=getImageTag($model->image);
                $thisData['baner_image']=getImageTag($model->baner_image);
                $thisData['meta_title']=$model->meta_title;
                $thisData['meta_description']=$model->meta_description;
                $thisData['status']=$model->status;
                $thisData['cb_col']='';
                $thisData['created_at']=formatDateTime($model->created_at);
                $createdBy = getUserInfo($model->created_by);
                $thisData['created_by']=$createdBy->name;

                $actBtns=[];
                if(checkActionAllowed('view','categories')){
                    $actBtns[]='
                    <a href="'.url('categories/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-file-2"></i>
                    </a>';
                }
                if(checkActionAllowed('update','categories')){
                    $actBtns[]='
                    <a href="'.url('categories/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-edit"></i>
                    </a>';
                }
                if(checkActionAllowed('delete','categories')){
                    $actBtns[]='
                    <a href="'.url('categories/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                    </a>';
                }

                $thisData['action_col']=getDatatableActionTemplate($actBtns);
                $dataArr[]=$thisData;
            }
        }


        $dtInputColsArr = [];
        $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
        $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
        $inputArr = [
            'columns' => $dtInputColsArr,
            'draw' => (int)$request->draw,
            'length' => (int)$request->length,
            'order' => [
                ['column'=>0,'dir'=>'asc'],
            ],
            'search' => ['value'=>$request->search['value'],'regex'=>false],
            'start' => $request->start,
        ];

        $response = [
            'draw' => (int)$request->draw,
            'input' => $inputArr,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords,
            'data' => $dataArr,
        ];
        return new JsonResponse($response);
    }

    public function create(Request $request)
    {
        $model = new Categories;
        if($request->isMethod('post')){
            $validatedData = $request->validate([
              'title' => 'required|string',
              'type' => 'required',
          ]);
            $model = new Categories;
            $model->parent_id=$request->parent_id;
            $model->title=$request->title;
            $model->type=$request->type;
            $model->short_description=$request->short_description;
            $model->description=$request->description;
            $model->image=$request->image;
            $model->baner_image=$request->baner_image;
            $model->meta_title=$request->meta_title;
            $model->meta_description=$request->meta_description;
            $model->status=$request->status;
            $model->created_by = Auth::user()->id;
            if ($model->save()) {
                return redirect('categories')->with('success', __('managecontent::categories.saved'));
            }
        }
        return view('categories::create',['model'=>$model]);

    }

    public function view(Categories $categories, $id)
    {
        $model=Categories::where('id',$id)->first();
        return view('categories::view',compact('model'));
    }

    public function update(Request $request, Categories $categories, $id)
    {    
        $model=Categories::where('id',$id)->first();
        if($request->isMethod('post')){
            $model=Categories::where('id',$id)->first();
            $model->parent_id=$request->parent_id;
            $model->title=$request->title;
            $model->type=$request->type;
            $model->short_description=$request->short_description;
            $model->description=$request->description;
            $model->image=$request->image;
            $model->baner_image=$request->baner_image;
            $model->meta_title=$request->meta_title;
            $model->meta_description=$request->meta_description;
            $model->status=$request->status;
            if ($model->save()) {
                return redirect('categories')->with('success', __('managecontent::categories.updated'));
            }
        }
        return view('categories::create',['model'=>$model]);
    }

    public function newModel()
    {
        return new Categories;
    }

    public function findModel($id)
    {
        return Categories::where('id', $id)->first();
    }
  //   public function destroy($id,$keyWord=null)
  //   {
  //     $model = $this->findModel($id);
  //     $model->delete();
  //     DB::table($model->getTable())
  //     ->where('id', $id)
  //     ->update(['deleted_by' => Auth::user()->id]);
  //     return redirect()->back();
  //         // return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  // }

}

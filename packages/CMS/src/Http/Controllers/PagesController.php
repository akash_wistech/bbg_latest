<?php

namespace NaeemAwan\CMS\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\CMS\Models\Pages;
use NaeemAwan\CMS\Models\PagesDetail;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $results=Pages::get();
    return view('namod-cms::pages.index', compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Pages;
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'title'=>'required',
        'descp'=>'nullable|string',
        'meta_title'=>'nullable|string',
        'meta_descp'=>'nullable|string',
        'meta_keyword'=>'nullable|string',
        'status'=>'required|integer',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:500',
      ]);
      $data= new Pages();
      $data->title=$request->title;
      $data->descp=$request->descp ?? '';
      $data->meta_title=$request->meta_title ?? '';
      $data->meta_descp=$request->meta_descp ?? '';
      $data->meta_keyword=$request->meta_keyword ?? '';
      $data->status=$request->status ?? 1;
      if ($data->save()) {
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('namod-cms::cms.saved'),]]);
        }else{
          return redirect('pages')->with('success', __('namod-cms::cms.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('namod-cms::cms.notsaved'),]]);
        }else{
          return redirect('pages')->with('error', __('namod-cms::cms.notsaved'));
        }
      }
    }
    return view('namod-cms::pages.create',compact('model'));
  }

  /**
  * Update status of model
  *
  * @param  \NaeemAwan\CMS\Models\Pages  $pages
  * @return \Illuminate\Http\Response
  */
  public function status(Request $request, $id)
  {
    $model = Pages::where('id', $id)->first();
    if($model->status==0){
      $status=1;
    }else{
      $status=0;
    }
    DB::table($model->getTable())
    ->where('id', $model->id)
    ->update(['status' => $status]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.statusUpdated'),]]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \NaeemAwan\CMS\Models\Pages  $pages
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, $id)
  {
    $model = Pages::where('id', $id)->first();
    $langDetails = $model->langDetails;
    if($langDetails!=null){
      foreach($langDetails as $langDetail){
        $model->title[$langDetail->lang] = $langDetail->title;
        $model->descp[$langDetail->lang] = $langDetail->descp;
        $model->meta_title[$langDetail->lang] = $langDetail->meta_title;
        $model->meta_descp[$langDetail->lang] = $langDetail->meta_descp;
        $model->meta_keyword[$langDetail->lang] = $langDetail->meta_keyword;
      }
    }
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'id'=>'required',
        'title'=>'required',
        'descp'=>'nullable|string',
        'meta_title'=>'nullable|string',
        'meta_descp'=>'nullable|string',
        'meta_keyword'=>'nullable|string',
        'status'=>'required|integer',
      ]);

      $data= Pages::find($request->id);
      $data->title=$request->title;
      if(isset($request->descp)){
        $data->descp=$request->descp;
      }
      if(isset($request->meta_title)){
        $data->meta_title=$request->meta_title;
      }
      if(isset($request->meta_descp)){
        $data->meta_descp=$request->meta_descp;
      }
      if(isset($request->meta_keyword)){
        $data->meta_keyword=$request->meta_keyword;
      }
      if(isset($request->status)){
        $data->status=$request->status;
      }
      $data->updated_at = date("Y-m-d H:i:s");
      if ($data->update()) {
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('namod-cms::cms.saved'),]]);
        }else{
          return redirect('pages')->with('success', __('namod-cms::cms.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('namod-cms::cms.notsaved'),]]);
        }else{
          return redirect('pages')->with('error', __('namod-cms::cms.notsaved'));
        }
      }
    }
    return view('namod-cms::pages.update', [
      'model'=>$model,
    ]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \NaeemAwan\CMS\Models\Pages  $pages
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $model= Pages::where('id',$id)->first();
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('namod-cms::cms.deleted'),]]);
  }

  public function ViewBySlug(Request $request)
  {
    $slug = '';
    if ($request->has('slug')) {
      $slug = $request['slug'];
    }
    $model = PagesDetail::where(['slug'=>$slug])->first();
    return view('namod-cms::pages.view-by-slug',compact('model'));

  }
}

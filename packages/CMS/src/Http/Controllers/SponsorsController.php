<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\Sponsors;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class SponsorsController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
  $moduleTypeId = $this->newModel()->moduleTypeId;
  return view('sponsors::.index',compact('moduleTypeId','request'));
}

public function newModel()
{
  return new Sponsors;
}

public function datatableData(Request $request)
{
  $moduleTypeId = $this->newModel()->moduleTypeId;
  $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');


// permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'membership',$query);



  if($request->search['value']!=''){
    $keyword = $request->search['value'];

// dd($keyword);
$srchFlds[]='title';
$srchFlds[]='type';
$srchFlds[]='image';
$srchFlds[]='short_description';
$srchFlds[]='description';
searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
  }



//   if($request->created_at!=''){
//       if(strpos($request->created_at," - ")){
//         $query->where(function($query) use($request){
//           list($start_date,$end_date)=explode(" - ",$request->created_at);
//           $query->where('created_at','>=',$start_date)
//           ->where('created_at','<=',$end_date);
//       });
//     }else{
//         $query->where('created_at',$request->created_at);
//     }
// }
// if($request->name!='')$query->where('name','like','%'.$request->name.'%');
// if($request->full_name!='')$query->where('full_name','like','%'.$request->full_name.'%');
// if($request->company_name!='')$query->where('company_name','like','%'.$request->company_name.'%');
// if($request->email!='')$query->where('email','like','%'.$request->email.'%');
// if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
// if($request->input_field!=''){
//   advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
// }

  $countQuery = clone $query;

  $totalRecords = $countQuery->count('id');

// $gridViewColumns=getGridViewColumns($moduleTypeId);


  $orderBy = 'id';
  if($request->ob)$orderBy = $request->ob;
  $orderByOrder = 'desc';
  if($request->obo)$orderByOrder = $request->obo;
  $models = $query->offset($request->start)
  ->limit($request->length)
  ->orderBy($orderBy,$orderByOrder)
  ->get();
  $dataArr=[];
// dd($models);

  if($models!=null){
    foreach($models as $model){
      $thisData=[];
//Colors column
// $colorArr = getActivityColors($moduleTypeId,$model->id);
// $thisData['cs_col']=implode("",$colorArr);

      $thisData['id']=$model->id;
      $thisData['title']=$model->title;
      $thisData['type']=$model->type;
      $thisData['image']=getImageTag($model->image);
      $thisData['short_description']=$model->short_description;
      $thisData['description']=$model->description;
      $thisData['sort_order']=$model->sort_order; 
      $thisData['status']=getvfcPublishArrLable()[$model->status];
      $thisData['cb_col']='';

// if($gridViewColumns!=null){
//   foreach($gridViewColumns as $gridViewColumn){
//     $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
//   }
// }
      $thisData['created_at']=formatDateTime($model->created_at);
      $createdBy = getUserInfo($model->created_by);
      $thisData['created_by']=$createdBy->name;

      $actBtns=[];
      if(checkActionAllowed('view','sponsors')){
        $actBtns[]='
        <a href="'.url('sponsors/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-file-2"></i>
        </a>';
      }
      if(checkActionAllowed('update','sponsors')){
        $actBtns[]='
        <a href="'.url('sponsors/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-edit"></i>
        </a>';
      }
      if(checkActionAllowed('delete','sponsors')){
        $actBtns[]='
        <a href="'.url('sponsors/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
        <i class="text-dark-50 flaticon-delete"></i>
        </a>';
      }

      $thisData['action_col']=getDatatableActionTemplate($actBtns);
      $dataArr[]=$thisData;
    }
  }


  $dtInputColsArr = [];
  $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
// $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
// if($gridViewColumns!=null){
//   foreach($gridViewColumns as $gridViewColumn){
//     $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
//   }
// }
  $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
  $inputArr = [
    'columns' => $dtInputColsArr,
    'draw' => (int)$request->draw,
    'length' => (int)$request->length,
    'order' => [
      ['column'=>0,'dir'=>'asc'],
    ],
    'search' => ['value'=>$request->search['value'],'regex'=>false],
    'start' => $request->start,
  ];

  $response = [
    'draw' => (int)$request->draw,
    'input' => $inputArr,
    'recordsTotal' => $totalRecords,
    'recordsFiltered' => $totalRecords,
    'data' => $dataArr,
  ];
  return new JsonResponse($response);

}


/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create(Request $request)
{
// dd($request->all());
  $model = new Sponsors;
  if($request->isMethod('post')){
    $validatedData = $request->validate([
      'title' => 'required|string',
      'type' => 'required',
      'short_description' => 'required|string',
    ]);
    $model = new Sponsors;
    $model->title = $request->title;
    $model->type = $request->type;
    $model->short_description = $request->short_description;
    $model->description = $request->description;
    $model->status = $request->status!=''?$request->status:1;
    $model->image = $request->image;
    $model->sort_order = $request->sort_order;
    $model->created_by = Auth::user()->id;
    if ($model->save()) {
      return redirect('sponsors')->with('success', __('managecontent::sponsors.saved'));
    }
  }
// dd("here");
  return view('sponsors::create',['model'=>$model]);
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//
}

/**
* Display the specified resource.
*
* @param  \App\Models\Sponsors  $sponsors
* @return \Illuminate\Http\Response
*/
public function view(Sponsors $sponsors, $id)
{
  $model=Sponsors::where('id',$id)->first();
  // dd($model);
  return view('sponsors::.view',compact('model'));
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Models\Sponsors  $sponsors
* @return \Illuminate\Http\Response
*/
  public function edit(Sponsors $sponsors)
  {

  }

  public function update(Request $request, Sponsors $sponsors, $id)
  {
    
    $model=Sponsors::where('id',$id)->first();
    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|string',
        'type' => 'required',
        'short_description' => 'required|string',
      ]);
      $model=Sponsors::where('id',$id)->first();
      $model->title = $request->title;
      $model->type = $request->type;
      $model->short_description = $request->short_description;
      $model->description = $request->description;
      $model->status = $request->status;
      $model->image = $request->image;
      $model->sort_order = $request->sort_order;
      if ($model->save()) {
        return redirect('sponsors')->with('success', __('managecontent::sponsors.updated'));
      }
    }
    return view('sponsors::create',['model'=>$model]);
  }


  public function findModel($id)
  {
    return Sponsors::where('id', $id)->first();
  }

  //frontend actions
  public function sponsorsfend()
  {
    $results = $this->newModel()::where(['status'=>1])->orderBy('sort_order', 'ASC')->paginate(10);
  // dd($results);
    return view('frontend-views::sponsors.index',compact('results'));
  }

  public function SponsorDetails($id=null)
  {
    $data = $this->findModel($id);
    return view('frontend-views::sponsors.sponsor_detail',compact('data'));
  }


}

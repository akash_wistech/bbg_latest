<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\CommitteMembers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class CommitteMembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $moduleTypeId = $this->newModel()->moduleTypeId;
        // dd($moduleTypeId);
      return view('committe-members::committe-members.index',compact('moduleTypeId','request'));
    }

    public function newModel()
    {
      return new CommitteMembers;
    }

    public function datatableData(Request $request)
    {

      $moduleTypeId = $this->newModel()->moduleTypeId;
      $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');


    // permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'membership',$query);



      if($request->search['value']!=''){
        $keyword = $request->search['value'];

        $srchFlds[]='full_name';
        $srchFlds[]='designation';
        $srchFlds[]='email';
        $srchFlds[]='phonenumber';
        $srchFlds[]='image';
        $srchFlds[]='description';
        searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
      }



      if($request->created_at!=''){
        if(strpos($request->created_at," - ")){
          $query->where(function($query) use($request){
            list($start_date,$end_date)=explode(" - ",$request->created_at);
            $query->where('created_at','>=',$start_date)
            ->where('created_at','<=',$end_date);
          });
        }else{
          $query->where('created_at',$request->created_at);
        }

      }

      if($request->full_name!='')$query->where('full_name','like','%'.$request->full_name.'%');
      
      if($request->input_field!=''){
        advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
      }

      $countQuery = clone $query;

      $totalRecords = $countQuery->count('id');

      $gridViewColumns=getGridViewColumns($moduleTypeId);


      $orderBy = 'id';
      if($request->ob)$orderBy = $request->ob;
      $orderByOrder = 'desc';
      if($request->obo)$orderByOrder = $request->obo;
      $models = $query->offset($request->start)
      ->limit($request->length)
      ->orderBy($orderBy,$orderByOrder)
      ->get();
      $dataArr=[];
        // dd($models);

      if($models!=null){
        foreach($models as $model){
          $thisData=[];
        //Colors column
        // $colorArr = getActivityColors($moduleTypeId,$model->id);
        // $thisData['cs_col']=implode("",$colorArr);

          $thisData['id']=$model->id;
          $thisData['full_name']=$model->full_name;
          $thisData['type']=getPredefinedListTitle($model->type);
          $thisData['designation']=$model->designation;
          $thisData['email']=$model->email;
          $thisData['phonenumber']=$model->phonenumber;
          $thisData['description']=$model->description;
          $thisData['status']=getvfcPublishArrLable()[$model->status];
          $thisData['image']=getImageTag($model->image);
          $thisData['sort_order']=$model->sort_order; 
          $thisData['cb_col']='';

        // if($gridViewColumns!=null){
        //   foreach($gridViewColumns as $gridViewColumn){
        //     $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
        //   }
        // }
          $thisData['created_at']=formatDateTime($model->created_at);
          $createdBy = getUserInfo($model->created_by);
          $thisData['created_by']=$createdBy->name;

          $actBtns=[];
          if(checkActionAllowed('view','committe-members')){
            $actBtns[]='
            <a href="'.url('committe-members/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
            <i class="text-dark-50 flaticon-file-2"></i>
            </a>';
          }
          if(checkActionAllowed('update','committe-members')){
            $actBtns[]='
            <a href="'.url('committe-members/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
            <i class="text-dark-50 flaticon-edit"></i>
            </a>';
          }
          if(checkActionAllowed('delete','committe-members')){
            $actBtns[]='
            <a href="'.url('committe-members/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
            <i class="text-dark-50 flaticon-delete"></i>
            </a>';
          }

          $thisData['action_col']=getDatatableActionTemplate($actBtns);
          $dataArr[]=$thisData;
        }
      }


      $dtInputColsArr = [];
      $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
        // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
        // if($gridViewColumns!=null){
        //   foreach($gridViewColumns as $gridViewColumn){
        //     $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
        //   }
        // }
      $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
      $inputArr = [
        'columns' => $dtInputColsArr,
        'draw' => (int)$request->draw,
        'length' => (int)$request->length,
        'order' => [
          ['column'=>0,'dir'=>'asc'],
        ],
        'search' => ['value'=>$request->search['value'],'regex'=>false],
        'start' => $request->start,
      ];

      $response = [
        'draw' => (int)$request->draw,
        'input' => $inputArr,
        'recordsTotal' => $totalRecords,
        'recordsFiltered' => $totalRecords,
        'data' => $dataArr,
      ];
      return new JsonResponse($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      // dd($request->all());
      $model = new CommitteMembers;
      if($request->isMethod('post')){
        $validatedData = $request->validate([
          'full_name' => 'required|string',
          'designation' => 'required|string',
          'status' => 'required|integer',
        ]);
        $model = new CommitteMembers;
        $model->full_name = $request->full_name;
        $model->designation = $request->designation;
        $model->email = $request->email;
        $model->phonenumber = $request->phonenumber;
        $model->type = $request->type;
        $model->sort_order = $request->sort_order;
        $model->description = $request->description;
        $model->status = $request->status;
        $model->image = $request->image;
        $model->created_by = Auth::user()->id;
        if ($model->save()) {
          return redirect('committe-members')->with('success', __('managecontent::committemembers.saved'));
        }
      }
      return view('committe-members::committe-members.create',['model'=>$model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommitteMembers  $committeMembers
     * @return \Illuminate\Http\Response
     */
    public function view(CommitteMembers $committeMembers, $id)
    {
      $model=CommitteMembers::where('id',$id)->first();
        // dd($model);
      return view('committe-members::committe-members.view',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommitteMembers  $committeMembers
     * @return \Illuminate\Http\Response
     */
    public function edit(CommitteMembers $committeMembers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommitteMembers  $committeMembers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommitteMembers $committeMembers, $id)
    {
        // dd($id);
      $model=CommitteMembers::where('id',$id)->first();
      if($request->isMethod('post')){
        $validatedData = $request->validate([
          'full_name' => 'required|string',
          'designation' => 'required|string',
          'status' => 'required|integer',
        ]);
        $model=CommitteMembers::where('id',$id)->first();
        $model->full_name = $request->full_name;
        $model->designation = $request->designation;
        $model->email = $request->email;
        $model->phonenumber = $request->phonenumber;
        $model->type = $request->type;
        $model->sort_order = $request->sort_order;
        $model->description = $request->description;
        $model->status = $request->status;
        $model->image = $request->image;
        if ($model->save()) {
          return redirect('committe-members')->with('success', __('managecontent::committemembers.updated'));
        }
      }
      return view('committe-members::committe-members.create',['model'=>$model]);
    }

    public function findModel($id)
    {
      return CommitteMembers::where('id', $id)->first();
    }

    public function boardmembersinfo()
    {
      $idz = getSetting('show_in_committe_members');
      // dd($idz);
      $staffCategories = explode(",",$idz);
      return view('frontend-views::committe-members.committee-info',compact('staffCategories'));
    }

    public function CommitteeDetails($id)
    {
      $data = $this->findModel($id);
      return view('frontend-views::committe-members.committee_staff_detail',compact('data'));
    }

    public function OperationsInfo()
    {
      $type_id =  getSetting('business_team_list');
      $staff = $this->newModel()::where(['type'=>$type_id,'status'=>1])->orderBy('sort_order', 'ASC')->get();
      return view('frontend-views::committe-members.operations-team-info',compact('staff'));
    }

    public function FutureVisionCollective()
    {
     $type_id = getSetting('future_vision_collective_list');
     // dd($type_id);
     $staff = $this->newModel()::where(['type'=>$type_id,'status'=>1])->orderBy('sort_order', 'ASC')->get();
     // dd($staff);
     return view('frontend-views::committe-members.future_vision_collective',compact('staff'));
   }

   public function BusinessDetails($id)
   {
    $data = $this->findModel($id);
    return view('frontend-views::committe-members.business-staff-detail',compact('data'));
  }

}

<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{

    public function index(Request $request)
    {
        // echo "in index action";
        // dd($status);
        $status='';
        if ($request->has('status')) {
            $status = $request->input('status');
        }
        $moduleTypeId = $this->newModel()->moduleTypeId;
        return view('gallery::.index',compact('moduleTypeId','request', 'status'));
    }

    public function datatableData(Request $request)
    {
        // echo "here controller";
        // dd($status);
      $status='';
      if ($request->has('status')) {
        $status = $request->input('status');
    }

    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())
    ->when($status, function ($query, $status){
        return $query->where('status', $status);
    })
    ->whereNull('deleted_at');

    if($request->search['value']!=''){
        $keyword = $request->search['value'];

        $srchFlds[]='name';
        $srchFlds[]='video_link';
        $srchFlds[]='image';
        $srchFlds[]='size';
        $srchFlds[]='position';
        $srchFlds[]='status';
        $srchFlds[]='description';
        searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    $countQuery = clone $query;
    $totalRecords = $countQuery->count('id');
    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];

    if($models!=null){
        foreach($models as $model){
            $thisData=[];
            $thisData['id']=$model->id;
            $thisData['name']=$model->name;
            $thisData['image']=getImageTag($model->image);
            $thisData['video_link']=$model->video_link;
            $thisData['size']=getGallerySizeArr()[$model->size];
            $thisData['position']=getGalleryPositionArr()[$model->position];
            $thisData['status']=getGalleryStatusLabelArr()[$model->status];
            $thisData['description']=$model->description;
            $thisData['cb_col']='';
            $thisData['created_at']=formatDateTime($model->created_at);
            $createdBy = getUserInfo($model->created_by);
            $thisData['created_by']=$createdBy->name;

            $actBtns=[];
            if(checkActionAllowed('view','gallery')){
                $actBtns[]='
                <a href="'.url('gallery/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
                <i class="text-dark-50 flaticon-file-2"></i>
                </a>';
            }
            if(checkActionAllowed('update','gallery')){
                $actBtns[]='
                <a href="'.url('gallery/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
                <i class="text-dark-50 flaticon-edit"></i>
                </a>';
            }
            if(checkActionAllowed('delete','gallery')){
                $actBtns[]='
                <a href="'.url('gallery/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
                </a>';
            }

            $thisData['action_col']=getDatatableActionTemplate($actBtns);
            $dataArr[]=$thisData;
        }
    }


    $dtInputColsArr = [];
    $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
        'columns' => $dtInputColsArr,
        'draw' => (int)$request->draw,
        'length' => (int)$request->length,
        'order' => [
            ['column'=>0,'dir'=>'asc'],
        ],
        'search' => ['value'=>$request->search['value'],'regex'=>false],
        'start' => $request->start,
    ];

    $response = [
        'draw' => (int)$request->draw,
        'input' => $inputArr,
        'recordsTotal' => $totalRecords,
        'recordsFiltered' => $totalRecords,
        'data' => $dataArr,
    ];
    return new JsonResponse($response);
}


public function create(Request $request)
{
        // dd($request->input());
    $model = new Gallery;
    if($request->isMethod('post')){
        $validatedData = $request->validate([
            'name' => 'required|string',
            'image' => 'required',
        ]);
        $model = new Gallery;
        $model->name = $request->name;
        $model->image = $request->image;
        $model->video_link = $request->video_link;
        $model->size = $request->size;
        $model->position = $request->position;
        $model->status = $request->status;
        $model->description = $request->description;
        $model->created_by = Auth::user()->id;
        if ($model->save()) {
            return redirect('gallery')->with('success', __('managecontent::gallery.saved'));
        }
    }
    return view('gallery::create',['model'=>$model]);
}


public function view(Gallery $gallery, $id)
{
    $model=Gallery::where('id',$id)->first();
    return view('gallery::.view',compact('model'));
}


public function update(Request $request, Gallery $gallery ,$id)
{
    $model=Gallery::where('id',$id)->first();
    if($request->isMethod('post')){
        $validatedData = $request->validate([
            'name' => 'required|string',
            'image' => 'required',
        ]);
        $model=Gallery::where('id',$id)->first();
        $model->name = $request->name;
        $model->image = $request->image;
        $model->video_link = $request->video_link;
        $model->size = $request->size;
        $model->position = $request->position;
        $model->status = $request->status;
        $model->description = $request->description;
        if ($model->save()) {
            return redirect('gallery')->with('success', __('managecontent::gallery.updated'));
        }
    }
    return view('gallery::create',['model'=>$model]);
}

public function newModel()
{
    return new Gallery;
}

public function findModel($id)
{
    return Gallery::where('id', $id)->first();
}

public function sendinvoice()
{
    return view('gallery::sendinvoice');
}

// public function destroy($id,$keyWord=null)
// {
//   $model = $this->findModel($id);
//   $model->delete();
//   DB::table($model->getTable())
//   ->where('id', $id)
//   ->update(['deleted_by' => Auth::user()->id]);
//   return redirect()->back();
//   // return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
// }

}

<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\Partners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class PartnersController extends Controller
{

    public function index(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        return view('partners::.index',compact('moduleTypeId','request'));
    }

    public function datatableData(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        $query = DB::table($this->newModel()->getTable())
        ->whereNull('deleted_at');

        if($request->search['value']!=''){
            $keyword = $request->search['value'];

            $srchFlds[]='name';
            $srchFlds[]='description';
            $srchFlds[]='logo';
            searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
        }

        $countQuery = clone $query;
        $totalRecords = $countQuery->count('id');
        $orderBy = 'id';
        if($request->ob)$orderBy = $request->ob;
        $orderByOrder = 'desc';
        if($request->obo)$orderByOrder = $request->obo;
        $models = $query->offset($request->start)
        ->limit($request->length)
        ->orderBy($orderBy,$orderByOrder)
        ->get();
        $dataArr=[];

        if($models!=null){
            foreach($models as $model){
                $thisData=[];
                $thisData['id']=$model->id;
                $thisData['name']=$model->name;
                $thisData['description']=$model->description;
                $thisData['logo']=getImageTag($model->logo);
                $thisData['cb_col']='';
                $thisData['created_at']=formatDateTime($model->created_at);
                $createdBy = getUserInfo($model->created_by);
                $thisData['created_by']=$createdBy->name;

                $actBtns=[];
                if(checkActionAllowed('view','partners')){
                    $actBtns[]='
                    <a href="'.url('partners/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-file-2"></i>
                    </a>';
                }
                if(checkActionAllowed('update','partners')){
                    $actBtns[]='
                    <a href="'.url('partners/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-edit"></i>
                    </a>';
                }
                if(checkActionAllowed('delete','partners')){
                    $actBtns[]='
                    <a href="'.url('partners/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                    </a>';
                }

                $thisData['action_col']=getDatatableActionTemplate($actBtns);
                $dataArr[]=$thisData;
            }
        }


        $dtInputColsArr = [];
        $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
        $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
        $inputArr = [
            'columns' => $dtInputColsArr,
            'draw' => (int)$request->draw,
            'length' => (int)$request->length,
            'order' => [
                ['column'=>0,'dir'=>'asc'],
            ],
            'search' => ['value'=>$request->search['value'],'regex'=>false],
            'start' => $request->start,
        ];

        $response = [
            'draw' => (int)$request->draw,
            'input' => $inputArr,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords,
            'data' => $dataArr,
        ];
        return new JsonResponse($response);
    }

    public function create(Request $request)
    {
        // dd($request->banners);
        $model = new Partners;
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'name' => 'required|string',
                'logo' => 'required',
            ]);
            $model = new Partners;
            $model->name=$request->name;
            $model->logo=$request->logo;
            $model->description=$request->description;
            $model->created_by = Auth::user()->id;
            if ($model->save()) {
                return redirect('partners')->with('success', __('managecontent::partners.saved'));
            }
        }
        return view('partners::create',['model'=>$model]);
    }

    public function view(Partners $partners, $id)
    {
        $model=Partners::where('id',$id)->first();
        return view('partners::.view',compact('model'));
    }

    public function update(Request $request, Partners $partners, $id)
    {
        $model=Partners::where('id',$id)->first();
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'name' => 'required|string',
                'logo' => 'required',
            ]);
            $model=Partners::where('id',$id)->first();
            $model->name=$request->name;
            $model->description=$request->description;
            $model->logo=$request->logo;
            if ($model->save()) {
                return redirect('partners')->with('success', __('managecontent::partners.updated'));
            }
        }
        return view('partners::create',['model'=>$model]);
    }

    public function newModel()
    {
        return new Partners;
    }

    public function findModel($id)
    {
        return Partners::where('id', $id)->first();
    }

  //   public function destroy($id,$keyWord=null)
  //   {
  //     $model = $this->findModel($id);
  //     $model->delete();
  //     DB::table($model->getTable())
  //     ->where('id', $id)
  //     ->update(['deleted_by' => Auth::user()->id]);
  //     return redirect()->back();
  //     // return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  // }
}

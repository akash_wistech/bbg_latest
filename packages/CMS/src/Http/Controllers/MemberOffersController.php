<?php

namespace NaeemAwan\CMS\Http\Controllers;

use NaeemAwan\CMS\Models\MemberOffers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Company;
use App\Models\Contact;

class MemberOffersController extends Controller
{

    public function index(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
// dd($moduleTypeId);
        return view('member-offers::.index',compact('moduleTypeId','request'));
    }

    public function datatableData(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        $query = DB::table($this->newModel()->getTable())
        ->whereNull('deleted_at');

        if ($request->has('company_id')) {
            // dd($request->company_id);
            $query->where(['offer_rel'=>'company','offer_rel_id'=>$request->company_id]);
        }

        if($request->search['value']!=''){
            $keyword = $request->search['value'];

            $srchFlds[]='title';
            $srchFlds[]='image';
            $srchFlds[]='short_description';
            $srchFlds[]='description';
            searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
        }

        $countQuery = clone $query;
        $totalRecords = $countQuery->count('id');
        $orderBy = 'id';
        if($request->ob)$orderBy = $request->ob;
        $orderByOrder = 'desc';
        if($request->obo)$orderByOrder = $request->obo;
        $models = $query->offset($request->start)
        ->limit($request->length)
        ->orderBy($orderBy,$orderByOrder)
        ->get();
        $dataArr=[];

        if($models!=null){
            foreach($models as $model){
                $thisData=[];
                $thisData['id']=$model->id;

                $thisData['category_id']=getCatParentTitle($model->category_id);

// dd($thisData['category_id']);




                $thisData['title']=$model->title;
                $thisData['short_description']=$model->short_description;
                $thisData['description']=$model->description;
                $thisData['image']=getImageTag($model->image);
                $thisData['is_active']=getStatusIconArr()[$model->is_active];
                $thisData['sort_order']=$model->sort_order;
                $thisData['offer_rel']=$model->offer_rel;
                $thisData['offer_rel_id']=getRelToForMemOff($model->offer_rel, $model->offer_rel_id);

                $thisData['cb_col']='';
                $thisData['created_at']=formatDateTime($model->created_at);
                $createdBy = getUserInfo($model->created_by);
                $thisData['created_by']=$createdBy->name;

                $actBtns=[];
                if(checkActionAllowed('view','member-offers')){
                    $actBtns[]='
                    <a href="'.url('member-offers/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-file-2"></i>
                    </a>';
                }
                if(checkActionAllowed('update','member-offers')){
                    $actBtns[]='
                    <a href="'.url('member-offers/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-edit"></i>
                    </a>';
                }
                if(checkActionAllowed('delete','member-offers')){
                    $actBtns[]='
                    <a href="'.url('member-offers/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                    </a>';
                }

                $thisData['action_col']=getDatatableActionTemplate($actBtns);
                $dataArr[]=$thisData;
            }
        }


        $dtInputColsArr = [];
        $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
        $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
        $inputArr = [
            'columns' => $dtInputColsArr,
            'draw' => (int)$request->draw,
            'length' => (int)$request->length,
            'order' => [
                ['column'=>0,'dir'=>'asc'],
            ],
            'search' => ['value'=>$request->search['value'],'regex'=>false],
            'start' => $request->start,
        ];

        $response = [
            'draw' => (int)$request->draw,
            'input' => $inputArr,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords,
            'data' => $dataArr,
        ];
        return new JsonResponse($response);
    }

    public function create(Request $request)
    {
// dd($request->all());
        $model = new MemberOffers;
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'title' => 'required|string',
                'short_description' => 'required|string',
                'category_id' => 'required|integer',
                'is_active' => 'required|integer',
            ]);
            $model = new MemberOffers;
            $model->category_id=$request->category_id;
            $model->title=$request->title;
            $model->short_description=$request->short_description;
            $model->description=$request->description;
            $model->image=$request->image;
            $model->is_active=$request->is_active;
            $model->sort_order=$request->sort_order;
            $model->offer_rel=$request->offer_rel;
            $model->offer_rel_id=$request->offer_rel_id;
            $model->created_by = Auth::user()->id;
            if ($model->save()) {
                return redirect('member-offers')->with('success', __('managecontent::memberoffers.saved'));
            }
        }
        return view('member-offers::create',['model'=>$model]);
    }

    public function view(MemberOffers $memberOffers, $id)
    {
        $model=MemberOffers::where('id',$id)->first();
        return view('member-offers::.view',compact('model'));
    }

    public function update(Request $request, MemberOffers $memberOffers, $id)
    {
        $model=MemberOffers::where('id',$id)->first();
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'title' => 'required|string',
                'short_description' => 'required|string',
                'category_id' => 'required|integer',
                'is_active' => 'required|integer',
            ]);
            $model=MemberOffers::where('id',$id)->first();
            $model->category_id=$request->category_id;
            $model->title=$request->title;
            $model->short_description=$request->short_description;
            $model->description=$request->description;
            $model->image=$request->image;
            $model->is_active=$request->is_active;
            $model->sort_order=$request->sort_order;
            $model->offer_rel=$request->offer_rel;
            $model->offer_rel_id=$request->offer_rel_id;
            if ($model->save()) {
                return redirect('member-offers')->with('success', __('managecontent::memberoffers.updated'));
            }
        }
        return view('member-offers::create',['model'=>$model]);
    }

    public function newModel()
    {
        return new MemberOffers;
    }

    public function findModel($id)
    {
        return MemberOffers::where('id', $id)->first();
    }

    public function getRelToMemCom(Request $request)
    {
        $data_key =  $request->post('data')['data_key'];
        $rel_id =  $request->post('data')['rel_id'];

        if ($data_key=='member') {
            $users = User::get()->toArray();
            $html='<option value="" selected="selected">Select</option>';
            foreach ($users as $key => $user) {if ($rel_id == $user['id']) {
                $html.='<option value="'.$user['id'].'" selected>'.$user['name'].'.</option>';
            }else{
                $html.='<option value="'.$user['id'].'">'.$user['name'].'.</option>';
            }
        }
        return $html;

    }
    if ($data_key=='company') {
        $companies = Company::get()->toArray();
        $html='<option value="" selected="selected">Select</option>';
        foreach ($companies as $key => $company) {
            if ($rel_id ==$company['id']) {
                $html.='<option value="'.$company['id'].'" selected>'.$company['title'].'.</option>';
            }else{
                $html.='<option value="'.$company['id'].'">'.$company['title'].'.</option>';
            }
        }
        return $html;
    }
}


public function MemberOffers(Request $request)
{
    if (Auth::check()) {
        $results = MemberOffers::where(['is_active'=>1])->orderBy('sort_order','desc')->get();
        $model = Contact::where(['id'=>Auth::user()->id])->first();
        return view('member-offers::members-offer-blocks',['model'=>$model,'results'=>$results]);
    }else{
        return redirect('member/login');
    }
}

public function MemberOffersDetail($id)
{
    $result = $this->findModel($id);
    return view('member-offers::member_offer_detail',['result'=>$result]);
}
}

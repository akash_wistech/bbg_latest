<?php

namespace NaeemAwan\CMS\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\CMS\Models\Pages;
use NaeemAwan\CMS\Models\Menus;
use NaeemAwan\CMS\Models\MenuItems;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MenusController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $results=Menus::get();
    return view('namod-cms::menus.index', compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Menus;
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'title'=>'required',
        'status'=>'required|integer',
      ]);
      $data= new Menus();
      $data->title=$request->title;
      $data->status=$request->status ?? 1;
      if ($data->save()) {
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('namod-cms::menu.saved'),]]);
        }else{
          return redirect('menus/edit/'.$data->id)->with('success', __('namod-cms::menu.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('namod-cms::menu.notsaved'),]]);
        }else{
          return redirect('menus')->with('error', __('namod-cms::menu.notsaved'));
        }
      }
    }
    return view('namod-cms::menus.create',compact('model'));
  }

  /**
  * Update status of model
  *
  * @param  \NaeemAwan\CMS\Models\Menus  $menus
  * @return \Illuminate\Http\Response
  */
  public function status(Request $request, $id)
  {
    $model = Menus::where('id', $id)->first();
    if($model->status==0){
      $status=1;
    }else{
      $status=0;
    }
    DB::table($model->getTable())
    ->where('id', $model->id)
    ->update(['status' => $status]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.statusUpdated'),]]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \NaeemAwan\CMS\Models\Menus  $menus
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, $id)
  {
    $model = Menus::where('id', $id)->first();
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'id'=>'required',
        'title'=>'required',
        'status'=>'required|integer',
      ]);

      $data= Menus::find($request->id);
      $data->title=$request->title;
      if(isset($request->status)){
        $data->status=$request->status;
      }
      $data->updated_at = date("Y-m-d H:i:s");
      if ($data->update()) {
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('namod-cms::menu.saved'),]]);
        }else{
          return redirect('menus')->with('success', __('namod-cms::menu.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('namod-cms::menu.notsaved'),]]);
        }else{
          return redirect('menus')->with('error', __('namod-cms::menu.notsaved'));
        }
      }
    }
    return view('namod-cms::menus.update', [
      'model'=>$model,
    ]);
  }

  public function addItem(Request $request)
  {
    $canAdd=false;
    if($request->input('mtype')=='page' && $request->input('menu_id')!="" && $request->input('page_id')!=""){
      $validator = Validator::make($request->all(), [
        'mtype'=>'required|string',
        'menu_id'=>'required|integer',
        'page_id'=>'required|integer',
      ]);
      $canAdd=true;
    }
    if($request->input('mtype')=='link' && $request->input('menu_id')>0 && $request->input('title')!="" && $request->input('url')!=""){
      $validator = Validator::make($request->all(), [
        'mtype'=>'required|string',
        'menu_id'=>'required|integer',
        'title'=>'required|string',
        'url'=>'required|string',
        'icon'=>'string',
        'css_class'=>'string',
        'target'=>'string',
      ]);
      $canAdd=true;
    }
    if($canAdd==true){
      $menuItem = new MenuItems;
      $menuItem->menu_id=$request->input('menu_id');
      $menuItem->parent_id=0;
      if($request->input('mtype')=='page'){
        $page = Pages::where('id',$request->input('page_id'))->first();
        if($page!=null){
          $menuItem->reference_type='NaeemAwan\CMS\Models\Pages';
          $menuItem->reference_id=$request->input('page_id');
          $menuItem->title=$page->lang->title;
        }
      }
      if($request->input('mtype')=='link'){
        $menuItem->reference_type='Link';
        $menuItem->url=$request->input('url');
        $menuItem->icon_font=$request->input('icon');
        $menuItem->title=$request->input('title');
        $menuItem->css_class=$request->input('css_class');
      }
      $menuItem->target=$request->input('target');
      $menuItem->has_child=0;
      if($menuItem->save()){
        echo generateMenuItemHtml($menuItem);
      }
    }else{
      echo "";
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \NaeemAwan\CMS\Models\Menus  $menus
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $model= Menus::where('id',$id)->first();
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('namod-cms::menu.deleted'),]]);
  }
}

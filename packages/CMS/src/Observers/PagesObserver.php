<?php

namespace NaeemAwan\CMS\Observers;

use ListHelper;
use NaeemAwan\CMS\Models\Pages;
use NaeemAwan\CMS\Models\PagesDetail;
use Illuminate\Support\Str;

class PagesObserver
{
  /**
  * Handle the Pages "created" event.
  *
  * @param  \App\Models\Pages  $pages
  * @return void
  */
  public function created(Pages $pages)
  {
    if ($pages->id) {
      $sysLangs = getSystemLanguages();
      foreach ($sysLangs as $key=>$val) {
        if(isset($pages->title[$key])){
          $title = $pages->title[$key];
          $descp = isset($pages->descp[$key]) ? $pages->descp[$key] : '';
          $meta_title = isset($pages->meta_title[$key]) ? $pages->meta_title[$key] : '';
          $meta_descp = isset($pages->meta_descp[$key]) ? $pages->meta_descp[$key] : '';
          $meta_keyword = isset($pages->meta_keyword[$key]) ? $pages->meta_keyword[$key] : '';
          $langDetail = PagesDetail::where('page_id',$pages->id)->where('lang',$key)->first();
          if($langDetail==null){
            $langDetail = new PagesDetail();
            $langDetail->page_id = $pages->id;
            $langDetail->lang = $key;
          }
          $langDetail->slug=Str::slug($title,'-');
          $langDetail->title = $title;
          $langDetail->descp = $descp;
          $langDetail->meta_title = $meta_title;
          $langDetail->meta_descp = $meta_descp;
          $langDetail->meta_keyword = $meta_keyword;
          $langDetail->save();
        }
      }
    }
  }

  /**
  * Handle the Pages "updated" event.
  *
  * @param  \App\Models\Pages  $pages
  * @return void
  */
  public function updated(Pages $pages)
  {
    $sysLangs = getSystemLanguages();
    foreach ($sysLangs as $key=>$val) {
      if(isset($pages->title[$key])){
        $title = $pages->title[$key];
        $descp = isset($pages->descp[$key]) ? $pages->descp[$key] : '';
        $meta_title = isset($pages->meta_title[$key]) ? $pages->meta_title[$key] : '';
        $meta_descp = isset($pages->meta_descp[$key]) ? $pages->meta_descp[$key] : '';
        $meta_keyword = isset($pages->meta_keyword[$key]) ? $pages->meta_keyword[$key] : '';
        $langDetail = PagesDetail::where('page_id',$pages->id)->where('lang',$key)->first();
        if($langDetail==null){
          $langDetail = new PagesDetail();
          $langDetail->page_id = $pages->id;
          $langDetail->lang = $key;
        }
        $langDetail->title = $title;
        $langDetail->descp = $descp;
        $langDetail->meta_title = $meta_title;
        $langDetail->meta_descp = $meta_descp;
        $langDetail->meta_keyword = $meta_keyword;
        $langDetail->save();
      }
    }
  }

  /**
  * Handle the Pages "deleted" event.
  *
  * @param  \App\Models\Pages  $pages
  * @return void
  */
  public function deleted(Pages $pages)
  {
    //
  }

  /**
  * Handle the Pages "restored" event.
  *
  * @param  \App\Models\Pages  $pages
  * @return void
  */
  public function restored(Pages $pages)
  {
    //
  }

  /**
  * Handle the Pages "force deleted" event.
  *
  * @param  \App\Models\Pages  $pages
  * @return void
  */
  public function forceDeleted(Pages $pages)
  {
    //
  }
}

<?php
namespace NaeemAwan\CMS\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\CMS\Models\Pages;
use NaeemAwan\CMS\Observers\PagesObserver;

class CMSServiceProvider extends ServiceProvider
{
  public function boot()
  {
    Pages::observe(PagesObserver::class);
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'namod-cms');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'namod-cms');$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'committe-members');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/view-from-chair', 'view-from-chair');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/sponsors', 'sponsors');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/gallery', 'gallery');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/categories', 'categories');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/member-offers', 'member-offers');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/banners', 'banners');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/partners', 'partners');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'managecontent');


    $this->loadViewsFrom(__DIR__ . '/../../resources/views/frontend-views', 'frontend-views');


    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/namod-cms'),
    ],'namod-cms-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'namod-cms-migrations');

    // $this->loadViewComponentsAs('workflow', [
    //   WorkflowFields::class,
    //   KanbanBoard::class,
    //   WorkflowFunnel::class,
    // ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

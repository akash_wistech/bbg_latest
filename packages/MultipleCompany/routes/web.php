<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\MultipleCompany\Http\Controllers\MultipleCompanyController;


Route::group(['middleware' => ['web','auth']], function () {
  Route::post('/suggestion/company-users/{module}/{id}', [MultipleCompanyController::class,'companyUsers']);
  Route::post('/module-action/delete-company/{module}/{id}/{mcid}', [MultipleCompanyController::class,'deleteCompany']);
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModuleCompany extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('module_company', function (Blueprint $table) {
      $table->id();
      $table->string('module_type',15)->index();
      $table->integer('module_id')->index();
      $table->integer('company_id')->index();
      $table->integer('role_id')->index()->nullable();
      $table->integer('job_title_id')->index()->nullable();
      $table->tinyInteger('is_primary')->default(1)->nullable();
      $table->timestamps();
      $table->timestamp('deleted_at')->nullable();
      $table->integer('created_by')->nullable();
      $table->integer('updated_by')->nullable();
      $table->integer('deleted_by')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('module_company');
  }
}

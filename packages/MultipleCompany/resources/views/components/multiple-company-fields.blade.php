<div class="card card-custom card-border mb-7{{($companies!=null ? '' : ' card-collapse')}}" data-card="true">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('multicompany::multicompany.heading')}}</h3>
    </div>
    <div class="card-toolbar">
      <a href="{{url('company/create')}}" class="btn btn-icon btn-sm btn-hover-light-primary mr-1 poplink" data-heading="{{__('common.create_company')}}">
        <i class="ki ki-plus icon-nm"></i>
      </a>
			<a href="#" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{__('common.toggle')}}">
				<i class="ki ki-arrow-down icon-nm"></i>
			</a>
		</div>
  </div>
  <div id="rdcompanies" class="card-body p-2"{{($companies!=null ? '' : ' style="display: none;"')}}>
    @if($companies!=null && count($companies)>0)
      @foreach($companies as $company)
      @php
        $model->comp_row_id[$n]=$company->id;
        $model->comp_id[$n]=$company->company_id;
        $companyName = '';
        if($company->company!=null){
          $companyName = $company->company->title;
        }
        $model->comp_name[$n]=$companyName;
        $model->comp_role_id[$n]=$company->role_id;
        $model->comp_job_title_id[$n]=$company->job_title_id;
      @endphp
      <div class="d-none">
        {{ Form::text('comp_row_id['.$n.']', $model->comp_row_id[$n], ['id'=>'comp_row_id-'.$n, 'class'=>'form-control']) }}
        {{ Form::text('comp_id['.$n.']', $model->comp_id[$n], ['id'=>'comp_id-'.$n, 'class'=>'form-control is-unique-h']) }}
      </div>
      <div class="row">
        <div class="col-sm-8">
          @if($n==1)
          {{ Form::label('comp_name',__('common.comp_name')) }}
          @endif
          {{ Form::text('comp_name['.$n.']', $model->comp_name[$n], ['data-fld'=>'comp_id-'.$n,'data-ds'=>url('suggestion/company-lookup'),'class'=>'form-control cnames autocomplete','autocomplete'=>'off']) }}
        </div>
        <div class="col-sm-3 d-none">
          @if($n==1)
          {{ Form::label('comp_role_id',__('common.comp_role')) }}
          @endif
          {{ Form::select('comp_role_id['.$n.']', $contactRoles, null, ['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>__('common.select')]) }}
        </div>
        <div class="col-sm-3">
          @if($n==1)
          {{ Form::label('comp_job_title_id',__('common.comp_job_title')) }}
          @endif
          {{ Form::select('comp_job_title_id['.$n.']', $jobRoles, null, ['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>__('common.select')]) }}
        </div>
        <div class="col-sm-1 text-center">
          <div class="form-group">
            @if($n==1)
            <label class="control-label" style="display:block;">&nbsp;</label>
            @endif
            @if($n>1)
            <a class="btn btn-danger btn-sm act-confirmation" href="{{url('module-action/delete-company/'.$model->moduleTypeId.'/'.$model->id.'/'.$company->id)}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="rdcompanies">
              <i class="fas fa-trash"></i>
            </a>
            @else
            <a href="javascript:;" class="btn btn-success btn-sm" onclick="addCompany()">
              <i class="fas fa-plus"></i>
            </a>
            @endif
          </div>
        </div>
      </div>
      @php
      $n++;
      @endphp
      @endforeach
    @endif
    @if($n==1)
    <div class="row">
      <div class="d-none">
        {{ Form::text('comp_id['.$n.']',null,['id'=>'comp_id-'.$n,'class'=>'form-control is-unique-h']) }}
      </div>
      <div class="col-sm-8">
        {{ Form::label('comp_name',__('common.comp_name')) }}
        {{ Form::text('comp_name['.$n.']', null, ['data-fld'=>'comp_id-'.$n,'data-ds'=>url('suggestion/company-lookup'),'class'=>'form-control cnames autocomplete','autocomplete'=>'off']) }}
      </div>
      <div class="col-sm-3 d-none">
        {{ Form::label('comp_role_id',__('common.comp_role')) }}
        {{ Form::select('comp_role_id['.$n.']', $contactRoles, null, ['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>__('common.select')]) }}
      </div>
      <div class="col-sm-3">
        {{ Form::label('comp_job_title_id',__('common.comp_job_title')) }}
        {{ Form::select('comp_job_title_id['.$n.']', $jobRoles, null, ['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>__('common.select')]) }}
      </div>
      <div class="col-sm-1 text-center">
        <div class="form-group">
          <label class="control-label" style="display:block;">&nbsp;</label>
          <a href="javascript:;" class="btn btn-success btn-sm" onclick="addCompany()">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
    </div>
    @endif
  </div>
</div>
@push('jsScripts')
$("body").on("click", ".remove-company", function () {
  $(this).parents(".new-comp-row").remove();
});

@endpush
@push('jsFunc')
var compRows = {{$n}};
function addCompany(){
  compRows++;
  html = '';
  html+= '<div class="row new-comp-row">';
  html+= '  <div class="d-none">';
  html+= '    <input type="text" id="comp_id-'+compRows+'" class="form-control is-unique-h" name="comp_id['+compRows+']">';
  html+= '  </div>';
  html+= '  <div class="col-sm-8">';
  html+= '    <div class="form-group field-comp_name-'+compRows+'">';
  html+= '      <input type="text" id="comp_name-'+compRows+'" data-fld="comp_id-'+compRows+'" data-ds="{{url('suggestion/company-lookup')}}" class="form-control cnames autocomplete" name="comp_name['+compRows+']" autocomplete="off">';
  html+= '    </div>';
  html+= '  </div>';
  html+= '  <div class="col-sm-3 d-none">';
  html+= '    <div class="form-group field-comp_role-'+compRows+'">';
  html+= '      <select name="comp_role_id['+compRows+']" class="form-control selectpicker" data-live-search="true" title="{{__('common.select')}}">';
  html+= '      {!!$contactRolesSelectOptionsHtml!!}';
  html+= '      </select>';
  html+= '    </div>';
  html+= '  </div>';
  html+= '  <div class="col-sm-3">';
  html+= '    <div class="form-group field-comp_job_title-'+compRows+'">';
  html+= '      <select name="comp_job_title_id['+compRows+']" class="form-control selectpicker" data-live-search="true" title="{{__('common.select')}}">';
  html+= '      {!!$jobRolesSelectOptionsHtml!!}';
  html+= '      </select>';
  html+= '    </div>';
  html+= '  </div>';
  html+= '  <div class="col-sm-1 text-center">';
  html+= '    <div class="form-group">';
  html+= '      <a href="javascript:;" class="btn btn-danger btn-sm remove-company">';
  html+= '        <i class="fas fa-trash"></i>';
  html+= '      </a>';
  html+= '    </div>';
  html+= '  </div>';
  html+= '</div>';
  $("#rdcompanies").append(html);
  $('.selectpicker').selectpicker();
}
@endpush

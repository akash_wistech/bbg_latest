<?php
namespace NaeemAwan\MultipleCompany\View\Components;

use Illuminate\View\Component;

class MultipleCompanyFields extends Component
{
  /**
  * model
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  object  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $companies = getModuleSavedCompanies($this->model);
    $contactRoles = getContactRoleListArr();
    $contactRolesSelectOptionsHtml = '';
    if($contactRoles!=null){
      foreach($contactRoles as $key=>$val){
        $contactRolesSelectOptionsHtml.='<option value="'.$key.'">'.$val.'</option>';
      }
    }
    $jobRoles = JobTitlesListArr();
    $jobRolesSelectOptionsHtml = '';
    if($jobRoles!=null){
      foreach($jobRoles as $key=>$val){
        $jobRolesSelectOptionsHtml.='<option value="'.$key.'">'.$val.'</option>';
      }
    }
    $n=1;
    return view('multicompany::components.multiple-company-fields', [
      'model'=>$this->model,
      'companies'=>$companies,
      'contactRoles'=>$contactRoles,
      'contactRolesSelectOptionsHtml'=>$contactRolesSelectOptionsHtml,
      'jobRoles'=>$jobRoles,
      'jobRolesSelectOptionsHtml'=>$jobRolesSelectOptionsHtml,
      'n'=>$n,
    ]);
  }
}

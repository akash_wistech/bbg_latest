<?php
namespace Naeemawan\MultipleCompany\View\Components;

use Auth;
use Illuminate\View\Component;
use Naeemawan\MultipleCompany\Models\ModuleCompany;

class MultipleCompanyFieldDetails extends Component
{
    /**
     * card
     */
    public $card=false;

    /**
     * model
     */
    public $model;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($model,$card=false)
    {
     $this->card = $card;
     $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
        $tagDetails = [];
        $publicTags=$this->convertTags(getModuleSavedTags($this->model->moduleTypeId,$this->model->id,0),0);
        if($publicTags!=''){
          $tagDetails[]=['heading'=>__('multiemailphone::multiemailphone.public_tags'),'tags'=>$publicTags];
        }
        //Private only for created by user
        if(Auth::user()->id==$this->model->created_by){
          $privateTags=$this->convertTags(getModuleSavedTags($this->model->moduleTypeId,$this->model->id,1),1);
          if($privateTags!=''){
            $tagDetails[]=['heading'=>__('multiemailphone::multiemailphone.private_tags'),'tags'=>$privateTags];
          }
        }
        return view('multiemailphone::components.custom-tag-fields-detail', [
          'showCard' => $this->card,
          'tagDetails' => $tagDetails,
          'model'=>$this->model
        ]);
    }

    public function convertTags($tagsString,$vis)
    {
      $arr = [];
      $tagsArr = explode(",",$tagsString);
      if($tagsArr){
        foreach($tagsArr as $key=>$val){
          if($val!=''){
            $arr[]='<span class="label label-light-'.($vis==0 ? 'primary' : 'success').' label-inline mr-2">'.$val.'</span>';
          }
        }
      }
      if(count($arr)>0){
        return implode(" ",$arr);
      }else{
        return '';
      }
    }
}

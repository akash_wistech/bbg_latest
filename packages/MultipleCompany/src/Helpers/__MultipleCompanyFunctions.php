<?php
use App\Models\Company;
use App\Models\User;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;

if (!function_exists('saveMultipleCompany')) {
  /**
  * save module companies
  */
  function saveMultipleCompany($model,$request,$type='form')
  {
    if($request->comp_id!=null && count($request->comp_id)>0){
      foreach($request->comp_id as $key=>$val){
        if($request->comp_id[$key]!=null && $request->comp_id[$key]!=''){
          $companyId=$request->comp_id[$key];
          $roleId=$request->comp_role_id[$key];
          $jobTitleId=$request->comp_job_title_id[$key];

          if(isset($request->comp_row_id[$key])){
            $rowId=$request->comp_row_id[$key];
            $childRow=ModuleCompany::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'id'=>$rowId])->first();
          }else{
            $childRow=new ModuleCompany;
            $childRow->module_type=$model->moduleTypeId;
            $childRow->module_id=$model->id;
          }

          $childRow->company_id=$companyId;
          $childRow->role_id=$roleId;
          $childRow->job_title_id=$jobTitleId;
          $childRow->save();
        }
      }
    }
  }
}

if (!function_exists('getModuleSavedCompanies')) {
  /**
  * get module saved address
  */
  function getModuleSavedCompanies($model)
  {
    return ModuleCompany::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->get();
  }
}



if (!function_exists('getRowMultipleCompaniesQuery')) {
  /**
  * get module saved address
  */
  function getRowMultipleCompaniesQuery($moduleTypeId,$id)
  {
    $query = Company::whereIn('id',function($query) use ($moduleTypeId,$id){
      $query->select('company_id')
      ->from(with(new ModuleCompany)->getTable())
      ->where(['module_type'=>$moduleTypeId,'module_id'=>$id]);
    })->get();
    return $query;
  }
}

if (!function_exists('getRowMultipleCompanies')) {
  /**
  * get module saved address
  */
  function getRowMultipleCompanies($moduleTypeId,$id)
  {
    $str = '';
    $query = getRowMultipleCompaniesQuery($moduleTypeId,$id);
    if($query!=null){
      foreach($query as $result){
        $str.='<span class="badge badge-info mr-1">'.$result->title.'</span>';
      }
    }
    return $str;
  }
}

if (!function_exists('getCompanyUsersListQuery')) {
  /**
  * get company users query
  */
  function getCompanyUsersListQuery($id,$moduleTypeId)
  {
    return User::whereIn('id',function($query) use ($id, $moduleTypeId){
      $query->select('module_id')
      ->from(with(new ModuleCompany)->getTable())
      ->where(['module_type'=>$moduleTypeId,'company_id'=>$id]);
    })
    ->orderBy('name','asc')
    ->get()->toArray();
  }
}

if (!function_exists('getCompanyPrimaryUsersListQuery')) {
  /**
  * get company users query
  */
  function getCompanyPrimaryUsersListQuery($id,$moduleTypeId)
  {
    return User::whereIn('id',function($query) use ($id, $moduleTypeId){
      $query->select('module_id')
      ->from(with(new ModuleCompany)->getTable())
      ->where(['module_type'=>$moduleTypeId,'company_id'=>$id,'is_primary'=>1]);
    })
    ->get()->toArray();
  }
}

if (!function_exists('getCompanyUsersList')) {
  /**
  * get company users query
  */
  function getCompanyUsersList($id,$moduleTypeId='contact')
  {
    $arr = [];
    $results = getCompanyUsersListQuery($id,$moduleTypeId);
    if($results!=null){
      foreach($results as $result){
        $arr[$result['id']] = $result['name'];
      }
    }
    return $arr;
  }
}

if (!function_exists('searchInMultipleCompanies')) {
  /**
  * get module saved address
  */
  function searchInMultipleCompanies($query,$moduleTypeId,$companyName)
  {
    $query->whereIn('id', function($query) use($moduleTypeId,$companyName){
      $query->select('module_id')
      ->from(with(new ModuleCompany)->getTable())
      ->whereIn('company_id', function($query) use($companyName){
        $query->select('id')
        ->from(with(new Company)->getTable())
        ->where('title', 'like','%'.$companyName.'%');
      })
      ->where('module_type', $moduleTypeId);
    })->get();
    return $query;
  }
}

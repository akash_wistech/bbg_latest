<?php
namespace NaeemAwan\MultipleCompany\Models;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use App\Models\Company;

class ModuleCompany extends FullTables
{
  use Blameable, SoftDeletes;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_company';

  public function company()
  {
    return $this->belongsTo(Company::class);
  }
}

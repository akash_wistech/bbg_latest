<?php

namespace NaeemAwan\MultipleCompany\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\MultipleCompany\View\Components\MultipleCompanyFields;
use NaeemAwan\MultipleCompany\View\Components\MultipleCompanyFieldDetails;

class MultipleCompanyServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'multicompany');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'multicompany');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/multicompany'),
    ],'multicompany-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'multicompany-migrations');

    $this->loadViewComponentsAs('multicompany', [
      MultipleCompanyFields::class,
      MultipleCompanyFieldDetails::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

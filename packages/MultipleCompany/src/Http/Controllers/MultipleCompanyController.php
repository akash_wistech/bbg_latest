<?php
namespace NaeemAwan\MultipleCompany\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class MultipleCompanyController extends Controller
{
  /**
  * Loads company users
  *
  * @return \Illuminate\Http\Response
  */
  public function companyUsers(Request $request,$module,$id)
  {
    $html = '';
    $results = getCompanyUsersListQuery($id,$module);
    if($results!=null){
      $html.='<option value="">'.__('common.select').'</option>';
      foreach($results as $result){
        $checked = '';
        if($request->has('oldsval') && $request->input('oldsval')==$result['id'])$checked = ' selected="selected"';
        $html.='<option value="'.$result['id'].'"'.$checked.'>';
        $html.=''.$result['name'];//.' ('.$result['email'].')';
        $html.='</option>';
      }
    }
    return new JsonResponse(['success'=>['optionsHtml'=>$html]]);
  }

  /**
  * Remove the specified company
  *
  * @return \Illuminate\Http\Response
  */
  public function deleteCompany($module,$id,$mcid)
  {
    $model = ModuleCompany::where(['module_type'=>$module,'module_id'=>$id,'id'=>$mcid])->first();
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $model->id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('rand_code',100)->nullable();
            $table->string('reference_number',100)->nullable();
            $table->integer('payment_no')->nullable()->index();
            $table->integer('company_id')->nullable()->index();
            $table->integer('user_id')->index();
            $table->date('payment_date')->nullable();
            $table->decimal('amount_received',10,2)->default(0)->nullable();
            $table->integer('payment_method')->default(0)->nullable()->index();
            $table->tinyInteger('send_notification')->default(0)->nullable()->index();
            $table->string('transaction_id',200)->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('is_completed')->default(0)->nullable()->index();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

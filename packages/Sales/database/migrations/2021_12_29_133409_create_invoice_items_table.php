<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
          $table->id();
          $table->integer('invoice_id')->index();
          $table->char('item_type',20)->index();
          $table->integer('item_id')->index();
          $table->text('title')->nullable();
          $table->longText('description')->nullable();
          $table->integer('qty')->default(1)->nullable()->index();
          $table->decimal('rate',10,2)->default(0)->nullable();
          $table->decimal('discount',10,2)->default(0)->nullable();
          $table->char('discount_type',10);
          $table->decimal('discount_amount',10,2)->default(0)->nullable();
          $table->text('discount_comments')->nullable();
          $table->integer('tax_id')->default(0)->nullable();
          $table->decimal('tax_amount',10,2)->default(0)->nullable();
          $table->decimal('total',10,2)->default(0)->nullable();
          $table->timestamps();
          $table->timestamp('deleted_at')->nullable();
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();
          $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}

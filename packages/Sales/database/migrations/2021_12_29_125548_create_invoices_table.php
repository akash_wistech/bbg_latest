<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('p_reference_number',100)->nullable();
            $table->integer('p_invoice_no')->nullable()->index();
            $table->string('reference_number',100)->nullable();
            $table->integer('invoice_no')->nullable()->index();
            $table->integer('company_id')->nullable()->index();
            $table->integer('user_id')->index();
            $table->tinyInteger('invoice_type')->default(0)->nullable();
            $table->date('p_invoice_date')->nullable();
            $table->date('invoice_date')->nullable();
            $table->date('due_date')->nullable();
            $table->tinyInteger('currency_id')->default(0)->nullable()->index();
            $table->tinyInteger('is_recurring')->default(0)->nullable()->index();
            $table->char('discount_calculation',10)->nullable();
            $table->longText('admin_note')->nullable();
            $table->tinyInteger('over_due_reminder')->default(1)->nullable()->index();

            $table->decimal('sub_total',10,2)->default(0)->nullable();
            // $table->char('discount_type',10)->default('percentage')->nullable();
            // $table->decimal('discount',10,2)->default(0)->nullable();
            // $table->decimal('adjustment',10,2)->default(0)->nullable();
            $table->decimal('grand_total',10,2)->default(0)->nullable();
            $table->tinyInteger('payment_status')->default(0)->nullable()->index();

            $table->longText('client_note')->nullable();
            $table->longText('terms_text')->nullable();
            $table->timestamps('sent_date_time')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}

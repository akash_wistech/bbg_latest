<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimations', function (Blueprint $table) {
             $table->id();
            $table->string('subject')->nullable();
            $table->string('module_type')->nullable();
            $table->integer('module_id')->nullable();
            $table->string('module_name')->nullable();
            $table->date('date')->nullable();
            $table->date('open_till')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->integer('assigned_to')->nullable();
            $table->string('to')->nullable();
            $table->text('address')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_no')->nullable();
            $table->decimal('sub_total', $precision = 10, $scale = 2)->nullable();
            $table->decimal('discount_value', $precision = 10, $scale = 2)->nullable();
            $table->integer('discount_type');
            $table->decimal('discount_amount', $precision = 10, $scale = 2)->nullable();
            $table->decimal('adjustment', $precision = 10, $scale = 2)->nullable();
            $table->decimal('total', $precision = 10, $scale = 2)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimations');
    }
}

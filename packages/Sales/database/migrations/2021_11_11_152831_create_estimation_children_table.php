<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimationChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimation_children', function (Blueprint $table) {
             $table->integer('estimation_id');
             $table->integer('item_id');
             $table->string('item_name')->nullable();
             $table->string('item_description')->nullable();
             $table->string('item_quantity')->nullable();
             $table->string('item_price')->nullable();
             $table->string('item_tax')->nullable();
             $table->string('item_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimation_children');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('reference_number',50)->nullable();
            $table->integer('order_no')->nullable();
            $table->string('order_id',50)->nullable();
            $table->integer('company_id')->default(0)->nullable()->index();
            $table->integer('user_id')->default(0)->nullable()->index();
            $table->tinyInteger('status')->default(0)->nullable()->index();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}

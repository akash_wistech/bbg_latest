<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequiredTemplate extends Model
{
    public $moduleTypeId = 'required-template';

    use HasFactory;
}

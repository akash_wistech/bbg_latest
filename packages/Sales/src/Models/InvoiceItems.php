<?php

namespace Wisdom\Sales\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceItems extends Model
{
  use Blameable, SoftDeletes;

  protected $table = 'invoice_items';

  protected $dates = ['created_at','updated_at','deleted_at'];

  protected $fillable = [
    'invoice_id',
    'title',
    'description',
    'qty',
    'rate',
    'tax_id',
    'total',
  ];

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }
}

<?php
namespace Wisdom\Sales\Models;
use App\Models\ChildTables;

class ModuleInvoice extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_invoice';
}

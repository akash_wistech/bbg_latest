<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposalCategory extends Model
{
    protected $table = 'proposal_categories';
    public $moduleTypeId = 'proposal_categories';
     public $timestamps = false;
    use HasFactory;
}

<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $template_id,$existing_template_id;
    public $moduleTypeId = 'group';

    use HasFactory;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
        });

        self::created(function ($model) {
            // echo "ponch gya created model me";
            // echo "<pre>";
            // print_r($model);
            // echo "</pre>";
            // dd("working");
            $model->saveGroupTemplates($model);
        });

        self::updating(function ($model) {
            // echo "ponch gya updating model me";
            // echo "<pre>";
            // print_r($model);
            // echo "</pre>";
            // dd("working");
            $model->saveGroupTemplates($model);
           
        });

        self::updated(function ($model) {
            // echo "ponch gya updated model me";
            // echo "<pre>";
            // print_r($model);
            // echo "</pre>";
            // dd("working");
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            // ... code here
        });
    }

    public function saveGroupTemplates($model)
    {
         GroupChild::where('group_id', $model->id)->delete(); 
            if ($model->template_id!=null) {
                foreach ($model->template_id as $key => $value) {
                    $child = new GroupChild;
                    $child->group_id = $model->id;
                    $child->template_id = $model->template_id[$key];
                    $child->save();
                }
            }
    }

}

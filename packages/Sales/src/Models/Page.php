<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    public $moduleTypeId = 'proposalpage';
    use HasFactory;
}

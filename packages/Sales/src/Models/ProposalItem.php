<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposalItem extends Model
{
    protected $table = 'proposal_items';
    public $moduleTypeId = 'proposal_items';

    use HasFactory;
}

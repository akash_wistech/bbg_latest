<?php
namespace Wisdom\Sales\Models;

use App\Models\ChildTables;

class OrderItems extends ChildTables
{
    protected $table = 'order_items';

    /**
    * Get Order
    */
    public function order()
    {
      return $this->belongsTo(Order::class,'order_id');
    }

    /**
    * Get Invoice
    */
    public function invoice()
    {
      return $this->belongsTo(Invoices::class,'invoice_id');
    }
}

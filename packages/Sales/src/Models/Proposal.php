<?php

namespace Wisdom\Sales\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
        protected $table = 'proposals';
    public $moduleTypeId = 'proposal';
    use HasFactory;
}

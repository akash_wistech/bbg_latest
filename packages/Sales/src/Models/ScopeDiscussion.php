<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScopeDiscussion extends Model
{
    use HasFactory;
}

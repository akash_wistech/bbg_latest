<?php

namespace Wisdom\Sales\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use App\Models\User;
use App\Models\Contact;
use App\Models\Prospect;
use App\Models\Company;
use Wisdom\Event\Models\EventSubscription;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoices extends Model
{
  public $customer_name,$inv_item_id,$item_type,$item_id,$title,$descp,$qty,$rate,$discount,$discount_type,$discount_comments,$tax_id,$old_invoice_item_id;
  use Blameable, SoftDeletes;

  protected $table = 'invoices';

  protected $dates = ['sent_date_time','created_at','updated_at','deleted_at'];

  public $moduleTypeId = 'invoice';

  protected $fillable = [
    'p_reference_number',
    'p_invoice_no',
    'reference_number',
    'invoice_no',
    'company_id',
    'user_id',
    'invoice_type',
    'p_invoice_date',
    'invoice_date',
    'due_date',
    'currency_id',
    'is_recurring',
    'discount_calculation',
    'admin_note',
    'over_due_reminder',
    'title',
    'descp',
    'qty',
    'rate',
    'tax_id',
    'sub_total',
    // 'discount_type',
    // 'discount',
    // 'adjustment',
    'grand_total',
    'client_note',
    'terms_text',
  ];

  /**
  * Get Items.
  */
  public function items()
  {
    return $this->hasMany(InvoiceItems::class,'invoice_id');
  }

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }

  /**
  * Get User
  */
  public function user()
  {
    return $this->belongsTo(Contact::class,'user_id');
  }

  public function prospect()
  {
    return $this->belongsTo(Prospect::class,'user_id');
  }

  /**
  * Get Company
  */
  public function company()
  {
    return $this->belongsTo(Company::class,'company_id');
  }

  /**
  * Get Bill to email
  */
  public function getBillToEmailAttribute()
  {
    $str = '';
    if($this->invoice_type==1  || $this->invoice_type==2){
      if ($this->usertype=='contact' || $this->usertype=='') {
        $str = $this->user!=null ? $this->user->email : '';
      }
      if ($this->usertype=='prospect') {
        $str = $this->prospect!=null ? $this->prospect->email : '';
      }
    }
    return $str;
  }

  /**
  * Get Bill to name
  */
  public function getBillToNameAttribute()
  {
    $str = '';
    if($this->invoice_type==1  || $this->invoice_type==2){
      if ($this->usertype=='contact' || $this->usertype=='') {
        $str = $this->user!=null ? $this->user->name : '';
          if ($this->user<>null) {
            if ($this->user->bill_to_name<>null) {
              $str=$this->user->bill_to_name;
            }
        }
      }
      if ($this->usertype=='prospect') {
        $str = $this->prospect!=null ? $this->prospect->full_name : '';
      }
    }
    // else{
    //   $moduleInvoiceRow = ModuleInvoice::where('invoice_id',$this->id)->where('module_type','contact')->first();
    //   if($moduleInvoiceRow!=null){
    //     // $eventSubscriberRow = EventSubscription::where('id',$moduleInvoiceRow->module_id)->first();
    //     $contact = Contact::where('id',$moduleInvoiceRow->module_id)->first();
    //     if($contact!=null){
    //       $str = $contact->name;
    //     }
    //   }
    // }
    return $str;
  }

  /**
  * Get Bill to company
  */
  public function getBillToCompanyNameAttribute()
  {
    $str = '';
    if($this->invoice_type==1 || $this->invoice_type==2){
      if ($this->usertype=='contact' || $this->usertype=='') {
      $str=$this->company!=null ? $this->company->title : '';
        if ($this->user<>null) {
            if ($this->user->bill_to_company<>null) {
              $str=$this->user->bill_to_company;
            }

        }
    }
    if ($this->usertype=='prospect') {
      $str = $this->prospect!=null ? $this->prospect->company_name : '';
    }
  }
    // else{
    //   $moduleInvoiceRow = ModuleInvoice::where('invoice_id',$this->id)->first();
    //   if($moduleInvoiceRow!=null){
    //     $eventSubscriberRow = EventSubscription::where('id',$moduleInvoiceRow->module_id)->first();
    //     if($eventSubscriberRow!=null){
    //       $str = $eventSubscriberRow->guest_company_name;
    //     }
    //   }
    // }
    return $str;
  }

  /**
  * Get Bill to company address
  */
  public function getBillToCompanyAddressAttribute()
  {
    $str = '';
    if($this->invoice_type==1 || $this->invoice_type==2){
        if ($this->usertype=='contact' || $this->usertype=='') {
            $str=$this->company!=null ? $this->company->address : '';
        if ($this->user<>null) {
        if ($this->user->bill_to_address<>null) {
            $str=$this->user->bill_to_address;
          }
        }
    }
    if ($this->usertype=='prospect') {
      $str ='---';
    }
  }
    // else{
    //   $moduleInvoiceRow = ModuleInvoice::where('invoice_id',$this->id)->first();
    //   if($moduleInvoiceRow!=null){
    //     $eventSubscriberRow = EventSubscription::where('id',$moduleInvoiceRow->module_id)->first();
    //     if($eventSubscriberRow!=null){
    //       $str = $eventSubscriberRow->guest_company_address;
    //     }
    //   }
    // }
    return $str;
  }

  /**
  * Get Bill to user link
  */
  public function getBillToUserLinkAttribute()
  {
    $str = '';
    if($this->invoice_type==1){
      $str = url('/contact/view/'.$this->user_id);
    }else{
      $moduleInvoiceRow = ModuleInvoice::where('invoice_id',$this->id)->first();
      if($moduleInvoiceRow!=null){
        $eventSubscriberRow = EventSubscription::where('id',$moduleInvoiceRow->module_id)->first();
        if($eventSubscriberRow!=null){
          $str = $eventSubscriberRow->guest_view_link;
        }
      }
    }
    return $str;
  }

  /**
  * return amount due
  */
  public function getAmountDueAttribute()
  {
    $amountDue = $this->grand_total;
    $paidAmounts = PaymentItems::where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->sum('amount_paid');
    if($paidAmounts!=null && $paidAmounts>0){

      // echo $this->grand_total.'-'.$paidAmounts;
      // die();
      $amountDue = $this->grand_total-$paidAmounts;
    }
    return $amountDue;
  }

  /**
  * return final ref number
  */
  public function getReferenceNoAttribute()
  {
    $invRefNo = $this->p_reference_number;
    if($this->reference_number!=null && $this->reference_number){
      $invRefNo = $this->reference_number;
    }
    return $invRefNo;
  }

  /**
  * return final invoice date
  */
  public function getFinalInvoiceDateAttribute()
  {
    $invDate = $this->p_invoice_date;
    if($this->reference_number!=null && $this->reference_number){
      $invDate = $this->invoice_date;
    }
    return $invDate;
  }

  /**
  * return next proforma invoice number
  */
  public function generatePInvoiceNumber()
  {
    $id = 1;
    $result = self::orderBy('p_invoice_no','desc')->first();
    if($result!=null){
      $id = $result->p_invoice_no+1;
    }
    return $id;
  }

  /**
  * return next proforma invoice number
  */
  public function generateInvoiceNumber()
  {
    $id = 1;
    $result = self::orderBy('invoice_no','desc')->first();
    if($result!=null){
      $id = $result->invoice_no+1;
    }
    return $id;
  }

  /**
  * return invoice currency
  */
  public function getInvCurrency()
  {
    if($this->currency_id>0){
      return getCurrenciesListArr()[$this->currency_id];
    }
    return getCurrencyPre();
  }

  /**
  * return taxable sub total
  */
  public function getTaxableSubTotal()
  {
    $total = 0;
    $taxArr=getTaxArr();
    if($this->items!=null){
      foreach($this->items as $item){
        $rowTotal = $item->qty*$item->rate;
        $rowWithTaxAmount = $rowTotal;
        if($item->tax_id>0){
          $itemTax = $taxArr[$item->tax_id];
          $thisItemTax = $rowTotal * ($itemTax/100);
          $rowWithTaxAmount=$rowTotal+$thisItemTax;
        }
        $total+=$rowWithTaxAmount;
      }
    }
    // dd($total);
    return $total;
  }

  /**
  * return invoice discount
  */
  public function getInvDiscount()
  {
    $discount=0;
    $total = $this->getTaxableSubTotal();
    //Disacount on Total Amount
    if($this->discount>0){
      $discountAmt = $this->discount;
      if($discountAmt>0){
        $discountType = $this->discount_type;
        if($discountType=='fixed'){
          $discount = $discountAmt;
        }else{
          $discount = $total * ($discountAmt/100);
        }
      }
    }
    // dd($discount);
    return $discount;
  }

  /**
  * return invoice adjustment
  */
  // public function getInvAdjustment()
  // {
  //   return 0;
  // }








}

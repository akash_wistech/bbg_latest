<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposalChild extends Model
{    public $moduleTypeId = 'proposal';
    use HasFactory;
}

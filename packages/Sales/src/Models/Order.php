<?php
namespace Wisdom\Sales\Models;

use App\Models\BlameableOnlyTables;
use App\Traits\Blameable;
use App\Models\User;
use App\Models\Company;

class Order extends BlameableOnlyTables
{
  use Blameable;
  protected $table = 'order';
  public $moduleTypeId = 'order';

  public $invoiceIdz;

  /**
  * Get User
  */
  public function user()
  {
    return $this->belongsTo(User::class,'user_id');
  }

  /**
  * Get Company
  */
  public function company()
  {
    return $this->belongsTo(Company::class,'company_id');
  }

  /**
  * Get Items.
  */
  public function items()
  {
    return $this->hasMany(OrderItems::class,'order_id');
  }

  /**
  * return currency
  */
  public function getInvCurrency()
  {
    // if($this->currency_id>0){
    //   return getCurrenciesListArr()[$this->currency_id];
    // }
    return getCurrencyPre();
  }

  /**
  * return next order number
  */
  public function generateNumber()
  {
    $id = 1;
    $result = self::orderBy('order_no','desc')->first();
    if($result!=null){
      $id = $result->order_no+1;
    }
    return $id;
  }

  /**
  * Get company name
  */
  public function getCompanyNameAttribute()
  {
    $str = '';
    if($this->company_id>0){
      $str = $this->company!=null ? $this->company->title : '';
    }else{
      //Get Billing Details
    }
    return $str;
  }

  /**
  * Get company name
  */
  public function getUserNameAttribute()
  {
    $str = '';
    if($this->user_id>0){
      $str = $this->user!=null ? $this->user->full_name : '';
    }else{
      //Get Billing Details
    }
    return $str;
  }
}

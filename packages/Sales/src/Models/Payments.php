<?php

namespace Wisdom\Sales\Models;
use App;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;
use App\Models\User;
use App\Models\Company;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Payments extends Model
{
  public $customer_name,$pmt_item_id,$module,$amount_due,$discount,$amount_paid,$tax_invoice;
  use Blameable, SoftDeletes;

  protected $table = 'payments';

  protected $dates = ['created_at','updated_at','deleted_at'];

  public $moduleTypeId = 'payment';

  protected $fillable = [
    'reference_number',
    'payment_no',
    'company_id',
    'user_id',
    'payment_date',
    'amount_received',
    'payment_method',
    'send_notification',
    'transaction_id',
    'note',
    'amount_due',
    'discount',
    'amount_paid',
    'tax_invoice',
  ];

  /**
  * Get Items.
  */
  public function items()
  {
    return $this->hasMany(PaymentItems::class,'payment_id');
  }

  /**
  * Get created by.
  */
  public function createdBy()
  {
    return $this->belongsTo(User::class,'created_by');
  }

  /**
  * Get updated by.
  */
  public function updatedBy()
  {
    return $this->belongsTo(User::class,'updated_by');
  }

  /**
  * Get User
  */
  public function user()
  {
    return $this->belongsTo(User::class,'user_id');
  }

  /**
  * Get Company
  */
  public function company()
  {
    return $this->belongsTo(Company::class,'company_id');
  }

  /**
  * return next proforma invoice number
  */
  public function generatePaymentNumber()
  {
    $id = 1;
    $result = self::orderBy('payment_no','desc')->first();
    if($result!=null){
      $id = $result->payment_no+1;
    }
    return $id;
  }

  /**
  * Updates invoices
  */
  public function updateInvoices()
  {
    if($this->is_completed==1){
      if($this->items->isNotEmpty()){
        foreach($this->items as $item){
          $moduleClass = getPaymentModuleClass()[$item->module_type];
          $moduleRow = $moduleClass::where('id',$item->module_id)->first();
          if($moduleRow!=null){
            DB::table($moduleRow->getTable())
            ->where('id', $item->module_id)
            ->update([
              'payment_status' => 1,
              'updated_at' => date("Y-m-d H:i:s"),
              'updated_by' => Auth::check() ? Auth::user()->id : NULL,
            ]);

            //Convert to tax Invoice
            // if($tax_invoice==1){
            if($moduleRow->invoice_no==null || $moduleRow->invoice_no<0){
              $invoiceNo=$moduleRow->generateInvoiceNumber();
              $invReferenceNumber='INV'.'-'.sprintf('%06d', $invoiceNo);

              DB::table($moduleRow->getTable())
              ->where('id', $item->module_id)
              ->update([
                'reference_number' => $invReferenceNumber,
                'invoice_no' => $invoiceNo,
                'invoice_date' => date("Y-m-d"),
              ]);
            }
          }
        }
      }
    }
  }
}

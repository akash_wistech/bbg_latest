<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estimation extends Model
{
    protected $table = 'estimations';
    public $moduleTypeId = 'estimation';
    use HasFactory;
}

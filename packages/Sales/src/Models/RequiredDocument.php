<?php

namespace Wisdom\Sales\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RequiredDocument extends Model
{
    public $moduleTypeId = 'required-document';

    public $template_id,$description,$existingId;
    

    use HasFactory;


  

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
        });

        self::created(function ($model) {

            $model->saveTemplates($model);

                
        });

        self::updating(function ($model) {

            $model->saveTemplates($model);
    });

        self::updated(function ($model) {
        });

        self::deleting(function ($model) {
        });

        self::deleted(function ($model) {
        });
    }

    public function saveTemplates($model)
    {
            // echo "<pre>";
            // print_r($model);
            // echo "</pre>";
            // dd("working");
        
            $savedIdz = [];
            $order_by=1;
            $xxx = 1;
            if ($model->description!=null) {
                foreach ($model->description as $key => $value) {
                    // echo $key.' '.$value; echo "<br>"; die();
                   // echo "<pre>"; print_r($model->existingId); echo "</pre>"; echo "<br>"; die();
                   if (isset($model->existingId[$xxx]) && $model->existingId[$xxx] != null) {
                    // echo $xxx; 
                    $child = RequiredDocumentChild::find($model->existingId[$xxx]);
                } else {
                    // echo "helo"; die();
                    $child = new RequiredDocumentChild;
                    $child->required_document_id = $model->id;
                }
                $child->template_id = $key;
                $child->description = $value;
                $child->order_by = $order_by;
                $child->save();
                $savedIdz[$key] = $child->id;

            $order_by++;
            $xxx++;
            }
            //die();
        }

        RequiredDocumentChild::where('required_document_id', $model->id)->whereNotIn('id', $savedIdz)->delete();

        if ($model->id) {
            // echo "string"; die();
                  $reference='SD'.'-'.date('y').'-'.sprintf('%03d', $model->id);

                  DB::table($model->getTable())
                  ->where('id', $model->id)
                  ->update(['reference_number' => $reference]);
                }
    }
}

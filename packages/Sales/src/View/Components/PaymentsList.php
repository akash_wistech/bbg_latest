<?php
namespace Wisdom\Sales\View\Components;

use Illuminate\View\Component;

class PaymentsList extends Component
{
  public $req;
  public $url;
  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($req,$url)
  {
    $this->req = $req;
    $this->url = $url;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $moduleTypeId='payments';

    $gridViewColumns = getGridViewColumns($moduleTypeId);

    $dtColsArr = [];
    $advSearchCols = [];

    $dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
    $dtColsArr[]=['title'=>__('common.reference_no'),'data'=>'reference_number','name'=>'reference_number'];
    $dtColsArr[]=['title'=>__('sales::payments.payment_date'),'data'=>'payment_date','name'=>'payment_date'];
    $dtColsArr[]=['title'=>__('common.name'),'data'=>'name','name'=>'name'];

    $dtColsArr[]=['title'=>__('common.amount'),'data'=>'amount','name'=>'amount'];
    $dtColsArr[]=['title'=>__('common.completed'),'data'=>'completed','name'=>'completed'];

    $dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
    $dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

    $dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

    return view('sales::components.payment_list',[
      'moduleTypeId'=>$moduleTypeId,
      'dtColsArr'=>$dtColsArr,
      'advSearchCols'=>$advSearchCols,
    ]);
  }
}

<?php

namespace Wisdom\Sales\Observers;

use ListHelper;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\InvoiceItems;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class InvoicesObserver
{
  /**
  * Handle the Invoices "created" event.
  *
  * @param  \App\Models\Invoices  $invoice
  * @return void
  */
  public function created(Invoices $invoice)
  {
    if ($invoice->id) {
      $pinvoiceno=$invoice->generatePInvoiceNumber();
      $preference_number='PINV'.'-'.sprintf('%06d', $pinvoiceno);

      DB::table($invoice->getTable())
      ->where('id', $invoice->id)
      ->update([
        'p_reference_number' => $preference_number,
        'p_invoice_no' => $pinvoiceno
      ]);

      if(getSetting('proforma_default')==0 || ($invoice->invoice_date!=null && $invoice->invoice_date!='')){
        $invoiceNo=$invoice->generateInvoiceNumber();
        $invReferenceNumber='INV'.'-'.sprintf('%06d', $invoiceNo);

        DB::table($invoice->getTable())
        ->where('id', $invoice->id)
        ->update([
          'reference_number' => $invReferenceNumber,
          'invoice_no' => $invoiceNo,
          'invoice_date' => $invoice->p_invoice_date,
        ]);
      }

      $this->saveItemsAndCalculations($invoice);
    }
  }

  /**
  * Handle the Invoices "updated" event.
  *
  * @param  \App\Models\Invoices  $invoice
  * @return void
  */
  public function updated(Invoices $invoice)
  {
    $this->saveItemsAndCalculations($invoice);
  }

  /**
  * Handle the Invoices "deleted" event.
  *
  * @param  \App\Models\Invoices  $invoice
  * @return void
  */
  public function deleted(Invoices $invoice)
  {
    //
  }

  /**
  * Handle the Invoices "restored" event.
  *
  * @param  \App\Models\Invoices  $invoice
  * @return void
  */
  public function restored(Invoices $invoice)
  {
    //
  }

  /**
  * Handle the Invoices "force deleted" event.
  *
  * @param  \App\Models\Invoices  $invoice
  * @return void
  */
  public function forceDeleted(Invoices $invoice)
  {
    //
  }

  public function saveItemsAndCalculations($invoice)
  {
    $grandTotal = 0;
    $invSubTotal = 0;

    $taxArr = getTaxArr();

    if($invoice->title!=null){
      foreach ($invoice->title as $key=>$val) {
        $oldInvoiceItemId=0;
        if(
          isset($invoice->qty[$key]) && $invoice->qty[$key]>0
        ){
          $inv_item_id = 0;
          if(isset($invoice->inv_item_id[$key]))$inv_item_id=$invoice->inv_item_id[$key];
          $oldInvoiceItemId = isset($invoice->old_invoice_item_id[$key]) ? $invoice->old_invoice_item_id[$key]: 0;
          $item_type = $invoice->item_type[$key];
          $item_id = $invoice->item_id[$key];
          $title = $invoice->title[$key];
          $description = isset($invoice->descp[$key]) ? $invoice->descp[$key] : '';
          $qty = isset($invoice->qty[$key]) ? $invoice->qty[$key] : '';
          $rate = isset($invoice->rate[$key]) ? $invoice->rate[$key] : '';
          $discount = isset($invoice->discount[$key]) ? $invoice->discount[$key] : '';
          $discount_type = isset($invoice->discount_type[$key]) ? $invoice->discount_type[$key] : '';
          $discount_comments = isset($invoice->discount_comments[$key]) ? $invoice->discount_comments[$key] : '';
          $tax_id = isset($invoice->tax_id[$key]) ? $invoice->tax_id[$key] : '';
          $total = $qty*$rate;
          $itemDiscount = calculateDiscount($total,$discount,$discount_type);
          // $total-=$itemDiscount;
          $invSubTotal+=$total;
          $itemTaxableAmount = $total;
          $itemTax=0;
          if($tax_id>0){
            $itemTaxPercent = $taxArr[$tax_id];
            $itemTax = $total * ($itemTaxPercent/100);
            $itemTaxableAmount=$total+$itemTax;
          }
          $grandTotal+=$itemTaxableAmount;
          if($inv_item_id>0){
            $item = InvoiceItems::where('invoice_id',$invoice->id)->where('id',$inv_item_id)->first();
          }else{
            $item = new InvoiceItems();
            $item->invoice_id = $invoice->id;
          }
          $item->item_type = $item_type;
          $item->item_id = $item_id;
          $item->title = $title;
          $item->description = $description;
          $item->qty = $qty;
          $item->rate = $rate;
          $item->discount = $discount;
          $item->discount_type = $discount_type;
          $item->discount_amount = $itemDiscount;
          $item->discount_comments = $discount_comments;
          $item->tax_id = $tax_id;
          $item->tax_amount = $itemTax;
          $item->total = $total;
          $item->old_invoice_item_id = $oldInvoiceItemId;
          $item->save();
        }
      }
    }

    DB::table($invoice->getTable())
    ->where('id', $invoice->id)
    ->update([
      'sub_total' => $invSubTotal,
      'grand_total' => $grandTotal
    ]);
  }
}

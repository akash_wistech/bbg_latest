<?php

namespace Wisdom\Sales\Observers;

use ListHelper;
use Wisdom\Sales\Models\Payments;
use Wisdom\Sales\Models\PaymentItems;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Auth;

class PaymentsObserver
{
  /**
  * Handle the Payments "created" event.
  *
  * @param  \App\Models\Payments  $payment
  * @return void
  */
  public function created(Payments $payment)
  {
    if ($payment->id) {
      $no=$payment->generatePaymentNumber();
      $reference_number='PMT'.'-'.sprintf('%06d', $no);

      $randCode = generateRandomString().$payment->id;

      DB::table($payment->getTable())
      ->where('id', $payment->id)
      ->update([
        'reference_number' => $reference_number,
        'payment_no' => $no,
        'rand_code' => $randCode
      ]);
      $payment->rand_code = $randCode;

      if($payment->module!=null){
        foreach ($payment->module as $key=>$val) {
          list($module_type,$module_id) = explode("-",$val);
          if(
            isset($payment->amount_paid[$module_type][$module_id]) && $payment->amount_paid[$module_type][$module_id]>0
          ){
            $amount_due = $payment->amount_due[$module_type][$module_id];
            $discount = $payment->discount[$module_type][$module_id];
            $amount_paid = $payment->amount_paid[$module_type][$module_id];
            // $tax_invoice = $payment->tax_invoice[$module_type][$module_id] ?? 0;

            $item = new PaymentItems();
            $item->payment_id = $payment->id;
            $item->module_type = $module_type;
            $item->module_id = $module_id;
            $item->amount_due = $amount_due;
            $item->discount = $discount;
            $item->amount_paid = $amount_paid;
            $item->save();

            if($payment->is_completed==1){
              $moduleClass = getPaymentModuleClass()[$module_type];
              $moduleRow = $moduleClass::where('id',$module_id)->first();
              if($moduleRow!=null){
                $nowAmountDue=$moduleRow->amount_due;
                if($nowAmountDue<$moduleRow->grand_total){
                  $paymentStatus=2;
                }
                if($nowAmountDue==0){
                  $paymentStatus=1;
                }

                DB::table($moduleRow->getTable())
                ->where('id', $module_id)
                ->update([
                  'payment_status' => $paymentStatus,
                  'updated_at' => date("Y-m-d H:i:s"),
                  'updated_by' => Auth::user()->id,
                ]);

                //Convert to tax Invoice
                // if($tax_invoice==1){
                if($moduleRow->invoice_no==null || $moduleRow->invoice_no<0){
                  $invoiceNo=$moduleRow->generateInvoiceNumber();
                  $invReferenceNumber='INV'.'-'.sprintf('%06d', $invoiceNo);

                  DB::table($moduleRow->getTable())
                  ->where('id', $module_id)
                  ->update([
                    'reference_number' => $invReferenceNumber,
                    'invoice_no' => $invoiceNo,
                    'invoice_date' => date("Y-m-d"),
                  ]);
                }
              }
            }
          }
        }
      }
    }
  }

  /**
  * Handle the Payments "updated" event.
  *
  * @param  \App\Models\Payments  $payment
  * @return void
  */
  public function updated(Payments $payment)
  {
    if($payment->module!=null){
      foreach ($payment->module as $key=>$val) {
        list($module_type,$module_id) = explode("-",$val);
        if(
          isset($payment->amount_paid[$module_type][$module_id]) && $payment->amount_paid[$module_type][$module_id]>0
        ){
          $pmt_item_id = 0;
          if(isset($payment->pmt_item_id[$module_type][$module_id]))$pmt_item_id=$payment->pmt_item_id[$module_type][$module_id];
          $amount_due = $payment->amount_due[$module_type][$module_id];
          $discount = $payment->discount[$module_type][$module_id];
          $amount_paid = $payment->amount_paid[$module_type][$module_id];
          // $tax_invoice = $payment->tax_invoice[$module_type][$module_id] ?? 0;

          if($pmt_item_id>0){
            $item = PaymentItems::where('payment_id',$payment->id)->where('id',$pmt_item_id)->first();
          }else{
            $item = new PaymentItems();
            $item->payment_id = $payment->id;
          }
          $item->module_type = $module_type;
          $item->module_id = $module_id;
          $item->amount_due = $amount_due;
          $item->discount = $discount;
          $item->amount_paid = $amount_paid;
          $item->save();

          if($payment->is_completed==1){
            $moduleClass = getPaymentModuleClass()[$module_type];
            $moduleRow = $moduleClass::where('id',$module_id)->first();
            if($moduleRow!=null){
              $nowAmountDue=$moduleRow->amount_due;
              if($nowAmountDue<$moduleRow->grand_total){
                $paymentStatus=2;
              }
              if($nowAmountDue==0){
                $paymentStatus=1;
              }
              if($amount_due==($amount_paid+$discount)){
                $paymentStatus=1;
              }

              DB::table($moduleRow->getTable())
              ->where('id', $module_id)
              ->update([
                'payment_status' => $paymentStatus,
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => Auth::user()->id,
              ]);

              //Convert to tax Invoice
              // if($tax_invoice==1){
              if($moduleRow->invoice_no==null || $moduleRow->invoice_no<0){
                $invoiceNo=$moduleRow->generateInvoiceNumber();
                $invReferenceNumber='INV'.'-'.sprintf('%06d', $invoiceNo);

                DB::table($moduleRow->getTable())
                ->where('id', $module_id)
                ->update([
                  'reference_number' => $invReferenceNumber,
                  'invoice_no' => $invoiceNo,
                  'invoice_date' => date("Y-m-d"),
                ]);
              }
            }
          }
        }
      }
    }
  }

  /**
  * Handle the Payments "deleted" event.
  *
  * @param  \App\Models\Payments  $payment
  * @return void
  */
  public function deleted(Payments $payment)
  {
    //
  }

  /**
  * Handle the Payments "restored" event.
  *
  * @param  \App\Models\Payments  $payment
  * @return void
  */
  public function restored(Payments $payment)
  {
    //
  }

  /**
  * Handle the Payments "force deleted" event.
  *
  * @param  \App\Models\Payments  $payment
  * @return void
  */
  public function forceDeleted(Payments $payment)
  {
    //
  }
}

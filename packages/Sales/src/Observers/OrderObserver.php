<?php

namespace Wisdom\Sales\Observers;

use ListHelper;
use Wisdom\Sales\Models\Order;
use Wisdom\Sales\Models\OrderItems;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Auth;

class OrderObserver
{
  /**
  * Handle the Order "created" event.
  *
  * @param  \App\Models\Order  $order
  * @return void
  */
  public function created(Order $order)
  {
    if ($order->id) {
      $no=$order->generateNumber();
      $reference_number='ORD'.'-'.sprintf('%06d', $no);

      $randCode = generateRandomString().$order->id;

      DB::table($order->getTable())
      ->where('id', $order->id)
      ->update([
        'reference_number' => $reference_number,
        'order_no' => $no,
        'order_id' => $randCode
      ]);
      $order->order_id = $randCode;

      $this->saveItems($order);
    }
  }

  /**
  * Handle the Order "updated" event.
  *
  * @param  \App\Models\Order  $order
  * @return void
  */
  public function updated(Order $order)
  {
    $this->saveItems($order);
  }

  /**
  * Handle the Order "deleted" event.
  *
  * @param  \App\Models\Order  $order
  * @return void
  */
  public function deleted(Order $order)
  {
    //
  }

  /**
  * Handle the Order "restored" event.
  *
  * @param  \App\Models\Order  $order
  * @return void
  */
  public function restored(Order $order)
  {
    //
  }

  /**
  * Handle the Order "force deleted" event.
  *
  * @param  \App\Models\Order  $order
  * @return void
  */
  public function forceDeleted(Order $order)
  {
    //
  }

  public function saveItems($order)
  {
    if($order->invoiceIdz!=null){
      foreach ($order->invoiceIdz as $key=>$val) {
        $item = new OrderItems();
        $item->order_id = $order->id;
        $item->invoice_id = $val;
        $item->save();
      }
    }
  }
}

<?php

use Wisdom\Sales\Models\RequiredDocument;
use Wisdom\Sales\Models\RequiredDocumentChild;
use Wisdom\Sales\Models\ScopeDiscussion;
use Wisdom\Sales\Models\GroupChild;
use Wisdom\Sales\Models\Page;

if(!function_exists('getProposalPagesList')){
  /**
  * return Proposal Pages list
  */
  function getProposalPagesList()
  {
    return Page::where('status',1)->get()->toArray();
  }
}

if(!function_exists('getProposalPagesById')){
  /**
  * return Proposal Page row
  */
  function getProposalPagesById($id)
  {
    return Page::where('id',$id)->first()->toArray();
  }
}

if(!function_exists('getProposalPagesListArr')){
  /**
  * return Proposal Pages list array
  */
  function getProposalPagesListArr()
  {
    $arr=[];
    $results = getProposalPagesList();
    if($results!=null){
      foreach($results as $result){
        $arr[$result['id']]=$result['title'];
      }
    }
    return $arr;
  }
}

if(!function_exists('getInvoicesTermsContent')){
  /**
  * return Invoices terms content
  */
  function getInvoicesTermsContent()
  {
    $str = '';
    $id = getSetting('invoice_terms_content');
    $result = getProposalPagesById($id);
    if($result!=null){
      $str = $result['description'];
    }
    return $str;
  }
}

if(!function_exists('getPaymentModuleClass')){
  /**
  * return Modules for payments
  */
  function getPaymentModuleClass()
  {
    return [
      'invoice' => '\Wisdom\Sales\Models\Invoices',
    ];
  }
}

if(!function_exists('getPaymentMethods')){
  /**
  * return Methods of payments
  */
  function getPaymentMethods()
  {
    return [
      1 => 'Bank',
      2 => 'Cash',
      3 => 'Credit Card',
      4 => 'Online',
    ];
  }
}

if(!function_exists('getIsRecurringOptArr')){
  /**
  * return YesNo array in reverse order
  */
  function getIsRecurringOptArr()
  {
    $arr = array_reverse(getYesNoArr());
    return $arr;
  }
}

if(!function_exists('getDiscountCalculationArr')){
  /**
  * return discount calculation types
  */
  function getDiscountCalculationArr()
  {
    return [
      'na' => __('sales::invoices.no_discount'),
      'beforetax' => __('sales::invoices.before_tax'),
      'aftertax' => __('sales::invoices.after_tax'),
    ];
  }
}

if(!function_exists('getPaymentStatusArr')){
  /**
  * return discount calculation types
  */
  function getPaymentStatusArr()
  {
    return [
      '0' => __('common.unpaid'),
      '1' => __('common.paid'),
      '2' => __('common.partial_paid'),
    ];
  }
}

if(!function_exists('getPaymentStatusLabelArr')){
  /**
  * return discount calculation types
  */
  function getPaymentStatusLabelArr()
  {
    return [
      '0' => '<span class="badge badge-info">'.__('common.unpaid').'</span>',
      '1' => '<span class="badge badge-success">'.__('common.paid').'</span>',
      '2' => '<span class="badge badge-danger">'.__('common.partial_paid').'</span>',
    ];
  }
}

if(!function_exists('getTaxArr')){
  /**
  * return tax array
  */
  function getTaxArr()
  {
    return [
      '1' => 5.00,
      '2' => 10.00,
      '3' => 18.00,
    ];
  }
}

if(!function_exists('formatInputInvPrice')){
  /**
  * return formated invoice price
  */
  function formatInputInvPrice($invoice,$amount)
  {
    return $invoice->getInvCurrency().''.$amount;
  }
}

if(!function_exists('getSalesItems')){
  /**
  * return Items List
  */
  function getSalesItems()
  {
    $items=[];
    $service_list_id = getSetting('service_list_id');
    $services = getPredefinedListOptionsArr($service_list_id);
    if($services!=null){
      foreach($services as $key=>$val){
        $items['Services']['service-'.$key] = $val;
      }
    }
    return $items;
  }
}

if(!function_exists('getSalesItemsWithDetail')){
  /**
  * return Items List with detail
  */
  function getSalesItemsWithDetail()
  {
    $items=[];
    $service_list_id = getSetting('service_list_id');
    $services = getPredefinedListItems($service_list_id);
    if($services!=null){
      foreach($services as $service){
        $items['service-'.$service->id] = [
          'item_type'=>'service',
          'item_id'=>$service->id,
          'title'=>$service->lang->title,
          'descp'=>nl2br($service->lang->descp),
          'rate'=>$service->pricing->rate,
          'tax_id'=>$service->pricing->tax_id
        ];
      }
    }
    return $items;
  }
}

if (!function_exists('getCountryArr')) {
  /**
  * return status array
  */
  function getCountryArr()
  {
    return [
      "AF" => "Afghanistan",
      "AX" => "Aland Islands",
      "AL" => "Albania",
      "DZ" => "Algeria",
      "AS" => "American Samoa",
      "AD" => "Andorra",
      "AO" => "Angola",
      "AI" => "Anguilla",
      "AQ" => "Antarctica",
      "AG" => "Antigua and Barbuda",
      "AR" => "Argentina",
      "AM" => "Armenia",
      "AW" => "Aruba",
      "AU" => "Australia",
      "AT" => "Austria",
      "AZ" => "Azerbaijan",
      "BS" => "Bahamas",
      "BH" => "Bahrain",
      "BD" => "Bangladesh",
      "BB" => "Barbados",
      "BY" => "Belarus",
      "BE" => "Belgium",
      "BZ" => "Belize",
      "BJ" => "Benin",
      "BM" => "Bermuda",
      "BT" => "Bhutan",
      "BO" => "Bolivia",
      "BQ" => "Bonaire, Sint Eustatius and Saba",
      "BA" => "Bosnia and Herzegovina",
      "BW" => "Botswana",
      "BV" => "Bouvet Island",
      "BR" => "Brazil",
      "IO" => "British Indian Ocean Territory",
      "BN" => "Brunei Darussalam",
      "BG" => "Bulgaria",
      "BF" => "Burkina Faso",
      "BI" => "Burundi",
      "KH" => "Cambodia",
      "CM" => "Cameroon",
      "CA" => "Canada",
      "CV" => "Cape Verde",
      "KY" => "Cayman Islands",
      "CF" => "Central African Republic",
      "TD" => "Chad",
      "CL" => "Chile",
      "CN" => "China",
      "CX" => "Christmas Island",
      "CC" => "Cocos (Keeling) Islands",
      "CO" => "Colombia",
      "KM" => "Comoros",
      "CG" => "Congo",
      "CD" => "Congo, Democratic Republic of the Congo",
      "CK" => "Cook Islands",
      "CR" => "Costa Rica",
      "CI" => "Cote D'Ivoire",
      "HR" => "Croatia",
      "CU" => "Cuba",
      "CW" => "Curacao",
      "CY" => "Cyprus",
      "CZ" => "Czech Republic",
      "DK" => "Denmark",
      "DJ" => "Djibouti",
      "DM" => "Dominica",
      "DO" => "Dominican Republic",
      "EC" => "Ecuador",
      "EG" => "Egypt",
      "SV" => "El Salvador",
      "GQ" => "Equatorial Guinea",
      "ER" => "Eritrea",
      "EE" => "Estonia",
      "ET" => "Ethiopia",
      "FK" => "Falkland Islands (Malvinas)",
      "FO" => "Faroe Islands",
      "FJ" => "Fiji",
      "FI" => "Finland",
      "FR" => "France",
      "GF" => "French Guiana",
      "PF" => "French Polynesia",
      "TF" => "French Southern Territories",
      "GA" => "Gabon",
      "GM" => "Gambia",
      "GE" => "Georgia",
      "DE" => "Germany",
      "GH" => "Ghana",
      "GI" => "Gibraltar",
      "GR" => "Greece",
      "GL" => "Greenland",
      "GD" => "Grenada",
      "GP" => "Guadeloupe",
      "GU" => "Guam",
      "GT" => "Guatemala",
      "GG" => "Guernsey",
      "GN" => "Guinea",
      "GW" => "Guinea-Bissau",
      "GY" => "Guyana",
      "HT" => "Haiti",
      "HM" => "Heard Island and Mcdonald Islands",
      "VA" => "Holy See (Vatican City State)",
      "HN" => "Honduras",
      "HK" => "Hong Kong",
      "HU" => "Hungary",
      "IS" => "Iceland",
      "IN" => "India",
      "ID" => "Indonesia",
      "IR" => "Iran, Islamic Republic of",
      "IQ" => "Iraq",
      "IE" => "Ireland",
      "IM" => "Isle of Man",
      "IL" => "Israel",
      "IT" => "Italy",
      "JM" => "Jamaica",
      "JP" => "Japan",
      "JE" => "Jersey",
      "JO" => "Jordan",
      "KZ" => "Kazakhstan",
      "KE" => "Kenya",
      "KI" => "Kiribati",
      "KP" => "Korea, Democratic People's Republic of",
      "KR" => "Korea, Republic of",
      "XK" => "Kosovo",
      "KW" => "Kuwait",
      "KG" => "Kyrgyzstan",
      "LA" => "Lao People's Democratic Republic",
      "LV" => "Latvia",
      "LB" => "Lebanon",
      "LS" => "Lesotho",
      "LR" => "Liberia",
      "LY" => "Libyan Arab Jamahiriya",
      "LI" => "Liechtenstein",
      "LT" => "Lithuania",
      "LU" => "Luxembourg",
      "MO" => "Macao",
      "MK" => "Macedonia, the Former Yugoslav Republic of",
      "MG" => "Madagascar",
      "MW" => "Malawi",
      "MY" => "Malaysia",
      "MV" => "Maldives",
      "ML" => "Mali",
      "MT" => "Malta",
      "MH" => "Marshall Islands",
      "MQ" => "Martinique",
      "MR" => "Mauritania",
      "MU" => "Mauritius",
      "YT" => "Mayotte",
      "MX" => "Mexico",
      "FM" => "Micronesia, Federated States of",
      "MD" => "Moldova, Republic of",
      "MC" => "Monaco",
      "MN" => "Mongolia",
      "ME" => "Montenegro",
      "MS" => "Montserrat",
      "MA" => "Morocco",
      "MZ" => "Mozambique",
      "MM" => "Myanmar",
      "NA" => "Namibia",
      "NR" => "Nauru",
      "NP" => "Nepal",
      "NL" => "Netherlands",
      "AN" => "Netherlands Antilles",
      "NC" => "New Caledonia",
      "NZ" => "New Zealand",
      "NI" => "Nicaragua",
      "NE" => "Niger",
      "NG" => "Nigeria",
      "NU" => "Niue",
      "NF" => "Norfolk Island",
      "MP" => "Northern Mariana Islands",
      "NO" => "Norway",
      "OM" => "Oman",
      "PK" => "Pakistan",
      "PW" => "Palau",
      "PS" => "Palestinian Territory, Occupied",
      "PA" => "Panama",
      "PG" => "Papua New Guinea",
      "PY" => "Paraguay",
      "PE" => "Peru",
      "PH" => "Philippines",
      "PN" => "Pitcairn",
      "PL" => "Poland",
      "PT" => "Portugal",
      "PR" => "Puerto Rico",
      "QA" => "Qatar",
      "RE" => "Reunion",
      "RO" => "Romania",
      "RU" => "Russian Federation",
      "RW" => "Rwanda",
      "BL" => "Saint Barthelemy",
      "SH" => "Saint Helena",
      "KN" => "Saint Kitts and Nevis",
      "LC" => "Saint Lucia",
      "MF" => "Saint Martin",
      "PM" => "Saint Pierre and Miquelon",
      "VC" => "Saint Vincent and the Grenadines",
      "WS" => "Samoa",
      "SM" => "San Marino",
      "ST" => "Sao Tome and Principe",
      "SA" => "Saudi Arabia",
      "SN" => "Senegal",
      "RS" => "Serbia",
      "CS" => "Serbia and Montenegro",
      "SC" => "Seychelles",
      "SL" => "Sierra Leone",
      "SG" => "Singapore",
      "SX" => "Sint Maarten",
      "SK" => "Slovakia",
      "SI" => "Slovenia",
      "SB" => "Solomon Islands",
      "SO" => "Somalia",
      "ZA" => "South Africa",
      "GS" => "South Georgia and the South Sandwich Islands",
      "SS" => "South Sudan",
      "ES" => "Spain",
      "LK" => "Sri Lanka",
      "SD" => "Sudan",
      "SR" => "Suriname",
      "SJ" => "Svalbard and Jan Mayen",
      "SZ" => "Swaziland",
      "SE" => "Sweden",
      "CH" => "Switzerland",
      "SY" => "Syrian Arab Republic",
      "TW" => "Taiwan, Province of China",
      "TJ" => "Tajikistan",
      "TZ" => "Tanzania, United Republic of",
      "TH" => "Thailand",
      "TL" => "Timor-Leste",
      "TG" => "Togo",
      "TK" => "Tokelau",
      "TO" => "Tonga",
      "TT" => "Trinidad and Tobago",
      "TN" => "Tunisia",
      "TR" => "Turkey",
      "TM" => "Turkmenistan",
      "TC" => "Turks and Caicos Islands",
      "TV" => "Tuvalu",
      "UG" => "Uganda",
      "UA" => "Ukraine",
      "AE" => "United Arab Emirates",
      "GB" => "United Kingdom",
      "US" => "United States",
      "UM" => "United States Minor Outlying Islands",
      "UY" => "Uruguay",
      "UZ" => "Uzbekistan",
      "VU" => "Vanuatu",
      "VE" => "Venezuela",
      "VN" => "Viet Nam",
      "VG" => "Virgin Islands, British",
      "VI" => "Virgin Islands, U.s.",
      "WF" => "Wallis and Futuna",
      "EH" => "Western Sahara",
      "YE" => "Yemen",
      "ZM" => "Zambia",
      "ZW" => "Zimbabwe"
    ];
  }
}

if (!function_exists('getPlaceHolder')) {
  /**
  * return Dropdown Placeholder
  */
  function getPlaceHolder()
  {
    return "Please select ...";
  }
}


if (!function_exists('gettemplatesArr')) {
  /**
  * return Dropdown Placeholder
  */
  function gettemplatesArr()
  {
    $templatesArr = DB::table('required_templates')->get()->pluck('title', 'id')->toArray();
    // echo "<pre>"; print_r($templatesArr); echo "</pre>"; die();
    return $templatesArr;
  }
}



if (!function_exists('getScopeStatusArr')) {
  /**
  * return status array
  */
  function getScopeStatusArr()
  {
    return [
      '1' => __('scopedocument::scopedocument.pending'),
      '2' => __('scopedocument::scopedocument.rejected'),
      '3' => __('scopedocument::scopedocument.approved'),
    ];
  }
}



if (!function_exists('getRequiredDocumentData')) {
  /**
  * return getRequiredDocumentData
  */
  function getRequiredDocumentData($id)
  {
    $parentRow = RequiredDocument::find($id);
    $childRows = RequiredDocumentChild::
    select(['description','template_id','required_document_id'])
    ->where(['required_document_id'=>$id])
    ->get();

    return[
      'parentRow'=>$parentRow,
      'childRows'=>$childRows,
    ];

  }
}




if (!function_exists('getDiscussionArr')) {
  /**
  * return getDiscussion Array
  */
  function getDiscussionArr($id)
  {
    return ScopeDiscussion::select(['message'])->where(['required_document_id'=>$id])->orderBy('created_at')->get();
  }
}


if (!function_exists('getExistingTempGroup')) {
  /**
  * return status array
  */
  function getExistingTempGroup($id)
  {
    return GroupChild::select('template_id')
    ->where(['group_id'=>$id])
    ->get()
    ->pluck('template_id', 'template_id')
    ->toArray();
  }
}

if (!function_exists('getExistingTempForDocs')) {
  /**
  * return getExistingTempForDocs array
  */
  function getExistingTempForDocs($id)
  {
    return DB::table('required_document_children')
    ->where('required_document_id',$id)
    ->orderBy('order_by')
    ->get();
  }
}

if (!function_exists('getEstimationStatus')) {
  /**
  * return Status
  */
  function getEstimationStatus()
  {

    $status = [
      '0'=>'Sent',
      '1'=>'Open',
      '2'=>'Recieved',
      '3'=>'Rejected',
      '4'=>'Accepted',
    ];
    return $status;
  }
}
if (!function_exists('getEstimationStatusLabel')) {
  /**
  * return Status
  */
  function getEstimationStatusLabel()
  {

    $status = [
      '0' => '<span class="label label-inline label-light-primary font-weight-bold">Sent</span>',
      '1' => '<span class="label label-inline label-light-warning font-weight-bold">Open</span>',
      '2' => '<span class="label label-inline label-light-info font-weight-bold">Recieved</span>',
      '3' => '<span class="label label-inline label-light-danger font-weight-bold">Rejected</span>',
      '4' => '<span class="label label-inline label-light-success font-weight-bold">Accepted</span>',
    ];
    return $status;
  }
}



if (!function_exists('getStaff')) {
  /**
  * return staff Member
  */
  function getStaff()
  {

    $staff = \NaeemAwan\StaffManager\Models\Staff::where('user_type', 10)->pluck('name', 'id')->toArray();

    return $staff;
  }
}


if (!function_exists('getProductServies')) {
  /**
  * return Status
  */
  function getProductServies()
  {




    $services=  DB::table('predefined_list')
    ->join('predefined_list_detail', 'predefined_list.id', '=' , 'predefined_list_detail.list_id')
    // ->leftJoin('predefined_list_financial_information', 'predefined_list.id', '=' , 'predefined_list_financial_information.predefinedlist_id')
    ->select('predefined_list.id', 'predefined_list_detail.title', 'predefined_list.parent_id')
    ->where('predefined_list.parent_id', 16)
    ->pluck('predefined_list_detail.title', 'predefined_list.id')
    ->toArray();



    $products=  DB::table('predefined_list')
    ->join('predefined_list_detail', 'predefined_list.id', '=' , 'predefined_list_detail.list_id')
    ->select('predefined_list.id', 'predefined_list_detail.title', 'predefined_list.parent_id')
    ->where('predefined_list.parent_id', 115)
    ->pluck('predefined_list_detail.title', 'predefined_list.id')
    ->toArray();

    $data['Products']= $products;
    $data['Services']= $services;

    return $data;
  }
}

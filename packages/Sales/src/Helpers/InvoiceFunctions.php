<?php
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Sales\Models\Order;
use Wisdom\Sales\Models\OrderItems;
use App\Models\Contact;
use App\Models\Prospect;
use Wisdom\Sales\Models\InvoiceItems;

if(!function_exists('getInvoicesTermsContent')){
  /**
  * return Invoices terms content
  */
  function getInvoicesTermsContent()
  {
    $str = '';
    $id = getSetting('invoice_terms_content');
    $result = getProposalPagesById($id);
    if($result!=null){
      $str = $result['description'];
    }
    return $str;
  }
}

if(!function_exists('getPaymentModuleClass')){
  /**
  * return Modules for payments
  */
  function getPaymentModuleClass()
  {
    return [
      'invoice' => '\Wisdom\Sales\Models\Invoices',
    ];
  }
}

if(!function_exists('getPaymentMethods')){
  /**
  * return Methods of payments
  */
  function getPaymentMethods()
  {
    return [
      1 => 'Bank',
      2 => 'Cash',
      3 => 'Credit Card',
      4 => 'Online',
    ];
  }
}

if(!function_exists('getPaymentMehtodLabelArr')){
  /**
  * return discount calculation types
  */
  function getPaymentMehtodLabelArr()
  {
    return [
      '1' => '<span class="badge badge-info mr-3">Bank</span>',
      '2' => '<span class="badge badge-success mr-3">Cash</span>',
      '3' => '<span class="badge badge-warning mr-3">Credit Card</span>',
      '4' => '<span class="badge badge-primary mr-3">Online</span>',
    ];
  }
}

if(!function_exists('getIsRecurringOptArr')){
  /**
  * return YesNo array in reverse order
  */
  function getIsRecurringOptArr()
  {
    $arr = array_reverse(getYesNoArr());
    return $arr;
  }
}

if(!function_exists('getDiscountCalculationArr')){
  /**
  * return discount calculation types
  */
  function getDiscountCalculationArr()
  {
    return [
      'na' => __('sales::invoices.no_discount'),
      'beforetax' => __('sales::invoices.before_tax'),
      'aftertax' => __('sales::invoices.after_tax'),
    ];
  }
}

if(!function_exists('getPaymentStatusArr')){
  /**
  * return discount calculation types
  */
  function getPaymentStatusArr()
  {
    return [
      '0' => __('common.unpaid'),
      '1' => __('common.paid'),
      '2' => __('common.partial_paid'),
      '3' => __('common.cancelled'),
      '4' => __('common.refunded'),
    ];
  }
}

if(!function_exists('getPaymentStatusLabelArr')){
  /**
  * return discount calculation types
  */
  function getPaymentStatusLabelArr()
  {
    return [
      '0' => '<span class="badge badge-info mr-3">'.__('common.unpaid').'</span>',
      '1' => '<span class="badge badge-success mr-3">'.__('common.paid').'</span>',
      '2' => '<span class="badge badge-danger mr-3">'.__('common.partial_paid').'</span>',
    ];
  }
}

if(!function_exists('getTaxArr')){
  /**
  * return tax array
  */
  function getTaxArr()
  {
    return [
      '1' => 5.00,
      '2' => 10.00,
      '3' => 18.00,
    ];
  }
}

if(!function_exists('formatInputInvPrice')){
  /**
  * return formated invoice price
  */
  function formatInputInvPrice($invoice,$amount)
  {
    return $invoice->getInvCurrency().''.$amount;
  }
}

if(!function_exists('formatInputInvPriceCol')){
  /**
  * return formated invoice price
  */
  function formatInputInvPriceCol($paymentId,$amount)
  {
    return $amount;
  }
}

if(!function_exists('formatInvPrice')){
  /**
  * return formated invoice price
  */
  function formatInvPrice($invoice,$amount)
  {
    return '<span class="cursign">'.$invoice->getInvCurrency().'</span><span class="amt">'.$amount.'</span>';
  }
}

if(!function_exists('getSalesItems')){
  /**
  * return Items List
  */
  function getSalesItems()
  {
    $items=[];
    $service_list_id = getSetting('service_list_id');
    $services = getPredefinedListOptionsArr($service_list_id);
    if($services!=null){
      foreach($services as $key=>$val){
        $items['Services']['service-'.$key] = $val;
      }
    }
    return $items;
  }
}

if(!function_exists('getSalesItemsWithDetail')){
  /**
  * return Items List with detail
  */
  function getSalesItemsWithDetail()
  {
    $items=[];
    $service_list_id = getSetting('service_list_id');
    $services = getPredefinedListItems($service_list_id);
    if($services!=null){
      foreach($services as $service){
        $items['service-'.$service->id] = [
          'item_type'=>'service',
          'item_id'=>$service->id,
          'title'=>$service->lang->title,
          'descp'=>nl2br($service->lang->descp),
          'rate'=>$service->pricing->rate,
          'tax_id'=>$service->pricing->tax_id
        ];
      }
    }
    return $items;
  }
}


if(!function_exists('getInvoiceTypesListArr')){
  /**
  * return Invoice Types
  */
  function getInvoiceTypesListArr()
  {
    return [
      '1' => 'Member',
      '2' => 'Event',
      '3' => 'Renewal',
    ];
  }
}


if(!function_exists('generateEventSubscriptionInvoice')){
  /**
  * generated event subscriber invoice
  */
  function generateEventSubscriptionInvoice($model,$event)
  {
    // yahan $model jo ha wo event subscription ha
    $guestType = $model->user_type;
    $guestModuleId = $model->user_id;

    $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->first();
      // dd($moduleInvoiceRow);
    if($moduleInvoiceRow==null){
      $invoice = new Invoices;
      $invoice->company_id = $model->company_id!=null ? $model->company_id : 0;
      if ($model->user_type_relation=='guest') {
          $invoice->usertype = 'prospect';
      }
      $invoice->user_id = $model->user_id;
      $invoice->invoice_type = 2;
      $invoice->p_invoice_date = date("Y-m-d");
      $invoice->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($invoice->p_invoice_date))));
      $invoice->currency_id = getSetting('currency');
      $invoice->is_recurring = 0;
      $invoice->discount_calculation = 'aftertax';

      //Event Fee Calculation
      $eventTaxId = $event->tax_id;
      $eventFees = getEventFeesByUserTypesArr($event->id);

      $item_type[] = 'event-subscription';
      $item_id[] = $model->id;
      $title[] = 'Registration for '.$model->guest_name.' ('.$event->title.')';
      $descp[] = '';
      $qty[] = 1;
      $rate[] = $eventFees[$model->user_type_relation];
      $discount[] = 0;
      $discount_type[] = 'percentage';
      $tax_id[] = $eventTaxId;


      $invoice->item_type = $item_type;
      $invoice->item_id = $item_id;
      $invoice->title = $title;
      $invoice->descp = $descp;
      $invoice->qty = $qty;
      $invoice->rate = $rate;
      $invoice->discount = $discount;
      $invoice->discount_type = $discount_type;
      $invoice->tax_id = $tax_id;
      $invoice->sent_date_time = \Carbon\Carbon::now();
      $invoice->terms_text = getInvoicesTermsContent();
      if($invoice->save()){
        $invoiceModule = new ModuleInvoice;
        $invoiceModule->module_type = $model->moduleTypeId;
        $invoiceModule->module_id = $model->id;
        $invoiceModule->invoice_id = $invoice->id;
        $invoiceModule->save();
        sendEmailInvoice($invoice,$emailattach=1);
      return $invoice->id;
      }
    }
  }
}

if(!function_exists('generatePaymentOrder')){
  /**
  * generate order and return id
  */
  function generatePaymentOrder($invoiceIdz)
  {
    $randomId='';
    $order = new Order;
    $order->invoiceIdz = $invoiceIdz;
    if(Auth::check()){
      $contact = Contact::where('id',Auth::user()->id)->first();
      $order->company_id = $contact->main_company_id;
      $order->user_id = $contact->id;
    }
    $order->save();
    $randomId = $order->order_id;
    return $randomId;
  }
}



if(!function_exists('generateEventSubscriptionInvoiceMultiple')){
  /**
  * generated event subscriber invoice
  */
  function generateEventSubscriptionInvoiceMultiple($subscrptionRows,$event,$primaryContact=0)
  {



    //jab login ho ga tu member otherwise ye as guest per kam kere ge
    // $model = Auth::user();
    if (Auth::user()) {
      if (Auth::user()->id ==1) {
        $model = Contact::where(['id'=>$primaryContact])->first();
          $guestType = 'contact';
            $guestModuleId = $primaryContact;
      }else {
        $model = Contact::where(['id'=>Auth::user()->id])->first();
          $guestType = 'contact';
            $guestModuleId = $model->id;
      }
    }else {
          $model = Prospect::where(['id'=>$primaryContact])->first();
            $guestType = 'prospect';
            $guestModuleId = $primaryContact;
    }
    if($subscrptionRows!=null){
      $invoice = new Invoices;
      $invoice->company_id = $model->company_id!=null ? $model->company_id : 0;
      $invoice->usertype = $guestType;
      $invoice->user_id = $model->id;
      $invoice->invoice_type = 2;
      $invoice->p_invoice_date = date("Y-m-d");
      $invoice->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($invoice->p_invoice_date))));
      $invoice->currency_id = getSetting('currency');
      $invoice->is_recurring = 0;
      $invoice->discount_calculation = 'aftertax';

      //Event Fee Calculation
      $eventTaxId = $event->tax_id;
      $eventFees = getEventFeesByUserTypesArr($event->id);

      foreach($subscrptionRows as $subscrptionRow){
        if ($guestModuleId==$subscrptionRow->user_id) {
            $event_subscription_id= $subscrptionRow->id;
        }
        $item_type[] = 'event-subscription';
        $item_id[] = $subscrptionRow->id;
        $title[] = 'Registration for '.$subscrptionRow->guest_name.' ('.$event->title.')';
        $descp[] = '';
        $qty[] = 1;
        $rate[] = $eventFees[$subscrptionRow->user_type_relation];
        $discount[] = 0;
        $discount_type[] = 'percentage';
        $tax_id[] = $eventTaxId;
      }

      $invoice->item_type = $item_type;
      $invoice->item_id = $item_id;
      $invoice->title = $title;
      $invoice->descp = $descp;
      $invoice->qty = $qty;
      $invoice->rate = $rate;
      $invoice->discount = $discount;
      $invoice->discount_type = $discount_type;
      $invoice->tax_id = $tax_id;
      $invoice->terms_text = getInvoicesTermsContent();
      if($invoice->save()){
        $invoiceModule = new ModuleInvoice;
        $invoiceModule->module_type = $subscrptionRow->moduleTypeId;
        $invoiceModule->module_id = $event_subscription_id;
        $invoiceModule->invoice_id = $invoice->id;
        $invoiceModule->save();
        sendEmailInvoice($invoice,$emailattach=1);
        return $invoice->id;
      }
    }
  }
}


if(!function_exists('getPaymentStatusEventMember')){
  /**
  * get Payment Status by Evnet Register Member from invoice
  */
  function getPaymentStatusEventMember($model)
  {
    $payment_status_id=100;
    $invoice_item =InvoiceItems::where(['item_type'=>'event-subscription','item_id'=>$model->id])->select('invoice_id')->first();
    if ($invoice_item) {
        $MemberRegisterInvoiceId =Invoices::where(['id'=>$invoice_item['invoice_id']])->select('payment_status')->first();
        $payment_status_id =$MemberRegisterInvoiceId['payment_status'];
    }
    if ($payment_status_id==1) {
      DB::table('event_subscriptions')->where('id', $model->id)->update(['status'=>'approved']);
    }
    return $payment_status_id;
  }

}

if(!function_exists('getUserFeeEventMember')){
  /**
  * get Payment Status by Evnet Register Member from invoice
  */
  function getUserFeeEventMember($model)
  {
    $fee=0;
    $invoice_item =InvoiceItems::where(['item_type'=>'event-subscription','item_id'=>$model->id])->select('rate')->first();
    if ($invoice_item) {
        $fee =$invoice_item['rate'];
    }
    return $fee;
  }

}

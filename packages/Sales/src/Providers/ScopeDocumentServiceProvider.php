<?php

namespace Wisdom\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Observers\InvoicesObserver;
use Wisdom\Sales\Models\Payments;
use Wisdom\Sales\Observers\PaymentsObserver;
use Wisdom\Sales\Models\Order;
use Wisdom\Sales\Observers\OrderObserver;
use Wisdom\Sales\View\Components\PaymentsList;

class ScopeDocumentServiceProvider extends ServiceProvider
{
  public function boot()
  {
    Invoices::observe(InvoicesObserver::class);
    Payments::observe(PaymentsObserver::class);
    Order::observe(OrderObserver::class);
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'required-document');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'required-template');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'proposal-pages');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'proposal-category');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'proposal-item');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/proposal', 'proposal');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views/estimation', 'estimation');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'group');
    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'emails');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'scopedocument');

    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

    $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'sales');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'sales');

    $this->loadViewComponentsAs('salescom', [
        PaymentsList::class,
    ]);
  }
}

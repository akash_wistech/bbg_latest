<?php

namespace Wisdom\Sales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\JsonResponse;
use Wisdom\Sales\Models\GroupChild;
use Wisdom\Sales\Models\RequiredTemplate;
use Wisdom\Sales\Models\Group;
use Wisdom\Sales\Models\RequiredDocumentChild;
use Wisdom\Sales\Models\ScopeDiscussion;
use Wisdom\Sales\Mail\MailHelper;
use Wisdom\Sales\Models\RequiredDocument;
use App\Http\Controllers\Controller;
use PDF;
use Auth;
// use Image;
// use Request;

class RequiredDocumentController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $moduleTypeId = (new RequiredDocument)->moduleTypeId;
        return view('required-document::required-document.index',compact('moduleTypeId','request'));
    }

    /**
    * returns data for datatable
    *
    * @return \Illuminate\Http\Response
    */
    public function datatableData(Request $request)
    {
        $moduleTypeId = (new RequiredDocument)->moduleTypeId;
        $query = DB::table((new RequiredDocument)->getTable())->whereNull('deleted_at');
        permissionListTypeFilter($moduleTypeId,(new RequiredDocument)->getTable(),'required_documents',$query);
        $countQuery = clone $query;
        $totalRecords = $countQuery->count('id');
        $models = $query->offset($request->start)
        ->limit($request->length)
        ->get();
        $dataArr=[];
        if($models!=null){
            foreach($models as $model){
                $statusType = getScopeStatusArr()[$model->status];
                $class = "label-light-success";
                $value="Not Set";
                if ($statusType=="Pending") {
                    $class = "label-light-info";
                    $value = "Pending";
                }
                if ($statusType=="Rejected") {
                    $class = "label-light-danger";
                    $value = "Rejected";
                }
                if ($statusType=="Approved") {
                    $class = "label-light-primary";
                    $value = "Approved";
                }
                $status ='<span class="label label-lg font-weight-bold label-inline '. $class.'">'. $value.'</span>';
                $revisions ='<span class="label label-lg font-weight-bold label-inline label-light-info">'.$model->revisions.'</span>';

                $thisData['id']               = $model->id;
                $thisData['reference_number'] = $model->reference_number;
                $thisData['subject']          = $model->subject;
                $thisData['status']           = $status;
                $thisData['module_type']      = $model->module_type;
                $thisData['module_keyword']   = $model->module_keyword;
                $thisData['date']             = $model->date;
                $thisData['open_till']        = $model->open_till;
                $thisData['revisions']        = $revisions;
                // $thisData['cb_col']='<input type="checkbox" name="selection[]" value="'.$model['id'].'">';

                //Colors column
                $colorArr = getActivityColors($moduleTypeId,$model->id);
                // dd($moduleTypeId);

                $thisData['cs_col']=implode("",$colorArr);

                $actBtns=[];
                if(checkActionAllowed('view','required-document')){
                    $actBtns[]='
                    <a href="'.url('/required-document/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" title="'.__('common.view').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-file-2"></i>
                    </a>';
                }
                if(checkActionAllowed('update','required-document')){
                    $actBtns[]='
                    <a href="'.url('/required-document/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" title="'.__('common.update').'" data-id="'.$model->id.'">
                    <i class="text-dark-50 flaticon-edit"></i>
                    </a>';
                }
                if(checkActionAllowed('delete','required-document')){
                    $actBtns[]='
                    <a href="'.url('/required-document/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                    </a>';
                }

                $thisData['action_col']=getDatatableActionTemplate($actBtns);

                // dd($thisData);

                $dataArr[]=$thisData;



                // print_r($model);
            }

        }


        $dtInputColsArr = [];

        $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
        $inputArr = [
            'columns' => $dtInputColsArr,
            'draw' => (int)$request->draw,
            'length' => (int)$request->length,
            'order' => [
                ['column'=>0,'dir'=>'asc'],
            ],
            'search' => ['value'=>$request->search['value'],'regex'=>false],
            'start' => $request->start,
        ];

        $response = [
            'draw' => (int)$request->draw,
            'input' => $inputArr,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords,
            'data' => $dataArr,
        ];
        // dd($response);
        return new JsonResponse($response);

    }

    public function delete($id)
    {
       // dd($id);
       $model = $this->findModel($id);
       $model->delete();
       DB::table($model->getTable())
       ->where('id', $id)
       ->update(['deleted_by' => Auth::user()->id]);
       DB::table('required_document_children')->where('required_document_id', $id)->delete();
       return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
   }


    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        // dd("hello");
        $model = new requiredDocument;
        return view('required-document::required-document.create',compact('model'));
    }



    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
      $request->validate([
        'subject' => 'required',
        'module_type' => 'required',
        'date' => 'required',
        'open_till' => 'required',
    ]);

      $model = new RequiredDocument;
      $model->subject = $request->subject;
      $model->date = $request->date;
      $model->open_till = $request->open_till;
      $model->revisions = $request->revisions;
      $model->status = $request->status;
      $model->module_type = $request->module_type;
      $model->module_keyword = $request->module_keyword;
      $model->module_id = $request->module_id;
      $model->description = $request->description;

      if ($model->save()) {
        return redirect('required-document')->with('success', __('scopedocument::scopedocument.scope-document.saved'));
    }else{
        return redirect('required-document')->with('error', __('scopedocument::scopedocument.scope-document.notsaved'));
    }

}

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\RequiredDocument  $requiredDocument
    * @return \Illuminate\Http\Response
    */
    public function view(RequiredDocument $requiredDocument, $id)
    {
        $model = RequiredDocument::find($id);
        $results = DB::table('required_document_children')
        ->join('required_templates', 'required_templates.id', '=', 'required_document_children.template_id')
        ->select('required_templates.title', 'required_document_children.description')
        ->where('required_document_children.required_document_id', '=', $id)
        ->get();

        // dd($results);

        return view('required-document::required-document.view',compact('model','results'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\RequiredDocument  $requiredDocument
    * @return \Illuminate\Http\Response
    */
    public function edit(RequiredDocument $requiredDocument, $id)
    {
        $model =  RequiredDocument::find($id);
        return view('required-document::required-document.update',compact('model'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Models\RequiredDocument  $requiredDocument
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, RequiredDocument $requiredDocument)
    {
      $request->validate([
        'subject' => 'required',
        'module_type' => 'required',
        'module_keyword' => 'required',
        'date' => 'required',
        'open_till' => 'required',
    ]);
        // dd($request->input());
      $model = requiredDocument::find($request->id);

        // $model->reference_number = $request->reference_number;
      $model->subject = $request->subject;
      $model->date = $request->date;
      $model->open_till = $request->open_till;
      $model->revisions = $request->revisions;
      $model->status = $request->status;
      $model->module_type = $request->module_type;
      $model->module_keyword = $request->module_keyword;
      $model->module_id = $request->module_id;
      $model->updated_at = date('Y-m-d H:i:s', strtotime('now'));
      $model->description = $request->description;
      $model->existingId = $request->existingId;

      if ($model->save()) {
        return redirect('required-document')->with('success', __('scopedocument::scopedocument.scope-document.saved'));
    }else{
        return redirect('required-document')->with('error', __('scopedocument::scopedocument.scope-document.notsaved'));
    }
}

    /**
    * Find a model based on the primary key
    *
    * @param  \App\Models\Opportunity  $predefinedList
    * @return \Illuminate\Http\Response
    */
    public function findModel($id)
    {
        return RequiredDocument::where('id', $id)->first();
    }



    public function gettemplates(Request $request)
    {
        // dd($request->data['template_id']);
        $id = $request->data['template_id'];
        $query = DB::table('required_templates')
        ->select(['title','description','id'])
        ->where('id', $id)
        ->first();
        return [
            'id' => $query->id,
            'title' => $query->title,
            'description'=> $query->description,
        ];
    }

    public function getgroup(Request $request)
    {
        $group_id = $request->data['group_id'];
        $templates= GroupChild::select('template_id')
        ->where(['group_id'=>$group_id])
        ->get()
        ->pluck('template_id', 'template_id')
        ->toArray();

        $group_title = Group::find($group_id);
        $group_title = $group_title->title;
        $data=[];
        foreach ($templates as  $value) {
            $results = RequiredTemplate::select(['id','title','description'])
            ->where(['id'=>$value])
            ->get();

            foreach ($results as $result) {
                $data[]=['id'=>$result->id,'title'=>$result->title,'description'=>$result->description];
            }
        }

        return [
            'data'=>$data,
            'group_title'=>$group_title,
        ];
    }

    public function sorting(Request $request)
    {
        $positions = $request->data['positions'];
        // $update = $request->data['update'];
        foreach ($positions as $position) {
            $index = $position[0];
            $newPosition = $position[1];
            $query = DB::table('required_document_children')
            ->where('id', $index)
            ->update(['order_by' => $newPosition]);
            if ($query) {
                return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('Order Changed')]]);
            }
        }



    }





    public function mailtemplate($id)
    {
        $model = new ScopeDiscussion();
        return view('required-document::required-document.email-template',compact('id','model'));
    }


    public function sendmail($id)
    {
        $link = request()->getHost();
        // dd($link);
        $linkAdress= "http://".$link."/required-document/mailtemplate/".$id;
        $details=[
            'title' => 'mail from wisdom',
            'body' => $linkAdress
        ];
        Mail::to("meharusama0011@gmail.com")->send(new MailHelper($details));
        echo "email send";
        return redirect('required-document/edit/'.$id);
    }


    public function scopedownload($id)
    {
        $model = RequiredDocument::find($id);

        $view = \View::make('required-document::required-document.scope-pdf',['id'=>$id]);
        $html_content = $view->render();

        PDF::SetTitle("List of Proposels");
        PDF::SetMargins(10, 30, 10);

        PDF::setHeaderCallback(function($pdf) {

            $img_path = url('assets/images/wisdom-logo.jpg');
            // dd($logo);
            $pdf->Image($img_path, 5, 5, 60, 0, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);

            $pdf->setFillColor(255,255,255);
            $pdf->SetFont('helvetica', 'B', 16);
            $pdf->Cell(0, 30, 'Wisdom Information Technology Solution LLC', 0, false, 'C', 20, 200, 1, false, '', 'M');

        });

        PDF::setFooterCallback(function($pdf) {
            $footerData = 'Wisdom Information Technology Solution LLC
            <br>Office 12A-04 DAMAC Heights, Tecom Dubai
            <br>VAT Number: 10032710600003';
            // set color for text
            $pdf->setFillColor(250, 250, 250);
            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('', '', 8);
            $pdf->writeHTMLCell(120, '', 45, $y=283, $footerData, 0, 0, 1, true, 'C', true);
            $pdf->Cell(0, 10, $pdf->getAliasNumPage(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

        });
        PDF::AddPage();
        PDF::writeHTML($html_content, true, false, true, false, '');
        ob_end_clean();
        // dd($html_content);
        PDF::Output($model->reference_number.'.pdf');
    }


    public function scopedecline($id)
    {
        $query = DB::table('required_documents')
        ->where('id', $id)
        ->update(['status' => 2]);
        if ($query) {
            return redirect('required-document/mailtemplate/' . $id);
        }
    }




    public function scopeapproved($id)
    {
        $query = DB::table('required_documents')
        ->where('id', $id)
        ->update(['status' => 3]);
        return redirect('required-document/mailtemplate/' . $id);
    }



    public function message(Request $request)
    {
        $model = new ScopeDiscussion();
        $model->required_document_id = $request->scope_id;
        $model->message = $request->discussion;
        if ($model->save()) {
            if ($request->route=="view") {
                return redirect('required-document/view/'.$request->scope_id)->with('success', __('scopedocument::scopedocument.scope-document.msgsent'));
            }
            if ($request->route=="disscuss") {
                return redirect('required-document/mailtemplate/'.$request->scope_id)->with('success', __('scopedocument::scopedocument.scope-document.msgsent'));
            }
        }


    }





}

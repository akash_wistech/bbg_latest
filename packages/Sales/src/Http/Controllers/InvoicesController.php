<?php

namespace Wisdom\Sales\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\InvoiceItems;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Event\Models\EventSubscription;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use View;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use PDF; // at the top of the file
use Wisdom\Sales\Models\Payments;
use Carbon\Carbon;

class InvoicesController extends Controller
{
  public $folderName='sales::invoices';
  public $controllerId='invoices';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view($this->folderName.'.index',compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
    $id;

    if ($request->has('user_id')) {
      $id = (int)$request->input('user_id');
      $query->where('user_id', $id);
    }


    if ($request->has('id')) {
      $id = (int)$request->input('id');
      $query->where('company_id', $id);
    }

    if ($request->has('event_id')) {
      $id = (int)$request->input('event_id');
      $sub = \Wisdom\Event\Models\EventSubscription::where('event_id',$id)->select('id');
      $query2= \DB::table('module_invoice')->whereIn('module_id',$sub)->where('module_type','event-member-register')->select('invoice_id');
      $query->where(function($query) use($sub,$query2){
        $query->whereIn('id',$query2);
      });
    }




    if ($request->has('type')) {
      $query->where('sent_date_time', '!=', null);
    }


    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='p_reference_number';
      $srchFlds[]='reference_number';
      $srchFlds[]='admin_note';
      $srchFlds[]='client_note';
      $srchFlds[]='terms_text';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where(function($query) use ($request){
      $query->where(function($query) use ($request){
        $query->where('p_reference_number','like','%'.$request->reference_no.'%')
        ->whereNull('reference_number');
      })
      ->orWhere('reference_number','like','%'.$request->reference_no.'%');
    })
      ;
      if($request->invoice_type!='')$query->where('invoice_type',$request->invoice_type);
      if($request->payment_status!='')$query->where('payment_status',$request->payment_status);
      if($request->admin_note!='')$query->where('admin_note','like','%'.$request->admin_note.'%');
      if($request->client_note!='')$query->where('client_note','like','%'.$request->client_note.'%');
      if($request->terms_text!='')$query->where('terms_text','like','%'.$request->terms_text.'%');

      if($request->input_field!=''){
        advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
      }

      $invoiceTypes = getInvoiceTypesListArr();

      $countQuery = clone $query;

      $totalRecords = $countQuery->count('id');

      $gridViewColumns=getGridViewColumns($moduleTypeId);

      $orderBy = 'id';
      if($request->ob)$orderBy = $request->ob;
      $orderByOrder = 'desc';
      if($request->obo)$orderByOrder = $request->obo;
      $models = $query->offset($request->start)
      ->limit($request->length)
      ->orderBy($orderBy,$orderByOrder)
      ->get();
      $dataArr=[];
      if($models!=null){
        foreach($models as $model){
          $thisData=[];
        //Colors column
          $thisData['cb_col']='';

          $colorArr = getActivityColors($moduleTypeId,$model->id);
          $thisData['cs_col']=implode("",$colorArr);

          $thisData['id']=$model->id;
          $invRefNo = $model->p_reference_number;
          $invDate = $model->p_invoice_date;
          if($model->reference_number!=null && $model->reference_number!=''){
            $invRefNo = $model->reference_number;
            $invDate = $model->invoice_date;
          }
          $thisData['reference_no']=$invRefNo;
          $thisData['invoice_date']=formatDate($invDate);
        // if($invoiceTypes!=null){
        //   $thisData['invoice_type']=isset($invoiceTypes[$model->invoice_type]) ? $invoiceTypes[$model->invoice_type] : '';
        // }
          if($invoiceTypes!=null){
            $thisData['invoice_type']=isset($invoiceTypes[$model->invoice_type]) ? $invoiceTypes[$model->invoice_type] : '';
          }

          $userInfo = '';
          if($model->invoice_type==1 || $model->invoice_type==2){
              if ($model->usertype=='contact' || $model->usertype=='') {
            $user = getUserInfo($model->user_id);
            if($user!=null)$userInfo.=$user->name;
            $company = getCompanyInfo($model->company_id);
            if($company!=null){
              if($userInfo!='')$userInfo.=' ('.$company->title.')';
            }
          }
          if ($model->usertype=='prospect') {
            $user = getProspectInfo($model->user_id);
            if($user!=null)$userInfo.=$user->full_name;
            if($user->company_name!=null){
              if($userInfo!='')$userInfo.=' ('.$user->company_name.')';
            }
          }

          }
          // else{
          //   $moduleInvoiceRow = ModuleInvoice::where('invoice_id',$model->id)->first();
          //   if($moduleInvoiceRow!=null){
          //     $eventSubscriberRow = EventSubscription::where('id',$moduleInvoiceRow->module_id)->first();
          //     if($eventSubscriberRow!=null){
          //       $userInfo.= $eventSubscriberRow->guest_name.'('.$eventSubscriberRow->guest_company_name.')';
          //     }
          //   }
          // }


          $thisData['name']=$userInfo;
          $thisData['payment']=getPaymentStatusLabelArr()[$model->payment_status];
          $thisData['grand_total']=$model->grand_total;

        // $thisData['name']=$model->name;
        // $companyNames = getRowMultipleCompanies($moduleTypeId,$model->id);
        // $thisData['company_name']=$companyNames;
        // $thisData['email']=$model->email;
        // $thisData['phone']=$model->phone;
        //
          if($gridViewColumns!=null){
            foreach($gridViewColumns as $gridViewColumn){
              $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
            }
          }
          $thisData['created_at']=formatDateTime($model->created_at);
          if ($model->created_by!=null && $model->created_by!=0) {
            $createdBy = getUserInfo($model->created_by);
            $thisData['created_by']=$createdBy->fullname;
          }else {
              $thisData['created_by']='---';
          }

          $actBtns=[];





        // invoice Generate Performa Invoice
          $btnTtitl='View Invoice';
          if($model->invoice_no==null){
            $btnTtitl='View Performa Invoice';
          }
          $actBtns[]='
          <a href="'.url('invoices/view-invoice/'.$model->id).'"
          class="btn btn-sm btn-clean btn-icon poplink"  data-id="'.$model->company_id.'" data-invoicetype="performa_invoice" data-invoiceid="'.$model->id.'" >
          <i class="fas fa-file-invoice-dollar" data-toggle="tooltip" title="'.$btnTtitl.'"></i>
          </a>';


          if($model->invoice_no==null){
          $actBtns[]='
          <a href="'.url('invoices/generate-tax-invoice/'.$model->id).'"
          class="btn btn-sm btn-clean btn-icon view act-confirmation"  data-id="'.$model->company_id.'"
          data-confirmationmsg="Are you sure you want generate Tax Invoice for this? This action can not be undone."
           data-invoicetype="performa_invoice" data-invoiceid="'.$model->id.'" data-pjxcntr="grid-table" >
          <i class="fas fas fa-file-invoice" data-toggle="tooltip" data-original-title="Generate Tax Invoice"></i>
          </a>';
        }

          if(checkActionAllowed('view')){
            $actBtns[]='
            <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
            <i class="text-dark-50 flaticon-file-2"></i>
            </a>';
          }
          if(checkActionAllowed('update')){
            $actBtns[]='
            <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
            <i class="text-dark-50 flaticon-edit"></i>
            </a>';
          }
          if(checkActionAllowed('delete')){
            $actBtns[]='
            <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
            <i class="text-dark-50 flaticon-delete"></i>
            </a>';
          }
          $thisData['action_col']=getDatatableActionTemplate($actBtns);
          $dataArr[]=$thisData;
        }
      }
      $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
      if($gridViewColumns!=null){
        foreach($gridViewColumns as $gridViewColumn){
          $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
        }
      }
      $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
      $inputArr = [
        'columns' => $dtInputColsArr,
        'draw' => (int)$request->draw,
        'length' => (int)$request->length,
        'order' => [
          ['column'=>0,'dir'=>'asc'],
        ],
        'search' => ['value'=>$request->search['value'],'regex'=>false],
        'start' => $request->start,
      ];

      $response = [
        'draw' => (int)$request->draw,
        'input' => $inputArr,
        'recordsTotal' => $totalRecords,
        'recordsFiltered' => $totalRecords,
        'data' => $dataArr,
      ];
      return new JsonResponse($response);
    }

  /**
  * Load list of pending invoices for a company
  *
  * @return \Illuminate\Http\Response
  */
  public function loadInvoicesByCustomer(Request $request, $id)
  {
    $html = '';
    $results = Invoices::where('company_id',$id)->whereNotIn('payment_status',[1])->get();
    if($results!=null){
      $html.='<table class="table table-bordered table-stripped">';
      $html.='  <thead>';
      $html.='  <tr>';
      $html.='    <th>'.__('common.date').'</th>';
      $html.='    <th>'.__('sales::invoices.invoice_no').'</th>';
      $html.='    <th>'.__('common.amount').'</th>';
      $html.='    <th>'.__('sales::payments.amount_due').'</th>';
      $html.='    <th>'.__('common.discount').'</th>';
      $html.='    <th>'.__('sales::payments.received').'</th>';
      $html.='  </tr>';
      $html.='  </thead>';
      $html.='  <tbody>';
      foreach($results as $result){
        $html.='  <tr class="item-row">';
        $html.='    <td>';
        $html.='      <input type="hidden" name="module[]" value="'.$result->moduleTypeId.'-'.$result->id.'" />';
        $html.='    '.formatDate($result->final_invoice_date).'';
        $html.='    </td>';
        $html.='    <td>'.$result->reference_no.'</td>';
        $html.='    <td>'.$result->grand_total.'</td>';
        $html.='    <td><input type="text" name="amount_due['.$result->moduleTypeId.']['.$result->id.']" value="'.$result->amount_due.'" class="form-control amt-due" readonly="readonly" /></td>';
        $html.='    <td><input type="text" name="discount['.$result->moduleTypeId.']['.$result->id.']" value="0" class="form-control discfld" autocomplete="off" required="true" /></td>';
        $html.='    <td><input type="text" name="amount_paid['.$result->moduleTypeId.']['.$result->id.']" value="0" class="form-control amt-paid calcttl" autocomplete="off" required="true" /></td>';
        $html.='  </tr>';
      }
      $html.='  </tbody>';
      $html.='</table>';
      return new JsonResponse(['success'=>['invoicesHtml'=>$html]]);
    }else{
      return new JsonResponse(['error'=>['invoicesHtml'=>'<div class="alert alert-danger">'.__('sales:invoices.not_found').'</div>']]);
    }
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Invoices;
    $model->p_invoice_date = date("Y-m-d");
    $model->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($model->p_invoice_date))));
    $model->p_invoice_no = __('common.autogenerated');
    $model->currency_id = getSetting('currency');
    $model->discount_calculation = 'aftertax';
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'customer_name'=>'required',
        'user_id'=>'required',
        // 'invoice_type'=>'nullable|integer',
        'p_invoice_date'=>'nullable|date',
        'due_date'=>'nullable|date',
        'is_recurring'=>'nullable|integer',
        'discount_calculation'=>'nullable|string',
        'over_due_reminder'=>'nullable|integer',
        'admin_note'=>'nullable|string',
        // 'discount'=>'nullable|number',
        // 'discount_type'=>'nullable|string',
        // 'adjustment'=>'nullable|number',
        'client_note'=>'nullable|string',
        'terms_text'=>'nullable|string',
      ]);
      unset($model->p_invoice_no);
      $model->user_id=$request->user_id;
      $model->company_id=$request->company_id ?? 0;
      $model->currency_id = $request->currency_id ?? getSetting('currency');
      // $model->invoice_type=$request->invoice_type ?? 0;
      $model->invoice_type=1;
      $model->p_invoice_date=$request->p_invoice_date ?? '';
      $model->due_date=$request->due_date ?? '';
      $model->is_recurring=$request->is_recurring ?? 0;
      $model->discount_calculation=$request->discount_calculation ?? '';
      $model->over_due_reminder=$request->over_due_reminder ?? 0;
      $model->admin_note=$request->admin_note ?? '';

      $model->item_type=$request->item_type ?? [];
      $model->item_id=$request->item_id ?? [];
      $model->title=$request->title ?? [];
      $model->descp=$request->descp ?? [];
      $model->qty=$request->qty ?? [];
      $model->rate=$request->rate ?? [];
      $model->discount=$request->discount ?? [];
      $model->discount_type=$request->discount_type ?? [];
      $model->discount_comments=$request->discount_comments ?? [];
      $model->tax_id=$request->tax_id ?? [];

      // $model->discount=$request->discount ?? 0;
      // $model->discount_type=$request->discount_type ?? '';
      // $model->adjustment=$request->adjustment ?? 0;
      $model->client_note=$request->client_note ?? 0;
      $model->terms_text=$request->terms_text ?? 0;
      if ($model->save()) {
        saveTags($model,$request,'form');
        if(function_exists('saveModuleComment')){
          saveModuleComment($model->moduleTypeId,$model->id,$model->admin_note);
        }
        if(function_exists('logAction')){
          $viewUrl = url('invoices/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' created an invoice: ';
          $message.= '<a href="'.$viewUrl.'">'.$model->p_reference_number.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'success',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::invoices.saved'),]]);
        }else{
          return redirect('invoices')->with('success', __('sales::invoices.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('sales::invoices.notsaved'),]]);
        }else{
          return redirect('invoices')->with('error', __('sales::invoices.notsaved'));
        }
      }
    }
    return view($this->folderName.'.create',compact('model','request'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \Wisdom\Sales\Models\Invoices  $invoices
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, $id)
  {
    $model = Invoices::where('id', $id)->first();
    $oldComment = $model->admin_note;
    $model->discount_calculation = 'aftertax';
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'id'=>'required',
        'customer_name'=>'required',
        'user_id'=>'required',
        // 'invoice_type'=>'nullable|integer',
        'p_invoice_date'=>'nullable|date',
        'due_date'=>'nullable|date',
        'is_recurring'=>'nullable|integer',
        'discount_calculation'=>'nullable|string',
        'over_due_reminder'=>'nullable|integer',
        'admin_note'=>'nullable|string',
        // 'discount'=>'nullable|number',
        // 'discount_type'=>'nullable|string',
        // 'adjustment'=>'nullable|number',
        'client_note'=>'nullable|string',
        'terms_text'=>'nullable|string',
      ]);

      $model= $this->findModel($id);
      $model->user_id=$request->user_id;
      $model->company_id=$request->company_id ?? 0;
      $model->currency_id = $request->currency_id ?? getSetting('currency');
      // $model->invoice_type=$request->invoice_type ?? 0;
      $model->invoice_type=1;
      $model->p_invoice_date=$request->p_invoice_date ?? '';
      $model->due_date=$request->due_date ?? '';
      $model->is_recurring=$request->is_recurring ?? 0;
      $model->discount_calculation=$request->discount_calculation ?? '';
      $model->over_due_reminder=$request->over_due_reminder ?? 0;
      $model->admin_note=$request->admin_note ?? '';

      $model->inv_item_id=$request->inv_item_id ?? [];
      $model->item_type=$request->item_type ?? [];
      $model->item_id=$request->item_id ?? [];
      $model->title=$request->title ?? [];
      $model->descp=$request->descp ?? [];
      $model->qty=$request->qty ?? [];
      $model->rate=$request->rate ?? [];
      $model->discount=$request->discount ?? [];
      $model->discount_type=$request->discount_type ?? [];
      $model->discount_comments=$request->discount_comments ?? [];
      $model->tax_id=$request->tax_id ?? [];

      // $model->discount=$request->discount ?? 0;
      // $model->discount_type=$request->discount_type ?? '';
      // $model->adjustment=$request->adjustment ?? 0;
      $model->client_note=$request->client_note ?? 0;
      $model->terms_text=$request->terms_text ?? 0;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if(function_exists('saveModuleComment')){
          if($oldComment!=$model->admin_note){
            saveModuleComment($model->moduleTypeId,$model->id,$model->admin_note);
          }
        }
        saveTags($model,$request,'form');
        if(function_exists('logAction')){
          $invRefNo = $model->p_reference_number;
          if($model->reference_number!=null && $model->reference_number!='')$invRefNo = $model->reference_number;
          $viewUrl = url('invoices/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' updated invoice: ';
          $message.= '<a href="'.$viewUrl.'">'.$invRefNo.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'info',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::invoices.saved'),]]);
        }else{
          return redirect('invoices')->with('success', __('sales::invoices.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('sales::invoices.notsaved'),]]);
        }else{
          return redirect('invoices')->with('error', __('sales::invoices.notsaved'));
        }
      }
    }
    if($request->ajax()){
      $viewFile = $this->folderName.'.ajax';
    }else{
      $viewFile = $this->folderName.'.update';
    }
    return view($viewFile, [
      'model'=>$model,
      'request'=>$request,
    ]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \Wisdom\Sales\Models\Invoices  $invoices
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $model= $this->findModel($id);
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);

    if(function_exists('logAction')){
      $viewUrl = url('invoices/view/'.$model->id);
      $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
      $message.= ' deleted invoice: ';
      $message.= ''.$model->reference_no.'';
      $logdata = [
        'module_type' => $model->moduleTypeId,
        'module_id' => $model->id,
        'message' => $message,
        'action_type' => 'danger',
      ];
      logAction([],$logdata);
    }

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::invoices.deleted'),]]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \Wisdom\Sales\Models\Invoices  $invoices
  * @return \Illuminate\Http\Response
  */
  public function destroyItem($iid,$id)
  {
    $parentModel= $this->findModel($iid);
    $model= $this->findItemModel($iid,$id);
    $model->delete();

    DB::table($model->getTable())
    ->where('invoice_id', $iid)
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);

    if(function_exists('logAction')){
      $viewUrl = url('invoices/view/'.$parentModel->id);
      $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
      $message.= ' deleted item from invoice: ';
      $message.= ''.$parentModel->reference_no.'';
      $logdata = [
        'module_type' => $parentModel->moduleTypeId.'-item',
        'module_id' => $id,
        'message' => $message,
        'action_type' => 'danger',
      ];
      logAction([],$logdata);
    }

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::invoices.deleted'),]]);
  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Invoices;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Invoices::where('id', $id)->first();
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findItemModel($iid,$id)
  {
    return InvoiceItems::where('invoice_id',$iid)->where('id', $id)->first();
  }

  public function generateTaxInvoice($id,Request $request){
    $model = $this->findModel($id);
    if($model->invoice_no==null){
      $invoiceNo=$model->generateInvoiceNumber();
      $invReferenceNumber='INV'.'-'.sprintf('%06d', $invoiceNo);

      DB::table($model->getTable())
      ->where('id', $model->id)
      ->update([
        'reference_number' => $invReferenceNumber,
        'invoice_no' => $invoiceNo,
        'invoice_date' => date("Y-m-d H:i:s"),
      ]);
    }
    return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('invoices/view-invoice/'.$model->id),'heading'=>__('Tax Invoice')]]);
  }

  public function viewInvoice($id,Request $request){

    $model = $this->findModel($id);
    return view('sales::invoices.view_customer_invoice',
      [
        'model' => $model,
        'emailattach'=>'0'
      ]);
  }

  public function viewInvoiceFend(Request $request, $id)
  {
    $model = $this->findModel($id);
    $view = \View::make('sales::invoices.view_customer_invoiceFend',['model'=>$model,'emailattach'=>'0']);
    $html_content = $view->render();
    PDF::SetTitle("View Invoice");
    // PDF::SetMargins(10, 30, 10);
    PDF::AddPage();
    PDF::writeHTML($html_content, true, false, true, false, '');
    ob_end_clean();
    $attachment =  PDF::Output("invoice".'.pdf' ,'S');

  }

  public function SendInvoice($id,Request $request){
    $invoiceId =$request->post('data')['invoiceid'];
    $invoiceType =$request->post('data')['invoiceType'];
    if ($id) {  $company=Company::where('id',$id)->orderBy('id','desc')->first(); }
    $invoices = Invoices::where('id', $invoiceId)->first();
    return view('sales::Invoices.send_invoice',
      [
        'company' => $company,
        'invoice' => $invoices,
        'invoiceType' => $request->data['invoiceType'],
      ]);
  }




  public function SendInvoiceEmail(Request $request){

    $model = $this->findModel($request->post('invoiceid'));
    $model->sent_date_time = \Carbon\Carbon::now();
    $model->save();
    sendEmailInvoice($model,$emailattach=1);
    return true;
  }


  public function makePdfStatement($company_id,$type,$download){

   $model = Company::where('id',$company_id)->first();
   $totalInvoiceAmount = Invoices::where('company_id',$model->id)->whereNotNull('invoice_date')->sum('grand_total');
   $totalPaymentAmount = Payments::where('company_id',$model->id)->sum('amount_received');
   $TotalBalence = $totalInvoiceAmount-$totalPaymentAmount;

   $invoices = Invoices::where('company_id',$model->id)->whereNotNull('invoice_date')
   ->select([
     'id',
     'reference_number',
     DB::raw("'invoice' as type"),
     'invoice_date as date',
     'grand_total as total',
     'due_date'
   ]);
   $payments = Payments::where('company_id',$model->id)
   ->select([
     'id',
     DB::raw("'' as reference_number"),
     DB::raw("'payment' as type"),
     'payment_date as date',
     'amount_received as total',
     DB::raw("'' as due_date"),
   ]);


   if ($type=='all') {
     $statements = $payments->union($invoices)->orderBy('date', 'ASC');
   }
   if ($type=='today') {
    $invoices->whereDate('invoice_date', Carbon::today());
    $payments->whereDate('payment_date', Carbon::today());
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }
  if ($type=='this_week') {
    $invoices->whereBetween('invoice_date',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
    $payments->whereBetween('payment_date',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }
  if ($type=='this_month') {
    $invoices->whereMonth('invoice_date', Carbon::now()->month);
    $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    $payments->whereMonth('payment_date', Carbon::now()->month);
    $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }
  if ($type=='last_month') {
    $invoices->whereMonth('invoice_date', '=', Carbon::now()->subMonth()->month);
    $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    $payments->whereMonth('payment_date', '=', Carbon::now()->subMonth()->month);
    $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }
  if ($type=='this_year') {
    $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }
  if ($type=='last_year') {
    $invoices->whereYear('invoice_date', now()->subYear()->year);
    $payments->whereYear('payment_date', now()->subYear()->year);
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }else {
    $statements = $payments->union($invoices)->orderBy('date', 'ASC');
  }


  $statements = $statements->get()->toArray();


  $view = \View::make('company.statementpdf',[
   'model' => $model,
   'totalInvoiceAmount' => $totalInvoiceAmount,
   'totalPaymentAmount' => $totalPaymentAmount,
   'TotalBalence' => $TotalBalence,
   'statements' => $statements,
   'type'=>'pdf'
 ]);
  $html_content = $view->render();

          // dd($html_content);

  PDF::SetTitle("Badges");
  PDF::SetMargins(10, 30, 10);
  PDF::setHeaderCallback(function($pdf) {
    $pdf->setFillColor(249,249,249);
    $pdf->SetFont('helvetica', 'B', 16);
    $pdf->Cell(0, 20, 'British Business Group', 0, false, 'C', 20, 200, 1, false, '', 'M');
  });

  PDF::AddPage();
  PDF::writeHTML($html_content, true, false, true, false, '');
  ob_end_clean();
          // dd($html_content);
  if ($download=='download') {
   PDF::Output($company_id.'.pdf','d');
 }else {
   PDF::Output($company_id.'.pdf');
 }


}


}

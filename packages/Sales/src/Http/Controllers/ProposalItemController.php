<?php

namespace Wisdom\Sales\Http\Controllers;

use Wisdom\Sales\Models\ProposalItem;
use Illuminate\Http\Request;
use Wisdom\Sales\Models\Page;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class ProposalItemController extends Controller
{

    public function newModel()
      {
        return new ProposalItem;
      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        return view('proposal-item::proposal-item.index',compact('moduleTypeId','request'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
    {
      $model = new ProposalItem();
     return view('proposal-item::proposal-item.create', compact('model','request'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        // dd($request->all());
     

        $validated = $request->validate([
        'title' => 'required',
        'status' => 'required',
        'price' => 'required',
        ]);

        $model = new ProposalItem();
        $model->title= $request->title;
        ( $request->description !=null ) ? ($model->description= $request->description) : '';
        $model->parent_id= $request->parent_id;
        $model->price= $request->price;
        $model->tax = $request->tax;
        $model->status= $request->status;
        $model->created_by=\Auth::user()->id;

        if ( $model->save()) {
         return redirect('proposal/item')->with('success', 'Succefully Added');
        }
         if ($validator->fails()) {
            return redirect('proposal/item/create')->withErrors($validated);
        }
        
    }

 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProposalItem  $proposalItem
     * @return \Illuminate\Http\Response
     */
       public function edit(Request $request, $id )
    {
      $model =  ProposalItem::find($id);
     return view('proposal-item::proposal-item.update',
      [
        'model'=> $model,
        'request'=>$request,

      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProposalItem  $proposalItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validated = $request->validate([
        'title' => 'required',
        'status' => 'required',
        ]);
        $model = ProposalItem::find($id);
        $model->title= $request->title;
        ( $request->description !=null ) ? ($model->description= $request->description) : '';
        $model->parent_id= $request->parent_id;
        $model->price= $request->price;
        $model->tax = $request->tax;
        $model->status= $request->status;
        $model->updated_by=\Auth::user()->id;
         if ( $model->save()) {
         return redirect('proposal/item')->with('success', 'Succefully Added');
        }
          if ($validator->fails()) {
            return redirect('proposal/item/create')->withErrors($validated);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProposalItem  $proposalItem
     * @return \Illuminate\Http\Response
     */



          public function datatableData(Request $request)
  {


    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'prospect',$query);

    $countQuery = clone $query;
    $totalRecords = $countQuery->count('id');
    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];

    if($models!=null){
      foreach($models as $model){
       
        // $thisData=[];

        // dd();

        $thisData['parent_id']= ($model->parent_id ==0 ) ?  '' :  getProposalCategoryById($model->parent_id);
  
        $thisData['id']=$model->id;
        $thisData['title']=$model->title;
        $thisData['description']=$model->description;
        $thisData['tax']=getTaxes()[$model->tax];
        $thisData['price']=$model->price;
        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }

       $User= getUser($model->created_by);
        $thisData['created_at']=formatDateTime($model->created_at);
        // $createdBy = getUserInfo($model->created_by);
        // $thisData['created_by']=$createdBy->fullname;
         $thisData['created_by']=$User->name;
         $statusType = getStatusArr()[$model->status];
                $class = "label-light-success";
                $value="Not Set";
                if ($statusType=="Disabled") {
                    $class = "label-light-danger";
                    $value = "Rejected";
                }
                if ($statusType=="Enable") {
                    $class = "label-light-success";
                    $value = "Approved";
                }

        $status ='<span class="label label-lg font-weight-bold label-inline '. $class.'">'. $value.'</span>';
        $thisData['status']=$status;
        $actBtns=[];
        // if(checkActionAllowed('create','opportunity')){
        //   $actBtns[]='
        //   <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-add-circular-button"></i>
        //   </a>';
        // }
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/prospect/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('edit')){
          $actBtns[]='
          <a href="'.url('/proposal/item/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('proposal/item/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }



    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  public function delete($id,Request $request)
    {
     
        $model = ProposalItem::find($id);
        if ( $model->delete()) {
             if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.proposal.deleted')]]);
          }else{
            return redirect('proposal/item');
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.proposal.notsaved'),]]);
          }else{
            return redirect('proposal/item')->with('error', __('app.proposal.notsaved'));
          }
        }
    } 

    
}

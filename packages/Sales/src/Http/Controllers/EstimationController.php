<?php

namespace Wisdom\Sales\Http\Controllers;

use Wisdom\Sales\Models\Estimation;
use Wisdom\Sales\Models\EstimationChild;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Models\Prospect;

class EstimationController extends Controller
{


  public function newModel()
  {
    return new Estimation;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $moduleTypeId = $this->newModel()->moduleTypeId;
      return view('estimation::index',compact('moduleTypeId','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $model = new Estimation();
      return view('estimation::create', compact('model','request'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id=0)
    {


     $assignedTo='';


     $model = Estimation::find($id);

     EstimationChild::where('estimation_id',$id)->delete();

     if ($model==null) {

      $model = new Estimation();

    }

    if (!empty($request->assigned_to)) {
     $assignedTo = implode(",",$request->assigned_to);
   }

   $model->subject = $request->subject;
   $model->module_type = $request->module_type;
   $model->module_id = $request->module_id;
   $model->module_name = $request->module_name;
   $model->date = $request->date;
   $model->open_till = $request->open_till;
   $model->status = $request->status;
   $model->assigned_to = $assignedTo;
   $model->to = $request->to;
   $model->address = $request->address;
   $model->email = $request->email;
   $model->phone_no = $request->phone;
   $model->sub_total = $request->sub_total;
   $model->subject = $request->subject;
   $model->discount_value = $request->discount_value;
   $model->discount_amount = $request->discount_amount;
   $model->adjustment = $request->adjustment;
   $model->total = $request->total;
   $model->created_by = \Auth::user()->id;
   if ($model->save()) {

    if ($request->items !=null) {
      foreach ($request->items as $key => $value) {
        $estimationChild =   new EstimationChild();
        $estimationChild->estimation_id = $model->id;
        $estimationChild->item_id = $value['item_id'];
        $estimationChild->item_name = $value['item_name'];
        $estimationChild->item_description = $value['item_description'];
        $estimationChild->item_quantity = $value['item_quantity'];
        $estimationChild->item_price = $value['item_price'];
        $estimationChild->item_tax = $value['item_tax'];
        $estimationChild->item_amount = $value['item_amount'];
        $estimationChild->save();
      }
    }
  }

  return redirect('estimation');


}

public function datatableData(Request $request)
{




  $moduleTypeId = $this->newModel()->moduleTypeId;
  $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
  permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'prospect',$query);

  $countQuery = clone $query;
  $totalRecords = $countQuery->count('id');
  $gridViewColumns=getGridViewColumns($moduleTypeId);

  $orderBy = 'id';
  if($request->ob)$orderBy = $request->ob;
  $orderByOrder = 'desc';
  if($request->obo)$orderByOrder = $request->obo;
  $models = $query->offset($request->start)
  ->limit($request->length)
  ->orderBy($orderBy,$orderByOrder)
  ->get();
  $dataArr=[];

  if($models!=null){
    foreach($models as $model){


        // $thisData=[];
      $thisData['id']=$model->id;
      $thisData['subject']=$model->subject;
      $thisData['to']=$model->to;
      $thisData['email']=$model->email;
      $thisData['phone']=$model->phone_no;
      $thisData['total']=$model->total;




      if($gridViewColumns!=null){
        foreach($gridViewColumns as $gridViewColumn){
          $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
        }
      }

      $User= getUserInfo($model->created_by);
        // $thisData['created_at']=formatDateTime($model->created_at);
        // $createdBy = getUserInfo($model->created_by);
        // $thisData['created_by']=$createdBy->fullname;
      $thisData['created_by']=$User->name;
      $thisData['status']=getEstimationStatusLabel()[$model->status];

      $actBtns=[];
        // if(checkActionAllowed('create','opportunity')){
        //   $actBtns[]='
        //   <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-add-circular-button"></i>
        //   </a>';
        // }
      if(checkActionAllowed('view')){
        $actBtns[]='
        <a href="'.url('/prospect/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-file-2"></i>
        </a>';
      }
      if(checkActionAllowed('edit')){
        $actBtns[]='
        <a href="'.url('/estimation/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-edit"></i>
        </a>';
      }
      if(checkActionAllowed('delete')){
        $actBtns[]='
        <a href="'.url('estimation/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
        <i class="text-dark-50 flaticon-delete"></i>
        </a>';
      }
      $thisData['action_col']=getDatatableActionTemplate($actBtns);
      $dataArr[]=$thisData;
    }
  }



  $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
  if($gridViewColumns!=null){
    foreach($gridViewColumns as $gridViewColumn){
      $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
    }
  }
  $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
  $inputArr = [
    'columns' => $dtInputColsArr,
    'draw' => (int)$request->draw,
    'length' => (int)$request->length,
    'order' => [
      ['column'=>0,'dir'=>'asc'],
    ],
    'search' => ['value'=>$request->search['value'],'regex'=>false],
    'start' => $request->start,
  ];

  $response = [
    'draw' => (int)$request->draw,
    'input' => $inputArr,
    'recordsTotal' => $totalRecords,
    'recordsFiltered' => $totalRecords,
    'data' => $dataArr,
  ];
  return new JsonResponse($response);
}



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Estimation  $estimation
     * @return \Illuminate\Http\Response
     */


    public function edit(Request $request, $id )
    {
      $model =  Estimation::find($id);
      return view('estimation::update', compact('model','request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Estimation  $estimation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estimation $estimation)
    {
        //
    }


    public function getProspect(Request $request, $id)
    {

     return  Prospect::where('id', $id)->first();
   }


   public function delete($id,Request $request)
   {

    $model = Estimation::find($id);

    EstimationChild::where('estimation_id',$id)->delete();


    $model = Estimation::find($id);
    if ( $model->delete()) {
     if($request->ajax()){
      return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.estimation.deleted')]]);
    }else{
      return redirect('estimation');
    }
  }else{
    if($request->ajax()){
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.estimation.notsaved'),]]);
    }else{
      return redirect('estimation')->with('error', __('app.estimation.notsaved'));
    }
  }
}



public function view(Request $request,$id){


  $estimation = DB::table('estimations')
  ->select(['id','subject','sub_total','discount_value','adjustment','total'])
  ->where('id', $id)->first();


  $estimation_child = DB::table('estimation_children')
  ->select(['sub_total','discount_value','adjustment','total'])
  ->where('estimation_id', $id)
  ->select('.item_name','.item_description', '.item_quantity','.item_price', '.item_tax', '.item_amount')
  ->get()->toArray();


  return [
    'estimation' => $estimation,
    'estimation_child'=> $estimation_child,
  ];
}


}

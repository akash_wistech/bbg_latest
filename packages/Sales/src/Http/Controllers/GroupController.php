<?php

namespace Wisdom\Sales\Http\Controllers;

use Wisdom\Sales\Models\Group;
use Wisdom\Sales\Models\GroupChild;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $moduleTypeId = (new Group)->moduleTypeId;
      return view('group::group.index',compact('moduleTypeId','request'));
    }


/**
* returns data for datatable
*
* @return \Illuminate\Http\Response
*/
public function datatableData(Request $request)
{

  $moduleTypeId = (new Group)->moduleTypeId;
  $query = DB::table((new Group)->getTable())->whereNull('deleted_at');
  permissionListTypeFilter($moduleTypeId,(new Group)->getTable(),'groups',$query);



  $countQuery = clone $query;

  $totalRecords = $countQuery->count('id');

  $models = $query->offset($request->start)
  ->limit($request->length)
  ->get();
    // print_r($models); die();

  $dataArr=[];
  if($models!=null){
    foreach($models as $model){

      $statusType = getStatusArr()[$model->status];
          // dd($statusType);
      if ($statusType=="Enable") {
        $class = "label-light-primary";
        $value="Enable";
      }
      if ($statusType=="Disable") {
        $class = "label-light-info";
        $value="Disable";
      }

      $status ='<span class="label label-lg font-weight-bold label-inline '. $class.'">'. $value.'</span>';


      $thisData['id']=$model->id;
      $thisData['title']=$model->title;
      $thisData['status']=$status;

      $colorArr = getActivityColors($moduleTypeId,$model->id);

      $thisData['cs_col']=implode("",$colorArr);

      $actBtns=[];
      if(checkActionAllowed('view','group')){
        $actBtns[]='
        <a href="'.url('/group/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" title="'.__('common.view').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-file-2"></i>
        </a>';
      }
      if(checkActionAllowed('update','group')){
        $actBtns[]='
        <a href="'.url('/group/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" title="'.__('common.update').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-edit"></i>
        </a>';
      }
      if(checkActionAllowed('delete','group')){
        $actBtns[]='
        <a href="'.url('/group/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
        <i class="text-dark-50 flaticon-delete"></i>
        </a>';
      }

      $thisData['action_col']=getDatatableActionTemplate($actBtns);
      $dataArr[]=$thisData;
    }

  }


  $dtInputColsArr = [];
  $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
  $inputArr = [
    'columns' => $dtInputColsArr,
    'draw' => (int)$request->draw,
    'length' => (int)$request->length,
    'order' => [
      ['column'=>0,'dir'=>'asc'],
    ],
    'search' => ['value'=>$request->search['value'],'regex'=>false],
    'start' => $request->start,
  ];

  $response = [
    'draw' => (int)$request->draw,
    'input' => $inputArr,
    'recordsTotal' => $totalRecords,
    'recordsFiltered' => $totalRecords,
    'data' => $dataArr,
  ];
// dd($response);
  return new JsonResponse($response);

}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $model = new Group;
      return view('group::group.create',compact('model','request'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFor(Request $request)
    {
      $model = new Group;
      return view('group::group._form',compact('model','request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

      $request->validate([
        'title' => 'required',
        'template_id' => 'required',
      ]);

      $model = new Group;
      $model->title = $request->title;
      $model->status = $request->status;

      $model->template_id = $request->template_id;


      if ($model->save()) {
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('scopedocument::scopedocument.group.saved')]]);
        }else{
          return redirect('group')->with('success', __('scopedocument::scopedocument.group.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('scopedocument::scopedocument.group.notsaved'),]]);
        }else{
          return redirect('group')->with('error', __('scopedocument::scopedocument.group.notsaved'));
        }
      }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function view(Group $group, $id)
    {
      $model = Group::find($id);

      $results = DB::table('group_children')
      ->join('required_templates', 'required_templates.id', '=', 'group_children.template_id')
      ->select('required_templates.title')
      ->where('group_children.group_id', '=', $id)
      ->get();

        // dd($results); die();

      


      return view('group::group.view',compact('model','results'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Group $group, $id)
    {
      $model = Group::find($id);
      return view('group::group.update',compact('model','request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
      $request->validate([
        'title' => 'required',
        'template_id' => 'required',
      ]);

      $model = Group::find($request->id);
      $model->title = $request->title;
      $model->status = $request->status;
      $model->updated_at = date('Y-m-d H:i:s', strtotime('now'));

      $model->template_id = $request->template_id;
      $model->existing_template_id = $request->existing_template_id;

      if ($model->save()) {
        return redirect('group')->with('success', __('scopedocument::scopedocument.group.saved'));
      }else{
        return redirect('group')->with('error', __('scopedocument::scopedocument.group.notsaved'));
      }

    }

      /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Opportunity  $predefinedList
  * @return \Illuminate\Http\Response
  */
      public function findModel($id)
      {
        return group::where('id', $id)->first();
      }
    }

<?php

namespace Wisdom\Sales\Http\Controllers;

use App\Http\Controllers\Controller;
use Wisdom\Sales\Models\Payments;
use Wisdom\Sales\Models\PaymentItems;
use Illuminate\Http\Request;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PaymentsController extends Controller
{
  public $folderName='sales::payments';
  public $controllerId='payments';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view($this->folderName.'.index',compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_number';
      $srchFlds[]='note';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }


    if ($request->has('event_id')) {
    $id = (int)$request->input('event_id');
    $sub = \Wisdom\Event\Models\EventSubscription::where('event_id',$id)->select('id');
    $query2= \DB::table('module_invoice')->whereIn('module_id',$sub)->where('module_type','event-member-register')->select('invoice_id');
    $payments= \DB::table('payment_items')->whereIn('module_id',$query2)->where('module_type','invoice')->select('payment_id');

    $query->where(function($query) use($sub,$payments){
        $query->whereIn('id',$payments);
    });
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->invoice_id!=''){
      $query->whereIn('id',function($query) use ($request){
        $query->select('payment_id')
        ->from((new PaymentItems)->getTable())
        ->where('module_type','invoice')
        ->where('module_id',$request->invoice_id);
      });
    }
    if($request->company_id!='')$query->where('company_id',$request->company_id);
    if($request->user_id!='')$query->where('user_id',$request->user_id);
    if($request->reference_number!='')$query->where('reference_number','like','%'.$request->reference_number.'%');
    if($request->admin_note!='')$query->where('note','like','%'.$request->admin_note.'%');

    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        //Colors column
        $thisData['cb_col']='';

        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_number']=$model->reference_number;
        $thisData['payment_date']=formatDate($model->payment_date);

        $userInfo = '';
        $user = getUserInfo($model->user_id);
        if($user!=null)$userInfo.=$user->name;
        $company = getCompanyInfo($model->company_id);
        if($company!=null){
          if($userInfo!='')$userInfo.=' ('.$company->title.')';
        }
        $thisData['name']=$userInfo;

        // $thisData['name']=$model->name;
        // $companyNames = getRowMultipleCompanies($moduleTypeId,$model->id);
        // $thisData['company_name']=$companyNames;
        // $thisData['email']=$model->email;
        // $thisData['phone']=$model->phone;
        //
        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        $thisData['amount']=formatInputInvPriceCol($model->id,$model->amount_received);
        $thisData['completed']=$model->is_completed==1 ? '<span class="label font-weight-bold label-lg label-light-success label-inline"><i class="fa fa-check"></i></span>' : '<span class="label label-light-primary label-inline"><i class="fa fa-times"></i></span>';
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_number','name'=>'reference_number','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function createForModule(Request $request,$moduleTypeId,$module_id)
  {
    $model = new Payments;
    $moduleClass = getPaymentModuleClass()[$moduleTypeId];
    $moduleRow = $moduleClass::where('id',$module_id)->first();
    $model->payment_date = date("Y-m-d");
    $amountDue = 0;
    if($moduleRow!=null){
      $model->company_id = $moduleRow->company_id;
      $model->user_id = $moduleRow->user_id;
      $amountDue = $moduleRow->amount_due;
      $model->amount_received = $amountDue;
    }
    $model->payment_no = __('common.autogenerated');
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'amount_received'=>'required|number',
        'payment_date'=>'nullable|date',
        'payment_method'=>'nullable|integer',
        'transaction_id'=>'nullable|string',
        'send_notification'=>'nullable|integer',
        // 'tax_invoice'=>'nullable|integer',
        'note'=>'nullable|string',
      ]);
      unset($model->payment_no);
      $model->amount_received=$request->amount_received ?? 0;
      $model->payment_date=$request->payment_date ?? date("Y-m-d");
      $model->payment_method=$request->payment_method ?? 0;
      $model->transaction_id=$request->transaction_id ?? '';
      $model->send_notification=$request->send_notification ?? 0;
      $model->note=$request->note ?? '';
      $model->is_completed=1;

      $model->module=[$moduleTypeId.'-'.$module_id];
      $model->amount_due=[$moduleTypeId=>[$module_id=>$amountDue]];
      $model->discount=[$moduleTypeId=>[$module_id=>0]];
      // $model->tax_invoice=[$moduleTypeId=>[$module_id=>($request->tax_invoice ? $request->tax_invoice : 0)]];
      $model->amount_paid=[$moduleTypeId=>[$module_id=>$model->amount_received]];
      if ($model->save()) {
        saveTags($model,$request,'form');
        if(function_exists('logAction')){
          $viewUrl = url('payments/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' created a payment: ';
          $message.= '<a href="'.$viewUrl.'">'.$model->reference_number.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'success',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::payments.saved'),]]);
        }else{
          return redirect('payments')->with('success', __('sales::payments.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('sales::payments.notsaved'),]]);
        }else{
          return redirect('payments')->with('error', __('sales::payments.notsaved'));
        }
      }
    }
    return view($this->folderName.'._form',compact('model'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Payments;
    $model->payment_date = date("Y-m-d");
    $model->payment_no = __('common.autogenerated');
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'customer_name'=>'required',
        'user_id'=>'required',
        'payment_date'=>'nullable|date',
        'payment_method'=>'nullable|integer',
        'transaction_id'=>'nullable|string',
        'send_notification'=>'nullable|integer',
        'note'=>'nullable|string',

        // 'tax_invoice'=>'nullable|integer',
        'discount'=>'nullable|number',
        'amount_paid'=>'nullable|amount_paid',
      ]);
      unset($model->payment_no);
      $model->user_id=$request->user_id;
      $model->company_id=$request->company_id ?? 0;
      $model->amount_received=$request->amount_received ?? 0;
      $model->payment_date=$request->payment_date ?? '';
      $model->payment_method=$request->payment_method ?? 0;
      $model->transaction_id=$request->transaction_id ?? '';
      $model->send_notification=$request->send_notification ?? 0;
      $model->note=$request->note ?? '';
      $model->is_completed=1;

      $model->module=$request->module;
      $model->amount_due=$request->amount_due;
      $model->discount=$request->discount;
      // $model->tax_invoice=$request->tax_invoice;
      $model->amount_paid=$request->amount_paid;

      if ($model->save()) {
        saveTags($model,$request,'form');
        if(function_exists('logAction')){
          $viewUrl = url('payments/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' created a payment: ';
          $message.= '<a href="'.$viewUrl.'">'.$model->reference_number.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'success',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::payments.saved'),]]);
        }else{
          return redirect('payments')->with('success', __('sales::payments.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('sales::payments.notsaved'),]]);
        }else{
          return redirect('payments')->with('error', __('sales::payments.notsaved'));
        }
      }
    }
    return view($this->folderName.'.create',compact('model'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \Wisdom\Sales\Models\Payments  $payments
  * @return \Illuminate\Http\Response
  */
  public function edit(Request $request, $id)
  {
    $model = $this->findModel($id);
    if($request->isMethod('post')){
      $data=$request->all();
      $validator = Validator::make($request->all(), [
        'id'=>'required',
        'customer_name'=>'required',
        'user_id'=>'required',
        'payment_date'=>'nullable|date',
        'payment_method'=>'nullable|integer',
        'transaction_id'=>'nullable|string',
        'send_notification'=>'nullable|integer',
        'note'=>'nullable|string',

        // 'tax_invoice'=>'nullable|integer',
        'discount'=>'nullable|number',
        'amount_paid'=>'nullable|amount_paid',
      ]);

      $model= $this->findModel($id);
      $model->amount_received=$request->amount_received;
      $model->payment_date=$request->payment_date ?? '';
      $model->payment_method=$request->payment_method ?? 0;
      $model->transaction_id=$request->transaction_id ?? '';
      $model->send_notification=$request->send_notification ?? 0;
      $model->note=$request->note ?? '';
      $model->is_completed=1;

      $model->module=$request->module;
      $model->pmt_item_id=$request->pmt_item_id;
      $model->amount_due=$request->amount_due;
      $model->discount=$request->discount;
      // $model->tax_invoice=$request->tax_invoice;
      $model->amount_paid=$request->amount_paid;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        saveTags($model,$request,'form');
        if(function_exists('logAction')){
          $recRefNo = $model->reference_number;
          $viewUrl = url('payments/view/'.$model->id);
          $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
          $message.= ' updated payment: ';
          $message.= '<a href="'.$viewUrl.'">'.$recRefNo.'</a>';
          $logdata = [
            'module_type' => $model->moduleTypeId,
            'module_id' => $model->id,
            'message' => $message,
            'target_url' => $viewUrl,
            'action_type' => 'info',
          ];
          logAction($request,$logdata);
        }
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::payments.saved'),]]);
        }else{
          return redirect('payments')->with('success', __('sales::payments.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('sales::payments.notsaved'),]]);
        }else{
          return redirect('payments')->with('error', __('sales::payments.notsaved'));
        }
      }
    }
    return view($this->folderName.'.update', [
      'model'=>$model,
    ]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \Wisdom\Sales\Models\Payments  $payments
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $model= $this->findModel($id);
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);

    if(function_exists('logAction')){
      $viewUrl = url('payments/view/'.$model->id);
      $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
      $message.= ' deleted invoice: ';
      $message.= ''.$model->reference_number.'';
      $logdata = [
        'module_type' => $model->moduleTypeId,
        'module_id' => $model->id,
        'message' => $message,
        'action_type' => 'danger',
      ];
      logAction([],$logdata);
    }

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::payments.deleted'),]]);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \Wisdom\Sales\Models\Payments  $payments
  * @return \Illuminate\Http\Response
  */
  public function destroyItem($iid,$id)
  {
    $parentModel= $this->findModel($iid);
    $model= $this->findItemModel($iid,$id);
    $model->delete();

    DB::table($model->getTable())
    ->where('invoice_id', $iid)
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);

    if(function_exists('logAction')){
      $viewUrl = url('payments/view/'.$parentModel->id);
      $message = '<a href="'.url('staff/view/'.Auth::user()->id).'">'.Auth::user()->name.'</a>';
      $message.= ' deleted item from invoice: ';
      $message.= ''.$parentModel->reference_number.'';
      $logdata = [
        'module_type' => $parentModel->moduleTypeId.'-item',
        'module_id' => $id,
        'message' => $message,
        'action_type' => 'danger',
      ];
      logAction([],$logdata);
    }

    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('sales::payments.deleted'),]]);
  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Payments;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Payments::where('id', $id)->first();
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findItemModel($iid,$id)
  {
    return PaymentItems::where('invoice_id',$iid)->where('id', $id)->first();
  }
}

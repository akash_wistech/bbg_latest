<?php

namespace Wisdom\Sales\Http\Controllers;


use Illuminate\Http\Request;
use Wisdom\Sales\Models\Proposal;
use Wisdom\Sales\Models\ProposalChild;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class ProposalController extends Controller
{

 public function newModel()
 {
  return new Proposal;
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $moduleTypeId = $this->newModel()->moduleTypeId;
      return view('proposal::index',compact('moduleTypeId','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $model = new Proposal();
      return view('proposal::create', compact('model','request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          // dd($request->all());
      $model = new Proposal();
      $model->subject = $request->subject;
      $model->module_type = $request->module_type;
      $model->module_keyword = $request->module_keyword;
      $model->module_id = $request->module_id;
      $model->proposal_page_id = $request->proposal_page_id;
      $model->estimation_id = $request->estimation_id;
      $model->created_by = \Auth::user()->id;;
      if($model->save()){
        $n=1;
        foreach ($request->sorting as $key => $value) {
         if (strpos($value, 'proposal') !== false) {
           $proposalId= str_replace('proposal-', '', $value);
           $proposalChild = new ProposalChild();
           $proposalChild->proposal_id = $model->id;
           $proposalChild->type = 'proposal';
           $proposalChild->source_id = $proposalId;
           $proposalChild->description = $request->proposal_description[$proposalId];
           $proposalChild->order_by = $n;
           $proposalChild->save();
         }
         if (strpos($value, 'estimation') !== false) {
           $estimationId= str_replace('estimation-', '', $value);
           $proposalChild = new ProposalChild();
           $proposalChild->proposal_id = $model->id;
           $proposalChild->type = 'estimation';
           $proposalChild->source_id = $estimationId;
           $proposalChild->order_by = $n;
           $proposalChild->save();
         }
         $n++;
       }
     }
     return redirect('proposal');
   }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {

      $model =  Proposal::find($id);
      return view('proposal::update', compact('model','request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
     
      $sortingarr = [];
      foreach($request->sorting as $val ){
        $tmp = explode( '-', $val );
        $sortingarr[$tmp[0]][]= $tmp[1];
      }


      if(isset($sortingarr['proposal'])) {
        DB::table('proposal_children')->where('proposal_id',$id)->where('type','proposal')->whereNotIn('source_id',$sortingarr['proposal'])->delete();         
      }
      else{
       DB::table('proposal_children')->where('proposal_id',$id)->where('type','proposal')->delete();         
     }

     if(isset($sortingarr['estimation'])) {
      DB::table('proposal_children')->where('proposal_id',$id)->where('type','estimation')->whereNotIn('source_id',$sortingarr['estimation'])->delete();         
    }
    else{
     DB::table('proposal_children')->where('proposal_id',$id)->where('type','estimation')->delete();         
   }

   $model =Proposal::where('id',$id)->first();
   $model->subject = $request->subject;
   $model->module_type = $request->module_type;
   $model->module_keyword = $request->module_keyword;
   $model->module_id = $request->module_id;
   $model->proposal_page_id = $request->proposal_page_id;
   $model->estimation_id = $request->estimation_id;
   $model->created_by = \Auth::user()->id;
   if($model->save()){
    $n=1;
    foreach ($request->sorting as $key => $value) {
     if (strpos($value, 'proposal') !== false) {
       $proposalId= str_replace('proposal-', '', $value);
       $proposalChild =ProposalChild::where('proposal_id',$id)->where('type','proposal')->where('source_id',$proposalId)->first();
       if ($proposalChild==null) {
        $proposalChild = new ProposalChild();
      }
      $proposalChild->proposal_id = $model->id;
      $proposalChild->type = 'proposal';
      $proposalChild->source_id = $proposalId;
      $proposalChild->description = $request->proposal_description[$proposalId];
      $proposalChild->order_by = $n;
      $proposalChild->save();
    }
    if (strpos($value, 'estimation') !== false) {
     $estimationId= str_replace('estimation-', '', $value);
     $proposalChild =ProposalChild::where('proposal_id',$id)->where('type','estimation')->where('source_id',$estimationId)->first();
     if ($proposalChild==null) {
       $proposalChild = new ProposalChild();
     }
     $proposalChild->proposal_id = $model->id;
     $proposalChild->type = 'estimation';
     $proposalChild->source_id = $estimationId;
     $proposalChild->order_by = $n;
     $proposalChild->save();
   }
   $n++;
 }
}
return redirect('proposal');
}


public function datatableData(Request $request)
{


  $moduleTypeId = $this->newModel()->moduleTypeId;
  $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
  permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'prospect',$query);

  $countQuery = clone $query;
  $totalRecords = $countQuery->count('id');
  $gridViewColumns=getGridViewColumns($moduleTypeId);

  $orderBy = 'id';
  if($request->ob)$orderBy = $request->ob;
  $orderByOrder = 'desc';
  if($request->obo)$orderByOrder = $request->obo;
  $models = $query->offset($request->start)
  ->limit($request->length)
  ->orderBy($orderBy,$orderByOrder)
  ->get();
  $dataArr=[];

  if($models!=null){
    foreach($models as $model){

      $thisData['id']=$model->id;
      $thisData['subject']=$model->subject;
      $thisData['module_type']=$model->module_type;
      $thisData['module_keyword']=$model->module_keyword;

      if($gridViewColumns!=null){
        foreach($gridViewColumns as $gridViewColumn){
          $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
        }
      }

      $User= getUserInfo($model->created_by);
      $thisData['created_at']=formatDateTime($model->created_at);
        // $createdBy = getUserInfo($model->created_by);
        // $thisData['created_by']=$createdBy->fullname;
      $thisData['created_by']=$User->name;
         // $statusType = getStatusArr()[$model->status];
        //         $class = "label-light-success";
        //         $value="Not Set";
        //         if ($statusType=="Disabled") {
        //             $class = "label-light-danger";
        //             $value = "Rejected";
        //         }
        //         if ($statusType=="Enable") {
        //             $class = "label-light-success";
        //             $value = "Approved";
        //         }

        // $status ='<span class="label label-lg font-weight-bold label-inline '. $class.'">'. $value.'</span>';
        // $thisData['status']=$status;
      $actBtns=[];
        // if(checkActionAllowed('create','opportunity')){
        //   $actBtns[]='
        //   <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-add-circular-button"></i>
        //   </a>';
        // }
      if(checkActionAllowed('edit')){
        $actBtns[]='
        <a href="'.url('/proposal/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-edit"></i>
        </a>';
      }
      if(checkActionAllowed('delete')){
        $actBtns[]='
        <a href="'.url('proposal/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
        <i class="text-dark-50 flaticon-delete"></i>
        </a>';
      }
      $thisData['action_col']=getDatatableActionTemplate($actBtns);
      $dataArr[]=$thisData;
    }
  }



  $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
  if($gridViewColumns!=null){
    foreach($gridViewColumns as $gridViewColumn){
      $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
    }
  }
  $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
  $inputArr = [
    'columns' => $dtInputColsArr,
    'draw' => (int)$request->draw,
    'length' => (int)$request->length,
    'order' => [
      ['column'=>0,'dir'=>'asc'],
    ],
    'search' => ['value'=>$request->search['value'],'regex'=>false],
    'start' => $request->start,
  ];

  $response = [
    'draw' => (int)$request->draw,
    'input' => $inputArr,
    'recordsTotal' => $totalRecords,
    'recordsFiltered' => $totalRecords,
    'data' => $dataArr,
  ];
  return new JsonResponse($response);
}


public function delete($id,Request $request)
{

  $model = Proposal::find($id);

  ProposalChild::where('proposal_id',$id)->delete();

  if ( $model->delete()) {
   if($request->ajax()){
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('Delete Successfully')]]);
  }else{
    return redirect('proposal');
  }
}else{
  if($request->ajax()){
    return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('Something Went Wrong!'),]]);
  }else{
    return redirect('proposal')->with('error', __('Something Went Wrong!'));
  }
}
} 



public function sorting(Request $request){


  $positions = $request->data['positions'];

     // dd($positions);
    // $update = $request->data['update'];
  foreach ($positions as $position) {
    $type = $position[0];
    $index = $position[1];
    $newPosition = $position[2];
    
    $query = DB::table('proposal_children')
    ->where('type', $type)
    ->where('source_id', $index)
    ->update(['order_by' => $newPosition]);
    
  }

  if ($query) {
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('Order Changed')]]);
  }



}
}

<?php

namespace Wisdom\Sales\Http\Controllers;

use Wisdom\Sales\Models\RequiredTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class RequiredTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // $results = RequiredTemplate::all();
      //   return view('required-template._index', compact('results'));


      // dd($request);
      $moduleTypeId = (new RequiredTemplate)->moduleTypeId;
      // dd($moduleTypeId);
      return view('required-template::required-template.index',compact('moduleTypeId','request'));
      
    }

      /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
      public function datatableData(Request $request)
      {
        // dd($request);
        $moduleTypeId = (new RequiredTemplate)->moduleTypeId;
        $query = DB::table((new RequiredTemplate)->getTable())->whereNull('deleted_at');
        permissionListTypeFilter($moduleTypeId,(new RequiredTemplate)->getTable(),'required_templates',$query);
        $countQuery = clone $query;

        $totalRecords = $countQuery->count('id');

        // $gridViewColumns=getGridViewColumns($moduleTypeId);

        $models = $query->offset($request->start)
        ->limit($request->length)
        ->get();
        // dd($models);


        $dataArr=[];
        if($models!=null){
          foreach($models as $model){

            $statusType = getStatusArr()[$model->status];
          // dd($statusType);
            if ($statusType=="Enable") {
              $class = "label-light-primary";
              $value="Enable";
            }
            if ($statusType=="Disable") {
              $class = "label-light-info";
              $value="Disable";
            }

            $status ='<span class="label label-lg font-weight-bold label-inline '. $class.'">'. $value.'</span>';


            $thisData['id']=$model->id;
            $thisData['title']=$model->title;
            $thisData['description']=$model->description;
            $thisData['status']=$status;
        // $thisData['cb_col']='<input type="checkbox" name="selection[]" value="'.$model['id'].'">';

        //Colors column
            $colorArr = getActivityColors($moduleTypeId,$model->id);
             // dd($moduleTypeId);

            $thisData['cs_col']=implode("",$colorArr);
            $actBtns=[];
            if(checkActionAllowed('view','required-template')){
              $actBtns[]='
              <a href="'.url('/required-template/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" title="'.__('common.view').'" data-id="'.$model->id.'">
              <i class="text-dark-50 flaticon-file-2"></i>
              </a>';
            }
            if(checkActionAllowed('update','required-template')){
              $actBtns[]='
              <a href="'.url('/required-template/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" title="'.__('common.update').'" data-id="'.$model->id.'">
              <i class="text-dark-50 flaticon-edit"></i>
              </a>';
            }
            if(checkActionAllowed('delete','required-template')){
              $actBtns[]='
              <a href="'.url('/required-template/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
              <i class="text-dark-50 flaticon-delete"></i>
              </a>';
            }

            $thisData['action_col']=getDatatableActionTemplate($actBtns);

            // dd($thisData);

            $dataArr[]=$thisData;



 // print_r($model);
          }

        }


        $dtInputColsArr = [];
        $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
        $inputArr = [
          'columns' => $dtInputColsArr,
          'draw' => (int)$request->draw,
          'length' => (int)$request->length,
          'order' => [
            ['column'=>0,'dir'=>'asc'],
          ],
          'search' => ['value'=>$request->search['value'],'regex'=>false],
          'start' => $request->start,
        ];

        $response = [
          'draw' => (int)$request->draw,
          'input' => $inputArr,
          'recordsTotal' => $totalRecords,
          'recordsFiltered' => $totalRecords,
          'data' => $dataArr,
        ];
     // dd($response);
        return new JsonResponse($response);

      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      // dd("helo");
      $model = new RequiredTemplate;
      return view('required-template::required-template.create',compact('model','request'));
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFor(Request $request)
    {
      // dd($request);
      $model = new RequiredTemplate;
      return view('required-template::required-template._pop_up_form',compact('model','request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd("hello");
        // dd($request->input());
      $model = new RequiredTemplate;
      $model->title = $request->title;
      $model->description = $request->description;
      $model->status = $request->status;


      if ($model->save()) {
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('scopedocument::scopedocument.required-template.saved')]]);
        }else{
          return redirect('required-template')->with('success', __('scopedocument::scopedocument.required-template.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('scopedocument::scopedocument.required-template.notsaved'),]]);
        }else{
          return redirect('required-template')->with('error', __('scopedocument::scopedocument.required-template.notsaved'));
        }
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequiredTemplate  $requiredTemplate
     * @return \Illuminate\Http\Response
     */
    public function view(RequiredTemplate $requiredTemplate, $id)
    {
      $model = RequiredTemplate::find($id);
      return view('required-template::required-template.view',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequiredTemplate  $requiredTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(RequiredTemplate $requiredTemplate, $id)
    {
      $model = RequiredTemplate::find($id);
      return view('required-template::required-template.update',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequiredTemplate  $requiredTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequiredTemplate $requiredTemplate)
    {
        // dd($request->input());
      $model = RequiredTemplate::find($request->id);
      $model->title = $request->title;
      $model->description = $request->description;
      $model->status = $request->status;

      if ($model->save()) {
        return redirect('required-template')->with('success', __('scopedocument::scopedocument.required-template.saved'));
      }else{
        return redirect('required-template')->with('error', __('scopedocument::scopedocument.required-template.notsaved'));
      }
    }


      /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Opportunity  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return RequiredTemplate::where('id', $id)->first();
  }

 

  }

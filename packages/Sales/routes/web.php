<?php

use Illuminate\Support\Facades\Route;
use Wisdom\Sales\Http\Controllers\RequiredDocumentController;



Route::match(['get', 'post'], "required-document/mailtemplate/{id}", [RequiredDocumentController::class, 'mailtemplate']);
Route::match(['get', 'post'], "required-document/scopedownload/{id}", [RequiredDocumentController::class, 'scopedownload']);
Route::match(['get', 'post'], "required-document/scopedecline/{id}", [RequiredDocumentController::class, 'scopedecline']);
Route::match(['get', 'post'], "required-document/scopeapproved/{id}", [RequiredDocumentController::class, 'scopeapproved']);
Route::match(['get', 'post'], "required-document/message", [RequiredDocumentController::class, 'message'])->name('required-document.message');

Route::match(['post','get'],'invoices/pdf/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'viewInvoiceFend']);

Route::group(['middleware' => ['web','auth']], function () {

    include ('RequiredDocument.php');
    include ('RequiredTemplate.php');
    include ('group.php');
    include ('page.php');
    include ('proposalcategory.php');
    include ('proposalitem.php');
    include ('proposal.php');
    include ('estimation.php');
    include ('invoices.php');
    include ('payments.php');

});

<?php
use Illuminate\Support\Facades\Route;

Route::get('/payments', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'index'])->name('payments');
Route::get('/payments/data-table-data', [Wisdom\Sales\Http\Controllers\PaymentsController::class,'datatableData']);
Route::match(['post','get'],'/payments/create', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'create'])->name('payments.create');
Route::match(['post','get'],'/payments/create/{module}/{id}', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'createForModule'])->name('payments.create.formodule');
Route::match(['post','get'],'/payments/update/{id}', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'edit'])->name('payments.update.{id}');
Route::match(['post','get'],'/payments/view/{id}', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'show'])->name('payments.view.{id}');
// Route::post('/payments/update/{id}', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'update'])->name('payments.update');
Route::post('/payments/delete/{id}', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'destroy'])->name('payments.destroy.{id}');
Route::post('/payments/delete-item/{iid}/{id}', [Wisdom\Sales\Http\Controllers\PaymentsController::class, 'destroyItem'])->name('payments.destroyItem.{iid}.{id}');

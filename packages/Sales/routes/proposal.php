<?php

use Wisdom\Sales\Http\Controllers\ProposalController;






Route::get("proposal/create", [ProposalController::class, 'create'])->name('proposal.create');
Route::post("proposal/store", [ProposalController::class, 'store'])->name('proposal.store');
Route::match(['get','post'],"proposal/edit/{id}", [ProposalController::class, 'edit'])->name('proposal.edit');
Route::post("proposal/update/{id}", [ProposalController::class, 'update'])->name('proposal.update');
Route::match(['get','post'],"proposal/", [ProposalController::class, 'index'])->name('proposal');
Route::match(['get','post'],"proposal/data-table-data", [ProposalController::class, 'datatableData']);
Route::match(['get', 'post'],'proposal/delete/{id}', [ProposalController::class,'delete'])->name('proposal.delete');


Route::match(['get', 'post'],'proposal/sorting', [ProposalController::class,'sorting'])->name('proposal.sorting');







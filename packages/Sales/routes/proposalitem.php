<?php



use Wisdom\Sales\Http\Controllers\ProposalItemController;


Route::get("proposal/item/create", [ProposalItemController::class, 'create'])->name('proposalitem.create');
Route::post("proposal/item/store", [ProposalItemController::class, 'store'])->name('proposalitem.store');
Route::match(['get','post'],"proposal/item/edit/{id}", [ProposalItemController::class, 'edit'])->name('proposalitem.edit');
Route::post("proposal/item/update/{id}", [ProposalItemController::class, 'update'])->name('proposalitem.update');
Route::match(['get','post'],"proposal/item", [ProposalItemController::class, 'index'])->name('proposalitem');
Route::match(['get','post'],"proposal/item/data-table-data", [ProposalItemController::class, 'datatableData']);
Route::match(['get', 'post'],'proposal/item/delete/{id}', [ProposalItemController::class,'delete'])->name('proposalitem.delete');





<?php

use Wisdom\Sales\Http\Controllers\ProposalCategoryController;






Route::get("proposal/category/create", [ProposalCategoryController::class, 'create'])->name('proposalcategory.create');
Route::post("proposal/category/store", [ProposalCategoryController::class, 'store'])->name('proposalcategory.store');
Route::match(['get','post'],"proposal/category/edit/{id}", [ProposalCategoryController::class, 'edit'])->name('proposalcategory.edit');
Route::post("proposal/category/update/{id}", [ProposalCategoryController::class, 'update'])->name('proposalcategory.update');
Route::match(['get','post'],"proposal/category", [ProposalCategoryController::class, 'index'])->name('proposalcategory');
Route::match(['get','post'],"proposal/category/data-table-data", [ProposalCategoryController::class, 'datatableData']);
Route::match(['get', 'post'],'proposal/category/delete/{id}', [ProposalCategoryController::class,'delete'])->name('proposalcategory.delete');





<?php

use Illuminate\Support\Facades\Route;
use Wisdom\Sales\Http\Controllers\RequiredDocumentController;

Route::get('/required-document', [RequiredDocumentController::class,'index']);

Route::get('/required-document/data-table-data', [RequiredDocumentController::class,'datatableData']);


Route::match(['get', 'post'], '/required-document/create', [RequiredDocumentController::class,'create'])->name('required-document.create');
Route::post("/required-document/store", [RequiredDocumentController::class, 'store'])->name('required-document.store');
Route::get("required-document/edit/{id}", [RequiredDocumentController::class, 'edit']);
Route::post("required-document/update/{id}", [RequiredDocumentController::class, 'update'])->name('required-document.update');
Route::match(['get', 'post'], "required-document/delete/{id}", [RequiredDocumentController::class, 'delete']);
Route::match(['get', 'post'], "required-document/view/{id}", [RequiredDocumentController::class, 'view']);

Route::post("/get-templates", [RequiredDocumentController::class, 'gettemplates']);
Route::post("/get-group", [RequiredDocumentController::class, 'getgroup']);

Route::match(['get', 'post'], "/sorting", [RequiredDocumentController::class, 'sorting']);






Route::match(['get', 'post'], "required-document/sendmail/{id}", [RequiredDocumentController::class, 'sendmail']);

// Route::get('/required-document/data-table-data', [RequiredDocumentController::class,'datatableData']);
// Route::match(['get', 'post'], '/required-document/import', [RequiredDocumentController::class,'import']);
// Route::post('/required-document/upload', [RequiredDocumentController::class,'upload']);
// Route::match(['get', 'post'], '/required-document/update/{id}', [RequiredDocumentController::class,'update']);
// Route::get('/required-document/view/{id}', [RequiredDocumentController::class,'show']);
// Route::post('/required-document/delete/{id}', [RequiredDocumentController::class,'destroy']);

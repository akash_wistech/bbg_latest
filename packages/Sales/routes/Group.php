<?php

use Illuminate\Support\Facades\Route;
use Wisdom\Sales\Http\Controllers\GroupController;

Route::get('/group', [GroupController::class,'index']);
Route::get('/group/data-table-data', [GroupController::class,'datatableData']);






Route::match(['get', 'post'], '/group/create', [GroupController::class,'create'])->name('group.create');
Route::match(['get', 'post'], '/group/store', [GroupController::class, 'store'])->name('group.store');
Route::get("group/view/{id}", [GroupController::class, 'view']);
Route::get("group/edit/{id}", [GroupController::class, 'edit']);
Route::post("group/update/{id}", [GroupController::class, 'update'])->name('group.update');
Route::match(['get', 'post'], "group/delete/{id}", [GroupController::class, 'destroy']);

Route::match(['get', 'post'], '/group/createFor', [GroupController::class,'createFor'])->name('group.createFor');
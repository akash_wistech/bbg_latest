<?php

use Wisdom\Sales\Http\Controllers\PageController;







Route::get("proposal/page/create", [PageController::class, 'create'])->name('proposalpage.create');
Route::post("proposal/page/store", [PageController::class, 'store'])->name('proposalpage.store');
Route::match(['get','post'],"proposal/page/edit/{id}", [PageController::class, 'edit'])->name('proposalpage.edit');
Route::post("proposal/page/update/{id}", [PageController::class, 'update'])->name('proposalpage.update');
Route::match(['get','post'],"proposal/page", [PageController::class, 'index'])->name('proposalpage');
Route::match(['get','post'],"proposal/page/data-table-data", [PageController::class, 'datatableData']);
Route::match(['get', 'post'],'proposal/page/delete/{id}', [PageController::class,'delete'])->name('proposalpage.delete');


Route::match(['get', 'post'],'proposal/page/view/{id}', [PageController::class,'view'])->name('proposalpage.view');






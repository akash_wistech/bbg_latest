<?php

use Illuminate\Support\Facades\Route;
use Wisdom\Sales\Http\Controllers\RequiredTemplateController;

Route::get('/required-template', [RequiredTemplateController::class,'index']);
Route::get('/required-template/data-table-data', [RequiredTemplateController::class,'datatableData']);






Route::match(['get', 'post'], '/required-template/create', [RequiredTemplateController::class,'create'])->name('required-template.create');
Route::match(['get', 'post'], '/required-template/store', [RequiredTemplateController::class, 'store'])->name('required-template.store');
Route::get("required-template/edit/{id}", [RequiredTemplateController::class, 'edit']);
Route::post("required-template/update/{id}", [RequiredTemplateController::class, 'update'])->name('required-template.update');
Route::match(['get', 'post'], "required-template/delete/{id}", [RequiredTemplateController::class, 'destroy']);

Route::get("required-template/view/{id}", [RequiredTemplateController::class, 'view']);


Route::match(['get', 'post'], '/required-template/createFor', [RequiredTemplateController::class,'createFor'])->name('required-template.createFor');
// Route::get('/required-template/data-table-data', [RequiredTemplateController::class,'datatableData']);
// Route::match(['get', 'post'], '/required-template/import', [RequiredTemplateController::class,'import']);
// Route::post('/required-template/upload', [RequiredTemplateController::class,'upload']);
// Route::match(['get', 'post'], '/required-template/update/{id}', [RequiredTemplateController::class,'update']);
// Route::get('/required-template/view/{id}', [RequiredTemplateController::class,'show']);
// Route::post('/required-template/delete/{id}', [RequiredTemplateController::class,'destroy']);

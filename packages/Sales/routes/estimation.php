<?php

use Wisdom\Sales\Http\Controllers\EstimationController;






Route::get("estimation/create", [EstimationController::class, 'create'])->name('estimation.create');
Route::post("estimation/store/{id}", [EstimationController::class, 'store'])->name('estimation.store');
Route::match(['get','post'],"estimation/edit/{id}", [EstimationController::class, 'edit'])->name('estimation.edit');
Route::post("estimation/update/{id}", [EstimationController::class, 'update'])->name('estimation.update');
Route::match(['get','post'],"estimation", [EstimationController::class, 'index'])->name('estimation');
Route::match(['get','post'],"estimation/data-table-data", [EstimationController::class, 'datatableData']);
Route::match(['get', 'post'],'estimation/delete/{id}', [EstimationController::class,'delete'])->name('estimation.delete');

Route::match(['get', 'post'],'estimation/view/{id}', [EstimationController::class,'view'])->name('estimation.view');


Route::match(['get', 'post'],'estimation/getProspect/{id}', [EstimationController::class,'getProspect'])->name('estimation.getProspect');

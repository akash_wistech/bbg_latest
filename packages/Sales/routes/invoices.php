<?php
use Illuminate\Support\Facades\Route;

Route::get('/invoices', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'index'])->name('invoices');
Route::get('/invoices/data-table-data', [Wisdom\Sales\Http\Controllers\InvoicesController::class,'datatableData']);
Route::match(['post','get'],'/invoices/create', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'create'])->name('invoices.create');
Route::match(['post','get'],'/invoices/update/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'edit'])->name('invoices.update.{id}');
Route::match(['post','get'],'/invoices/view/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'show'])->name('invoices.view.{id}');
// Route::post('/invoices/update/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'update'])->name('invoices.update');
Route::post('/invoices/delete/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'destroy'])->name('invoices.destroy.{id}');
Route::post('/invoices/delete-item/{iid}/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'destroyItem'])->name('invoices.destroyItem.{iid}.{id}');

Route::match(['post','get'],'/invoices/customer-invoices/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'loadInvoicesByCustomer'])->name('invoices.list.by.customer.{id}');


Route::match(['post','get'],'invoices/view-invoice/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'viewInvoice']);
Route::match(['post','get'],'invoices/generate-tax-invoice/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'generateTaxInvoice']);

Route::match(['post','get'],'invoices/send-invoice/{id}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'SendInvoice']);
Route::match(['post','get'],'invoices/send-invoice-email', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'SendInvoiceEmail']);
Route::match(['post','get'],'invoices/make-pdf-statement/{id}/{type}/{download}', [Wisdom\Sales\Http\Controllers\InvoicesController::class, 'makePdfStatement']);



<?php
return [
  'heading' => 'Payments',
  'saved' => 'Payment saved successfully',
  'notsaved' => 'Error while saving invoice',
  'deleted' => 'Payment trashed successfully',
  'payment_date' => 'Payment Date',
  'amount_received' => 'Amount Recieved',
  'amount_due' => 'Amount Due',
  'received' => 'Received',
  'send_notification' => 'Do not send invoice payment recorded email to customer contacts',
  'generate_tax_invoice' => 'Generate Tax Invoice',
];

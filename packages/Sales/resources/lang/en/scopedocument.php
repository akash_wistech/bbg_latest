<?php
return [

  'pending' => 'Pending',
  'rejected' => 'Rejected',
  'approved' => 'Approved',


    'proposal'=>[
     'status' => 'Status',
     'action' => 'Action',
     'create' => 'New',
     'import' => 'Import',
     'update' => 'Update',
     'update_rec' => 'Update :name',
     'history' => 'History',
     'view' => 'View',
     'delete' => 'Delete',
     'save' => 'Save',
     'cancel' => 'Cancel',
   ],


   'required-template' => [
    'heading' => 'Scope Template',
    'saved' => 'Saved successfully',
    'notsaved' => 'SomeThing Went Wrong',
    'id' =>'Id',
    'title' =>'Title',
    'description' =>'Description',
    'status' =>'Status',
    'action' =>'Action',
    'create' =>'New',
    'update' =>'Update',
    'view' =>'View',
  ],

  'scope-document' => [
    'heading' => 'Scope Document',
    'emailtemplate'=>'Proposal',
    'saved' => 'Saved successfully',
    'notsaved' => 'SomeThing Went Wrong',
    'msgsent' => 'Message Sent',
    'msgnotsent' => 'Message Not Sent',
    'id' =>'Id',
    'status' =>'Status',
    'reference_number' =>'Ref#',
    'subject' =>'Subject',
    'module-type' =>'Module Type',
    'module-keyword' =>'Moddle Keywork',
    'scope-date' =>'Date',
    'scope-opentill' =>'Open Till',
    'scope-revisions' =>'Revisions',


    'action' =>'Action',
    'create' =>'New',
    'update' =>'Update',
    'view' =>'View',
  ],

  'group' => [
    'heading' => 'Group',
    'saved' => 'Saved successfully',
    'notsaved' => 'SomeThing Went Wrong',
    'id' =>'Id',
    'title' =>'Title',
    'status' =>'Status',
    'action' =>'Action',
    'create' =>'New',
    'update' =>'Update',
    'view' =>'View',
  ],

];

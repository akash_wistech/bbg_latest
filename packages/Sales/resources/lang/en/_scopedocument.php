<?php
return [


'proposal'=>[
	'status' => 'Status',
  'action' => 'Action',
  'create' => 'New',
  'import' => 'Import',
  'update' => 'Update',
  'update_rec' => 'Update :name',
  'history' => 'History',
  'view' => 'View',
  'delete' => 'Delete',
  'save' => 'Save',
  'cancel' => 'Cancel',
],

];

<ul class="nav nav-tabs  mb-5  d-flex flex-column navi navi-hover navi-active navi-accent border-bottom-0">

     <li class="navi-item my-1">
        <a class="navi-link active" data-toggle="tab" href="#tab-1">
            <span class="navi-icon pr-4"><i class="far fa-building"></i></span>
            <span class="navi-text font-size-h6 ">{{__('common.overview')}}</span>
        </a>
    </li>
    <li class="navi-item my-1">
        <a class="navi-link mx-0" data-toggle="tab" href="#tab-payments">
            <span class="navi-icon pr-4"><i class="fas fa-file-invoice-dollar"></i></span>
            <span class="navi-text font-size-h6">{{__('common.payments')}}</span>
        </a>
    </li>
     <li class="navi-item my-1">
        <a class="navi-link mx-0" data-toggle="tab" href="#tab-actions">
            <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
            <span class="navi-text font-size-h6">{{__('common.actions')}}</span>
        </a>
    </li>
    <li class="navi-item my-1">
        <a class="navi-link mx-0" data-toggle="tab" href="#tab-attachments">
            <span class="navi-icon pr-4"><i class="fas fa-file-invoice-dollar"></i></span>
            <span class="navi-text font-size-h6">{{__('common.attachments')}}</span>
        </a>
    </li>
</ul>

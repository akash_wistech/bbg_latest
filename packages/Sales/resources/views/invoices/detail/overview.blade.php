@php
$invoiceTypes = getInvoiceTypesListArr();
@endphp
<div class="card card-custom gutter-b">
  <div class="card-header border-0 pt-5">
    <h3 class="card-title align-items-start flex-column">
      <span class="card-label font-weight-bolder text-dark">
        {{ $model->reference_no }} {!!getPaymentStatusLabelArr()[$model->payment_status]!!}
      </span>
    </h3>
    @if($model->payment_status!=1)
    <div class="card-toolbar">
      <ul class="nav nav-pills nav-pills-sm nav-dark-75">
        <li class="nav-item">
          <a href="{{url('/payments/create/invoice/'.$model->id)}}" class="btn btn-sm btn-primary poplink" data-heading="{{__('sales::invoices.pay_for',['invoiceId'=>$model->reference_no])}}">{{__('common.payment')}}</a>
        </li>
      </ul>
    </div>
    @endif
  </div>
  <div class="card-body pt-2 pb-0 mt-n3">
    <div class="tab-content mt-5 mb-5">
      <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="tab-1" id="tab-1">
        <div class="row mb-2">
        @if($invoiceTypes!=null && isset($invoiceTypes[$model->invoice_type]))
          <div class="col-md-6 col-sm-6">
            <strong>{{__('sales::invoices.invoice_type:')}}</strong> {{$invoiceTypes[$model->invoice_type]}}
          </div>
          @endif
        </div>
        <div class="row mb-2">
          <div class="col-md-6 col-sm-6">
            <strong>{{__('sales::invoices.invoice_date:')}}</strong> {{formatDate($model->final_invoice_date)}}
          </div>
          <div class="col-md-6 col-sm-6 text-right">
            <strong>{{__('sales::invoices.due_date:')}}</strong> {{formatDate($model->due_date)}}
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <address>
              <b style="color:black" class="company-name-formatted">{{config('app.name')}}</b><br>
              {!! nl2br(getSetting('company_address')); !!}
            </address>
          </div>
          <div class="col-sm-6 text-right">
            <span class="bold">{{__('sales::invoices.bill_to:')}}</span>
            <address>
              <strong>
                <a href="{{$model->bill_to_user_link}}" target="_blank">{{$model->bill_to_name}}</a>
                @if($model->company_id>0)
                <a href="{{url('/company/view/'.$model->company_id)}}" target="_blank">{{$model->bill_to_company_name}}</a>
                @else
                {{$model->bill_to_company_name}}
                @endif
              </strong>
              @if($model->company!=null)
              <br>
              {!! $model->bill_to_company_address; !!}
              @endif
            </address>
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-12">
            <strong>{{__('sales::invoices.admin_note')}}</strong><br />
            {!!nl2br($model->admin_note)!!}
          </div>
        </div>
        <div class="text-dark-50 line-height-md">
          <div id="grid-table" class="table-responsive">
    				<table id="inv-items" class="table">
    					<thead>
    						<tr>
    							<th class="pl-0 font-weight-bold text-muted text-uppercase">{{__('common.items')}}</th>
    							<th class="font-weight-bold text-muted text-uppercase">{{__('common.description')}}</th>
    							<th width="70" class="text-right font-weight-bold text-muted text-uppercase">{{__('common.qty')}}</th>
    							<th width="120" class="text-right font-weight-bold text-muted text-uppercase">{{__('common.rate')}}</th>
    							<th width="160" class="text-right font-weight-bold text-muted text-uppercase">{{__('common.discount')}}</th>
    							<th width="100" class="text-right font-weight-bold text-muted text-uppercase">{{__('common.tax')}}</th>
    							<th width="100" class="text-right pr-0 font-weight-bold text-muted text-uppercase">{{__('common.total')}}</th>
    						</tr>
    					</thead>
    					<tbody>
                @php
                $taxRows='';
                $totalTax=0;
                $totalDiscount=0;
                $taxArr=getTaxArr();
                $i=1;
                $total = 0;
                $subTotal = 0;
                @endphp
                @if($model->items!=null)
                @foreach($model->items as $item)
                @php
                $totalDiscount+=$item->discount_amount;
                $itemTotal = $item->total;
                $itemTaxableAmount = $itemTotal;
                if($item->tax_id>0){
                  $itemTaxableAmount=$itemTotal+$item->tax_amount;
                  $totalTax+=$item->tax_amount;
                  //$taxRows.='<tr class="tax-item">';
                  //$taxRows.=' <td class="border-0 font-weight-bolder text-left">'.__('common.tax').' ('.$taxArr[$item->tax_id].'%)</td>';
                  //$taxRows.=' <td class="border-0 font-weight-bolder text-right pr-0"><span class="cursign">'.$model->getInvCurrency().'</span>'.$item->tax_amount.'</td>';
                  //$taxRows.='</tr>';
                }
                $itemTaxableAmount-=$item->discount_amount;
                $subTotal+= $item->total;
                $total+=$itemTaxableAmount;
                @endphp
    						<tr class="item-row font-weight-boldest{{$i>1 ? ' border-bottom-0' : ''}}">
    							<td class="pl-0 pt-7 d-flex align-items-center">
                    {{$item->title}}
                  </td>
    							<td class="align-middle">
                    {{nl2br($item->description)}}
                  </td>
    							<td class="text-right pt-7 align-middle">
                    {{$item->qty}}
                  </td>
    							<td class="text-right pt-7 align-middle">
                    {{$item->rate}}
                  </td>
    							<td class="text-right pt-7 align-middle">
                    <span data-toggle="tooltip" title="{{$item->discount_comments}}">{{$item->discount.''.($item->discount_type=='percentage' ? '%' : '')}}</span>
                  </td>
    							<td class="text-right pt-7 align-middle">
                    {{$item->tax_id>0 ? $taxArr[$item->tax_id].'%' : ''}}
                  </td>
    							<td class="text-primary pr-0 pt-7 text-right align-middle">
                    {{formatInputInvPrice($model,$itemTaxableAmount)}}
                  </td>
    						</tr>
                @php
                $i++;
                @endphp
                @endforeach
                @endif
    					</tbody>
    				</table>
            <div class="col-12 col-sm-5 float-right">
              <div class="bg-primary-o-10 p-2 text-right">
                <table class="table mb-0">
                  <tfoot>
                    <tr id="trst">
                      <td class="border-0 font-weight-bolder text-left">{{__('sales::invoices.sub_total')}}</td>
                      <td id="subtotal-amt" class="border-0 font-weight-bolder text-right pr-0">{!!formatInvPrice($model,$subTotal)!!}</td>
                    </tr>
                    <tr>
                      <td class="border-0 font-weight-bolder text-left">{{__('common.tax')}}</td>
                      <td id="tax-amt" class="border-0 font-weight-bolder text-right pr-0">{!!formatInvPrice($model,$totalTax)!!}</td>
                    </tr>
                    <tr>
                      <td class="border-0 font-weight-bolder text-left">{{__('common.discount')}}</td>
                      <td id="tax-amt" class="border-0 font-weight-bolder text-right pr-0">-{!!formatInvPrice($model,$totalDiscount)!!}</td>
                    </tr>
                    <tr>
                      <td class="border-0 font-weight-bolder font-size-h5 text-left">{{__('sales::invoices.grand_total')}}</td>
                      <td id="gtotal-amt" class="border-0 font-weight-bolder font-size-h5 text-success text-right pr-0">{!!formatInvPrice($model,$total)!!}</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
    			</div>
    		</div>
        <div class="row mb-3">
          <div class="col-12">
            <strong>{{__('sales::invoices.client_note')}}</strong><br />
            {!!nl2br($model->client_note)!!}
          </div>
        </div>
        <div class="row mb-3">
          <div class="col-12">
            <strong>{{__('sales::invoices.terms_text')}}</strong><br />
            {!!nl2br($model->terms_text)!!}
          </div>
        </div>
        @if($model->invoice_no==null)
        <div class="row mb-3">
          <div class="col-12">
            <center class="text-danger">
              <strong>{{__('sales::invoices.tax_invoice_after_payment')}}</strong>
            </center>
          </div>
        </div>
        @endif
        <x-customtags-custom-tags-field-details :model="$model"/>
      </div>
      <div class="tab-pane fade" role="tabpanel" aria-labelledby="tab-payments" id="tab-payments">
        <x-salescom-payments-list :req="$request" :url="'payments/data-table-data?invoice_id='.$model->id" />
      </div>
      <div class="tab-pane fade" role="tabpanel" aria-labelledby="tab-actions" id="tab-actions">
        <x-actionlog-activity-history :model="$model"/>
      </div>
      <div class="tab-pane fade" role="tabpanel" aria-labelledby="tab-attachments" id="tab-attachments">
        <x-actionlog-attachment :model="$model"/>
      </div>
      <div class="tab-pane fade" role="tabpanel" aria-labelledby="tab-actions" id="tab-actions">
        <x-actionlog-activity-history :model="$model"/>
      </div>
    </div>
  </div>
</div>
<x-actionlog-activity :model="$model"/>

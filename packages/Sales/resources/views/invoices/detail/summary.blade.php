<div class="d-flex mt-6">
  <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
    <div class="symbol symbol-50 symbol-lg-90">
      <img src="{{ ($model->logo<>null) ? $model->logo : asset('images/default-placeholder.png') }}" alt="image" />
    </div>
  </div>
  <div class="flex-grow-1">
    <div class="d-flex justify-content-between flex-wrap mt-1">
      <div class="d-flex mr-3">
        <p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->bill_to_name }}</p>
      </div>
    </div>
    <div class="d-flex flex-wrap justify-content-between mt-1">
      <div class="d-flex flex-column flex-grow-1 pr-8">
        <div class="d-flex flex-wrap ">
          <p class="text-dark-50 text-hover-primary font-weight-bold">
            <span class="text-muted font-weight-bold">
              {!! $model->bill_to_company_name !!}
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

@extends('layouts.app')
@section('title', __('sales::invoices.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'invoices'=>__('sales::invoices.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('sales::invoices._form')
@endsection

<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col mx-2">
			<div class="row mt-4">
				<input type="hidden" id="get_invoice_id" name="invoice_id" value="{{ $model->id }}">
				<div class="col-12 col-md-7" id="invoice_logo">
					<a href="/"><img src="https://d2a5i90cjeax12.cloudfront.net/BBG_NEW_WEBSITE_PLACEHOLDER_RGB_200x60px5f6a012c5ec34.jpg" alt="BBG-Dubai"></a>
				</div>
				<div class="col-12  col-md-5" id="invoice_date_number">

	<input type="hidden" name="id" id='get_invoice_id' value="{{ $model->id }}">

				<?php if ($model->invoice_no==null) { ?>
					<h1 style="color: #000;"><b>PROFORMA INVOICE</b></h1>
					<span><b>Invoice date: </b> {{ formatDateTime($model->p_invoice_date) }}</span><br>
					<span><b>Performa Invoice number:</b> {{ $model->p_reference_number }}</span>

				<?php	} if ($model->invoice_no!=null) {  ?>
					<h1 style="color: #000;"><b>Invoice</b></h1>
					<span><b>Invoice date: </b> {{ formatDateTime($model->invoice_date) }}</span><br>
					<span><b>Invoice number: </b> {{ $model->reference_number }}</span><br><br>
          <span><b>Proforma Invoice date: </b> {{ formatDateTime($model->p_invoice_date) }}</span><br>
					<span><b>Proforma Invoice number:</b> {{ $model->p_reference_number }}</span>
				<?php } ?>
				</div>
		 	</div>

			<div class="row mt-4">
				<div class="col-12 col-md-7 d-flex align-items-end mb-12" id="invoice_comapny_info">
					<div class="">
						<span><br><b>Telehephone:</b> +971 4 397 0303</span><br>
						<span><b>Email:</b> info@bbgdxb.com</span><br>
						<span><b>VAT Registration no: </b>
							<u>TRN 100001769700003</u></span><br>
					</div>
					</div>
					<div class="col-12 col-md-5 " id="invoice_client_info">
						<br>
						<h3>Bill To:</h3>
						<span><b>Name:</b> {{ $model->bill_to_name }}</span><br>
						<span><b>Company: </b>{{ $model->bill_to_company_name }} </span><br>
						<span><b>Address: </b>{{ $model->bill_to_company_address }} </span><br>
					</div>
			 </div>


					<div  class="col-3 col-lg-12 px-0 mt-4">
							<table class="table" cellpadding="5">
								<thead>
									<tr>
										<th class="col-md-8" style="width:90%; background: #1d355f; color: #ffffff; text-align: left;">Item
										</th>
										<th class="col-md-8" style="width:90%; background: #1d355f; color: #ffffff; text-align: left;">Discount
										</th>
										<th class="col-md-8" style="width:90%; background: #1d355f; color: #ffffff; text-align: left;">Tax
										</th>
										<th class="col-md-4" style="width:5%; background: #1d355f; color: #ffffff; text-align: center">Amount
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
                  $taxRows='';
									$totalTax = 0;
	                $totalDiscount=0;
                  $subTotal=0;
                  $total=0;
                  $taxArr=getTaxArr();
									if ($model <> null) {
										if ($model->items <> null) {
												foreach ($model->items as $key => $item) {
				                $totalDiscount+=$item->discount_amount;
		                    $itemTotal = $item->total;
		                    $itemTaxableAmount = $itemTotal;
		                    if($item->tax_id>0){
		                      $itemTaxableAmount=$itemTotal+$item->tax_amount;
														$totalTax+=$item->tax_amount;
		                      // $taxRows.='<tr class="tax-item">';
		                      // $taxRows.=' <td class="border-0 font-weight-bolder text-left">'.__('common.tax').' ('.$taxArr[$item->tax_id].'%)</td>';
		                      // $taxRows.=' <td class="border-0 font-weight-bolder text-right pr-0"><span class="cursign">'.$model->getInvCurrency().'</span>'.$item->tax_amount.'</td>';
		                      // $taxRows.='</tr>';
		                    }
				                $itemTaxableAmount-=$item->discount_amount;
		                    $subTotal+= $item->total;
		                    $total+=$itemTaxableAmount;
                          ?>
													<tr>
                            <td><?= $item->title; ?></td>
              							<td>
                              <span data-toggle="tooltip" title="{{$item->discount_comments}}">{{$item->discount.''.($item->discount_type=='percentage' ? '%' : '')}}</span>
                            </td>
              							<td>
                              {{$item->tax_id>0 ? $taxArr[$item->tax_id].'%' : ''}}
                            </td>
                            <td style="text-align: right;">{{formatInputInvPrice($model,$itemTaxableAmount)}}</td>
                          </tr>
										<?php
                  }		}		 }		?>
								</tbody>
							</table>
              <div class="col-12 col-sm-5 float-right">
                <div class="bg-primary-o-10 p-2 text-right">
                  <table class="table mb-0">
                    <tfoot>
                      <tr id="trst">
                        <td class="border-0 font-weight-bolder text-left">{{__('sales::invoices.sub_total')}}</td>
                        <td id="subtotal-amt" class="border-0 font-weight-bolder text-right pr-0">{!!formatInvPrice($model,$subTotal)!!}</td>
                      </tr>
                      <tr>
                        <td class="border-0 font-weight-bolder text-left">{{__('common.tax')}}</td>
                        <td id="subtotal-amt" class="border-0 font-weight-bolder text-right pr-0">{!!formatInvPrice($model,$totalTax)!!}</td>
                      </tr>
                      <tr>
                        <td class="border-0 font-weight-bolder text-left">{{__('common.discount')}}</td>
                        <td id="subtotal-amt" class="border-0 font-weight-bolder text-right pr-0">-{!!formatInvPrice($model,$totalDiscount)!!}</td>
                      </tr>
                      {!!$taxRows!!}
                      <tr>
                        <td class="border-0 font-weight-bolder font-size-h5 text-left">{{__('sales::invoices.grand_total')}}</td>
                        <td id="gtotal-amt" class="border-0 font-weight-bolder font-size-h5 text-success text-right pr-0">{!!formatInvPrice($model,$total)!!}</td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
						</div>


					<div class="mx-2">
						<a href="{{ url('pay-tabs/'.$model->id) }}">Pay Online</a><br>
						<p>&nbsp;</p>
						<p><strong>Below are the methods of payments acceptable by BBG:</strong></p>
						<p><strong>CHEQUE:</strong>&nbsp;Payable to British Business Group</p>
						<p><strong>BANK TRANSFER-(NET OF BANK CHARGES)</strong></p>
						<table width="98%" cellspacing="0" cellpadding="0" align="left">
							<tbody>
								<tr>
									<td width="25%">In favor&nbsp;of</td>
									<td width="1%">:</td>
									<td>British Business Group</td>
								</tr>
								<tr>
									<td width="25%">Bank</td>
									<td valign="top" width="1%">:</td>
									<td>HSBC Bank Middle East, P.O. Box 3766, Jumeirah 1, Dubai, UAE</td>
								</tr>
								<tr>
									<td width="25%">Account No</td>
									<td width="1%">:</td>
									<td>030-123756-001</td>
								</tr>
								<tr>
									<td width="25%">IBAN</td>
									<td width="1%">:</td>
									<td>AE880200000030123756001</td>
								</tr>
								<tr>
									<td width="25%">Swift Code</td>
									<td width="1%">:</td>
									<td>BBMEAEAD</td>
								</tr>
							</tbody>
						</table>
						<p>&nbsp;</p>
						<?php if ($model->invoice_no==null) { ?>
						<p style="color: red; font-size: 10px; text-align: center;"><span style="color: #ff0000;"><em><strong>*Note:&nbsp;</strong>Tax Invoice will be issued upon payment.</em></span></p>
					<?php }else{  ?>
						<br><br>
					<?php } ?>
						<p><br><br></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ($emailattach!=1) {?>
<div class="modal-footer">
	<button type="button" class="btn btn-default btn-danger" id="member_send_invoice">Send To Admin</button>
	<button type="button" class="btn btn-default btn-danger" id="member_send_invoice">Send Invoice</button>
	<button type="button" class="btn btn-default btn-danger" data-dismiss="modal" id="closeModalBox">Close</button>
</div>

<?php } ?>

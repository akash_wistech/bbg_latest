@php
$frmOptions=[];
@endphp

@if($request->ajax())
@php
$frmOptions['class']='simple-ajax-submit';
@endphp
@endif
{!! Form::model($model, $frmOptions) !!}
@php
$usersList=[];
$companyName = '';
if($model->company!=null){
  $companyName=$model->company->title;
  $usersList = getCompanyUsersList($model->company_id);
}
$invoiceRef = $model->p_reference_number;
if($model->reference_number!=null && $model->reference_number){
  $invoiceRef = $model->reference_number;
}

$compInputOpts = [];
if($model->id!=null){
  $compInputOpts['class']='form-control';
  $compInputOpts['readonly']='readonly';
}else{
  $compInputOpts['class']='autocomplete-company form-control';
  $compInputOpts['data-ds']=url('suggestion/module-lookup/company');
  $compInputOpts['data-mtype']=($model->module_type!='' ? $model->module_type : '');
  $compInputOpts['data-fld']='company_id';
  $compInputOpts['data-module']='contact';
  $compInputOpts['data-oldsval']=$model->user_id;
  $compInputOpts['data-sfld']='user_id';
}
$termsContent = $model->terms_text;
if($termsContent==null || $termsContent==''){
  $termsContent = getInvoicesTermsContent();
}
$invoiceTypes = getInvoiceTypesListArr();
@endphp
@if(!$request->ajax())
<div class="card card-custom tabs-card mb-2">
  <div class="card-body">
    @endif
    <div class="row">
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('company_id',__('common.company'),['class'=>'required']) }}
          {!! Form::text('customer_name', $companyName, $compInputOpts) !!}
          {{ Form::hidden('company_id', $model->company_id, ['id'=>'company_id','class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('user_id',__('common.user_id'),['class'=>'required']) }}
          {!! Form::select('user_id', $usersList, $model->user_id, ['id'=>'user_id', 'class' => 'form-control', 'required'=>'true']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('p_invoice_no',__('sales::invoices.invoice_no'),['class'=>'required']) }}
          {!! Form::text('p_invoice_no', $invoiceRef, ['class' => 'form-control', 'readonly' => 'readonly', 'autocomplete'=>'off', 'required'=>'true']) !!}
        </div>
      </div>
      @if($invoiceTypes!=null)
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('invoice_type',__('sales::invoices.invoice_type'),['class'=>'required']) }}
          {!! Form::select('invoice_type', $invoiceTypes, $model->invoice_type, ['class' => 'form-control', 'required'=>'true']) !!}
        </div>
      </div>
      @endif
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('p_invoice_date',__('sales::invoices.p_invoice_date'),['class'=>'required']) }}
          <div class="input-group date">
            {!! Form::text('p_invoice_date', $model->p_invoice_date, ['class' => 'form-control dtpicker', 'autocomplete'=>'off', 'required'=>'true']) !!}
            <div class="input-group-append">
              <div class="input-group-text"><i class="la la-calendar-check-o"></i></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('due_date',__('sales::invoices.due_date'),['class'=>'required']) }}
          <div class="input-group date">
            {!! Form::text('due_date', $model->due_date, ['class' => 'form-control dtpicker', 'autocomplete'=>'off', 'required'=>'true']) !!}
            <div class="input-group-append">
              <div class="input-group-text"><i class="la la-calendar-check-o"></i></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {!! Form::label('currency_id', __('common.currency')) !!}
          {!! Form::select('currency_id', getCurrenciesListArr(), $model->currency_id, ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {!! Form::label('is_recurring', __('sales::invoices.is_recurring')) !!}
          {!! Form::select('is_recurring', getIsRecurringOptArr(), $model->is_recurring, ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3 d-none">
        <div class="form-group">
          {!! Form::label('discount_calculation', __('sales::invoices.discount_calculation')) !!}
          {!! Form::select('discount_calculation', getDiscountCalculationArr(), $model->discount_calculation, ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group p1t-11">
          <div class="checkbox-inline">
						<label class="checkbox">
              {!! Form::checkbox('over_due_reminder', 1, ($model->over_due_reminder==1 ? true : false)) !!}
  						<span></span>{{__('sales::invoices.prevent_over_due_reminder')}}
            </label>
					</div>
        </div>
      </div>
    </div>
    <div class="form-group">
      {!! Form::label('admin_note', __('sales::invoices.admin_note')) !!}
      {!! Form::textarea('admin_note', $model->admin_note, ['class' => 'form-control', 'rows'=>4]) !!}
    </div>
    <div class="text-dark-50 line-height-md">
      <select id="item-selection" class="form-control selectpicker" data-live-search="true" title="{{__('common.select')}}">
        @foreach(getSalesItems() as $key=>$item)
        <optgroup label="{{$key}}">
          @foreach($item as $iKey=>$iVal)
          <option value="{{$iKey}}" data-content="">{{$iVal}}</option>
          @endforeach
        </optgroup>
        @endforeach
      </select>
			<div id="grid-table" class="table-responsive">
				<table id="inv-items" class="table">
					<thead>
						<tr>
							<th class="pl-0 font-weight-bold text-muted text-uppercase">Items</th>
							<th class="font-weight-bold text-muted text-uppercase">Description</th>
							<th width="70" class="text-center font-weight-bold text-muted text-uppercase">Qty</th>
							<th width="120" class="text-center font-weight-bold text-muted text-uppercase">Rate</th>
							<th width="140" class="text-center font-weight-bold text-muted text-uppercase">Discount</th>
							<th width="100" class="text-center font-weight-bold text-muted text-uppercase">Tax</th>
							<th width="120" class="text-center pr-0 font-weight-bold text-muted text-uppercase">Amount</th>
							<th width="25" class="text-center pr-0 font-weight-bold text-muted text-uppercase"><i class="fa fa-cog icon-sm"></i></th>
						</tr>
					</thead>
					<tbody>
            @php
            $taxArr=getTaxArr();
            $i=1;
            $total = 0;
            $subTotal = 0;
            @endphp
            @if($model->items!=null)
            @foreach($model->items as $item)
						<tr class="item-row font-weight-boldest{{$i>1 ? ' border-bottom-0' : ''}}">
							<td class="pl-0 pt-7 d-flex align-items-center align-top">
                <input id="inv_item_id-{{$i}}" name="inv_item_id[{{$i}}]" value="{{$item->id}}" type="hidden" />
                <input id="item_type-{{$i}}" name="item_type[{{$i}}]" value="{{$item->item_type}}" type="hidden" />
                <input id="item_id-{{$i}}" name="item_id[{{$i}}]" value="{{$item->item_id}}" type="hidden" />
                <div class="input-group input-group-sm">
                  <input id="title-{{$i}}" name="title[{{$i}}]" value="{{$item->title}}" type="text" class="form-control" />
                </div>
              </td>
							<td class="align-top">
                <div class="input-group input-group-sm">
                  <textarea id="descp-{{$i}}" name="descp[{{$i}}]" rows=3 type="text" class="form-control">{{$item->description}}</textarea>
                </div>
              </td>
							<td class="text-right pt-7 align-top">
                <div class="input-group input-group-sm">
                  <input id="qty-{{$i}}" name="qty[{{$i}}]" value="{{$item->qty}}" type="number" class="form-control qty calculateTotal" />
                </div>
              </td>
							<td class="text-right pt-7 align-top">
                <div class="input-group input-group-sm">
                  <input id="rate-{{$i}}" name="rate[{{$i}}]" value="{{$item->rate}}" type="number" class="form-control rate calculateTotal" />
                </div>
              </td>
							<td class="text-right pt-7 align-top">
                <div class="input-group input-group-sm">
                  <input id="discount-{{$i}}" name="discount[{{$i}}]" value="{{$item->discount}}" type="number" class="form-control discount calculateTotal" />
                  <div class="input-group-append">
                    <select id="discount_type-{{$i}}" name="discount_type[{{$i}}]" class="form-control discount-type calculateTotal">
                      <option value="percentage"{{$item->discount_type=='percentage' ? ' selected="selected"' : ''}}>%</option>
                      <option value="fixed"{{$item->discount_type=='fixed' ? ' selected="selected"' : ''}}>{{__('common.fixed')}}</option>
                    </select>
                  </div>
                </div>
              </td>
							<td class="text-right pt-7 align-top">
                <div class="input-group input-group-sm">
                  <select id="tax_id-{{$i}}" name="tax_id[{{$i}}]" class="form-control tax_id calculateTotal">
                    <option value="0">{{__('sales::invoices.no_tax')}}</option>
                    @foreach($taxArr as $key=>$val)
                    <option value="{{$key}}"{{$item->tax_id==$key ? ' selected' : ''}}>{{$val.'%'}}</option>
                    @endforeach
                  </select>
                </div>
              </td>
							<td class="text-primary pr-0 pt-7 text-right align-top">
                <div class="input-group input-group-sm">
                  <input id="row-total-{{$i}}" value="{{$item->total}}" readonly type="text" class="form-control rowTotal" />
                </div>
              </td>
							<td class="text-primary pr-0 pt-7 text-right align-top">
                <a href="{{url('invoices/delete-item/'.$model->id.'/'.$item->id)}}" class="btn btn-sm btn-icon btn-danger act-confirmation"  data-toggle="tooltip" title="{{__('common.delete')}}" data-id="{{$item->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                  <i class="fa fa-trash icon-1x"></i>
                </a>
              </td>
						</tr>
            @php
            $subTotal+= $item->total;
            $i++;
            @endphp
            @endforeach
            @endif
					</tbody>
          <tfoot>
            @php
            $total+=$model->getTaxableSubTotal();
            @endphp
						<tr>
							<td colspan="2"></td>
							<td colspan="3" class="font-weight-bolder text-right">{{__('sales::invoices.sub_total')}}</td>
							<td id="subtotal-amt" class="font-weight-bolder text-right pr-0">{!!formatInvPrice($model,$subTotal)!!}</td>
              <td></td>
						</tr>
            @php
            //echo 'Sub'.$total.'<br />';
            $discount = $model->getInvDiscount();
            $total-=$discount;
            //echo 'After Discount '.$total.'<br />';
            @endphp
						<tr>
							<td colspan="2" class="border-0 pt-0"></td>
							<td colspan="3" class="font-weight-bolder text-right">
                <div class="row">
                  <div class="col-5 pt-4">
                    {{__('sales::invoices.discount')}}
                  </div>
                  <div class="col-7">
                    <div class="input-group date">
                      {!! Form::number('discount', $model->discount, ['id'=>'inv-discount', 'class' => 'form-control calculateTotal', 'autocomplete'=>'off']) !!}
                      <div class="input-group-append">
                        <select id="discount_type" name="discount_type" class="form-control">
                          <option value="percentage"{{$model->discount_type=='percentage' ? ' selected' : ''}}>%</option>
                          <option value="fixed"{{$model->discount_type=='fixed' ? ' selected' : ''}}>Fixed</option>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
              </td>
							<td id="invdiscount-amt" class="font-weight-bolder text-right pr-0">{!!formatInvPrice($model,-$discount)!!}</td>
              <td></td>
						</tr>
            @php
            $total+=$model->adjustment;
            //echo 'After Adjustment '.$total.'<br />';
            @endphp
						<tr>
							<td colspan="2" class="border-0 pt-0"></td>
							<td colspan="3" class="font-weight-bolder text-right">
                <div class="row">
                  <div class="col-7 pt-4">
                    {{__('sales::invoices.adjustment')}}
                  </div>
                  <div class="col-5">
                    {!! Form::number('adjustment', $model->adjustment, ['id'=>'inv-adjustment', 'class' => 'form-control calculateTotal', 'autocomplete'=>'off']) !!}
                  </div>
                </div>
              </td>
							<td id="invadjustment-amt" class="font-weight-bolder text-right pr-0">{!!formatInvPrice($model,$model->adjustment)!!}</td>
              <td></td>
						</tr>
						<tr>
							<td colspan="2" class="border-0 pt-0"></td>
							<td colspan="3" class="font-weight-bolder font-size-h5 text-right">{{__('sales::invoices.grand_total')}}</td>
							<td id="gtotal-amt" class="font-weight-bolder font-size-h5 text-success text-right pr-0">{!!formatInvPrice($model,$total)!!}</td>
              <td class=""></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
    <div class="form-group">
      {!! Form::label('client_note', __('sales::invoices.client_note')) !!}
      {!! Form::textarea('client_note', $model->client_note, ['class' => 'form-control', 'rows'=>4]) !!}
    </div>
    <div class="form-group">
      {!! Form::label('terms_text', __('sales::invoices.terms_text')) !!}
      {!! Form::textarea('terms_text', $termsContent, ['class' => 'form-control editor', 'rows'=>4]) !!}
    </div>
    @if(!$request->ajax())
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/invoices') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
@else
<div class="card-footer">
  <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
  <a href="{{ url('/invoices') }}" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
@endif
{!! Form::close() !!}

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
@endpush
@push('jsFunc')
var prdservArr = {!!json_encode(getSalesItemsWithDetail())!!}
var taxArr = {!!json_encode(getTaxArr())!!}
varInvRows = {{$i}};
function generateInvItemRow(itemInfo)
{
  html = '';
  html+= '<tr class="item-row tmp-row font-weight-boldest'+(varInvRows>1 ? ' border-bottom-0' : '')+'">';
  html+= '  <td class="pl-0 pt-7 d-flex align-items-center align-top">';
  html+= '    <input id="item_type-'+varInvRows+'" name="item_type['+varInvRows+']" value="'+itemInfo.item_type+'" type="hidden" />';
  html+= '    <input id="item_id-'+varInvRows+'" name="item_id['+varInvRows+']" value="'+itemInfo.item_id+'" type="hidden" />';
  html+= '    <div class="input-group input-group-sm">';
  html+= '      <input id="title-'+varInvRows+'" name="title['+varInvRows+']" value="'+itemInfo.title+'" type="text" class="form-control" />';
  html+= '    </div>';
  html+= '  </td>';
  html+= '  <td class="align-top">';
  html+= '    <div class="input-group input-group-sm">';
  html+= '      <textarea id="descp-'+varInvRows+'" name="descp['+varInvRows+']" rows=3 type="text" class="form-control ">'+itemInfo.descp+'</textarea>';
  html+= '    </div>';
  html+= '  </td>';
  html+= '  <td class="text-right pt-7 align-top">';
  html+= '    <div class="input-group input-group-sm">';
  html+= '      <input id="qty-'+varInvRows+'" name="qty['+varInvRows+']" value="1" type="number" class="form-control qty calculateTotal" />';
  html+= '    </div>';
  html+= '  </td>';
  html+= '  <td class="text-right pt-7 align-top">';
  html+= '    <div class="input-group input-group-sm">';
  html+= '      <input id="rate-'+varInvRows+'" name="rate['+varInvRows+']" value="'+itemInfo.rate+'" type="number" class="form-control rate calculateTotal" />';
  html+= '    </div>';
  html+= '  </td>';
  html+= '  <td class="text-right pt-7 align-top">';
  html+= '    <div class="input-group input-group-sm">';
  html+= '      <input id="discount-'+varInvRows+'" name="discount['+varInvRows+']" value="0" type="number" class="form-control rate calculateTotal" />';
  html+= '      <div class="input-group-append">';
  html+= '        <select id="discount_type-'+varInvRows+'" name="discount_type['+varInvRows+']" class="form-control discount">';
  html+= '          <option value="percentage">%</option>';
  html+= '          <option value="fixed">{{__('common.fixed')}}</option>';
  html+= '        </select>';
  html+= '      </div>';
  html+= '    </div>';
  html+= '  </td>';
  html+= '  <td class="text-right pt-7 align-top">';
  html+= '    <div class="input-group input-group-sm">';
  html+= '    <select id="tax_id-'+varInvRows+'" name="tax_id['+varInvRows+']" class="form-control tax_id calculateTotal">';
  html+= '      <option value="0">{{__('sales::invoices.no_tax')}}</option>';
  @foreach($taxArr as $key=>$val)
  html+= '      <option value="{{$key}}">{{$val.'%'}}</option>';
  @endforeach
  html+= '    </select>';
  html+= '    </div>';
  html+= '  </td>';
  html+= '  <td class="text-primary pr-0 pt-7 text-right align-top">';
  html+= '    <div class="input-group input-group-sm">';
  html+= '      <input id="row-total-'+varInvRows+'" value="" readonly type="text" class="form-control rowTotal" />';
  html+= '    </td>';
  html+= '  </td>';
  html+= '  <td class="text-primary pr-0 pt-7 text-right align-top">';
  html+= '    <a href="javascript:;" class="btn btn-block btn-icon btn-sm btn-danger del-temp-row"><i class="fa fa-trash icon-1x"></i></a>';
  html+= '  </td>';
  html+= '</tr>';
  varInvRows++;
  calculateItems();
  $("#inv-items tbody").append(html);
}
function calculateItems()
{
  subTotal = 0;
  totalAmt = 0;
  discount=0;
  disacountableAmount = 0;
  taxAmount = [];
  $('.item-row').each(function(i, obj) {
    _t = $(this);
    qty = _t.find("input.qty").val();
    rate = _t.find("input.rate").val();
    taxId = _t.find("select.tax_id").val();
    tax = taxArr[taxId];
    rowTotal = qty*rate;

    subTotal+=rowTotal;

    rowWithTaxAmount = rowTotal;

    if(taxId>0){
      thisRowTax = rowTotal * (tax/100);
      taxAmount[tax+'%'] = thisRowTax;
      rowWithTaxAmount=rowTotal+thisRowTax;
    }

    _t.find("input.rowTotal").val(rowTotal);

    totalAmt+=rowWithTaxAmount;
  });

  console.log(taxAmount);
  $("#subtotal-amt .amt").html(subTotal);

  discount = calculateDiscount(totalAmt)
  if($("#inv-discount")!=''){
    discountAmt = parseFloat($("#inv-discount").val());
    if(discountAmt>0){
      discountType = $("#discount_type").val();
      if(discountType=='fixed'){
        discount = discountAmt;
      }else{
        discount = totalAmt * (discountAmt/100);
      }
    }
  }

  totalAmt-=discount;
  $("#invdiscount-amt .amt").html('-'+discount);

  adjustment=0;
  if($("#inv-adjustment").val()){
    adjustment = parseFloat($("#inv-adjustment").val());
  }
  totalAmt+=adjustment;
  $("#invadjustment-amt .amt").html(adjustment);

  $("#gtotal-amt .amt").html(totalAmt);
}
function calculateDiscount(total)
{
  discount = 0;
  if($("#inv-discount")!=''){
    discountAmt = parseFloat($("#inv-discount").val());
    if(discountAmt>0){
      discountType = $("#discount_type").val();
      if(discountType=='fixed'){
        discount = discountAmt;
      }else{
        discount = total * (discountAmt/100);
      }
    }
  }
  return discount;
}
function initPageScripts()
{
  calculateItems();
}
@endpush()
@push('jsScripts')
tinymce.init({
  selector: '.editor',
  width:'100%',
  height : "180",
  menubar: false,
  toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
  plugins : 'advlist autolink link image lists charmap code table',
  image_advtab: true ,
  external_filemanager_path:"/filemanager/",
  filemanager_title:"Choose File" ,
  external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
});
$("body").on("change", "#item-selection", function (e) {
  var item = $(this).val();
  itemInfo = prdservArr[item];
  generateInvItemRow(itemInfo);
});
$("body").on("blur", ".calculateTotal", function (e) {
  calculateItems();
});
$("body").on("change", ".calculateTotal,#inv-adjustment", function (e) {
  calculateItems();
});
$("body").on("change", "#inv-discount,#discount_type", function (e) {
  if($("#discount_calculation").val()=="na"){
    swal({title: "{{__('common.info')}}", html: "{{__('sales::invoices.select_discount_calculation')}}", type: "info"});
  }else{
    calculateItems();
  }
});
$("body").on("click", ".del-temp-row", function (e) {
  _this = $(this);
  swal({
    title: "{{__('common.confirmation')}}",
    html: "{{__('common.deleteconfirmation')}}",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "{{__('common.confirm')}}",
    cancelButtonText: "{{__('common.cancel')}}",
  }).then((result) => {
    if (result.value) {
      _this.parent('tr.tmp-row').delete();
      calculateItems();
    }else{
      return false;
    }
  });
});
@endpush()

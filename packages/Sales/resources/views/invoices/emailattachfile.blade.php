<?php





// dd($invoice);

// $type = ($invoice <> null)  ? ($invoice->invoice_related_to == "event") ? "E-" : "PINV-00" : "";
$_invoice_id = ($invoice <> null)  ? $invoice->id : 0;

$inv_subtotal = 0;
$inv_tax = 0;
$inv_total = 0;
// $discount_type = $invoice->discount_type;
if($invoice <> null){
    $inv_subtotal = $invoice->sub_total;
    // $inv_tax = $invoice->tax;
    // $discount = $invoice->discount;
    // $adjustment = $invoice->adjustment;
    $inv_total = $invoice->grand_total;
}


 ?>





<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col mx-2">
			<div class="row mt-4">
				<div class="col-12 col-md-7" id="invoice_logo">
					<a href="/"><img src="https://d2a5i90cjeax12.cloudfront.net/BBG_NEW_WEBSITE_PLACEHOLDER_RGB_200x60px5f6a012c5ec34.jpg" alt="BBG-Dubai"></a>
				</div>
				<div class="col-12  col-md-5" id="invoice_date_number">


				<?php if ($invoice->reference_number==null) { ?>
					<h1 style="color: #000;"><b>PRO-FORMA INVOICE</b></h1>
					<span><b>Invoice date: </b> {{ formatDateTime($invoice->p_invoice_date) }}</span><br>
					<span><b>Performa Invoice number:</b> {{ $invoice->p_reference_number }}</span>

				<?php	} if ($invoice->reference_number!=null) {  ?>
					<h1 style="color: #000;"><b>Tax Invoice</b></h1>
					<span><b>Invoice date: </b> {{ formatDateTime($invoice->invoice_date) }}</span><br>
					<span><b>Tax Invoice number: </b> {{ $invoice->reference_number }}</span>
				<?php } ?>
				</div>
		 	</div>

			<div class="row mt-4">
				<div class="col-12 col-md-7 d-flex align-items-end mb-12" id="invoice_comapny_info">
					<div class="">
						<span><br><b>Telehephone:</b> +971 4 397 0303</span><br>
						<span><b>Email:</b> info@bbgdxb.com</span><br>
						<span><b>VAT Registration no: </b>
							<u>TRN 100001769700003</u></span><br>
					</div>
					</div>
					<div class="col-12 col-md-5 " id="invoice_client_info">
						<br>
						<h3>Bill To:</h3>
						<!-- <span><b>Name:</b> kiran arif(Member)</span><br> -->
						<span><b>Company: </b>{{ $company->title }} </span><br>
						<span><b>Postal Code: </b> <?php //echo $company->postal_code ?> <br></span>
						<span><b>Address: </b> <?php //echo $company->address ?><br></span>
						<span><b>Telephone: </b> <?php //echo $company->phonenumber ?></span><br>
						<span><b>VAT Registration no: </b> <?php //echo $company->vat_number ?></span><br>
					</div>
			 </div>


					<div  class="col-3 col-lg-12 px-0 mt-4">
							<table class="table" cellpadding="5">
								<thead>
									<tr>
										<th class="col-md-8" style="width:90%; background: #1d355f; color: #ffffff; text-align: left;">Item
										</th>
										<th class="col-md-4" style="width:5%; background: #1d355f; color: #ffffff; text-align: center">Amount
										</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if ($invoice <> null) {
										if ($invoice->items <> null) {
												foreach ($invoice->items as $key => $item) {
                          ?>
													<tr>
                            <td><?= $item->title.'('.$item->item_type.')'; ?></td>
                            <td style="text-align: right;">AED <?= $item->total; ?></td>
                          </tr>
										<?php	}		}		 }		?>

									<tr>
										<td style="width:70%;text-align: left;">
											<strong>Subtotal</strong>
                      <!-- <br><strong>Discount</strong>
											<br><strong>Adjustment</strong> -->
											<br><strong>VAT(5%)</strong>
										</td>
										<td style="width:30%;text-align: right;">
                        <strong><?= $inv_subtotal; ?></strong>
                        <!-- <br><strong><?php //echo'-'.$discount.(($discount_type=='percentage')?'%' : 0); ?></strong> -->
                        <!-- <br><strong><?php  //echo $adjustment; ?></strong> -->
                        <br/><strong><?php //$inv_tax; ?></strong>
                    </td>
									</tr>

									<tr>
										<td style="width:70%;text-align: left;">
											<br><strong>Total</strong>
										</td>
										<td style="width:30%;text-align: right;  padding-right:0px;">
											<br>
											<strong>
													AED <span id="before_adjust_total">
																			<?= $inv_total ?>
																	</span>
													<input type="hidden"
																 value="<?= $inv_total ?>"
																 id="actual_total"/>
											</strong>
											</strong>
										</td>
									</tr>
								</tbody>
							</table>
						</div>


					<div class="mx-2">
						<!-- <a href="https://bbgdubai.org/site/invoice-payment?invoice_id=11506">Pay Online</a><br> -->
						<p>&nbsp;</p>
						<p><strong>Below are the methods of payments acceptable by BBG:</strong></p>
						<p><strong>CHEQUE:</strong>&nbsp;Payable to British Business Group</p>
						<p><strong>BANK TRANSFER-(NET OF BANK CHARGES)</strong></p>
						<table width="98%" cellspacing="0" cellpadding="0" align="left">
							<tbody>
								<tr>
									<td width="25%">In favor&nbsp;of</td>
									<td width="1%">:</td>
									<td>British Business Group</td>
								</tr>
								<tr>
									<td width="25%">Bank</td>
									<td valign="top" width="1%">:</td>
									<td>HSBC Bank Middle East, P.O. Box 3766, Jumeirah 1, Dubai, UAE</td>
								</tr>
								<tr>
									<td width="25%">Account No</td>
									<td width="1%">:</td>
									<td>030-123756-001</td>
								</tr>
								<tr>
									<td width="25%">IBAN</td>
									<td width="1%">:</td>
									<td>AE880200000030123756001</td>
								</tr>
								<tr>
									<td width="25%">Swift Code</td>
									<td width="1%">:</td>
									<td>BBMEAEAD</td>
								</tr>
							</tbody>
						</table>
						<p>&nbsp;</p>
						<?php if ($invoice->reference_number==null) { ?>
						<p style="color: red; font-size: 10px; text-align: center;"><span style="color: #ff0000;"><em><strong>*Note:&nbsp;</strong>Tax Invoice will be issued upon payment.</em></span></p>
					<?php }else{  ?>
						<br><br>
					<?php } ?>
						<p><br><br></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>

@extends('layouts.app')
@section('title', __('sales::invoices.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'invoices'=>__('sales::invoices.heading'),
'invoices/update/'.$model->id=>__('common.update_rec',['name'=>$model->reference_no]),
__('common.view')
]])
@endsection
@php
$companyNames = getRowMultipleCompanies($model->moduleTypeId,$model->id);

$tabs=[];

if($model->company!=null){
  $summary = view('company.detail.summary',['model'=>$model->company]);
}else{
  $summary = view('sales::invoices.detail.summary',['model'=>$model]);
}
$tabs = view('sales::invoices.detail.tabs',compact('model'));
$overview = view('sales::invoices.detail.overview',compact('model','request'));
@endphp

@section('content')

<x-view-card :model="$model" :overview="$overview" :summary="$summary" :tabs="$tabs" />

@endsection

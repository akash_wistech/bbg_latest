@extends('layouts.app')
@section('title', __('sales::payments.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'payments'=>__('sales::payments.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('sales::payments._full_form')
@endsection

@extends('layouts.app')
@section('title', __('sales::payments.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'payments'=>__('sales::payments.heading'),
__('common.view')
]])
@endsection

@section('content')
@php
$usersList=[];
$companyName='';
if($model->company!=null){
  $companyName =$model->company->title;
  $usersList = getCompanyUsersList($model->company_id);
}
$invoiceRef = $model->reference_number;

$compInputOpts = [];
if($model->id!=null){
  $compInputOpts['class']='form-control';
  $compInputOpts['readonly']='readonly';
}else{
  $compInputOpts['class']='autocomplete-company form-control';
  $compInputOpts['data-ds']=url('suggestion/module-lookup/company');
  $compInputOpts['data-mtype']=($model->module_type!='' ? $model->module_type : '');
  $compInputOpts['data-fld']='company_id';
  $compInputOpts['data-module']='contact';
  $compInputOpts['data-oldsval']=$model->user_id;
  $compInputOpts['data-sfld']='user_id';
}
@endphp
<div class="card card-custom tabs-card mb-2">
  <div class="card-body">
    <div class="row">
      <div class="col-12 col-sm-3">
        <div class="form-group">
          <strong>{{ Form::label('company_id',__('common.company')) }}</strong><br />
          {!! $companyName !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          <strong>{{ Form::label('user_id',__('common.user_id')) }}</strong><br />
          {!! $usersList[$model->user_id] !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          <strong>{{ Form::label('payment_date',__('sales::payments.payment_date')) }}</strong><br />
          {!! formatDate($model->payment_date) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          <strong>{{ Form::label('amount_received',__('sales::payments.amount_received')) }}</strong><br />
          {!! $model->amount_received !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          <strong>{{ Form::label('payment_method',__('common.payment_method')) }}</strong><br />
          {!! getPaymentMethods()[$model->payment_method] !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          <strong>{{ Form::label('transaction_id',__('common.transaction_id')) }}</strong><br />
          {!! $model->transaction_id !!}
        </div>
      </div>
      @if($model->send_notification==1)
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{__('sales::payments.send_notification')}}
        </div>
      </div>
      @endif
    </div>
    <div class="form-group">
      <strong>{!! Form::label('note', __('common.note')) !!}</strong><br />
      {!! nl2br($model->note) !!}
    </div>
    <div id="pmt-details">
      @if($model->items!=null && count($model->items)>0)
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>{{__('common.date')}}</th>
            <th>{{__('sales::invoices.invoice_no')}}</th>
            <th>{{__('common.amount')}}</th>
            <th>{{__('sales::payments.amount_due')}}</th>
            <th>{{__('common.discount')}}</th>
            <th>{{__('sales::payments.received')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($model->items as $item)
          <tr class="item-row">
            <td>
              {{formatDate($item->invoice->final_invoice_date)}}
            </td>
            <td>{{$item->invoice->reference_no}}</td>
            <td>{{$item->invoice->grand_total}}</td>
            <td>
                {{$item->amount_due}}
            </td>
            <td>
                {{$item->discount}}
            </td>
            <td>
                {{$item->amount_paid}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @endif
    </div>
  </div>
</div>
@endsection

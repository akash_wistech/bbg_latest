{!! Form::model($model, []) !!}
@php
$usersList=[];
$companyName='';
if($model->company!=null){
  $companyName =$model->company->title;
  $usersList = getCompanyUsersList($model->company_id);
}
$invoiceRef = $model->reference_number;

$compInputOpts = [];
if($model->id!=null){
  $compInputOpts['class']='form-control';
  $compInputOpts['readonly']='readonly';
}else{
  $compInputOpts['class']='autocomplete-company form-control';
  $compInputOpts['data-ds']=url('suggestion/module-lookup/company');
  $compInputOpts['data-mtype']=($model->module_type!='' ? $model->module_type : '');
  $compInputOpts['data-fld']='company_id';
  $compInputOpts['data-module']='contact';
  $compInputOpts['data-oldsval']=$model->user_id;
  $compInputOpts['data-sfld']='user_id';
}
@endphp
<div class="card card-custom tabs-card mb-2">
  <div class="card-body">
    <div class="row">
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('company_id',__('common.company'),['class'=>'required']) }}
          {!! Form::text('customer_name', $companyName, $compInputOpts) !!}
          {{ Form::hidden('company_id', $model->company_id, ['id'=>'company_id','class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('user_id',__('common.user_id'),['class'=>'required']) }}
          {!! Form::select('user_id', $usersList, $model->user_id, ['id'=>'user_id', 'class' => 'form-control', 'required'=>'true']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('payment_date',__('sales::payments.payment_date'),['class'=>'required']) }}
          <div class="input-group date">
            {!! Form::text('payment_date', $model->payment_date, ['class' => 'form-control dtpicker', 'autocomplete'=>'off', 'required'=>'true']) !!}
            <div class="input-group-append">
              <div class="input-group-text"><i class="la la-calendar-check-o"></i></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('amount_received',__('sales::payments.amount_received'),['class'=>'required']) }}
          {!! Form::text('amount_received', $model->amount_received, ['id'=>'total-rec', 'class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('payment_method',__('common.payment_method'),['class'=>'required']) }}
          {!! Form::select('payment_method', ['0'=>__('common.select')]+getPaymentMethods(), $model->payment_method, ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-3">
        <div class="form-group">
          {{ Form::label('transaction_id',__('common.transaction_id'),['class'=>'required']) }}
          {!! Form::text('transaction_id', $model->transaction_id, ['class' => 'form-control', 'autocomplete'=>'off']) !!}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group pt-11">
          <div class="checkbox-inline">
            <label class="checkbox">
              {!! Form::checkbox('send_notification', 1, ($model->send_notification==1 ? true : false)) !!}
              <span></span>{{__('sales::payments.send_notification')}}
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      {!! Form::label('note', __('common.note')) !!}
      {!! Form::textarea('note', $model->note, ['class' => 'form-control', 'rows'=>4]) !!}
    </div>
    <div id="pmt-details">
      @if($model->items!=null && count($model->items)>0)
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>{{__('common.date')}}</th>
            <th>{{__('sales::invoices.invoice_no')}}</th>
            <th>{{__('common.amount')}}</th>
            <th>{{__('sales::payments.amount_due')}}</th>
            <th>{{__('common.discount')}}</th>
            <th>{{__('sales::payments.received')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($model->items as $item)
          <tr class="item-row">
            <td>
              {{ Form::hidden('module[]', $item->module_type.'-'.$item->module_id) }}
              {{ Form::hidden('pmt_item_id['.$item->module_type.']['.$item->module_id.']', $item->id) }}

              {{formatDate($item->invoice->final_invoice_date)}}
            </td>
            <td>{{$item->invoice->reference_no}}</td>
            <td>{{$item->invoice->grand_total}}</td>
            <td>
              @if($item->invoice->payment_status!=1)
                {!! Form::text('amount_due['.$item->module_type.']['.$item->module_id.']', $item->amount_due, ['class' => 'form-control amt-due', 'readonly'=>'readonly']) !!}
              @else
                {{$item->amount_due}}
              @endif
            </td>
            <td>
              @if($item->invoice->payment_status!=1)
                {!! Form::text('discount['.$item->module_type.']['.$item->module_id.']', $item->discount, ['class' => 'form-control discfld', 'autocomplete'=>'off', 'required'=>'true']) !!}
              @else
                {{$item->discount}}
              @endif
            </td>
            <td>
              @if($item->invoice->payment_status!=1)
                {!! Form::text('amount_paid['.$item->module_type.']['.$item->module_id.']', $item->amount_paid, ['class' => 'form-control amt-paid calcttl', 'autocomplete'=>'off', 'required'=>'true']) !!}
              @else
                {{$item->amount_paid}}
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @endif
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/payments') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}
@push('jsScripts')
varAlertShown=false;
$("body").on("click", ".tax_invoice", function (e) {
  if($(this).is(":checked")) {
    if(varAlertShown==false){
      swal({title: "{{__('common.note')}}", html: "{{__('sales::invoices.tax_action_not_reversable')}}", type: "info"});
      varAlertShown=true;
    }
  }
});
$("body").on("change", ".calcttl", function (e) {
  calculateTotalPaid();
});
$("body").on("change", ".discfld", function (e) {
  _t = $(this);
  amtDue = parseFloat(_t.parents("tr.item-row").find(".amt-due").val());
  discount = parseFloat(_t.parents("tr.item-row").find(".discfld").val());
  rowTotal = amtDue-discount;
  _t.parents("tr.item-row").find(".amt-paid").val(rowTotal);
  calculateTotalPaid();
});
@endpush()
@push('jsFunc')
@if($model->id==null)
function compSelUpdated(module,id)
{
  detContainer = 'pmt-details';
  myApp.block('#'+detContainer, {
   overlayColor: '#000000',
   state: 'danger',
   message: "{{__('common.please_wait')}}"
  });
  url = "{{url('invoices/customer-invoices')}}/"+id;
  $.ajax({
    url: url,
    type: 'POST',
    dataType: "json",
    success: function(response) {
      if(response['success']){
        $("#"+detContainer).html(response["success"]["invoicesHtml"]);
      }else{
        $("#"+detContainer).html(response["error"]["invoicesHtml"]);
      }
      myApp.unblock('#'+detContainer);
    },
  });
}
@endif
function calculateTotalPaid(){
  totalAmt = 0;
  $('.item-row').each(function(i, obj) {
    _t = $(this);
    amt = parseFloat(_t.find("input.amt-paid").val());
    totalAmt+=amt;
  });
  console.log(totalAmt);
  $("#total-rec").val(totalAmt);
}
@endpush()

@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/payments/create', 'method'=>'post'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];
$advSearchCols = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];

$dtColsArr[]=['title'=>__('sales::payments.payment_date'),'data'=>'payment_date','name'=>'payment_date'];
$dtColsArr[]=['title'=>__('common.name'),'data'=>'name','name'=>'name'];

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('sales::payments.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('sales::payments.heading')
]])
@endsection


@section('content')

<!-- $dtColsArr[]=['title'=>__('common.reference_no'),'data'=>'reference_no','name'=>'reference_no']; -->
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :showStats="true" :controller="'payments'" :jsonUrl="'payments/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="true" :advSrchColArr="$advSearchCols" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>
@endsection

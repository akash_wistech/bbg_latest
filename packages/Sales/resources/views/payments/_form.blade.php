@extends('layouts.blank')
@section('content')
{!! Form::model($model, ['class' => 'simple-ajax-submit', 'data-pjxcntr'=>'invoice-preview', 'data-blockcntr'=>'general-modal']) !!}
@php
$customerName = '';
if($model->user!=null){
  $customerName.= $model->user->name;
}
if($model->company!=null){
  if($customerName!='')$customerName.=' ('.$model->company->title.')';
}
$invoiceRef = $model->reference_number;
@endphp
<div class="row">
  <div class="col-12 col-sm-3">
    <div class="form-group">
      {{ Form::label('amount_received',__('sales::payments.amount_received'),['class'=>'required']) }}
      {!! Form::text('amount_received', $model->amount_received, ['class' => 'form-control', 'autocomplete'=>'off', 'required'=>'true']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-3">
    <div class="form-group">
      {{ Form::label('payment_date',__('sales::payments.payment_date'),['class'=>'required']) }}
      <div class="input-group date">
        {!! Form::text('payment_date', $model->payment_date, ['class' => 'form-control dtpicker', 'autocomplete'=>'off', 'required'=>'true']) !!}
        <div class="input-group-append">
          <div class="input-group-text"><i class="la la-calendar-check-o"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-3">
    <div class="form-group">
      {{ Form::label('payment_method',__('common.payment_method'),['class'=>'required']) }}
      {!! Form::select('payment_method', ['0'=>__('common.select')]+getPaymentMethods(), $model->payment_method, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-3">
    <div class="form-group">
      {{ Form::label('transaction_id',__('common.transaction_id'),['class'=>'required']) }}
      {!! Form::text('transaction_id', $model->transaction_id, ['class' => 'form-control', 'autocomplete'=>'off']) !!}
    </div>
  </div>
  <div class="col-12 col-sm-8">
    <div class="form-group">
      <div class="checkbox-inline">
        <label class="checkbox">
          {!! Form::checkbox('send_notification', 1, ($model->send_notification==1 ? true : false)) !!}
          <span></span>{{__('sales::payments.send_notification')}}
        </label>
      </div>
    </div>
  </div>
</div>
<div class="form-group">
  {!! Form::label('note', __('common.note')) !!}
  {!! Form::textarea('note', $model->note, ['class' => 'form-control', 'rows'=>4]) !!}
</div>
<div>
  <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
  <a href="{{ url('/payments') }}" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
@endsection
{!! Form::close() !!}
@push('jsScripts')
varAlertShown=false;
$("body").on("click", "#tax_invoice", function (e) {
  if($("#tax_invoice").is(":checked")) {
    if(varAlertShown==false){
      swal({title: "{{__('common.note')}}", html: "{{__('sales::invoices.tax_action_not_reversable')}}", type: "info"});
      varAlertShown=true;
    }
  }
});
@endpush()

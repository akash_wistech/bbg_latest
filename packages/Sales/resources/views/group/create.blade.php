@extends('layouts.app')


@section('title', __('scopedocument::scopedocument.group.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'group'=>__('scopedocument::scopedocument.group.heading'),
__('scopedocument::scopedocument.group.create')
]])
@endsection
@section('content')


{{Form::model($model, array('route' => array('group.store')))}}

@csrf
@include('group::group._form')
{{ Form::close() }}


@endsection

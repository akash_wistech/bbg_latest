@extends('layouts.app')

@section('title', __('scopedocument::scopedocument.group.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'group'=>__('scopedocument::scopedocument.group.heading'),
__('scopedocument::scopedocument.group.view')
]])
@endsection


@section('content')

<?php //dd($model); ?>

<style>
  .heared-outline{
  border-top:3px solid #26A69A;
}
</style>

<section class="card card-custom with-activity">
  <header class="card-header heared-outline">
    <div class="card-title">
      <h3 class="card-label">{{ $model->title }}</h3>
    </div>
    @if(checkActionAllowed('update','group'))
    <div class="card-toolbar">
      <a href="{{ url('/group/edit',['id'=>$model->id]) }}" class="btn btn-icon btn-sm btn-hover-light-primary">
        <i class="flaticon-edit-1"></i>
      </a>
    </div>
    @endif
  </header>
  <div class="card-body multi-cards multi-tabs">
    <div class="row">
      <div class="col-sm-12">
        <section class="card card-custom card-border mb-3" data-card="true">
          <header class="card-header heared-outline">
            <div class="card-title">
              <h3 class="card-label">{{ __('common.info') }}</h3>
            </div>
            <div class="card-toolbar">
              <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
                <i class="ki ki-arrow-down icon-nm"></i>
              </a>
            </div>
          </header>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <strong>{{ __('common.created:') }}</strong> {{ formatDateTime($model->created_at) }}
              </div>
              <div class="col-sm-12 my-2">
                <strong>Templates: </strong> 
                @foreach($results as $value)
                		{{$value->title}},
                @endforeach
              </div>
            </div>







          </div>
        </section>
      </div>
    </div>
  </div>
</section>
@endsection
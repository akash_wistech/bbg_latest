<?php 
use Illuminate\Support\Facades\DB;
use Wisdom\ScopeDocument\Models\GroupChild;

if ($model!=null) {
  $existing_templates = getExistingTempGroup($model->id); 
  // echo "<pre>"; print_r($existing_templates); echo "</pre>"; die();         
}
?>

<style>
  .heared-outline{
  /*border-top:3px solid #26A69A;*/
}
</style>

@if($request->ajax())

{{Form::model($model, array('route' => array('group.store'),'class'=>'simple-ajax-submit'))}}
@csrf
@endif

<div class="card card-custom">
    <div class="card-header heared-outline">
    <div class="card-title">
      <h3 class="card-label">Group</h3>
    </div>
  </div>
  <div class="card-body">
    
    <div class="row">
      <div class="col-12 col-sm-6">
        <div class="form-group">
         {{ Form::label('title', 'Title')}}<span class="text-danger">*</span>
         {{ Form::text('title', null, ['class'=> 'form-control'])}}
         <span style="color: red;">@error('title'){{$message}} @enderror</span>
       </div>
     </div>
     <div class="col-12 col-sm-6">
      <div class="form-group">
        {{ Form::label('status', 'Status')}}
        {{ Form::select('status', getStatusArr(), null, ['class'=> 'form-control'])}}
      </div>
    </div>
  </div>




<div class="row">
 <div class="col-12 col-sm-12">
  <div class="form-group">
    {{ Form::label('template_id', 'Templates')}}<span class="text-danger">*</span>
    {{ Form::select('template_id[]',gettemplatesArr(), $existing_templates, [
    'class'=> 'form-control select2',
    'multiple' => 'true',
    ])}}
    <span style="color: red;">@error('template_id'){{$message}} @enderror</span>
  </div>
</div>
</div>

</div>


<div class="card-footer">
  <div class="row">
   <div class="col-lg-12">
     <button type="submit" class="btn btn-primary mr-2 mx-2 ">Save</button>
     @if($request->ajax())
     <a href="javascript:;" data-dismiss="modal" class="btn btn-default">{{__('common.cancel')}}</a>
     
     @else
     <a href="{{ url('/group') }}" class="btn btn-default">{{__('common.cancel')}}</a>
     @endif
   </div>
 </div>
</div>

</div>

@if($request->ajax())
{{ Form::close() }}
@endif()
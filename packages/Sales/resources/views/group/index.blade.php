@php
$btnsList = [];
if(checkActionAllowed('create','required-template')){
	$btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/group/create', 'method'=>'post'];
}

$dtColsArr = [];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.group.id'),'data'=>'id','name'=>'id','orderable'=>false,];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.group.title'),'data'=>'title','name'=>'title','orderable'=>false,'width'=>800];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.group.status'),'data'=>'status','name'=>'status','orderable'=>false];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.group.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>100];
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('scopedocument::scopedocument.group.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('scopedocument::scopedocument.group.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'group'" :jsonUrl="'group/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection

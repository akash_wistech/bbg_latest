@extends('layouts.app')
@section('title', 'Proposal')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'proposal/category'=>'Proposal Category',
  __('common.create')
]])
@endsection
@section('content')
@include('proposal-category::proposal-category._form')
@endsection

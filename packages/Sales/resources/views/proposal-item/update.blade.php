@extends('layouts.app')
@section('title', 'Proposal')

<?php  // dd($model);?>

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'campaign'=>'Proposal',
  __('common.update')
]])
@endsection
@section('content')

<style>
  
  .required:after{ 
        content:'*'; 
        color:red; 
        padding-left:5px;
    } 
</style>


<div class="card card-custom">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">Update List
        <!-- <small>sub title</small></h3> -->
      </div>
    </div>
    <div class="card-body">

<?php
       ?>
    {!! Form::model($model, ['method' => 'POST', 'route' => ['proposalitem.update', $model->id], 'files' => true, 'id' => 'form']) !!}
       @include('proposal-item::proposal-item._form',compact('model'))
    {!! Form::close() !!}
    </div>
  </div>

@endsection

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
@endpush
@push('jsScripts')
tinymce.init({
  selector: '.editor',
  width:'100%',
  height : "280",
  menubar: false,
  toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
  plugins : 'advlist autolink link image lists charmap code table',
  image_advtab: true ,
  external_filemanager_path:"/filemanager/",
  filemanager_title:"Choose File" ,
  external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
});
@endpush







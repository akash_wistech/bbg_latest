<?php
$btnsList = [];


if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('app.proposal.item.create'),'icon'=>'plus','class'=>'success','link'=>'proposal/item/create', 'method'=>'post'];
}

if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>'/prospect/import'];
}

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.parent'),'data'=>'parent_id','name'=>'parent_id'];
$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
$dtColsArr[]=['title'=>__('common.tax'),'data'=>'tax','name'=>'tax'];
$dtColsArr[]=['title'=>__('common.price'),'data'=>'price','name'=>'price'];
$dtColsArr[]=['title'=>__('common.description'),'data'=>'description','name'=>'description'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('common.status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];
?>
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('app.proposal.category.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('app.proposal.item.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="'listname'" :jsonUrl="'proposal/item/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false"  :colorCol="true" :moduleTypeId="$moduleTypeId"  :cbSelection="false" />
  </div>
</div>
@endsection

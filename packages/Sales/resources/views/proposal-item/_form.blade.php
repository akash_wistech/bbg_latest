
<?php  ?>
			<div class="row">
				<div class="form-group col-sm-4 pt-3">
					{{ Form::label('parent_id','Parent Category') }}
					<div class="input-group date">

						{{ Form::select('parent_id',[0=>'Select Parent']+getProposalCategory(),$model->parent_id,['class'=>'form-control']); }}
					</div>
				</div>
				<div class="form-group col-sm-4 pt-3">
					{{ Form::label('title','Title',['class'=>'required']) }}
					<div class="input-group date">
						<input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{$model->title}}" required>
			   	@error('title')
					    <div class="text-danger pt-1">{{ $message }}</div>
					@enderror
					</div>
				</div>
				<div class="form-group col-sm-4 pt-3">
					{{ Form::label('status','Status',['class'=>'required']) }}
					<div class="input-group date">
						<input id="status" type="text" name="status" class="form-control @error('status') is-invalid @enderror" value="{{$model->status}}" required>
			   	@error('status')
					    <div class="text-danger pt-1">{{ $message }}</div>
					@enderror
				    </div>
				</div>
			</div>	



			<div class="row">
				<div class="form-group col-sm-4 pt-3">
					{{ Form::label('tax','Tax') }}
					<div class="input-group date">
						{{ Form::select('tax',getTaxes(),$model->tax,['class'=>'form-control']); }}
					</div>
				</div>
				<div class="form-group col-sm-4 pt-3">
					{{ Form::label('price','Price',['class'=>'required']) }}
					<div class="input-group date">
						<input id="price" type="text" name="price" class="form-control @error('price') is-invalid @enderror" value="{{$model->price}}" required>
			   	@error('price')
					    <div class="text-danger pt-1">{{ $message }}</div>
					@enderror
					</div>

				</div>
			</div>	

			<div class="col-12 col-sm-12 px-0">
				<div class="form-group pt-3">
					{{ Form::label('description','Description') }}
					<div class="input-group date">
						  {{ Form::textarea('description', null, [
					         'class'   => 'form-control editor',
					          ]);}} 
					</div>
				</div>
			</div>
		<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>

	





@extends('layouts.app')
@section('title', 'Proposal')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'proposal'=>'Proposal',
  __('New')
]])
@endsection
@section('content')
<div class="card card-custom bg-transparent shadow-none">
  <div class="card-header bg-white">
    <div class="card-title">
      <h3 class="card-label">Create Proposal
        <!-- <small>sub title</small></h3> -->
      </div>
    </div>
    
  {!! Form::model($model, ['method' => 'POST', 'route' => ['proposal.store'], 'files' => true, 'id' => 'form']) !!}
    @include('proposal::_form')
    {!! Form::close() !!}
   

@endsection

<?php
$btnsList = [];



if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('Add'),'icon'=>'plus','class'=>'success','link'=>'proposal/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('Import'),'icon'=>'file-import','class'=>'primary','link'=>'/prospect/import'];
}

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];


$dtColsArr[]=['title'=>__('Id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('Subject'),'data'=>'subject','name'=>'subject'];
$dtColsArr[]=['title'=>__('Module Type'),'data'=>'module_type','name'=>'module_type'];
$dtColsArr[]=['title'=>__('Module Keyword'),'data'=>'module_keyword','name'=>'module_keyword'];
 
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}
$dtColsArr[]=['title'=>__('Created'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('Created By'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('Action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];
?>
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('Proposal'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('Proposal')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="'listname'" :jsonUrl="'proposal/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false"  :colorCol="true" :moduleTypeId="$moduleTypeId"  :cbSelection="false" />
  </div>
</div>
@endsection

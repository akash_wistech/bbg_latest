	

<?php 

$proposalPages = DB::table('pages')->select(['id','title'])->get();
$estimation = DB::table('estimations')->select(['id','subject'])->get();
$proposalChildData= DB::table('proposal_children')->where('proposal_id',$model->id)->orderBy('order_by')->get()->toArray();

// dd($proposalChildData);
 ?>


<style>
	
  .required:after{ 
        content:'*'; 
        color:red; 
        padding-left:5px;
    }	

    .borderless td, .borderless th {
    border: none;
}
</style>

<div class="card-body bg-white ">
<div class="row">
	<div class="col-sm-6  col-12">
		<div class="form-group col">
			{{ Form::label('subject','Subject',['class'=>'required']) }}
			<div class="input-group date">
				<input id="subject" type="text" name="subject" class="form-control @error('subject') is-invalid @enderror" value="{{ ($model->subject!=null) ? $model->subject : ''  }}" required>
			@error('subject')
					<div class="text-danger pt-1">{{ $message }}</div>
			@enderror
			</div>
		</div>
	</div>
		<div class="col-md-6 col-sm-4 col-12">
		<div class="form-group">
		{{ Form::label('module_type',__('common.source')) }}
		{{ Form::select('module_type', [''=>__('common.select')] + getSourcedModulesArr(), null, ['id'=>'oppr-module_type', 'class'=>'form-control','required']) }}
		</div>
		</div>
</div>



<div class="row">

@php
$moduleType = $model->module_type;
@endphp
   
<div id="module_type-lookup" class="col-md-6  col-12 col-sm-4 {{$moduleType!='' ? '' : 'd-none'}}">
  <div class="form-group">
    {{ Form::label('module_id',$moduleType==null || $moduleType=='' ? '' : getModulesLabelListArr()[$moduleType], ['class'=>'required']) }}
    {{ Form::text('module_keyword', null, ['id'=>'lookup-input', 'class'=>'autocomplete form-control','required', 'data-ds'=>($model->module_type!='' ? url('suggestion/module-lookup/'.$model->module_type) : ''), 'data-mtype'=>($model->module_type!='' ? $model->module_type : ''), 'data-fld'=>'oppr-module_id']) }}
    {{ Form::hidden('module_id', $model->module_id, ['id'=>'oppr-module_id','class'=>'form-control']) }}
  </div>
</div>

</div>		

 </div>
  



<!-- side bar design -->
<div class="row my-5" data-sticky-container>
    <div class="col-lg-3 col-xl-2 my-lg-0 my-5">
     <div class="card card-custom sticky" data-sticky="true" data-margin-top="80" data-sticky-for="1023" data-sticky-class="stickyjs">
			   <!--begin::Card--> 
					<div class="card-body py-2 px-6">
						<!--begin::Accordion-->
						<div class="accordion accordion-light accordion-toggle-arrow" id="accordionExample5">
							<div class="card">
								<div class="card-header" id="headingOne5">
									<div class="card-title" style="font-size:13px" data-toggle="collapse" data-target="#collapseOne5">
									<i class="flaticon2-layers-1"></i>Templates</div>
								</div>
								<div id="collapseOne5" class="collapse show" data-parent="#accordionExample5">
									<div class="card-body scroll scroll-pull" data-scroll="true" data-suppress-scroll-x="false" data-swipe-easing="false" style="height:200px">
										<ul class="navi navi-hover navi-accent navi-active">  
									    <?php if ($proposalPages!=null) {
													foreach ($proposalPages as $value) {
														$proposalChild= DB::table('proposal_children')->where('proposal_id',$model->id)
														->where('type', 'proposal')->where('source_id',$value->id)->first(); ?>
													<li class="navi-item">
														<a class="navi-link  proposal_page_data <?= ($proposalChild!=null) ? 'active' : '' ?>  "  id="proposal-page<?= $value->id ?>" data-id="<?= $value->id ?>" href="javascript:;">
														<span class="navi-text"><?= $value->title ?></span>
														</a>
													</li>
											<?php }	} ?>
										</ul>
								  </div>
								</div>
							</div>
							<div class="card">
								<div class="card-header" id="headingTwo5">
									<div class="card-title collapsed" style="font-size:13px" data-toggle="collapse" data-target="#collapseTwo5">
									<i class="flaticon2-file"></i>Estimation</div>
								</div>
								<div id="collapseTwo5" class="collapse" data-parent="#accordionExample5">
									<div class="card-body scroll scroll-pull" data-scroll="true" data-suppress-scroll-x="false" data-swipe-easing="false" style="height:200px">
									<ul class="navi navi-hover navi-accent navi-active">  
								    <?php if ($estimation!=null) {
											foreach ($estimation as $value) {
												$estimationChild= DB::table('proposal_children')->where('proposal_id',$model->id)
														->where('type', 'estimation')->where('source_id',$value->id)->first(); ?>
											<li class="navi-item">
												<a class="navi-link  estimation_data <?= ($estimationChild!=null) ? 'active' : '' ?> "  id="estimation-<?= $value->id ?>" data-id="<?= $value->id ?>" href="javascript:;">
												<span class="navi-text"><?= $value->subject ?></span>
												</a>
											</li>
										<?php }	} ?>
									</ul>
								  </div>
								</div>
							</div>
						</div>
						<!--end::Accordion-->
					</div>
				 <!--end::Card-->
        </div>
    </div>

    <div class="col-lg-9 col-xl-10" >
     <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">	<i class="flaticon2-mail text-primary mr-3"></i>Proposal</h3>
            </div>
        </div>
        <div class="card-body" style="min-height:250px">
        	<div class="tab-content mt-5 template-content sort-able">



        		<?php foreach ($proposalChildData as $key => $value) {

        			if ($value->type=='proposal') {

        		$templateTitle = DB::table('pages')->where('id',$value->source_id)->pluck('title')->first();?>
			      <div class="card card-custom withsorting example-preview p-0 my-4 shadow-none card-collapsed append-area" data-card="true" data-type="proposal" data-index="<?=$value->source_id?>" data-position="<?=$value->order_by?>" id="proposal_kt_card-<?= $value->source_id ?>" >
						<div class="card-header border-0">
						<div class="card-title">
						<h3 class="card-label pl-2"><?= $templateTitle ?></h3>
						</div>
						<div class="card-toolbar">
						<a href="javascript:;" class="btn  btn-icon btn-sm btn-light-primary mr-2" data-card-tool="toggle">
						<i class="ki ki-arrow-down icon-nm"></i>
						</a>
						<a href="javascript:;" class="btn btn-icon btn-sm btn-light-danger" data-card-tool="remove">
						<i class="ki ki-close icon-nm"></i>
						</a>
						</div>
						</div>
						<div class="card-body pt-0">
						<textarea class="form-control editor" id="proposal-description-<?= $value->source_id ?>" name="proposal_description[<?= $value->source_id ?>]" cols="30" id="description"><?= $value->description ?></textarea>
						<input type="hidden" class="form-control"  name="sorting[]" value="proposal-<?= $value->source_id ?>"> 
						</div>
						</div>

						<?php
					  }
					  	if ($value->type=='estimation') {


					  		$estimation = DB::table('estimations')->where('id',$value->source_id)->first();

					  		// dd($estimation);

					  	 ?>

			<div class="card card-custom withsorting example-preview p-0 my-4 shadow-none card-collapsed append-area" data-card="true" data-type="estimation" data-index="<?=$estimation->id?>" id="estimation_kt_card-<?= $estimation->id ?>" >
			<div class="card-header  border-0">
			<div class="card-title">
			<h3 class="card-label pl-2"><?= $estimation->subject ?></h3>
			<input type="hidden" class="form-control"  name="sorting[]" value="estimation-<?= $estimation->id ?>">
			</div>
			<div class="card-toolbar">
			<a href="javascript:;" class="btn  btn-icon  btn-sm btn-light-primary mr-2" data-card-tool="toggle">
			<i class="ki ki-arrow-down icon-nm"></i>
			</a>
			<a href="#" class="btn btn-icon  btn-sm btn-light-danger" data-card-tool="remove">
			<i class="ki ki-close icon-nm"></i>
			</a>
			</div>
			</div>
			<div class="card-body pt-0">
			<table class="table table-striped border   table-borderless">
			<thead>
			<tr>
			<th scope="col">#</th>
			<th scope="col">Add Item:</th>
			<th scope="col">Description:</th>
			<th scope="col">Quantity:</th>
			<th scope="col">Price:</th>
			<th scope="col">Tax:</th>
			<th scope="col">Amount:</th>
			</tr>
			</thead>
			<tbody>
	
	<?php 

	$estimationChild = DB::table('estimation_children')->where('estimation_id',$estimation->id)->get()->toArray();

	 
$j=1;
	 foreach ($estimationChild as $key => $value) {	 ?>
			<tr>
			<th scope="row"><?= ($j++)  ?></th>
			<td><?= $value->item_name  ?></td>
			<td><?= $value->item_description  ?></td>
			<td><?= $value->item_quantity  ?></td>
			<td><?= $value->item_price  ?></td>
			<td><?= $value->item_tax  ?></td>
			<td><?= $value->item_amount  ?></td>
			</tr>
			<?php } ?>
			
			
		  <tr>
			<td colspan=4></td>
			<th colspan=2>Sub Total:</th>
			<td colspan=1><?= $estimation->sub_total ?></td>
			</tr>
			<tr>
			<td colspan=4></td>
			<th colspan=2>Discount:</th>
			<td colspan=1><?= $estimation->discount_amount ?></td>
			</tr>
			<tr>
			<td colspan=4></td>
			<th colspan=2>Adjustment:</th>
			<td colspan=1><?= $estimation->adjustment ?></td>
			</tr>
			<tr>
			<td colspan=4></td>
			<th colspan=2>Total:</th>
			<td colspan=1><?= $estimation->total ?></td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>	


			<?php		  	}



						

						 } ?>



        	</div>
        </div>
	  </div>
	  </div>
</div>
<!-- side bar design -->

		
		<div class="card card-custom card-border"></div>
		<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
									

</div>
	




	



@push('js')

<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('assets/plugins/custom/jquery-autocomplete/js/jquery.autocomplete.js')}}"></script>
<script src="{{asset('assets/js/pages/features/miscellaneous/sticky-panels.js')}}"></script>

@endpush
@push('jsScripts')


	function TinyMceEditorFunction(){
		tinymce.remove('.editor');
		tinymce.init({
			selector: '.editor',
			menubar: false,
			toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code', 'advlist | autolink | lists'],
			plugins : 'advlist autolink link image lists charmap code table',
			image_advtab: true ,
			external_filemanager_path:"/filemanager/",
			filemanager_title:"Choose File" ,
			external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
		});
	}

TinyMceEditorFunction();

<!-- Get source and show input -->
var moduleTitles = {!!json_encode(getModulesLabelListArr())!!}
$("body").on("change", "#oppr-module_type",function(e){
  if($(this).val()!=""){
    $("#lookup-input").data('ds',"{{url('suggestion/module-lookup')}}/"+$(this).val());
    $("#lookup-input").data('mtype',$(this).val());
    $("#module_type-lookup").find("label").html(moduleTitles[$(this).val()]);
    $("#module_type-lookup").removeClass("d-none");
  }else{
    $("#module_type-lookup").addClass("d-none");
  }
});


<!-- <script> -->
var proposalChildData = <?= json_encode($proposalChildData)  ?>



$.each(proposalChildData,function(key,item){
	if(item.type=="proposal"){
		var	cardcolapseId='proposal_kt_card-'+item.source_id;
				card =	new KTCard(cardcolapseId);
        card.on('afterRemove', function(card) {
      	$("#proposal-page"+item.source_id).removeClass('active')
        });  
	  }
	if(item.type=="estimation"){
		var	cardcolapseId='estimation_kt_card-'+item.source_id;
		console.log(item);
				card =	new KTCard(cardcolapseId);
        card.on('afterRemove', function(card) {
      	$("#estimation-"+item.source_id).removeClass('active')
        });  
	  }
	});

var proposal_id_descp=[];

<!-- Show proposal pages -->
$(".proposal_page_data").click(function(){
	var proposal_page_id = $(this).data("id");
_this=$(this);

var	act =$(this).hasClass("active");
if(!act){
	$.ajax({
		method: 'post',
		url:'proposal/page/view/'+proposal_page_id,
		dataType: "JSON",
		data: {
			"_token": "{{ csrf_token() }}",
		},
		success:function(data) {
			$(_this).addClass("active");

			html='';
		$.each(data,function(key,item){
			html+='<div class="card card-custom example-preview p-0 my-4 shadow-none card-collapsed append-area" data-card="true" id="proposal_kt_card-'+item.id+'" >';
			html+='<div class="card-header border-0">';
			html+='<div class="card-title">';
			html+='<h3 class="card-label pl-2">'+item.title+'</h3>';
			html+='</div>';
			html+='<div class="card-toolbar">';
			html+='<a href="javascript:;" class="btn  btn-icon btn-sm btn-light-primary mr-2" data-card-tool="toggle">';
			html+='<i class="ki ki-arrow-down icon-nm"></i>';
			html+='</a>';
			html+='<a href="javascript:;" class="btn btn-icon btn-sm btn-light-danger" data-card-tool="remove">';
			html+='<i class="ki ki-close icon-nm"></i>';
			html+='</a>';
			html+='</div>';
			html+='</div>';
			html+='<div class="card-body pt-0">';
			html+='<textarea class="form-control editor" id="proposal-description-'+item.id+'" name="proposal_description['+item.id+']" cols="30" id="description">'+item.description+'</textarea>';
				html+='<input type="hidden" class="form-control"  name="sorting[]" value="proposal-'+item.id+'"> ';
			html+='</div>';
			html+='</div>';
			proposal_id_descp[item.id]=item.description;

		});
			$(".template-content").append(html);
			TinyMceEditorFunction();
			
				$.each(data,function(key,item){
	      var	cardcolapseId='proposal_kt_card-'+item.id;
		   	card =	new KTCard(cardcolapseId);

		   // card.on('beforeRemove', function(card) {
         //   toastr.info(item.title+' Template has been removed ');
          //  return confirm('Are you sure to remove this card ?');  // remove card after user confirmation
        //});

        card.on('afterRemove', function(card) {
      	$("#proposal-page"+item.id).removeClass('active')
        }); 
				});	
			
		},
		error: function(data){
			console.log("fail");
		}
	});
	}
});


				 	

<!-- Show Estimation -->
$(".estimation_data").click(function(){
	var estimation_id = $(this).data("id");
	_this=$(this);
	var	act =$(this).hasClass("active");
if(!act){
	$.ajax({
		method: 'post',
		url:'estimation/view/'+estimation_id,
		dataType: "JSON",
		data: {
			"_token": "{{ csrf_token() }}",
		},
		success:function(data) {
			$(_this).addClass("active");
			var html='';
			html+='<div class="card card-custom example-preview p-0 my-4 shadow-none card-collapsed append-area" data-card="true" id="estimation_kt_card-'+data.estimation.id+'" >';
			html+='<div class="card-header  border-0">';
			html+='<div class="card-title">';
			html+='<h3 class="card-label pl-2">'+data.estimation.subject+'</h3>';
			html+='<input type="hidden" class="form-control"  name="sorting[]" value="estimation-'+data.estimation.id+'"> ';
			html+='</div>';
			html+='<div class="card-toolbar">';
			html+='<a href="javascript:;" class="btn  btn-icon  btn-sm btn-light-primary mr-2" data-card-tool="toggle">';
			html+='<i class="ki ki-arrow-down icon-nm"></i>';
			html+='</a>';
			html+='<a href="#" class="btn btn-icon  btn-sm btn-light-danger" data-card-tool="remove">';
			html+='<i class="ki ki-close icon-nm"></i>';
			html+='</a>';
			html+='</div>';
			html+='</div>';
			html+='<div class="card-body pt-0">';
			html+='<table class="table table-striped border table-borderless">';
			html+='<thead>';
			html+='<tr>';
			html+='<th scope="col">#</th>';
			html+='<th scope="col">Add Item:</th>';
			html+='<th scope="col">Description:</th>';
			html+='<th scope="col">Quantity:</th>';
			html+='<th scope="col">Price:</th>';
			html+='<th scope="col">Tax:</th>';
			html+='<th scope="col">Amount:</th>';
			html+='</tr>';
			html+='</thead>';
			html+='<tbody>';
			var countItems=1;
		$.each(data.estimation_child,function(key,item){
			html+='<tr>';
			html+='<th scope="row">'+(countItems++)+'</th>';
			html+='<td>'+item.item_name+'</td>';
			html+='<td>'+item.item_description+'</td>';
			html+='<td>'+item.item_quantity+'</td>';
			html+='<td>'+item.item_price+'</td>';
			html+='<td>'+item.item_tax+'</td>';
			html+='<td>'+item.item_amount+'</td>';
			html+='</tr>';
		});
		  html+='<tr>';
			html+='<td colspan=4></td>';
			html+='<th colspan=2>Sub Total:</th>';
			html+='<td colspan=1>'+data.estimation.sub_total+'</td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td colspan=4></td>';
			html+='<th colspan=2>Discount:</th>';
			html+='<td colspan=1>'+data.estimation.discount_value+'</td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td colspan=4></td>';
			html+='<th colspan=2>Adjustment:</th>';
			html+='<td colspan=1>'+data.estimation.adjustment+'</td>';
			html+='</tr>';
			html+='<tr>';
			html+='<td colspan=4></td>';
			html+='<th colspan=2>Total:</th>';
			html+='<td colspan=1>'+data.estimation.total+'</td>';
			html+='</tr>';
			html+='</tbody>';
			html+='</table>';
			html+='</div>';
			html+='</div>';	
			$(".template-content").append(html);
      var	cardcolapseId='estimation_kt_card-'+data.estimation.id;
	   	card =	new KTCard(cardcolapseId);
      card.on('afterRemove', function(card) {
    	$("#estimation-"+data.estimation.id).removeClass('active')
      }); 
		},
		error: function(data){
			console.log("fail");
		}
	});
}
});











$(".sort-able").sortable({
	    axis: 'y',

	start: function(e, ui){
        tinyMCE.triggerSave();
    },
    
    update: function (event, ui) {
    	 var data = $(this).sortable('serialize');

    	 $("#order-by").val(data);

    	console.log(data);
    	$(this).children().each(function(index){

    		//console.log(index);
    		if($(this).attr('data-position')!=(index+1)){
    			$(this).attr('data-position',(index+1)).addClass('updated');
    		}
    	});

    	saveNewpositions();


   },
    stop: function(e,ui) {
        $(this).find('textarea').each(function(){
            tinyMCE.execCommand( 'mceRemoveEditor', false, $(this).attr("id") );
            tinyMCE.execCommand( 'mceAddEditor', false, $(this).attr("id")  );       
        });
    }

});


	function saveNewpositions()
	{
		var orderByItems=[];
		$('.withsorting').each(function(){
			var thisArr = [];
			thisArr[$(this).data("type")] = $(this).data("index");
			orderByItems.push(thisArr);
		});

		console.log(orderByItems);


		var positions=[];
		$('.updated').each(function(){
			positions.push([$(this).attr('data-type'),$(this).attr('data-index'),$(this).attr('data-position')]);
			$(this).removeClass('updated');
		});

		//console.log(positions);
		data = {positions:positions}
		$.ajax({
			method: 'post',
			url:'proposal/sorting',
			dataType: "text",
			data: {
				"_token": "{{ csrf_token() }}",
				"data":data,
				},
				success: function(response){
					//console.log(response);

				}
		});
	}

$(".sort-able").disableSelection();

@endpush



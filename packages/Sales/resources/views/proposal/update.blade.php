@extends('layouts.app')
@section('title', 'Proposal')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'campaign'=>'Proposal',
  __('Update')
]])
@endsection
@section('content')

<style>
  .required:after{ 
        content:'*'; 
        color:red; 
        padding-left:5px;
    } 
</style>


<div class="card card-custom bg-transparent shadow-none">
  <div class="card-header bg-white">
    <div class="card-title">
      <h3 class="card-label">Create Proposal
        <!-- <small>sub title</small></h3> -->
      </div>
    </div>
  
  {!! Form::model($model, ['method' => 'POST', 'route' => ['proposal.update', $model->id], 'files' => true, 'id' => 'form']) !!}
    @include('proposal::_form')
    {!! Form::close() !!}   

@endsection



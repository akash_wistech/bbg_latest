<div class="card card-custom">

		<div class="card-body">

			<?php if ($request->ajax()) { ?>
			
		{!! Form::model($model, ['method' => 'POST', 'route' => ['list.itemstore'], 'files' => true, 'id' => 'form','class'=>'simple-ajax-submit']) !!}
		
		<?php } else{ ?>	
	{!! Form::model($model, ['method' => 'POST', 'route' => ['list.itemstore'], 'files' => true, 'id' => 'form']) !!}

<?php } ?>
			

			<div class="col-12 col-sm-12 px-0" id="lhitms" style="display:block;">
				<div class="form-group pt-3">
					{{ Form::label('module_type','module_type') }}
					<div class="input-group ">
						{{ Form::text('module_type', null, ['class'=>'form-control','id'=>'sel-module-type']) }}
					</div>
				</div>

				<div class="form-group pt-3">
					{{ Form::label('module_ids','module_ids') }}
					<div class="input-group ">
						{{ Form::text('module_ids', null, ['class'=>'form-control','id'=>'sel-module-ids']) }}
					</div>
				</div>
			</div>	

			<div class="col-12 col-sm-12 px-0">
				<div class="form-group pt-3">
					{{ Form::label('list_id','Select List to save selection to',['class'=>'required']) }}
					<div class="input-group date">
					
						{{  Form::select('list_id',[null=>'Please Select'] + getListArr() ,'',['class'=>'form-control','required']); }}
					</div>
				</div>
			</div>	

		<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
		{!! Form::close() !!}
		</div>
	</div>
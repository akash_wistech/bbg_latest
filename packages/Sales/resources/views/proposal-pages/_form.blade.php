

<style>

  .required:after{
        content:'*';
        color:red;
        padding-left:5px;
    }
</style>


<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">New Proposal Page
				<!-- <small>sub title</small></h3> -->
			</div>
		</div>
		<div class="card-body">

	{!! Form::model($model, ['method' => 'POST', 'route' => ['proposalpage.store'], 'files' => true, 'id' => 'form']) !!}

			<div class="row">
				<div class="form-group col-sm-6 pt-3">
					{{ Form::label('title','Name',['class'=>'required']) }}
					<div class="form-group date">
						<input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror" required>
			   	@error('title')
					    <div class="text-danger pt-1">{{ $message }}</div>
					@enderror
					</div>
				</div>
		
				<div class="form-group col-sm-6 pt-3">
					{{ Form::label('status','Status',['class'=>'required']) }}
					{{ Form::select('status', getStatusArr(), $model->status, ['class'=> 'form-control'])}}

				</div>
			</div>
			<div class="col-12 col-sm-12 px-0">
				<div class="form-group pt-3">
					{{ Form::label('description','Description') }}
					<div class="input-group date">
						  {{ Form::textarea('description', null, [
					         'class'   => 'form-control editor',
					          ]);}}

					</div>
				</div>
			</div>
		<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
		{!! Form::close() !!}
		</div>
	</div>





@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
@endpush
@push('jsScripts')
tinymce.init({
  selector: '.editor',
  width:'100%',
  height : "280",
  menubar: false,
  toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
  plugins : 'advlist autolink link image lists charmap code table',
  image_advtab: true ,
  external_filemanager_path:"/filemanager/",
  filemanager_title:"Choose File" ,
  external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
});
@endpush

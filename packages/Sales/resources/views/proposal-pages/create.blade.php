@extends('layouts.app')
@section('title', 'Proposal Page')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'proposal/page'=>'Proposal Page',
  __('New')
]])
@endsection
@section('content')
@include('proposal-pages::proposal-pages._form')
@endsection

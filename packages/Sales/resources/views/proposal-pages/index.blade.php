<?php
$btnsList = [];

if(checkActionAllowed('create')){
	$btnsList[] = ['label'=>__('Add'),'icon'=>'plus','class'=>'success','link'=>'proposal/page/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
	$btnsList[] = ['label'=>__('Import'),'icon'=>'file-import','class'=>'primary','link'=>'/prospect/import'];
}

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('Id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('Title'),'data'=>'title','name'=>'title'];
$dtColsArr[]=['title'=>__('Description'),'data'=>'description','name'=>'description'];
if($gridViewColumns!=null){
	foreach($gridViewColumns as $gridViewColumn){
		$dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
	}
}
$dtColsArr[]=['title'=>__('Ccreated'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('Created By'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('Status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('Action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];
?>
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('Proposal Page'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('Proposal Page')
]])
@endsection


@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'listname'" :jsonUrl="'proposal/page/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false"  :colorCol="true" :moduleTypeId="$moduleTypeId"  :cbSelection="false" />
	</div>
</div>
@endsection

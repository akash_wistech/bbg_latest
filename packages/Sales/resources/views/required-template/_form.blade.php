<style>
  .heared-outline{
    /*border-top:3px solid #26A69A;*/
  }
</style>
<?php //dd($request); ?>
<div class="card card-custom">
  <div class="card-header heared-outline">
    <div class="card-title">
      <h3 class="card-label ">Template</h3>
    </div>
  </div>
  <div class="card-body">

    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
         {{ Form::label('title', 'Title')}}<span class="text-danger">*</span>
         {{ Form::text('title', null, ['class'=> 'form-control'])}}
       </div>
     </div>
   </div>

   <div class="row">
    <div class="col-12 col-sm-12">
      <div class="form-group">
        {{ Form::label('description', 'Description')}}<span class="text-danger">*</span>
        {{ Form::textarea('description', null, [
        'class'   => 'form-control editor',
        ]);}}
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-sm-12">
      <div class="form-group">
        {{ Form::label('status', 'Status')}}
        {{ Form::select('status', getStatusArr(), null, ['class'=> 'form-control'])}}
      </div>
    </div>
  </div>
  
</div>


<div class="card-footer">
  <div class="row">
   <div class="col-lg-12">
     <button type="submit" class="btn btn-primary mr-2 mx-2 ">Save</button>
     <a href="{{ url('/required-template') }}" class="btn btn-default">{{__('common.cancel')}}</a>


     
   </div>
 </div>
</div>

</div>


@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script>
  $(document).ready(function () {
    $('#temp-form').validate({ // initialize the plugin
      rules: {
        title: {
          required: true
        }
      }
    });
  });
</script>
@endpush

@push('jsScripts')
tinymce.init({
selector: '.editor',
menubar: false,
toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
plugins : 'advlist autolink link image lists charmap code table',
image_advtab: true ,
external_filemanager_path:"/filemanager/",
filemanager_title:"Choose File" ,
external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
});
@endpush



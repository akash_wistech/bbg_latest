@extends('layouts.app')
@section('title', __('scopedocument::scopedocument.required-template.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'required-template'=>__('scopedocument::scopedocument.required-template.heading'),
  __('scopedocument::scopedocument.required-template.update')
]])
@endsection
@section('content')

{{Form::model($model, array('route' => array('required-template.update',$model->id),'id'=>'temp-form'))}}
	  @csrf
    @include('required-template::required-template._form')
{{ Form::close() }}


@endsection

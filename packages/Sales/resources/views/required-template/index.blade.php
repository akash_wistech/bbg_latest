@php
$btnsList = [];
if(checkActionAllowed('create','required-template')){
	$btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/required-template/create', 'method'=>'post'];
}

$dtColsArr = [];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.required-template.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.required-template.title'),'data'=>'title','name'=>'title','orderable'=>false];
//$dtColsArr[]=['title'=>__('scopedocument::scopedocument.required-template.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.required-template.status'),'data'=>'status','name'=>'status','orderable'=>false,'width'=>100];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.required-template.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>100];
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('scopedocument::scopedocument.required-template.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('scopedocument::scopedocument.required-template.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'requiredtemplate'" :jsonUrl="'required-template/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection

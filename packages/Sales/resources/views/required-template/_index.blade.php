@extends('layouts.app')

@section('content')



	<div class="card card-custom py-4">
		<div class="card-header" style="font-size: large;">
			<b>{{ __('List') }}</b>

			<a href="{{url('required-template/create')}}" class="btn btn-link-success btn-sm font-bolder-bold float-right mx-2 px-2 py-2">
				<i class="fas fa-plus text"></i>
			</a>
		</div>
		<div class="card-body">
			<table class="table table-sm table-condensed">
				<thead>
					<tr>
						<th scope="col" class="text-center">#</th>
						<th scope="col" class="text-center">Title</th>
						<th scope="col" class="text-center">Description</th>
						<th scope="col" class="text-center">Status</th>
						<th scope="col" class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					@php $i=1 @endphp
						@if($results!=null)
							@foreach($results as $model)
					<tr>
						<th class="text-center py-4">{{$i}}</th>
						<th class="text-center py-4 font-weight-normal">{{$model->title}}</th>
						<th class="text-center py-4 font-weight-normal">{{$model->description}}</th>
						<th class="text-center py-4 font-weight-normal">{{$model->status}}</th>
						<td class="text-center">

							<div class="dropdown dropdown-inline">
								<a href="#" class="btn btn-light btn-sm font-size-sm font-weight-bolder dropdown-toggle text-dark-75 px-2 py-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
								<div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
									<!--begin::Navigation-->
									<ul class="navi navi-hover">
										<li class="navi-item">
											<a href="{{url('required-template/edit/'.$model->id)}}"  class="navi-link">
												<span class="navi-icon">
													<i class="far fa-edit"></i>
												</span>
												<span class="navi-text">Edit</span>
											</a>
										</li>

										<li class="navi-item">
											<a href=""  class="navi-link">
												<span class="navi-icon">
													<i class="far fa-eye"></i>
												</span>
												<span class="navi-text">View</span>
											</a>
										</li>

										<li class="navi-item">
											<a href="{{url('required-template/delete/'.$model->id)}}"  class="navi-link">
												<span class="navi-icon">
													<i class="far fa-trash-alt"></i>
												</span>
												<span class="navi-text">Delete</span>
											</a>
										</li>
									</ul>
									<!--end::Navigation-->
								</div>
							</div>

						</td>
					</tr>
					@php $i++ @endphp
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>




@endsection

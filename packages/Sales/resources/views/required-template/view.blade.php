@extends('layouts.app')


@section('title', __('scopedocument::scopedocument.required-template.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'required-template'=>__('scopedocument::scopedocument.required-template.heading'),
__('scopedocument::scopedocument.required-template.view')
]])
@endsection


@section('content')

<?php //dd($model); ?>

<style>
	.heared-outline{
		border-top:3px solid #26A69A;
	}
</style>


<div class="card card-custom" data-card="true" data-card-tooltips="false" id="kt_card_2">
	<div class="card-header heared-outline">
		<div class="card-title">
			<h3 class="card-label"><?= $model->title ?> <small class="text-white">{{ __('common.created:') }}</small>  <small class="text-white"> {{ formatDateTime($model->created_at) }}</small></h3>
		</div>
		<div class="card-toolbar">
			@if(checkActionAllowed('update','required-template'))
			<a href="{{ url('/required-template/edit',['id'=>$model->id]) }}" class="btn btn-icon btn-sm btn-hover-light-primary">
				<i class="flaticon-edit-1"></i>
			</a>
			@endif
			<a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
				<i class="ki ki-arrow-down icon-nm"></i>
			</a>
		</div>
	</div>
	<div class="card-body">
		<p><?= $model->description ?></p>
	</div>
</div>

@endsection
@extends('layouts.scope')
@section('title', __('scopedocument::scopedocument.scope-document.heading'))
@section('content')

<?php
//dd($id);
$results = getRequiredDocumentData($id);

$parentRow = $results['parentRow'];
$childRows = $results['childRows'];

// dd($parentRow);
// dd($childRows);

?>

<style>
  .heared-outline{
  border-top:3px solid #FFA000;
}
</style>

<div class="card card-custom">
  <div class="card-body heared-outline">
    <!--begin::Invoice-->
    <div class="row justify-content-center ">
      <!-- thid col-11 means the margin from left side -->
      <div class="col-md-11">
        <!-- begin: Invoice header-->
        <div class="d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-row">
         <img src="{{asset('assets/images/wisdom-logo.jpg')}}" class="" width="150" height="">
       </div>

       <div class="row">
        <div class="col-md-6 d-flex justify-content-between pb-5 pb-md-5 flex-column flex-md-column">
          <span class=" font-weight-boldest"><?= $parentRow->reference_number ?></span>
          <span class=" font-weight-bold text-muted mb-8"><?= $parentRow->subject ?></span>
        </div>
        <div class="col-md-6">
         <div class=" flex-row align-items-md-end px-0 float-right">
          <span class="d-flex flex-row align-items-md-end font-size-h5 font-weight-bold text-muted">

           <a href="{{url('required-document/scopedownload',['id'=>$id])}}">
            <button type="button" class="btn btn-sm btn-secondary mr-2">Download</button>
          </a>

          <div style="<?=($parentRow->status==1)?'':'display: none;'?>">
           <a href="{{url('required-document/scopedecline',['id'=>$id])}}">
            <button type="button" class="btn btn-sm btn-danger mr-2">Decline</button>
          </a>
        </div>

        <div style="<?=($parentRow->status==1)?'':'display: none;'?>">
         <a href="{{url('required-document/scopeapproved',['id'=>$id])}}">
          <button type="button" class="btn btn-sm btn-success mr-2">Accept</button>
        </a>
      </div>

    </span>
  </div>
</div>
</div>
<!--end: Invoice header-->

<!--begin: Invoice body-->
<div class="row border-bottom pb-10">
  <div class="col-md-8" style="text-align: justify;">
    <?php
    foreach ($childRows as $childRow) {?>
      <?= $childRow->description ?>
      <?php
    }
    ?>
    <div class="border-bottom w-100 mt-7 mb-13"></div>
  </div>
  <div class="col-md-4 border-left-md ">

    <div class="card card-custom">
      <div class="card-header card-header-tabs-line">
        <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#kt_builder_themes">Summary</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#kt_builder_page">Discussion</a>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <div class="tab-content  flex-column flex-md-row">
          <div class="tab-pane active" id="kt_builder_themes">
            <span class="font-weight-bold">Wisdom Information Technology Solution LLC</span><br>
            <span class="">Office 12A-04 DAMAC Heights, Tecom  Dubai Dubai UAE</span><br>
            <span class="">VAT Number: 10032710600003</span>
          </div>

  <?php $messages = getDiscussionArr($id); ?>

          <div class="tab-pane" id="kt_builder_page">
                @foreach($messages as $message)
                <p><small class="flaticon2-send-1"></small> {{$message->message}}</p>
                @endforeach

            <div class="border-bottom w-100 mt-3 mb-3"></div>

            {{Form::model($model, array('route' => array('required-document.message')))}}
            @csrf
            <div style="display: none;">
            <input type="text" class="form-control form-control-sm" value="<?=$id?>" name="scope_id">
            <input type="text" class="form-control form-control-sm" value="disscuss" name="route">
            </div>
            {{ Form::label('discussion', 'Drop Your Message here!')}}
            {{ Form::text('discussion', null, ['class'=> 'form-control'])}}
            <button type="submit" class="btn btn-sm btn-success my-2 float-right">Send</button>
            {{ Form::close() }}
          </div>

        </div>
      </div>

    </div>
  </div>
  <!--end: Invoice body-->
</div>
</div>
<!--end::Invoice-->
</div>
</div>
</div>








@endsection

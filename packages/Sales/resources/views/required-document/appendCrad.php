<div class="card card-custom mx-6 append-area" data-card="true" data-card-tooltips="false" id="kt_card_2">
	<div class="card-header bg-success">
		<div class="card-title">
			<h3 class="card-label text-white">Description</h3>
		</div>
		<div class="card-toolbar">
			<a href="#" class="btn btn-icon btn-sm btn-primary mr-1" data-card-tool="toggle"><i class="ki ki-arrow-down icon-nm"></i></a>
			<a class="btn btn-icon btn-sm btn-danger remove"><i class="ki ki-close icon-nm"></i></a>
		</div>
	</div>
	<div class="card-body">
		<div style="display:none;">
			{{ Form::text('template_id[]', null, ['class'=> 'form-control'])}}
		</div>
		<textarea class="form-control editor" name="description[]" cols="50" rows="10" id="description"></textarea>
	</div>
</div>
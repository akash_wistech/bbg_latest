@php
$btnsList = [];
if(checkActionAllowed('create','required-document')){
	$btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/required-document/create', 'method'=>'post'];
}

$dtColsArr = [];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.id'),'data'=>'id','name'=>'id','orderable'=>false,'width'=>1];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.reference_number'),'data'=>'reference_number','name'=>'reference_number','orderable'=>false,'width'=>50];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.subject'),'data'=>'subject','name'=>'subject','orderable'=>false,'width'=>50];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.module-type'),'data'=>'module_type','name'=>'module_type','orderable'=>false,'width'=>50];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.module-keyword'),'data'=>'module_keyword','name'=>'module_keyword','orderable'=>false,'width'=>50];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.scope-date'),'data'=>'date','name'=>'date','orderable'=>false,'width'=>50];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.scope-opentill'),'data'=>'open_till','name'=>'open_till','orderable'=>false,'width'=>50];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.scope-revisions'),'data'=>'revisions','name'=>'revisions','orderable'=>false,'width'=>5];



$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.status'),'data'=>'status','name'=>'status','orderable'=>false,'width'=>5];
$dtColsArr[]=['title'=>__('scopedocument::scopedocument.scope-document.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>50];


@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('scopedocument::scopedocument.scope-document.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('scopedocument::scopedocument.scope-document.heading')
]])
@endsection

<?php //dd("hello") ?>

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'requireddocument'" :jsonUrl="'required-document/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection



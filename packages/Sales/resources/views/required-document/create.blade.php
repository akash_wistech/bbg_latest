@extends('layouts.app')
@section('title', __('scopedocument::scopedocument.scope-document.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'required-document'=>__('scopedocument::scopedocument.scope-document.heading'),
  __('scopedocument::scopedocument.scope-document.create')
]])
@endsection
@section('content')

{{Form::model($model, array('route' => array('required-document.store'),'id'=>'scope-form'))}}
	  @csrf
    @include('required-document::required-document._form')
{{ Form::close() }}


@endsection

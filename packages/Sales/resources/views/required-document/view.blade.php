@extends('layouts.app')

@section('title', __('scopedocument::scopedocument.scope-document.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'required-document'=>__('scopedocument::scopedocument.scope-document.heading'),
__('scopedocument::scopedocument.scope-document.view')
]])
@endsection

@section('content')

<?php //dd($model); ?>

<style>
.heared-outline{
  border-top:3px solid #26A69A;
}
</style>

<section class="card card-custom with-activity">
  <header class="card-header heared-outline">
    <div class="card-title">
      <h3 class="card-label">{{ $model->reference_number }}</h3>
    </div>
    @if(checkActionAllowed('update','required-document'))
    <div class="card-toolbar">
      <a href="{{ url('/required-document/edit',['id'=>$model->id]) }}" class="btn btn-icon btn-sm btn-hover-light-primary">
        <i class="flaticon-edit-1"></i>
      </a>
    </div>
    @endif
  </header>
  <div class="card-body multi-cards multi-tabs">
    <div class="row">
      <div class="col-sm-12">
        <section class="card card-custom card-border mb-3" data-card="true">
          <header class="card-header heared-outline">
            <div class="card-title">
              <h3 class="card-label">{{ __('common.info') }}</h3>
            </div>
            <div class="card-toolbar">
              <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
                <i class="ki ki-arrow-down icon-nm"></i>
              </a>
            </div>
          </header>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <strong>{{ __('common.created:') }}</strong> {{ formatDateTime($model->created_at) }}
              </div>
              <div class="col-sm-6">
                <strong>{{ __('common.reference_no:') }}</strong> {{ $model->reference_number }}
              </div>

              <div class="col-sm-6 my-2">
                <strong>Subject:</strong> {{ $model->subject }}
              </div>
              <div class="col-sm-6 my-2">
                <strong>{{$model->module_type}}:</strong> {{ $model->module_keyword }}
              </div>

              <div class="col-sm-6">
                <strong>Open till:</strong> {{ $model->open_till }}
              </div>
              <div class="col-sm-6">
                <strong>Revisions:</strong> {{ $model->revisions }}
              </div>

              <div class="col-sm-6 my-2">
                <strong>Status:</strong> {{ getScopeStatusArr()[$model->status] }}
              </div>
            </div>
            
          </div>

          
          


          <?php 
          $messages = getDiscussionArr($model->id);

          ?>


          <div class="card card-custom my-6 mx-4" data-card="true" data-card-tooltips="false" id="kt_card_2">
            <div class="card-header heared-outline">
              <div class="card-title">
                <h3 class="card-label">Discussion</h3>
              </div>
              <div class="card-toolbar">
                <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                  <i class="ki ki-arrow-down icon-nm"></i>
                </a>
              </div>
            </div>
            <div class="card-body">
              @foreach($messages as $message)
              <p><small class="flaticon2-send-1"></small> {{$message->message}}</p>
              @endforeach
            </div>

             {{Form::model($model, array('route' => array('required-document.message')))}}
              @csrf
              <div style="display: none;">
                <input type="text" class="form-control form-control-sm" value="<?=$model->id?>" name="scope_id">
                <input type="text" class="form-control form-control-sm" value="view" name="route">
              </div>

              <div class="mx-2" style="display:flex;">
                <div class="col-5">
                  {{ Form::label('discussion', 'Drop Your Message here!')}}
                  {{ Form::text('discussion', null, ['class'=> 'form-control'])}}
                </div>
                <div class="col-1">
                  <button type="submit" class="btn btn-sm btn-success my-8">Send</button>
                </div>
              </div>
              {{ Form::close() }}

          </div>



          <?php 
// dd($results);
          foreach ($results as  $value) {?>
            <div class="card card-custom my-6 mx-4 card-collapsed" data-card="true" data-card-tooltips="false" id="kt_card_2">
              <div class="card-header heared-outline">
                <div class="card-title">
                  <h3 class="card-label "><?= $value->title ?> </h3>
                </div>
                <div class="card-toolbar">
                  <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                    <i class="ki ki-arrow-down icon-nm"></i>
                  </a>
                </div>
              </div>
              <div class="card-body">
                <p><?= $value->description ?></p>
              </div>
            </div>
            <?php
          }
          ?>







        </div>
      </section>
    </div>
  </div>
</div>
</section>



@endsection
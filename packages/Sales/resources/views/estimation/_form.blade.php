

<style>
  .required:after{
        content:'*';
        color:red;
        padding-left:5px;
    }
</style>

<?php

if (!isset($model->id)) {
	$model->id=0;
}

$model->assigned_to =(explode(",",$model->assigned_to));

	 ?>
	{!! Form::model($model, ['method' => 'POST', 'route' => ['estimation.store', $model->id], 'files' => true, 'id' => 'form']) !!}
			<div class="row">
				<div class="col-sm-6  col-12">
					<div class="form-group col">
						{{ Form::label('subject','Subject',['class'=>'required']) }}
						<div class="input-group date">
							<input id="subject" type="text" name="subject" class="form-control @error('subject') is-invalid @enderror" value="{{ ($model->subject!=null) ? $model->subject : ''  }}" required>
						@error('subject')
								<div class="text-danger pt-1">{{ $message }}</div>
						@enderror
						</div>
					</div>

<div class="col-12 col-sm-12">
<div class="form-group">
{{ Form::label('module_type',__('Type')) }}
{{ Form::select('module_type', [''=>__('common.select')] + getSourcedModulesArr(), $model->module_type, ['id'=>'oppr-module_type', 'class'=>'form-control']) }}
</div>
</div>



@php
$moduleType = $model->module_type;
@endphp


<div id="module_type-lookup" class="col-12 col-sm-12 {{$moduleType!='' ? '' : 'd-none'}}">
<div class="form-group">
{{ Form::label('module_id',$moduleType==null || $moduleType=='' ? '' : getModulesLabelListArr()[$moduleType]) }}
{{ Form::text('module_name', null, ['id'=>'lookup-input', 'class'=>'autocomplete_custom client_data form-control', 'data-ds'=>($model->module_type!='' ? url('suggestion/module-lookup/'.$model->module_type) : ''), 'data-mtype'=>($model->module_type!='' ? $model->module_type : ''), 'data-fld'=>'oppr-module_id']) }}
{{ Form::hidden('module_id', $model->module_id, ['id'=>'oppr-module_id','class'=>'form-control']) }}
</div>
</div>




				<div class="row px-4">
					<div class="form-group col-sm-6 pt-3">
					{{ Form::label('date','Current Date',['class'=>'required']) }}
					<div class="input-group date">
						<input id="date" type="text" name="date" class="form-control dtpicker @error('date') is-invalid @enderror" 'autocomplete'='off' value="{{$model->date}}" required>
			   	@error('date')
					    <div class="text-danger pt-1">{{ $message }}</div>
					@enderror
				    </div>
				</div>
				<div class="form-group col-sm-6 pt-3">
					{{ Form::label('open_till','Open Till',['class'=>'required']) }}
					<div class="input-group open_till">
						<input id="open_till" type="text" name="open_till" class="form-control dtpicker @error('open_till') is-invalid @enderror" 'autocomplete'='off' value="{{$model->open_till}}" required>
			   	@error('open_till')
					    <div class="text-danger pt-1">{{ $message }}</div>
					@enderror
				    </div>
				</div>
			</div>
		</div>

		<div class="col-sm-6 col-12">
			<div class="row px-4">
			<div class="form-group col-md-6">
				{{ Form::label('status','Status') }}
				<div class="input-group date">
					{{ Form::select('status',[0=>'Select Type']+getEstimationStatus(),$model->status,['class'=>'form-control']); }}
				</div>
			</div>
			<div class="form-group col-md-6">
				{{ Form::label('assigned_to','Assigned To') }}
				<div class="input-group date">
					{{ Form::select('assigned_to[]',[0=>'Select']+getStaff(),$model->assigned_to,['class'=>'form-control selectpicker', 'multiple'=>'multiple']); }}
				</div>
			</div>

			</div>

			<div class="form-group col">
				{{ Form::label('to','To',['class'=>'required']) }}
				<div class="input-group date">
					<input id="to" type="text" name="to" class="form-control @error('to') is-invalid @enderror" value="{{$model->to}}" required>
				@error('to')
						<div class="text-danger pt-1">{{ $message }}</div>
				@enderror
				</div>
			</div>

			<div class="form-group col">
				{{ Form::label('address','Address',['class'=>'required']) }}
				<div class="input-group date">
					<textarea id="address" type="text" name="address" class="form-control @error('address') is-invalid @enderror"  required>{{$model->address}}</textarea>
				@error('address')
						<div class="text-danger pt-1">{{ $message }}</div>
				@enderror
				</div>
			</div>

			<div class="row px-4">
				<div class="form-group col">
					{{ Form::label('email','Email',['class'=>'required']) }}
					<div class="input-group date">
						<input id="email" type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{$model->email}}" required>
					@error('email')
							<div class="text-danger pt-1">{{ $message }}</div>
					@enderror
					</div>
				</div>
				<div class="form-group col">
					{{ Form::label('phone','Phone',['class'=>'required']) }}
					<div class="input-group date">
						<input id="phone" type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{$model->phone_no}}" required>
					@error('phone')
							<div class="text-danger pt-1">{{ $message }}</div>
					@enderror
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row">
				<div class="col ">
					<div class="separator separator-dashed my-8"></div>

					<div id="kt_repeater_1" >
						<div class="" >
						<label class="col-lg-2 col-form-label mb-3"><h4>Select Item:</h4></label>
						<div class="form-group row">
							<!-- <label class="col-lg-2 col-form-label text-right"></label> -->
							<div class="col-lg-4">
								<a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
								<i class="la la-plus"></i>Add</a>
							</div>
						</div>

						<div class="form-group row">
							<div data-repeater-list="items" class="col-lg-12">
								<div data-repeater-item="" class="form-group repeat-row row align-items-center">


									<div class="col-lg-2  col-md-3 col-sm-6">
											{{ Form::label('add_item','Add Item:',['class'=>'']) }}
											{{ Form::hidden('item_id',null, ['class'=>'form-control predefined-service']) }}
											{{ Form::text('item_name', null, [ 'class'=>'autocomplete1 form-control', 'data-ds'=>(url('suggestion/module-lookup2/'))]) }}
										<div class="d-md-none mb-2"></div>
									</div>


									<div class="col-lg-3  col-md-3 col-sm-6">
															{{ Form::label('name','Description:',['class'=>'']) }}
														<div class="input-group">
																{{ Form::text('item_description', null, ['class'=> 'form-control  item_description_c', 'placeholder'=>'description','readonly'])}}
															<div class="input-group-append">
																<button class="btn btn-sm tk-desp btn-light-primary" data-toggle="modal" data-des="<?= $model->item_description ?>" data-target="#exampleModal" type="button">Edit</button>

															</div>
														</div>
													</div>


										<div class="col-md-1 col-sm-3">
											{{ Form::label('quantity','Quantity:',['class'=>'']) }}
									  	{{ Form::text('item_quantity', null, ['class'=> 'form-control item_quantity_c', 'placeholder'=>'____'])}}
										<div class="d-md-none mb-2"></div>
									</div>
									<div class="col-md-2 col-sm-3">
											{{ Form::label('price','Price:',['class'=>'']) }}
									  	{{ Form::text('item_price', null, ['class'=> 'form-control item_price_c', 'placeholder'=>'____'])}}
										<div class="d-md-none mb-2"></div>
									</div>
									<div class="col-md-1 col-sm-4">
											{{ Form::label('tax','Tax:',['class'=>'']) }}
									  	{{ Form::text('item_tax', null, ['class'=> 'form-control item_tax_c', 'placeholder'=>'____'])}}
										<div class="d-md-none mb-2"></div>
									</div>
									<div class="col-md-2 col-sm-4">
											{{ Form::label('amount','Amount:',['class'=>'']) }}
									  	{{ Form::text('item_amount', 0, ['class'=> 'form-control item_amount_c', 'placeholder'=>'____', 'readonly'=>'readonly'])}}
										<div class="d-md-none mb-2"></div>
									</div>
									<div class="col-md-1 mt-8 col-sm-4">
										<a href="javascript:;" data-repeater-delete=""  class="btn btn-sm delete_c font-weight-bolder btn-light-danger">
										<i class="la la-trash-o"></i>Delete</a>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>




				</div>
			</div>


					<div class="  ">
					<div class="col-md-2 col-sm-4 ml-auto ">
							{{ Form::label('sub_total','Sub Total:',['class'=>'ml-3']) }}
					  	{{ Form::text('sub_total', null, ['class'=> 'form-control sub_total_c ml-3', 'placeholder'=>'____', 'readonly '=>'readonly '])}}
						<div class="d-md-none mb-2"></div>
					</div>

					<div class="col-md-7 col-sm-12 px-0 mr-0 ml-auto mt-5 d-flex  justify-content-end ">
							{{ Form::label('discount_value','Discount:',['class'=>'ml-3 col-2 mt-4']) }}
					  	{{ Form::text('discount_value', null, ['class'=> 'form-control discount_value_c  ml-3 col-3', 'placeholder'=>'____'])}}
					  	{{ Form::text('discount_amount', null, ['class'=> 'form-control discount_amount_c  ml-5 ml-3 col-3', 'placeholder'=>'____','readonly '=>'readonly '])}}
						<div class="d-md-none mb-2"></div>
					</div>

							<div class="col-md-7 col-sm-12 px-0 mr-0 ml-auto mt-5 d-flex  justify-content-end ">
							{{ Form::label('adjustment','Adjustment:',['class'=>'ml-3 col-2 mt-4']) }}
					  	{{ Form::number('adjustment', null, ['class'=> 'form-control adjustment_c ml-3 col-3', 'placeholder'=>'____'])}}
					  	{{ Form::number('adjustment_value', $model->adjustment, ['class'=> 'form-control adjustment_value_c ml-5 ml-3 col-3', 'placeholder'=>'____', 'readonly'=>'readonly'])}}
						<div class="d-md-none mb-2"></div>
					</div>

					<div class="col-md-2 col-sm-4 ml-auto ">
							{{ Form::label('total',' Total:',['class'=>'ml-3']) }}
					  	{{ Form::text('total', null, ['class'=> 'form-control total_c tax ml-3', 'placeholder'=>'____'])}}
						<div class="d-md-none mb-2"></div>
					</div>

					</div>



		<button type="submit" class="btn btn-light-primary">{{__('common.save')}}</button>
		{!! Form::close() !!}



<!-- Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Description</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
						<textarea class="form-control editor" id="" name="" cols="30" id="description"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>
            </div>
        </div>
    </div>
</div>



@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

@endpush


@push('jsScripts')


$("body").on("click",".tk-desp",function(){

var as = $(this).data("value");
console.log(as);

});



<!-- <script> -->

tinymce.init({
  selector: '.editor',
  width:'100%',
  height : "280",
  menubar: false,
  toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
  plugins : 'advlist autolink link image lists charmap code table',
  image_advtab: true ,
  external_filemanager_path:"/filemanager/",
  filemanager_title:"Choose File" ,
  external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
});

var moduleTitles = {!!json_encode(getModulesLabelListArr())!!}
$("body").on("change", "#oppr-module_type",function(e){
  if($(this).val()!=""){
    $("#lookup-input").data('ds',"{{url('suggestion/module-lookup')}}/"+$(this).val());
    $("#lookup-input").data('mtype',$(this).val());
    $("#module_type-lookup").find("label").html(moduleTitles[$(this).val()]);
    $("#module_type-lookup").removeClass("d-none");
  }else{
    $("#module_type-lookup").addClass("d-none");
  }
});


 if($(".autocomplete1").length>0){
    $("body").on("keypress", ".autocomplete1", function () {
      _t=$(this);
      _this=$(this);
      $(this).autocomplete({
        serviceUrl: _t.data("ds"),
        noCache: true,
        onSelect: function(suggestion) {
          if(_t.parents(".repeat-row").find(".predefined-service").val() !=suggestion.data){
             _t.parents(".repeat-row").find(".predefined-service").val(suggestion.data);
             $.ajax({
						  	url: "{{ url('predefinedlists/services/') }}"+"/"+suggestion.data,
						  	success: function(result){
						  	_t.parents(".repeat-row").find(".item_name_c").val(result[0].title);
						  	_t.parents(".repeat-row").find(".item_description_c").val(result[0].descp);
						  	_t.parents(".repeat-row").find(".item_quantity_c").val(1);
						  	_t.parents(".repeat-row").find(".item_price_c").val(result[0].price);
						  	_t.parents(".repeat-row").find(".item_tax_c").val(result[0].tax);
						  	var	amount = ((result[0].price*(result[0].tax/100))+result[0].price);
						  	_t.parents(".repeat-row").find(".item_amount_c").val(amount);
						  	calculateAll(_t);

						}});
          }
        },
        onInvalidateSelection: function() {
          $("."+_t.data("flclass")).val("0");
        }
      });
    });
  }



$("body").on("change", ".item_quantity_c,.item_price_c, .item_tax_c,.discount_value_c, .adjustment_c" , function(){

_this= $(this);
calculateAll(_this);
});




function calculateAll(_this=undefined){

console.log("workin");
// Sub Total
var  sub_t=0;
$(".item_amount_c").each(function() {





if(_this==undefined){
_this= $(this);
}

if(_this=='remove'){
	console.log($(this).val());

sub_t +=	parseFloat($(this).val()) ? parseFloat($(this).val()) : 0 ;
}
var qunatity=0;
var price=0;
var tax=0;
var amount_cal=0;

if(_this!='remove'){

	qunatity = _this.parents(".repeat-row").find(".item_quantity_c").val();
	price = _this.parents(".repeat-row").find(".item_price_c").val();
	tax = _this.parents(".repeat-row").find(".item_tax_c").val();
	total_cal =qunatity*price;


	console.log('quantity:'+qunatity);
	console.log('price:'+price);
	console.log('tax:'+tax);
	console.log('total:'+total_cal);

	if (tax!=null) {
		amount_cal = qunatity*price*(tax/100);
	}
	if (amount_cal!=null) {
		  total_cal = amount_cal+total_cal;
	}
	var amount = _this.parents(".repeat-row").find(".item_amount_c").val(total_cal);
	sub_t+=		parseFloat($(this).val()) ? parseFloat($(this).val()) : 0 ;
}
  $(".sub_total_c").val(sub_t);
});



total=sub_t;

// Discount Value
var DiscountValue = $(".discount_value_c").val();


if(DiscountValue !=null){
	discountAmount =	total*(DiscountValue/100);
	$(".discount_amount_c").val(discountAmount);
	total = total-discountAmount;
}
var Adjustment = $(".adjustment_c").val();

	$(".adjustment_value_c").val(Adjustment);
	total=total+(parseInt(Adjustment) ? parseInt(Adjustment) : 0 );

$(".total_c").val(total);
}



  if($(".autocomplete_custom").length>0){
    $("body").on("keypress", ".autocomplete_custom", function () {
      _t=$(this);
      $(this).autocomplete({
        serviceUrl: _t.data("ds"),
        noCache: true,
        onSelect: function(suggestion) {
          if($("#"+_t.data("fld")).val()!=suggestion.data){
            $("#"+_t.data("fld")).val(suggestion.data);
            prospectId= $("#oppr-module_id").val();
            $.ajax({
							url: "{{ url('estimation/getProspect/') }}"+"/"+prospectId,
							success: function(result){
								$("#to").val(result.full_name);
								$("#email").val(result.email);
								$("#phone").val(result.phone);
							}

						})

          }
        },
        onInvalidateSelection: function() {
          $("#"+_t.data("fld")).val("0");
        }
      });
    });
  }







@endpush

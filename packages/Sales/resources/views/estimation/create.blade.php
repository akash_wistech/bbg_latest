@extends('layouts.app')
@section('title', 'Estimation')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'estimation/create'=>'Estimation Create',
  __('New')
]])
@endsection
@section('content')

<div class="card card-custom">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">Create Estimation
        <!-- <small>sub title</small></h3> -->
      </div>
    </div>
    <div class="card-body">

      @include('estimation::_form')

    </div>
  </div>


@push('js')
<script src="{{asset('assets/plugins/custom/form/form-repeater.js')}}"></script>
@endpush

@endsection

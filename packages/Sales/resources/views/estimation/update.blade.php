@extends('layouts.app')
@section('title', 'Estimation')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'campaign'=>'Estimaiton',
__('Update')
]])
@endsection
@section('content')

<style>
  .required:after{ 
    content:'*'; 
    color:red; 
    padding-left:5px;
  } 
</style>


<div class="card card-custom">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">Update Estimation
        <!-- <small>sub title</small></h3> -->
      </div>
    </div>
    <div class="card-body">

      @include('estimation::_form')

    </div>
  </div>




  <?php $Echild= Wisdom\Sales\Models\EstimationChild::where('estimation_id',$model->id)->get()->toArray(); 


  $Echild =json_encode($Echild);

 // dd($Echild);

  ?>








  @push('js')
  <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
  @endpush
  @push('jsScripts')



  var echild = <?= $Echild ?>

  var $repeater = $('#kt_repeater_1').repeater({

  hide: function (deleteElement) {
  
  
  $(this).slideUp(deleteElement)

  setTimeout(function(){
  return calculateAll('remove');
}, 500);

},
});
$repeater.setList(echild);
@endpush



@endsection
<?php
$btnsList = [];

if(checkActionAllowed('create')){
	$btnsList[] = ['label'=>__('Add'),'icon'=>'plus','class'=>'success','link'=>'estimation/create', 'method'=>'post'];
}


$gridViewColumns = getGridViewColumns($moduleTypeId);


$dtColsArr = [];

// $dtColsArr[]=['title'=>__('common.parent'),'data'=>'parent_id','name'=>'parent_id'];
$dtColsArr[]=['title'=>__('Id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('Subject'),'data'=>'subject','name'=>'subject'];
$dtColsArr[]=['title'=>__('To'),'data'=>'to','name'=>'to'];
$dtColsArr[]=['title'=>__('Email'),'data'=>'email','name'=>'email'];
$dtColsArr[]=['title'=>__('Phone'),'data'=>'phone','name'=>'phone'];
$dtColsArr[]=['title'=>__('Total'),'data'=>'total','name'=>'total'];


if($gridViewColumns!=null){
	foreach($gridViewColumns as $gridViewColumn){
		$dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
	}
}
// $dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('Created By'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('Status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('Action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];

?>
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('Estimation'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('Estimation')
]])
@endsection


@section('content')


<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'listitem'" :jsonUrl="'estimation/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false"  :colorCol="true" :moduleTypeId="$moduleTypeId"  :cbSelection="false" />
	</div>
</div>
@endsection

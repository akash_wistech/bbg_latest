<?php
namespace NaeemAwan\PredefinedLists\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\PredefinedLists\Models\PredefinedList;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class PredefinedListController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index($parent=0)
  {
    // dd($parent);
    $results=PredefinedList::where('parent_id', $parent)->get();
    return view('pdlists::predefined.index', compact('results','parent'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request,$parent)
  {
    // dd($request->all());

    $model = new PredefinedList;
    $model->parent_id = $parent;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'ltitle'=>'required',
        'descp'=>'nullable|string',
        'status'=>'required|integer',
        'rate'=>'nullable|number',
        'tax_id'=>'nullable|integer',
        'parent_category'=>'nullable|integer',
        'type'=>'nullable|string',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:500',
      ]);
      $model= new PredefinedList();
      $model->ltitle=$request->ltitle;
      $model->parent_id=$request->parent_id ?? $parent;
      $model->descp=$request->descp ?? '';
      $model->status=$request->status ?? 1;

      if($model->parent_id==getSetting('service_list_id')){
        $model->rate=$request->rate;
        $model->tax_id=$request->tax_id;
      }

      if($model->parent_id==getSetting('categories_list_id')){
        $model->parent_category=$request->parent_category;
        $model->type=$request->type;
      }

      if ($model->save()) {
        if($request->hasFile('image')){
          $image = $model->saveImage($request->file('image'), true);
        }
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('pdlists::predefinedlist.saved')]]);
        }else{
          return redirect('predefined-list/'.$model->parent_id)->with('success', __('pdlists::predefinedlist.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('app.general.error'),'msg'=>__('pdlists::predefinedlist.notsaved'),]]);
        }else{
          return redirect('predefined-list/'.$model->parent_id)->with('error', __('pdlists::predefinedlist.notsaved'));
        }
      }
    }
    return view('pdlists::predefined.create',compact('model','parent'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\PredefinedList  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = PredefinedList::where('id', $id)->first();
    $parent = $model->parent_id;
    $langDetails = $model->langDetails;
    if($langDetails!=null){
      foreach($langDetails as $langDetail){
        $model->ltitle[$langDetail->lang] = $langDetail->title;
        $model->descp[$langDetail->lang] = $langDetail->descp;
      }
    }
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'id'=>'required',
        'ltitle'=>'required',
        'descp'=>'nullable|string',
        'status'=>'required|integer',
        'rate'=>'nullable|number',
        'tax_id'=>'nullable|integer',
      ]);

      $model= PredefinedList::find($request->id);
      $model->ltitle=$request->ltitle;
      $model->descp=$request->descp;
      $model->status=$request->status;
      if($model->parent_id==getSetting('service_list_id')){
        $model->rate=$request->rate;
        $model->tax_id=$request->tax_id;
      }

      if($model->parent_id==getSetting('categories_list_id')){
        $model->parent_category=$request->parent_category;
        $model->type=$request->type;
      }

      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->hasFile('image')){
          $image = $model->saveImage($request->file('image'), true);
        }
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('pdlists::predefinedlist.saved'),]]);
        }else{
          return redirect('predefined-list/'.$model->parent_id)->with('success', __('pdlists::predefinedlist.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('app.general.error'),'msg'=>__('pdlists::predefinedlist.notsaved'),]]);
        }else{
          return redirect('predefined-list/'.$model->parent_id)->with('error', __('pdlists::predefinedlist.notsaved'));
        }
      }
    }

    return view('pdlists::predefined.edit', [
      'model'=>$model,
      'parent'=>$parent
    ]);

  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\PredefinedList  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return PredefinedList::where('id', $id)->first();
  }
}

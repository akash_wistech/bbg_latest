<?php
use NaeemAwan\PredefinedLists\Models\PredefinedList;
use NaeemAwan\CMS\Models\Categories;

if (!function_exists('currentPDLLevel')) {
    /**
     * return storage dir path
     */
    function currentPDLLevel($parent,$i)
    {
      // echo '>'.$i.'<';
      $model = PredefinedList::select('parent_id')->where('id',$parent)->first();
      if($model!=null){
        $i++;
        if($model->parent_id>0){
          $i = currentPDLLevel($model->parent_id,$i++);
        }
      }
      return $i;
    }

  }


  if (!function_exists('getPrededinedListBreadCrumbs')) {
    /**
     * return array of predefined lists for breadcrumbs
     */
    function getPrededinedListBreadCrumbs($parent,$breadCrumbsArr)
    {
      $model = PredefinedList::where('id',$parent)->first();
      if($model!=null){
        if($model->parent_id>0){
          $breadCrumbsArr = getPrededinedListBreadCrumbs($model->parent_id,$breadCrumbsArr);
          $breadCrumbsArr['admin/predefined-list/'.$model->id] = $model->lang->title;
        }else{
          $breadCrumbsArr['admin/predefined-list/'.$model->id] = $model->lang->title;
        }
        return $breadCrumbsArr;
      }
    }
  }


  if (!function_exists('getPredefinedItem')) {
    /**
     * return array of predefined lists for breadcrumbs
     */
    function getPredefinedItem($id)
    {
      return PredefinedList::where('id',$id)->first();
    }
  }


  if (!function_exists('getPredefinedListItems')) {
  /**
  * All predefined lists
  */
  function getPredefinedListItems($parent)
  {
    return PredefinedList::where(['parent_id' => $parent, 'status' => 1])->get();
  }
}

if (!function_exists('getPredefinedListItemsArr')) {
  /**
  * predefined list sub options
  */
  function getPredefinedListItemsArr($parent)
  {
    $items = getPredefinedListItems($parent);
    $arr = [];
    if($items!=null){
      foreach($items as $item){
        if($item->lang!=null){
          $arr[$item->id]=cleanString($item->lang->title);
        }
      }
    }
    return $arr;
  }
}


if (!function_exists('getModuleServices')) {
  /**
  * save module managers
  */
  function getModuleServices($moduleTypeId,$layout,$urlPre,$arr)
  {
    $service_list_id = getSetting('service_list_id');
    $items = getPredefinedListOptionsArr($service_list_id);
    if($items!=null){
      foreach($items as $key=>$val){
        $link = '/'.$urlPre;
        $link.= '?';
        if($layout!=''){
          $link.= 'layout='.$layout.'&id='.$key;
        }else{
          $link.= 'service_type='.$key;
        }

        $arr[]=['label'=>$val,'icon'=>'','class'=>'','link'=>$link];
      }
    }
    return $arr;
  }
}



if (!function_exists('getCategoryListPredefined')) {
  function getCategoryListPredefined()
  {
    return [
      'general' => 'General',
      'offers' => 'Offers',
    ];
  }

}

<?php
namespace NaeemAwan\PredefinedLists\Observers;

use NaeemAwan\PredefinedLists\Models\PredefinedList;
use NaeemAwan\PredefinedLists\Models\PredefinedListDetail;
use NaeemAwan\PredefinedLists\Models\PredefinedListPrice;
use NaeemAwan\PredefinedLists\Models\PredefinedCategoriesList;

class PredefinedListObserver
{
/**
* Handle the PredefinedList "created" event.
*
* @param  \NaeemAwan\PredefinedLists\Models\PredefinedList  $predefinedList
* @return void
*/
public function created(PredefinedList $predefinedList)
{
// dd($predefinedList); 

  if ($predefinedList->id) {
    $sysLangs = getSystemLanguages();
    foreach ($sysLangs as $key=>$val) {
      if(isset($predefinedList->ltitle[$key])){
        $title = $predefinedList->ltitle[$key];
        $descp = isset($predefinedList->descp[$key]) ? $predefinedList->descp[$key] : '';
        $langDetail = PredefinedListDetail::where('list_id',$predefinedList->id)->where('lang',$key)->first();
        if($langDetail==null){
          $langDetail = new PredefinedListDetail();
          $langDetail->list_id = $predefinedList->id;
          $langDetail->lang = $key;
        }
        $langDetail->title = $title;
        $langDetail->descp = $descp;
        $langDetail->save();
      }
    }

//Save Rates Info
    if($predefinedList->parent_id==getSetting('service_list_id')){
      $pricingDetail = new PredefinedListPrice();
      $pricingDetail->list_id = $predefinedList->id;
      $pricingDetail->rate = $predefinedList->rate;
      $pricingDetail->tax_id = $predefinedList->tax_id;
      $pricingDetail->save();
    }

//Save Categories Info
    if($predefinedList->parent_id==getSetting('categories_list_id')){
//Delete Old
      PredefinedCategoriesList::where(['list_id'=>$predefinedList->id])->delete();
      $pricingDetail = new PredefinedCategoriesList();
      $pricingDetail->list_id = $predefinedList->id;
      $pricingDetail->parent_category = $predefinedList->parent_category;
      $pricingDetail->type = $predefinedList->type;
      $pricingDetail->save();
    }
  }
}

/**
* Handle the PredefinedList "updated" event.
*
* @param  \App\Models\PredefinedList  $predefinedList
* @return void
*/
public function updated(PredefinedList $predefinedList)
{
// dd($predefinedList); 

  $sysLangs = getSystemLanguages();
  foreach ($sysLangs as $key=>$val) {
    if(isset($predefinedList->ltitle[$key])){
      $title = $predefinedList->ltitle[$key];
      $descp = isset($predefinedList->descp[$key]) ? $predefinedList->descp[$key] : '';
      $langDetail = PredefinedListDetail::where('list_id',$predefinedList->id)->where('lang',$key)->first();
      if($langDetail==null){
        $langDetail = new PredefinedListDetail();
        $langDetail->list_id = $predefinedList->id;
        $langDetail->lang = $key;
      }
      $langDetail->title = $title;
      $langDetail->descp = $descp;
      $langDetail->save();
    }
  }

  //Save Rates Info
  if($predefinedList->parent_id==getSetting('service_list_id')){
  //Delete Old
    PredefinedListPrice::where(['list_id'=>$predefinedList->id])->delete();
    $pricingDetail = new PredefinedListPrice();
    $pricingDetail->list_id = $predefinedList->id;
    $pricingDetail->rate = $predefinedList->rate;
    $pricingDetail->tax_id = $predefinedList->tax_id;
    $pricingDetail->save();
  }

  //Save Categories Info
  if($predefinedList->parent_id==getSetting('categories_list_id')){
  //Delete Old
    PredefinedCategoriesList::where(['list_id'=>$predefinedList->id])->delete();
    $pricingDetail = new PredefinedCategoriesList();
    $pricingDetail->list_id = $predefinedList->id;
    $pricingDetail->parent_category = $predefinedList->parent_category;
    $pricingDetail->type = $predefinedList->type;
    $pricingDetail->save();
  }
}

/**
* Handle the PredefinedList "deleted" event.
*
* @param  \App\Models\PredefinedList  $predefinedList
* @return void
*/
public function deleted(PredefinedList $predefinedList)
{
//
}

/**
* Handle the PredefinedList "restored" event.
*
* @param  \App\Models\PredefinedList  $predefinedList
* @return void
*/
public function restored(PredefinedList $predefinedList)
{
//
}

/**
* Handle the PredefinedList "force deleted" event.
*
* @param  \App\Models\PredefinedList  $predefinedList
* @return void
*/
public function forceDeleted(PredefinedList $predefinedList)
{
//
}
}

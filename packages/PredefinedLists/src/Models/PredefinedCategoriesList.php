<?php

namespace NaeemAwan\PredefinedLists\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\ChildTables;

class PredefinedCategoriesList extends ChildTables
{
	use HasFactory;

	protected $table = 'predefined_categories_lists';

	public $timestamps = false;

	protected $fillable = [
		'list_id',
		'parent_id',
		'type',
	];
}

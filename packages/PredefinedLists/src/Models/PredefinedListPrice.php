<?php
namespace NaeemAwan\PredefinedLists\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\ChildTables;

class PredefinedListPrice extends ChildTables
{
  use HasFactory;

  protected $table = 'predefined_list_price';

  public $timestamps = false;

  protected $fillable = [
    'list_id',
    'rate',
    'tax_id',
  ];

  /**
  * Get list.
  */
  public function mainRow()
  {
    return $this->belongsTo(PredefinedList::class,'list_id');
  }

}

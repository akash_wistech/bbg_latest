<?php
namespace NaeemAwan\PredefinedLists\Models;
use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use App\Traits\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Image;
use App\Traits\Imageable;
use App\Models\FullTables;

class PredefinedList extends FullTables
{
  public $ltitle,$descp,$rate,$tax_id;
  public $parent_category,$type;
  use HasFactory, Blameable, SoftDeletes, Sortable, Imageable;

  protected $table = 'predefined_list';

  protected $fillable = [
    'ltitle',
    'status',
    'parent_id',
    'descp',
    'rate',
    'tax_id',
    'parent_category',
    'type',
  ];

  public $sortable = ['id', 'ltitle', 'descp', 'status'];

  /**
  * enabled condition
  */
  public function scopeEnabled($query)
  {
    return $query->where('status', 1);
  }

  /**
  * level condition
  */
  public function scopeChild($query,$parent=0)
  {
    return $query->where('parent_id', $parent);
  }

  /**
  * locale condition
  */
  public function scopeLocale($query,$lang='en')
  {
    $pdl = (new PredefinedListDetail)->getTable();
    return $query->join($pdl,$pdl.'.list_id','=',$this->getTable().'.id')
    ->where('lang', $lang);
  }

  /**
  * Enable / Disable the record
  * @return boolean
  */
  public function updateStatus()
  {
    $status=1;
    if($this->status==1)$status=0;

    DB::table($this->table)
    ->where('id', $this->id)
    ->update(['status' => $status]);

    return true;
  }

  /**
  * Get language details.
  */
  public function langDetails()
  {
    return $this->hasMany(PredefinedListDetail::class,'list_id');
  }

  /**
  * Get single language detail.
  */
  public function lang()
  {
    $locale = App::currentLocale();
    return $this->hasOne(PredefinedListDetail::class,'list_id')->where('lang',$locale);
  }

  /**
  * Get single language detail.
  */
  public function forLang($locale)
  {
    return PredefinedListDetail::where('list_id',$this->id)->where('lang',$locale)->first();
  }

  /**
  * Get title for current lang
  */
  public function getTitleAttribute()
  {
    return $this->lang->title;
  }

  /**
  * Get description for current lang
  */
  public function getDescriptionAttribute()
  {
    return $this->lang->descp;
  }

  /**
  * Get updated by.
  */
  public function parentList()
  {
    return $this->belongsTo(PredefinedList::class,'parent_id');
  }

  /**
  * Get sub options.
  */
  public function subItems()
  {
    return $this->hasMany(PredefinedList::class, 'parent_id');
  }

  /**
  * Get sub options.
  */
  public function subItemsArr()
  {
    return $this->hasMany(PredefinedList::class, 'parent_id');
  }

  /**
  * Get single language detail.
  */
  public function pricing()
  {
    return $this->hasOne(PredefinedListPrice::class,'list_id');
  }

    /**
  * Get single language detail.
  */
    public function categorylisting()
    {
      return $this->hasOne(PredefinedCategoriesList::class,'list_id');
    }

  /**
  * Get sub options.
  */
  public function subOptionsCount()
  {
    return PredefinedList::where('parent_id',$this->id)->count('id');
  }

  /**
  * Overwrited the image method in the imageable
  *
  * @return Illuminate\Database\Eloquent\Collection
  */
  public function image()
  {
    return $this->morphOne(Image::class, 'imageable')
    ->where(function ($q) {
      $q->whereNull('featured')->orWhere('featured', 0);
    })->orderBy('order', 'asc');
  }

}

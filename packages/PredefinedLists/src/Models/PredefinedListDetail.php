<?php
namespace NaeemAwan\PredefinedLists\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\ChildTables;

class PredefinedListDetail extends ChildTables
{
  use HasFactory;

  protected $table = 'predefined_list_detail';

  public $timestamps = false;

  protected $fillable = [
    'list_id',
    'lang',
    'title',
    'descp',
  ];

  /**
  * Get list.
  */
  public function mainRow()
  {
    return $this->belongsTo(PredefinedList::class,'list_id');
  }

}

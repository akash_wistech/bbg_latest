<?php
return [
  'heading' => 'Predefined Lists',
  'saved' => 'List saved successfully',
  'notsaved' => 'List not saved',
  'suboptcount' => 'Sub Options',
];

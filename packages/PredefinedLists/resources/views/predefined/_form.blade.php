

@php
$sysLanguages=getSystemLanguages();
$col = 6;
if(count($sysLanguages)==1)$col=12;
@endphp
<div class="card card-custom mb-2">
  <div class="card card-body">
    <div class="row">
      @foreach($sysLanguages as $key=>$val)
      @php
      $labelTxt = __('common.title_in',['lang'=>$val]);
      $labelHintTxt = __('common.title_hint_in',['lang'=>$val]);
      if(count($sysLanguages)==1){
      $labelTxt = __('common.title');
      $labelHintTxt = __('common.title_hint');
    }
    @endphp
    <div class="col-md-<?= $col?>">
      <div class="form-group">
        {!! Form::label('ltitle['.$key.']', $labelTxt . '*') !!}
        {!! Form::text('ltitle['.$key.']', null, ['class' => 'form-control', 'placeholder' => $labelHintTxt, 'required']) !!}
        <div class="help-block with-errors"></div>
      </div>
    </div>
    @endforeach
  </div>



  <div class="row">
    @foreach($sysLanguages as $key=>$val)
    @php
    $labelTxt = __('common.descp_in',['lang'=>$val]);
    $labelHintTxt = __('common.descp_hint_in',['lang'=>$val]);
    if(count($sysLanguages)==1){
    $labelTxt = __('common.descp');
    $labelHintTxt = __('common.descp_hint');
  }
  @endphp
  <div class="col-md-<?= $col?>">
    <div class="form-group">
      {!! Form::label('descp['.$key.']', $labelTxt . '*') !!}
      {!! Form::textarea('descp['.$key.']', null, ['class' => 'form-control', 'rows'=>4, 'placeholder' => $labelHintTxt]) !!}
      <div class="help-block with-errors"></div>
    </div>
  </div>
  @endforeach
</div>


@if($model->parent_id==getSetting('service_list_id'))
@php
$taxArr=[];
if(function_exists('getTaxArr')){
$taxArr=getTaxArr();
}
$rate=0;
$taxId=0;
if($model->pricing!=null){
$rate=$model->pricing->rate;
$taxId=$model->pricing->tax_id;
}
@endphp

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      {!! Form::label('rate', __('common.rate') . '*') !!}
      {!! Form::text('rate', $rate, ['class' => 'form-control', 'placeholder' => __('pdlists::predefinedlist.rate_hint'), 'required']) !!}
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      {!! Form::label('tax_id', __('common.tax_id') . '*') !!}
      <select id="tax_id" name="tax_id" class="form-control">
        <option value="0">{{__('common.no_tax')}}</option>
        @foreach($taxArr as $key=>$val)
        <option value="{{$key}}"{{$taxId==$key ? ' selected' : ''}}>{{$val.'%'}}</option>
        @endforeach
      </select>
    </div>
  </div>
</div>
@endif


<?php
if ($model->parent_id==getSetting('categories_list_id')) {
  $p_cat='';
  $type='';
  $categories_list = getSetting('categories_list_id');

  if ($model->categorylisting!=null) {
    $p_cat = $model->categorylisting->parent_category;
  }


  if ($model->categorylisting!=null) {
    $type = $model->categorylisting->type;
  }

  ?>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        {{ Form::label('parent_category',__('common.parent_category')) }}
        {{ Form::select('parent_category',  [''=>__('common.select')] + getPredefinedListOptionsArr($categories_list), $p_cat, ['class'=>'form-control']) }}
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        {!! Form::label('type', __('common.type')) !!}
        {{ Form::select('type', getCategoryListPredefined(), $type, ['class'=>'form-control']) }}
      </div>
    </div>
  </div>
  <?php
}

?>



<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      {!! Form::label('image', __('common.image')) !!}
      <div class="input-group file-input">
        <div class="input-group-prepend">
          <span class="input-group-text img-icon">
            @if($model->featuredImage!='')
            @php
            $imageSrc=getStorageFileUrl(optional($model->featuredImage)->path, 'thumbnail');
            @endphp
            <img src="{{$imageSrc}}" width="20" height="20" />
            @else
            <i class="flaticon-presentation"></i>
            @endif
          </span>
        </div>
        <div class="custom-file">
          {{ Form::file('image', ['class'=>'form-control custom-file-input']) }}
          <label class="custom-file-label text-truncate" for="image">{{ __('common.choose_image') }}</label>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      {!! Form::label('status', __('common.status')) !!}
      {!! Form::select('status', getStatusArr(), null, ['class' => 'form-control', 'rows'=>4]) !!}
      <div class="help-block with-errors"></div>
    </div>
  </div>
</div>
</div>
<div class="card-footer">
  {!! Form::submit(trans('common.save'), ['class' => 'btn btn-success']) !!}
</div>
</div>

@php
if($parent>0){
  $breadCrumbsArr['predefined-list']= __('pdlists::predefinedlist.heading');
  $breadCrumbsArr = getPrededinedListBreadCrumbs($parent,$breadCrumbsArr);
}else{
  $breadCrumbsArr[] = __('pdlists::predefinedlist.heading');
}

$canHaveSubOpts = false;
if(currentPDLLevel($parent,1)<=config('predefinedlists.levels')){
  $canHaveSubOpts = true;
}
@endphp
@extends('layouts.app',['btnsList'=>[
  ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'predefined-list/create/'.$parent]
]])
@section('title', __('pdlists::predefinedlist.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => $breadCrumbsArr])
@endsection

@section('content')
<div class="d-flex flex-column-fluid">
  <div class="container">
    <div class="card card-custom">
      <div id="grid-table" class="card card-body">
        <div class="overlay-wrapper">
          <table class="table">
            <thead>
              <tr>
                <th width="50">#</th>
                @if($parent>0)
                <th width="100">{{__('common.image')}}</th>
                @endif
                <th>{{__('common.title')}}</th>
                @if($canHaveSubOpts)
                <th width="100">{{__('pdlists::predefinedlist.suboptcount')}}</th>
                @endif
                <th width="100">{{__('common.status')}}</th>
                <th width="105">{{__('common.action')}}</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=1; ?>
              @foreach($results as $result)
              @php
              if($result->status==1){
                $confrmMsg=__('common.confirmDisable');
              }else{
                $confrmMsg=__('common.confirmEnable');
              }
              @endphp
              <tr>
                <td>{{$i}}</td>
                @if($parent>0)
                @php
                $imageSrc=getStorageFileUrl(optional($result->featuredImage)->path, 'thumbnail');
                @endphp
                <td><img src="{{$imageSrc}}" width="30" /></td>
                @endif
                <td>{{$result->lang!=null ? $result->lang->title : '??'}}</td>
                @if($canHaveSubOpts)
                <td>{{$result->subOptionsCount()}}</td>
                @endif
                <td>{!!getEnableDisableIconWithLink($result->status,'predefined-list/status/'.$result->id,$confrmMsg,'grid-table')!!}</td>
                <td>
                  @if($canHaveSubOpts)
                  <a href="{{ url('predefined-list/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon subopts' data-toggle="tooltip" title='{{__('common.sub_options')}}' data-id="{{$result->id}}">
                    <i class="text-dark-50 flaticon-map"></i>
                  </a>
                  @endif
                  <a href="{{ url('predefined-list/update/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon edit' data-toggle="tooltip" title='{{__('common.update')}}' data-id="{{$result->id}}">
                    <i class="text-dark-50 flaticon-edit"></i>
                  </a>
                  <a href="{{ url('predefined-list/delete/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon act-confirmation' data-toggle="tooltip" title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                    <i class="text-dark-50 flaticon-delete"></i>
                  </a>
                </td>
              </tr>
              <?php $i++; ?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@php
if($parent>0){
  $breadCrumbsArr['predefined-list']= __('pdlists::predefinedlist.heading');
  $breadCrumbsArr = getPrededinedListBreadCrumbs($parent,$breadCrumbsArr);
  $breadCrumbsArr[] = __('common.update');
}else{
  $breadCrumbsArr = [
    'predefined-list'=>__('pdlists::predefinedlist.heading'),
    __('common.update')
  ];
}
@endphp
@extends('layouts.app')
@section('title', __('pdlists::predefinedlist.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => $breadCrumbsArr])
@endsection

@section('content')
  {!! Form::model($model, ['method' => 'POST', 'files' => true, 'id' => 'form']) !!}
  @include('pdlists::predefined._form')
  {!! Form::close() !!}
@endsection

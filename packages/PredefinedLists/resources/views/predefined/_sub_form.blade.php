<div class="card card-custom mb-2">
  <div class="card-header">
    <h3 class="card-title">
      {{__('app.predefinedlist.subOptionAddHeading',['list'=>$list])}}
    </h3>
  </div>
  <div class="card card-body">
    <div class="row">
      @foreach($sysLanguages as $key=>$val)
      <div class="col-sm-6">
        <div class="form-group">
          {!! Form::text('ltitle['.$key.']', null, ['class' => 'form-control', 'placeholder' => trans('app.general.title_in_hint',['lang'=>$val]), 'required']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      @endforeach
    </div>
    <div class="row">
      <div class="col-md-11">
        <div class="form-group">
        <div class="input-group file-input">
          <div class="input-group-prepend">
      		    <span class="input-group-text img-icon">
                <i class="flaticon-presentation"></i>
              </span>
      		</div>
          <div class="custom-file">
            {{ Form::file('image', ['class'=>'form-control custom-file-input']) }}
            <label class="custom-file-label text-truncate" for="image">{{ __('app.general.choose_image') }}</label>
          </div>
        </div>
      </div>
      </div>
      <div class="col-md-1">
        <div class="form-group">
          {!! Form::submit(trans('app.general.save'), ['class' => 'btn btn-success']) !!}
        </div>
      </div>
    </div>
  </div>
</div>

{!! Form::hidden('parent_id', null, ['class' => 'form-control']) !!}
<div class="card card-custom mb-2">
  <div class="card-body">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#tab1">
          <span class="nav-icon">
            <i class="flaticon2-gear"></i>
          </span>
          <span class="nav-text">General Info</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#tab2">
          <span class="nav-icon">
            <i class="flaticon2-bell-1"></i>
          </span>
          <span class="nav-text">Meta Info</span>
        </a>
      </li>
    </ul>
    <div class="tab-content border border-top-0">
      <div id="tab1" class="tab-pane fade active show card card-custom">
        <div class="card-body">
          <div class="row">
            @foreach($sysLanguages as $key=>$val)
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('title['.$key.']', __('app.general.title_in',['lang'=>$val]) . '*') !!}
                {!! Form::text('title['.$key.']', null, ['class' => 'form-control', 'placeholder' => trans('app.general.title_in_hint',['lang'=>$val]), 'required']) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            @foreach($sysLanguages as $key=>$val)
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('descp['.$key.']', __('app.general.descp_in',['lang'=>$val])) !!}
                {!! Form::textarea('descp['.$key.']', null, ['class' => 'form-control', 'rows'=>4, 'placeholder' => trans('app.general.descp_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('image', __('app.general.image')) !!}
                <div class="input-group file-input">
                  <div class="input-group-prepend">
                    <span class="input-group-text img-icon">
                      @if($model->featuredImage!='')
                      @php
                      $imageSrc=getStorageFileUrl(optional($model->featuredImage)->path, 'thumbnail');
                      @endphp
                      <img src="{{$imageSrc}}" width="70" height="70" />
                      @else
                      <i class="flaticon-presentation"></i>
                      @endif
                    </span>
                  </div>
                  <div class="custom-file">
                    {{ Form::file('image', ['class'=>'form-control custom-file-input']) }}
                    <label class="custom-file-label text-truncate" for="image">{{ __('app.general.choose_image') }}</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('status', __('app.general.status')) !!}
                {!! Form::select('status', getStatusArr(), null, ['class' => 'form-control', 'rows'=>4]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="tab2" class="tab-pane fade card card-custom">
        <div class="card-body">
          <div class="row">
            @foreach($sysLanguages as $key=>$val)
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('meta_title['.$key.']', __('app.general.meta_title_in',['lang'=>$val])) !!}
                {!! Form::text('meta_title['.$key.']', null, ['class' => 'form-control', 'placeholder' => trans('app.general.meta_title_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            @foreach($sysLanguages as $key=>$val)
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('meta_descp['.$key.']', __('app.general.meta_descp_in',['lang'=>$val])) !!}
                {!! Form::textarea('meta_descp['.$key.']', null, ['class' => 'form-control', 'rows'=>4, 'placeholder' => trans('app.general.meta_descp_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="row">
            @foreach($sysLanguages as $key=>$val)
            <div class="col-md-6">
              <div class="form-group">
                {!! Form::label('meta_keyword['.$key.']', __('app.general.meta_keyword_in',['lang'=>$val])) !!}
                {!! Form::textarea('meta_keyword['.$key.']', null, ['class' => 'form-control', 'rows'=>4, 'placeholder' => trans('app.general.meta_keyword_in_hint',['lang'=>$val])]) !!}
                <div class="help-block with-errors"></div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    {!! Form::submit(trans('app.general.save'), ['class' => 'btn btn-success']) !!}
  </div>
</div>

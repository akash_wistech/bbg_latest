<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\PredefinedLists\Http\Controllers\PredefinedListController;


Route::group(['middleware' => ['web','auth']], function () {
  Route::get('/predefined-list', [PredefinedListController::class, 'index'])->name('predefined-list');
  Route::get('/predefined-list/{parent}', [PredefinedListController::class, 'index'])->name('predefined-list.{parent}');
  Route::match(['post','get'],'/predefined-list/create', [PredefinedListController::class, 'create'])->name('predefined-list.create');
  Route::match(['post','get'],'/predefined-list/create/{parent}', [PredefinedListController::class, 'create'])->name('predefined-list.create.{parent}');
  Route::post('/predefined-list/store', [PredefinedListController::class, 'store'])->name('predefined-list.store');
  Route::match(['post','get'],'/predefined-list/status/{id}', [PredefinedListController::class, 'status'])->name('predefined-list.status.{id}');
  Route::match(['post','get'],'/predefined-list/update/{id}', [PredefinedListController::class, 'update'])->name('predefined-list.update');
  Route::post('/predefined-list/delete/{id}', [PredefinedListController::class, 'destroy'])->name('predefined-list.destroy.{id}');
});

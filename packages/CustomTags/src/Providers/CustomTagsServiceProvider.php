<?php

namespace NaeemAwan\CustomTags\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\CustomTags\Models\Tags;
use NaeemAwan\CustomTags\Models\ModuleTag;
use NaeemAwan\CustomTags\View\Components\CustomTagsFields;
use NaeemAwan\CustomTags\View\Components\CustomTagsFieldDetails;

class CustomTagsServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'customtags');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'customtags');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/customtags'),
    ],'customtags-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'customtags-migrations');

    $this->loadViewComponentsAs('customtags', [
      CustomTagsFields::class,
      CustomTagsFieldDetails::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

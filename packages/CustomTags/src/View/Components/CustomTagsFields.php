<?php
namespace NaeemAwan\CustomTags\View\Components;

use Illuminate\View\Component;

class CustomTagsFields extends Component
{
  /**
  * card
  */
  public $card=false;

  /**
  * model
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($model,$card=false)
  {
    $this->card = $card;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {

    $this->model->public_tags = getModuleSavedTags($this->model->moduleTypeId,$this->model->id,0);
    $this->model->private_tags = getModuleSavedTags($this->model->moduleTypeId,$this->model->id,1);

    return view('customtags::components.custom-tags-fields', [
      'showCard' => $this->card,
      'model'=>$this->model,
    ]);
  }
}

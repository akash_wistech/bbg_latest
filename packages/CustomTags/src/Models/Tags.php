<?php
namespace NaeemAwan\CustomTags\Models;
use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;

class Tags extends FullTables
{
  use HasFactory, Blameable, SoftDeletes;

  protected $table = 'tags';

  protected $fillable = [
    'title',
  ];
}

<?php

namespace NaeemAwan\CustomTags\Models;
use App\Models\ChildTables;

class ModuleTag extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_tag';
}

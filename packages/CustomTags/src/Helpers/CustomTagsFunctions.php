<?php
use NaeemAwan\CustomTags\Models\Tags;
use NaeemAwan\CustomTags\Models\ModuleTag;

if (!function_exists('saveTags')) {
  /**
  * return list of modules workflow can have
  */
  function saveTags($model,$request,$type='form')
  {
    $privateTags=[];
    $publicTags=[];
    if($type=='import'){
      if(isset($request['private_tags']))$privateTags=$request['private_tags'];
      if(isset($request['public_tags']))$publicTags=$request['public_tags'];
    }else{
      $privateTags=$request->private_tags;
      $publicTags=$request->public_tags;
    }
    if(isset($privateTags) && $privateTags!=null){
      saveTagsByVisibility($model,$privateTags,1);
    }
    if(isset($publicTags) && $publicTags!=null){
      saveTagsByVisibility($model,$publicTags,0);
    }
  }
}

if (!function_exists('saveTagsByVisibility')) {
  /**
  * save tags by visibility
  */
  function saveTagsByVisibility($model,$tags,$visibility)
  {
    if($tags!=null){
      $tags=explode(",",$tags);
      $tagIds=[];
      foreach($tags as $key=>$val){
        $val = trim($val);
        $tagRow=Tags::where('title',$val)->first();
        if($tagRow==null){
          $tagRow=new Tags;
          $tagRow->title=$val;
          $tagRow->save();
        }
        $tagIds[]=$tagRow->id;
      }
      ModuleTag::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
        'visibility'=>$visibility,
      ])
      ->whereNotIn('tag_id',$tagIds)->delete();

      foreach($tagIds as $key=>$val){
        $childRow=ModuleTag::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'tag_id'=>$val,'visibility'=>$visibility])->first();
        if($childRow==null){
          $childRow=new ModuleTag;
          $childRow->module_type=$model->moduleTypeId;
          $childRow->module_id=$model->id;
          $childRow->tag_id=$val;
          $childRow->visibility=$visibility;
          $childRow->save();
        }
      }
    }
  }
}

if (!function_exists('getModuleSavedTags')) {
  /**
  * get saved tags
  */
  function getModuleSavedTags($moduleTypeId,$moduleId,$visibility)
  {
    $value = '';
    $moduleTags = Tags::whereIn('id', function($query) use ($moduleTypeId,$moduleId,$visibility){
      $query->select('tag_id')
      ->from(with(new ModuleTag)->getTable())
      ->where('module_type', $moduleTypeId)
      ->where('module_id', $moduleId)
      ->where('visibility', $visibility);
    })->get();
    if($moduleTags!=null){
      foreach($moduleTags as $moduleTag){
        $value.=($value!='' ? ',' : '').$moduleTag->title;
      }
    }
    return $value;
  }
}

if (!function_exists('getCustomTagsImportInputFields')) {
  /**
  * map inputs for importing
  */
  function getCustomTagsImportInputFields($selOptsHtml)
  {
    $tableHtml = '';

    $tableHtml.='   <tr>';
    $tableHtml.='     <td>'.__('customtags::customtags.public_tags').'</td>';
    $tableHtml.='     <td><select name="input_map_field[static][public_tags]" class="form-control">'.$selOptsHtml.'</td>';
    $tableHtml.='   </tr>';

    $tableHtml.='   <tr>';
    $tableHtml.='     <td>'.__('customtags::customtags.private_tags').'</td>';
    $tableHtml.='     <td><select name="input_map_field[static][private_tags]" class="form-control">'.$selOptsHtml.'</td>';
    $tableHtml.='   </tr>';

    return $tableHtml;
  }
}

@foreach($tagDetails as $tagDetail)
@if($showCard==true)
<div class="card card-custom">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">{{$tagDetail['heading']}}</h3>
    </div>
  </div>
  <div class="card-body">
@else
  <div class="mt-3"><strong>{{$tagDetail['heading']}}</strong></div>
@endif
    {!!$tagDetail['tags']!!}
@if($showCard==true)
  </div>
</div>
@endif
@endforeach

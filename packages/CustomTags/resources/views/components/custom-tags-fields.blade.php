@if($showCard==true)
<div class="card card-custom">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('customtags::customtags.heading')}}</h3>
    </div>
  </div>
  <div class="card-body p-2">
@endif
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          {{ Form::label('public_tags',__('customtags::customtags.public_tags')) }}
          {{ Form::text('public_tags', null, ['class'=>'form-control tagsInput','placeholder'=>__('common.add_tags')]) }}
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          {{ Form::label('private_tags',__('customtags::customtags.private_tags')) }}
          {{ Form::text('private_tags', null, ['class'=>'form-control tagsInput','placeholder'=>__('common.add_tags')]) }}
        </div>
      </div>
    </div>
@if($showCard==true)
  </div>
</div>
@endif

@push('css')
<link href="{{asset('assets/plugins/custom/jquery-tags-input/jquery.tagsinput.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/jquery-tags-input/jquery.tagsinput.min.js')}}"></script>
@endpush

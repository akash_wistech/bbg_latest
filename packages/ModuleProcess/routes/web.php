<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\ModuleProcess\Http\Controllers\WorkflowController;


Route::group(['middleware' => ['web','auth']], function () {
  Route::get('/workflow', [WorkflowController::class,'index']);
  Route::match(['get', 'post'], '/workflow/create', [WorkflowController::class,'create']);
  Route::match(['get', 'post'], '/workflow/update/{id}', [WorkflowController::class,'update']);
  Route::post('/workflow/save-board-order', [WorkflowController::class,'saveBoardOrder']);
  Route::post('/workflow/save-board-item-order', [WorkflowController::class,'saveBoardItemOrder']);
  Route::post('/workflow/delete/{id}', [WorkflowController::class,'destroy']);
  Route::post('/workflow/delete-stage/{id}', [WorkflowController::class,'destroyStage']);
});

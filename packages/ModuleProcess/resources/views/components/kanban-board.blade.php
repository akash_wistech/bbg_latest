<div class="card-header">
  <div class="card-title">
    <h3 class="card-label">
      {{$workFlow->title}}
      (<a href="{{url('opportunity?layout=funnel&id='.$workFlow->id)}}" target="_blank">{{__('workflow::workflow.view_funnel')}}</a>)
    </h3>
  </div>
</div>
<div id="kanban-card" class="card card-body p-2">
  <div id="kanban-board" class="table-responsive"></div>
</div>
<input type="hidden" id="board_sort_order">
@foreach($wfBoards as $wfBoard)
<input type="hidden" id="board_item_sort_order-{{$wfBoard['id']}}">
@endforeach
@push('cssStyles')
{{implode("",$clrClasses)}}
@endpush
@push('cssStyles')
.kanban-container{width:{{$kanbanWidth}}px !important;}
.kanban-container .kanban-board{margin-right: 0.5rem !important;}
@endpush
@push('css')
<link href="{{asset('assets/plugins/custom/kanban/kanban.bundle.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/kanban/kanban.bundle.js')}}"></script>
@endpush
@push('jsScripts')
var kanban = new jKanban({
  element: '#kanban-board',
  gutter: '0',
  widthBoard: '250px',
  boards: {!!json_encode($wfBoards)!!},
  dropBoard: function(el, target, source, sibling){
    var _target = $(target.parentElement);
    var bsorted = [];
    var nodes = _target.find(".kanban-board");
    var currentbOrder = 0;
    $.each( nodes, function( key, value ) {
      bsorted.push({
        "id": $(value).data("id"),
        "order": currentbOrder++
      })
    });
    $("#board_sort_order").val(JSON.stringify(bsorted));
    url = "{{url('workflow/save-board-order')}}";
    data = {id: {{$workFlow->id}}, sort_order: $("#board_sort_order").val()};
    makeSilentAjaxRequestWithData(url,data,"");

  },
  dropEl: function(el, target, source, sibling) {
    var _target = $(target.parentElement);
    _targetBoardId = _target.data("id");
    //Sorting Target
    var sorted = [];
    var nodes = kanban.getBoardElements(_targetBoardId);
    var currentOrder = 0;
    nodes.forEach(function(value, index, array) {
      sorted.push({
        "id": $(value).data("eid"),
        "order": currentOrder++
      })
    });
    $("#board_item_sort_order-"+_targetBoardId).val(JSON.stringify(sorted));
    let otbId = _targetBoardId;
    otbId = otbId.replace("_kb_", "");
    turl = "{{url('workflow/save-board-item-order')}}";
    tdata = {module: "{{$moduleTypeId}}", wfid: {{$workFlow->id}}, stgid: otbId, sort_order: $("#board_item_sort_order-"+_targetBoardId).val()};
    makeSilentAjaxRequestWithData(turl,tdata,"");

    //Sorting Source
    var _source = $(source.parentElement);
    _sourceBoardId = _source.data("id");
    if(_targetBoardId!=_sourceBoardId){
      var ssorted = [];
      var snodes = kanban.getBoardElements(_sourceBoardId);
      var scurrentOrder = 0;
      snodes.forEach(function(value, index, array) {
        ssorted.push({
          "id": $(value).data("eid"),
          "order": scurrentOrder++
        })
      });
      $("#board_item_sort_order-"+_sourceBoardId).val(JSON.stringify(ssorted));
      let osbId = _sourceBoardId;
      osbId = osbId.replace("_kb_", "");
      surl = "{{url('workflow/save-board-item-order')}}";
      sdata = {module: "{{$moduleTypeId}}", wfid: {{$workFlow->id}}, stgid: osbId, sort_order: $("#board_item_sort_order-"+_sourceBoardId).val()};
      makeSilentAjaxRequestWithData(surl,sdata,"");
    }
  },
});

//setTimeout(function(){
  $('[data-toggle="tooltip"]').tooltip()
//}, 1000);
@endpush

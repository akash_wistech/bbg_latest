@if($showCard==true)
<div class="card card-custom">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('workflow::workflow.heading')}}</h3>
    </div>
  </div>
  <div class="card-body p-2">
@endif
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          {{ Form::label('workflow_id',__('workflow::workflow.workflow_id')) }}
          {{ Form::select('workflow_id', $workflowList, (isset($model->workflow_id) && $model->workflow_id!=null ? $model->workflow_id : null), ['id'=>'workflowsel', 'class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          {{ Form::label('workflow_stage_id',__('workflow::workflow.workflow_stage_id')) }}
          {{ Form::select('workflow_stage_id', $stagesList, (isset($model->workflow_stage_id) && $model->workflow_stage_id!=null ? $model->workflow_stage_id : null), ['id'=>'workflowstagesel', 'class'=>'form-control']) }}
        </div>
      </div>
    </div>
@if($showCard==true)
  </div>
</div>
@endif

@push('jsScripts')
var wfsList = {!!json_encode($workflowStages)!!}
$("body").on("change","#service_type", function(e) {
  $("#workflowsel").html("");
	$("#workflowstagesel").html("");
  _tval=$(this).val();
	array_list=wfsList['{{$model->moduleTypeId}}']['services'][_tval]['workflows'];
  $("#workflowsel").append('<option value="">{{__('common.select')}}</option>');
  $.each( array_list, function( key, value ) {
    $("#workflowsel").append('<option value=\"'+value['id']+'">'+value['title']+'</option>');
  });
});
$("body").on("change","#workflowsel", function(e) {
	$("#workflowstagesel").html("");
	array_list=wfsList['{{$model->moduleTypeId}}']['services'][$("#service_type").val()]['workflows'][$(this).val()]['stages'];
  //$("#workflowstagesel").append('<option value="">{{__('common.select')}}</option>');
  $.each( array_list, function( key, value ) {
    $("#workflowstagesel").append('<option value=\"'+value['id']+'">'+value['title']+'</option>');
	});
});
@endpush

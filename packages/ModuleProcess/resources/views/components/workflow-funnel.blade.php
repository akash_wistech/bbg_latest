<div class="card-header">
  <div class="card-title">
    <h3 class="card-label">{{$workFlow->title}}</h3>
  </div>
</div>
<div id="wf-funnel-card" class="card card-body">
  <div class="row">
    <div class="col-12 col-sm-3">
      <div id='funnel-name-container'></div>
      <br class="clearfix" />
    </div>
    <div class="col-12 col-sm-6 text-center">
      <div id='funnel-container'></div>
      <br class="clearfix" />
    </div>
    <div class="col-12 col-sm-3">
      <div id='funnel-amount-container'></div>
      <br class="clearfix" />
    </div>
  </div>
  <div id="funnel-summary"></div>
</div>
@push('cssStyles')
@endpush
@push('css')
<link href="{{asset('assets/plugins/custom/auxlib/auxlib.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/custom/workflow-funnel/css/workflowFunnel.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/plugins/custom/workflow-funnel/css/view.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/auxlib/auxlib.js')}}"></script>
<script src="{{asset('assets/plugins/custom/workflow-funnel/js/X2Geometry.js?v='.now())}}"></script>
<script src="{{asset('assets/plugins/custom/workflow-funnel/js/BaseFunnel.js?v='.now())}}"></script>
<script src="{{asset('assets/plugins/custom/workflow-funnel/js/Funnel.js?v='.now())}}"></script>
<script>
ofunnel = new Funnel ({!! json_encode($funnelOptions) !!});
</script>
@endpush
@push('jsScripts')
@endpush

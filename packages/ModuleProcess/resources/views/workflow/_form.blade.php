{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom multi-tabs">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('title',__('common.title')) }}
          {{ Form::text('title', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('completion_day',__('workflow::workflow.completion_day')) }}
          <div class="input-group">
            {{ Form::text('completion_day', null, ['class'=>'form-control']) }}
            <div class="input-group-append">
              <span class="input-group-text">{{__('common.days')}}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if($modulesList!=null)
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('assigned_modules',__('workflow::workflow.assigned_modules')) }}
          <div class="checkbox-inline">
            @foreach($modulesList as $key=>$val)
            @php
            $cbChecked=false;
            if($model->id!=null){
              $cbChecked = getWorkflowModuleChecked($model->id,$key);
            }
            @endphp
            <label class="checkbox">
              {{ Form::checkbox('assigned_modules['.$key.']', $key, $cbChecked) }}
              <span></span>{{$val}}
            </label>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    @endif
    @if($serviceTypeList!=null)
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('service_type',__('workflow::workflow.service_type')) }}
          <div class="checkbox-inline">
            @foreach($serviceTypeList as $key=>$val)
            @php
            $cbChecked=false;
            if($model->id!=null){
              $cbChecked = getWorkflowServiceChecked($model->id,$key);
            }
            @endphp
            <label class="checkbox">
              {{ Form::checkbox('service_type['.$key.']', $key, $cbChecked) }}
              <span></span>{{$val}}
            </label>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    @endif
    <div class="card card-custom card-border p-2">
      <div class="card-header p-2">
        <div class="card-title">
          <h3 class="card-label">{{__('workflow::workflow.stages')}}</h3>
        </div>
        <div class="card-toolbar">
          <a href="javascript:;" onclick="addStage();" class="btn btn-sm btn-icon btn-light-success">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
      @php
      $sRows=0;
      @endphp
      <div class="card-body p-2">
        <ul id="stages-container" class="ui-sortable">
          @if($model->stages!=null)
          @foreach($model->stages as $stage)
          <li>
            {{ Form::hidden('stage_id['.$sRows.']', $stage->id, ['class'=>'form-control']) }}
            <div id="stage-{{$sRows}}" class="card card-custom card-border mb-3 p-2">
              <div class="card-body p-0">
                <div class="row">
                  <div class="col-12 col-sm-4">
                    <div class="form-group p-0 m-0">
                      {{ Form::label('stage_name',__('workflow::workflow.stage_name')) }}
                      {{ Form::text('stage_name['.$sRows.']', $stage->title, ['class'=>'form-control','placeholder'=>__('workflow::workflow.stage_name')]) }}
                    </div>
                  </div>
                  <div class="col-12 col-sm-1">
                    <div class="form-group p-0 m-0">
                      {{ Form::label('stage_color',__('workflow::workflow.stage_color')) }}
                      {{ Form::text('stage_color['.$sRows.']', $stage->color_code, ['class'=>'form-control colorpicker','placeholder'=>__('workflow::workflow.stage_color')]) }}
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="form-group p-0 m-0">
                      {{ Form::label('stage_descp',__('workflow::workflow.stage_descp')) }}
                      {{ Form::text('stage_descp['.$sRows.']', $stage->descp, ['class'=>'form-control','placeholder'=>__('workflow::workflow.stage_descp')]) }}
                    </div>
                  </div>
                  <div class="col-sm-1 act-btn text-center">
                    <a href="{{url('/workflow/delete-stage/'.$stage->id)}}" class="btn btn-sm btn-danger mt-5 act-confirmation" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="stages-container">
                      <i class="fa fa-times pr-0"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </li>
          @php
          $sRows++;
          @endphp
          @endforeach
          @else
          <li>
            <div id="stage-{{$sRows}}" class="card card-custom card-border mb-3 p-2">
              <div class="card-body p-0">
                <div class="row">
                  <div class="col-12 col-sm-4">
                    <div class="form-group p-0 m-0">
                      {{ Form::label('stage_name',__('workflow::workflow.stage_name')) }}
                      {{ Form::text('stage_name['.$sRows.']', null, ['class'=>'form-control','placeholder'=>__('workflow::workflow.stage_name')]) }}
                    </div>
                  </div>
                  <div class="col-12 col-sm-1">
                    <div class="form-group p-0 m-0">
                      {{ Form::label('stage_color',__('workflow::workflow.stage_color')) }}
                      {{ Form::text('stage_color['.$sRows.']', null, ['class'=>'form-control colorpicker','placeholder'=>__('workflow::workflow.stage_color')]) }}
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="form-group p-0 m-0">
                      {{ Form::label('stage_descp',__('workflow::workflow.stage_descp')) }}
                      {{ Form::text('stage_descp['.$sRows.']', null, ['class'=>'form-control','placeholder'=>__('workflow::workflow.stage_descp')]) }}
                    </div>
                  </div>
                  <div class="col-12 col-sm-1">
                  </div>
                </div>
              </div>
            </div>
          </li>
          @php
          $sRows++;
          @endphp
          @endif
        </ul>
        <input id="stageRank" name="stage_sort_order" type="hidden" value="" />
      </div>
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/workflow') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}
@push('css')
<link href="{{asset('assets/plugins/custom/spectrum-colorpicker/spectrum.css')}}" rel="stylesheet">
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/spectrum-colorpicker/spectrum.js')}}"></script>
<script src="{{asset('assets/plugins/custom/jquery-ui-1.12.1/jquery-ui.js')}}"></script>
@endpush
@push('jsScripts')
initScripts();
@endpush()
<script>
var sRows={{$sRows}};
function addStage()
{
  sRows++;
  html ='';
  html+='<li>';
  html+='<section id="stage-'+sRows+'" class="card card-custom card-border mb-3 p-2">';
  html+=' <div class="card-body p-0">';
  html+='   <div class="row">';
  html+='     <div class="col-sm-4">';
  html+='       <div class="form-group p-0 m-0">';
  html+='         <label for="workflow-stage_name-'+sRows+'">{{__('workflow::workflow.stage_name')}}</label>';
  html+='         <input id="workflow-stage_name-'+sRows+'" class="form-control" name="stage_name['+sRows+']" placeholder="{{__('workflow::workflow.stage_name')}}">';
  html+='       </div>';
  html+='     </div>';
  html+='     <div class="col-sm-1">';
  html+='       <div class="form-group p-0 m-0">';
  html+='         <label for="workflow-stage_color'+sRows+'">{{__('workflow::workflow.stage_color')}}</label>';
  html+='         <input id="workflow-stage_color'+sRows+'" class="form-control colorpicker" name="stage_color['+sRows+']" type="text" id="stage_color" placeholder="{{__('workflow::workflow.stage_color')}}">';
  html+='       </div>';
  html+='     </div>';
  html+='     <div class="col-sm-6">';
  html+='       <div class="form-group p-0 m-0">';
  html+='         <label for="workflow-stage_descp'+sRows+'">{{__('workflow::workflow.stage_descp')}}</label>';
  html+='         <input id="workflow-stage_descp'+sRows+'" class="form-control" name="stage_descp['+sRows+']" type="text" id="stage_descp" placeholder="{{__('workflow::workflow.stage_descp')}}">';
  html+='       </div>';
  html+='     </div>';
  html+='     <div class="col-sm-1 act-btn text-center">';
  html+='       <a href="javascript:;" onclick="removeStage('+sRows+')" class="btn btn-sm btn-danger mt-5">';
  html+='         <i class="fa fa-times pr-0"></i>';
  html+='       </a>';
  html+='     </div>';
  html+='   </div>';
  html+=' </div>';
  html+='</section>';
  html+='</li>';
  $("#stages-container").append(html);
  initScripts();
}
function removeStage(r)
{
  $("#stage-"+r).parent("li").remove();
}
function initScripts()
{
  $("#stages-container").sortable({
    revert: 100,
    placeholder: "placeholder",
    update: function (event, ui) {
      var data = $(this).sortable("toArray");
      $("#stageRank").val(data.join(","));
    }
  });
  $(".colorpicker").spectrum({preferredFormat: "hex",});
}
</script>

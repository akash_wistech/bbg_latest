@extends('layouts.app')
@section('title', __('workflow::workflow.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'workflow'=>__('workflow::workflow.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('workflow::workflow._form')
@endsection

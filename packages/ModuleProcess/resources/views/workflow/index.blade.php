@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/workflow/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('workflow::workflow.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('workflow::workflow.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <div class="overlay-wrapper">
      @if($results->isNotEmpty())
      <table class="table">
        <thead>
          <tr>
            <th>{{__('common.id')}}</th>
            <th>{{__('common.title')}}</th>
            <th>{{__('workflow::workflow.modules')}}</th>
            <th>{{__('workflow::workflow.servicetype')}}</th>
            <th>{{__('workflow::workflow.stages')}}</th>
            <th width="100">{{__('common.action')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($results as $result)
          <tr id="result_id_{{ $result->id }}">
            <td>{{ $result->id }}</td>
            <td>{{ $result->title }}</td>
            <td>{!! $result->selectedModules() !!}</td>
            <td>{!! $result->selectedServiceTypes() !!}</td>
            <td>{!! $result->savedStageNames() !!}</td>
            <td>
              @if(checkActionAllowed('update'))
              <a href="{{ url('/workflow/update',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon edit' title='{{__('common.update')}}' data-id="{{$result->id}}">
                <i class="text-dark-50 flaticon-edit"></i>
              </a>
              @endif
              @if(checkActionAllowed('delete'))
              <a href="{{ url('/workflow/delete',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon act-confirmation' title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
              </a>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <div class="text-center">{{ __('common.noresultfound') }}</div>
      @endif
    </div>
  </div>
</div>
@endsection

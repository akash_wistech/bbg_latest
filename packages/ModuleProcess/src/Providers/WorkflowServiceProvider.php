<?php

namespace NaeemAwan\ModuleProcess\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\ModuleProcess\Models\Workflow;
use NaeemAwan\ModuleProcess\Observers\WorkflowObserver;
use NaeemAwan\ModuleProcess\View\Components\WorkflowFields;
use NaeemAwan\ModuleProcess\View\Components\KanbanBoard;
use NaeemAwan\ModuleProcess\View\Components\WorkflowFunnel;

class WorkflowServiceProvider extends ServiceProvider
{
  public function boot()
  {
    Workflow::observe(WorkflowObserver::class);
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'workflow');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'workflow');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/workflow'),
    ],'workflow-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'workflow-migrations');

    $this->loadViewComponentsAs('workflow', [
      WorkflowFields::class,
      KanbanBoard::class,
      WorkflowFunnel::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

<?php
namespace NaeemAwan\ModuleProcess\View\Components;

use Illuminate\View\Component;
use NaeemAwan\ModuleProcess\Models\Workflow;

class WorkflowFunnel extends Component
{
  public $moduleTypeId,$controller,$id;
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($moduleTypeId,$controller,$id)
  {
    $this->moduleTypeId=$moduleTypeId;
    $this->controller=$controller;
    $this->id=$id;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $curreny=getCurrencyPre();
    $workFlow = Workflow::where('id',$this->id)->first();
    $clrClasses=[];
    $stages = $workFlow->stages;
    $stagesArr=[];
    $stageRecordsArr=[];
    $stageValuesArr=[];
    $stageColorsArr=[];
    $stageNameLinksArr=[];
    $totalAmount=0;
    if($stages!=null){
      foreach($stages as $stage){
        $stageColorsArr[]=$stage->color_code;
        $stagesArr[$stage->id]=['id'=>$stage->id,'name'=>$stage->title,'requirePrevious'=>'-1','roles'=>[],'complete'=>false,'createDate'=>null,'completeDate'=>null,'requireComment'=>"0"];
        $stageInfo = getWorkflowStageModulesCountAndAmount($this->moduleTypeId,$this->controller,$workFlow->id,$stage->id);
        $stageRecordsArr[]=$stageInfo['count'];
        $stageValuesArr[]=$curreny.$stageInfo['totalAmount'];
        $stageNameLinksArr[]='<a onclick="WorkflowViewManager.getStageMembers('.$stage->id.'); return false;" url="javascript:;">'.$stage->title.'</a>';
      }
    }

    $funnelOptions = [];
    $funnelOptions['workflowStatus']=[
      "id"=>$workFlow->id,
      "stages"=>$stagesArr,
      "started"=>false,
      "completed"=>false,
      "financial"=>1,
      "financialModel"=>'accounts',
      "financialField"=>'dealvalue',
    ];
    $funnelOptions['translations'] = ["Total Records"=>__('workflow::workflow.total_records'),"Total Amount"=>__('workflow::workflow.total_amount')];
    $funnelOptions['stageCount'] = count($stages);
    $funnelOptions['recordsPerStage'] = $stageRecordsArr;
    $funnelOptions['stageValues'] = $stageValuesArr;
    $funnelOptions['totalValue'] = $curreny.$totalAmount;
    $funnelOptions['containerSelector'] = "#funnel-container";
    $funnelOptions['containerNameSelector'] = "#funnel-name-container";
    $funnelOptions['containerAmountSelector'] = "#funnel-amount-container";
    $funnelOptions['containerSummarySelector'] = "#funnel-summary";
    $funnelOptions['stageNameLinks'] = $stageNameLinksArr;
    $funnelOptions['colors'] = $stageColorsArr;

    return view('workflow::components.workflow-funnel',[
      'workFlow' => $workFlow,
      'funnelOptions' => $funnelOptions,
    ]);
  }
}

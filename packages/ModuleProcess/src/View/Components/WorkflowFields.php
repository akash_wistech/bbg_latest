<?php
namespace NaeemAwan\ModuleProcess\View\Components;

use Illuminate\View\Component;

class WorkflowFields extends Component
{
  /**
  * card
  */
  public $card=false;

  /**
  * model
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($model,$card=false)
  {
    $this->card = $card;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $workflowStages = getModuleWorkFlowStagesListArr($this->model->moduleTypeId);

    $workflowList[''] = __('common.select');
    $stagesList[''] = __('common.select');

    // if($this->model!=null){
      $savedInfo = getModuleWorkFlowSavedInfo($this->model);
      $serviceType = $this->model->service_type;
      $WorkFlowId = 0;
      $WorkFlowStageId = 0;
      if($savedInfo!=null){
        $WorkFlowId = $savedInfo['workflow_id'];
        $WorkFlowStageId = $savedInfo['workflow_stage_id'];
      }
      if($serviceType==0 || $serviceType=='')$serviceType = old('service_type');
      if($WorkFlowId==0 || $WorkFlowId=='')$WorkFlowId = old('workflow_id');
      if($WorkFlowStageId==0 || $WorkFlowStageId=='')$WorkFlowStageId = old('workflow_stage_id');

      if($serviceType>0){
        $selectedWorksFlows = $workflowStages[$this->model->moduleTypeId]['services'][$serviceType]['workflows'];
        if($selectedWorksFlows!=null){
          foreach($selectedWorksFlows as $selectedWorksFlow){
            $workflowList[$selectedWorksFlow['id']] = $selectedWorksFlow['title'];
          }
        }
        if($WorkFlowId>0){
          $selectedWorksFlowStages = $workflowStages[$this->model->moduleTypeId]['services'][$serviceType]['workflows'][$WorkFlowId]['stages'];
          if($selectedWorksFlowStages!=null){
            foreach($selectedWorksFlowStages as $selectedWorksFlowStage){
              $stagesList[$selectedWorksFlowStage['id']] = $selectedWorksFlowStage['title'];
            }
          }
        }
        $this->model->workflow_id = $WorkFlowId;
        $this->model->workflow_stage_id = $WorkFlowStageId;
      }
    // }

    return view('workflow::components.workflow-fields', [
      'showCard' => $this->card,
      'model'=>$this->model,
      'workflowStages'=>$workflowStages,
      'workflowList'=>$workflowList,
      'stagesList'=>$stagesList,
    ]);
  }
}

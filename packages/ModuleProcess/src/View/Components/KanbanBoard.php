<?php
namespace NaeemAwan\ModuleProcess\View\Components;

use Illuminate\View\Component;
use NaeemAwan\ModuleProcess\Models\Workflow;

class KanbanBoard extends Component
{
  public $moduleTypeId,$controller,$id;
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($moduleTypeId,$controller,$id)
  {
    $this->moduleTypeId=$moduleTypeId;
    $this->controller=$controller;
    $this->id=$id;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $workFlow = Workflow::where('id',$this->id)->first();
    $clrClasses=[];
    $stages = $workFlow->stages;
    $wfBoards=[];
    if($stages!=null){
      foreach($stages as $stage){
        $items = [];
        $stageModules = getWorkflowStageModules($this->moduleTypeId,$this->controller,$workFlow->id,$stage->id);
        if($stageModules!=null){
          foreach($stageModules as $stageModule){;
            $itemDetail = getBoardItemHtml($this->moduleTypeId,$stageModule);
            $items[]=['id'=>$stageModule['id'],'title'=>$itemDetail];
          }
        }

        $textColor = getReverseColor($stage->color_code);
        $clrClasses[]='.kanban-board-header.stage_clr'.str_replace("#","",$stage->color_code).'{background-color:'.$stage->color_code.';}.kanban-board-header.stage_clr'.str_replace("#","",$stage->color_code).' .kanban-title-board{color:'.$textColor.' !important;}';
        $wfBoards[] = ['id'=>'_kb_'.$stage->id,'bid'=>$stage->id,'title'=>$stage->title,'class'=>'stage_clr'.str_replace("#","",$stage->color_code),'item'=>$items];
      }
    }
    $totalBoards = count($wfBoards);
    return view('workflow::components.kanban-board',[
      'moduleTypeId' => $this->moduleTypeId,
      'workFlow' => $workFlow,
      'wfBoards' => $wfBoards,
      'clrClasses' => $clrClasses,
      'kanbanWidth' => (($totalBoards*250)+($totalBoards*6)),
    ]);
  }
}

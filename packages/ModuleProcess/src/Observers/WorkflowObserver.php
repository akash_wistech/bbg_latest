<?php
namespace NaeemAwan\ModuleProcess\Observers;

use NaeemAwan\ModuleProcess\Models\Workflow;
use NaeemAwan\ModuleProcess\Models\WorkflowDataItem;
use NaeemAwan\ModuleProcess\Models\WorkflowModule;
use NaeemAwan\ModuleProcess\Models\WorkflowServiceType;
use NaeemAwan\ModuleProcess\Models\WorkflowStage;
use Illuminate\Support\Facades\DB;

class WorkflowObserver
{
  /**
  * Handle the Workflow "created" event.
  *
  * @param  \NaeemAwan\ModuleProcess\Models\Workflow  $workflow
  * @return void
  */
  public function created(Workflow $workflow)
  {
    if ($workflow->id) {
      //Saving Assigned To
      if($workflow->assigned_modules!=null){
        DB::table((new WorkflowModule)->getTable())->where('workflow_id', $workflow->id)->whereNotIn('module_type',$workflow->assigned_modules)->delete();
        foreach($workflow->assigned_modules as $key=>$val){
          $workflowModule=WorkflowModule::where(['workflow_id'=>$workflow->id,'module_type'=>$val])->first();
          if($workflowModule==null){
            $workflowModule=new WorkflowModule;
            $workflowModule->workflow_id=$workflow->id;
            $workflowModule->module_type=$val;
            $workflowModule->save();
          }
        }
      }
      //Saving service types
      if($workflow->service_type!=null){
        DB::table((new WorkflowServiceType)->getTable())->where('workflow_id', $workflow->id)->whereNotIn('service_type',$workflow->service_type)->delete();
        foreach($workflow->service_type as $key=>$val){
          $workflowModule=WorkflowServiceType::where(['workflow_id'=>$workflow->id,'service_type'=>$val])->first();
          if($workflowModule==null){
            $workflowModule=new WorkflowServiceType;
            $workflowModule->workflow_id=$workflow->id;
            $workflowModule->service_type=$val;
            $workflowModule->save();
          }
        }
      }
      if($workflow->stage_name!=null){
        $r=1;
        foreach($workflow->stage_name as $key=>$val){
          $stageName=$workflow->stage_name[$key];
          $stageColor=$workflow->stage_color[$key];
          $stageDescp=$workflow->stage_descp[$key];
          if(isset($workflow->stage_id[$key]) && $workflow->stage_id[$key]>0){
            $stageRow=WorkflowStage::where(['workflow_id'=>$workflow->id,'id'=>$workflow->stage_id[$key]])->first();
          }else{
            $stageRow=new WorkflowStage;
            $stageRow->workflow_id=$workflow->id;
          }
          $stageRow->title=$stageName;
          $stageRow->color_code=$stageColor;
          $stageRow->descp=$stageDescp;
          $stageRow->sort_order=$r;
          $stageRow->save();
          $r++;
        }
      }
    }
  }

  /**
  * Handle the Workflow "updated" event.
  *
  * @param  \App\Models\Workflow  $workflow
  * @return void
  */
  public function updated(Workflow $workflow)
  {
    //Saving Assigned To
    if($workflow->assigned_modules!=null){
      DB::table((new WorkflowModule)->getTable())->where('workflow_id', $workflow->id)->whereNotIn('module_type',$workflow->assigned_modules)->delete();
      foreach($workflow->assigned_modules as $key=>$val){
        $workflowModule=WorkflowModule::where(['workflow_id'=>$workflow->id,'module_type'=>$val])->first();
        if($workflowModule==null){
          $workflowModule=new WorkflowModule;
          $workflowModule->workflow_id=$workflow->id;
          $workflowModule->module_type=$val;
          $workflowModule->save();
        }
      }
    }
    //Saving service types
    if($workflow->service_type!=null){
      DB::table((new WorkflowServiceType)->getTable())->where('workflow_id', $workflow->id)->whereNotIn('service_type',$workflow->service_type)->delete();
      foreach($workflow->service_type as $key=>$val){
        $workflowModule=WorkflowServiceType::where(['workflow_id'=>$workflow->id,'service_type'=>$val])->first();
        if($workflowModule==null){
          $workflowModule=new WorkflowServiceType;
          $workflowModule->workflow_id=$workflow->id;
          $workflowModule->service_type=$val;
          $workflowModule->save();
        }
      }
    }
    if($workflow->stage_name!=null){
      $r=1;
      foreach($workflow->stage_name as $key=>$val){
        $stageName=$workflow->stage_name[$key];
        $stageColor=$workflow->stage_color[$key];
        $stageDescp=$workflow->stage_descp[$key];
        if(isset($workflow->stage_id[$key]) && $workflow->stage_id[$key]>0){
          $stageRow=WorkflowStage::where(['workflow_id'=>$workflow->id,'id'=>$workflow->stage_id[$key]])->first();
        }else{
          $stageRow=new WorkflowStage;
          $stageRow->workflow_id=$workflow->id;
        }
        $stageRow->title=$stageName;
        $stageRow->color_code=$stageColor;
        $stageRow->descp=$stageDescp;
        $stageRow->sort_order=$r;
        $stageRow->save();
        $r++;
      }
    }
  }

  /**
  * Handle the Workflow "deleted" event.
  *
  * @param  \App\Models\Workflow  $workflow
  * @return void
  */
  public function deleted(Workflow $workflow)
  {
    //
  }

  /**
  * Handle the Workflow "restored" event.
  *
  * @param  \App\Models\Workflow  $workflow
  * @return void
  */
  public function restored(Workflow $workflow)
  {
    //
  }

  /**
  * Handle the Workflow "force deleted" event.
  *
  * @param  \App\Models\Workflow  $workflow
  * @return void
  */
  public function forceDeleted(Workflow $workflow)
  {
    //
  }
}

<?php

namespace NaeemAwan\ModuleProcess\Models;
use App\Models\ChildTables;

class WorkflowModule extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'workflow_module';
}

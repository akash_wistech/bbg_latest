<?php

namespace NaeemAwan\ModuleProcess\Models;
use App\Models\ChildTables;

class WorkflowStage extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'workflow_stage';

  protected $fillable = [
    'workflow_id',
    'title',
    'color_code',
    'descp',
    'conversion_rate',
    'value',
    'require_previous',
    'require_comment',
    'sort_order',
  ];
}

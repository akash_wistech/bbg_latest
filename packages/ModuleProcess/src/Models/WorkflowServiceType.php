<?php

namespace NaeemAwan\ModuleProcess\Models;
use App\Models\ChildTables;

class WorkflowServiceType extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'workflow_service_type';
}

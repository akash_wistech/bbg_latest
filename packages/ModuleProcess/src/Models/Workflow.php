<?php
namespace NaeemAwan\ModuleProcess\Models;
use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;

class Workflow extends FullTables
{
  use HasFactory, Blameable, SoftDeletes;

	public $assigned_modules,$service_type;
	//Stage Attrs
	public $stage_id,$stage_name,$stage_color,$stage_descp,$stage_sort_order;

  protected $table = 'workflow';

  protected $fillable = [
    'title',
  ];

  /**
  * Get stages
  */
  public function stages()
  {
    return $this->hasMany(WorkflowStage::class,'workflow_id')->orderBy('sort_order','asc');
  }

  /**
  * Get stage names
  */
  public function savedStageNames()
  {
    $html = '';
    $stagesList = WorkflowStage::select('title','color_code')
    ->where('workflow_id',$this->id)
    ->orderBy('sort_order','asc')->get();
    if($stagesList!=null){
      foreach($stagesList as $stage){
        $html.='<span class="badge badge-success mb-1 mr-1" style="background:'.$stage->color_code.';">'.$stage->title.'</span>';
      }
    }
    return $html;
  }

  /**
  * Get service types
  */
  public function serviceTypes()
  {
    return $this->hasMany(WorkflowServiceType::class,'workflow_id');
  }

  /**
  * Get service type names
  */
  public function selectedServiceTypes()
  {
    $html = '';
    $serviceTypeList = getServiceTypeListArr();
    if($serviceTypeList!=null){
      foreach($serviceTypeList as $key=>$val){
        if(getWorkflowServiceChecked($this->id,$key)==true){
          $html.='<span class="badge badge-primary mb-1 mr-1">'.$val.'</span>';
        }
      }
    }
    return $html;
  }

  /**
  * Get modules
  */
  public function modules()
  {
    return $this->hasMany(WorkflowModule::class,'workflow_id');
  }

  /**
  * Get modules
  */
  public function selectedModules()
  {
    $html = '';
    $modulesList = getWorkflowModules();
    if($modulesList!=null){
      foreach($modulesList as $key=>$val){
        if(getWorkflowModuleChecked($this->id,$key)==true){
          $html.='<span class="badge badge-success mb-1 mr-1">'.$val.'</span>';
        }
      }
    }
    return $html;
  }
}

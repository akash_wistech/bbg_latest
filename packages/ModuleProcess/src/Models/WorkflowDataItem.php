<?php

namespace NaeemAwan\ModuleProcess\Models;
use App\Models\BlameableOnlyTables;

class WorkflowDataItem extends BlameableOnlyTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'workflow_data_item';
}

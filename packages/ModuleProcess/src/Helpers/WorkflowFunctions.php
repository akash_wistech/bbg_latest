<?php
use NaeemAwan\ModuleProcess\Models\Workflow;
use NaeemAwan\ModuleProcess\Models\WorkflowServiceType;
use NaeemAwan\ModuleProcess\Models\WorkflowModule;
use NaeemAwan\ModuleProcess\Models\WorkflowDataItem;
use NaeemAwan\StaffManager\Models\ModuleManager;

if (!function_exists('getWorkflowModules')) {
    /**
     * return list of modules workflow can have
     */
    function getWorkflowModules()
    {
      return [
        'opportunity' => __('app.general.opportunities'),
      ];
    }
}

if (!function_exists('getWorkflowServiceChecked')) {
    /**
     * return if a service type is selected
     */
    function getWorkflowServiceChecked($id,$service_id)
    {
      if(DB::table((new WorkflowServiceType)->getTable())->where('workflow_id', $id)->where('service_type',$service_id)->exists()){
        return true;
      }else{
        return false;
      }
    }
}
if (!function_exists('getWorkflowModuleChecked')) {
    /**
     * return if a modules is selected
     */
    function getWorkflowModuleChecked($id,$module_type)
    {
      if(DB::table((new WorkflowModule)->getTable())->where('workflow_id', $id)->where('module_type',$module_type)->exists()){
        return true;
      }else{
        return false;
      }
    }
}
if (!function_exists('getWorkflowById')) {
    /**
     * return if a modules is selected
     */
    function getWorkflowById($id)
    {
      return Workflow::where('id',$id)->first();
    }
}
if (!function_exists('getWorkFlowStagesListArr')) {
    /**
     * return all work flows with stages
     */
    function getModuleWorkFlowStagesListArr($module)
    {
      $arr = [];
      $moduleTitle = getWorkflowModules()[$module];
      $serviceTypeList = getServiceTypeListArr();
      if($serviceTypeList!=null){
        $services=[];
        foreach($serviceTypeList as $key=>$val){
          $workflows=[];
          $workFlowRows = Workflow::whereIn('id',function($query) use($module){
            $query->select('workflow_id')
            ->from((new WorkflowModule)->getTable())
            ->where('module_type', $module);
          })
          ->whereIn('id',function($query) use($key){
            $query->select('workflow_id')
            ->from((new WorkflowServiceType)->getTable())
            ->where('service_type', $key);
          })
          ->get();
          if($workFlowRows!=null){
            foreach($workFlowRows as $workFlowRow){
              $wfstages = [];
              if($workFlowRow->stages!=null){
                foreach($workFlowRow->stages as $stage){
                  $wfstages[$stage->id]=['id'=>$stage->id,'title'=>$stage->title];
                }
              }
              $workflows[$workFlowRow->id]=['id'=>$workFlowRow->id,'title'=>$workFlowRow->title,'stages'=>$wfstages];
            }
          }
          $services[$key]=['id'=>$key,'title'=>$val,'workflows'=>$workflows];
        }
        $arr[$module]=['id'=>$module,'title'=>$moduleTitle,'services'=>$services];
      }
      return $arr;
    }
}

if (!function_exists('saveWorlflowField')) {
  /**
  * save module managers
  */
  function saveWorlflowField($model,$request)
  {
    // DB::table((new WorkflowDataItem)->getTable())
    // ->where('module_type', $model->moduleTypeId)
    // ->where('module_id', $model->id)
    // ->delete();
    $row = WorkflowDataItem::where('module_type',$model->moduleTypeId)
    ->where('module_id',$model->id)
    ->first();
    if($row==null){
      $row = new WorkflowDataItem;
      $row->module_type = $model->moduleTypeId;
      $row->module_id = $model->id;
      $row->workflow_id = $request->workflow_id;
      $row->workflow_stage_id = $request->workflow_stage_id;
      $row->save();
    }else{
      if($row->workflow_id!=$request->workflow_id || $row->workflow_stage_id!=$request->workflow_stage_id){
        DB::table((new WorkflowDataItem)->getTable())
        ->where('module_type', $model->moduleTypeId)
        ->where('module_id', $model->id)
        ->update([
          'workflow_id' => $request->workflow_id,
          'workflow_stage_id' => $request->workflow_stage_id,
          'updated_at' => date("Y-m-d H:i:s"),
          'updated_by' => Auth::user()->id,
        ]);
      }
    }
	}
}

if (!function_exists('getModuleWorkFlowSavedInfo')) {
  /**
  * save module managers
  */
  function getModuleWorkFlowSavedInfo($model)
  {
    $row = WorkflowDataItem::where('module_type',$model->moduleTypeId)
    ->where('module_id',$model->id)
    ->first();
    if($row!=null){
      return [
        'workflow_id'=>$row['workflow_id'],
        'workflow_stage_id'=>$row['workflow_stage_id']
      ];
    }
	}
}

if (!function_exists('getModuleWorkFlowsListArr')) {
  /**
  * save module managers
  */
  function getModuleWorkFlowsListArr($moduleTypeId)
  {
    return Workflow::whereIn('id',function($query) use($moduleTypeId){
      $query->select('workflow_id')
      ->from((new WorkflowModule)->getTable())
      ->where('module_type', $moduleTypeId);
    })
    ->get();
	}
}

if (!function_exists('getWorkFlowsStagesListArr')) {
  /**
  * save module managers
  */
  function getWorkFlowsStagesListArr($wfId)
  {
    return Workflow::whereIn('id',function($query) use($moduleTypeId){
      $query->select('workflow_id')
      ->from((new WorkflowModule)->getTable())
      ->where('module_type', $moduleTypeId);
    })
    ->get();
	}
}

if (!function_exists('getModuleWorkFlows')) {
  /**
  * save module managers
  */
  function getModuleWorkFlows($moduleTypeId,$layout,$urlPre,$arr)
  {
    $workFlowRows = Workflow::whereIn('id',function($query) use($moduleTypeId){
      $query->select('workflow_id')
      ->from((new WorkflowModule)->getTable())
      ->where('module_type', $moduleTypeId);
    })
    ->get();
    if($workFlowRows!=null){
      foreach($workFlowRows as $workFlowRow){
        $link = '/'.$urlPre;
        $link.= '?';
        if($layout!=''){
          $link.= 'layout='.$layout.'&id='.$workFlowRow->id;
        }else{
          $link.= 'service_type='.$workFlowRow->id;
        }

        $arr[]=['label'=>$workFlowRow->title,'icon'=>'','class'=>'','link'=>$link];
      }
    }
    return $arr;
	}
}

if (!function_exists('getWorkflowStageModulesCountAndAmount')) {
  /**
  * Calculate count and amount for funnel
  */
  function getWorkflowStageModulesCountAndAmount($moduleTypeId,$controller,$wfId,$stgId)
  {
    $count=0;$totalAmount=0;
    $joinedClass = getModelClass()[$moduleTypeId];
    $joinTbleName = (new $joinedClass)->getTable();
    $query =  WorkflowDataItem::select((new WorkflowDataItem)->getTable().'.module_id')
    ->join($joinTbleName,$joinTbleName.".id","=",(new WorkflowDataItem)->getTable().".module_id")
    ->where([
      (new WorkflowDataItem)->getTable().'.module_type'=>$moduleTypeId,
      (new WorkflowDataItem)->getTable().'.workflow_id'=>$wfId,
      (new WorkflowDataItem)->getTable().'.workflow_stage_id'=>$stgId
    ])
    ->orderBy((new WorkflowDataItem)->getTable().'.sort_order','asc');
    if($controller=='opportunity'){
      $query->where('rec_type','o');
    }
    if($controller=='lead'){
      $query->where('rec_type','l');
    }

    $mainTable = (new WorkflowDataItem)->getTable();
    permissionKanbanListTypeFilter($moduleTypeId,$mainTable,$controller,$query);

    $count = $query->count();
    $totalAmount = $query->sum('quote_amount');

    return ['count'=>$count,'totalAmount'=>$totalAmount];
  }
}

if (!function_exists('getWorkflowStageModules')) {
  /**
  * save module managers
  */
  function getWorkflowStageModules($moduleTypeId,$controller,$wfId,$stgId)
  {
    $items=[];
    $joinedClass = getModelClass()[$moduleTypeId];
    $joinTbleName = (new $joinedClass)->getTable();
    $query =  WorkflowDataItem::select((new WorkflowDataItem)->getTable().'.module_id')
    ->join($joinTbleName,$joinTbleName.".id","=",(new WorkflowDataItem)->getTable().".module_id")
    ->where([
      (new WorkflowDataItem)->getTable().'.module_type'=>$moduleTypeId,
      (new WorkflowDataItem)->getTable().'.workflow_id'=>$wfId,
      (new WorkflowDataItem)->getTable().'.workflow_stage_id'=>$stgId
    ])
    ->orderBy((new WorkflowDataItem)->getTable().'.sort_order','asc');
    if($controller=='opportunity'){
      $query->where('rec_type','o');
    }
    if($controller=='lead'){
      $query->where('rec_type','l');
    }

    $mainTable = (new WorkflowDataItem)->getTable();
    permissionKanbanListTypeFilter($moduleTypeId,$mainTable,$controller,$query);

    $results = $query->get();
    if($results!=null){
      foreach($results as $result){
        $moduleRow = (getModelClass()[$moduleTypeId])::where('id',$result->module_id)->first();
        if($moduleRow!=null){
          $btnsArr=[];
          $btnsArr[]=['icon'=>'<i class="fa fa-edit"></i>','url'=>getModelUrlPre()[$moduleTypeId].'/update/'.$moduleRow->id,'title'=>__('common.update')];
          $btnsArr[]=['icon'=>'<i class="fa fa-table"></i>','url'=>getModelUrlPre()[$moduleTypeId].'/view/'.$moduleRow->id,'title'=>__('common.view')];
          $btnsArr = getAssignModuleBtn($btnsArr,$moduleRow);
          $btnsArr = getActivityBtns($btnsArr,$moduleRow,$result);
          $items[] = [
            'id'=>$moduleRow->id,
            'module_type'=>$moduleRow->module_type,
            'module_id'=>$moduleRow->module_id,
            'title'=>$moduleRow->title,
            'service_type'=>$moduleRow->service_type,
            'source'=>$moduleRow->source,
            'descp'=>$moduleRow->descp,
            'btns'=>$btnsArr
          ];
        }
      }
    }
    return $items;
	}
}

if (!function_exists('getWorkflowStatsData')) {
  /**
  * save module managers
  */
  function getWorkflowStatsData($mmodule,$workflow,$model)
  {
    $module = $mmodule;
    if($mmodule=='lead')$module = 'opportunity';
    $stats = [];
    if($workflow->stages!=null){

      $joinedClass = getModelClass()[$module];
      $joinTbleName = (new $joinedClass)->getTable();

      foreach ($workflow->stages as $stage) {
        $query =  WorkflowDataItem::join($joinTbleName,$joinTbleName.".id","=",(new WorkflowDataItem)->getTable().".module_id")
        ->where([
          (new WorkflowDataItem)->getTable().'.module_type'=>$module,
          (new WorkflowDataItem)->getTable().'.workflow_id'=>$workflow->id,
          (new WorkflowDataItem)->getTable().'.workflow_stage_id'=>$stage->id
        ]);
        if($mmodule=='opportunity'){
          $query->where('rec_type','o');
        }
        if($mmodule=='lead'){
          $query->where('rec_type','l');
        }
        //Created At
        if($model->created_at!=null && $model->created_at!=''){
          if(strpos($model->created_at," - ")){
            $query->where(function($query) use($model){
              list($start_date,$end_date)=explode(" - ",$model->created_at);
              $query->where((new WorkflowDataItem)->getTable().'.updated_at','>=',$start_date)
              ->where((new WorkflowDataItem)->getTable().'.updated_at','<=',$end_date);
            });
          }else{
            $query->where((new WorkflowDataItem)->getTable().'.updated_at',$model->created_at);
          }
        }
        //Created By
        if($model->created_by!=null)$query->whereIn((new WorkflowDataItem)->getTable().'.updated_by',$model->created_by);
        $stageCount=$query->count($joinTbleName.'.id');
        // if($stageCount>0){
          $stats[]=['label'=>$stage->title.' ('.$stageCount.')','data'=>$stageCount];
        // }
      }
    }
    return $stats;
	}
}

if (!function_exists('getBoardItemHtml')) {
  /**
  * generate board item html
  */
  function getBoardItemHtml($orgModuleTypeId,$stageModule)
  {
    $html = '';
    $html.='<div class="card card-custom card-stretch gutter-b">';
    $html.='  <div class="card-header border-0">';
    $html.='    <h3 class="card-title font-weight-bolder text-dark">'.$stageModule['title'].'</h3>';
    // if($stageModule['btns']!=null){
    // $html.='    <div class="card-toolbar">';
    // $html.='      <div class="class="dropdown dropdown-inline"">';
    // $html.='        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    // $html.='          <i class="ki ki-bold-more-ver"></i>';
    // $html.='        </a>';
    // $html.='        <div class="dropdown-menu dropdown-menu-xs dropdown-menu-right">';
    // $html.='          <ul class="navi navi-hover">';
    // foreach($stageModule['btns'] as $key=>$val){
    // $html.='            <li class="navi-item">';
    // $html.='               '.$val;
    // $html.='            </li>';
    // }
    // $html.='          </ul>';
    // $html.='        </div>';
    // $html.='      </div>';
    // $html.='    </div>';
    // }
    $html.='  </div>';
    $html.='  <div class="card-body p-2">';
    $pdItem = getPredefinedItem($stageModule['service_type']);
    $html.='    <span class="label label-sm label-light-primary label-inline py-2 mb-2">'.$pdItem->lang->title.'</span>';
    $html.='    <br />';
    $html.='    '.getModulesListArr()[$stageModule['module_type']].getModuleNameByType($stageModule['module_type'],$stageModule['module_id']);;
    $html.='    <br />';
    $html.='    '.(truncateText($stageModule['descp'],85));
    $html.='  </div>';
    // echo '<pre>';print_r($stageModule);echo '</pre>';
    $html.='  '.getAssignedUsersSymbol($orgModuleTypeId,$stageModule['id'],25);
    if($stageModule['btns']!=null){
    $html.='  <div class="card-footer align-items-center">';
    $html.='    <div class="d-flex align-items-center justify-content-between">';
    foreach($stageModule['btns'] as $key=>$val){
      $url = ($val['url']!='' ? ' onclick="window.open(\''.url($val['url']).'\')"' : '');
      $classes = 'btn btn-clean btn-icon btn-xs mr-1'.(isset($val['class'])!='' ? ' '.$val['class'] : '');
      $otherAtrs = (isset($val['otherOpts'])!='' ? ' '.$val['otherOpts'] : '');
      $tooltip = (isset($val['title']) ? ' data-toggle="tooltip" data-placement="top" title="'.$val['title'].'" data-original-title="'.$val['title'].'"' : '');
    $html.='      <a href="javascript:;"'.$url.'" class="'.$classes.'"'.$otherAtrs.$tooltip.'>';
    $html.='        '.$val['icon'];
    $html.='      </a>';
    }
    $html.='    </div>';
    $html.='  </div>';
    }
    $html.='</div>';
    return $html;
	}
}

<?php
namespace NaeemAwan\ModuleProcess\Http\Controllers;

use App\Http\Controllers\Controller;
use NaeemAwan\ModuleProcess\Models\Workflow;
use NaeemAwan\ModuleProcess\Models\WorkflowStage;
use NaeemAwan\ModuleProcess\Models\WorkflowDataItem;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class WorkflowController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $results=Workflow::get();
    return view('workflow::workflow.index', compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Workflow;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required',
        'completion_day'=>'required|integer',
        'assigned_modules'=>'required',
        'stage_name'=>'nullable',
        'stage_color'=>'nullable',
        'stage_descp'=>'nullable',
      ]);
      $model= new Workflow();
      $model->title=$request->title;
      $model->completion_day=$request->completion_day ?? 0;
      $model->assigned_modules=$request->assigned_modules?? [];
      $model->service_type=$request->service_type;
      $model->stage_name=$request->stage_name;
      $model->stage_color=$request->stage_color;
      $model->stage_descp=$request->stage_descp;
      if ($model->save()) {
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('workflow::workflow.saved')]]);
        }else{
          return redirect('workflow')->with('success', __('workflow::workflow.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('app.general.error'),'msg'=>__('workflow::workflow.notsaved'),]]);
        }else{
          return redirect('workflow')->with('error', __('workflow::workflow.notsaved'));
        }
      }
    }
    $modulesList = getWorkflowModules();
    $serviceTypeList = getServiceTypeListArr();
    return view('workflow::workflow.create',compact('model','modulesList','serviceTypeList'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Workflow  $wrkflow
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = Workflow::where('id', $id)->first();
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required',
        'completion_day'=>'required|integer',
        'assigned_modules'=>'required',
        'stage_name'=>'nullable',
        'stage_color'=>'nullable',
        'stage_descp'=>'nullable',
      ]);

      $model= Workflow::find($request->id);
      $model->title=$request->title;
      $model->completion_day=$request->completion_day ?? 0;
      $model->assigned_modules=$request->assigned_modules?? [];
      $model->service_type=$request->service_type;
      $model->stage_id=$request->stage_id;
      $model->stage_name=$request->stage_name;
      $model->stage_color=$request->stage_color;
      $model->stage_descp=$request->stage_descp;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('workflow::workflow.saved'),]]);
        }else{
          return redirect('workflow')->with('success', __('workflow::workflow.saved'));
        }
      }else{
        if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('app.general.error'),'msg'=>__('workflow::workflow.notsaved'),]]);
        }else{
          return redirect('workflow')->with('error', __('workflow::workflow.notsaved'));
        }
      }
    }
    $modulesList = getWorkflowModules();
    $serviceTypeList = getServiceTypeListArr();
    return view('workflow::workflow.update', [
      'model'=>$model,
      'modulesList'=>$modulesList,
      'serviceTypeList'=>$serviceTypeList,
    ]);

  }

  /**
  * Save workflow stage sort order
  *
  * @param  \App\Models\WorkflowStage
  * @return \Illuminate\Http\Response
  */
  public function saveBoardOrder(Request $request)
  {
    $model = $this->findModel($request->id);
    if($model!=null){
      $sortOrder = $request->sort_order;
      $sortOrder = json_decode($sortOrder,true);
      if($sortOrder!=null){
        $i=1;
        foreach($sortOrder as $stageOrder){
          $stageId = $stageOrder['id'];
          $stageId = str_replace("_kb_","",$stageId);

          DB::table((new WorkflowStage)->getTable())
          ->where(['workflow_id'=>$request->id,'id'=>$stageId])
          ->update(['sort_order' =>$i]);
          $i++;
        }
      }
      return new JsonResponse(['ok'=>['heading'=>__('common.success'),'msg'=>__('workflow::workflow.board_order_saved'),]]);
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('workflow::workflow.workflownotfound'),]]);
    }
  }

  /**
  * Save workflow stage item sort order
  *
  * @param  \App\Models\WorkflowStage
  * @return \Illuminate\Http\Response
  */
  public function saveBoardItemOrder(Request $request)
  {
    $model = $this->findModel($request->wfid);
    if($model!=null){
      $stage = $this->findStageModel($request->stgid);
      if($stage!=null){
        $sortOrder = $request->sort_order;
        $sortOrder = json_decode($sortOrder,true);
        if($sortOrder!=null){
          $i=1;
          foreach($sortOrder as $itemOrder){
            $itemId = $itemOrder['id'];
            $row = WorkflowDataItem::where('module_type',$request->module)
            ->where('module_id',$itemId)
            ->where('workflow_id',$request->wfid)
            ->first();
            if($row->workflow_stage_id!=$request->stgid){
              DB::table((new WorkflowDataItem)->getTable())
              ->where(['module_type'=>$request->module,'module_id'=>$itemId,'workflow_id'=>$request->wfid])
              ->update(['workflow_stage_id'=>$request->stgid,'sort_order' =>$i,'updated_at'=>date("Y-m-d H:i:s"),'updated_by'=>Auth::user()->id]);
            }else{
              DB::table((new WorkflowDataItem)->getTable())
              ->where(['module_type'=>$request->module,'module_id'=>$itemId,'workflow_id'=>$request->wfid])
              ->update(['workflow_stage_id'=>$request->stgid,'sort_order' =>$i]);
            }

            $i++;
          }
        }
        return new JsonResponse(['ok'=>['heading'=>__('common.success'),'msg'=>__('workflow::workflow.board_order_saved'),]]);
      }else{
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('workflow::workflow.stagenotfound'),]]);
      }
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('workflow::workflow.workflownotfound'),]]);
    }
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Workflow  $wrkflow
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Workflow::where('id', $id)->first();
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\WorkflowStage
  * @return \Illuminate\Http\Response
  */
  public function findStageModel($id)
  {
    return WorkflowStage::where('id', $id)->first();
  }

  /**
  * Remove the specified resource from storage.
  *
  * @return \Illuminate\Http\Response
  */
  public function destroyStage($id)
  {
    $model = WorkflowStage::where('id',$id)->first();
    $model->delete();
    DB::table($model->getTable())
    ->where('id', $id)
    ->update(['deleted_by' => Auth::user()->id]);
    return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowStage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_stage', function (Blueprint $table) {
            $table->id();
            $table->integer('workflow_id')->nullable();
            $table->string('title',100)->nullable();
            $table->char('color_code',7)->nullable();
            $table->text('descp')->nullable();
            $table->decimal('conversion_rate', $precision = 10, $scale = 2)->default(0)->nullable();
            $table->decimal('value', $precision = 10, $scale = 2)->default(0)->nullable();
            $table->tinyInteger('require_previous')->default(0)->nullable();
            $table->tinyInteger('require_comment')->default(0)->nullable();
            $table->integer('sort_order')->default(0)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_stage');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkflowDataItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_data_item', function (Blueprint $table) {
          $table->char('module_type',25)->nullable();
          $table->integer('module_id')->nullable();
          $table->integer('workflow_id')->nullable();
          $table->integer('workflow_stage_id')->nullable();
          $table->integer('sort_order')->default(0)->nullable();
          $table->timestamps();
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();

          $table->index('module_type');
          $table->index('module_id');
          $table->index('workflow_id');
          $table->index('workflow_stage_id');
          $table->index('sort_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_data_item');
    }
}

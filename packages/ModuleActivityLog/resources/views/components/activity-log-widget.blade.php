<div class="card card-custom card-stretch gutter-b">
  <div class="card-header align-items-center border-0 mt-4 d-none">
    <h3 class="card-title align-items-start flex-column">
      <span class="font-weight-bolder text-dark">{{__('namod-activitylog::activitylog.heading')}}</span>
    </h3>
  </div>
  @if($results->isNotEmpty())
  <div class="card-body pt-0">
      <div class="timeline timeline-6 no-ldt mt-3">
        @foreach($results as $result)
          <div class="timeline-item align-items-start">
            <div class="timeline-badge">
              <i class="fa fa-genderless text-{{$result->action_type}} icon-xl"></i>
            </div>
            <div class="font-weight-mormal font-size-lg timeline-content pl-3">
              {!!$result->message!!}
              <div class="text-dark-75 text-muted font-size-sm">{{convertTime($result->created_at)}}</div>
            </div>
          </div>
        @endforeach
      </div>
  </div>
  <div class="card-footer text-right">
    {{ $results->links() }}
  </div>
  @else
    <div class="card-body pt-0">
      <x-empty-spaces/>
    </div>
  @endif
</div>

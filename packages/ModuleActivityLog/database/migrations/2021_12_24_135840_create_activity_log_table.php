<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_log', function (Blueprint $table) {
            $table->id();
            $table->string('controller_id',50)->nullable();
            $table->string('action_id',50)->nullable();
            $table->char('module_type',20)->nullable();
            $table->integer('module_id')->default(0)->nullable();
            $table->string('url',150)->nullable();
            $table->text('message')->nullable();
            $table->string('target_url',150)->nullable();
            $table->string('user_ip',32)->nullable();
            $table->text('page_referrer')->nullable();
            $table->string('action_type',50)->nullable();
            $table->text('request_data')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_log');
    }
}

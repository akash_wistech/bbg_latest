<?php
namespace NaeemAwan\ModuleActivityLog\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\ModuleActivityLog\Models\ActivityLog;
use NaeemAwan\ModuleActivityLog\View\Components\ActivityLogWidget;
use Illuminate\Support\Facades\Log;

class ActivityLogServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'namod-activitylog');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'namod-activitylog');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/namod-activitylog'),
    ],'namod-activitylog-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'namod-activitylog-migrations');

    $this->loadViewComponentsAs('namod-activitylog', [
      ActivityLogWidget::class,
    ]);

    // dashboard_widgets()->registerItem([
    //   'id'          => 'dbActivity',
    //   'priority'    => 3,
    //   'name'        => 'dashboard-activity',
    //   'viewHtml'    => '<x-namod-activitylog-activity-log-widget />',
    //   'visible'     => true,
    // ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

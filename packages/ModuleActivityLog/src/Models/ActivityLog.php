<?php

namespace NaeemAwan\ModuleActivityLog\Models;
use App;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Blameable;

class ActivityLog extends Model
{
  use Blameable;

  protected $table = 'activity_log';

  protected $dates = ['created_at','updated_at'];

  protected $fillable = [
    'controller_id',
    'action_id',
    'url',
    'message',
    'target_url',
    'user_ip',
    'page_referrer',
    'action_type',
    'request_data',
  ];
}

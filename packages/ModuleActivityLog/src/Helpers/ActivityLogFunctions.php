<?php
// use App;
use NaeemAwan\ModuleActivityLog\Models\ActivityLog;
//
if (!function_exists('logAction')) {
    /**
     * Save a log entry
     */
    function logAction($request,$data)
    {
      $controllerActionArr=[];
      if(function_exists('getCurrentControllerAndAction')){
        $controllerActionArr = getCurrentControllerAndAction();
      }
      $referrer = null;
      if($request!=null && $request->has('referrer')){
        $referrer = $request->input('referrer');
      }else if(isset($_SERVER['HTTP_REFERER'])){
        $referrer = $_SERVER['HTTP_REFERER'];
      }
      $logItem = new ActivityLog;
      if($controllerActionArr!=null && isset($controllerActionArr['controller']))$logItem->controller_id = $controllerActionArr['controller'];
      if($controllerActionArr!=null && isset($controllerActionArr['action']))$logItem->action_id = $controllerActionArr['action'];
      $logItem->url = URL::current();
      $logItem->message = $data['message'];
      if(isset($data['module_type']))$logItem->module_type = $data['module_type'];
      if(isset($data['module_id']))$logItem->module_id = $data['module_id'];
      if(isset($data['target_url']))$logItem->target_url = $data['target_url'];
      $logItem->user_ip = request()->getClientIp();
      $logItem->page_referrer = $referrer;
      $logItem->action_type = $data['action_type'];
      if($request!=null){
        $logItem->request_data = json_encode($request->all());
      }
      $logItem->save();
    }
}

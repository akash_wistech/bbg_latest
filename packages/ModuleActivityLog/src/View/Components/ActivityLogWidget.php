<?php
namespace NaeemAwan\ModuleActivityLog\View\Components;

use Illuminate\View\Component;
use NaeemAwan\ModuleActivityLog\Models\ActivityLog;
use Auth;

class ActivityLogWidget extends Component
{
  public $module,$id;
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($module=null,$id=null)
  {
    $this->module=$module;
    $this->id=$id;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $query = ActivityLog::where('id','>',0);
    //Super Admin
    if(Auth::user()->user_type!=20){
      $query->where('created_by',Auth::user()->id);
    }
    if($this->module!=null){
      $query->where('module_type',$this->module);
    }
    if($this->id!=null){
      $query->where('module_id',$this->id);
    }
    $results = $query->orderBy('updated_at','desc')->simplePaginate(10);
    return view('namod-activitylog::components.activity-log-widget',[
      'module' => $this->module,
      'id' => $this->id,
      'results' => $results,
    ]);
  }
}

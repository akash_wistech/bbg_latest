<?php
use Illuminate\Support\Facades\Route;

// Route::group(['middleware' => ['web','auth']], function () {
//   Route::get('/pages', [NaeemAwan\ModuleCMS\Http\Controllers\PagesController::class, 'index'])->name('pages');
//   Route::match(['post','get'],'/pages/create', [NaeemAwan\ModuleCMS\Http\Controllers\PagesController::class, 'create'])->name('pages.create');
//   Route::match(['post','get'],'/pages/edit/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\PagesController::class, 'edit'])->name('pages.edit.{id}');
//   Route::match(['post','get'],'/pages/status/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\PagesController::class, 'status'])->name('pages.status.{id}');
//   Route::post('/pages/update/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\PagesController::class, 'update'])->name('pages.update');
//   Route::post('/pages/delete/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\PagesController::class, 'destroy'])->name('pages.destroy.{id}');
//
//   Route::get('/menus', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'index'])->name('menus');
//   Route::match(['post','get'],'/menus/create', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'create'])->name('menus.create');
//   Route::match(['post','get'],'/menus/edit/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'edit'])->name('menus.edit.{id}');
//   Route::match(['post','get'],'/menus/status/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'status'])->name('menus.status.{id}');
//   Route::match(['post','get'],'/menus/add-item', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'addItem'])->name('menus.add.item');
//   Route::post('/menus/update/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'update'])->name('menus.update');
//   Route::post('/menus/delete/{id}', [NaeemAwan\ModuleCMS\Http\Controllers\MenusController::class, 'destroy'])->name('menus.destroy.{id}');
// });

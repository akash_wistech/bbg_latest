<?php
use NaeemAwan\Base\Facades\DashboardWidgetsFacade;

if (!function_exists('dashboard_widgets')) {
  /**
  * @return \NaeemAwan\Base\Supports\DashboardWidgets
  */
  function dashboard_widgets()
  {
    return DashboardWidgetsFacade::getFacadeRoot();
  }
}


if (!function_exists('load_dashboard_widgets')) {
  /**
  * @return \NaeemAwan\Base\Supports\DashboardWidgets
  */
  function load_dashboard_widgets()
  {
    return dashboard_widgets()->getAll();
  }
}

if (!function_exists('getMonthsInDates')) {
  /**
  * @return integer
  */
  function getMonthsInDates($d1,$d2)
  {
    // Creates DateTime objects
    $datetime1 = date_create($d1);
    $datetime2 = date_create($d2);

    // Calculates the difference between DateTime objects
    $interval = date_diff($datetime1, $datetime2);

    // Printing result in years & months format
    echo $interval->format('%m');
  }
}

if (!function_exists('getDataInDates')) {
  /**
  * @return integer
  */
  function getDataInDates( $diffType, $start, $end, $relative=false){

       if( is_string( $start)) $start = date_create( $start);
       if( is_string( $end)) $end = date_create( $end);

       $diff = date_diff( $start, $end, ! $relative);

       switch( $diffType){
           case "y":
               $total = $diff->y + $diff->m / 12 + $diff->d / 365.25; break;
           case "m":
               $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
               break;
           case "d":
               $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
               break;
           case "h":
               $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
               break;
           case "i":
               $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
               break;
           case "s":
               $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
               break;
          }
       if( $diff->invert)
               return -1 * $total;
       else    return $total;
   }
}

<?php
namespace NaeemAwan\Base\View\Components;
use Auth;
use Illuminate\View\Component;

class ViewCard extends Component
{
  public $model=null;
  public $summary='';
  public $tabs='';
  public $overview='';
  /**
  * Create the component instance.
  *
  * @param  array  $tabs
  * @return void
  */
  public function __construct($model,$summary,$overview,$tabs)
  {
    $this->model=$model;
    $this->summary=$summary;
    $this->tabs=$tabs;
    $this->overview=$overview;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    return view('base::components.view-card',[
      'model'=>$this->model,
      'summary'=>$this->summary,
      'tabs'=>$this->tabs,
      'overview'=>$this->overview,
    ]);
  }
}

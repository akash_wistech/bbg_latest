<?php
namespace NaeemAwan\Base\View\Components;
use Auth;
use Illuminate\View\Component;

class EmptySpace extends Component
{
  public $icon='';
  public $showText=false;
  public $text='';
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($icon='',$text='',$showText=false)
  {
    $this->icon=$icon;
    $this->text=$text;
    $this->showText=$showText;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $icon='';
    $icon = $this->icon;
    if($icon=='')$icon = asset('assets/media/svg/misc/empty_space-gray.svg');
    $text='';
    if($this->showText==true){
      $text = $this->text;
      if($text=='')$text=__('common.no_results_found');
    }

    $html ='<div class="text-center text-primary">';
    $html.='  <img src="'.$icon.'" width="50%" />';
    if($text!=''){
      $html.='  <br />';
      $html.='  '.$text;
    }
    $html.='  </div>';
    return $html;
  }
}

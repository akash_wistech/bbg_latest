<?php
namespace NaeemAwan\Base\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use NaeemAwan\Base\View\Components\EmptySpace;
use NaeemAwan\Base\View\Components\ViewCard;

class NABaseServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'base');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'base');

    Blade::component('empty-spaces', EmptySpace::class);
    Blade::component('view-card', ViewCard::class);
  }

  /**
  * Register any application services.
  *
  * @return void
  */
  public function register()
  {
  }
}

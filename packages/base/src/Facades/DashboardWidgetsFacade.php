<?php

namespace NaeemAwan\Base\Facades;

use NaeemAwan\Base\Supports\DashboardWidgets;
use Illuminate\Support\Facades\Facade;

class DashboardWidgetsFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return DashboardWidgets::class;
    }
}

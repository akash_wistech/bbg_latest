<?php
namespace Naeemawan\Base\Supports;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Psr\SimpleCache\InvalidArgumentException;
use RuntimeException;
use URL;

class DashboardWidgets
{
    /**
     * Get all registered widgets from package
     * @var array
     */
    protected $widgets = [];

    /**
     * Add Widget
     * @param array $options
     * @return $this
     */
    public function registerItem(array $options): self
    {
        if (isset($options['children'])) {
            unset($options['children']);
        }

        $defaultOptions = [
            'id'          => '',
            'priority'    => 99,
            'name'        => '',
            'viewHtml'    => '',
            'visible'     => true,
        ];

        $options = array_merge($defaultOptions, $options);
        $id = $options['id'];


        $this->widgets[$id] = $options;

        return $this;
    }

    /**
     * @param array|string $id
     * @return $this
     */
    public function removeItem($id): self
    {
        $id = is_array($id) ? $id : func_get_args();
        foreach ($id as $item) {
          Arr::forget($this->widgets, $item);
        }
        return $this;
    }

    /**
     * @param string $id
     * @param null|string $parentId
     * @return bool
     */
    public function hasItem($id, $parentId = null): bool
    {
        return Arr::has($this->widgets, $id . '.name');
    }


    /**
     * Rearrange links
     * @return Collection
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function getAll(): Collection
    {
        // do_action('render_dashboard_widgets');

        $widgets = $this->widgets;

        return collect($widgets)->sortBy('priority');
    }
}

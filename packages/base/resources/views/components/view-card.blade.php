<div class="d-flex flex-row">
  <div class="flex-row-auto offcanvas-mobile w-300px w-xl-350px">
    <div class="card card-custom card-stretch">
      <div class="card-body pt-4">
        {!! $summary !!}
        {!! $tabs !!}
      </div>
    </div>
  </div>
  <div class="flex-row-fluid ml-lg-8">
    {!! $overview !!}
  </div>
</div>

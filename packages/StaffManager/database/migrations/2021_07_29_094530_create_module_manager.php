<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_manager', function (Blueprint $table) {
          $table->char('module_type',15);
          $table->integer('module_id');
          $table->integer('staff_id');

          $table->index('module_type');
          $table->index('module_id');
          $table->index('staff_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_manager');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAdminMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menu', function (Blueprint $table) {
            $table->id();
            $table->integer('parent')->default(0)->nullable();
            $table->string('icon',100)->nullable();
            $table->string('title',50)->nullable();
            $table->string('route',50)->nullable();
            $table->string('controller_id',50)->nullable();
            $table->string('action_id',50)->nullable();
            $table->tinyInteger('show_in_menu')->default(0)->nullable();
            $table->tinyInteger('ask_list_type')->default(0)->nullable();
            $table->tinyInteger('rank')->default(0)->nullable();

            $table->index('parent');
            $table->index('show_in_menu');
            $table->index('rank');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_menu');
    }
}

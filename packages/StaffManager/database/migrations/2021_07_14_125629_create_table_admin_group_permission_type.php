<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAdminGroupPermissionType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_group_permission_type', function (Blueprint $table) {
          $table->integer('group_id');
          $table->integer('menu_id');
          $table->string('controller_id',50);
          $table->tinyInteger('list_type')->default(1);

          $table->index('group_id');
          $table->index('menu_id');
          $table->index('list_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_group_permission_type');
    }
}

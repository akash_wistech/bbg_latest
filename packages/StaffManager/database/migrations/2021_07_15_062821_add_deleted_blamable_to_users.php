<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedBlamableToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->tinyInteger('user_type')->default(0)->nullable()->after('id');
          $table->integer('permission_group_id')->default(0)->nullable()->after('user_type');
          $table->string('signature_image',100)->nullable()->after('image');
          $table->timestamp('deleted_at')->nullable();
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();
          $table->integer('deleted_by')->nullable();

          $table->index('user_type');
          $table->index('permission_group_id');
          $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->dropColumn('user_type');
          $table->dropColumn('deleted_at');
          $table->dropColumn('created_by');
          $table->dropColumn('updated_by');
          $table->dropColumn('deleted_by');
        });
    }
}

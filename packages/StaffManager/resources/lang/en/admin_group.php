<?php
return [
  'heading' => 'Permission Groups',
  'saved' => 'Permission Group saved successfully',
  'all_listing' => 'All Listing',
  'own_listing' => 'Own Listing',
];

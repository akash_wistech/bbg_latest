<?php
return [
  'heading' => 'Staff',
  'permission_group_id' => 'Permission Group',
  'saved' => 'Staff member saved successfully',
  'manager_id' => 'Assign To',
  'assign_module_to' => 'Assignment: :module',
  'assignment_successfully' => 'Assigned successfully',
  'profile_updated' => 'Profile updated successfully',
  'password_updated' => 'Password updated successfully',
  'old_password' => 'Old Password',
  'new_password' => 'New Password',
  'confirm_password' => 'Confirm Password',
  'info_saved' => 'information saved successfully',
];

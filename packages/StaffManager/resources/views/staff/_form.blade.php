@php
$memeberImage=$model->image;
$jobroles_list_id = getSetting('staffjobroles_list_id');
@endphp
@push('cssStyles')
.custom-file-label::after{content: "{{ __('common.browse') }}";}
.file-input .input-group-text img{width:30px; height:30px;}
.file-input .input-group-text.img-icon{padding:3px;}
@endpush

{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('permission_group_id',__('staffmanager::staff.permission_group_id')) }}
          {{ Form::select('permission_group_id', [''=>__('common.select')] + getAdminGroupList(), $model->permission_group_id, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('jobrole_id',__('common.jobrole_id')) }}
          {{ Form::select('jobrole_id', [''=>__('common.select')] + getPredefinedListItemsArr($jobroles_list_id), $model->jobrole_id, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('fullname',__('common.fullname')) }}
          {{ Form::text('fullname', $model->name, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('phone',__('common.phone')) }}
          {{ Form::text('phone', $model->phone, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('email',__('common.email')) }}
          {{ Form::email('email', $model->email, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('password',__('common.password')) }}
          {{ Form::password('password', ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('image',__('common.image')) }}
          <div class="input-group file-input">
            <div class="input-group-prepend">
              <span class="input-group-text{{ $memeberImage!='' ? ' img-icon' : '' }}">
                @if($memeberImage!='')
                <img src="{{ getStorageFileUrl('images/'.$memeberImage) }}" width="26" height="26" />
                @else
                <i class="fas fa-user"></i>
                @endif
              </span>
            </div>
            <div class="custom-file">
              {{ Form::file('image', ['class'=>'form-control custom-file-input', 'accept'=>'.png, .jpg, .jpeg']) }}
              <label class="custom-file-label text-truncate" for="image">{{ __('common.choose_file') }}</label>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('status',__('common.status')) }}
          {{ Form::select('status', getStatusArr(), $model->status, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/staff') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}

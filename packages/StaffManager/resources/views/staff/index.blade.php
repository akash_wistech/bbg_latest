@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/staff/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('staffmanager::staff.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('staffmanager::staff.heading')
]])
@endsection


@section('content')

<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <div class="overlay-wrapper">
      @if($results->isNotEmpty())
      <table class="table">
        <thead>
          <tr>
            <th>{{__('common.id')}}</th>
            <th>{{__('common.photo')}}</th>
            <th>{{__('staffmanager::staff.permission_group_id')}}</th>
            <th>{{__('common.jobrole_id')}}</th>
            <th>{{__('common.email')}}</th>
            <th>{{__('common.name')}}</th>
            <th>{{__('common.phone')}}</th>
            <th width="100">{{__('common.status')}}</th>
            <th width="100">{{__('common.action')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($results as $result)
          @php
          $jobRoleTitle = '';
          $jobRole = getPredefinedItem($result->jobrole_id);
          if($jobRole!=null){
            $jobRoleTitle=$jobRole->lang->title;
          }
          $memeberImage=$result->image;
          @endphp
          <tr id="result_id_{{ $result->id }}">
            <td>{{ $result->id }}</td>
            <td>
              @if($memeberImage!='')
              <img src="{{ getStorageFileUrl('images/'.$memeberImage) }}" width="26" height="26" />
              @else
              <i class="fas fa-user"></i>
              @endif
            </td>
            <td>{{ $result->permissionGroup->title }}</td>
            <td>{{ $jobRoleTitle }}</td>
            <td>{{ $result->email }}</td>
            <td>{{ $result->name }}</td>
            <td>{{ $result->phone }}</td>
            <td>{!!getEnableDisableIconWithLink($result->status,'/staff/status/'.$result->id,getStatusConfirmMsg()[$result->status],'grid-table')!!}</td>
            <td>
              @if(checkActionAllowed('update'))
              <a href="{{ url('/staff/update',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon edit' title='{{__('common.update')}}' data-id="{{$result->id}}">
                <i class="text-dark-50 flaticon-edit"></i>
              </a>
              @endif
              @if(checkActionAllowed('delete'))
              <a href="{{ url('/staff/destroy',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon act-confirmation' title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
              </a>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <div class="text-center">{{ __('common.noresultfound') }}</div>
      @endif
    </div>
  </div>
</div>
@endsection

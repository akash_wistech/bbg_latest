@extends('layouts.app')
@section('title', __('common.change_password'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'staff'=>__('common.change_password'),
]])
@endsection
@section('content')
{!! Form::model($model) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('old_password',__('staffmanager::staff.old_password')) }}
          {{ Form::password('old_password', ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('new_password',__('staffmanager::staff.new_password')) }}
          {{ Form::password('new_password', ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('new_password_confirmation',__('staffmanager::staff.confirm_password')) }}
          {{ Form::password('new_password_confirmation', ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/staff') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}

@endsection

@extends('layouts.app')
@section('title', __('common.update_profile'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'staff'=>__('common.update_profile'),
]])
@endsection
@section('content')
@php
$memeberImage=$model->image;
$memeberSignatureImage=$model->signature_image;
$jobroles_list_id = getSetting('jobroles_list_id');
if($memeberImage!=''){
  $memeberImage = getStorageFileUrl('images/'.$memeberImage);
}else{
  $memeberImage = asset('assets/media/users/blank.png');
}
$defImg = $memeberImage;
if($memeberSignatureImage!=''){
  $memeberSignatureImage = getStorageFileUrl('images/'.$memeberSignatureImage,'large');
}else{
  $memeberSignatureImage = asset('assets/media/users/signature.png');
}
$defSignatureImg = $memeberSignatureImage;
@endphp
@push('cssStyles')
.custom-file-label::after{content: "{{ __('common.browse') }}";}
.file-input .input-group-text img{width:30px; height:30px;}
.file-input .input-group-text.img-icon{padding:3px;}
.image-input .image-input-wrapper.signature-image{width:280px;}
.image-input .btn.btn-icon.btn-xs{padding-left: 5px;}
@endpush
{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row mb-5">
      <div class="col-12 col-sm-6 text-center">
        <label><strong>{{__('common.profile_photo')}}</strong></label><br />
        <div class="image-input image-input-empty image-input-outline" style="background-image: url('{{$memeberImage}}')">
					<div class="image-input-wrapper"></div>
					<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="{{__('common.change_profile_photo')}}">
						<i class="fa fa-pen icon-sm text-muted"></i>
            {{ Form::file('image', ['class'=>'icon-file-input','accept'=>'.png, .jpg, .jpeg']) }}
					</label>
					<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow p-0 remove-ii-img" data-action="cancel" data-defimg="{{$defImg}}" data-toggle="tooltip" title="" data-original-title="{{__('common.cancel_profile_photo')}}">
						<i class="ki ki-bold-close icon-xs text-muted"></i>
					</span>
				</div>
			</div>
      <div class="col-12 col-sm-6 text-center">
        <label><strong>{{__('common.signature')}}</strong></label><br />
        <div class="image-input image-input-empty image-input-outline" style="background-image: url('{{$defSignatureImg}}')">
					<div class="image-input-wrapper signature-image"></div>
					<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="{{__('common.change_signature')}}">
						<i class="fa fa-pen icon-sm text-muted"></i>
            {{ Form::file('signature_image', ['class'=>'icon-file-input','accept'=>'.png, .jpg, .jpeg']) }}
					</label>
					<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow p-0 remove-ii-img" data-action="cancel" data-defimg="{{$defImg}}" data-toggle="tooltip" title="" data-original-title="{{__('common.cancel_signature')}}">
						<i class="ki ki-bold-close icon-xs text-muted"></i>
					</span>
				</div>
			</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('fullname',__('common.fullname')) }}
          {{ Form::text('fullname', $model->name, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('phone',__('common.phone')) }}
          {{ Form::text('phone', $model->phone, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('email',__('common.email')) }}
          {{ Form::email('email', $model->email, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/staff') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}

@endsection

@extends('layouts.app')
@section('title', __('staffmanager::staff.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'staff'=>__('staffmanager::staff.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('staffmanager::staff._form')
@endsection

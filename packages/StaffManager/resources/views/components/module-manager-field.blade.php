<div class="row">
  <div class="col-12 col-sm-12">
    <div class="form-group">
      {{ Form::label('manager_id',__('staffmanager::staff.manager_id')) }}
      {{ Form::select('manager_id[]', getStaffMemberListArr(), null, ['class'=>'form-control frmselect2','multiple'=>'multiple']) }}
    </div>
  </div>
</div>

@push('jsScripts')
$(".frmselect2").select2({
	allowClear: true,
	width: "100%",
});
@endpush

@php
$menuLinks=getAdminMenuList();
@endphp

@push('jsScripts')
$(".cbl1").on("click",function(){
  thisid=$(this).attr("value");
  $(".cbl1_"+thisid).prop("checked",$(this).prop("checked"));
  $.uniform.update(".cbl1_"+thisid);
});
$(".cbl2").on("click",function(){
  thisid=$(this).attr("value");
  $(".cbl2_"+thisid).prop("checked",$(this).prop("checked"));
  $.uniform.update(".cbl2_"+thisid);
});
$(".cbl3").on("click",function(){
  thisid=$(this).attr("value");
  $(".cbl3_"+thisid).prop("checked",$(this).prop("checked"));
  $.uniform.update(".cbl3_"+thisid);
});
@endpush

{!! Form::model($model) !!}
<div class="card multi-tabs card-custom mb-2">
  <div class="card-body scp">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('title',__('common.title')) }}
          {{ Form::text('title', $model->title, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('status',__('common.status')) }}
          {{ Form::select('status', getStatusArr(), $model->status, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    @if ($menuLinks)
    <div class="row">
      @foreach ($menuLinks as $menuLink)
      @php
      $boxSelected=false;
      if($model!=null){
        $boxSelected=isChecked($model->id,$menuLink['id']);
      }
      $secondLevelOptions=getSubOptions($menuLink['id']);
      @endphp
      <div class="col-sm-6">
        <div class="card card-custom card-border mb-3 card-collapse" data-card="true">
          <div class="card-header bg-primary">
            <div class="card-title">
              <h3 class="card-label text-white">
                <label>
                  <input type="checkbox"{{ ($boxSelected==true ? ' checked="checked"' : '') }} class="cbl1" name="menu_opts[]" value="{{ $menuLink['id'] }}" /> {{ $menuLink['title'] }}
                </label>
              </h3>
            </div>
            <div class="card-toolbar">
              @if($menuLink['ask_list_type']==1 && $menuLink['action_id']=='index')
              @php
                $model->list_type[$menuLink['id']] = getMenuPermissinListingType($model->id,$menuLink['id']);
              @endphp
              <span>
                {{ Form::select('list_type['.$menuLink['id'].']', [''=>__('common.select')] + getPermissionListingType(), null, ['class'=>'form-control form-control-sm']) }}
              </span>
              @endif
              <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                <i class="ki ki-arrow-down icon-nm text-white"></i>
              </a>
            </div>
          </div>
          <div class="card-body" style="display:none;">
            @if ($secondLevelOptions!=null)
            @foreach ($secondLevelOptions as $secondLevelOption)
            @php
            $boxSelected=false;
            if($model!=null){
              $boxSelected=isChecked($model->id,$secondLevelOption['id']);
            }
            $thirdLevelOptions=getSubOptions($secondLevelOption['id']);
            @endphp
            @if ($thirdLevelOptions!=null)
            <div class="card card-custom card-border mt-3" data-card="true">
              <div class="card-header bg-success">
                <div class="card-title">
                  <h3 class="card-label text-white">
                    <label>
                      <input type="checkbox"{{ ($boxSelected==true ? ' checked="checked"' : '') }} class="cbl2 cbl1_{{ $menuLink['id'] }}" name="menu_opts[]" value="{{ $secondLevelOption['id'] }}" /> {{ $secondLevelOption['title'] }}
                    </label>
                  </h3>
                </div>
                <div class="card-toolbar">
                  @if($secondLevelOption['ask_list_type']==1 && $secondLevelOption['action_id']=='index')
                  @php
                    $model->list_type[$secondLevelOption['id']] = getMenuPermissinListingType($model->id,$secondLevelOption['id']);
                  @endphp
                  <span>
                    {{ Form::select('list_type['.$secondLevelOption['id'].']', [''=>__('common.select')] + getPermissionListingType(), null, ['class'=>'form-control form-control-sm']) }}
                  </span>
                  @endif
                  <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                    <i class="ki ki-arrow-down icon-nm text-white"></i>
                  </a>
                </div>
              </div>
              <div class="card-body">
                @foreach ($thirdLevelOptions as $thirdLevelOption)
                @php
                $boxSelected=false;
                if($model!=null){
                  $boxSelected=isChecked($model->id,$thirdLevelOption['id']);
                }
                $fourthLevelOptions=getSubOptions($thirdLevelOption['id']);
                @endphp

                @if ($fourthLevelOptions!=null)
                <div class="card card-custom card-border mt-3">
                  <div class="card-header bg-warning">
                    <div class="card-title">
                      <h3 class="card-label text-white">
                        <label>
                          <input type="checkbox"{{ ($boxSelected==true ? ' checked="checked"' : '') }} class="cbl3 cbl1_{{ $menuLink['id'] }} cbl2_{{ $secondLevelOption['id'] }}" name="menu_opts[]" value="{{ $thirdLevelOption['id'] }}" />
                          {{ $thirdLevelOption['title'] }}
                        </h3>
                      </div>
                      <div class="card-toolbar">
                        <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
                          <i class="ki ki-arrow-down icon-nm text-white"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    @foreach ($fourthLevelOptions as $fourthLevelOption)
                    @php
                    $boxSelected=false;
                    if($model!=null){
                      $boxSelected=isChecked($model->id,$fourthLevelOption['id']);
                    }
                    @endphp
                    <label>
                      <input type="checkbox"{{ ($boxSelected==true ? ' checked="checked"' : '') }} class="cbl1_{{ $menuLink['id'] }} cbl2_{{ $secondLevelOption['id'] }} cbl3_{{ $thirdLevelOption['id'] }}" data-parent="" name="menu_opts[]" value="{{ $fourthLevelOption['id'] }}" />
                      {{ $fourthLevelOption['title'] }}
                    </label>
                    @endforeach
                  </div>
                </div>
                @else
                <label>
                  <input type="checkbox"{{ ($boxSelected==true ? ' checked="checked"' : '') }} class="cbl1_{{ $menuLink['id'] }} cbl2_{{ $secondLevelOption['id'] }}" data-parent="" name="menu_opts[]" value="{{ $thirdLevelOption['id'] }}" />
                  {{ $thirdLevelOption['title'] }}
                </label>
                @endif
                @endforeach
              </div>
            </div>
            @else
            <label>
              <input type="checkbox"{{ ($boxSelected==true ? ' checked="checked"' : '') }} class="cbl2 cbl1_{{ $menuLink['id'] }}" name="menu_opts[]" value="{{ $secondLevelOption['id'] }}" /> {{ $secondLevelOption['title'] }}
            </label>
            @endif
            @endforeach
            @endif
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @endif
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/admin-group') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}

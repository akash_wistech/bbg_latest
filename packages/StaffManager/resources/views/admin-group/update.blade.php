@extends('layouts.app')
@section('title', __('staffmanager::admin_group.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'admin-group'=>__('staffmanager::admin_group.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('staffmanager::admin-group._form')
@endsection

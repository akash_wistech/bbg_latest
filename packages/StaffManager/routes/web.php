<?php

use Illuminate\Support\Facades\Route;
use NaeemAwan\StaffManager\Http\Controllers\StaffController;
use NaeemAwan\StaffManager\Http\Controllers\AdminGroupController;

Route::group(['middleware' => ['web','auth']], function () {

  Route::match(['get', 'post'], 'update-profile', [StaffController::class, 'updateProfile']);
  Route::match(['get', 'post'], 'change-password', [StaffController::class, 'changePassword']);
  Route::post('save-dt-cols', [StaffController::class, 'saveDataTableColumns'])->name('savedtcols');

  // Route::resource('staff', [StaffController::class,'index']);
  Route::get('staff', [StaffController::class,'index']);
  Route::match(['get', 'post'], 'staff/create', [StaffController::class,'create']);
  Route::match(['get', 'post'], 'staff/update/{id}', [StaffController::class,'update']);
  Route::match(['get', 'post'], 'staff/status/{id}', [StaffController::class,'status']);
  Route::post('staff/destroy/{id}', [StaffController::class,'destroy']);

  Route::match(['get', 'post'], 'assign-module/{module}/{id}', [StaffController::class,'assignModule']);

  // Route::resource('admin-group', [AdminGroupController::class]);
  Route::get('admin-group', [AdminGroupController::class,'index']);
  Route::match(['get', 'post'], 'admin-group/create', [AdminGroupController::class,'create']);
  Route::match(['get', 'post'], 'admin-group/update/{id}', [AdminGroupController::class,'update']);
  Route::match(['get', 'post'], 'admin-group/status/{id}', [AdminGroupController::class,'status']);

  Route::post('admin-group/destroy/{id}', [AdminGroupController::class,'destroy']);
});

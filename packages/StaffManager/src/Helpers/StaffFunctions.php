<?php
// use Auth;
use App\Models\User;
use NaeemAwan\ModuleActionLog\Models\ActionLogManager;
use NaeemAwan\StaffManager\Models\ModuleManager;
use NaeemAwan\StaffManager\Models\AdminGroup;
use NaeemAwan\StaffManager\Models\AdminGroupPermissions;
use NaeemAwan\StaffManager\Models\AdminGroupPermissionType;
use NaeemAwan\StaffManager\Models\AdminMenu;

if (!function_exists('getListingTypeByController')) {
	/**
  * returns listing type value for the group
  */
	function getListingTypeByController($controller)
	{
		if($controller!=null){
			$result=AdminGroupPermissionType::select(['list_type'])
			->where(['group_id'=>Auth::user()->permission_group_id,'controller_id'=>$controller])
			->first();
			if($result!=null){
				return $result['list_type'];
			}else{
				return 1;
			}
		}else{
			return 1;
		}
	}
}

if (!function_exists('permissionListTypeFilter')) {
	/**
	* add list type filter
	*/
	function permissionListTypeFilter($moduleTypeId,$mainTable,$controller,$query)
	{
		if(getListingTypeByController($controller)==2){
			$query->whereIn('id',function($query) use ($moduleTypeId,$mainTable,$controller){
				$query->select('module_id')
		    ->from((new ModuleManager)->getTable())
		    ->where(['module_type'=>$moduleTypeId,'staff_id'=>Auth::user()->id]);
			});
			// $query->join((new ModuleManager)->getTable(),(new ModuleManager)->getTable().'.module_id','=',$mainTable.'.id')
			// ->where((new ModuleManager)->getTable().'.module_type',$moduleTypeId)
			// ->where([(new ModuleManager)->getTable().'.staff_id' => Auth::user()->id]);
    }
	}
}

if (!function_exists('permissionKanbanListTypeFilter')) {
	/**
	* add list type filter
	*/
	function permissionKanbanListTypeFilter($moduleTypeId,$mainTable,$controller,$query)
	{
		if(getListingTypeByController($controller)==2){
			$query->whereIn('id',function($query) use ($moduleTypeId,$mainTable,$controller){
				$query->select('module_id')
		    ->from((new ModuleManager)->getTable())
		    ->where(['module_type'=>$moduleTypeId,'staff_id'=>Auth::user()->id]);
			});
			// $query->join((new ModuleManager)->getTable(),(new ModuleManager)->getTable().'.module_id','=',$mainTable.'.module_id')
			// ->where((new ModuleManager)->getTable().'.module_type',$moduleTypeId)
			// ->where([(new ModuleManager)->getTable().'.staff_id' => Auth::user()->id]);
    }
	}
}

if (!function_exists('getPermissionListingType')) {
	/**
	* returns listing types for permission group
	*/
	function getPermissionListingType()
	{
		return [
			'1'=>__('staffmanager::admin_group.all_listing'),
			'2'=>__('staffmanager::admin_group.own_listing'),
		];
	}
}

if (!function_exists('getAdminGroupList')) {
  /**
  * return admin permission groups
  */
  function getAdminGroupList()
  {
    return AdminGroup::select('id','title')->enabled()->get()->pluck('title', 'id')->toArray();
  }
}

if (!function_exists('getAdminMenuList')) {
  /**
  * return admin menu list
  */
  function getAdminMenuList()
  {
    return AdminMenu::select('id','title','action_id','ask_list_type')->where('parent',0)->orderBy('rank','asc')->get()->toArray();
  }
}

if (!function_exists('getSubOptions')) {
  /**
  * return admin menu sub options
  */
  function getSubOptions($parent_id)
  {
    return AdminMenu::select('id','title','ask_list_type','action_id')->where('parent',$parent_id)->orderBy('rank','asc')->get()->toArray();
  }
}

if (!function_exists('isChecked')) {
  /**
  * return if option is checked
  */
  function isChecked($group_id,$menu_id)
  {
    if($menu_id!=null){
      $result=AdminGroupPermissions::where([['group_id','=',$group_id],['menu_id','=',$menu_id]])->first();
      if($result!=null){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
}

if (!function_exists('getMenuPermissinListingType')) {
  /**
  * return if option is checked
  */
  function getMenuPermissinListingType($group_id,$menu_id)
  {
    $result = AdminGroupPermissionType::where(['group_id'=>$group_id,'menu_id'=>$menu_id])->first();
		if($result!=null){
			return $result['list_type'];
		}
  }
}

if (!function_exists('getMyPermittedMenuItems')) {
  /**
  * return logged in users menu items
  */
  function getMyPermittedMenuItems()
  {
    $loggedInUser=Auth::user();
    return AdminGroupPermissions::select(['menu_id'])->where('group_id',$loggedInUser->permission_group_id);
  }
}

if (!function_exists('generateStaffMenu')) {
  /**
  * return html for side menu
  */
  function generateStaffMenu()
  {
    $loggedInUser=Auth::user();
    $groupId=$loggedInUser->permission_group_id;
    $subQueryPermitted=getMyPermittedMenuItems();
    $html='';
    $results=AdminMenu::select([
      'id',
      'title',
      'route',
      'controller_id',
      'action_id',
      'icon',
    ])
    ->where([['parent',0],['show_in_menu',1]])
    ->whereIn('id',$subQueryPermitted)
    ->orderBy('rank','asc')
    ->get()->toArray();
    if($results!=null){
      foreach($results as $result){
        $html.=generateSubMenu($result,$groupId,$subQueryPermitted);
      }
    }

    return $html;


  }
}

if (!function_exists('generateSubMenu')) {
  /**
  * return html for sub menu
  */
  function generateSubMenu($result,$groupId,$subQueryPermitted)
  {
    $html='';
		$subOptions = getSubMenuOptions($groupId,$result['id'],$subQueryPermitted);
		$isActive=false;
		$activeIdz = isActive();
		if(in_array($result['id'],$activeIdz))$isActive=true;
		if($subOptions!=null){
			$html.='<li class="menu-item  menu-item-submenu'.($isActive==true ? ' menu-item-open' : '').'" aria-haspopup="true" data-menu-toggle="hover">';
			$html.='	<a href="javascript:;" class="menu-link menu-toggle'.($isActive==true ? ' menu-item-active' : '').'">';
			$html.='		<span class="menu-icon"><i class="menu-icon text-white '.$result['icon'].'"></i></span>';
			$html.='		<span class="menu-text text-white">';
			$html.='		 '.$result['title'].'';
			$html.='		</span>';
			$html.='		<i class="menu-arrow text-white"></i>';
			$html.='	</a>';
			$html.='	<div class="menu-submenu">';
			$html.='	  <i class="menu-arrow text-white"></i>';
			$html.='	  <ul class="menu-subnav">';
			foreach($subOptions as $subOption){
				$html.=generateSubMenu($subOption,$groupId,$subQueryPermitted);
			}
			$html.='	  </ul>';
			$html.='	</div>';
			$html.='</li>';
		}else{
			$html.='<li class="menu-item'.($isActive==true ? ' menu-item-active' : '').'"  aria-haspopup="true">';
			$html.='	<a href="'.url('/'.$result['route']).'" class="menu-link">';
      if($result['icon']!=null && $result['icon']!=''){
        $html.='		<span class="menu-icon"><i class="menu-icon text-white '.$result['icon'].'"></i></span>';
      }else{
        $html.='		<i class="menu-bullet text-white menu-bullet-dot"><span></span></i>';
      }
			$html.='		<span class="menu-text text-white">'.$result['title'].'</span>';
			$html.='	</a>';
			$html.='</li>';
		}
		return $html;
  }
}

if (!function_exists('getSubMenuOptions')) {
  /**
  * return html for sub menu options
  */
  function getSubMenuOptions($groupId,$parent_id,$subQueryPermitted)
  {
    return $results=AdminMenu::select([
			'id',
			'title',
			'route',
			'controller_id',
			'action_id',
			'icon',
		])
		->where([['parent',$parent_id],['show_in_menu',1]])
		->whereIn('id',$subQueryPermitted)
		->orderBy('rank','asc')
		->get()->toArray();
  }
}

if (!function_exists('isActive')) {
  /**
  * return active status
  */
  function isActive()
  {
    $currentRequest=getCurrentControllerAndAction();
    $controllerId=$currentRequest['controller'];

		$result=AdminMenu::select(['id','parent'])->where(['controller_id'=>$controllerId])->first();
		if($result!=null){
			$idz[]=$result['id'];
			return getMenuParentIdz($result['parent'],$idz);
		}
		return [0];
  }
}

if (!function_exists('getMenuParentIdz')) {
  /**
  * return active status
  */
  function getMenuParentIdz($parent_id,$idz)
  {
    $result=AdminMenu::select(['id','parent'])->where('id',$parent_id)->first();
		if($result!=null){
			$idz[]=$result['id'];
			return getMenuParentIdz($result['parent'],$idz);
		}else{
			return $idz;
		}
  }
}

if (!function_exists('checkActionAllowed')) {
  /**
  * return active status
  */
  function checkActionAllowed($action_id,$controller_id=null)
  {
    if($controller_id==null){
			$currentRequest=getCurrentControllerAndAction();
	    $controller_id=$currentRequest['controller'];
		}
		$menu=AdminMenu::where(['controller_id'=>$controller_id,'action_id'=>$action_id])->first();
		if($menu!=null){
			$group_id = Auth::user()->permission_group_id;
			$result=AdminGroupPermissions::where(['group_id'=>$group_id,'menu_id'=>$menu->id])->first();
			if($result==null){
				return false;
				exit;
			}
		}else{
			return false;
			exit;
		}
		return true;
  }
}

if (!function_exists('checkPagePermission')) {
  /**
  * return active status
  */
  function checkPagePermission()
  {
    $controller=Yii::$app->controller;
		$loggedInUser=Yii::$app->user->identity;
		$groupId=$loggedInUser->groupId;
		$menu=AdminMenu::find()->where(['controller_id'=>$controller->id,'action_id'=>$controller->action->id])->one();
		if($menu!=null){
			if($menu->param1!=null && $menu->value1!=null){
				if(Yii::$app->request->get($menu->param1) && Yii::$app->request->get($menu->param1)==$menu->value1){
					$result=AdminGroupPermissions::find()->where(['group_id'=>$groupId,'menu_id'=>$menu->id,'is_allowed'=>1])->one();
					if($result==null){
						return $controller->redirect(['site/not-allowed']);
					}
				}else{
					return $controller->redirect(['site/not-allowed']);
				}
			}else{
				$result=AdminGroupPermissions::find()->where(['group_id'=>$groupId,'menu_id'=>$menu->id,'is_allowed'=>1])->one();
				if($result==null){
					return $controller->redirect(['site/not-allowed']);
				}
			}
		}else{
			return $controller->redirect(['site/not-allowed']);
		}
  }
}


if (!function_exists('saveModuleManagerField')) {
  /**
  * save module managers
  */
  function saveModuleManagerField($model,$request)
  {
		$model->manager_id=$request->manager_id;

		if($model->manager_id!=null){
			ModuleManager::where([
				'module_type'=>$model->moduleTypeId,
				'module_id'=>$model->id
			])
			->whereNotIn('staff_id',$model->manager_id)->delete();

			foreach($model->manager_id as $key=>$val){
				if($val!=null && $val>0){
					$managerRow=ModuleManager::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'staff_id'=>$val])->first();
					if($managerRow==null){
						$managerRow=new ModuleManager;
						$managerRow->module_type=$model->moduleTypeId;
						$managerRow->module_id=$model->id;
						$managerRow->staff_id=$val;
						$managerRow->save();
					}
				}
			}
		}
    $managerRow=ModuleManager::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'staff_id'=>$model->created_by])->first();
    if($managerRow==null){
			$managerRow=new ModuleManager;
			$managerRow->module_type=$model->moduleTypeId;
			$managerRow->module_id=$model->id;
			$managerRow->staff_id=$model->created_by;
			$managerRow->save();
    }
	}
}

if (!function_exists('saveImportAssignedField')) {
  /**
  * save module managers from import
  */
  function saveImportAssignedField($model,$request)
  {
		$managerNames = explode(",",$request['assign_to']);
		$managerIds=[];
		foreach($managerNames as $key=>$val){
			$val = trim($val);
			$val = str_replace("  "," ",$val);
			$val = str_replace("  "," ",$val);
			// echo 'select * from `users` where `user_type` = 10 and LOWER(name)="'.strtolower($val).'" limit 1<hr />';
			$staff=User::where('user_type',10)->whereRaw('LOWER(name)=?',[strtolower($val)])->first();
			if($staff!=null){
				// echo "Found ".strtolower($val)."<br />";
				$managerIds[]=$staff->id;
			}
		}
		// echo '<pre>';print_r($managerIds);echo '</pre>';
		foreach($managerIds as $key=>$val){
			$managerRow=ModuleManager::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'staff_id'=>$val])->first();
			if($managerRow==null){
				$managerRow=new ModuleManager;
				$managerRow->module_type=$model->moduleTypeId;
				$managerRow->module_id=$model->id;
				$managerRow->staff_id=$val;
				$managerRow->save();
			}
		}
		// die("In import assigned");
	}
}

if (!function_exists('getSavedModuleManagerIdz')) {
  /**
  * return status array
  */
  function getSavedModuleManagerIdz($model)
  {
    return ModuleManager::select('staff_id')->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->get()->pluck('staff_id', 'staff_id')->toArray();
  }
}

if (!function_exists('getAssignModuleBtn')) {
  /**
  * assignment btns for kanban
  */
  function getAssignModuleBtn($btnsArr,$model)
  {
		$btnsArr[]=['icon'=>'<i class="fa fa-user-plus"></i>','url'=>'','title'=>__('common.assign_to'),'class'=>'load-modal','otherOpts'=>'data-url="'.url('assign-module/'.$model->moduleTypeId.'/'.$model->id).'" data-heading="'.__('staffmanager::staff.assign_module_to',['module'=>$model->title]).'"'];
	  return $btnsArr;
  }
}

if (!function_exists('getAssignedToImportInputFields')) {
  /**
  * map inputs for importing
  */
  function getAssignedToImportInputFields($selOptsHtml)
  {
    $tableHtml = '';

    $tableHtml.='   <tr>';
    $tableHtml.='     <td>'.__('staffmanager::staff.manager_id').'</td>';
    $tableHtml.='     <td><select name="input_map_field[static][assign_to]" class="form-control">'.$selOptsHtml.'</td>';
    $tableHtml.='   </tr>';

    return $tableHtml;
  }
}

if (!function_exists('getAssignedUsersSymbol')) {
  /**
  * Get assigned user for module
  */
  function getAssignedUsersSymbol($moduleTypeId,$moduleId,$size=25)
  {
		$html = '';
    $users = User::where('user_type',10)
		->whereIn('id',function($query) use($moduleTypeId,$moduleId){
			$query->select('staff_id')
	    ->from(with(new ModuleManager)->getTable())
	    ->where(['module_type'=>$moduleTypeId,'module_id'=>$moduleId]);
		})
		->get();
		$html = '<div class="p-2">';
		// $html.= '<hr />'.$moduleTypeId.' - '.$moduleId.'<hr />';
		$html.= generateAssignedUserHtml($users,$size,false);
		$html.= '</div>';
		return $html;
  }
}

if (!function_exists('getCalendarAssignedUsersSymbol')) {
  /**
  * Get assigned users for calendar task
  */
  function getCalendarAssignedUsersSymbol($taskId,$size=25)
  {
    $html = '';
		$users = User::where('user_type',10)
		->whereIn('id',function($query) use($taskId){
			$query->select('staff_id')
	    ->from(with(new ActionLogManager)->getTable())
	    ->where(['action_log_id'=>$taskId]);
		})
		->get();
		return generateAssignedUserHtml($users,$taskId,$size,false);
  }
}

if (!function_exists('generateAssignedUserHtml')) {
  /**
  * Generate html for assigned users
  */
	function generateAssignedUserHtml($users,$taskId,$size,$inline=true)
	{
		$html = '';
		if($users!=null){
			$html.='  <div class="'.($inline==true ? 'd-flex ' : '').'align-items-center">';
			$html.='  	<div class="d-flex flex-column mr-2">';
			$html.='  		<span class="font-weight-bold">';
			$html.='  			<strong>'.__('staffmanager::staff.manager_id').'</strong> ';
			$html.='  			<a href="javascript:;" class="btn btn-icon btn-xs btn-success load-modal" title="'.__('actionlog::calendar.assign_task_to').'" data-url="'.url('assign-task/'.$taskId).'" data-heading="'.__('actionlog::calendar.assign_task_to').'" data-mid="secondary-modal§">';
			$html.='  				<i class="fa fa-plus icon-sm"></i>';
			$html.='  			</a>';
			$html.='  		</span>';
			$html.='  	</div>';
			$html.='  	<div class="symbol-group symbol-hover mt-1 mb-3">';
			foreach($users as $user){
				$memeberImage = $user->image;
				if($memeberImage!=''){
					$memeberImage = getStorageFileUrl('images/'.$memeberImage);
				}else{
					$memeberImage = asset('assets/media/users/blank.png');
				}
		    $html.='    <div class="symbol symbol-'.$size.' symbol-circle" data-toggle="tooltip" data-title="'.$user->name.'">';
		    $html.='      <img alt="Pic" src="'.$memeberImage.'"/>';
		    $html.='    </div>';
			}
	    $html.='  	</div>';
	    $html.='  </div>';
		}
		return $html;
	}
}

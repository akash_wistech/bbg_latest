<?php
namespace NaeemAwan\StaffManager\View\Components;

use Illuminate\View\Component;

class ModuleManagerField extends Component
{
  /**
  * model.
  *
  * @var string
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $model = $this->model;

    if($model->id!=null){
      $model->manager_id = getSavedModuleManagerIdz($model);
    }

    return view('staffmanager::components.module-manager-field',compact('model'));
  }
}

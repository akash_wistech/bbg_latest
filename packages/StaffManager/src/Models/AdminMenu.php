<?php

namespace NaeemAwan\StaffManager\Models;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'admin_menu';

  protected $fillable = [
    'parent',
    'icon',
    'title',
    'controller_id',
    'action_id',
    'show_in_menu',
    'rank'
  ];
}

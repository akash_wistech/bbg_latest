<?php

namespace NaeemAwan\StaffManager\Models;
use App\Models\ChildTables;

class ModuleManager extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_manager';

  protected $fillable = [
    'module_type',
    'module_id',
    'staff_id',
  ];
}

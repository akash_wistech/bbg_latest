<?php

namespace NaeemAwan\StaffManager\Models;
use App\Models\ChildTables;

class AdminGroupPermissionType extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'admin_group_permission_type';

  protected $fillable = [
    'group_id',
    'menu_id',
    'controller_id',
    'list_type',
  ];

  /**
  * Get the event type.
  */
  public function permissionGroup()
  {
    return $this->belongsTo(AdminGroup::class,'group_id');
  }

  /**
  * Get the event type.
  */
  public function adminMenu()
  {
    return $this->belongsTo(AdminMenu::class,'menu_id');
  }
}

<?php

namespace NaeemAwan\StaffManager\Models;
use App\Models\FullTables;

class AdminGroup extends FullTables
{
  public $list_type;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'admin_group';

  protected $fillable = [
    'title',
    'status'
  ];

  public function scopeEnabled($query)
  {
      return $query->where('status', 1);
  }
}

<?php

namespace NaeemAwan\StaffManager\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FullTables;

class Staff extends FullTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'users';

  protected $fillable = [
      'name',
      'email',
      'password',
      'image',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password',
      'remember_token',
  ];

  public $sortable = ['id', 'title', 'status'];

  public function scopeEnabled($query)
  {
      return $query->where('status', 1);
  }

  public function scopeStaff($query)
  {
      return $query->where('user_type', 10);
  }

  /**
  * Get the permission group.
  */
  public function permissionGroup()
  {
    return $this->belongsTo(AdminGroup::class,'permission_group_id');
  }
}

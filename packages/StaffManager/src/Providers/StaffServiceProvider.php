<?php

namespace NaeemAwan\StaffManager\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\StaffManager\View\Components\ModuleManagerField;

class StaffServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'staffmanager');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'staffmanager');
    // $this->publishes([
    //   __DIR__.'/../../config/staffmanager.php' => config_path('staffmanager.php'),
    // ],'staffmanager-config');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/staffmanager'),
    ],'staffmanager-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'staffmanager-migrations');

    $this->loadViewComponentsAs('staffmanager', [
        ModuleManagerField::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
    // $this->mergeConfigFrom(
    //   __DIR__.'/../../config/staffmanager.php',
    //   'staffmanager'
    // );
    // foreach (glob(app_path().'/StaffManager/src/Helpers/*.php') as $filename){
    //     require_once($filename);
    // }
  }
}

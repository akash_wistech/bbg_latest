<?php

namespace NaeemAwan\StaffManager\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Auth;
use NaeemAwan\StaffManager\Models\AdminMenu;
use NaeemAwan\StaffManager\Models\AdminGroup;
use NaeemAwan\StaffManager\Models\AdminGroupPermissions;
use NaeemAwan\StaffManager\Models\AdminGroupPermissionType;

class AdminGroupController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $pageSize = getSetting('grid_page_size');
    $results = AdminGroup::paginate($pageSize);

    return view('staffmanager::admin-group.index',compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model=new AdminGroup;
    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|unique:admin_group,title',
        'status' => 'required|numeric',
        'list_type' => 'nullable',
        'menu_opts' => 'nullable',
      ]);

      $item = new AdminGroup;
      $item->title = $request->title;
      $item->status = $request->status;
      if($item->save()){
        if($request->menu_opts!=null){
          foreach($request->menu_opts as $key=>$val){
            $chileItem = new AdminGroupPermissions;
            $chileItem->group_id=$item->id;
            $chileItem->menu_id=$val;
            $chileItem->save();
            $menuItem=AdminMenu::select(['controller_id','action_id'])->where(['id'=>$val])->first();
    				if($menuItem!=null && $menuItem['action_id']=='index'){
    					$permissionListType=new AdminGroupPermissionType;
    					$permissionListType->group_id=$item->id;
    					$permissionListType->menu_id=$val;
    					$permissionListType->controller_id=$menuItem['controller_id'];
    					if(isset($request->list_type[$val])){
    						$permissionListType->list_type=$request->list_type[$val];
    					}
    					$permissionListType->save();
    				}
          }
        }
      }
      return redirect('admin-group')->with('success', __('staffmanager::admin_group.saved'));
    }
    return view('staffmanager::admin-group.create',['model'=>$model]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update($id,Request $request)
  {
    $model  = AdminGroup::where(['id'=>$id])->first();
    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'title' => 'required|unique:admin_group,title,'.$id,
        'status' => 'required|numeric',
        'list_type' => 'nullable',
        'menu_opts' => 'nullable',
      ]);
      $item = AdminGroup::where(['id'=>$id])->first();
      $item->title = $request->title;
      $item->status = $request->status;
      if($item->update()){
        DB::table('admin_group_permissions')->where('group_id', $id)->delete();
        DB::table((new AdminGroupPermissionType)->getTable())->where('group_id', $id)->delete();
        if($request->menu_opts!=null){
          foreach($request->menu_opts as $key=>$val){
            $chileItem = new AdminGroupPermissions;
            $chileItem->group_id=$item->id;
            $chileItem->menu_id=$val;
            $chileItem->save();
            $menuItem=AdminMenu::select(['controller_id','action_id','ask_list_type'])->where(['id'=>$val])->first();
    				if($menuItem!=null && $menuItem['action_id']=='index' && $menuItem['ask_list_type']==1){
    					$permissionListType=new AdminGroupPermissionType;
    					$permissionListType->group_id=$item->id;
    					$permissionListType->menu_id=$val;
    					$permissionListType->controller_id=$menuItem['controller_id'];
    					if(isset($request->list_type[$val])){
    						$permissionListType->list_type=$request->list_type[$val];
    					}
    					$permissionListType->save();
    				}
          }
        }
      }
      return redirect('admin-group')->with('success', __('staffmanager::admingroup.saved'));
    }
    return view('staffmanager::admin-group.update',['model'=>$model]);
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\AdminGroup  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return AdminGroup::where('id', $id)->first();
  }
}

<?php

namespace NaeemAwan\StaffManager\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Storage;
use Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use NaeemAwan\StaffManager\Models\Staff;

class StaffController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $pageSize = getSetting('grid_page_size');
    $results = Staff::staff()->paginate($pageSize);

    return view('staffmanager::staff.index',compact('results'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model=new Staff;
    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'permission_group_id' => 'required|numeric',
        'jobrole_id' => 'required|numeric',
        'email' => 'required|email|unique:users,email',
        'fullname' => 'required|string',
        'phone' => 'nullable|numeric',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'password' => 'nullable|string|min:6|max:12',
        'status' => 'required|numeric',
      ]);
      $user = new Staff;
      $user->user_type = 10;
      $user->permission_group_id = $request->permission_group_id;
      $user->jobrole_id = $request->jobrole_id;
      if($request->password){
        $user->password = Hash::make($request->password);
      }
      $user->name = $request->fullname;
      $user->email = $request->email;
      $user->phone = $request->phone;
      $user->status = $request->status;
      if($request->hasFile('image')){
        $fileName = uploadImage($request,'image','images');
        $user->image = $fileName;
      }
      $user->save();

      return redirect('staff')->with('success', __('staffmanager::staff.saved'));
    }
    return view('staffmanager::staff.create',['model'=>$model]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update($id,Request $request)
  {
    $model  = Staff::where(['id'=>$id])->first();
    if($request->isMethod('post')){
      $validatedData = $request->validate([
        'permission_group_id' => 'required|numeric',
        'jobrole_id' => 'required|numeric',
        'email' => 'required|email|unique:users,email,'.$id,
        'fullname' => 'required|string',
        'phone' => 'nullable|numeric',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'password' => 'nullable|string|min:6|max:12',
        'status' => 'required|numeric',
      ]);
      $user = Staff::where(['id'=>$id])->first();

      $user->permission_group_id = $request->permission_group_id;
      $user->jobrole_id = $request->jobrole_id;
      if($request->password){
        $user->password = Hash::make($request->password);
      }
      $user->name = $request->fullname;
      $user->phone = $request->phone;
      $user->email = $request->email;
      $user->status = $request->status;
      if($request->hasFile('image')){
        $oldFile = $user->image;
        $fileName = uploadImage($request,'image','images');
        $user->image = $fileName;

        if($oldFile!=''){
          Storage::disk('local')->delete('images/'.$oldFile);
        }
      }
      $user->update();

      return redirect('staff')->with('success', __('staffmanager::staff.saved'));
    }
    return view('staffmanager::staff.update',['model'=>$model]);
  }

  /**
  * Assign a module to staff
  *
  * @return \Illuminate\Http\Response
  */
  public function assignModule(Request $request,$module,$id)
  {
    $modelClass = getModelClass()[$module];
    $model = $modelClass::where('id',$id)->first();
    if($model!=null){
      if($request->isMethod('post')){
        $validatedData = $request->validate([
          // 'permission_group_id' => 'required|numeric',
        ]);
        saveModuleManagerField($model,$request);
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('staffmanager::staff.assignment_successfully'),]]);
        exit;
      }
      $model->manager_id = getSavedModuleManagerIdz($model);
      return view('staffmanager::staff.assign_module', [
        'model'=>$model,
      ]);
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('common.notfound'),]]);
    }
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\PredefinedList  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return PredefinedList::where('id', $id)->first();
  }

  /**
  * Shows & updates update profile page
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function updateProfile(Request $request)
  {
    $id = Auth::user()->id;
    $model  = Staff::where(['id'=>$id])->first();
    if($request->isMethod('post')){

      $validatedData = $request->validate([
        'email' => 'required|email|unique:users,email,'.$id,
        'fullname' => 'required|string',
        'phone' => 'nullable|numeric',
        'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'signature_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
      $user = Staff::where(['id'=>$id])->first();
      $user->name = $request->fullname;
      $user->phone = $request->phone;
      $user->email = $request->email;
      if($request->hasFile('image')){
        $oldFile = $user->image;
        $fileName = uploadImageInpublic($request,'image','images');
        $user->image = $fileName;
        if($oldFile!=''){
          Storage::disk('local')->delete('images/'.$oldFile);
        }
      }
      if($request->hasFile('signature_image')){
        $oldSigFile = $user->signature_image;
        $sigFileName = uploadImageInpublic($request,'signature_image','images');
        $user->signature_image = $sigFileName;

        if($oldSigFile!=''){
          Storage::disk('local')->delete('images/'.$oldSigFile);
        }
      }
      $user->update();
      return redirect('update-profile')->with('success', __('staffmanager::staff.profile_updated'));
    }
    return view('staffmanager::staff.update_profile',compact('model'));
  }

  /**
  * Shows & updates password
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function changePassword(Request $request)
  {
    $id = Auth::user()->id;
    $model  = Staff::where(['id'=>$id])->first();
    if($request->isMethod('post')){
      $validatedData = $request->validate([
      'old_password' => 'required|string|min:6|max:12',
      'new_password' => 'required|string|min:6|max:12|confirmed',
      'new_password_confirmation' => 'required|string|min:6|max:12',
      ]);
      if (!Hash::check($request->old_password, $model->password)) {
        return redirect('change-password')->withErrors(['old_password' => 'Old Password does not match']);
      }
      $user = Staff::where(['id'=>$id])->first();
      $user->password = Hash::make($request->new_password);
      $user->update();

      return redirect('change-password')->with('success', __('staffmanager::staff.password_updated'));
    }
    return view('staffmanager::staff.change_password',compact('model'));
  }

  /**
  * Saves datatable column visibility
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function saveDataTableColumns(Request $request)
  {
    $id = Auth::user()->id;
    $model  = Staff::where(['id'=>$id])->first();
    if($request->isMethod('post')){
      $colsArr = $request->cols_arr;
      if($model->dt_col_visibility==null){
        $data[$request->module_type]=$colsArr;
      }else{
        $data = json_decode($model->dt_col_visibility,true);
        $data[$request->module_type] = $colsArr;
        // if(isset($dt_col_visibility[$request->module_type])){
        //
        // }else{
        //
        // }
        // foreach($dt_col_visibility as $key=>$val){
        //   if($key==$request->module_type){
        //     $data[$request->module_type]=$colsArr;
        //   }else{
        //     $data[$key] = $val;
        //   }
        // }
      }
      $model->dt_col_visibility = json_encode($data);
      $model->update();
      return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('staffmanager::staff.info_saved')]]);
      exit;
    }
  }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();

            $table->string('reference_no',100)->nullable();
            $table->string('type')->nullable()->default(null);
            $table->string('country_id')->nullable()->default(null);
            $table->string('city_id')->nullable()->default(null);
            $table->text('address')->nullable()->default(null);
            $table->string('title')->nullable()->default(null);
            $table->string('venue')->nullable()->default(null);
            $table->string('tba')->nullable()->default(null);
            $table->date('registration_start')->nullable()->default(null);
            $table->date('registration_end')->nullable()->default(null);
            $table->date('cancellation_tillDate')->nullable()->default(null);
            $table->date('event_startDate')->nullable()->default(null);
            $table->date('event_endDate')->nullable()->default(null);
            $table->string('event_startTime')->nullable()->default(null);
            $table->string('event_endTime')->nullable()->default(null);
            $table->text('short_description')->nullable()->default(null);
            $table->string('group_size')->nullable()->default(null);
            $table->string('image')->nullable()->default(null);
            $table->string('fb_gallery')->nullable()->default(null);
            $table->string('condition')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);

            $table->string('tax_id')->nullable()->default(0);
            $table->string('member_fee')->nullable()->default(null);
            $table->string('nonmember_fee')->nullable()->default(null);
            $table->string('committee_fee')->nullable()->default(null);
            $table->string('honourary_fee')->nullable()->default(null);
            $table->string('sponsor_fee')->nullable()->default(null);
            $table->string('focus_chair_fee')->nullable()->default(null);
            $table->integer('currency')->nullable()->default(null);
            $table->integer('display_price')->nullable()->default(null);
            $table->integer('sticky')->nullable()->default(null);
            $table->integer('userNote')->nullable()->default(null);
            $table->integer('status')->nullable()->default(null);
            $table->integer('quizNight')->nullable()->default(null);
            $table->string('active')->nullable()->default(null);
            $table->string('expiry_day')->nullable()->default(null);
            $table->string('longitude')->nullable()->default(null);
            $table->string('latitude')->nullable()->default(null);
            $table->integer('special_events')->nullable()->default(null);
            $table->string('diet_option')->nullable()->default(null);
            $table->text('diet_option_description')->nullable()->default(null);
            $table->text('other_diet_option_description')->nullable()->default(null);
            $table->string('members_only')->nullable()->default(null);
            $table->string('map_address')->nullable()->default(null);
            $table->string('info_only')->nullable()->default(null);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->index('status');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

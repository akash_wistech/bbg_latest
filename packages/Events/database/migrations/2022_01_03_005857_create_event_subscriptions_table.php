<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->enum('user_type', ['member','guest'])->nullable();
            $table->integer('event_id')->nullable();
            $table->string('userNote')->nullable();
            $table->string('designation')->nullable();
            $table->integer('attended')->nullable();
            $table->string('fee_paid')->nullable();
            $table->integer('subtotal')->nullable();
            $table->integer('tax')->nullable();
            $table->enum('payment_status', ['paid','pending'])->nullable();
            $table->enum('status', ['approved','pending','cancelled','deleted'])->nullable();
            $table->enum('cancel_request', ['0','1'])->nullable();
            $table->integer('gen_invoice')->nullable();
            $table->integer('focGuest')->nullable();
            $table->enum('user_type_relation', ['member','guest','committee','focus-chair','platinum-sponsor','speaker','speakers-guest','VIP','business-associate','honourary','foc-multiplier','foc-other'])->default('member')->nullable();
            $table->string('paymentMethod')->nullable();
            $table->string('session_key')->nullable();
            $table->integer('diet_option')->nullable();
            $table->string('diet_option_description')->nullable();
            $table->text('other_diet_option_description')->nullable();
            $table->integer('walkin')->nullable();
            $table->integer('vaccinated')->nullable();
            $table->integer('refered_by')->nullable();
            $table->integer('registered_by')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_subscriptions');
    }
}

<?php

use Illuminate\Support\Facades\Route;
use Wisdom\Event\Http\Controllers\EventController;



Route::group(['middleware' => ['web','auth']], function () {
	include('event.php');
});

Route::group(['middleware' => ['web']], function () {
	include('EventsFrontEnd.php');
Route::match(['get', 'post'],'/event/get-guest-form/', [EventController::class,'GetGuestForm']);
});



<?php
use Illuminate\Support\Facades\Route;
use Wisdom\Event\Http\Controllers\EventController;
use Wisdom\Event\Http\Controllers\EventInvoiceController;


Route::match(['get', 'post'],'/event/create', [EventController::class,'create']);
Route::get('event', [EventController::class,'index']);
	// Route::match(['get','post'],'event/datatable',[EventController::class, 'datatable'])->name('event.datatable');
Route::match(['get','post'],'event/update/{id}',[EventController::class, 'update'])->name('event.update');
Route::match(['get','post'],'event/delete/{id}',[EventController::class, 'delete'])->name('event.delete');
Route::match(['get','post'],'event/view/{id}',[EventController::class, 'show'])->name('event.view');
Route::get('/event/data-table-data', [EventController::class,'datatable']);
Route::get('/event-member/data-table-data/{id}', [EventController::class,'EventSubscribeGrid']);

Route::match(['get','post'],'event/event-subscribe/update/{id}',[EventController::class, 'EventSubscribeUpdate']);


Route::match(['get', 'post'],'/event/event-subscribe/{id}', [EventController::class,'EventSubscribe']);
Route::match(['get', 'post'],'/event/make-badge/{id}', [EventController::class,'makeBadge']);
Route::match(['get', 'post'],'/event/event-member-index/{id}', [EventController::class,'EventMemberIndex']);



Route::match(['get', 'post'],'/event/generate-invoice', [EventInvoiceController::class,'generateInvoice']);


Route::match(['get', 'post'],'/event/check-email-duplication', [EventController::class,'CheckEmailDuplication']);

Route::match(['get', 'post'],'/event/changestatus/{id}', [EventController::class,'changestatus']);


// New Route

Route::match(['get', 'post'],'/event/get-event-registeration-type/', [EventController::class,'getEventRegisterationType']);
Route::match(['get', 'post'],'/event/get-guest-form/', [EventController::class,'GetGuestForm']);
Route::match(['get', 'post'],'/event/get-company-wiht-members/', [EventController::class,'getCompanyWithMembers']);








// 3/19/2022

Route::match(['get', 'post'],'/event/event-subscribe/{id}', [EventController::class,'EventSubscribe']);


// why
// Route::match(['get', 'post'],'/event/get-members/', [EventController::class,'getMemberOnCompanyId']);








	// Route::match(['get','post'],'event/send-invoice/{member}',[EventController::class, 'SendInvoice'])->name('event.delete');

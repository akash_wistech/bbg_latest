

<?php
use Illuminate\Support\Facades\Route;
use Wisdom\Event\Http\Controllers\FrontendControllers\EventController;


Route::get('event-fend', [EventController::class, 'EventsIndex'])->name('event-fend');
Route::match(['get', 'post'], '/events/event-details/{id}', [EventController::class,'EventsDetails']);

Route::match(['get', 'post'], '/events/sync-to-calendar/{id}', [EventController::class,'SyncToCalendar']);



// 3/21/2020

Route::match(['get', 'post'], '/events/event-registeration/{id}/{s}', [EventController::class,'EventRegisteration']);

@extends('frontend.app')
@section('content')
{!! Form::open(array('url' => 'news-fend', 'method' => 'get', 'id' => 'member-searcher','enableClientScript' => false)) !!}
<section class="MainArea">
	<div class="container">


		<div class="row ">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea ">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft ">

					<div class="row">
						<div class="col-2"></div>
						<div class="col-md-8 col-md-offset-2 loginFormstyle">
							<div class="Heading text-center">
								<h3>Add Event to Calendar</h3>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="SiteText">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											{{ Form::text('email', '', ['class'=>'form-control','placeholder'=>'Enter Your Email Address']) }}
											{{ Form::hidden('event_id', $event_id, ['class'=>'form-control']) }}
										</div>
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
											{!! Form::submit('Add to Calendar', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o']) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-2"></div>
					</div>

				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea ">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>


{!! Form::close() !!}
@endsection
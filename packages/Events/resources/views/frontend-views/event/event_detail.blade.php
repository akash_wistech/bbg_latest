@extends('frontend.app')
@section('content')
<?php
// dd($event);


$eventMemberFee = getEventFeesValue($event->id, 'member');
$eventNonmemberFee = getEventFeesValue($event->id, 'guest');

?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">

                    <?php
                    if (isset($event) && $event <> null) {
                        $no_image = asset('assets/images/dummy-image-1.jpg');
                        // echo $event->id; die();

                        $title = $event->title;
                        // $share_url = url('/events/event-details?id='.$event->id);

                        ?>
                        <div class="Heading">
                            <?php
                            if (Auth::check()) {?>
                                <a target="_blank" href="{{url('/events/sync-to-calendar/'.$event->id)}}" class="pull-right Mybtn d-none" style="margin-top:10px;"><i class="fa fa-calendar mx-2" aria-hidden="true"></i>Add to my calendar</a>
                                <?php
                            }
                            ?>

                            <h3><?= "Event Details"; ?></h3>
                            <h4><?= $event->title; ?></h4>
                        </div>

                        <div class="EventDetail">
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                    <div id="multi-item-example1" class="carousel slide carousel-multi-item"
                                    data-ride="carousel">
                                    <div class="controls-top">
                                        <a class="btn-floating" href="#multi-item-example1" data-slide="prev"><i
                                            class="fa fa-chevron-left"></i></a>
                                            <a class="btn-floating" href="#multi-item-example1" data-slide="next"><i
                                                class="fa fa-chevron-right"></i></a>
                                            </div>

                                            <div class="carousel-inner" role="listbox">
                                                <?php
                                                $isFirst = true;
                                                if ($event->image <> null && $event->image <> "") {
                                                    // dd("eherer");
                                                    $isFirst = false;
                                                    ?>

                                                    <div class="carousel-item active">
                                                        <div class="col-md-12">
                                                            <div class="Slides text-center">
                                                                <img class="img-fluid"
                                                                src="<?= ($event->image) ? $event->image : $no_image; ?>"
                                                                alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }
                                                // dd("hello how are you");
                                                $i = 0;
                                                foreach ($event->eventImages as $row) {
                                                    // dd("here");

                                                    if($i == 0 && $isFirst){
                                                        // dd("i-o");
                                                        ?>
                                                        <div class="carousel-item active">
                                                            <div class="col-md-12">
                                                                <div class="Slides text-center">
                                                                    <img class="img-fluid"
                                                                    src="<?= ($row->image) ? $row->image : \Yii::$app->params['no_image']; ?>"
                                                                    alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php

                                                    }    else{
                                                        // dd("hhh");
                                                        ?>
                                                        <div class="carousel-item">
                                                            <div class="col-md-12">
                                                                <div class="Slides text-center">
                                                                    <img class="img-fluid" src="<?= $row->image ?>" alt="">
                                                                </div>
                                                            </div>
                                                            <!--/.Second slide-->
                                                        </div>
                                                    <?php }$i++;
                                                }
                                                // dd("here wear");
                                                ?>
                                                <!--/.Slides-->
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="EventDetailText">
                                            <p>
                                                <span class="BoldText">Event Date:</span> <?= formatDate($event->event_startDate); ?>
                                                <br>
                                                <span class="BoldText">Event Time:</span>
                                                <?php
                                                $event_start_time = $date = Date('g:i A', strtotime($event->event_startTime));
                                                $event_end_time = $date = Date('g:i A', strtotime($event->event_endTime));
                                                ?>

                                                <?= $event_start_time; ?>
                                                to <?= $event_end_time; ?><br>
                                                <span class="BoldText">Venue:</span> <?= $event->venue; ?> <br>
                                                <span class="BoldText">Location:</span> <?= $event->address; ?><br>
                                            </p>
                                            <span class="ViewMap">
                                                <a href="#event_map" class="smooth_scroll">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i> View Map
                                                </a>
                                            </span>
                                            <p>
                                                <br>
                                                <?php

                                                if (!$event->members_only) {
                                                    // dd("here");
                                                    if ($event->specialEventsFee <> null && $event->special_events == '1') {

                                                    } else { ?>
                                                    <?php $Event_fees = getEventFeesByUserTypesArr($event->id);
                                                     ?>
                                                     @if( isset($Event_fees['member']) && $Event_fees['member'] != '0' && $Event_fees['member'] != null  )
                                                        <span class="BoldText">Member Registrations:</span> <?= $eventMemberFee ?>
                                                        AED <br>
                                                        <span class="BoldText">Pre-members Fee:</span> <?= $eventNonmemberFee ?>
                                                        AED <br>
                                                    @else
                                                        <span class="BoldText">Members Fee :</span> FREE <br>
                                                    @endif

                                          <?php
                                                    }
                                                } else if ($event->members_only && Auth::check()) {

                                                    if ($event->specialEventsFee <> null && $event->special_events == '1') {
                                                        echo '<span class="BoldText">Special Events</span><br/>';
                                                        foreach ($event->specialEventsFee as $spEvent) {
                                                            ?>
                                                            <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                            :</span> <?= vatInclusive($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                        }
                                                    } else { ?>
                                                      <?php $Event_fees = getEventFeesByUserTypesArr($event->id); ?>
                                                       @if( isset($Event_fees['member']) && $Event_fees['member'] != '0' && $Event_fees['member'] != null  )
                                                        <span class="BoldText">Member Registrations:</span> <?= $eventMemberFee ?>
                                                        AED <br>
                                                        <span class="BoldText">Pre-members Fee:</span> <?= $eventNonmemberFee ?>
                                                        AED <br>
                                                        @else
                                                            <span class="BoldText">Members Fee :</span> FREE <br>
                                                        @endif
                                                        <?php
                                                    }
                                                } else if ($event->members_only && !Auth::check()) {

                                                    if ($event->specialEventsFee <> null  && $event->special_events == '1') {
                                                        echo '<span class="BoldText">Special Events</span><br/>';
                                                        foreach ($event->specialEventsFee as $spEvent) {
                                                            ?>
                                                            <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                            :</span> <?= vatInclusive($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                        }
                                                    } else { ?>
                                                        <span class="BoldText">Member Registrations:</span>
                                                        <?= $eventMemberFee ?>
                                                        AED <br>
                                                        <span class="BoldText">Pre-members Fee:</span> N/A AED <br>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </p>

                                        </div>

                            <?php  if ($event->registration_start <= date('Y-m-d') && $event->registration_end >= date('Y-m-d')) {
                                      if ($event->members_only==null && Auth::check() && ($event->info_only==null) ) {
                                          ?>
                                          <span class="RegisterEventbtn" style="background-color:green">
                                             <a href="{{url('events/event-registeration',['id'=>$event->id,'s'=>1])}}" class="members_only event_reg_btn"><i class="fa fa-map-marker mx-2" aria-hidden="true"></i>Register Now</a>
                                         </span>
                                <?php  } elseif ($event->members_only && Auth::check() && ($event->info_only==null) ) {  ?>
                                      <span class="RegisterEventbtn">
                                          <a href="{{url('events/event-registeration',['id'=>$event->id,'s'=>1])}}" class="members_only event_reg_btn"><i class="fa fa-map-marker mx-2" aria-hidden="true"></i>Register Now </a>
                                      </span>
                               <?php } elseif ($event->members_only==null && (!Auth::check()) && ($event->info_only==null) ) {  ?>
                                      <span class="RegisterEventbtn">
                                          <a href="{{url('events/event-registeration',['id'=>$event->id,'s'=>1])}}" class="members_only event_reg_btn"><i class="fa fa-map-marker mx-2" aria-hidden="true"></i>Register Now </a>
                                      </span>
                                      <p style="color:#D03C55; text-align: center;">Members must be
                                      logged in to receive member rate</p>
                                      <?php
                                  }
                                    } else if ($event->registration_start > date('Y-m-d')) {
                                        ?>
                                        <span class="label label-warning" style="padding: 10px; background-color: #f0ad4e; color: white;"><i class="fa fa-lock"></i> Not yet open</span>
                                        <?php
                                    } else if ($event->registration_end < date('Y-m-d')) {
                                        ?>
                                        <span class="label label-warning" style="padding: 10px; background-color: #f0ad4e; color: white;"><i class="fa fa-lock"></i> Registration Closed</span>
                                        <?php
                                    } else if ($event->event_endDate < date('Y-m-d')) {
                                        ?>
                                        <span class="label label-warning" style="padding: 10px; background-color: #f0ad4e;"><i class="fa fa-lock"></i> Registration Closed</span>
                                        <?php
                                    }
                                    ?>

                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <div class="DescriptionHeading">
                                        <p><?= $event->description; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>

                        <div id="event_map" class="col-md-8">
                            <div style="width: 100%">
                                <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="20"
                                marginwidth="20"
                                src="https://maps.google.com/maps?q=<?= urlencode($event->map_address); ?>&sensor=false&hl=en;z=14&amp;output=embed"></iframe>
                            </div>
                            <br/>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div id="share-buttons"></div>
                <div id="share"></div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                @include('frontend.blocks.RightSideBarWidget')
            </div>

        </div>
    </div>
</section>

@endsection


@push('css')

<link href="{{asset('theme/bbg/resources/miSlider/css/mislider.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('theme/bbg/resources/miSlider/css/mislider-cameo.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('theme/bbg/resources/miSlider/css/mislider-skin-cameo.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('theme/bbg/resources/miSlider/css/styles.css')}}" rel="stylesheet" type="text/css" />
<style>
 a.btn-floating {
     background: none !important;
 }
</style>
<style>
    .mis-slider {
        background-color: #cd2c47 !important;
    }

    .clientsbg {
        background: #ffff;
    }

    .mis-slider li img{
        max-height: 270px !important;
    }
</style>
@endpush

@push('js')
<script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
<script>
    var title = "{{$title}}";
    $("#share").jsSocials({
        url : $(this).data("url"),
        text: title,
        shareIn: "popup",
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
    });
</script>
@endpush

@extends('frontend.app')
@section('content')

<?php 
use App\Models\Contact;
use Wisdom\Event\Models\EventSubscription;

$nonmember_fee = 0;
$member_fee = 0;
$includeDiet = 0;
if ($event <> null) {
	$nonmember_fee = $event->nonmember_fee;
	$member_fee = $event->member_fee;
	$includeDiet = $event->diet_option;
}

$subTotal = 0;
$vat = 0;
$total = 0;
$invoices = [];

	//echo "<pre>"; print_r($event); echo "</pre>"; die();
?>


<section class="MainArea">
	<div class="container">
		<div class="row Eventform">
			<div class="stepwizard">
				<div class="stepwizard-row setup-panel">
					<div class="stepwizard-step">
						<a href="#step-1" type="button"
						<?php //dd($request['s']); ?>
						class="mybutn btn btn-primary btn-circle <?= ($request['s']== 1)  ? "" : " not-active" ?>">Step
					1</a>
				</div>
				<div class="stepwizard-step" id="step-1-btn">
					<a href="#step-2" type="button"
					class="mybutn btn btn-default btn-circle <?= ($request['s'] == 2) ? "" : " not-active" ?>"
					disabled="disabled">Step
				2</a>
			</div>
			<div class="stepwizard-step">
				<a href="#step-3" type="button"
				class="mybutn btn btn-default btn-circle <?= ($request['s'] == 3) ? "" : " not-active" ?>"
				disabled="disabled">Step
			3</a>
		</div>
	</div>
</div>
<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
	{!! Form::open(array('url' => 'news-fend', 'method' => 'post', 'id' => 'EventRegistrationForm','enableClientScript' => false, 'enableClientScript' => false)) !!}

	<input type="hidden" name="event_id" id="event_id" value="<?= $event->id; ?>">
	<input type="hidden" id="register_event_non_mem_fee" name="nonmember_fee" value="<?= $nonmember_fee; ?>">
	<input type="hidden" id="register_event_mem_fee" name="member_fee" value="<?= $member_fee; ?>">
	<input type="hidden" value="<?= $includeDiet;?>" id="IncludeDietOption" />
	<?php //dd("helo world"); ?>
	<div class="row setup-content" id="step-1">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<h3> Select Members:</h3>

			<div class="form-group">
				<table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
					<tbody>
						<?php
						$num = 1;
						$is_reg = false;

						if (!Auth::check()) {

							$contact = Contact::where(['id'=>Auth::user()->id])->first();
							// dd($contact->id);

							$is_reg = false;
							$isRegistered = EventSubscription::where([
								'event_id' => $event->id,
								'user_id' => $contact->id
							])->first();
							// dd($isRegistered);

							if ($isRegistered == null) {
								$is_reg = true;
							}

							if ($contact <> null) {
								// dd($contact);

								if ($contact->user_type_relation == "guest") {
									$member_fee = ($event <> null) ? $event->nonmember_fee : 0;
								}

								if ($contact->user_type_relation == "committee") {
									$member_fee = ($event <> null) ? $event->committee_fee : 0;
								}

								if ($contact->user_type_relation == "focus-chair") {
									$member_fee = ($event <> null) ? $event->focus_chair_fee : 0;
								}

								if ($contact->user_type_relation == "platinum-sponsor") {
									$member_fee = ($event <> null) ? $event->sponsor_fee : 0;
								}

								if ($contact->user_type_relation == "honourary") {
									$member_fee = ($event <> null) ? $event->honourary_fee : 0;
								}

								if ($contact->user_type_relation == "business-associate") {
									$member_fee = ($event <> null) ? $event->member_fee : 0;
								}
							}

							?>
							<tr>
								<td class="text-center"><?= $num; ?></td>
								<td class="text-center">
									<div class="checkbox">
										<label>
											<input <?= (is_array($isRegistered) && count($isRegistered) > 0) ? 'disabled checked' : "checked"; ?>
											type="checkbox" class="event_member_contacts"
											name="contact[]"
											value="<?= $contact->id; ?>">
											<span class="cr">
												<i class="cr-icon glyphicon glyphicon-ok"></i>
											</span>
										</label>
										<input type="hidden" name="contactfee[<?= $contact->id; ?>]"
										value="<?= $member_fee; ?>">
									</div>
								</td>
								<td>


									<?= $contact->first_name . " " . $contact->last_name . " (" . ucwords(($contact->group <> null) ? $contact->group->title : '') . ")"; ?>
                                           <!-- <span class="pull-right pricebox">
                                                    <?php /*= vatInclusive($member_fee); */?>
                                                </span>-->
                                            </td>
                                            <td>

                                            	<div class="checkbox">
                                            		<label>
                                            			Fully vaccinated against COVID-19
                                            			<input
                                            			type="checkbox" class="event_member_vaccinated"
                                            			name="contactVac[<?= $contact->id; ?>]"
                                            			value="1">
                                            			<span class="cr">
                                            				<i class="cr-icon glyphicon glyphicon-ok"></i>
                                            			</span>
                                            		</label>
                                            	</div>
                                            </td>
                                        </tr>
                                        <?php
                                        $num++;
                                    }else{
                                    	// dd("hello else of not guest ");
                                    }

                                    if ($contacts <> null && count($contacts) > 0) {
                                    	// dd("helo");

                                    	foreach ($contacts as $contact) {

                                    		$isRegistered = EventSubscriptions::where([
                                    			'refered_by' => Auth::user()->id,
                                    			'event_id' => $event->id,
                                    			'user_id' => $contact->id
                                    		]);

                                    		if ($isRegistered <> null) {
                                    			$is_reg = true;
                                    		}
                                    		$member_fee = ($event <> null) ? $event->member_fee : "";

                                    		if ($contact <> null) {

                                    			if ($contact->user_type_relation == "guest") {
                                    				$member_fee = ($event <> null) ? $event->nonmember_fee : 0;
                                    			}

                                    			if ($contact->user_type_relation == "committee") {
                                    				$member_fee = ($event <> null) ? $event->committee_fee : 0;
                                    			}

                                    			if ($contact->user_type_relation == "focus-chair") {
                                    				$member_fee = ($event <> null) ? $event->focus_chair_fee : 0;
                                    			}

                                    			if ($contact->user_type_relation == "platinum-sponsor") {
                                    				$member_fee = ($event <> null) ? $event->sponsor_fee : 0;
                                    			}

                                    			if ($contact->user_type_relation == "honourary") {
                                    				$member_fee = ($event <> null) ? $event->honourary_fee : 0;
                                    			}

                                    			if ($contact->user_type_relation == "business-associate") {
                                    				$member_fee = ($event <> null) ? $event->member_fee : 0;
                                    			}
                                    		}
                                    		?>
                                    		<tr>
                                    			<td class="text-center"><?= $num; ?></td>
                                    			<td class="text-center">
                                    				<div class="checkbox">
                                    					<label>
                                    						<input <?= ($isRegistered) ? 'disabled checked' : ""; ?>
                                    						type="checkbox" class="event_member_contacts"
                                    						name="contact[]"
                                    						value="<?= $contact->id; ?>">
                                    						<span class="cr">
                                    							<i class="cr-icon glyphicon glyphicon-ok"></i>
                                    						</span>
                                    					</label>
                                    					<input type="hidden" name="contactfee[<?= $contact->id; ?>]"
                                    					value="<?= $member_fee; ?>">
                                    				</div>
                                    			</td>
                                    			<td>
                                    				<?= $contact->first_name . " " . $contact->last_name; ?>
                                                <!--<span class="pull-right pricebox">
                                                    <?php /*= vatInclusive($member_fee); */?>
                                                </span>--><br/>
                                            </td>

                                            <td>

                                            	<div class="checkbox">
                                            		<label>
                                            			Fully vaccinated against COVID-19
                                            			<input
                                            			type="checkbox" class="event_member_vaccinated"
                                            			name="contactVac[<?= $contact->id; ?>]"
                                            			value="1">
                                            			<span class="cr">
                                            				<i class="cr-icon glyphicon glyphicon-ok"></i>
                                            			</span>
                                            		</label>
                                            	</div>
                                            </td>
                                        </tr>
                                        <?php
                                        $num++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <?php
                    if (!$event->members_only) {
                    	?>
                    	<h3>
                    		Do you want to invite a guest ?
                    		<span class="pull-right">
                    			<div class="checkbox">
                    				<label>
                    					<input type="checkbox" value="" id="inviteGuest">
                    					<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    					<span>Yes</span>
                    				</label>
                    			</div>
                    		</span>
                    		<span class="pull-right">
                    			<input type="number" min="0" name="" value="0" class="pricebox"
                    			id="input_guest_number"/>
                    		</span>
                    	</h3>
                    	<div id="section_invite_guest">
                    		<table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">
                    			<tbody>
                    				<tr>
                    					<td class="numberofguests">Number of Guests:</td>
                    					<td class="text-right">
                    						<span id="guestsNumbers">1</span>
                    						<span> x <span id="nonmember_fee"> <?= vatInclusive($nonmember_fee); ?></span> = <span
                    						class="nonmember_fee_total"> <?= vatInclusive($nonmember_fee); ?></span>
                    					</span>
                    				</td>
                    			</tr>
                    		</tbody>
                    	</table>
                    	<h3 class="text-right">Total: <span class="nonmember_fee_total">0</span></h3>
                    	<div class="Heading text-center">
                    		<h3>Guest Information</h3>
                    	</div>
                    	<div id="guestInformation"></div>
                    </div>
                    <?php
                }
                ?>
                <button class="Mybtn pull-left" id="EventRegistersBtn" type="button">
                	<i class="fa fa-money"></i> Proceed to Payment
                </button>

                {!! Form::close() !!}
            </div>
        </div>

    </div>
</div>
<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
</div>
</section>
<?php //echo HomeBottomWidget::widget(); ?>
@endsection
@push('css')
<style>
	.not-active {
		pointer-events: none;
		cursor: default;
	}
</style>
@endpush
@extends('frontend.app')
@section('content')

<?php
$no_image = asset('assets/images/dummy-image-1.jpg');
// dd($title);
?>

<section class="MainArea">
	<div class="container">
		<div class="row">
			<!-- search when type is past -->
			<div class="col-md-12 bbgevents_abudhabi text-right">

			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<div class="Heading">
						<h3><?= $title ?></h3>
					</div>

					<?php
					if (isset($events) && $events <> null && count($events) > 0) {
                        // echo "here"; die();
						foreach ($events as $event) {
							$share_url = url('events/event-details?id='.$event->id);
							// echo $share_url; die();

							?>
							<div class="Event1">
								<div class="row">
									<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
										<!--Second slide-->

										<img src="<?= ($event->image) ?  $event->image  : $no_image ?>"
										class="img-fluid" alt="">

									</div>
									<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
										<div class="EventText">
											<h3><i class="fa fa-calendar"
												aria-hidden="true"></i> <?= formatDate($event->event_startDate); ?>
												<h5>
													<i class="fa fa-map-marker"
													aria-hidden="true"></i> <?= $event->venue; ?>
												</h5>
											</h3>

											<h4><?= $event->title; ?></h4>
											<p><?= $event->short_description; ?></p>
											<?php //dd($event); ?>
											<div class="row">
												<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
													<div style="padding-bottom:30px; padding-top:0 !important;" class="EventDetail">

														<a href="<?= url('events/event-details',['id'=>$event->id]) ?>" class="Mybtn" type="button">Event Detail</a>


													</div>
												</div>

												@push('js')
												<script>
													id = "{{$event->id}}";
													share_url = "{{$share_url}}";
													title = "{{$event->title}}";
													$("#share"+id ).jsSocials({
														url : share_url,
														text: title,
														shareIn: "popup",
														showLabel: false,
														showCount: false,
														shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
													});
												</script>
												@endpush

												<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
													<div id="share<?= $event->id;?>"></div>
                                            <!--<span class="pull-right SocialLinksEvents">
                                                <a href="#">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                </a>
                                            </span>-->
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else{
            	?>
            	<div class="alert alert-danger">No events found!</div>
            	<?php
            }
            ?>
            <div class="col-md-8 text-center">
            	<?php echo $events->withQueryString()->links() ?>
            </div>
    </div>
    <br/>
    <br/>
</div>
            <!--<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <? /*= RightSidebarWidget::widget(); */ ?>
            </div>-->
        </div>
    </div>
</section>

@endsection

@push('css')
<style>
	.bbgevents_abudhabi a:hover {
		color: #428bca !important;
	}
	.paddingRightLeft {
		padding-bottom: 25px;
	}
</style>
@endpush

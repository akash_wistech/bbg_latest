@extends('frontend.app')
@section('content')
@push('css')
<link href="{{asset('assets/plugins/custom/jquery-autocomplete/css/jquery.autocomplete.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/custom_css_for_frontend.css')}}" rel="stylesheet" type="text/css" />

@endpush
@php
$usersList=[];
$companyName='';
if($model->company!=null){
$companyName =$model->company->title;
$usersList = getCompanyUsersList($model->company_id);
}
$invoiceRef = $model->reference_number;

$compInputOpts = [];
if($model->id!=null){
$compInputOpts['class']='form-control';
$compInputOpts['readonly']='readonly';
}else{
$compInputOpts['class']='autocomplete-company form-control';
$compInputOpts['data-ds']=url('suggestion/company');
$compInputOpts['data-mtype']=($model->module_type!='' ? $model->module_type : '');
$compInputOpts['data-fld']='company_id';
$compInputOpts['data-module']='contact';
$compInputOpts['data-oldsval']=$model->user_id;
$compInputOpts['data-sfld']='user_id';
}
@endphp
<section class="MainArea" >
    {!! Form::model($model, ['files' => true,'id'=>'EventRegistrationForm']) !!}
    <input type="hidden" id="submit_type" name="submit_type" value="0" />
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard px-0">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button"
                        <?php //dd($request['s']); ?>
                        class="mybutn btn btn-primary btn-circle <?= ($request['s']== 1)  ? "" : " not-active" ?>">Step
                    1</a>
                </div>
                <div class="stepwizard-step" id="step-1-btn">
                    <a href="#step-2" type="button"
                    class="mybutn btn btn-default btn-circle <?= ($request['s'] == 2) ? "" : " not-active" ?>"
                    disabled="disabled">Step
                2</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button"
                class="mybutn btn btn-default btn-circle <?= ($request['s'] == 3) ? "" : " not-active" ?>"
                disabled="disabled">Step
            3</a>
        </div>
    </div>
</div>

<div class="col-12 mt-5">


    <input type="hidden" id='event-id' name="event_id" value="{{ $id }}">
    <!-- STEP 1 SECTION -->
    <div class="row setup-content" id="step-1">
        <!-- COMPANY dropdown AND USER DROP ONLY SHOW WHEN MEMBER IS LOGGED  IN -->
        <?php   if (!Auth::check()) { ?>
          <!-- First Guest Form, It shows if member is not logged In. -->
          <div class=" px-4">
                <div class="card card-custom remove_guest_1  parent-card col-12 my-5" style="padding-left:70px;">
                    <div class="position-absolute " style="left:0px; top:0; bottom:0; width:70px; background-color:#EEEEEE;">
                        <h4 class="text-center font-weight" style="margin-top:6rem !important;">Guest</h4>
                    </div>
                    <div class="card-body  row">
                        <div class="col-11" style="padding-left:2.5rem !important; padding-top:2.5rem !important">
                            <div class="row"><div class="col-4">
                                <input class="form-control" name="guestinfo[100][100][user_id]" type="hidden" id="user-id" value="0">
                                <input class="form-control" placeholder="Name" name="guestinfo[100][100][name]" type="text" id="name" required="">
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <input class="form-control js-user-email" placeholder="Email" name="guestinfo[100][100][email]" type="text" required="">
                                </div>
                            </div>
                            <div class="col-4">
                                <input class="form-control" placeholder="mobile" name="guestinfo[100][100][mobile]" type="text" required="">
                            </div>
                            <div class="col-4">
                                <input class="form-control" placeholder="Company" name="guestinfo[100][100][company]" type="text" required="">
                            </div>
                            <div class="col-4">
                                <input class="form-control" placeholder="Designation" name="guestinfo[100][100][designation]" type="text" id="designation">
                            </div>
                            <div class="col-4 d-none">
                                <select class="form-control" id="user_type_relation" name="guestinfo[100][100][user_type_relation]" required="">
                                    <option value="guest" selected="selected">Pre Member</option>

                             </select>
                         </div>
                     </div>
                     <div class="row mt-7">
                        <div class="form-group col-2 d-none" style="padding-top: 1rem !important;padding-left: 0 !important;">
                            <div class="checkbox-inline col-12">
                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                    <input type="checkbox" name="guestinfo[100][100][focGuest]" value="1">
                                    <span></span>Foc Guest</label>
                                </div>
                            </div>
                            <div class="form-group col-3" style="padding-top: 1rem !important;padding-left: 0 !important;">
                                <div class="checkbox-inline col-12">
                                    <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                        <input class="diet-option-box" name="guestinfo[100][100][diet_option]" type="checkbox" value="1">
                                        <span></span>Diet Option</label>
                                    </div>
                                </div>
                                <div class="form-group col-6 " style="padding-top: 1rem !important;padding-left: 0 !important;">
                                    <div class="checkbox-inline col-12">
                                        <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input type="checkbox" name="guestinfo[100][100][vaccinated]" value="1">
                                            <span>
                                            </span>Fully vaccinated against COVID-19</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 diet-option-description-select" style="display: none;">
                                        <div class="form-group">
                                            <select class="form-control diet-select-options" name="guestinfo[100][100][diet_option_description]">
                                                <option value="" selected="selected">Select Dietry Option...</option>
                                                <option value="Standard">Standard</option>
                                                <option value="Vegetarian">Vegetarian</option>
                                                <option value="other">Other Dietary</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 other-diet-option-description" style="display: none;">
                                        <div class="form-group">
                                            <textarea class="form-control " placeholder="Write Description" rows="6" name="guestinfo[100][100][other_diet_option_description]" cols="50"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
            <?php
            }
             ?>

        <!--IF I add members, They will show here  -->
        <div class="members_here px-4">
          <?php
          if (Auth::check()) {?>
           <div class="dummy-member-box rounded" style="border: 2px dashed #E0E0E0; min-height:130px; background-color:#fcfcfc; text-align:center; padding-top:50px">
             <p style="margin: auto; ">Add Member by clicking Add Registration, it will show here.</p>
           </div>
        <?php  }
           ?>
        </div>

        <!--IF I add Guests, They will show here  -->
        <div id="guestInformation" class="px-4">
        </div>


          <!-- it shows if select members from my company by selecting from add registeration  -->
          <div class=" p-4 member-selection-my-company d-none ">
            <div class="bg-white">
            <h3 class="pl-4 mb-4" style="background: #1d355f; color:white;">Contact From My Company</h3>
          <div class="row mx-0">
          <div class="col-6">
              <div class="form-group">
                  {{ Form::label('mycompany',__('common.company'),['class'=>'required']) }}
                  {!! Form::text('mycompany','',[ 'id'=>'mycompany' ,'class'=>'form-control', 'readonly'=>'readonly']) !!}
              </div>
          </div>
          <div class="col-6">
              <div class="form-group">
                  {{ Form::label('mycompanyuser',__('common.user_id'),['class'=>'required']) }}
                  {!! Form::select('mycompanyuser',[], $model->mycompanyuser, [ 'id'=>'mycompanyuser','class' => 'form-control comeMembers']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>

        <!-- it shows if select members from other company by selecting from add registeration  -->
        <div class="member-selection-other-company d-none  p-4">
        <div class="bg-white">
              <h3 class="pl-4 mb-4" style="background: #1d355f; color:white;">Contact From Other Companies</h3>
            <div class="row mx-0">
            <div class="col-6">
                <div class="form-group">
                    {{ Form::label('company_id',__('common.company'),['class'=>'required']) }}
                    {!! Form::text('customer_name', $companyName,$compInputOpts) !!}
                    {{ Form::hidden('company_id', $model->company_id, ['id'=>'company_id','class'=>'form-control']) }}
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    {{ Form::label('user_id',__('common.user_id'),['class'=>'required']) }}
                    {!! Form::select('user_id',$usersList, $model->user_id, ['id'=>'user_id', 'class' => 'form-control comeMembers']) !!}
                </div>
            </div>
          </div>
        </div>
      </div>




      <!-- Add Registration -->
      <?php if (Auth::check()) {?>
      <div class="d-flex my-5 flex-row-reverse">
        <div class="card-header px-3 border-0 pt-5">
                <div class="card-toolbar">
                  <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="">
                    <button href="#" class="Mybtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Add Registration
                    </button>
                    <div class="dropdown-menu dropdown-menu-md dropdown-menu-right shadow-sm"  x-placement="top-end" style="border: none; width: 285px; border-radius: 7px;">
                      <ul class="navi navi-hover" style=" padding: 0;">
                        <?php
                        $userTypesList = EventRegisterationType();
                        if($userTypesList!=null){
                          foreach($userTypesList as $key=>$val){  ?>
                            <li class="navi-item my-2">
                              <a href="javascript:;" class="navi-link <?= (($key=='addguest') ? 'add_guest_2' : 'add-registeration-type') ?> " data-usertype="{{ $key }}" style="font-size: 15px;  color: black;">
                                <span class="navi-icon">
                                  <i class="flaticon2-drop"></i>
                                </span>
                                <span class="navi-text">{{ $val }}</span>
                              </a>
                            </li>
                          <?php }} ?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
         </div>
         <?php }else{ ?>
          <div class="">
              <div class="card-header border-0">
                      <div class="card-toolbar " style="float:right">
                        <a href="javascript:;" data-memberid="'.$member_id.'" class="btn btn-light-primary ml-2  font-weight-bolder navi-link  add_guest_2" data-usertype="guest"
                         style="padding:10px; width:90px; font-size:14px" >
                        Add Guest</a>
                      </div>
              </div>
          </div>
        <?php } ?>

<?php $Event_fees = getEventFeesByUserTypesArr($event->id); ?>

        <!-- Save and Proceed to Pay -->
        <div class=" row my-4">
          <div class="col-6">
            @if (Auth::check())
              <!-- <button type="submit" class="Mybtn pull-left" style="margin-right:10px;" onclick="$('#submit_type').val(0)"><i class="fa fa-money"></i> Save </button> -->
            @endif
            @if( isset($Event_fees['member']) && $Event_fees['member'] != '0' && $Event_fees['member'] != null  )
              <button type="submit" class="Mybtn pull-left" onclick="$('#submit_type').val(1)"><i class="fa fa-money"></i> Complete the online payment to confirm your registration</button>
              @else
              <button type="submit" class="Mybtn pull-left" onclick="$('#submit_type').val(1)"><i class="fa fa-money"></i> Finalise booking</button>
              @endif

          </div>
        </div>
        </div>
        <!-- Closing of step 1 -->

</div>
</div>
</div>
{!! Form::close() !!}
</section>
@endsection




@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/custom/jquery-autocomplete/js/jquery.autocomplete.js')}}"></script>
<script>

    $(document).ready(function() {
        var GuestCount=1;
        var memberCount=1;
        var eventId = $('#event-id').val();
        var frontend = 'frontend-form';
        var companyName = $('.autocomplete-company').val();
        var company_id = $("#company_id").val();


        var CurrentLoggedInUser= <?= ((\Auth::user()!=null)? \Auth::user()->id : 0)  ?>


        // Add guest button
        $("body").on("click",'.add_guest_2', function(){
          $('.member-selection-my-company').addClass('d-none');
          $('.member-selection-other-company').addClass('d-none');
            GuestCount++;
            guestType= 'guest';
            var frontend = 'frontend-form';
            var html = '';
            var eventId = $('#event-id').val();
            data = {eventId,GuestCount,guestType,frontend};
            $.ajax({
              url:"<?= url('event/get-guest-form')?>",
              type:"POST",
              data: {
                "_token": "<?= csrf_token() ?>",
                data:data,
            },
            success:function (data) {
               $('#guestInformation').append(data);
               }
           });
          });


        //It runs when we select member from user list
        $('body').on('change','.comeMembers', function(){
            memberid= $(this).val();
            getmemberDetials(memberid);

        });

        // Remove member
        $("body").on("click", '.remove_div', function(){
            $(this).parents('.tr_remove').remove();
            $("#primary_contact option[value='"+$(this).data("id")+"']").remove();
        });
        // Remove Guest
        $("body").on("click", '.remove_guest', function(){
            $(this).parents('.remove_guest_1').remove();
        });

        // Guest Diet First Option
        $('body').on('click', '.diet-option-box', function() {
            if($(this).prop("checked") == true){
              if ($(this).parents('.parent-card').find('.diet-select-options').val()=='other') {
                $(this).parents('.parent-card').find('.other-diet-option-description').show();
            }
            $(this).parents('.parent-card').find('.diet-option-description-select').show();
            }
          else if($(this).prop("checked") == false){
              $(this).parents('.parent-card').find('.diet-option-description-select').hide();
              $(this).parents('.parent-card').find('.other-diet-option-description').hide();
              }
        });

        // After checked diet_option , we select diet , this function will work
        $('body').on('change', '.diet-select-options', function() {
        if($(this).val()=='other'){
            $(this).parents('.parent-card').find('.other-diet-option-description').show();
        }else{
            $(this).parents('.parent-card').find('.other-diet-option-description').hide();
        }
        });

      $('body').on('click', '.diet-option-box-member', function() {
            if($(this).prop("checked") == true){
                $(this).parents('.tr_remove').find('.diet-option-description-select-member').show();
                $('#user_type_relation').addClass('mt-4');
            }
            else if($(this).prop("checked") == false){
                $(this).parents('.tr_remove').find('.diet-option-description-select-member').hide();
                $('#user_type_relation').removeClass('mt-4');
            }
          });



       // New Script
      //EventRegisterationType
      $("body").on("click",'.add-registeration-type', function(){
          GuestCount++;
          Registerationtype= $(this).data("usertype");
          data = {Registerationtype,CurrentLoggedInUser,memberCount,frontend};
          memberCount++;
          // eventId
          if (Registerationtype=='myself') {
          if (CurrentLoggedInUser!=0) {
            getmemberDetials(CurrentLoggedInUser);
          }
        }
        if (Registerationtype=='contactofmycompany') {
          $('.member-selection-my-company').removeClass('d-none');
          $('.member-selection-other-company').addClass('d-none');
          getCompanyWithMembers(CurrentLoggedInUser);
        }
        if (Registerationtype=='contactofothercompany') {
          $('.member-selection-my-company').addClass('d-none');
          $('.member-selection-other-company').removeClass('d-none');
        }
      });


      //Get Member EventsDetails
      function getmemberDetials(memberid){
          data = {member_id:memberid,memberCount,eventId,frontend};
          memberCount++;
          $.ajax({
              url:"<?= url('event/get-event-registeration-type')?>",
              type:"POST",
              data: {
                  "_token": "<?= csrf_token() ?>",
                  data: data,
              },
              success:function (data) {
                console.log(data);
                  $('.members_here').append(data.html);
                  if (data.status=='error') {
                    swal({
                           title: 'Error!',
                           text: data.msg,
                           type:data.status,
                           confirmButtonText: 'OK'
                         });
                  }
                  $('.dummy-member-box').addClass('d-none');
              }
          });
      }

      // Add Current user company name wit members
      function getCompanyWithMembers(memberid){
        data = {memberid};
        $.ajax({
            url:"<?= url('/event/get-company-wiht-members/')?>",
            type:"POST",
            data: {
                "_token": "<?= csrf_token() ?>",
                data: data,
            },
            success:function (data) {
              console.log(data['html']);
                $('#mycompany').val(data['companyName']);
                $('#mycompanyuser').html(data['html']);
            }
        });
      }
    });
</script>
@endpush

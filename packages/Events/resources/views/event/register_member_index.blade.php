<?php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/event/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];
$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('User Type'),'data'=>'user_type','name'=>'user_type'];
$dtColsArr[]=['title'=>__('Name'),'data'=>'name','name'=>'name'];
$dtColsArr[]=['title'=>__('Email'),'data'=>'email','name'=>'email'];
$dtColsArr[]=['title'=>__('Mobile'),'data'=>'phone','name'=>'phone'];
$dtColsArr[]=['title'=>__('Fee'),'data'=>'fee','name'=>'fee'];
$dtColsArr[]=['title'=>__('Payment Status'),'data'=>'payment_status','name'=>'payment_status'];
$dtColsArr[]=['title'=>__('Paid By'),'data'=>'paid_by','name'=>'paid_by'];
$dtColsArr[]=['title'=>__('Status'),'data'=>'status','name'=>'status'];
// $dtColsArr[]=['title'=>__('Vaccinated'),'data'=>'vaccinated','name'=>'vaccinated'];

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>


<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :showStats="false" :controller="'event'" :cbSelection="false" :exportMenu="true" :jsonUrl="'event-member/data-table-data/'.$id" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>

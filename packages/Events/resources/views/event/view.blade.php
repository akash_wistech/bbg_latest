@extends('layouts.app')
@section('title', __('Event'))
@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'event '=> __('Event'),
]])
@endsection
@section('content')

<?php


// dd($model->id);


$RegisterdMembers = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->count();
$MembersOnly = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->where('user_type','member')->count();
$guestonly = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->where('user_type','guest')->count();
$Fee_paid = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->sum('fee_paid');

$attended = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->where('attended',1)->count();
$not_attended = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->where('attended',0)->count();
$Cancelled = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->where('cancel_request','!=','0')->count();


$query = DB::table('payments')->whereNull('deleted_at');

$net_revenue=0;
$sub = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->select('id');
if ($sub) {
  $query2= \DB::table('module_invoice')->whereIn('module_id',$sub)->where('module_type','event-member-register')->select('invoice_id');
  if ($query2) {
    $payments= \DB::table('payment_items')->whereIn('module_id',$query2)->where('module_type','invoice')->select('payment_id');
    if ($payments) {
      $query->where(function($query) use($sub,$payments){
          $query->whereIn('id',$payments);
      });
      if ($query) {
        $net_revenue =  $query->sum('amount_received');
      }
    }
  }
}

$net_profit = $net_revenue- $model->expense;





 ?>

	<div class="container border rounded mt-4 py-5 px-0">
		<div class="d-flex px-0">
		<div class="col-3 border-right rounded bg-white py-4 px-6">
			<!--begin::Details-->
			<div class="d-flex mt-6">
				<!--begin: Pic-->
				<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
					<div class="symbol symbol-50 symbol-lg-90">
						<img src="{{ ($model->image<>null) ? $model->image : asset('images/default-placeholder.png') }}" alt="image" />
					</div>
					<div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
						<span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
					</div>
				</div>
				<!--end::Pic-->
				<!--begin::Info-->
				<div class="flex-grow-1">
					<!--begin::Title-->
					<div class="d-flex justify-content-between flex-wrap mt-1">
						<div class="d-flex mr-3">
							<p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->title }}</p>
							<?php if($model->status==1){  ?>
							<a href="#">
								<i class="flaticon2-correct text-success font-size-h5"></i>
							</a>
							<?php } ?>
						</div>
					</div>
					<!--end::Title-->
					<!--begin::Content-->
					<div class="d-flex flex-wrap justify-content-between mt-1">
						<div class="d-flex flex-column flex-grow-1 pr-8">
							<div class="d-flex flex-wrap ">

								<?php if ($model->venue) { ?>
								<p class="text-dark-50 text-hover-primary font-weight-bold">
                  <span class="text-muted font-weight-bold"><i class="flaticon2-placeholder mr-2 font-size-lg"></i>
                    {{ $model->venue }}</span></p>
								<?php } ?>
							</div>
						</div>
					</div>
					<!--end::Content-->
				</div>
				<!--end::Info-->
			</div>
			<!--end::Details-->
			<div class="pb-7 mt-4">
			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Group Size:</span>
				<a href="#" class="text-muted text-hover-primary">{{ $model->group_size }}</a>
			</div>
			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Type:</span>
				<a href="#" class="text-muted text-hover-primary">{{ getPredefinedListItemsArr(1617)[$model->type] }}</a>
			</div>
      <?php
      foreach(getUserTypeRelation() as $key=>$val){
        $model->fees[$key]=getEventFeeByGuestType($model->id,$key);
        if ($key=='member' || $key=='guest'){
      ?>
      <div class="d-flex align-items-center justify-content-between mb-2">
        <span class="font-weight-bold mr-2">{{ $val }}</span>
        <span class="text-muted">{{ (($model->fees[$key]!=null)?$model->fees[$key] : 'N/A') }}</span>
      </div>
    <?php
    }
    } ?>
			</div>

			<ul class="nav nav-tabs  mb-5  d-flex flex-column navi navi-hover navi-active navi-accent border-bottom-0">

			     <li class="navi-item my-1">
			        <a class="navi-link active" data-toggle="tab" href="#kt_tab_pane_1">
			            <span class="navi-icon pr-4"><i class="far fa-building"></i></span>
			            <span class="navi-text font-size-h6 ">Dashboard</span>
			        </a>
			    </li>
          <li class="navi-item my-1">
			        <a class="navi-link" data-toggle="tab" href="#kt_tab_pane_2">
                <span class="svg-icon pr-3"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Group.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <polygon points="0 0 24 0 24 24 0 24"/>
                      <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                      <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
                  </g>
                </svg><!--end::Svg Icon--></span>
			            <span class="navi-text font-size-h6 ">Registerd Members</span>
			        </a>
			    </li>
			    <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#kt_tab_pane_3">
			            <span class="navi-icon pr-4"><i class="fas fa-file-invoice-dollar"></i></span>
			            <span class="navi-text font-size-h6">Invoices</span>
			        </a>
			    </li>
			     <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#payments">
			            <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
			            <span class="navi-text font-size-h6">Payment</span>
			        </a>
			    </li>
          <li class="navi-item my-1">
             <a class="navi-link mx-0" data-toggle="tab" href="#tab-actions">
                 <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
                 <span class="navi-text font-size-h6">Actions</span>
             </a>
         </li>
			</ul>
			</div>


			<div class="tab-content col-9 rounded pr-0" id="myTabContent" >

						 <div class="tab-pane fade show active bg-white rounded" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_2" style="min-height:550px">

						    	<div class="card card-custom shadow-none">
								 <div class="card-header">
								  <div class="card-title">
								            <span class="card-icon">
								                <i class="far fa-object-group text-primary"></i>
								            </span>
								   <h3 class="card-label">
								    Dashboard

								   </h3>
								  </div>
								        <div class="card-toolbar">
								            <a href="{{ url('event/update/'.$model->id) }}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="" data-original-title="Update">
											<i class="text-dark-50 flaticon-edit"></i>
										    </a>
								        </div>
								 </div>
								 <div class="card-body">
									<!--begin::Content-->
                  <div class="pr-3 pb-4">
                    <h5>Short Description</h5>
                    <div class="font-weight-bold text-dark-50 py-5 py-lg-2 mr-5">
                        {!! $model->short_description !!}
                    </div>
                  </div>
                  <div class="">
                    <div id="chartdiv"></div>
                  </div>


                <div class="row">
                <div class="mt-6 col-6">
                  <h4>Registration Date</h4>
                  <div class="d-flex flex-wrap align-items-center py-2 col-9 px-0">
											<div class="d-flex align-items-center mr-10">
												<div class="mr-6">
													<div class="font-weight-bold mb-2">Start Date</div>
													<span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold">{{ date("d F,Y", strtotime($model->registration_start)) }}</span>
												</div>
												<div class="">
													<div class="font-weight-bold mb-2">Due Date</div>
													<span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold">{{ date("d F,Y", strtotime($model->registration_end)) }}</span>
												</div>
											</div>
									</div>
									<div class="flex-grow-1 flex-shrink-0 w-150px w-xl-300px mt-4 mt-sm-6 col-5 px-0">
										<span class="font-weight-bold">Registerd Members</span>
										<div class="progress progress-xs mt-2 mb-2">
											<div class="progress-bar bg-warning" role="progressbar" style="width:{{ $Registerd_members_percent.'%' }};" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="font-weight-bolder text-dark">{{ $Registerd_members }}</span>
									</div>
                </div>

                <div class="mt-6 col-6">
                  <h4>Event Date</h4>
                  <div class="d-flex flex-wrap align-items-center py-2 col-9 px-0">
                      <div class="d-flex align-items-center mr-10">
                        <div class="mr-6">
                          <div class="font-weight-bold mb-2">Start Date</div>
                          <span class="btn btn-sm btn-text btn-light-primary text-uppercase font-weight-bold">{{ date("d F,Y", strtotime($model->event_startDate)) }}</span>
                        </div>
                        <div class="">
                          <div class="font-weight-bold mb-2">Due Date</div>
                          <span class="btn btn-sm btn-text btn-light-danger text-uppercase font-weight-bold">{{ date("d F,Y", strtotime($model->event_endDate)) }}</span>
                        </div>
                      </div>
                  </div>
                </div>
              </div>

              <div class="row justify-content-center my-8 ">
                <div class="card card-custom bgi-no-repeat card-stretch gutter-b col-3 mr-2" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
											<!--begin::Body-->
											<div class="card-body">
												<span class="svg-icon svg-icon-2x svg-icon-info">
													<i class="flaticon-piggy-bank display-4 text-primary font-weight-bold"></i>
												</span>
												<span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 d-block">{{ 'AED '.number_format($net_profit)  }}</span>
												<span class="font-weight-bold text-muted font-size-lg">Profit</span>
											</div>
											<!--end::Body-->
										</div>


                    <div class="card card-custom bgi-no-repeat card-stretch gutter-b col-3 mx-2" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
                          <!--begin::Body-->
                          <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-info">
                            <i class="flaticon-confetti display-4 text-primary font-weight-bold"></i>
                            </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 d-block">{{ (($model->expense!=null) ? 'AED '.$model->expense : 'AED 0') }}</span>
                            <a href="{{ url('event/update/'.$model->id) }}" class="font-weight-bold text-muted font-size-lg">Expenses</a>
                          </div>
                          <!--end::Body-->
                        </div>

                        <div class="card card-custom bgi-no-repeat card-stretch gutter-b col-3 mx-2" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
                              <!--begin::Body-->
                              <div class="card-body">
                                <span class="svg-icon svg-icon-1x svg-icon-info">
                                  <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
                                  <i class="flaticon-pie-chart display-4 text-primary font-weight-bold"></i>
                                  <!--end::Svg Icon-->
                                </span>
                                <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0  d-block">{{ 'AED '.number_format($net_revenue)  }}</span>
                                <span class="font-weight-bold text-muted font-size-lg">Net</span>
                              </div>
                              <!--end::Body-->
                            </div>
                          </div>
									<!--end::Content-->
								  </div>
								</div>
							</div>





        <div class="tab-pane fade  bg-white rounded" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2" style="min-height:550px">
          <div class="card card-custom shadow-none border-0">
				 <div class="card-header">
				  <div class="card-title">
				   <h3 class="card-label">
             <span class="svg-icon pr-2"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Communication\Group.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                   <polygon points="0 0 24 0 24 24 0 24"/>
                   <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                   <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
               </g>
             </svg><!--end::Svg Icon--></span>
            Registerd Members</h3>

				  </div>
          <div class="card-toolbar">
          </div>
				 </div>
				 <div class="card-body p-0 shadow-none border-0">
             @include('event::event.register_member_index',['request'=>$request,'id'=>$model->id])
				 </div>
				</div>
    	</div>


		  <div class="tab-pane fade bg-white rounded" id="kt_tab_pane_3" role="tabpanel" aria-labelledby="kt_tab_pane_3" style="min-height:550px">
		  		<div class="card card-custom shadow-none">
				 <div class="card-header">
				  <div class="card-title">
				            <span class="card-icon">
				                <i class="flaticon2-chat-1 text-primary"></i>
				            </span>
				   <h3 class="card-label">
				    Invoices
				   </h3>
				  </div>
				 </div>
				 <div class="card-body p-0">
              @include('event::event.invoices_grid',['request'=>$request,'model'=>$model])
				 </div>
				</div>
			</div>





				<div class="tab-pane fade bg-white rounded" id="payments" role="tabpanel" aria-labelledby="kt_tab_pane_4" style="min-height:550px">
					<div class="card card-custom shadow-none">
					 <div class="card-header">
					  <div class="card-title">
					            <span class="card-icon">
					                <i class="flaticon2-chat-1 text-primary"></i>
					            </span>
					   <h3 class="card-label">
					    Payments
					   </h3>
					  </div>
					 </div>
					 <div class="card-body">
					       <x-salescom-payments-list :req="$request" :url="'payments/data-table-data?event_id='.$model->id" />
					 </div>
					</div>
				</div>
        <div class="tab-pane fade bg-white rounded"  style="min-height:670px" role="tabpanel" aria-labelledby="tab-actions" id="tab-actions">
          <div class="card card-custom shadow-none">
           <div class="card-header">
            <div class="card-title">
              <span class="card-icon">
                  <i class="flaticon2-chat-1 text-primary"></i>
              </span>
             <h3 class="card-label">
              Action Log
             </h3>
            </div>
           </div>
           <div class="card-body">
             <x-actionlog-activity-history :model="$model"/>
           </div>
          </div>

        </div>

				</div>
			</div>
		</div>

<x-actionlog-activity :model="$model"/>
    <!-- Styles -->
    <style>
    #chartdiv {
      width: 70%;
      height: 350px;
    }
    </style>

    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/5/index.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
    <script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

    <!-- Chart code -->
    <script>
    am5.ready(function() {

    // Create root element
    // https://www.amcharts.com/docs/v5/getting-started/#Root_element
    var root = am5.Root.new("chartdiv");


    // Set themes
    // https://www.amcharts.com/docs/v5/concepts/themes/
    root.setThemes([
      am5themes_Animated.new(root)
    ]);


    // Create chart
    // https://www.amcharts.com/docs/v5/charts/xy-chart/
    var chart = root.container.children.push(am5xy.XYChart.new(root, {
      panX: true,
      panY: true,
      wheelX: "panX",
      wheelY: "zoomX"
    }));

    // Add cursor
    // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
    var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {}));
    cursor.lineY.set("visible", false);


    // Create axes
    // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
    var xRenderer = am5xy.AxisRendererX.new(root, { minGridDistance: 30 });
    xRenderer.labels.template.setAll({
      rotation: -90,
      centerY: am5.p50,
      centerX: am5.p100,
      paddingRight: 15
    });

    var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root, {
      maxDeviation: 0.3,
      categoryField: "country",
      renderer: xRenderer,
      tooltip: am5.Tooltip.new(root, {})
    }));

    var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
      maxDeviation: 0.3,
      renderer: am5xy.AxisRendererY.new(root, {})
    }));


    // Create series
    // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
    var series = chart.series.push(am5xy.ColumnSeries.new(root, {
      name: "Series 1",
      xAxis: xAxis,
      yAxis: yAxis,
      valueYField: "value",
      sequencedInterpolation: true,
      categoryXField: "country",
      tooltip: am5.Tooltip.new(root, {
        labelText:"{valueY}"
      })
    }));

    series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5 });
    series.columns.template.adapters.add("fill", (fill, target) => {
      return chart.get("colors").getIndex(series.columns.indexOf(target));
    });

    series.columns.template.adapters.add("stroke", (stroke, target) => {
      return chart.get("colors").getIndex(series.columns.indexOf(target));
    });


    // Set data
    var data = [{
      country: "Registerd",
      value: <?= $RegisterdMembers ?>
    }, {
      country: "Members",
      value: <?= $MembersOnly ?>
    }, {
      country: "Pre-members",
      value: <?= $guestonly ?>
    }, {
    //   country: "Fee Paid",
    //   value: <?php //echo $Fee_paid ?>
    // }, {
    //   country: "Not Paid",
    //   value: 23
    // }, {
      country: "attended",
      value: <?= $attended ?>
    }, {
      country: "No Show",
      value: <?= $not_attended ?>
    }, {
      country: "Cancelled",
      value: <?= $Cancelled ?>
    }  ];


    xAxis.data.setAll(data);
    series.data.setAll(data);


    // Make stuff animate on load
    // https://www.amcharts.com/docs/v5/concepts/animations/
    series.appear(1000);
    chart.appear(1000, 100);

    }); // end am5.ready()
    </script>





























@endsection

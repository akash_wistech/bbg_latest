
@extends('layouts.app')
@section('title', __('event_lang::event.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'event'=> __('event_lang::event.heading'),
  __('common.create')
]])
@endsection
@section('content')

<?php




if ($model->user_type=='guest') {
    $user = $model->guest;
    $readonly_member='';
    $readonly_member_solid='';
}
if (isset($model->user_type) && $model->user_type=='member') {
    $user = $model->user;
    $readonly_member='readonly';
    $readonly_member_solid='form-control-solid';
}



$company=  getCompanyInfo($model->company_id);

// dd($model);


 ?>

{!! Form::model($model, ['files' => true]) !!}
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif

<div class="row">

  <div class="col-xs-12 col-sm-4">
    <div class="form-group">
      {{ Form::label('name',__('Name')) }}
      {{ Form::text('name', (($model->user_type=='guest')?$user->full_name : $user->name ), ['class'=>'form-control '.$readonly_member_solid,$readonly_member]) }}
      <input type="hidden" name="id" value="{{ $user->id }}">
      <input type="hidden" name="user_type" value="{{ $model->user_type }}">
    </div>
  </div>
  <div class="col-xs-12 col-sm-4">
    <div class="form-group">
      {{ Form::label('email',__('Email')) }}
      {{ Form::text('email', $user->email, ['class'=>'form-control '.$readonly_member_solid,$readonly_member]) }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-4">
    <div class="form-group">
      {{ Form::label('mobile',__('Mobile')) }}
      {{ Form::text('mobile', $user->phone, ['class'=>'form-control '.$readonly_member_solid,$readonly_member]) }}
    </div>
  </div>
</div>


<div class="row ">
  <div class="col-xs-12 col-sm-4 d-none">
    <div class="form-group">
      {{ Form::label('designation',__('Designation')) }}
			{{ Form::text('designation', $model->designation, ['class'=>'form-control']) }}
      </div>
  </div>
  <div class="col-xs-12 col-sm-4">
    <div class="form-group">
      {{ Form::label('company',__('Company')) }}
      {{ Form::text('company', (($model->user_type=='guest')?$user->company_name : $company->title ), ['class'=>'form-control  '.$readonly_member_solid,$readonly_member]) }}
    </div>
  </div>
  <div class="col-xs-12 col-sm-4">
    <div class="form-group">
      {{ Form::label('user_type_relation',__('User Type')) }}
      {{ Form::select('user_type_relation',[''=>__('common.select')] +getUserTypeRelation(), $model->user_type_relation, ['class'=>'form-control']) }}
	  </div>
  </div>

  <div class="col-xs-12 col-sm-4">
    <div class="form-group">
      {{ Form::label('fee_paid',__('Fee Paid')) }}
      {{ Form::text('fee_paid', $model->fee_paid, ['class'=>'form-control', 'disabled'=>'disabled']) }}
    </div>
  </div>
</div>

<div class="row mt-5">

	<?php echo getCustomFieldsMultiple($model,$isSub=false,$card=false,4) ?>
</div>






<div class="">



								     <div class="form-group row mt-2">
								        <div class="col col-form-label">
								            <div class="checkbox-inline">
								                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
								                    <input type="checkbox" name="focGuest" value="1" {{ ($model->focGuest=='1') ? 'checked':''}} />
								                    <span></span>
								                    Foc Guest
								                </label>
								                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
								                    <input type="checkbox" name="diet_option" class="diet-option-box" value="1"  {{ ($model->diet_option=='1') ? 'checked':''}}/>
								                    <span></span>
								                    Diet Options
								                </label>
								                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
								                    <input type="checkbox" name="vaccinated" value="1"  {{ ($model->vaccinated=='1') ? 'checked':''}}/>
								                    <span></span>
								                    Vaccinated
								                </label>
                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                   <input type="checkbox" name="walkin" value="1"  {{ ($model->walkin=='1') ? 'checked':''}}/>
                                   <span></span>
                                   Walkin
                               </label>
                               <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                   <input type="checkbox" name="attended" value="1"  {{ ($model->attended=='1') ? 'checked':''}}/>
                                   <span></span>
                                   Attended
                               </label>
								            </div>
								        </div>
								    </div>

                    <?php //dd($model); ?>

                    <div class="row">
							    	<div class="col-3 diet-option-description-select" style="{{ ($model->diet_option==1) ? '' : 'display: none;' }}">
							    		<div class="form-group">
							    			{{ Form::select('diet_option_description', [''=>__('Select Dietry Option...')] + getdietoptiondescription(),  $model->diet_option_description, ['class'=>'form-control diet-select-options']) }}
							    		</div>
							    	</div>
							    	<div class="col-9 other-diet-option-description" style="{{ ($model->diet_option_description=='other') ? '' : 'display: none;' }}">
							    		<div class="form-group">
							    			{{ Form::textarea('other_diet_option_description', $model->other_diet_option_description, ['class'=>'form-control ', 'placeholder'=>'Write Description', 'rows'=>3]) }}
							    		</div>
							    	</div>
							    </div>

</div>




</div>
<div class="card-footer">
  <button type="submit" class="btn btn-success">{{__('common.save')}}</button>
  <a href="{{ url('/event/event-member-index/'.$EventId) }}" class="btn btn-default">{{__('common.cancel')}}</a>
</div>
</div>
{!! Form::close() !!}
@endsection


@push('js')

<script>
			$('body').on('click', '.diet-option-box', function() {
				if($(this).prop("checked") == true){

					if ($('.diet-select-options').val()=='other') {
					$('.other-diet-option-description').show();
					}

					$('.diet-option-description-select').show();
				}
				else if($(this).prop("checked") == false){
					$('.diet-option-description-select').hide();
					$('.other-diet-option-description').hide();
				}
			});

			$('body').on('change', '.diet-select-options', function() {
				if($(this).val()=='other'){
					$('.other-diet-option-description').show();
				}else{
					$('.other-diet-option-description').hide();
				}
			});
		</script>
@endpush

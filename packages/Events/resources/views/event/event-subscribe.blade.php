@extends('layouts.app')
@section('title', __('Event Registration'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'event'=> __('Event Registration'),
  __('common.create')
]])
@endsection
@section('content')

@php
$usersList=[''=>'Select Company first before chosing members.'];
$companyName='';
if($model->company!=null){
  $companyName =$model->company->title;
  $usersList = getCompanyUsersList($model->company_id);
}
$invoiceRef = $model->reference_number;

$compInputOpts = [];
if($model->id!=null){
  $compInputOpts['class']='form-control';
  $compInputOpts['readonly']='readonly';
}else{
  $compInputOpts['class']='autocomplete-company form-control';
  $compInputOpts['data-ds']=url('suggestion/company');
  $compInputOpts['data-mtype']=($model->module_type!='' ? $model->module_type : '');
  $compInputOpts['data-fld']='company_id';
  $compInputOpts['data-module']='contact';
  $compInputOpts['data-oldsval']=$model->user_id;
  $compInputOpts['data-sfld']='user_id';
}
@endphp

<?php

// dd("here");
$company =App\Models\Company::get()->pluck('title','id')->toArray(); ?>

<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<div class="container">
			<div class="card card-custom">
				<div class="card-body p-0">
			   {!! Form::model($model, ['files' => true,'id'=>'kt_form']) !!}
  				<div class="card-body">
  					@if ($errors->any())
  					<div class="alert alert-danger">
  						@foreach ($errors->all() as $error)
  						<div>{{ $error }}</div>
  						@endforeach
  					</div>
  					@endif


						<div>
              <!-- Company and Members dropdown -->
              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('company_id',__('common.company'),['class'=>'required']) }}
                    {!! Form::text('customer_name', $companyName,$compInputOpts) !!}
                    {{ Form::hidden('company_id', $model->company_id, ['id'=>'company_id','class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('user_id',__('common.user_id'),['class'=>'']) }}
                    {!! Form::select('user_id',$usersList, $model->user_id, ['id'=>'user_id', 'class' => 'form-control comeMembers']) !!}
                  </div>
                </div>

                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('primary_pay',__('common.primary_pay'),['class'=>'']) }}
                    {!! Form::select('primary_pay',[''=>'Select primary Member when adding members'],'', ['id'=>'primary_pay', 'class' => 'form-control']) !!}
                  </div>
                </div>
                <input type="hidden" id='event-id' name="event_id" value="{{ $id }}">
              </div>

            <!-- Members will show here -->
            <div class="members_here">
              <div class="dummy-member-box rounded" style="border: 2px dashed #E0E0E0; min-height:130px; background-color:#fcfcfc; text-align:center; padding-top:50px">
                 <p style="margin: auto; ">Members will be  shown here.<br> Add Members by selecting member from contact</p>
               </div>
            </div>


            <!-- Add Guest Button -->
            <div class="row my-5 flex-row-reverse">
              <div class="card-header px-3 border-0 pt-5">
                <div class="card-toolbar">
                  <div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="">
                    <a href="#" class="btn btn-clean btn-hover-light-primary btn-light-primary btn-sm btn-icon font-size-sm font-weight-bolder mx-2" style="width:90px; height:40px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Add Guest
                    </a>
                    <div class="dropdown-menu dropdown-menu-md dropdown-menu-right" style="z-index:99"  x-placement="top-end">
                      <ul class="navi navi-hover">
                        <?php
                        $userTypesList = getUserTypeRelation();
                        if($userTypesList!=null){
                          foreach($userTypesList as $key=>$val){ ?>
                            <li class="navi-item">
                              <a href="javascript:;" class="navi-link add_guest_2" data-usertype="{{ $key }}">
                                <span class="navi-icon">
                                  <i class="flaticon2-drop"></i>
                                </span>
                                <span class="navi-text">{{ $val }}</span>
                              </a>
                            </li>
                          <?php }} ?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>







          <!-- Guests will show here -->
              <div id="guestInformation">
                <div class="dummy-member-box rounded" style="border: 2px dashed #E0E0E0; min-height:130px; background-color:#fcfcfc; text-align:center; padding-top:50px">
                 <p style="margin: auto; ">Members and Guest will show here. </p>
               </div>
              </div>
          </div>


					</div>
					<div class="card-footer border-0">
						<button type="submit" class="btn btn-success">{{__('common.save')}}</button>
						<a href="{{ url('/event') }}" class="btn btn-default">{{__('common.cancel')}}</a>
					</div>
					 {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>


@endsection
@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
var GuestCount=1;
var memberCount=1;
var eventId = $('#event-id').val();
var frontend = 'backend';
//It runs when we select member from user list
$('body').on('change','.comeMembers', function(){
    memberid= $(this).val();
    getmemberDetials(memberid);
});

//Get Member EventsDetails
function getmemberDetials(memberid){
    data = {member_id:memberid,memberCount,eventId,frontend};
    $.ajax({
        url:"<?= url('event/get-event-registeration-type')?>",
        type:"POST",
        data: {
            "_token": "<?= csrf_token() ?>",
            data: data,
        },
        success:function (data) {
            if (data.status=='success') {
                memberCount++;
              $("#primary_pay").append(new Option(data.primary_pay_array.name, data.primary_pay_array.id));
                $('.members_here').append(data.html);
                $('.dummy-member-box').addClass('d-none');
            }
            if (data.status=='error') {
              swal({
                     title: 'Error!',
                     text: data.msg,
                     type:data.status,
                     confirmButtonText: 'OK'
                   });
            }
        }
    });
}


// Add guest button
$("body").on("click",'.add_guest_2', function(){
GuestCount++;
guestType= $(this).data('usertype');
  var eventId = $('#event-id').val();
  data = {eventId,GuestCount,guestType};
$.ajax({
  url:"<?= url('event/get-guest-form')?>",
  type:"POST",
  data: {
    "_token": "<?= csrf_token() ?>",
    data:data,
 },
 success:function (data) {
 $('#guestInformation').append(data);

 }
});
});


$("body").on("click", '.remove_div', function(){
$(this).parents('.tr_remove').remove();
});

$("body").on("click", '.remove_guest', function(){
$(this).parents('.remove_guest_1').remove();
});


// Guest Diet First Option
$('body').on('click', '.diet-option-box', function() {
  if($(this).prop("checked") == true){
    if ($(this).parents('.parent-card').find('.diet-select-options').val()=='other') {
      $(this).parents('.parent-card').find('.other-diet-option-description').show();
  }
  $(this).parents('.parent-card').find('.diet-option-description-select').show();
  }
else if($(this).prop("checked") == false){
    $(this).parents('.parent-card').find('.diet-option-description-select').hide();
    $(this).parents('.parent-card').find('.other-diet-option-description').hide();
    }
});

// After checked diet_option , we select diet , this function will work
$('body').on('change', '.diet-select-options', function() {
if($(this).val()=='other'){
    $(this).parents('.parent-card').find('.other-diet-option-description').show();
}else{
    $(this).parents('.parent-card').find('.other-diet-option-description').hide();
}
});

// diet-option-box-member
$('body').on('click', '.diet-option-box-member', function() {
  if($(this).prop("checked") == true){
      $(this).parents('.tr_remove').find('.diet-option-description-select-member').show();
      $('#user_type_relation').addClass('mt-4');
  }
  else if($(this).prop("checked") == false){
      $(this).parents('.tr_remove').find('.diet-option-description-select-member').hide();
      $('#user_type_relation').removeClass('mt-4');
  }
});


tinymce.init({
selector: '.editor',
height: "20px",
menubar: false,
toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
plugins : 'advlist autolink link image lists charmap code table',
image_advtab: true ,
});
$('.kt_timepicker_1').timepicker();
$('#checkbox1').change(function() {
    if(this.checked) {
    $('.member_fee').addClass('d-none');
    }
    if(!this.checked) {
    $('.member_fee').removeClass('d-none');
    }
});
</script>


@endpush

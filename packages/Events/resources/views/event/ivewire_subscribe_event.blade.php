
@extends('layouts.app')
@section('title', __('Event Registration'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'event'=> __('Event Registration'),
  __('common.create')
]])
@endsection
@section('content')


<?php $company =App\Models\Company::get()->pluck('title','id')->toArray(); ?>

@livewire('event-members-register',[
  'model'=>$model,
  'event_id'=>$id,
])
@endsection
@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>

<script>
$('body').on('click', '.diet-option-box', function() {
	// $('.diet-option-box').click(function(){
		if($(this).prop("checked") == true){
			$(this).parents('.parent-card').find('.diet-option-description-select').show();
		}
		else if($(this).prop("checked") == false){
			$(this).parents('.parent-card').find('.diet-option-description-select').hide();
			$(this).parents('.parent-card').find('.other-diet-option-description').hide();
		}
	});

$('body').on('change', '.diet-select-options', function() {
	// $('.diet-select-options').click(function(){
		if($(this).val()=='other'){
			$(this).parents('.parent-card').find('.other-diet-option-description').show();
		}else{
			$(this).parents('.parent-card').find('.other-diet-option-description').hide();
		}
	});

	$("body").on("click" ,"#inviteGuest-backend", function(){
// console.log("hello world");
_this = $(this);
GuestInvitee(_this);
});


var GuestCount=1;

	$('body').on('click', '.add-member-guest-btn', function(){

		guestInformationn(GuestCount);
    GuestCount++;
	});

	$('body').on('click', '.remove-mem-guest-btn', function(){
		$(this).parents('.guest-member-parent').remove();
	});

// 	function GuestInvitee(_this)
// 	{
// 		if (_this.is(':checked')) {
// 		// console.log("hello");
// 		var num_guest = $("#input_guest_number").val();
// 		console.log("num_guest:");
// 		console.log(num_guest);
// 		if(num_guest == '0'){
// 			$("#input_guest_number").val(1);
// 			num_guest = 1;
// 		}

// 		var total = parseFloat(num_guest) * parseFloat($("#nonmember_fee").text());
// 		$(".nonmember_fee_total").text(total);
// 		$("#guestsNumbers").text(num_guest);
// 		$("#guestInformation").html(guestInformationn(num_guest));

// 		$("#section_invite_guest").show();
// 		}
// 		else {
// 			$("#section_invite_guest").hide();
// 			$("#guestInformation").html('');
// 			$("#input_guest_number").val(1);
// 		}
// }

var GuestCount=1;

$("body").on("click",'.add_guest_1', function(){
GuestCount++;
_this= $(this);
var id= $(this).data('id');
  var html = '';
  var eventId = $('#event-id').val();
  data = {eventId:eventId,member_id:id, GuestCount:GuestCount};

$.ajax({
  url:"<?= url('event/get-guest-form')?>",
  type:"POST",
  data: {
    "_token": "<?= csrf_token() ?>",
    data:data,
 },
 success:function (data) {
 $(_this).parents('.tr_remove').find('.add_guest_here_1').append(data);

 }
});
});

$("body").on("click",'.add_guest_2', function(){

GuestCount++;
_this= $(this);

  var html = '';
  var eventId = $('#event-id').val();
  data = {eventId:eventId, GuestCount:GuestCount};

$.ajax({
  url:"<?= url('event/get-guest-form')?>",
  type:"POST",
  data: {
    "_token": "<?= csrf_token() ?>",
    data:data,
 },
 success:function (data) {
 $('#guestInformation').append(data);

 }
});
});





			$('body').on('click', '.diet-option-box', function() {
				if($(this).prop("checked") == true){

          console.log($(this).parents('.parent-card').find('.diet-select-options').val());

					if ($(this).parents('.parent-card').find('.diet-select-options').val()=='other') {
					$(this).parents('.parent-card').find('.other-diet-option-description').show();
					}

				$(this).parents('.parent-card').find('.diet-option-description-select').show();
				}
				else if($(this).prop("checked") == false){
				$(this).parents('.parent-card').find('.diet-option-description-select').hide();
				$(this).parents('.parent-card').find('.other-diet-option-description').hide();
				}
			});

			$('body').on('change', '.diet-select-options', function() {
				if($(this).val()=='other'){
				$(this).parents('parent-card').find('.other-diet-option-description').show();
				}else{
				$(this).parents('parent-card').find('.other-diet-option-description').hide();
				}
			});

</script>

@endpush


@push('js-script')

tinymce.init({
selector: '.editor',
height: "20px",
menubar: false,
toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
plugins : 'advlist autolink link image lists charmap code table',
image_advtab: true ,
});


  $('.kt_timepicker_1').timepicker();

    $('#checkbox1').change(function() {
        if(this.checked) {
        $('.member_fee').addClass('d-none');
        }

        if(!this.checked) {
        $('.member_fee').removeClass('d-none');
        }


    });




var memberCount=1;

$('body').on('change','.comeMembers', function(){
_this= $(this).val();

var eventId = $('#event-id').val();
var companyName = $('.autocomplete-company').val();
var company_id = $("#company_id").val();

console.log(company_id);

data = {member_id:_this,memberCount:memberCount,eventId:eventId,companyName:companyName,company_id:company_id};
memberCount++;
$.ajax({
  url:"<?= url('event/get-members-detail')?>",
  type:"POST",
  data: {
    "_token": "<?= csrf_token() ?>",
    data: data,
 },
 success:function (data) {
 $('.members_here').append(data);

 }
});
});


$("body").on("click", '.remove_div', function(){
$(this).parents('.tr_remove').addClass('d-none');
});

$("body").on("click", '.remove_guest', function(){
$(this).parents('.remove_guest_1').addClass('d-none');
});

$('body').on('click', '.diet-option-box', function() {
		if($(this).prop("checked") == true){
       $(this).parents('.remove_guest_1').find('.diet-option-description-select').show();
		}
		else if($(this).prop("checked") == false){
      $(this).parents('.remove_guest_1').find('.diet-option-description-select').hide();
		//	$(this).parents('.parent-card').find('.diet-option-description-select')
			//$(this).parents('.parent-card').find('.other-diet-option-description').hide();
		}
	});

  $('body').on('click', '.diet-option-box-member', function() {
  		if($(this).prop("checked") == true){
         $(this).parents('.tr_remove').find('.diet-option-description-select-member').show();
         $('#user_type_relation').addClass('mt-4');
  		}
  		else if($(this).prop("checked") == false){
        $(this).parents('.tr_remove').find('.diet-option-description-select-member').hide();
        $('#user_type_relation').removeClass('mt-4');
  		//	$(this).parents('.parent-card').find('.diet-option-description-select-member')
  			//$(this).parents('.parent-card').find('.other-diet-option-description').hide();
  		}
  	});



@endpush

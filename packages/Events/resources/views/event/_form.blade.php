@extends('layouts.app')
@section('content')



<link href="{{asset('assets/css/pages/wizard/wizard-3.css')}}" rel="stylesheet" type="text/css" />
<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<div class="container">
			<div class="card card-custom">
				<div class="card-body p-0">
					<!--begin: Wizard-->
					<div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first" data-wizard-clickable="true">
						<!--begin: Wizard Nav-->
						<div class="wizard-nav">
							<div class="wizard-steps px-8 py-8 px-lg-15 py-lg-3">
								<!--begin::Wizard Step 1 Nav-->
								<div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
									<div class="wizard-label">
										<h3 class="wizard-title">
										General</h3>
										<div class="wizard-bar"></div>
									</div>
								</div>
								<!--end::Wizard Step 1 Nav-->
								<!--begin::Wizard Step 2 Nav-->
								<div class="wizard-step" data-wizard-type="step">
									<div class="wizard-label">
										<h3 class="wizard-title">
										Add Images</h3>
										<div class="wizard-bar"></div>
									</div>
								</div>
								<!--end::Wizard Step 2 Nav-->
							</div>
						</div>
						<!--end: Wizard Nav-->
						<!--begin: Wizard Body-->
						<div class="row justify-content-center px-8">
							<div class="col">
								<!--begin: Wizard Form-->
								{!! Form::model($model, ['files' => true,'id'=>'kt_form']) !!}


									<!--begin: Wizard Step 1-->
									<div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

										<div class="card-body">
										@if ($errors->any())
										<div class="alert alert-danger">
											<div>{{ 'Please Fill Required Fields.' }}</div>
										</div>
										@endif

										<div class="row px-0">



											<div class="col-md-4 col-sm-6 col-12">
												<div class="form-group">
													{{ Form::label('title',__('Title'),['class'=>'required']) }}
													{{ Form::text('title', $model->title, ['class'=>'form-control '.($errors->has('title') ? ' is-invalid' : ''), 'placeholder'=>'title']) }}
												</div>
											</div>
											<div class="col-md-4 col-sm-6 col-12">
												<div class="form-group">
													{{ Form::label('type',__('Type'),['class'=>'required']) }}
													{{ Form::select('type', [''=>__('common.select')] + getPredefinedListItemsArr(1617), $model->type, ['class'=>'form-control'.($errors->has('type') ? ' is-invalid' : '')]) }}

												</div>
											</div>
											<div class="col-md-4 col-sm-6 col-12">
												<div class="form-group">
													{{ Form::label('venue',__('Venue'),['class'=>'required']) }}
													{{ Form::text('venue', $model->venue, ['class'=>'form-control'.($errors->has('venue') ? ' is-invalid' : ''), 'placeholder'=>'venue']) }}
												</div>
											</div>
								  		<div class="col-md-8 col-sm-6 col-12">
												<div class="form-group">
													{{ Form::label('map_address',__('Map Address')) }}
													{{ Form::text('map_address', $model->map_address, ['class'=>'form-control', 'placeholder'=>'Map Address']) }}
												</div>
											</div>
											<!-- <div class="col-md-4 col-sm-6 col-12">
												<div class="form-group">
													{{ Form::label('fb_gallery',__('Facebook Gallery')) }}
													{{ Form::text('fb_gallery', $model->fb_gallery, ['class'=>'form-control', 'placeholder'=>'facebook gallery']) }}
												</div>
											</div> -->
											<div class=" col-md-4 col-sm-6 col-12">
												<div class="form-group">
													{{ Form::label('group_size',__('Group Size')) }}
													{{ Form::text('group_size', $model->group_size, ['class'=>'form-control required'.($errors->has('group_size') ? ' is-invalid' : '')]) }}
												</div>
											</div>
										</div>

										<div class="row px-0">
											<div class="col-12">
												<div class="form-group">
													{{ Form::label('address',__('Adderss')) }}
													{{ Form::textarea('address', $model->address, ['class'=>'form-control', 'placeholder'=>'address', 'rows'=>'2']) }}
												</div>
											</div>
										</div>




										<div class="row">
											<div class="col-xs-12 col-sm-12">
												<div class="form-group">
													{{ Form::label('short_description',__('Short Description')) }}
													{{ Form::textarea('short_description', $model->short_description, ['class'=>'form-control'.($errors->has('short_description') ? ' is-invalid' : ''), 'placeholder'=>'short description','rows'=>'3']) }}
												</div>
											</div>
										</div>



										<div class="row">
											<div class="col-lg-6 col-12">
												<div class="mt-4 mb-10 ">
													<h3 >Timings</h3>
												</div>
												<div class="row">
													<div class="form-group col-sm-6 col-12 ">
												    <label class="col"><?= __('Registration Start') ?></label>
												    <div class="col px-0">
												     <div class="input-group date" >
												      <input type="text" class="form-control popdtpicker {{($errors->has('registration_start') ? ' is-invalid' : '')}}" name="registration_start" placeholder='registration start' readonly  value="{{ $model->registration_start }}"/>
												      <div class="input-group-append">
												       <span class="input-group-text">
												        <i class="la la-calendar button-date-select"></i>
												       </span>
												      </div>
												     </div>
												    </div>
												   </div>

												   <div class="form-group col-sm-6 col-12 ">
												    <label class="col"><?= __('Registration End') ?></label>
												    <div class="col px-0">
												     <div class="input-group date" >
												      <input type="text" class="form-control popdtpicker {{($errors->has('registration_end') ? ' is-invalid' : '')}}" name="registration_end" placeholder='registration end' readonly  value="{{ $model->registration_end }}"/>
												      <div class="input-group-append">
												       <span class="input-group-text">
												        <i class="la la-calendar button-date-select"></i>
												       </span>
												      </div>
												     </div>
												    </div>
												   </div>
												</div>



												<div class="row">
													<div class="form-group col-sm-6 col-12 ">
												    <label class="col"><?= __('Event Start Date') ?></label>
												    <div class="col px-0">
												     <div class="input-group date" >
												      <input type="text" class="form-control popdtpicker {{($errors->has('event_startDate') ? ' is-invalid' : '')}}" name="event_startDate" placeholder='event start date' readonly  value="{{ $model->event_startDate }}"/>
												      <div class="input-group-append">
												       <span class="input-group-text">
												        <i class="la la-calendar button-date-select"></i>
												       </span>
												      </div>
												     </div>
												    </div>
												   </div>

												   <div class="form-group col-sm-6 col-12 ">
												    <label class="col"><?= __('Event End Date') ?></label>
												    <div class="col px-0">
												     <div class="input-group date" >
												      <input type="text" class="form-control popdtpicker {{($errors->has('event_endDate') ? ' is-invalid' : '')}}" name="event_endDate" placeholder='event end date' readonly  value="{{ $model->event_endDate }}"/>
												      <div class="input-group-append">
												       <span class="input-group-text">
												        <i class="la la-calendar button-date-select"></i>
												       </span>
												      </div>
												     </div>
												    </div>
												   </div>
												</div>



												<div class="row">

													<div class="form-group col-sm-6 col-12 ">
												    <label class="col">Event Start Time</label>
												    <div class="col px-0">
												     <input class="form-control kt_timepicker_1" name="event_startTime" value="{{$model->event_startTime}}" readonly placeholder="event start time" type="text"/>
												    </div>
												   </div>

												   <div class="form-group col-sm-6 col-12 ">
												    <label class="col">Event End Time</label>
												    <div class="col px-0">
												     <input class="form-control kt_timepicker_1" name="event_endTime" value="{{$model->event_endTime}}" readonly placeholder="event end time" type="text"/>
												    </div>
												   </div>
												</div>

												<div class="row">
													<div class="form-group col-sm-6 col-12 ">
														<label class="col"><?= __('Cancellation Date') ?></label>
														<div class="col px-0">
														 <div class="input-group date" >
															<input type="text" class=	"form-control popdtpicker {{($errors->has('cancellation_tillDate') ? ' is-invalid' : '')}}" name="cancellation_tillDate" placeholder='cancellation date' readonly  value="{{ $model->cancellation_tillDate }}"/>
															<div class="input-group-append">
															 <span class="input-group-text">
																<i class="la la-calendar button-date-select"></i>
															 </span>
															</div>
														 </div>
														</div>
													 </div>
												</div>
											</div>

											<div class="col-lg-6 col-12">
												<div class="mt-4 mb-10 ">
													<h3 >Fees</h3>
												</div>

											   <div class="member_fee">
												<div class="row">
													@foreach(getUserTypeRelation() as $key=>$val)
													@php
													$model->fees[$key]=getEventFeeByGuestType($model->id,$key);
													@endphp
												  <div class="col-sm-4 col-6">
													<div class="form-group">
														{{ Form::label('fees['.$key.']',$val.' Fees') }}
														{{ Form::text('fees['.$key.']', null, ['class'=>'form-control']) }}
													</div>
												  </div>
													@endforeach
												</div>
												@php
												$taxArr=getTaxArr();
												@endphp
												<div class="row">
												<div class="col-sm-4 col-6">
													<div class="form-group">
														{{ Form::label('expense',__('Expense')) }}
														{{ Form::text('expense', null, ['class'=>'form-control']) }}
													</div>
												</div>
													<div class="col-sm-4 col-6" style="visibility:hidden;">
														<div class="form-group">
															{{ Form::label('tax_id',__('Tax')) }}
															<select id="tax_id" name="tax_id" class="form-control">
						                    <option value="0">{{__('sales::invoices.no_tax')}}</option>
						                    @foreach($taxArr as $key=>$val)
						                    <!-- <option value="{{$key}}"{{$model->tax_id==$key ? ' selected' : ''}}>{{$val.'%'}}</option> -->
						                    <option value="{{$key}}"{{1==$key ? ' selected' : ''}}>{{$val.'%'}}</option>
						                    @endforeach
						                  </select>
														</div>
													</div>
												</div>

												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12 col-sm-12 px-0 mt-4">
												<div class="form-group">
													{{ Form::label('description',__('event_lang::event.description')) }}
													{{ Form::text('description', $model->description, ['class'=>'form-control editor']) }}
												</div>
											</div>
										</div>



							     <div class="form-group row mt-2">
							        <div class="col col-form-label">
							            <div class="checkbox-inline">
							                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
							                    <input type="checkbox" name="active" value="1" {{ ($model->active=='1') ? 'checked':''}} />
							                    <span></span>
							                    Published
							                </label>
							                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
							                    <input type="checkbox" name="members_only" value="1" {{ ($model->members_only=='1') ? 'checked':''}}/>
							                    <span></span>
							                    Members Only
							                </label>
							                <!-- <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
							                    <input type="checkbox" name="diet_option"<?php //echo ($model->diet_option=='on') ? 'checked':'' ?>/>
							                    <span></span>
							                    Diet Option
							                </label> -->
							                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
							                    <input type="checkbox" name="info_only" value="1" {{ ($model->info_only=='1') ? 'checked':''}}/>



							                    <span></span>
							                    Info Only
							                </label>
							            </div>
							        </div>
							    </div>

<div class="mb-10">
	<x-customfields-custome-fields  card="true" :model="$model"/>
</div>






<?php

$customfields = getCustomFieldsByModule('event-member-register');
if ($customfields) {
 $n=1;
	foreach ($customfields as $key => $customfield) {

		if ($n==1 && $customfield<>null) {
		?>
		<div class="col-6 px-0">
			<div class=" mb-6 ">
				<h4 style="font-size:20px"> Event Form Fields</h4>
			</div>
			</div>
	<div class="row pl-4">

<?php

}
		$ModuleCustomFieldsOption= \Wisdom\CustomeFields\Models\ModuleCustomFieldsOption::where('module_type','event-member-register')->where('module_id',$model->id)
															->where('custom_field_id',$customfield->id)->whereNull('deleted_at')->first();


		?>
		<label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary mr-6">
				<input type="checkbox" name="<?=  $model->moduleTypeId.'['.$customfield->id.']' ?>" {{ ($ModuleCustomFieldsOption<>null) ? 'checked':''}} />

				<span class="mr-3"></span>
				{{ ucfirst($customfield->title) }}
		</label>

<?php	 if ($n==1 && $customfield<>null) { ?>
	</div>
<?php $n++; } } }  ?>





									</div>
									</div>
									<!--end: Wizard Step 1-->


									<!--begin: Wizard Step 2-->
									<div class="pb-5" data-wizard-type="step-content">
										<div class="row ">
									<!-- image here -->
									<div class="col-xs-12 col-md-2">
										<div id="images" class="form-group pb-0 mb-2">
											<a href="" data-thumbid="thumb-image-main" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
												<img src="{{ $model->image != '' && $model->image <> null ? $model->image : asset('assets/images/dummy-image.jpg') }}"
												alt="" width="145" height="125" title=""
												data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
											</a>
											{{ Form::text('image', $model->image, ['maxlength' => true, 'data-targetid' => 'input-image-main','class'=>'form-control d-none']) }}
										</div>
										<span class="label label-xl label-muted font-weight-bold label-inline mx-8">Main Image</span>
									</div>
									<!-- image end -->
								</div>

								<div class="row my-6">
									<div class="col-12">
										<table class="table table-light table-bordered table-sm" id="events">
											<thead class="thead-light">
												<tr>
													<th class="text-center">Image</th>
													<th class="">Title</th>
													<th class="">Short Description</th>
													<th class="">Sort Order</th>
													<th class="text-center">
														<a href="javascript:;" class="add-button btn btn-icon btn-success btn-xs ml-2" data-toggle="tooltip" title="" aria-haspopup="true" aria-expanded="false" data-original-title="Add">
															<i class="flaticon2-plus"></i>
														</a>
													</th>
												</tr>
											</thead>
											<tbody>
												<?php $image_row = 0;
												if ($model->id!=null) {
													$results = getEventRows($model->id);
													if ($results!=null) {
														foreach ($results as $value) {
						// dd($value);
															?>
															<tr class="append-area">
																<input class="form-control d-none " name="existingId[<?= $image_row ?>]" type="text" value="<?= $value['id'] ?>">
																<td>
																	<div id="images" class="form-group pb-0 mb-2 pt-3 mx-1 text-center">
																		<a href="javascript:;" data-thumbid="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail" data-content="" width="20">
																			<img src="<?= $value['image']!=''? $value['image'] : asset('assets/images/dummy-image.jpg') ?>" width="60"
																			data-placeholder="{{asset('assets/images/dummy-image.jpg')}}">
																		</a>
																		<input value="<?= $value['image'] ?>" data-targetid="input-image<?php echo $image_row; ?>" class="form-control d-none" name="events_images[<?php echo $image_row; ?>][image]" type="text">
																	</div>
																</td>
																<td>
																	<div class="form-group pt-6">
																		<input class="form-control" name="events_images[<?php echo $image_row; ?>][title]" type="text" value="<?= $value['title']  ?>">
																	</div>
																</td>
																<td>
																	<div class="form-group pt-6">
																		<input class="form-control description" name="events_images[<?php echo $image_row; ?>][description]" type="text" value="<?= $value['description'] ?>">
																	</div>

																</td>
																<td>
																	<div class="form-group pt-6">
																		<input class="form-control" name="events_images[<?php echo $image_row; ?>][sort_order]" type="number"value="<?= $value['sort_order']  ?>">
																	</div>
																</td>
																<td class="text-center pt-9">
																	<a href="javascript:;" class="remove-row btn btn-icon btn-danger btn-xs ml-2" data-toggle="tooltip" aria-haspopup="true" aria-expanded="false"><i class="flaticon2-cross"></i></a>
																</td>
															</tr>

															<?php
															$image_row++;
														}
													}
												}
												?>

											</tbody>
										</table>
									</div>
								</div>
									</div>
									<!--end: Wizard Step 2-->


									<!--begin: Wizard Actions-->
									<div class="d-flex justify-content-between border-top">
										<!-- <div class="mr-2">
											<button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Previous</button>
										</div>
										<div>
											<button type="button" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-submit">Submit</button>
											<button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-next">Next</button>
										</div> -->

										<div class="card-footer border-0">
											<button type="submit" class="btn btn-success">{{__('common.save')}}</button>
											<a href="{{ url('/event') }}" class="btn btn-default">{{__('common.cancel')}}</a>
										</div>

									</div>
									<!--end: Wizard Actions-->
								  {!! Form::close() !!}
								<!--end: Wizard Form-->
							</div>
						</div>
						<!--end: Wizard Body-->
					</div>
					<!--end: Wizard-->
				</div>
			</div>
		</div>
		<!--end::Container-->
	</div>
	<!--end::Entry-->










@endsection

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
  <script src="{{asset('assets/js/pages/custom/wizard/wizard-3.js')}}"></script>

@endpush


@push('js-script')

tinymce.init({
selector: '.editor',
menubar: false,
toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
plugins : 'advlist autolink link image lists charmap code table',
image_advtab: true ,
});


  $('.kt_timepicker_1').timepicker();



    $('#checkbox1').change(function() {
        if(this.checked) {
        $('.member_fee').addClass('d-none');
        }

        if(!this.checked) {
        $('.member_fee').removeClass('d-none');
        }


    });

	var image_row = <?= $image_row ?>;

	$('body').on('click', '.add-button', function(e) {
		addImage();
	});

	function addImage() {
		html  = '<tr class="append-area">';
		html += '<td><div id="images" class="form-group pb-0 mb-2 pt-3 text-center"><a href="javascript:;" data-thumbid="thumb-image'+image_row+'" data-toggle="image" class="img-thumbnail" data-content="" width="20"><img src="{{asset("assets/images/dummy-image.jpg")}}" alt="" width="75" title="" data-placeholder="http://bbg-laravel-v1.local/assets/images/dummy-image.jpg"></a><input maxlength="" data-targetid="input-image'+image_row+'" class="form-control d-none" name="events_images['+image_row+'][image]" type="text"></div></td>';
		html += '<td><div class="form-group pt-9"><input class="form-control" name="events_images[' + image_row + '][title]" type="text"> </div></td>';
		html += '<td><div class="form-group pt-9"><input class="form-control" name="events_images[' + image_row + '][description]" type="text"> </div></td>';
		html += '<td><div class="form-group pt-9"><input class="form-control" name="events_images[' + image_row + '][sort_order]" type="number"> </div></td>';
		html += '<td class="text-center pt-12"><a href="javascript:;"  class="remove-row btn btn-icon btn-danger btn-xs ml-2" data-toggle="tooltip" aria-haspopup="true" aria-expanded="false"><i class="fa flaticon2-cross"></i></a></td>';
		html += '</tr>';
		$('#events tbody').append(html);
		image_row++;
	}

	$('body').on('click', '.remove-row', function(e) {
		_this = $(this);
		swal({
			title: "{{__('common.confirmation')}}",
			html: "You wont be able to revert this!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "{{__('common.confirm')}}",
			cancelButtonText: "{{__('common.cancel')}}",
		}).then((result) => {
			if (result.value) {
				_this.parents('.append-area').remove();
			}else{
				return false;
			}
		});
	});

<!-- end delete code -->

$('.popdtpicker').datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
  orientation: "bottom left",
  todayBtn: "linked",
  clearBtn: true,
  }).on('change', function(){
      $('.datepicker').hide();
  });

	$('.button-date-select').click(function (e) {
	    $(this).parents('.date').find('.popdtpicker').datepicker('show');

	   });



@endpush

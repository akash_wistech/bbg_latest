@extends('layouts.app')
@section('title', __('event_lang::event.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'event'=> __('event_lang::event.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('event::event._form')
@endsection



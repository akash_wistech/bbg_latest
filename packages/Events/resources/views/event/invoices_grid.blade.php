<?php

$sub = \Wisdom\Event\Models\EventSubscription::where('event_id',$model->id)->get();
$subAr=[];
foreach ($sub as $key => $value) {
  $subAr[]=$value['id'];
}
if (!empty($subAr)) {
  $ad =  \DB::table('module_invoice')->whereIn('module_id',$subAr)->where('module_type','event-member-register')
  ->select('invoice_id')
  ->get()->toArray();
}
// dd($subAr);





$invoiceTypes = getInvoiceTypesListArr();
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/invoices/create', 'method'=>'post'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];
$advSearchCols = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('sales::invoices.invoice_no'),'data'=>'reference_no','name'=>'reference_no'];
$dtColsArr[]=['title'=>__('sales::invoices.invoice_date'),'data'=>'invoice_date','name'=>'invoice_date'];
if($invoiceTypes!=null){
$dtColsArr[]=['title'=>__('sales::invoices.invoice_type'),'data'=>'invoice_type','name'=>'invoice_type'];
$advSearchCols[]=['title'=>__('sales::invoices.invoice_type'),'name'=>'invoice_type','input_type'=>'select','subopts'=>$invoiceTypes];
}
$dtColsArr[]=['title'=>__('common.name'),'data'=>'name','name'=>'name'];
$dtColsArr[]=['title'=>__('common.amount'),'data'=>'grand_total','name'=>'grand_total'];
$dtColsArr[]=['title'=>__('common.payment'),'data'=>'payment','name'=>'payment'];
$advSearchCols[]=['title'=>__('common.payment'),'name'=>'payment_status','input_type'=>'select','subopts'=>getPaymentStatusArr()];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
?>



<div class="card card-custom shadow-none border-0">
  <div id="grid-table2" class="card card-body shadow-none border-0">
    <x-data-table-grid :sid="2" :request="$request" :showStats="false" :controller="'invoices'" :cbSelection="false" :exportMenu="true" :jsonUrl="'invoices/data-table-data?event_id='.$model->id" :dtColsArr="$dtColsArr" :advSearch="false" :colorCol="true" :advSrchColArr="$advSearchCols" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="generateInvoice" tabindex="-1" role="dialog" aria-labelledby="generateInvoice" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Send Invoice</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i aria-hidden="true" class="ki ki-close"></i>
				</button>
			</div>
			<div class="modal-body-send-invoice">
			</div>
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-default btn-danger" id="member_send_invoice_to_admin">Send to admin</button> -->
					<button type="button" class="btn btn-default btn-danger" id="member_send_invoice">Send Invoice</button>
					<button type="button" class="btn btn-default btn-danger" data-dismiss="modal" id="closeModalBox">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--end::Modal-->

  @push('js-script')

  var  invoiceType;
  var  invoiceid;
  $('body').on('click','.send_invoice', function(){
  var id =$(this).attr("data-id");
   invoiceType =$(this).attr("data-invoicetype");
   invoiceid =$(this).attr("data-invoiceid");
  data={invoiceType:invoiceType,invoiceid:invoiceid};

  console.log(invoiceType);
    		$.ajax({
          url:"<?= url('/invoices/send-invoice/')?>"+"/"+id,
          type:"POST",
          dataType: "html",
          data: {
            "_token": "<?= csrf_token() ?>",
            data: data,
         },
         success:function (data) {
         	$(".modal-body-send-invoice").html(data);
     		}
  });

  });


  $("body").on("click", "#member_send_invoice", function (e) {
  var id =$('.send_invoice').attr("data-id");
      $.ajax({
          url: "/invoices/send-invoice-email",
          type: "POST",
          data: {company_id:id,invoiceType:invoiceType,invoiceid:invoiceid},
          success: function (res) {
               Swal.fire("Done!", "Email has sent successfully.", "success");
          },

      });
  });

  @endpush

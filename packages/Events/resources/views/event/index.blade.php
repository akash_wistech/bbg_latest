<?php

$date = date("Y-m-d");

$btnsList = [];

if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/event/create', 'method'=>'post'];
}


$dtColsArr = [];

$advSearchCols=[];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
$dtColsArr[]=['title'=>__('event_lang::event.venue'),'data'=>'venue','name'=>'venue'];
// $dtColsArr[]=['title'=>__('event_lang::event.type'),'data'=>'type','name'=>'type'];
$dtColsArr[]=['title'=>__('event_lang::event.event_startDate'),'data'=>'event_startDate','name'=>'event_startDate'];
$dtColsArr[]=['title'=>__('event_lang::event.event_endDate'),'data'=>'event_endDate','name'=>'event_endDate'];
$dtColsArr[]=['title'=>__('event_lang::event.registration_start'),'data'=>'registration_start','name'=>'registration_start'];
$dtColsArr[]=['title'=>__('event_lang::event.registration_end'),'data'=>'registration_end','name'=>'registration_end'];
$dtColsArr[]=['title'=>__('event_lang::event.cancellation_tillDate'),'data'=>'cancellation_tillDate','name'=>'cancellation_tillDate'];
$dtColsArr[]=['title'=>__('event_lang::event.event_startTime'),'data'=>'event_startTime','name'=>'event_startTime'];
$dtColsArr[]=['title'=>__('event_lang::event.event_endTime'),'data'=>'event_endTime','name'=>'event_endTime'];
// $dtColsArr[]=['title'=>__('event_lang::event.member_fee'),'data'=>'member_fee','name'=>'member_fee'];
// $dtColsArr[]=['title'=>__('event_lang::event.nonmember_fee'),'data'=>'nonmember_fee','name'=>'nonmember_fee'];
$dtColsArr[]=['title'=>__('event_lang::event.address'),'data'=>'address','name'=>'address'];

$dtColsArr[]=['title'=>__('event_lang::event.active'),'data'=>'active','name'=>'active'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

 $dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
?>

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('event_lang::event.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('event_lang::event.heading')
]])
@endsection


@section('content')


<?php
use NaeemAwan\ModuleSubscription\Models\Subscription;
use App\Models\Company;

use Wisdom\Event\Models\Event;

$events = Event::whereNull('deleted_at')
// ->where('visibility',1)
->count();

$essential = Subscription::where('membership_type_id',1)->count();
$business = Subscription::where('membership_type_id',2)->count();
$business_advance = Subscription::where('membership_type_id',3)->count();
$moduleTypeId='';

 ?>

          <!--begin::Entry-->
          <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
              <div class="card card-custom card-transparent mt-5">
                <div class="card-body p-0">
                  <!--begin: Wizard-->
                  <div class="wizard wizard-4" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="true">
                    <!--begin: Wizard Nav-->
                    <div class="wizard-nav">
                      <div class="wizard-steps">
                        <!--begin::Wizard Step 1 Nav-->
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                          <div class="wizard-wrapper" data-wizard-type="action-prev">
                            <div class="wizard-label">
                              <div class="wizard-title" >Upcoming Events</div>
                            </div>
                          </div>
                        </div>
                        <!--end::Wizard Step 1 Nav-->
                        <!--begin::Wizard Step 2 Nav-->
                        <div class="wizard-step" data-wizard-type="step">
                          <div class="wizard-wrapper" data-wizard-type="action-next">
                            <div class="wizard-label">
                              <div class="wizard-title">Past Events</div>
                            </div>
                          </div>
                        </div>
                        <!--end::Wizard Step 2 Nav-->


                      </div>
                    </div>
                    <!--end: Wizard Nav-->
                    <!--begin: Wizard Body-->
                    <div class="card card-custom card-shadowless rounded-top-0">
                      <div class="card-body p-0">
                        <div class="row justify-content-center">
                          <div class="col-xl-12 col-xxl-12 py-5">
                            <!--begin: Wizard Form-->
                            <form class="form" id="kt_form">
                              <!--begin: Wizard Step 1-->
                              <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <div class="card card-custom shadow-none ">
                                  <div id="grid-table1" class="card card-body border-0">
                                    <x-data-table-grid :sid="1" :request="$request" :controller="'event'" :cbSelection="false" :exportMenu="true" :jsonUrl="'event/data-table-data?Upcoming=true'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
                                  </div>
                                </div>
                              </div>
                              <!--end: Wizard Step 1-->
                              <!--begin: Wizard Step 2-->
                              <div class="pb-5" data-wizard-type="step-content">
                                <div class="card card-custom shadow-none ">
                                  <div id="grid-table2" class="card card-body border-0">
                                    <x-data-table-grid :sid="2" :request="$request" :controller="'event'" :cbSelection="false" :exportMenu="true" :jsonUrl="'event/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
                                  </div>
                                </div>
                              </div>
                              <!--end: Wizard Step 2-->
                            </form>
                            <!--end: Wizard Form-->
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--end: Wizard Bpdy-->
                  </div>
                  <!--end: Wizard-->
                </div>
              </div>
            </div>
            <!--end::Container-->
          </div>
          <!--end::Entry-->


@endsection


@push('css')
<link href="{{asset('assets/css/pages/wizard/wizard-4.css')}}" rel="stylesheet" type="text/css" />
@endpush

@push('js')
<script src="{{asset('assets/js/pages/custom/wizard/wizard-4.js')}}"></script>
@endpush

<div class="row">
    <div class="col-md-12 pt10 pb10 mb15" <?= (isset($type) && ($type == 'pdf')) ? 'style="float: left;width: 100%;margin: 10;"' : '' ?> >
        <br>
        <div class="memberBadge" style="margin: 5px auto;border: solid 1px #ccc;width: 446px;">
          <img src="<?= public_path('images/logo.png') ?>" style="float:left; text-align:left" alt="BBG"/>

            <div class="row" style="text-align: center;">
                <div class="col-md-12 mt20 mb20">
                  <h1 class="text-blue pt20 pb20" <?= (isset($type) && ($type == 'pdf')) ? 'style="color: #1e3560 !important;"' : '' ?> >


                      <?php
                      if ($detail->user_type=='member') {
                          echo $detail->user->name;
                      }
                      if ($detail->user_type=='guest') {
                            echo $detail->guest->full_name;
                      }

                       ?></h1>
                      <?php
                      $companyName = '-';

                      if ($detail->user_type=='member') {
                          $companyName = $detail->company->title;
                      }
                      if ($detail->user_type=='guest') {
                          $companyName = $detail->guest->company_name	;
                      }
                      ?>

                    <h3 class="text-blue pt20 pb20" style="color: #1e3560 !important;">
                      <?= $companyName; ?>
                    </h3>
                    <h2 style="color:  #be2030">
                      <?= ($detail->user_type == 'member') ? 'Member' : "Guest"; ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>

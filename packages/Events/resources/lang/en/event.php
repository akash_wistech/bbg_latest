<?php
return [
  'heading' => 'Event',
  'name' => 'User Name',
  'first_name' => 'First Name',
  'last_name' => 'Last Name',
  'title' => 'Title',
  'description' => 'Description',


  'saved' => 'Event saved successfully',
  'notsaved' => 'List not saved',
  'suboptcount' => 'Sub Options',

  'sector' => 'Sector',
  'fax' => 'Fax',
  'postal_code' => 'Postal Code',
  'emirates_number' => 'Emirate',
  'url' => 'Website Url',
  'vat_number' => 'Vat Number',
  'address' => 'Address',
  'show_in_directory' => 'Show In Directory',
  'is_active' => 'Status',
  'about_company' => 'About Company',
  'yes' => 'Yes',
  'no' => 'No',
  'saved' => 'Event saved successfully',

  'type' => 'Event Type',
  'venue' => 'venue',
  'event_startDate' => 'Start Date',
  'event_endDate' => 'End Date',
  'registration_start' => 'Registration Start',
  'registration_end' => 'Registration End',
  'cancellation_tillDate' => 'Cancellation Till Date',
  'event_startTime' => 'Event Start Time',
  'event_endTime' => 'Event End Time',
  'member_fee' => 'Member Fee',
  'nonmember_fee' => 'Non Member Fee',
  'active' => 'Status',


];

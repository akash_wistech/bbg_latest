<?php
return [
  'confirm_generate_invoice' => 'Are you sure you want to generate invoice for this?',
  'invoice_generated' => 'Invoice',
  'no_event_found' => 'Event not found!',
];

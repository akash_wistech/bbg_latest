<?php

use Wisdom\Membership\Models\MembershipType;
use Wisdom\Company\Models\Company;
use Wisdom\Event\Models\EventImages;
use Wisdom\Event\Models\EventFee;
use Wisdom\Event\Models\Event;
use Carbon\Carbon;
use Wisdom\Event\Models\EventSubscription;
use Wisdom\News\Models\News;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Sales\Models\Invoices;

if (!function_exists('getEventStatus')) {
  /**
  * return status array
  */
  function getEventStatus()
  {
    return [
      '' => '<span class="label label-inline label-light-danger font-weight-bold">In Active</span>',
      0 => '<span class="label label-inline label-light-danger font-weight-bold">In Active</span>',
      '1' => '<span class="label label-inline label-light-success font-weight-bold">Active</span>',
    ];
  }
}



if (!function_exists('getEventType')) {
  /**
  * return status Event Type
  */
  // function getEventType()
  // {
  //   return
  // }
}


if (!function_exists('getEventRows')) {
  function getEventRows($id=null)
  {
    return EventImages::where(['event_id' => $id])->get()->toArray();
  }
}

if (!function_exists('getMemberOnCompanyId')) {
  function getMemberOnCompanyId($id=null)
  {

    dd($id);

  }
}
if (!function_exists('getdietoptiondescription')) {

  function getdietoptiondescription()
  {
    return [
      'Standard' => 'Standard',
      'Vegetarian' => 'Vegetarian',
      'other' => 'Other Dietary',
    ];
  }
}


if (!function_exists('getVaccinated')) {
  /**
  * return status array
  */
  function getVaccinated()
  {
    return [
      '' => '<span class="label label-inline label-light-danger font-weight-bold">Not Vaccinated</span>',
      '0' => '<span class="label label-inline label-light-danger font-weight-bold">Not Vaccinated</span>',
      '1' => '<span class="label label-inline label-light-success font-weight-bold">Vaccinated</span>',
    ];
  }
}


if (!function_exists('getUserTypeRelation')) {
  /**
  * return status array
  */
  function getUserTypeRelation()
  {
    return [
      'member'=>'Member',
      'guest'=>'Pre-Member',
      'committee'=>'AB',
      // 'focus-chair'=>'Focus Chair',
      'platinum-sponsor'=>'Annual/Key Partner',
      // 'honourary'=>'Honorary',
      'speaker'=>'Speaker',
      'speakers-guest'=>'Speaker\'s Guest',
      'VIP'=>'VIP',
      'business-associate'=>'Business Associate',
      'foc-multiplier'=>'FOC Multiplier',
      'foc-other'=>'FOC Other'
    ];
  }
}



if (!function_exists('getUserTypeRelationFeeFields')) {
  /**
  * return fields for fees
  */
  function getUserTypeRelationFeeFields()
  {
    return [
      'member'=>'Member Fee',
      'guest'=>'Non Member Fee',
      'committee'=>'Committee Fee',
      'honourary'=>'Honorary Fee',
      'platinum-sponsor'=>'Sponsor Fee',
      'focus-chair'=>'Focus Chair Fee',
    ];
  }
}

if (!function_exists('getEventFeeByGuestType')) {
  /**
  * return fields for fees
  */
  function getEventFeeByGuestType($id,$type)
  {
    $fee=0;
    $result = EventFee::where(['event_id'=>$id,'guest_type_id'=>$type])->first();
    if($result!=null){
      $fee = $result->fee;
    }
    return $fee;
  }
}

if (!function_exists('getEventFeesByUserTypesArr')) {
  /**
  * return fields for fees
  */
  function getEventFeesByUserTypesArr($id)
  {
    $arr = [];
    $results = EventFee::where(['event_id'=>$id])->get();
    if($results!=null){
      foreach($results as $result){
        $arr[$result->guest_type_id] = $result->fee;
      }
    }
    return $arr;
  }
}
//
// if (!function_exists('generateInvoiceForEvent')) {
//   /**
//   * return fields for fees
//   */
//   function generateInvoiceForEvent($eventId,$type)
//   {
//     $fee=0;
//     $result = EventFee::where(['event_id'=>$id,'guest_type_id'=>$type])->first();
//     if($result!=null){
//       $fee = $result->fee;
//     }
//     return $fee;
//   }
// }

if (!function_exists('vatInclusive')) {
  function vatInclusive($amount)
  {
    if ($amount >0) {
      return round(((5/100) * $amount)  + $amount,2);
    }
  }
}

// Get Events by filters such as event-joined,
if (!function_exists('getTotalEvents')) {
  function getTotalEvents($company_id=0,$attended=null)
  {
    // use \Wisdom\Event\Models\Event;
    $query = \Wisdom\Event\Models\Event::whereNull('events.deleted_at');
    if (($company_id!=0 && $company_id!=null) && $attended==null ) {
      $query =  $query->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')->whereNull('event_subscriptions.deleted_at')
        ->where('event_subscriptions.company_id',$company_id);
         }
     if (($company_id!=0 && $company_id!=null) && $attended==1 ) {
       $query =  $query->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')->whereNull('event_subscriptions.deleted_at')
         ->where('event_subscriptions.company_id',$company_id)->where('event_subscriptions.attended',$attended);
          }
   return $Events =  collect($query->select([DB::raw('Year(`event_startDate`) as year'),DB::raw('Count(*) As Total_Rows')])
    ->groupBy(DB::raw('Year(`event_startDate`)'))->orderBy('year','DESC')->limit(3)->get()->toArray())->pluck('Total_Rows','year')->all();
  }
}



// Get Events by filters such as event-joined against user,
if (!function_exists('getTotalEventsByUser')) {
  function getTotalEventsByUser($user_id=0,$attended=null)
  {
    // use \Wisdom\Event\Models\Event;
    $query = \Wisdom\Event\Models\Event::whereNull('events.deleted_at');
    if (($user_id!=0 && $user_id!=null) && $attended==null ) {
      $query =  $query->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')->whereNull('event_subscriptions.deleted_at')
        ->where('event_subscriptions.user_id',$user_id);
         }
     if (($user_id!=0 && $user_id!=null) && $attended==1 ) {
       $query =  $query->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')->whereNull('event_subscriptions.deleted_at')
         ->where('event_subscriptions.user_id',$user_id)->where('event_subscriptions.attended',$attended);
          }
   return $Events =  collect($query->select([DB::raw('Year(`event_startDate`) as year'),DB::raw('Count(*) As Total_Rows')])
    ->groupBy(DB::raw('Year(`event_startDate`)'))->orderBy('year','DESC')->limit(3)->get()->toArray())->pluck('Total_Rows','year')->all();
  }
}

// Get joined Events by filters year by user
if (!function_exists('getJoinedSumByUser')) {
  function getJoinedSumByUser($user_id,$year)
  {
    $event_s_ids = \Wisdom\Event\Models\EventSubscription::join('events', 'event_subscriptions.event_id', '=', 'events.id')->whereNull('event_subscriptions.deleted_at')
    ->where('event_subscriptions.user_id',$user_id)->select('event_subscriptions.id')->whereYear('events.event_startDate', '=', $year);
    $event_s_inv_ids = \Wisdom\Sales\Models\ModuleInvoice::where('module_type','event-member-register')->whereIn('module_id',$event_s_ids)->select('invoice_id');
    $event_s_paysum = \Wisdom\Sales\Models\PaymentItems::where('module_type','invoice')->whereIn('module_id',$event_s_inv_ids);
    $event_s_paysum = collect($event_s_paysum->select([DB::raw(''.$year.' As year'),DB::raw('sum(payment_items.amount_paid) As Total_Rows')])->get()->toArray())->pluck('Total_Rows','year')->all();

    if ($event_s_paysum[$year]==null) {
      return 0;
    }
     return $event_s_paysum[$year];
  }
}





// Get joined Events by filters year
if (!function_exists('getJoinedSum')) {
  function getJoinedSum($company_id,$year)
  {
    $event_s_ids = \Wisdom\Event\Models\EventSubscription::join('events', 'event_subscriptions.event_id', '=', 'events.id')->whereNull('event_subscriptions.deleted_at')
    ->where('event_subscriptions.company_id',$company_id)->select('event_subscriptions.id')->whereYear('events.event_startDate', '=', $year);
    $event_s_inv_ids = \Wisdom\Sales\Models\ModuleInvoice::where('module_type','event-member-register')->whereIn('module_id',$event_s_ids)->select('invoice_id');
    $event_s_paysum = \Wisdom\Sales\Models\PaymentItems::where('module_type','invoice')->whereIn('module_id',$event_s_inv_ids);
    $event_s_paysum = collect($event_s_paysum->select([DB::raw(''.$year.' As year'),DB::raw('sum(payment_items.amount_paid) As Total_Rows')])->get()->toArray())->pluck('Total_Rows','year')->all();

    if ($event_s_paysum[$year]==null) {
      return 0;
    }
     return $event_s_paysum[$year];
  }
}


// Get uploaded News by filters year
if (!function_exists('getNewsByYear')) {
  function getNewsByYear($company_id,$year)
  {
    $query = News::where(function($query) use ($company_id){
      $query->where(function($query) use ($company_id){
        $query
        // ->where('related_to','member')
        ->where('company_id',$company_id);
      })
      ->orWhere(function($query) use($company_id){
        $query->where('related_to','member')
        ->whereIn('rel_id',function($query) use($company_id){
          $query->select('module_id')->from(with(new ModuleCompany)->getTable())
          ->where('module_type','contact')->where('company_id', $company_id);
        });
      });
    })
    ->where(function($query) use($year){
      $query->whereYear('created_at',$year);
    })
    ->count('id');
    return $query;
  }
}


// Get getPaymentsByYear
if (!function_exists('getPaymentsByYear')) {
  function getPaymentsByYear($type=[1,3])
  {
    $year_in_payments = \Wisdom\Sales\Models\Payments::select([\DB::raw('Year(`payment_date`) as year')])
    ->whereNotNull('payment_date')->groupBy(\DB::raw('Year(`payment_date`)'))->get()->pluck('year');
    $invoices_id = \Wisdom\Sales\Models\Invoices::select('id')->whereIn('invoice_type',$type);
    $event_s_paysum = \Wisdom\Sales\Models\PaymentItems::where('module_type','invoice')->whereIn('module_id',$invoices_id)->select('payment_id');

    $months=['12','11', '10', '9', '8', '7', '6', '5', '4', '3','2','1'];
    foreach ($year_in_payments as $key => $year) {

      for ($i=12; $i >0 ; $i--) {
      $event_s_pay = \Wisdom\Sales\Models\Payments::whereIn('id',$event_s_paysum)
      ->select('amount_received')
      ->whereNotNull('payment_date')
      ->whereYear('payment_date', '=', $year)
      ->whereMonth('payment_date', '=', $i)
      ->sum('amount_received');
      $PaymentByMonth[$year][]=(int)$event_s_pay;
    }
    }

    return $PaymentByMonth;
  }
}


// Get getPaymentsByYear
if (!function_exists('getPaymentYear')) {
  function getPaymentYear()
  {

return \Wisdom\Sales\Models\Payments::select([\DB::raw('Year(`payment_date`) as year')])
->whereNotNull('payment_date')->groupBy(\DB::raw('Year(`payment_date`)'))->get()->pluck('year');
}
}


// Event Registeration Type
if (!function_exists('EventRegisterationType')) {
    function EventRegisterationType()
    {
      return [
        'myself'=> 'Add Myself',
        'contactofmycompany'=> 'Add member/business associate from my company',
        'contactofothercompany'=> 'Add member from another company ',
        'addguest'=> 'Add Pre-member guest',
       ];
    }
  }




// Add Registration Type
if (!function_exists('GetInvoiceIdByEventSubscribe')) {
  function GetInvoiceIdByEventSubscribe($event)
  {
      $Invoice_id =ModuleInvoice::where(['module_type'=>$event->moduleTypeId,'module_id'=>$event->id])->first();
      if ($Invoice_id) {
        $invoice =Invoices::where(['id'=>$Invoice_id['invoice_id']])->first();
        return $invoice;
      }
      return null;

}
}











//

<?php
namespace Wisdom\Event\Http\Controllers;

use Wisdom\Event\Models\Event;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Event\Models\EventSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Auth;
use App\Models\User;
use PDF; // at the top of the file

class EventInvoiceController extends Controller
{
  public function generateInvoice(Request $request)
  {
    if($request->has('eid')){
      $event = $this->findEventModel($request->input('eid'));
      if($event!=null){
        $moduleTypeId = $request->input('module');
        $moduleId = $request->input('id');
        $guestType = $request->input('guest_type');
        $guestModuleId = $request->input('guest_id');
        $company_id = 0;
        if($request->has('cid') && $request->input('cid')!='')$company_id = $request->input('cid');
        if($moduleTypeId!='' && $moduleId>0){
          $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$moduleTypeId,'module_id'=>$moduleId])
          ->first();
          if($moduleInvoiceRow==null){
            $invoice = new Invoices;
            $invoice->company_id = $company_id;
            $invoice->user_id = $moduleId;
            $invoice->invoice_type = 2;
            $invoice->p_invoice_date = date("Y-m-d");
            $invoice->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($invoice->p_invoice_date))));
            $invoice->currency_id = getSetting('currency');
            $invoice->is_recurring = 0;
            $invoice->discount_calculation = 'aftertax';
            $item_type = [];
            $item_id = [];
            $title = [];
            $descp = [];
            $qty = [];
            $rate = [];
            $discount = [];
            $discount_type = [];
            $tax_id = [];

            //Event Fee Calculation
            $eventTaxId = $event->tax_id;
            $eventFees = getEventFeesByUserTypesArr($event->id);

            $guestQuery = EventSubscription::where('event_id',$event->id);
            if($guestModuleId>0){
              $guestQuery->where('user_id',$guestModuleId)->where('user_type',$guestType);
            }
            $guests = $guestQuery->get();
            if($guests!=null){
              foreach ($guests as $guest) {
                $item_type[] = 'event';
                $item_id[] = $guest->id;
                $title[] = 'Invitation for '.$guest->guest_name;
                $descp[] = '';
                $qty[] = 1;
                $rate[] = $eventFees[$guest->user_type_relation];
                $discount[] = 0;
                $discount_type[] = 'percentage';
                $tax_id[] = $eventTaxId;
              }
            }

            $invoice->item_type = $item_type;
            $invoice->item_id = $item_id;
            $invoice->title = $title;
            $invoice->descp = $descp;
            $invoice->qty = $qty;
            $invoice->rate = $rate;
            $invoice->discount = $discount;
            $invoice->discount_type = $discount_type;
            $invoice->tax_id = $tax_id;

            $invoice->terms_text = getInvoicesTermsContent();
            if($invoice->save()){
              $invoiceModule = new ModuleInvoice;
              $invoiceModule->module_type = $moduleTypeId;
              $invoiceModule->module_id = $moduleId;
              $invoiceModule->invoice_id = $invoice->id;
              $invoiceModule->save();

              $popHeading = __('event_lang::invoice.invoice_generated');
              return new JsonResponse(['reload'=>['modalId'=>'general-modal','url'=>url('/invoices/view-invoice/'.$invoice->id),'heading'=>$popHeading]]);
            }

          }
        }
      }else{
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('event_lang::invoice.no_event_found'),]]);
      }
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('event_lang::invoice.no_event_found'),]]);
    }
  }

  public function findEventModel($id)
  {
    return Event::where('id', $id)->first();
  }
}

<?php
namespace Wisdom\Event\Http\Controllers\FrontendControllers;


use Wisdom\Event\Models\Event;
use Wisdom\Event\Models\EventImages;
use Wisdom\Event\Models\EventSubscription;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Membership\Models\Membership;
use Wisdom\CustomeFields\Models\ModuleCustomFieldsOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Auth;
use App\Models\User;
use PDF; // at the top of the file
use App\Models\Prospect;
use Wisdom\Event\Models\EventFee;

class EventController extends Controller
{

  public function newModel()
  {
    return new Event;
  }

  public function findModel($id)
  {
    return Event::where('id', $id)->first();
  }



  public function EventsIndex(Request $request)
  {
    if ($request->has('type')) {
      $title = 'PAST EVENTS';
      $events = DB::table('events')
      ->where('event_startDate', '<=', date('Y-m-d'))
      ->where('active', '=', '1')
      ->orderBy('event_startDate', 'asc')
      ->paginate(8);
      // dd($events);
    }else{
      $title = 'Upcoming Events';
      $events = DB::table('events')
      ->where('event_startDate', '>=', date('Y-m-d'))
      ->where('active', '=', '1')
      ->orderBy('event_startDate', 'asc')
      ->paginate(8);
    }

    // dd($events);


    return view('frontend-views::event.index',compact('events','title'));
  }

  public function EventsDetails($id='')
  {
    $event = $this->findModel($id);
    // dd($event);
    return view('frontend-views::event.event_detail',compact('event'));
  }

  public function SyncToCalendar(Request $request, $event_id='')
  {
    if($request->isMethod('post')){

    }
    return view('frontend-views::event.add_to_calendar',compact('event_id'));
  }




  public function EventRegisteration(Request $request, $id='', $s='')
  {

   $event = $this->findModel($id);
   $model= new EventSubscription;


  if($request->isMethod('post')){

    $invoiceIdz = [];
    $combinedpay = [];
    $primaryContact=null;
    $guestinfo =  $request->guestinfo;
    $primaryContact=null;
    // Register Members in Event
    if ($request->memberinfo) {
      foreach ($request->memberinfo as $key => $member) {
        $member_detail=  User::where(['id'=>$member['id']])->first();
        $event_subscription = new  EventSubscription ;
        $event_subscription->user_id  = $member_detail->id;
        $event_subscription->company_id  = $member['company_id'];
          if (isset($member['invite_only']) && $member['invite_only'] ==1) {
            $event_subscription->primary_pay  = $member_detail->id;
          }else {
            if (isset($request->primary_pay) && $request->primary_pay!=null) {
                $event_subscription->primary_pay  = $request->primary_pay;
            }else {
              if (Auth::user() && Auth::user()->id !=1) {
                $event_subscription->primary_pay  = Auth::user()->id;
                $primaryContact = Auth::user()->id;
              }
            }
          }
        $event_subscription->event_id  = $id;
        $event_subscription->fee_paid  = getEventFeesValue($event->id,'member');

        if ((isset($member['focGuest']) && $member['focGuest']==1)) {
          $event_subscription->fee_paid=0;
        }
        $event_subscription->user_type = 'member';
        $event_subscription->focGuest = ((isset($member['focGuest']) && $member['focGuest'] <>null) ? $member['focGuest'] : 0);
        $event_subscription->vaccinated = ((isset($member['vaccinated']) && $member['vaccinated'] <>null) ? $member['vaccinated'] : 0);
        $event_subscription->diet_option = ((isset($member['diet_option']) && $member['diet_option'] <>null) ? $member['diet_option'] : 0);
        $event_subscription->user_type_relation  = $member['user_type_relation'];


        if (isset($member['diet_option']) && $member['diet_option'] <>null) {
          $event_subscription->diet_option_description = $member['diet_option_description'];
          if (isset($member['diet_option_description']) && $member['diet_option_description'] =='other') {
            $event_subscription->other_diet_option_description = $member['other_diet_option_description'];
          }
        }
        $event_subscription->status = 'pending';
        $event_subscription->registered_by = Auth::user()->id;
        $event_subscription->created_by = 1;
        if($event_subscription->save()){
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $event_subscription->input_field[$key]=$val;
            }
          }
          saveCustomField($event_subscription,$request);
          if (isset($member['invite_only']) && $member['invite_only'] ==1) {
              generateEventSubscriptionInvoice($event_subscription,$event);
          }else {
            $combinedpay[]= $event_subscription;
          }

        }
      }
    }




    // Register Guests in Event
    if ($guestinfo) {
      foreach ($guestinfo as $key => $guests) {
        if($guests){
          foreach ($guests as $key2 => $guest) {
            $prospect = new \App\Models\Prospect;
            $prospect->full_name = $guest['name'];
            $prospect->email = $guest['email'];
            $prospect->phone = $guest['mobile'];
            $prospect->company_name = $guest['company'];
            if($prospect->save()){

              if ($key==100) {
                $primaryContact=$prospect->id;
              }

              $event_subscription = new  EventSubscription ;
            // $event_subscription->user_id  = (($guest['user_id']!=null ? $guest['user_id'] : '') );
              $event_subscription->user_id  = $prospect->id;
              $event_subscription->designation = $guest['designation'];
              $event_subscription->user_type_relation  = $guest['user_type_relation'];
              $event_subscription->focGuest = ((isset($guest['focGuest']) && $guest['focGuest'] <>null) ? $guest['focGuest'] : 0);
              $event_subscription->vaccinated = ((isset($guest['vaccinated']) && $guest['vaccinated'] <>null) ? $guest['vaccinated'] : 0);
              $event_subscription->diet_option = ((isset($guest['diet_option']) && $guest['diet_option'] <>null) ? $guest['diet_option'] : 0);
              $event_subscription->diet_option_description = $guest['diet_option_description'];
              $event_subscription->event_id  = $id;
              $event_subscription->fee_paid  = getEventFeesValue($event->id,'guest');
              if (isset($guest['invite_only']) && $guest['invite_only'] ==1) {
              }else {
                if (isset($request->primary_pay) && $request->primary_pay!=null) {
                    $event_subscription->primary_pay = $request->primary_pay;
                }
              }

              if ( (isset($guest['focGuest']) && $guest['focGuest'])==1) {
                $event_subscription->fee_paid=0;
              }

              if (isset($guest['diet_option']) && $guest['diet_option'] <>null) {
                $event_subscription->diet_option_description = $guest['diet_option_description'];
                if (isset($guest['diet_option_description']) && $guest['diet_option_description'] =='other') {
                  $event_subscription->other_diet_option_description = $guest['other_diet_option_description'];
                }
              }
              $event_subscription->user_type = 'guest';
              $event_subscription->created_by = 1;
              $event_subscription->refered_by = $guest['user_id'];
              $event_subscription->registered_by = ((Auth::user()!=null)? Auth::user()->id : 0);
              $event_subscription->status = 'pending';
              if($event_subscription->save()){
                if($request->input_field!=null){
                  foreach($request->input_field as $key=>$val){
                    $event_subscription->input_field[$key]=$val;
                  }
                }
                saveCustomField($event_subscription,$request);
                if (isset($guest['invite_only']) && $guest['invite_only'] ==1) {
                    generateEventSubscriptionInvoice($event_subscription,$event);
                }else {
                  $combinedpay[]= $event_subscription;
                }

              }
            }
          }
        }
      }
    }


    if (isset($combinedpay)) {
        $invoiceIdz[]=generateEventSubscriptionInvoiceMultiple($combinedpay,$event,$primaryContact);
    }

    if($request->submit_type==0){
      return redirect('events/registrations')->with('success', __('Event Members has been Regitered, Please pay at your earliest to avoid any errors'));
    }else{
      if($invoiceIdz!=null){
        $orderId = generatePaymentOrder($invoiceIdz);
        return redirect('/pay-tabs/order/'.$orderId);
      }else{
        return redirect('events/event-registeration/'.$id.'/'.$s)->with('error', __('Error performing task, Please try again in a while'));
      }
    }
  }
  return view('frontend-views::.event-registeration',compact('event','model','request','id'));



}







}

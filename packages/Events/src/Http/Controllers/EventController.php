<?php
namespace Wisdom\Event\Http\Controllers;


use Wisdom\Event\Models\Event;
use Wisdom\Event\Models\EventImages;
use Wisdom\Event\Models\EventSubscription;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Membership\Models\Membership;
use Wisdom\CustomeFields\Models\ModuleCustomFieldsOption;
use NaeemAwan\ModuleSubscription\Models\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Auth;
use App\Models\User;
use App\Models\Contact;
use PDF; // at the top of the file
use App\Models\Prospect;
use App\Models\Company;

class EventController extends Controller
{

 public function newModel()
 {
  return new Event;
}

public function findModel($id)
{
  return Event::where('id', $id)->first();
}

public function delete($id)
{
  $model = $this->findModel($id);
  if ($model->delete()) {
    EventImages::where('event_id', $id)->delete();
  }
  return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $moduleTypeId = $this->newModel()->moduleTypeId;
      return view('event::event.index',compact('moduleTypeId','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $model= new Event;


      if($request->isMethod('post')){

               // dd($request->all());

        $validatedData = $request->validate([
          'title' => 'required|string',
          'venue' => 'required|string',
          'type' => 'required|string',
          'group_size' => 'required|integer',
          'short_description' => 'required|string',
          'registration_start' => 'required|date',
          'registration_end' => 'required|date',
          'event_startDate' => 'required|date',
          'event_endDate' => 'required|date',
          'cancellation_tillDate' => 'required|date',
          'description' => 'required|string',
            // 'title' => 'required|string',
            // 'phonenumber' => 'required|string',
            // 'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'is_active' => 'required|numeric',
        ]);

        $event = new Event;
        $event->title = $request->title;
        $event->type = $request->type;
        $event->venue = $request->venue;
        $event->address = $request->address;
        $event->map_address = $request->map_address;
        // $event->fb_gallery = $request->fb_gallery;
        $event->group_size = $request->group_size;
        $event->short_description = $request->short_description;
        $event->registration_start = $request->registration_start;
        $event->registration_end = $request->registration_end;
        $event->cancellation_tillDate = $request->cancellation_tillDate;
        $event->event_startDate = $request->event_startDate;
        $event->event_endDate = $request->event_endDate;
        $event->event_startTime = $request->event_startTime;
        $event->event_endTime = $request->event_endTime;
        $event->tax_id = $request->tax_id;
        $event->member_fee = $request->member_fee;
        $event->nonmember_fee = $request->nonmember_fee;
        $event->committee_fee = $request->committee_fee;
        $event->honourary_fee = $request->honourary_fee;
        $event->sponsor_fee = $request->sponsor_fee;
        $event->focus_chair_fee = $request->focus_chair_fee;
        $event->description = $request->description;
        $event->active = $request->active;
        $event->members_only = $request->members_only;
        $event->diet_option = $request->diet_option;
        $event->info_only = $request->info_only;
        $event->created_by = Auth::user()->id;
        $event->events_images=$request->events_images;
        $event->image = $request->image;
        $event->fees = $request->fees;
        $event->expense = $request->expense;

        if ($event->save()) {
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $event->input_field[$key]=$val;
            }
          }
          saveCustomField($event,$request);

          if (isset($request->events) && $request->events !=null) {
           ModuleCustomFieldsOption::where('module_type','event-member-register')->where('module_id',$event->id)->delete();
           foreach ($request->events as $key => $value) {
            $module_custom_fields_option = new  ModuleCustomFieldsOption;
            $module_custom_fields_option->module_type = 'event-member-register';
            $module_custom_fields_option->module_id =$event->id;
            $module_custom_fields_option->custom_field_id =$key;
            $module_custom_fields_option->save();
          }
        }
        return redirect('event')->with('success', __('event_lang::event.saved'));
      }
    }
    return view('event::event.create',['model'=>$model]);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {

     $moduleTypeId = $this->newModel()->moduleTypeId;
     $model=Event::where('id',$id)->first();


     $Registerd_members = DB::table('event_subscriptions')->where('event_id',$model->id)->whereNull('deleted_at')->count();
     $Fee_paid = DB::table('event_subscriptions')->where('event_id',$model->id)->whereNull('deleted_at')->sum('fee_paid');
     $Registerd_members_percent =  ($Registerd_members/$model->group_size)*100;

     if ($model) {
       return view('event::event.view',[
         'model'=>$model,
         'moduleTypeId'=>$moduleTypeId,
         'request'=>$request,
         'Registerd_members'=>$Registerd_members,
         'Fee_paid'=>$Fee_paid,
         'Registerd_members_percent'=>$Registerd_members_percent,

       ]);
     }
     return redirect('event')->with('error', __('Member data is not Available'));

   }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $model=Event::where('id',$id)->first();

      if($request->isMethod('post')){


          // $validatedData = $request->validate([
          //   'name' => 'required|string',
          //   'phonenumber' => 'required|string',
          //   'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          //   'is_active' => 'required|numeric',
          // ]);

          // dd($request->all());

        $event=Event::where('id',$id)->first();
        $event->title = $request->title;
        $event->type = $request->type;
        $event->venue = $request->venue;
        $event->address = $request->address;
        $event->map_address = $request->map_address;
        // $event->fb_gallery = $request->fb_gallery;
        $event->group_size = $request->group_size;
        $event->short_description = $request->short_description;
        $event->registration_start = $request->registration_start;
        $event->registration_end = $request->registration_end;
        $event->cancellation_tillDate = $request->cancellation_tillDate;
        $event->event_startDate = $request->event_startDate;
        $event->event_endDate = $request->event_endDate;
        $event->event_startTime = $request->event_startTime;
        $event->event_endTime = $request->event_endTime;
        $event->tax_id = $request->tax_id;
        $event->member_fee = $request->member_fee;
        $event->nonmember_fee = $request->nonmember_fee;
        $event->committee_fee = $request->committee_fee;
        $event->honourary_fee = $request->honourary_fee;
        $event->sponsor_fee = $request->sponsor_fee;
        $event->focus_chair_fee = $request->focus_chair_fee;
        $event->description = $request->description;
        $event->active = $request->active;
        $event->members_only = $request->members_only;
        $event->diet_option = $request->diet_option;
        $event->info_only = $request->info_only;
        $event->updated_by = Auth::user()->id;
        $event->image = $request->image;
        $event->fees = $request->fees;
        $event->expense = $request->expense;


        $event->events_images=$request->events_images;
        $event->existingId = $request->existingId;
        $event->updated_at = date('Y-m-d H:i:s', strtotime('now'));

        if ($event->save()) {


// dd($request->events);
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $event->input_field[$key]=$val;
            }
          }
          saveCustomField($event,$request);

          if (isset($request->events) && $request->events !=null) {
           ModuleCustomFieldsOption::where('module_type','event-member-register')->where('module_id',$event->id)->delete();
           foreach ($request->events as $key => $value) {
            $module_custom_fields_option = new  ModuleCustomFieldsOption;
            $module_custom_fields_option->module_type = 'event-member-register';
            $module_custom_fields_option->module_id =$event->id;
            $module_custom_fields_option->custom_field_id =$key;
            $module_custom_fields_option->save();
          }
        }








        return redirect('event')->with('success', __('event_lang::event.saved'));
      }
    }



    return view('event::event.create',['model'=>$model]);

  }




  public function datatable(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
    if ($request->has('Upcoming')) {
      if ($request->input('Upcoming')) {
        $query->whereDate('event_endDate', '>', date("Y-m-d"));
      }
    }
    else {
      $query->whereDate('event_endDate', '<', date("Y-m-d"));
    }
    // permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'membership',$query);
    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='title';
      $srchFlds[]='venue';
      $srchFlds[]='address';
      // $srchFlds[]='email';
      // $srchFlds[]='phone';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }
    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->name!='')$query->where('name','like','%'.$request->name.'%');
    // if($request->full_name!='')$query->where('full_name','like','%'.$request->full_name.'%');
    // if($request->company_name!='')$query->where('company_name','like','%'.$request->company_name.'%');
    // if($request->email!='')$query->where('email','like','%'.$request->email.'%');
    // if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
    // if($request->input_field!=''){
    //   advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    // }
    $countQuery = clone $query;
    $totalRecords = $countQuery->count('id');
    $gridViewColumns=getGridViewColumns($moduleTypeId);
    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        //Colors column
        // $colorArr = getActivityColors($moduleTypeId,$model->id);
        // $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['title']=$model->title;
        // $thisData['type']=getPredefinedListItemsArr(7)[$model->type];
        $thisData['venue']=$model->venue;
        $thisData['address']=$model->address;
        $thisData['event_startDate']=$model->event_startDate;
        $thisData['event_endDate']=$model->event_endDate;
        $thisData['registration_start']=$model->registration_start;
        $thisData['registration_end']=$model->registration_end;
        $thisData['cancellation_tillDate']=$model->cancellation_tillDate;
        $thisData['event_startTime']=$model->event_startTime;
        $thisData['event_endTime']=$model->event_endTime;
        // $thisData['member_fee']=$model->member_fee;
        // $thisData['nonmember_fee']=$model->nonmember_fee;
        $thisData['active']=getEventStatus()[$model->active];

        // $thisData['status']=getMembershipTypeStatusArr()[$model->status];
        $thisData['cb_col']='';

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        if ($createdBy) {
          $thisData['created_by']=$createdBy->name;
        }


        $actBtns=[];
        // if(checkActionAllowed('view')){
        $actBtns[]='
        <a href="'.url('/event/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-file-2"></i>
        </a>';
        // }

        // active and deative active account
        $actBtns[]='
        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon status-change" data-toggle="tooltip" title="'.(($model->active==1) ? 'Click to UnApprove' : 'Click to Approve').'" data-url="'.url('/event/changestatus',['id'=>$model->id]).'">
        <i class="'.(($model->active==1) ? 'text-primary far fa-check-square' : 'text-danger fas fa-ban').'"></i>
        </a>';


        $actBtns[]='
        <a href="'.url('/event/event-subscribe',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="Register Member" data-id="'.$model->id.'">
        <i class="far fa-user"></i>
        </a>';

        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/event/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/event/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }

        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }


    $dtInputColsArr = [];
    $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);

  }




// Get members on Company id.
  public function getEventRegisterationType(Request $request){

    $companyName='---';
    $eventId =  $request->post('data')['eventId'];
    $member_id =  $request->post('data')['member_id'];
    $memberCount =  $request->post('data')['memberCount'];
    $member =  Contact::where('id',$member_id)->first();
    $company_id =  $member->getMainCompanyIdAttribute();
    if ($company_id !=null && $company_id!=0 ) {
        $company = Company::where('id',$company_id)->first();
        if ($company) {
            $companyName =  $company->title;
        }
      }
      $frontend='';
      if (isset($request->post('data')['frontend'])) {
            $frontend =  $request->post('data')['frontend'];
      }
      $primary_pay_array=[
        'id'=>$member_id,
        'name'=>$member->name,
      ];
    $checkMemberAlreadyExists = EventSubscription::where('event_id',$eventId)->where('user_type','member')->where('user_id',$member_id)->first();
    if ($checkMemberAlreadyExists==null) {
      $model = new EventSubscription;
      $membershipType = Subscription::where('company_id',$company_id)->select('membership_type_id')->first();
      if ($membershipType) {
      $userTypesList = getUserTypeRelation();
      // selectGuestType
      $html='';
      $html.='<div class="card tr_remove card-custom position-relative my-5" id="member-'.$member->id.'" style="padding-left:30px">';
      $html.='<div class="position-absolute" style="left:0px; top:0; bottom:0; width:30px; background-color:#1d355f">';
      $html.='<h4 class="text-center mt-15 font-weight"> </h4>';
      $html.='';
      $html.='</div>';
      $html.='<div class=" flex-wrap border-0 pt-3 pb-0" style="min-height:60px;display: flex;justify-content: space-between;">';
      $html.='<h3 class="card-title align-items-start flex-column">';
      $html.='<input type="hidden" name="memberinfo['.$memberCount.'][id]" value="'.$member->id.'"  />';
      $html.='<input type="hidden" name="memberinfo['.$memberCount.'][company_id]" value="'.$company_id.'"  />';
      $html.='<span class="card-label font-weight-bolder text-dark" style="margin-left: 2rem !important;margin-right: 0.3rem !important;">'.$member->name.'</span>';
      $html.='<span class="" style="font-size: 0.925rem;color: #B5B5C3 !important;font-weight: 500 !important;   margin-top: 0.25rem !important;">'.$companyName.'('.(($membershipType!=null) ? getMembershipTypesListArr()[$membershipType->membership_type_id] : '---' ).')</span>';
      $html.='</h3>';
      $html.='<div class="card-toolbar" style="margin-left: 2rem !important;margin-right: 2rem !important;">';
      $html.='<div class="dropdown d-flex " data-toggle="tooltip" title="" data-placement="left" data-original-title="Quick actions" style="display: inline-block;">';
      $html.='<div>';
      $html.='<a href="javascript:;" class=" btn btn-light-danger remove_div font-weight-bolder mx-1  font-size-sm" style="color: #F64E60;background-color: #FFE2E5;border-color: transparent;" data-id='.$member->id.'><i class="fa fa-trash"></i></a>';
        $html.='</div>';
      // $html.='<a href="javascript:;" class="btn btn-light-primary font-weight-bolder add_guest_1 mx-2 font-size-sm" data-id="'.$member_id.'" style="color: #3699FF;background-color: #E1F0FF;border-color: transparent;">Add Guest</a>';
      $html.='<div class="d-none">';
      $html.='<a href="javascript:;" data-memberid="'.$member_id.'" class="btn btn-light-primary ml-2  font-weight-bolder navi-link  add_guest_1" data-usertype="guest"
              style="'.(($frontend=='frontend-form')?'display:block !important' : 'display:none').'">';
      $html.='Add Guest</a>';
      $html.='</div>';
      $html.='<div class="card-header px-3 border-0 pt-0 bg-white" style="display:none;">';
      // '.(($frontend=='frontend-form')?'display:none !important' : '').'
      $html.='<div class="card-toolbar">';
        $html.='<div class="dropdown dropdown-inline" data-toggle="tooltip" title="" data-placement="left" data-original-title="">';

          $html.='<a href="#" class="btn btn-light-primary  font-weight-bolder" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';

          $html.='Add Guest</a>';
          $html.='<div class="dropdown-menu dropdown-menu-md dropdown-menu-right" style="z-index:99"  x-placement="top-end">';
            $html.='<ul class="navi navi-hover">';
              if($userTypesList!=null){
                foreach($userTypesList as $key=>$val){
                  $html.='<li class="navi-item">';
                    $html.='<a href="javascript:;" data-memberid="'.$member_id.'" class="navi-link  add_guest_1" data-usertype="'.$key.'">';
                      $html.='<span class="navi-icon">';
                        $html.='<i class="flaticon2-drop"></i>';
                      $html.='</span>';
                      $html.='<span class="navi-text">'.$val.'</span>';
                    $html.='</a>';
                  $html.='</li>';
                 }}
              $html.='</ul>';
            $html.='</div>';
          $html.='</div>';
        $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      $html.='<div class="card-body py-0">';
      $html.='<div class=" parent-card" style="margin-bottom: 1.5rem !important;">';
      $html.='<div class="row align-items-center">';
      $html.='<div class="col-12">';
      $html.='<div class="row align-items-center">';
      $html.='<!-- Free of Cost Guest Checkbox -->';
      $html.='<div class="d-flex align-items-center col-xl-3 col-lg-4 col-sm-6 col-12 my-1 pr-0" style="'.(($frontend=='frontend-form')?'display:none !important' : '').'" >';
      $html.='<!--begin::Bullet-->';
      $html.='<span class="bullet bullet-bar bg-solid align-self-stretch"></span>';
      $html.='<!--end::Bullet-->';
      $html.='<!--begin::Checkbox-->';
      $html.='<label class="checkbox checkbox-lg checkbox-light-solid checkbox-inline flex-shrink-0 m-0 mx-4">';
      $html.='<input type="checkbox" name="memberinfo['.$memberCount.'][focGuest]" value="1">';
      $html.='<span></span>';
      $html.='</label>';
      $html.='<!--end::Checkbox-->';
      $html.='<!--begin::Text-->';
      $html.='<div class="d-flex flex-column flex-grow-1">';
      $html.='<p class="text-dark-75 font-weight-bold font-size-lg mb-1">Foc Guest</p>';
      $html.='</div>';
      $html.='<!--end::Text-->';
      $html.='</div>';



      $html.='<!-- Diet Option Checkbox -->';
      $html.='<div class="d-flex align-items-center col-xl-3 col-lg-4 col-sm-6 col-12 my-1 pr-0">';
      $html.='<!--begin::Bullet-->';
      $html.='<span class="bullet bullet-bar bg-solid align-self-stretch"></span>';
      $html.='<!--end::Bullet-->';
      $html.='<!--begin::Checkbox-->';
      $html.='<label class="checkbox  checkbox-lg checkbox-light-solid checkbox-inline flex-shrink-0 m-0 mx-4">';
      $html.='<input type="checkbox" class="diet-option-box" name="memberinfo['.$memberCount.'][diet_option]" value="1">';
      $html.='<span></span>';
      $html.='</label>';
      $html.='<!--end::Checkbox-->';
      $html.='<!--begin::Text-->';
      $html.='<div class="d-flex flex-column flex-grow-1">';
      $html.='<p class="text-dark-75 font-weight-bold font-size-lg mb-1">Dietary Requirement</p>';
      $html.='</div>';
      $html.='<!--end::Text-->';
      $html.='</div>';

  if (Auth::check()) {
      $html.='<!-- Invite Only Checkbox -->';
      $html.='<div class="d-flex align-items-center col-xl-3 col-lg-4 col-sm-6 col-12 my-1 pr-0">';
      $html.='<!--begin::Bullet-->';
      $html.='<span class="bullet bullet-bar bg-solid align-self-stretch"></span>';
      $html.='<!--end::Bullet-->';
      $html.='<!--begin::Checkbox-->';
      $html.='<label class="checkbox  checkbox-lg checkbox-light-solid checkbox-inline flex-shrink-0 m-0 mx-4">';
      $html.='<input type="checkbox" class="primary_pay" name="memberinfo['.$memberCount.'][invite_only]" value="1">';
      $html.='<span></span>';
      $html.='</label>';
      $html.='<!--end::Checkbox-->';
      $html.='<!--begin::Text-->';
      $html.='<div class="d-flex flex-column flex-grow-1">';
      $html.='<p class="text-dark-75 font-weight-bold font-size-lg mb-1">Invite Only</p>';
      $html.='</div>';
      $html.='<!--end::Text-->';
      $html.='</div>';
    }
      $html.='<div class="col-xl-3 col-lg-4 col-sm-6 col-12 my-4 my-md-0" style="'.(($frontend=='frontend-form')?'display:none !important' : '').'">';
      $html.='<select class="form-control" id="user_type_relation" name="memberinfo['.$memberCount.'][user_type_relation]">';
      $html.='<option value="" selected="selected">Select User Type</option>';
      $userTypesList = getUserTypeRelation();
      if($userTypesList!=null){
        foreach($userTypesList as $key=>$val){
          $html.='<option value="'.$key.'"'.($key=='member' ? ' selected="selected"' : '').'>'.$val.'</option>';
        }
      }

      $html.='</select>';
      $html.='</div>';
      $html.='<div class="col-xl-3 col-lg-4 col-sm-6 col-12 my-4 diet-option-description-select"  style="display:none;">';
      $html.='<select class="form-control diet-select-options " name="memberinfo['.$memberCount.'][diet_option_description]"><option value="" selected="selected">Select Dietry Option</option><option value="Standard">Standard</option><option value="Vegetarian">Vegetarian</option><option value="other">Other Dietary</option></select>';
      $html.='</div>';
      $html.='<div class="col-md col-12 mt-5 other-diet-option-description" style="display: none;">';
      $html.='<div class="form-group">';
      $html.='<textarea class="form-control" placeholder="Write Description" rows="3" name="memberinfo['.$memberCount.'][other_diet_option_description] cols="50"></textarea>';
      $html.='</div>';
      $html.='</div>';
      $html.=getCustomFieldsMultiple($model,$isSub=false,$card=false,$eventId);

      $html.='</div>';
      $html.='</div>';
      $html.='</div>';
      $html.='</div> ';

      $html.='<div class="add_guest_here_1 col-12 px-0">';
      $html.='</div> ';
      $html.='</div>';
      $html.='<!--end::Body-->';
      $html.='</div>';
      return[
        'html'=>$html,
        'primary_pay_array'=>$primary_pay_array,
        'status'=>'success',
        'msg'=>'Member has Added.',
      ];

    }
    return[
      'html'=>'',
      'status'=>'error',
      'msg'=>'Subscription is not valid',
    ];
  }
  return[
    'html'=>'',
    'status'=>'error',
    'msg'=>'Member has already been registered',
  ];
}
















function GetGuestForm(Request $request){

 $model= new EventSubscription;
 $frontend='';
 if (isset($request->post('data')['frontend'])) {
       $frontend =  $request->post('data')['frontend'];
 }
$userTypesList = getUserTypeRelation();
 $eventId =  $request->post('data')['eventId'];
 $GuestCount =  $request->post('data')['GuestCount'];
 $guestType =  $request->post('data')['guestType'];
 $member_id=0;
 if (isset($request->post('data')['member_id']) && $request->post('data')['member_id'] <>null) {

  $member_id= $request->post('data')['member_id'];
}

 $guestTypeProper = str_replace("-"," ",$guestType);

$html='';
$html.= '<div class="card card-custom remove_guest_1  guest-member-parent parent-card col-12" style="padding-left:60px; margin-bottom: 1.25rem !important; margin-top: 1.25rem !important; ">';
$html.= '<div class="position-absolute " style="left:0px; top:0; bottom:0; width:90px; background-color:#EEEEEE;">';
$html.= '<h4 class="text-center font-weight-bold" style="margin-top:6rem !important;">'.$userTypesList[$guestType].'</h4>';
// margin-top:8rem !important;
$html.= '</div>';
$html.= '<div class="card-body  row" style="padding-left:40px">';
$html.= '<div class="col-md-11 col-12" style="padding-left:2.5rem !important;">';
//  padding-top:2.5rem !important
$html.= '<div class="row">';
$html.= '<div class="col-xl-4 col-md-6 col-sm-12 my-1">';
$html.= '<input class="form-control" name="guestinfo['.$member_id.']['.$GuestCount.'][user_id]" type="hidden"  id="user-id" value="'.$member_id.'" />';
$html.= '<input class="form-control required" placeholder="Full Name" name="guestinfo['.$member_id.']['.$GuestCount.'][name]" type="text" id="name" required/>';
$html.= '</div>';
$html.= '<div class="col-xl-4 col-md-6 col-sm-12 my-1">';
$html.= '<input class="form-control js-user-email required" placeholder="Email" name="guestinfo['.$member_id.']['.$GuestCount.'][email]" type="text" required>';
$html.= '</div>';
$html.= '<div class="col-xl-4 col-md-6 col-sm-12 my-1">';
$html.= '<input class="form-control required" placeholder="mobile" name="guestinfo['.$member_id.']['.$GuestCount.'][mobile]" type="text" >';
$html.= '</div>';
$html.= '<div class="col-xl-4 col-md-6 col-sm-12 my-1">';
$html.= '<input class="form-control" placeholder="Company" name="guestinfo['.$member_id.']['.$GuestCount.'][company]" type="text">';
$html.= '</div>';
$html.= '<div class="col-xl-4 col-md-6 col-sm-12 my-1">';
$html.= '<input class="form-control" placeholder="Designation" name="guestinfo['.$member_id.']['.$GuestCount.'][designation]" type="text" id="designation">';
$html.= '</div>';
$html.='<div class="col-xl-4 col-md-6 col-sm-12 my-1">';
$html.='<select style="display: none;" class="form-control" name="guestinfo['.$member_id.']['.$GuestCount.'][user_type_relation]">';
$html.='<option value="">Select User Type</option>';
if($userTypesList!=null){
  foreach($userTypesList as $key=>$val){
    if ($guestType==$key) {
        $html.='<option value="'.$key.'" selected>'.$val.'</option>';
    }
    else {
      $html.='<option value="'.$key.'">'.$val.'</option>';
    }

  }
}
$html.='</select>';
$html.='</div>';
$html.= '</div>';
$html.='<div class="row">';
$html.='<div class="form-group  col-lg-4 col-sm-6 col-12 my-1" style="padding-top: 1rem !important;padding-left: 0 !important; '.(($frontend=='frontend-form')?'display:none !important' : '').'">';
$html.='<div class="checkbox-inline col-12">';
$html.='<label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary mx-2">';
$html.='<input type="checkbox" name="guestinfo['.$member_id.']['.$GuestCount.'][focGuest]" value="1">';
$html.='<span></span>';
$html.='Foc Guest';
$html.='</label>';
$html.='</div>';
$html.='</div>';
$html.='<div class="form-group col-lg-4 col-sm-6 col-12 my-1" style="padding-top: 1rem !important;padding-left: 0 !important;">';
$html.='<div class="checkbox-inline col-12">';
$html.='<label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary mx-2">';
$html.='<input class="diet-option-box mr-2" name="guestinfo['.$member_id.']['.$GuestCount.'][diet_option]" type="checkbox" value="1">';
$html.='<span></span>';
$html.='Dietary Requirement';
$html.='</label>';
$html.='</div>';
$html.='</div>';



if (Auth::check()) {
$html.='<!-- Invite Only Checkbox -->';
$html.='<div class="form-group col-lg-4 col-sm-6 col-12 my-1" style="padding-top: 1rem !important;padding-left: 0 !important;">';
$html.='<div class="checkbox-inline col-12">';
$html.='<label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary mx-2">';
$html.='<input type="checkbox" class="primary_pay mr-2" name="guestinfo['.$member_id.']['.$GuestCount.'][invite_only]" value="1">';
$html.='<span></span>';
$html.='Invite Only';
$html.='</label>';
$html.='</div>';
$html.='</div>';
}
$html.='<div class=" col-lg-4 col-sm-6 col-12 my-4 diet-option-description-select" style="display: none;">';
$html.='<div class="form-group">';
$html.='<select class="form-control diet-select-options" name="guestinfo['.$member_id.']['.$GuestCount.'][diet_option_description]"><option value="" selected="selected">Select Dietry Option...</option><option value="Standard">Standard</option><option value="Vegetarian">Vegetarian</option><option value="other">Other Dietary</option></select>';
$html.='</div>';
$html.='</div>';
$html.='<div class="col-md col-12 other-diet-option-description" style="display: none;">';
$html.='<div class="form-group">';
$html.='<textarea class="form-control " placeholder="Write Description" rows="3" name="guestinfo['.$member_id.']['.$GuestCount.'][other_diet_option_description]" cols="50"></textarea>';
$html.='</div>';
$html.='</div>';
$html.='</div>';
$html.='<div class="row">';
$html.=getCustomFieldsMultiple($model,$isSub=false,$card=false,$eventId);
$html.='</div>';
$html.='</div>';
$html.='<div class="col-md-1 col-12 text-center" >';
$html.='<div class="mt-md-10 mt-2 ml-md-0 ml-5"  style="padding-right:0 ; "><span class="btn btn-light-danger remove_guest  font-weight-bold Mybtn"><i class="fa fa-trash"></i></span>';
// margin-top:6.25rem !important;
$html.='</div>';
$html.='</div>';
$html.='</div>';
$html.='</div>';
return $html;

}




public function EventSubscribeGrid(Request $request,$id){


   $moduleTypeId ='event-member-register';
   $query = DB::table('event_subscriptions')->where('event_id',$id)->whereNull('deleted_at');
   if($request->search['value']!=''){
     $keyword = $request->search['value'];
     // $srchFlds[]='name';
      $srchFlds[]='user_type';
      $srchFlds[]='status';
     searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
   }
 if($request->created_at!=''){
   if(strpos($request->created_at," - ")){
     $query->where(function($query) use($request){
       list($start_date,$end_date)=explode(" - ",$request->created_at);
       $query->where('created_at','>=',$start_date)
       ->where('created_at','<=',$end_date);
     });
   }else{
     $query->where('created_at',$request->created_at);
   }
 }
 if($request->name!='')$query->where('name','like','%'.$request->name.'%');
         // if($request->full_name!='')$query->where('full_name','like','%'.$request->full_name.'%');
         // if($request->company_name!='')$query->where('company_name','like','%'.$request->company_name.'%');
         // if($request->email!='')$query->where('email','like','%'.$request->email.'%');
         // if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
         // if($request->input_field!=''){
         //   advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
         // }

 $countQuery = clone $query;

 $totalRecords = $countQuery->count('id');

         // $gridViewColumns=getGridViewColumns($moduleTypeId);


 $orderBy = 'id';
 if($request->ob)$orderBy = $request->ob;
 $orderByOrder = 'desc';
 if($request->obo)$orderByOrder = $request->obo;
 $models = $query->offset($request->start)
 ->limit($request->length)
 ->orderBy($orderBy,$orderByOrder)
 ->get();
 $dataArr=[];



 if($models!=null){
   foreach($models as $model){
     $thisData=[];
              // dd($model->status);
             //Colors column
             // $colorArr = getActivityColors($moduleTypeId,$model->id);
             // $thisData['cs_col']=implode("",$colorArr);

     $thisData['id']=$model->id;
     $thisData['user_type']=$model->user_type;
     $module = '';
     $moduleId = $model->user_id;

     if ($model->user_type) {

       if ($model->user_type=='member') {
         $module = 'contact';
         $user =User::where('id',$model->user_id)->select('name','email','phone')->first();
         if ($user) {
           $user = $user->toArray();
         }
         $thisData['name']=$user['name'];
         $thisData['email']=$user['email'];
         $thisData['phone']=$user['phone'];
       }
       if ($model->user_type=='guest') {
         $module = 'prospect';
         $user =Prospect::where('id',$model->user_id)->select('full_name','email','phone')->first();
         if($user){
          $user = $user->toArray();
        }
        $thisData['name']=$user['full_name'];
        $thisData['email']=$user['email'];
        $thisData['phone']=$user['phone'];
      }
    }
    $thisData['fee']=getUserFeeEventMember($model);
    $thisData['payment_status']='N/A';
    $payment_status_id=getPaymentStatusEventMember($model);
    if ($payment_status_id!=100) {
      $thisData['payment_status']=getPaymentStatusLabelArr()[$payment_status_id];
    }
    $thisData['paid_by']='--';
    if ($model->primary_pay) {
      $PaiddBy = getUserInfo($model->primary_pay);
      if ($PaiddBy) {
       $thisData['paid_by']=$PaiddBy->name;
     }
    }

    if ($payment_status_id==1) {
        $thisData['status']='approved';
    }else {
        $thisData['status']=$model->status;
    }


    // $thisData['vaccinated']=getVaccinated()[$model->vaccinated];
     //$thisData['active']=getEventStatus()[$model->active];
     // $thisData['status']=getMembershipTypeStatusArr()[$model->status];
    $thisData['cb_col']='';
             // if($gridViewColumns!=null){
             //   foreach($gridViewColumns as $gridViewColumn){
             //     $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
             //   }
             // }
    $thisData['created_at']=formatDateTime($model->created_at);
    $createdBy = getUserInfo($model->created_by);
    if ($createdBy) {
     $thisData['created_by']=$createdBy->name;
   }


   $actBtns=[];
             //Generate Invoice
   $subModule = (new EventSubscription)->moduleTypeId;
               //Check if invoice already generated
   $moduleInvoiceRow = ModuleInvoice::where(['module_type'=>$subModule,'module_id'=>$model->id])
   ->first();
   if($moduleInvoiceRow!=null){
     $invoiceId=$moduleInvoiceRow->invoice_id;
     $invoiceUrl = url('/invoices/view-invoice/'.$invoiceId);
     $actBtns[]='
     <a href="'.$invoiceUrl.'" class="btn btn-sm btn-clean btn-icon view poplink" data-toggle="tooltip" title="'.__('common.view_invoice').'" data-id="'.$model->id.'">
     <i class="text-dark-50 flaticon-doc"></i>
     </a>';
   }else{
     // $invoiceUrl = url('/event/generate-invoice?eid='.$id.'&module='.$subModule.'&id='.$model->id.'&guest_type='.$model->user_type.'&guest_id='.$model->user_id.'&cid='.$model->company_id);
     // $actBtns[]='
     // <a href="'.$invoiceUrl.'" class="btn btn-sm btn-clean btn-icon view act-confirmation" data-confirmationmsg="'.__('event_lang::invoice.confirm_generate_invoice').'" data-pjxcntr="grid-table" data-toggle="tooltip" title="'.__('common.generate_invoice').'" data-id="'.$model->id.'">
     // <i class="text-dark-50 flaticon-doc"></i>
     // </a>';
   }
             // if(checkActionAllowed('view')){
             //   $actBtns[]='
             //   <a href="'.url('/event/view',['id'=>$model->user_id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->user_id.'">
             //   <i class="text-dark-50 flaticon-file-2"></i>
             //   </a>';
             // }

   $actBtns[]='
   <a href="'.url('/event/make-badge',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="Make Bage">
   <i class="far fa-id-badge"></i>
   </a>';

             //   $actBtns[]='
             //   <a href="'.url('/event/event-subscribe',['id'=>$model->user_id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="Register Member" data-id="'.$model->user_id.'">
             //   <i class="far fa-user"></i>
             //   </a>';

   if(checkActionAllowed('update')){
     $actBtns[]='
     <a href="'.url('/event/event-subscribe/update',['id'=>$model->user_id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->user_id.'">
     <i class="text-dark-50 flaticon-edit"></i>
     </a>';
   }
             // if(checkActionAllowed('delete')){
             //   $actBtns[]='
             //   <a href="'.url('/event/delete',['id'=>$model->user_id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->user_id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
             //   <i class="text-dark-50 flaticon-delete"></i>
             //   </a>';
             // }

   $thisData['action_col']=getDatatableActionTemplate($actBtns);
   $dataArr[]=$thisData;
 }
}


$dtInputColsArr = [];
$dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
         // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
         // if($gridViewColumns!=null){
         //   foreach($gridViewColumns as $gridViewColumn){
         //     $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
         //   }
         // }
$dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
$inputArr = [
 'columns' => $dtInputColsArr,
 'draw' => (int)$request->draw,
 'length' => (int)$request->length,
 'order' => [
   ['column'=>0,'dir'=>'asc'],
 ],
 'search' => ['value'=>$request->search['value'],'regex'=>false],
 'start' => $request->start,
];

$response = [
 'draw' => (int)$request->draw,
 'input' => $inputArr,
 'recordsTotal' => $totalRecords,
 'recordsFiltered' => $totalRecords,
 'data' => $dataArr,
];
return new JsonResponse($response);






}


public function EventMemberIndex(Request $request,$id)
{
 $moduleTypeId = $this->newModel()->moduleTypeId;
 return view('event::event.register_member_index',compact('moduleTypeId','request','id'));
}





public function EventSubscribeUpdate(Request $request,$user_id){



 $model = EventSubscription::where('user_id',$user_id)->first();


 $moduleTypeId = $this->newModel()->moduleTypeId;

 $EventId =    $model->event_id;

 if($request->isMethod('post')){
           // dd($request->email);

   if ($request->user_type=='member') {
     $model->walkin = $request->walkin;
     $model->attended = $request->attended;
     $model->user_type_relation = $request->user_type_relation;
     $model->focGuest = ((isset($request->focGuest) && $request->focGuest <>null) ? $request->focGuest : 0);
     $model->diet_option = ((isset($request->diet_option) && $request->diet_option <>null) ? $request->diet_option : 0);
     $model->vaccinated = ((isset($request->vaccinated) && $request->vaccinated <>null) ? $request->vaccinated : 0);
     if ($request->focGuest==1) {
       $model->fee_paid=0;
     }
   }

   if ($request->user_type=='guest') {
     $guest = Prospect::where('id',$request->id)->first();
     $guest->full_name = $request->name;
     $guest->email = $request->email;
     $guest->phone = $request->mobile;
     $guest->company_name = $request->company;
     $guest->save();

     $model->walkin = $request->walkin;
     $model->attended = $request->attended;
     $model->user_type_relation = $request->user_type_relation;
     $model->focGuest = ((isset($request->focGuest) && $request->focGuest <>null) ? $request->focGuest : 0);
     $model->diet_option = ((isset($request->diet_option) && $request->diet_option <>null) ? $request->diet_option : 0);
     $model->vaccinated = ((isset($request->vaccinated) && $request->vaccinated <>null) ? $request->vaccinated : 0);
     if ($request->focGuest==1) {
       $model->fee_paid=0;
     }
   }
  // dd($request['diet_option_description']);

   if (isset($request['diet_option']) && $request['diet_option'] <>null) {
     $model->diet_option_description = $request['diet_option_description'];
     if (isset($request['diet_option_description']) && $request['diet_option_description'] =='other') {
       $model->other_diet_option_description = $request['other_diet_option_description'];
     }
   }

   if($model->save()){
    if($request->input_field!=null){
      foreach($request->input_field as $key=>$val){
        $model->input_field[$key]=$val;
      }
    }
    saveCustomField($model,$request);
  }
  return redirect('event/view/'.$model->event_id)->with('success', __('event_lang::event.deleted'));
}
return view('event::event.subscribe_event_update_form',compact('moduleTypeId','request','user_id','model','EventId'));
}








public function makeBadge($id){

 $model = EventSubscription::where('id',$id)->first();
 $view = \View::make('event::event.single-badge',['detail' => $model, 'type' => 'pdf']);
 $html_content = $view->render();

      // dd($html_content);

 PDF::SetTitle("Badges");
 PDF::SetMargins(10, 30, 10);

      // PDF::setHeaderCallback(function($pdf) {
      //
      //     $img_path = url('assets/images/wisdom-logo.jpg');
      //     // dd($logo);
      //     $pdf->Image($img_path, 5, 5, 60, 0, 'JPG', '', '', true, 150, '', false, false, 0, false, false, false);
      //
      //     $pdf->setFillColor(255,255,255);
      //     $pdf->SetFont('helvetica', 'B', 16);
      //     $pdf->Cell(0, 30, 'Wisdom Information Technology Solution LLC', 0, false, 'C', 20, 200, 1, false, '', 'M');
      //
      // });

      // PDF::setFooterCallback(function($pdf) {
      //     $footerData = 'Wisdom Information Technology Solution LLC
      //     <br>Office 12A-04 DAMAC Heights, Tecom Dubai
      //     <br>VAT Number: 10032710600003';
      //     // set color for text
      //     $pdf->setFillColor(250, 250, 250);
      //     $pdf->SetTextColor(0,0,0);
      //     $pdf->SetFont('', '', 8);
      //     $pdf->writeHTMLCell(120, '', 45, $y=283, $footerData, 0, 0, 1, true, 'C', true);
      //     $pdf->Cell(0, 10, $pdf->getAliasNumPage(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
      //
      // });
 PDF::AddPage();
 PDF::writeHTML($html_content, true, false, true, false, '');
 ob_end_clean();
      // dd($html_content);
 PDF::Output($id.'.pdf');
}




public function   CheckEmailDuplication(Request $request){

  dd($request->post('data'));

  $email =  $request->post('data');
  $RecordExists=  User::where('email',$email)->first();
  if ($RecordExists) {
    return true;
  }
  $RecordExists=  Prospect::where('email',$email)->first();
  if ($RecordExists) {
    return true;
  }
        // return [
        //   'RecordExists'=>$RecordExists
        // ];

  json_encode('aq');

}


public function EventsDetails($id='')
{
  dd($id);
}

public function changestatus(Request $request, $id)
{
  $model = $this->findModel($id);
  if ($model->active=='1') {
      $active ='0';
  }
  if ($model->active=='0') {
      $active='1';
  }
  $model->active=$active;
  if ($model->update()) {
    return new JsonResponse(['success'=>['heading'=>__('success'),'msg'=>__('Status has been changed')]]);
  }
  return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.contact.notsaved')]]);
}



// Add New function




public function EventSubscribe($id, Request $request)
{

  $event = $this->findModel($id);
  $model= new EventSubscription;
  if($request->isMethod('post')){
    $invoiceIdz = [];
    $combinedpay = [];
    $primaryContact=null;
    $guestinfo =  $request->guestinfo;
    $primaryContact= $request->primary_pay;

    // Register Members in Event
    if ($request->memberinfo) {
      foreach ($request->memberinfo as $key => $member) {
        $member_detail=  User::where(['id'=>$member['id']])->first();
        $event_subscription = new  EventSubscription ;
        $event_subscription->user_id  = $member_detail->id;
        $event_subscription->company_id  = $member['company_id'];
          if (isset($member['invite_only']) && $member['invite_only'] ==1) {
            $event_subscription->primary_pay  = $member_detail->id;
          }else {
            if (isset($request->primary_pay) && $request->primary_pay!=null) {
                $event_subscription->primary_pay  = $request->primary_pay;
            }else {
              if (Auth::user() && Auth::user()->id !=1) {
                $primaryContact = Auth::user()->id;
              }
            }
          }
        $event_subscription->event_id  = $id;
        $event_subscription->fee_paid  = $event->member_fee;

        if ((isset($member['focGuest']) && $member['focGuest']==1)) {
          $event_subscription->fee_paid=0;
        }
        $event_subscription->user_type = 'member';
        $event_subscription->focGuest = ((isset($member['focGuest']) && $member['focGuest'] <>null) ? $member['focGuest'] : 0);
        $event_subscription->vaccinated = ((isset($member['vaccinated']) && $member['vaccinated'] <>null) ? $member['vaccinated'] : 0);
        $event_subscription->diet_option = ((isset($member['diet_option']) && $member['diet_option'] <>null) ? $member['diet_option'] : 0);
        $event_subscription->user_type_relation	 = $member['user_type_relation'];
        if (isset($member['diet_option']) && $member['diet_option'] <>null) {
          $event_subscription->diet_option_description = $member['diet_option_description'];
          if (isset($member['diet_option_description']) && $member['diet_option_description'] =='other') {
            $event_subscription->other_diet_option_description = $member['other_diet_option_description'];
          }
        }
      $event_subscription->status = 'pending';
      $event_subscription->registered_by = Auth::user()->id;
        $event_subscription->created_by = Auth::user()->id;;
        if($event_subscription->save()){
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $event_subscription->input_field[$key]=$val;
            }
          }
          saveCustomField($event_subscription,$request);
          if (isset($member['invite_only']) && $member['invite_only'] ==1) {
            generateEventSubscriptionInvoice($event_subscription,$event);
        }else {
          $combinedpay[]= $event_subscription;
        }
        }
      }
    }
     // Register Guests in Event
    if ($guestinfo) {

      foreach ($guestinfo as $key => $guests) {
        if($guests){
          foreach ($guests as $key2 => $guest) {
            $prospect = new \App\Models\Prospect;
            $prospect->full_name = $guest['name'];
            $prospect->email = $guest['email'];
            $prospect->phone = $guest['mobile'];
            $prospect->company_name = $guest['company'];
            if($prospect->save()){
            if ($key==100) {
              $primaryContact=$prospect->id;
            }

              $event_subscription = new  EventSubscription ;
          // $event_subscription->user_id  = (($guest['user_id']!=null ? $guest['user_id'] : '') );
              $event_subscription->user_id  = $prospect->id;
              $event_subscription->designation = $guest['designation'];
              $event_subscription->user_type_relation	 = $guest['user_type_relation'];
              $event_subscription->focGuest = ((isset($guest['focGuest']) && $guest['focGuest'] <>null) ? $guest['focGuest'] : 0);
              $event_subscription->vaccinated = ((isset($guest['vaccinated']) && $guest['vaccinated'] <>null) ? $guest['vaccinated'] : 0);
              $event_subscription->diet_option = ((isset($guest['diet_option']) && $guest['diet_option'] <>null) ? $guest['diet_option'] : 0);
              $event_subscription->diet_option_description = $guest['diet_option_description'];
              $event_subscription->event_id  = $id;
              $event_subscription->fee_paid  = $event->nonmember_fee;
              if (isset($guest['invite_only']) && $guest['invite_only'] ==1) {
              }else {
                if (isset($request->primary_pay) && $request->primary_pay!=null) {
                    $event_subscription->primary_pay = $request->primary_pay;
                }
              }

              if ( (isset($guest['focGuest']) && $guest['focGuest'])==1) {
                $event_subscription->fee_paid=0;
              }

              if (isset($guest['diet_option']) && $guest['diet_option'] <>null) {
                $event_subscription->diet_option_description = $guest['diet_option_description'];
                if (isset($guest['diet_option_description']) && $guest['diet_option_description'] =='other') {
                  $event_subscription->other_diet_option_description = $guest['other_diet_option_description'];
                }
              }


              $event_subscription->user_type = 'guest';
              $event_subscription->created_by = 1;
              $event_subscription->refered_by = $guest['user_id'];
              $event_subscription->registered_by = ((Auth::user()!=null)? Auth::user()->id : 0);
            $event_subscription->status = 'pending';

              if($event_subscription->save()){
                if($request->input_field!=null){
                  foreach($request->input_field as $key=>$val){
                    $event_subscription->input_field[$key]=$val;
                  }
                }
                saveCustomField($event_subscription,$request);
                if (isset($guest['invite_only']) && $guest['invite_only'] ==1) {
                  generateEventSubscriptionInvoice($event_subscription,$event);
              }else {
                $combinedpay[]= $event_subscription;
              }
              }
            }
          }
        }
      }
    }
  if (isset($combinedpay)) {
      $invoiceIdz[]=generateEventSubscriptionInvoiceMultiple($combinedpay,$event,$primaryContact);
  }
    return redirect('event')->with('success', __('Event Members has been Regitered'));
  }


  return view('event::event.event-subscribe',compact('request','id','model'));



}







   //
   //  public function getMemberOnCompanyId(Request $request){
   //
   //
   //    $company_id =  $request->post('data')['company_id'];
   //    $members =  User::where('company_id',$company_id)->get();
   //
   //    dd($members);
   //
   //    $html='<option value="" selected="selected">Select</option>';
   //    foreach ($members as $key => $member) {
   //     $html.='<option value="'.$member->id.'">'.$member->first_name."  ".$member->last_name.'.</option>';
   //   }
   //   return $html;
   //
   // }


     public function getCompanyWithMembers(Request $request){
       $member_id =  $request->post('data')['memberid'];
       $member =  Contact::where('id',$member_id)->first();
       $company_id =  $member->getMainCompanyIdAttribute();
       if ($company_id) {
           $company = Company::where('id',$company_id)->first();
           if ($company) {
               $companyName =  $company->title;

               $members=getCompanyUsersListQuery($company_id,$member->moduleTypeId);
               $html='<option value="select">Select</option>';
               foreach ($members as $mem) {
                   $html.='<option value="'.$mem['id'].'">'.$mem['name'].'</option>';
               }
           }
         }
     return[
       'html'=>$html,
       'companyName'=>$companyName,
     ];
 }












}

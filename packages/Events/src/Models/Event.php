<?php

namespace Wisdom\Event\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wisdom\Event\Models\EventImages;
use Illuminate\Support\Facades\DB;


class Event extends Model
{
  use HasFactory;
  public $fees;
  public $input_field;
  public $moduleTypeId = 'events';
  public $events_images,$existingId;

  public static function boot()
  {
    parent::boot();

    self::creating(function ($model) {
    });

    self::created(function ($model) {
      $model->saveEvents($model);
    });

    self::updating(function ($model) {
      $model->saveEvents($model);
    });

    self::updated(function ($model) {
    });

    self::deleting(function ($model) {
    });

    self::deleted(function ($model) {
    });
  }

  public function saveEvents($model)
  {
    // echo "<pre>";
    // print_r($model);
    // echo "</pre>";
    // dd("working");

    $xxx = 0;
    $savedIdz = [];
    if ($model->events_images!=null) {
      foreach ($model->events_images as $key => $value) {
        if (isset($model->existingId[$xxx]) && $model->existingId[$xxx] != null) {
          //print_r($model->existingId[$xxx]); echo "<br>";
          $child = EventImages::find($model->existingId[$xxx]);
        } else {
          $child = new EventImages;
          $child->event_id = $model->id;
        }
        $child->title = $value['title'];
        $child->description = $value['description'];
        $child->image = $value['image'];
        $child->sort_order = $value['sort_order'];
        $child->save();
        $savedIdz[$key] = $child->id;
        $xxx++;
      }
    }
    EventImages::where('event_id', $model->id)->whereNotIn('id', $savedIdz)->delete();

    //Saving Fees
    if(isset($model->fees) && $model->fees!=null){
      foreach($model->fees as $key=>$val){
        $eventFeeRow=EventFee::where(['event_id'=>$model->id,'guest_type_id'=>$key])->first();
        if($eventFeeRow!=null){
          DB::table($eventFeeRow->getTable())
          ->where('event_id', $model->id)
          ->where('guest_type_id', $key)
          ->update([
            'fee' => $val,
          ]);
        }else{
          $eventFeeRow=new EventFee;
          $eventFeeRow->event_id=$model->id;
          $eventFeeRow->guest_type_id=$key;
          $eventFeeRow->fee=$val;
          $eventFeeRow->save();
        }
      }
    }

  }

  public function EventImages(){
    return $this->hasMany(EventImages::class, 'event_id', 'id');
  }
}

<?php

namespace Wisdom\Event\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Prospect;
use App\Models\Company;

class EventSubscription extends Model
{
    use HasFactory;
    public $input_field;
    public $moduleTypeId = 'event-member-register';



    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function guest()
    {
        return $this->hasOne(Prospect::class, 'id', 'user_id');
    }
    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function getGuestNameAttribute()
    {
      $name='';
      if($this->user_type=='member')$name=$this->user->name;
      if($this->user_type=='guest')$name=$this->guest->full_name;
      return $name;
    }

    public function getGuestCompanyNameAttribute()
    {
      $companyName='';
      // if($this->user_type=='member')$companyName=$this->company->title;
      // if($this->user_type=='guest')$companyName=$this->guest->company_name;
      return $companyName='Hello';
    }

    public function getGuestCompanyAddressAttribute()
    {
      $companyAddres='';
      // if($this->user_type=='member')$companyAddres=$this->company->address;
      return $companyAddres="Address";
    }

    public function getGuestViewLinkAttribute()
    {
      $link='';
      if($this->user_type=='member')$link=url('/contact/view/'.$this->user_id);
      if($this->user_type=='guest')$link=url('/prospect/view/'.$this->user_id);
      return $link;
    }
}

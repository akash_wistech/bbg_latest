<?php

namespace Wisdom\Event\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\ChildTables;

class EventFee extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'event_fee';
}

<?php

namespace Wisdom\Event\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Prospect;
use App\Models\Company;
use Wisdom\Event\Models\Event;

class EventSubscription extends Model
{
  use HasFactory;
  public $input_field;
  public $moduleTypeId = 'event-member-register';



  public function user()
  {
    return $this->hasOne(User::class, 'id', 'user_id');
  }
  public function guest()
  {
    return $this->hasOne(Prospect::class, 'id', 'user_id');
  }
  public function company()
  {
    return $this->hasOne(Company::class, 'id', 'company_id');
  }

  public function getGuestNameAttribute()
  {
    $name='';
    if($this->user_type=='guest')$name=$this->guest->full_name;
    if($this->user_type=='member') {
      $name=$this->user->name;
      if ($this->user->bill_to_name<>null) {
          $name=$this->user->bill_to_name;
      }
    }
    return $name;
  }

  public function getGuestCompanyNameAttribute()
  {
    $companyName='';
    if($this->user_type=='guest')$companyName=$this->guest->company_name;
    if($this->user_type=='member') {
        $companyName=$this->company!=null ? $this->company->title : '';
      if ($this->user->bill_to_company<>null) {
          $companyName=$this->user->bill_to_company;
      }
    }

    return $companyName;
  }

  public function getGuestCompanyAddressAttribute()
  {
    $companyAddres='';
    if($this->user_type=='member') {
        $companyAddres=$this->company!=null ? $this->company->address : '';
      if ($this->user->bill_to_address<>null) {
          $companyAddres=$this->user->bill_to_address;
      }
    }
    return $companyAddres;
  }

  public function getGuestViewLinkAttribute()
  {
    $link='';
    if($this->user_type=='member')$link=url('/contact/view/'.$this->user_id);
    if($this->user_type=='guest')$link=url('/prospect/view/'.$this->user_id);
    return $link;
  }




  public function EventData()
  {
    // dd("hello");
    return $this->hasOne(Event::class, 'id', 'event_id');
  }


}

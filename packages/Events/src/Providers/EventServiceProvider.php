<?php
namespace Wisdom\Event\Providers;

use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
		$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'event');
		$this->loadViewsFrom(__DIR__ . '/../../resources/views/frontend-views', 'frontend-views');
		$this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'event_lang');
		$this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
	}
}

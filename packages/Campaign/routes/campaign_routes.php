<?php

use Illuminate\Support\Facades\Route;

use Umair\Campaign\Http\Controllers\ListNameController;
use Umair\Campaign\Http\Controllers\ListItemController;
use Umair\Campaign\Models\ListName;

Route::middleware(['web', 'auth'])->group(function () {
	Route::get('/campaign', [Umair\Campaign\Http\Controllers\CampaignController::class,'index']);
	Route::get('/campaign/data-table-data', [Umair\Campaign\Http\Controllers\CampaignController::class,'datatableData']);
	Route::match(['get', 'post'], '/campaign/create', [Umair\Campaign\Http\Controllers\CampaignController::class,'create']);
	Route::post('/campaign/store', [Umair\Campaign\Http\Controllers\CampaignController::class,'store'])->name('campaign.store');
	Route::match(['get', 'post'], '/campaign/edit/{id}', [Umair\Campaign\Http\Controllers\CampaignController::class,'edit']);
	Route::match(['get', 'post'], '/campaign/update/{id}', [Umair\Campaign\Http\Controllers\CampaignController::class,'update'])->name('campaign.update');
	Route::post('/template/status/{id}', [Umair\Campaign\Http\Controllers\CampaignController::class,'status']);
	Route::get('/template/view/{id}', [Umair\Campaign\Http\Controllers\CampaignController::class,'show']);
	Route::post('/campaign/delete/{id}', [Umair\Campaign\Http\Controllers\CampaignController::class,'destroy']);
	Route::post('/campaign/media', [Umair\Campaign\Http\Controllers\CampaignController::class, 'store_media'])->name('campaign.store_media');

	Route::get('/smtp', [Umair\Campaign\Http\Controllers\SmtpController::class,'index']);
	Route::match(['get', 'post'], '/smtp/create', [Umair\Campaign\Http\Controllers\SmtpController::class,'create']);
	Route::post('/smtp/store', [Umair\Campaign\Http\Controllers\SmtpController::class,'store'])->name('smtp.store');
	Route::match(['get', 'post'], '/smtp/edit/{id}', [Umair\Campaign\Http\Controllers\SmtpController::class,'edit']);
	Route::match(['get', 'post'], '/smtp/update/{id}', [Umair\Campaign\Http\Controllers\SmtpController::class,'update'])->name('smtp.update');
	Route::post('/smtp/status/{id}', [Umair\Campaign\Http\Controllers\SmtpController::class,'status']);
	Route::post('/smtp/delete/{id}', [Umair\Campaign\Http\Controllers\SmtpController::class,'destroy']);

	Route::get('/template', [Umair\Campaign\Http\Controllers\TemplateController::class,'index']);
	Route::get('/template/data-table-data', [Umair\Campaign\Http\Controllers\TemplateController::class,'datatableData']);
	Route::get('/template/gjs-assets', [Umair\Campaign\Http\Controllers\TemplateController::class,'gjsAssets']);
	Route::match(['get', 'post'], '/template/create', [Umair\Campaign\Http\Controllers\TemplateController::class,'create']);
	Route::post('/template/store', [Umair\Campaign\Http\Controllers\TemplateController::class,'store'])->name('template.store');
	Route::match(['get', 'post'], '/template/edit/{id}', [Umair\Campaign\Http\Controllers\TemplateController::class,'edit']);
	Route::match(['get', 'post'], '/template/update/{id}', [Umair\Campaign\Http\Controllers\TemplateController::class,'update'])->name('template.update');
	Route::post('/template/status/{id}', [Umair\Campaign\Http\Controllers\TemplateController::class,'status']);
	Route::get('/template/view/{id}', [Umair\Campaign\Http\Controllers\TemplateController::class,'show']);
	Route::get('/template/gjs-drop-upload', [Umair\Campaign\Http\Controllers\TemplateController::class,'gjsDropUpload']);
	Route::post('/template/delete/{id}', [Umair\Campaign\Http\Controllers\TemplateController::class,'destroy']);

	Route::match(['get', 'post'], '/list/create', [ListNameController::class,'create'])->name('list.create');
	Route::match(['get', 'post'], 'list/createForm', [ListNameController::class,'createForm'])->name('list.createForm');
	Route::match(['get', 'post'], '/list/store', [ListNameController::class,'store'])->name('list.store');
	Route::match(['get', 'post'], '/list/choose', [ListNameController::class,'choose'])->name('list.choose');
	Route::match(['get', 'post'], '/list/itemstore', [ListNameController::class,'itemstore'])->name('list.itemstore');
	Route::match(['get', 'post'], '/list', [ListNameController::class,'index'])->name('list');
	Route::get('/list/data-table-data', [ListNameController::class,'datatableData']);
	Route::post('/list/status/{id}', [ListNameController::class,'status']);
	Route::match(['get', 'post'], '/list/delete/{id}', [ListNameController::class,'destroy'])->name('list.delete');
	Route::match(['get', 'post'], '/list/edit/{id}', [ListNameController::class,'edit'])->name('list.edit');
	Route::match(['get', 'post'], '/list/update/{id}', [ListNameController::class,'update'])->name('list.update');

	Route::match(['get', 'post'], '/listitem/{id}', [ListItemController::class,'index'])->name('listitem');
	Route::match(['get', 'post'], '/listitem/datatableData/{id}', [ListItemController::class,'datatableData'])->name('listitem.datatableData');
	Route::match(['get', 'post'], '/listitem/delete/{id}/{list_id}/{module_type}', [ListItemController::class,'delete'])->name('listitem.delete');
});

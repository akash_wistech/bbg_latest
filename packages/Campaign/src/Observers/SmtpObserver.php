<?php
namespace Umair\Campaign\Observers;

use Umair\Campaign\Models\Smtp;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class SmtpObserver
{
  /**
  * Handle the Smtp "created" event.
  *
  * @param  \App\Models\Smtp  $smtp
  * @return void
  */
  public function created(Smtp $smtp)
  {
    if ($smtp->id) {
      $this->saveAddInfo($smtp);
    }
  }

  /**
  * Handle the Smtp "updated" event.
  *
  * @param  \App\Models\Smtp  $smtp
  * @return void
  */
  public function updated(Smtp $smtp)
  {
    $this->saveAddInfo($smtp);
  }

  /**
  * Handle the Smtp "deleted" event.
  *
  * @param  \App\Models\Smtp  $smtp
  * @return void
  */
  public function deleted(Smtp $smtp)
  {
    //
  }

  /**
  * Handle the Smtp "restored" event.
  *
  * @param  \App\Models\Smtp  $smtp
  * @return void
  */
  public function restored(Smtp $smtp)
  {
    //
  }

  /**
  * Handle the Smtp "force deleted" event.
  *
  * @param  \App\Models\Smtp  $smtp
  * @return void
  */
  public function forceDeleted(Smtp $smtp)
  {
    //
  }

  public function saveAddInfo($smtp)
  {
    //Default Option
    if($smtp->default_status==1){
      DB::table($smtp->getTable())
      ->where('id', '!=', $smtp->id)
      ->update([
        'default_status' => 0,
      ]);
    }
  }
}

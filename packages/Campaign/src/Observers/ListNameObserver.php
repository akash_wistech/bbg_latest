<?php
namespace Umair\Campaign\Observers;

use Umair\Campaign\Models\ListName;
use Umair\Campaign\Models\ListItem;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ListNameObserver
{
  /**
  * Handle the ListName "created" event.
  *
  * @param  \App\Models\ListName  $listName
  * @return void
  */
  public function created(ListName $listName)
  {
    if ($listName->id) {
      $this->saveModuleItems($listName);
    }
  }

  /**
  * Handle the ListName "updated" event.
  *
  * @param  \App\Models\ListName  $listName
  * @return void
  */
  public function updated(ListName $listName)
  {
    $this->saveModuleItems($listName);
  }

  /**
  * Handle the ListName "deleted" event.
  *
  * @param  \App\Models\ListName  $listName
  * @return void
  */
  public function deleted(ListName $listName)
  {
    //
  }

  /**
  * Handle the ListName "restored" event.
  *
  * @param  \App\Models\ListName  $listName
  * @return void
  */
  public function restored(ListName $listName)
  {
    //
  }

  /**
  * Handle the ListName "force deleted" event.
  *
  * @param  \App\Models\ListName  $listName
  * @return void
  */
  public function forceDeleted(ListName $listName)
  {
    //
  }

  public function saveModuleItems($listName)
  {
    if($listName->module_ids!=null){
      $moduleIdz=explode(",",$listName->module_ids);
      foreach($moduleIdz as $key=>$val){
				$listItem=ListItem::where(['listname_id'=>$listName->id,'module_id'=>$val,'module_type'=>$listName->module_type])->first();
				if($listItem==null){
					$listItem=new ListItem;
					$listItem->listname_id=$listName->id;
					$listItem->module_id=$val;
					$listItem->module_type=$listName->module_type;
					$listItem->save();
				}
			}
		}
  }
}

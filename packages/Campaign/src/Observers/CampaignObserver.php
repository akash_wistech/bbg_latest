<?php
namespace Umair\Campaign\Observers;

use Umair\Campaign\Models\Campaign;
use Umair\Campaign\Models\CampaignList;
use Umair\Campaign\Models\CampaignSmtp;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CampaignObserver
{
  /**
  * Handle the Campaign "created" event.
  *
  * @param  \App\Models\Campaign  $campaign
  * @return void
  */
  public function created(Campaign $campaign)
  {
    if ($campaign->id) {
      $this->saveAddInfo($campaign);
    }
  }

  /**
  * Handle the Campaign "updated" event.
  *
  * @param  \App\Models\Campaign  $campaign
  * @return void
  */
  public function updated(Campaign $campaign)
  {
    $this->saveAddInfo($campaign);
  }

  /**
  * Handle the Campaign "deleted" event.
  *
  * @param  \App\Models\Campaign  $campaign
  * @return void
  */
  public function deleted(Campaign $campaign)
  {
    //
  }

  /**
  * Handle the Campaign "restored" event.
  *
  * @param  \App\Models\Campaign  $campaign
  * @return void
  */
  public function restored(Campaign $campaign)
  {
    //
  }

  /**
  * Handle the Campaign "force deleted" event.
  *
  * @param  \App\Models\Campaign  $campaign
  * @return void
  */
  public function forceDeleted(Campaign $campaign)
  {
    //
  }

  public function saveAddInfo($campaign)
  {
    //Save List ids
    if($campaign->list_id!=null){
      CampaignList::where('campaign_id', $campaign->id)->whereNotIn('list_id', $campaign->list_id)->delete();
      foreach ($campaign->list_id as $key=>$val) {
        if($val!=null && $val>0){
          $checkAlready = CampaignList::where(['campaign_id'=>$campaign->id,'list_id'=>$val]);
          if(!$checkAlready->exists()){
            $recipient = new CampaignList;
            $recipient->campaign_id = $campaign->id;
            $recipient->list_id = $val;
            $recipient->save();
          }
        }
      }
    }
    //Save smtp ids
    if($campaign->smtp_id!=null){
      CampaignSmtp::where('campaign_id', $campaign->id)->whereNotIn('smtp_id', $campaign->smtp_id)->delete();
      foreach ($campaign->smtp_id as $key=>$val) {
        if($val!=null && $val>0){
          $checkAlready = CampaignSmtp::where(['campaign_id'=>$campaign->id,'smtp_id'=>$val]);
          if(!$checkAlready->exists()){
            $recipient = new CampaignSmtp;
            $recipient->campaign_id = $campaign->id;
            $recipient->smtp_id = $val;
            $recipient->save();
          }
        }
      }
    }
  }
}

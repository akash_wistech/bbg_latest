<?php

namespace Umair\Campaign\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\FullTables;

class ListName extends FullTables
{
    protected $table = 'list_names';
    public $moduleTypeId = 'list';
    public $module_type,$module_ids;
    use HasFactory;



   public function user()
  {
    return $this->belongsTo(User::class,'created_by');
  }

}

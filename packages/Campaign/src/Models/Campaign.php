<?php

namespace Umair\Campaign\Models;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;

class Campaign extends FullTables
{
  use Blameable, SoftDeletes;
  public $smtp_id,$list_id;
	protected $table = 'campaign';
}

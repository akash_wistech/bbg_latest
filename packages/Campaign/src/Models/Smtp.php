<?php

namespace  Umair\Campaign\Models;

use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;

class Smtp extends FullTables
{
  use Blameable, SoftDeletes;
	protected $table = 'smtp';
}

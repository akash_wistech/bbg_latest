<?php
namespace Umair\Campaign\Models;

use App\Traits\Blameable;
use App\Models\BlameableOnlyTables;

class CampaignList extends BlameableOnlyTables
{
  use Blameable;
	protected $table = 'campaign_list';
}

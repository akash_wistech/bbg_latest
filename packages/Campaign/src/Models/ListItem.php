<?php

namespace Umair\Campaign\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListItem extends Model
{
    protected $table = 'list_items';
    public $moduleTypeId = 'listitem';
    public $timestamps = false;
    use HasFactory;
}

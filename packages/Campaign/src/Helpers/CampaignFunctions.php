<?php
use Umair\Campaign\Models\Smtp;
use Umair\Campaign\Models\Template;
use Umair\Campaign\Models\ListName;
use Umair\Campaign\Models\ListItem;
use Illuminate\Support\Facades\Storage;



if (!function_exists('getTemplate')) {
    /**
     * return status array
     */
    function getTemplate()
    {
      return Template::get()->pluck('title', 'id');
    }
}


if (!function_exists('getListNamesArr')) {
    /**
     * return status array
     */
    function getListNamesArr()
    {
      return ListName::get()->pluck('title', 'id');
    }
}

if (!function_exists('getList')) {
    /**
     * return status array
     */
    function getList()
    {
      return [
        '1' => 'List 1',
        '2' =>'List 2',
      ];
    }
}
if (!function_exists('getDefaultSmtpId')) {
    /**
     * return default smtp id
     */
    function getDefaultSmtpId()
    {
      $id = 0;
      $result = Smtp::where('status',1)->where('default_status',1)->first();
      if($result!=null){
        $id = $result->id;
      }
     return ;
    }
}
if (!function_exists('getSmtpList')) {
    /**
     * return smtp array
     */
    function getSmtpList()
    {
     return Smtp::where('status',1)->get();
    }
}

if (!function_exists('getSmtpListArr')) {
    /**
     * return smtp list array
     */
    function getSmtpListArr()
    {
      $arr = [];
      $smtpType = getSmtpType();
      $results = getSmtpList();
      if($results->isNotEmpty()){
        foreach($results as $result){
          $arr[$result->id] = $smtpType[$result->type].' - '.$result->host;
        }
      }
     return $arr;
    }
}

if (!function_exists('getSmtpType')) {
    /**
     * return smtp types
     */
    function getSmtpType()
    {
      return [
        'gmail' => 'Gmail',
        'aws' => 'AWS SES',
        'mailgun' => 'Mailgun',
        'alibaba' => 'Alibaba',
      ];
    }
}

if (!function_exists('getSmtpServiceTypesTrackingOptsArr')) {
    /**
     * Get SMTP Service Types Tracking Urls
     */
    function getSmtpServiceTypesTrackingOptsArr()
    {
      return [
        'gmail' => __('common.not_applicable'),
        'aws' => 'aws-ses/stats',
        'mailgun' => 'mail-gun/stats',
        'alibaba' => 'alibaba-mail/stats',
      ];
    }
}

if (!function_exists('getSmtpServiceTypesTrackingOptsUrlArr')) {
    /**
     * Get SMTP Service Types Tracking Urls
     */
    function getSmtpServiceTypesTrackingOptsUrlArr()
    {
      $arr = [];
      $list = getSmtpServiceTypesTrackingOptsArr();
      if($list!=null){
        foreach($list as $key=>$val){
          if($key==1){
            $arr[$key]=$val;
          }else{
            $arr[$key]=url($val);
          }
        }
      }
      return $arr;
    }
}

if (!function_exists('getEncryptionType')) {
    /**
     * return status array
     */
    function getEncryptionType()
    {
      return [
        'tls' => 'TLS',
        'ssl' =>'SSL',
      ];
    }
}


if (!function_exists('getListArr')) {
    /**
     * return status array
     */
    function getListArr()
    {

      $model = ListName::select('id','title')
      ->get()
      ->pluck('title', 'id')
      ->toArray();
      return $model;
    }
}

if (!function_exists('getCampaignSentStatus')) {
    /**
     * return status array
     */
    function getCampaignSentStatus($row)
    {

      if($row->sent==0){
        return '<span class="label label-inline label-light-warning">'.__('common.pending').'</span>';
      }
      if($row->sent==1){
        return '<span class="label label-inline label-light-success">'.__('common.sent').'</span>';
      }
      if($row->sent==2){
        return '<span class="label label-inline label-light-info">'.__('common.in_queue').'</span>';
      }
      if($row->sent==3){
        return '<span class="label label-inline label-light-primary">'.__('common.sending').'</span>';
      }
    }
}

if (!function_exists('subscribeUser')) {
    /**
     * subscribe user to mailing list
     */
    function subscribeUser($model)
    {
      //Save to mailing list.
      $mailingListId = getSetting('mailing_list_id');
      if($mailingListId>0){
        $listItem=ListItem::where(['listname_id'=>$mailingListId,'module_id'=>$model->id,'module_type'=>$model->moduleTypeId])->first();
        if($listItem==null){
          $listItem=new ListItem;
          $listItem->listname_id=$mailingListId;
          $listItem->module_id=$model->id;
          $listItem->module_type=$model->moduleTypeId;
          $listItem->save();
        }
      }
    }
}


if (!function_exists('searchModuleRows')) {
    /**
     * return query filter for parent model search
     */
    function searchModuleRows($query,$keyword)
    {
      $moduleSourceNameFieldArr = getModuleSourceNameField();
      //Get All module types.
      $moduleTypes = DB::table((new ListItem)->getTable())->select('module_type')->distinct()->get();
      if($moduleTypes->isNotEmpty()){
      $query->where(function($query) use ($moduleTypes, $keyword, $moduleSourceNameFieldArr){
        //Loop module types
        foreach($moduleTypes as $moduleType){
          //Search in parent model
          $val = $moduleType->module_type;
          $nameCol = $moduleSourceNameFieldArr[$val];
          $subQuerymodelClass = getModelClass()[$val];

            $query->orWhere(function($query) use ($val, $subQuerymodelClass, $keyword, $nameCol){
              $query->where('module_type',$val)
              ->whereIn('module_id',function($query) use ($val, $subQuerymodelClass, $keyword, $nameCol){

                $query->from((new $subQuerymodelClass)->getTable())
                ->selectRaw('id')
                ->where($nameCol, 'like', '%'.$keyword.'%')
                ->orWhere('email', 'like', '%'.$keyword.'%')
                ->orWhere('phone', 'like', '%'.$keyword.'%');
              });
            });
          }
        });
      }
    }
}




if (!function_exists('getGjsAssets')) {
  function getGjsAssets()
  {
    $jsArr = [];
    $allowedExts = [];
    foreach(getSystemAllowedImageTypes() as $key=>$val){
      $allowedExts[]='*.'.$val;
    }
    foreach (Storage::files('gjs') as $filename) {
        $jsArr[]=['src'=>getStorageFileUrl($filename)];
    }
    return json_encode($jsArr);
  }
}


if (!function_exists('getTemplatesforInvoices')) {
  function getTemplatesforInvoices()
  {
    $arr=[];
    $results = Template::get();
    if($results!=null){
      foreach($results as $result){
        $arr[$result['id']]=$result['title'];
      }
    }
    return $arr;

  }
}





















//

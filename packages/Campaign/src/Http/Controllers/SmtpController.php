<?php
namespace Umair\Campaign\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use DataTables;
use Umair\Campaign\Models\Smtp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SmtpController extends Controller
{
  public function index(){
    $results=Smtp::get();
    return view('smtp::smtp.index', compact('results'));
  }

  public function create(){
    $model =new Smtp();
    return view('smtp::smtp.create', compact('model'));
  }

  public function store(Request $request){
    $validator = Validator::make($request->all(), [
      'host'=>'required',
      'username'=>'required',
      'port'=>'required'
    ]);
    if($request->default==1){

    }

    $model= new Smtp();
    $model->type=$request->type;
    $model->host=$request->host;
    $model->username=$request->username;
    $model->password=$request->password;
    $model->port=$request->port;
    $model->encryption=$request->encryption;
    $model->mail_from=$request->mail_from;
    $model->default_status=$request->default;
    $model->status=$request->status;
    $model->save();

     return redirect('smtp')->with('success', __('campaign_langs::campaign_langs.smtp.saved'));
  }

  public function edit($id){
    $model =Smtp::find($id);
    return view('smtp::smtp.edit', compact('model'));
  }

  public function update(Request $request, $id){

    $validator = Validator::make($request->all(), [
      'host'=>'required',
      'username'=>'required',
      'port'=>'required',
      'password'=>'required'
    ]);

    $model= Smtp::find($id);
    $model->type=$request->type;
    $model->host=$request->host;
    $model->username=$request->username;
    $model->password=$request->password;
    $model->port=$request->port;
    $model->encryption=$request->encryption;
    $model->mail_from=$request->mail_from;
    $model->default_status=$request->default;
    $model->status=$request->status;
    $model->updated_at = date("Y-m-d H:i:s");
    $model->save();

     return redirect('smtp')->with('success', __('campaign_langs::campaign_langs.smtp.saved'));
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Smtp  $smtp
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Smtp::where('id', $id)->first();
  }
}

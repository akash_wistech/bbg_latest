<?php
namespace Umair\Campaign\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Validator;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use DataTables;
use App\Http\Controllers\Controller;
use Umair\Campaign\Models\Template;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
  public $folderName='campaign::templates';
  public $controllerId='template';

  public function index(Request $request){
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view('campaign::templates.index', compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;

    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='title';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->name!='')$query->where('title','like','%'.$request->name.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        $thisData['cb_col']='';

        $thisData['id']=$model->id;
        $thisData['title']=$model->title;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }


        $route = url('template/status/'.$model->id);
        if($model->status==0){
          $confrmMsg = __('common.confirmEnable');
        }else{
          $confrmMsg = __('common.confirmDisable');
        }
        $thisData['status']=getEnableDisableIconWithLink($model->status,$route,$confrmMsg,'grid-table');

        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];
        if(checkActionAllowed('view')){
          // $actBtns[]='
          // <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          // <i class="text-dark-50 flaticon-file-2"></i>
          // </a>';
        }
        // if(checkActionAllowed('edit')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        // }
        // if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        // }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  public function create(){
    $model = new Template();
    return view('campaign::templates.create', compact('model'));
  }

  public function store(Request $request){
    $validator = Validator::make($request->all(), [
      'title'=>'required',
    ]);

    $model= new Template();
    $model->title=$request->title;
    $model->descp=$request->descp;
    $model->template_css=$request->template_css;
    $model->email_body=$request->email_body;
    $model->sms_body=$request->sms_body ?? null;
    $model->status=1;
    $model->save();

    return redirect('template')->with('success', __('campaign_langs::campaign_langs.template.saved'));
  }

  public function edit($id){
    $model =Template::find($id);
    return view('template::templates.edit', compact('model'));
  }

  public function update(Request $request, $id){
    $validator = Validator::make($request->all(), [
      'title'=>'required'
    ]);

    $model= Template::find($id);
    $model->title=$request->title;
    $model->descp=$request->descp;
    $model->template_css=$request->template_css;
    $model->email_body=$request->email_body;
    $model->sms_body=$request->sms_body;
    $model->save();

    return redirect('template')->with('success', __('campaign_langs::campaign_langs.template.saved'));
  }

  public function gjsAssets(Request $request){
		$jsArr = [];
		// $folder = Yii::$app->fileHelperFunctions->gjsAssetPath['abs'].Yii::$app->user->identity->company_id;
		// $files=FileHelper::findFiles($folder);
		// if($files!=null){
		// 	foreach($files as $file){
		// 		$path_parts = pathinfo($file);
		// 		$jsArr[]=['src'=>Yii::$app->fileHelperFunctions->gjsAssetPath['rel'].Yii::$app->user->identity->company_id.'/'.$path_parts['basename']];
		// 	}
		// }
    return new JsonResponse($jsArr);
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function newModel()
  {
    return new Template;
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Template  $smtp
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Template::where('id', $id)->first();
  }
}

<?php

namespace Umair\Campaign\Http\Controllers;

use Umair\Campaign\Models\ListName;
use Umair\Campaign\Models\ListItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class ListItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function newModel()
      {
        return new ListItem;
      }


     public function index($id,Request $request)
    {
      $model = ListName::where('id',$id)->first();
        $moduleTypeId = $this->newModel()->moduleTypeId;
         return view('campaign::listitem.index',
          [
            'model'=>$model,
            'moduleTypeId'=>$moduleTypeId,
            'request'=>$request,
            'id'=>$id
          ]);

    }

    public function datatableData($id,Request $request)
    {


    $modelClassList = getModelClass();
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table('list_items')->where('listname_id',$id);

    // permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'prospect',$query);
    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      searchModuleRows($query,$keyword);
    }

    $countQuery = clone $query;
    $totalRecords = $countQuery->count('module_id');
    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'listname_id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];

    if($models!=null){
      foreach($models as $model){

        $list_id= $model->listname_id;
        $module_type=$model->module_type;

        $name = '';
        $email = '';
        $phone = '';

        $row = getModuleRow($model->module_type,$model->module_id);
        if($row!=null){
          $name = $row->full_name;
          $email = $row->email;
          $phone = $row->phone;
        }




   $model= \App\Models\Prospect::find($model->module_id);

        // $thisData=[];
        $thisData['name']=$name;
        $thisData['email']=$email;
        $thisData['phone']=$phone;


        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }


        $actBtns=[];
        // if(checkActionAllowed('create','opportunity')){
        //   $actBtns[]='
        //   <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-add-circular-button"></i>
        //   </a>';
        // }
        // if(checkActionAllowed('view')){
        //   $actBtns[]='
        //   <a href="'.url('/prospect/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-file-2"></i>
        //   </a>';
        // }
        // if(checkActionAllowed('edit')){
        //   $actBtns[]='
        //   <a href="'.url('/list/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-edit"></i>
        //   </a>';
        // }
        if(true){
          $actBtns[]='
          <a href="'.url('/listitem/delete',['id'=>$model->id,'list_id' =>$list_id,'module_type'=>$module_type]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
         $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }

    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);

    }


         public function delete($id,$list_id,$module_type ,Request $request)
    {

        $model = ListItem::where('module_id',$id)->where('listname_id',$list_id)->where('module_type',$module_type);
        if ( $model->delete()) {
             if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.prospect.saved')]]);
          }else{
            return redirect('listitem',['id'=>$list_id]);
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.prospect.notsaved'),]]);
          }else{
            return redirect('listitem',['id'=>$list_id])->with('error', __('app.prospect.notsaved'));
          }
        }
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListItem  $listItem
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListItem  $listItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ListItem $listItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ListItem  $listItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListItem $listItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListItem  $listItem
     * @return \Illuminate\Http\Response
     */

}

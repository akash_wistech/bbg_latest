<?php

namespace Umair\Campaign\Http\Controllers;


use Illuminate\Http\Request;
use Umair\Campaign\Models\ListName;
use Umair\Campaign\Models\ListItem;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class ListNameController extends Controller
{

  // 2500000055133188
  //2500000055366211
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function newModel()
  {
    return new ListName;
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\ListName  $list
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return ListName::where('id', $id)->first();
  }


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new ListName();
    return view('campaign::list.create', compact('model','request'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {

    $model = new ListName();
    $model->title= $request->title;
    if($request->has('module_type')){
      $model->module_type= $request->module_type;
    }
    if($request->has('module_ids')){
      $model->module_ids= $request->module_ids;
    }
    $model->status= 1;
    $model->created_by=\Auth::user()->id;
    $model->save();

    if ( $model->save()) {
      if($request->ajax()){
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('campaign_langs::campaign_langs.list.saved')]]);
      }else{
        return redirect('list')->with('success', __('campaign_langs::campaign_langs.list.saved'));
      }
    }else{
      if($request->ajax()){
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('campaign_langs::campaign_langs.list.notsaved'),]]);
      }else{
        return redirect('prospect')->with('error', __('campaign_langs::campaign_langs.list.notsaved'));
      }
    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\ListName  $listName
  * @return \Illuminate\Http\Response
  */
  public function edit( Request $request, $id )
  {
    $model =  ListName::find($id);
    return view('campaign::list.update', compact('model','request'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\ListName  $listName
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id )
  {

    $model = ListName::find($id);
    $model->title= $request->title;
    if ( $model->save()) {
      if($request->ajax()){
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('campaign_langs::campaign_langs.list.saved')]]);
      }else{
        return redirect('list')->with('success', __('campaign_langs::campaign_langs.list.saved'));
      }
    }else{
      if($request->ajax()){
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('campaign_langs::campaign_langs.list.notsaved'),]]);
      }else{
        return redirect('list')->with('error', __('campaign_langs::campaign_langs.list.notsaved'));
      }
    }
  }

  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;

    return view('campaign::list.index',compact('moduleTypeId','request'));

  }

  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'prospect',$query);


    $countQuery = clone $query;
    $totalRecords = $countQuery->count('id');
    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];

    if($models!=null){
      foreach($models as $model){

        // $thisData=[];
        $thisData['id']=$model->id;
        $thisData['title']=$model->title;
        $list= ListItem::where('listname_id',$model->id)->count();
        $thisData['contact']='<a href="'.url('/listitem',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view">
        '.$list.'</a>';

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }

        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;


        $route = url('list/status/'.$model->id);
        if($model->status==0){
          $confrmMsg = __('common.confirmEnable');
        }else{
          $confrmMsg = __('common.confirmDisable');
        }

        $thisData['status']=getEnableDisableIconWithLink($model->status,$route,$confrmMsg,'grid-table');
        $actBtns=[];
        // if(checkActionAllowed('create','opportunity')){
        //   $actBtns[]='
        //   <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-add-circular-button"></i>
        //   </a>';
        // }
        if(checkActionAllowed('view')){
          // $actBtns[]='
          // <a href="'.url('/list/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          // <i class="text-dark-50 flaticon-file-2"></i>
          // </a>';
        }
        if(checkActionAllowed('edit')){
          $actBtns[]='
          <a href="'.url('/list/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/list/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }



    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }



  public function createForm(Request $request)
  {
    $model = new ListName();
    return view('list::list._pop_form', compact('model','request'));
  }

  public function choose(Request $request)
  {
    $model = new ListName();
    return view('list::list.choose', compact('model','request'));
  }


  public function itemstore(Request $request)
  {
    $model = $this->findModel($request->list_id);
    $model->module_type = $request->module_type;
    $model->module_ids = $request->module_ids;
    $model->updated_at = date("Y-m-d H:i:s");
    $model->save();
    if($request->ajax()){
      return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('campaign_langs::campaign_langs.list.saved')]]);
    }else{
      return redirect('list');
    }
  }


}

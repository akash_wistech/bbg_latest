<?php
namespace Umair\Campaign\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Validator;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use DataTables;
use Umair\Campaign\Models\Campaign;
use Umair\Campaign\Models\CampaignList;
use Umair\Campaign\Models\CampaignSmtp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Image as IImage;

class CampaignController extends Controller
{
  public $folderName='campaign::campaign';
  public $controllerId='campaign';

  public function index(Request $request){
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view('campaign::campaign.index', compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;

    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='title';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->name!='')$query->where('title','like','%'.$request->name.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        $thisData['cb_col']='';

        $thisData['id']=$model->id;
        $thisData['subject']=$model->subject;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }

        $sentBy = getUserInfo($model->from_id);
        $thisData['from_id']=$sentBy->fullname;
        $thisData['distribution_date']=formatDate($model->distribution_date);
        $thisData['sent']=getCampaignSentStatus($model);

        $route = url($controllerId.'/status/'.$model->id);
        if($model->status==0){
          $confrmMsg = __('common.confirmEnable');
        }else{
          $confrmMsg = __('common.confirmDisable');
        }
        $thisData['status']=getEnableDisableIconWithLink($model->status,$route,$confrmMsg,'grid-table');

        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;

        $actBtns=[];
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('edit')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/edit',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  public function create(){
    $model= new Campaign();
    $model->subject='new campaign';
    $model->smtp_id=[getDefaultSmtpId()];
    $model->status=1;
    $model->save();
    return redirect('campaign/edit/'.$model->id)->with('success', __('campaign_langs::campaign_langs.campaign.saved'));
  }

  public function store_media(Request $request)
  {
    $allowedFileTypes=getSystemAllowedFileTypes();
    if ($request->hasFile('file')) {
      $file = $request->file('file');
      if (in_array($file->getClientOriginalExtension(),$allowedFileTypes)) {
        $content = file_get_contents($request->file('file'));
        if (preg_match('/\<\?php/i', $content)) {
        }else{

          $uploadedFileName = uploadImage($request,$request->file('file'),'newsletter');

          $url = Storage::url('newsletter/'.$uploadedFileName);
          return response()->json([
            'name' => $uploadedFileName,
            'path' => $url,
            'url' => asset($url),
          ]);
        }
      }
    }
  }

  public function edit($id){
    $model =  $this->findModel($id);
    $model->list_id = CampaignList::select('list_id')->where('campaign_id', $model->id)->get()->pluck('list_id', 'list_id')->toArray();
    $model->smtp_id = CampaignSmtp::select('smtp_id')->where('campaign_id', $model->id)->get()->pluck('smtp_id', 'smtp_id')->toArray();
    return view('campaign::campaign.edit', compact('model'));
  }

  public function update(Request $request, $id){
    $validator = Validator::make($request->all(), [
      'subject'=>'required',
      'from_id'=>'required',
      'smtp_id'=>'required'
    ]);

    $model= Campaign::find($id);
    $model->subject=$request->subject;
    $model->distribution_date=$request->distribution_date;
    $model->from_id=$request->from_id;
    $model->reply_to=$request->reply_to;
    $model->template_id=$request->template_id;
    $model->list_id=$request->list_id;
    $model->smtp_id=$request->smtp_id;
    $model->descp=$request->descp;
    $model->updated_at=date("Y-m-d H:i:s");
    $model->save();
    return redirect('campaign')->with('success', __('campaign_langs::campaign_langs.campaign.saved'));
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function newModel()
  {
    return new Campaign;
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Campaign  $smtp
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Campaign::where('id', $id)->first();
  }
}

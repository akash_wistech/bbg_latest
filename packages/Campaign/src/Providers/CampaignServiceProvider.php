<?php

namespace Umair\Campaign\Providers;

use Illuminate\Support\ServiceProvider;
use Umair\Campaign\Models\ListName;
use Umair\Campaign\Observers\ListNameObserver;
use Umair\Campaign\Models\Smtp;
use Umair\Campaign\Observers\SmtpObserver;
use Umair\Campaign\Models\Campaign;
use Umair\Campaign\Observers\CampaignObserver;

class CampaignServiceProvider extends ServiceProvider
{
	public function boot()
	{
    ListName::observe(ListNameObserver::class);
    Smtp::observe(SmtpObserver::class);
    Campaign::observe(CampaignObserver::class);
		$this->loadRoutesFrom(__DIR__.'/../../routes/campaign_routes.php');
		$this->loadViewsFrom(__DIR__.'/../../resources/views', 'campaign');
		$this->loadViewsFrom(__DIR__.'/../../resources/views', 'smtp');
		$this->loadViewsFrom(__DIR__.'/../../resources/views', 'template');
		$this->loadViewsFrom(__DIR__.'/../../resources/views', 'list');
		$this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
		 $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'campaign_langs');
	}
}

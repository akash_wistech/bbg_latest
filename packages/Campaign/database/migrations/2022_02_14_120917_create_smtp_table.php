<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smtp', function (Blueprint $table) {
            $table->id();
            $table->char('type',20)->nullable();
            $table->string('host',150)->nullable();
            $table->string('username',150)->nullable();
            $table->string('password',150)->nullable();
            $table->char('port',10)->nullable();
            $table->char('encryption',3)->nullable();
            $table->string('mail_from',150)->nullable();
            $table->integer('default_status')->default(0)->nullable()->index();
            $table->integer('status')->default(1)->nullable()->index();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smtp');
    }
}

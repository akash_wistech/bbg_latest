<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_email', function (Blueprint $table) {
          $table->integer('campaign_id')->default(0)->nullable()->index();
          $table->integer('list_id')->default(0)->nullable()->index();
          $table->char('module_type',50)->nullable()->index();
          $table->integer('module_id')->default(0)->nullable()->index();
          $table->integer('smtp_id')->default(0)->nullable()->index();
          $table->string('email',100)->nullable();
          $table->integer('status')->default(0)->nullable()->index();
          $table->date('distributed_at')->nullable();
          $table->integer('delivered')->default(0)->nullable();
          $table->integer('open')->default(0)->nullable();
          $table->integer('click')->default(0)->nullable();
          $table->integer('soft_bounced')->default(0)->nullable();
          $table->integer('hard_bounced')->default(0)->nullable();
          $table->integer('spam_complain')->default(0)->nullable();
          $table->timestamps();
          $table->timestamp('deleted_at')->nullable();
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();
          $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_email');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_list', function (Blueprint $table) {
          $table->integer('campaign_id')->default(0)->nullable()->index();
          $table->integer('list_id')->default(0)->nullable()->index();
          $table->timestamps();
          $table->integer('created_by')->nullable();
          $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_list');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignAttachmentTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('campaign_attachment', function (Blueprint $table) {
      $table->integer('campaign_id')->default(0)->nullable()->index();
      $table->string('file_title',150)->nullable();
      $table->string('file_name',150)->nullable();
      $table->timestamps();
      $table->timestamp('deleted_at')->nullable();
      $table->integer('created_by')->nullable();
      $table->integer('updated_by')->nullable();
      $table->integer('deleted_by')->nullable();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('campaign_attachment');
  }
}

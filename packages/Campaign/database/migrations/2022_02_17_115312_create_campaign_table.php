<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->id();
            $table->string('subject',100)->nullable();
            $table->integer('from_id')->default(0)->nullable();
            $table->string('reply_to',100)->nullable();
            $table->integer('template_id')->default(0)->nullable();
            $table->longText('descp')->nullable();
            $table->timestamp('distribution_date')->nullable();
            $table->integer('per_min_email')->default(0)->nullable();
            $table->integer('status')->default(0)->nullable()->index();
            $table->integer('sent')->default(0)->nullable()->index();
            $table->timestamp('sent_date_time')->nullable();
            $table->integer('delivered')->default(0)->nullable();
            $table->integer('open')->default(0)->nullable();
            $table->integer('click')->default(0)->nullable();
            $table->integer('soft_bounced')->default(0)->nullable();
            $table->integer('hard_bounced')->default(0)->nullable();
            $table->integer('spam_complain')->default(0)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}

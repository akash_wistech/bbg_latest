@extends('layouts.app')
@section('title', __('campaign_langs::campaign_langs.template.heading'))
@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'template'=>__('campaign_langs::campaign_langs.template.heading'),
  __('common.create')
]])
@endsection
@php
$formRoute = ['template.store'];
@endphp
@section('content')
@include('campaign::templates._form')
@endsection

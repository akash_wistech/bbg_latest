@extends('layouts.app')
@section('title', __('campaign_langs::campaign_langs.template.heading'))
@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'template'=>__('campaign_langs::campaign_langs.template.heading'),
  __('common.update')
]])
@endsection
@php
$formRoute = ['template.update',$model->id];
@endphp
@section('content')
@include('campaign::templates._form')
@endsection

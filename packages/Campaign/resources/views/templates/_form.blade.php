@push('css')
<link rel="stylesheet" href="{{asset('assets/plugins/custom/grapesjs/grapesjs/dist/css/grapes.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/plugins/custom/grapesjs/grapesjs-preset-newsletter/dist/grapesjs-preset-newsletter.css')}}">
@endpush
<div class="card card-custom">
  <div class="card-body">
    {!! Form::model($model, ['method' => 'POST', 'route' => $formRoute, 'files' => true, 'id' => 'email-template-form']) !!}
    <div class="form-group row w-100  mx-auto">
      {!! Form::label('title', 'Title',['class'=>'required font-size-h7  ']) !!}
      {!! Form::text('title', null, ['placeholder'=>'Enter Title', 'class'=>'form-control mb-5','id'=> 'title'],'required') !!}
    </div>
    <div class="d-none">
      {!! Form::textarea('descp', null, ['class'=>'form-control','id'=> 'template-descp'],'required') !!}
      {!! Form::textarea('template_css', null, ['class'=>'form-control','id'=> 'template-template_css'],'required') !!}
      {!! Form::textarea('email_body', null, ['class'=>'form-control','id'=> 'template-email_body'],'required') !!}
      {!! Form::textarea('sms_body', null, ['class'=>'form-control','id'=> 'sms_body'],'required') !!}
    </div>
    {!! Form::close() !!}
    <div class="form-group row w-100  mx-auto" >
      {!! Form::label('descp', 'Template Body') !!}
      <div class="good" id="gjs">{!!$model->email_body!='' ? $model->email_body : ''!!}</div>
    </div>
    <div class="row col-12 mb-5">
      <button type="button" id="ht-cs" class="btn btn-primary" onclick="$('#email-template-form').submit();">Submit</button>
    </div>
  </div>
</div>
@push('jsScripts')
var jsonAssets = new Array();

$.getJSON("{{url('template/gjs-assets')}}", function(data) {
  jsonAssets = data;
});

@if($model->descp!='')
const TemplateHtml = {
  html: `{!!$model->descp!!}`,
  css: `{!!$model->template_css!!}`,
  components: null,
  style: null,
};
@endif

var editor = grapesjs.init({
  container : "#gjs",
  noticeOnUnload: 0,
  storageManager: {type: null},
  @if($model->descp!='')
  components: TemplateHtml.components || TemplateHtml.html,
  style: TemplateHtml.style || TemplateHtml.css,
  @endif
  plugins: ["gjs-preset-newsletter","gjs-plugin-ckeditor","grapesjs-style-bg","gjs-blocks-flexbox","grapesjs-touch","grapesjs-typed"],
  pluginsOpts: {
    "gjs-preset-newsletter": {
      modalLabelImport: "Paste all your code here below and click import",
      modalLabelExport: "Copy the code and use it wherever you want",
      codeViewerTheme: "material",
      //defaultTemplate: templateImport,
      importPlaceholder: "<table class=\"main-body\" width=\"100%\"><tr class=\"row\"><td class=\"main-body-cell\"><table class=\"container\" width=\"670\" align=\"center\"><tr><td class=\"container-cell\"></td></tr></table></td></tr></table>",
      cellStyle: {
        "font-size": "12px",
        "font-weight": 300,
        "vertical-align": "top",
        color: "rgb(111, 119, 125)",
        margin: 0,
        padding: 0,
      }
    }
  },
  assetManager: {
    type: "remote",
    storageType  	: "",
    storeOnChange  : true,
    storeAfterUpload  : true,
    assets    	: {!! getGjsAssets() !!},
    contentTypeJson: true,
    uploadFile: function(e) {
      var files = e.dataTransfer ? e.dataTransfer.files : e.target.files;
      var formData = new FormData();
      for(var i in files){
        formData.append("file-"+i, files[i]) //containing all the selected images from local
      }
      $.ajax({
        url: "{{url('/template/gjs-drop-upload')}}",
        type: "POST",
        data: formData,
        contentType:false,
        crossDomain: true,
        dataType: "json",
        mimeType: "multipart/form-data",
        processData:false,
        success: function(result){
          var myJSON = [];
          $.each( result["data"], function( key, value ) {
            myJSON[key] = value;
          });
          var images = myJSON;
          editor.AssetManager.add(images); //adding images to asset manager of GrapesJS
        }
      });
    },
  },
});

//editor.DomComponents.clear();
editor.on('change:changesCount', e => {
  // Work after first edition
  // var htmlWithCss = editor.runCommand('gjs-get-inlined-html');
  var templatwHtmlWithCss = editor.runCommand("gjs-get-inlined-html");
  var templateHtml = editor.getHtml();
  var templateCss = editor.getCss();

  $("#template-descp").val(templateHtml);
  $("#template-template_css").val(templateCss);
  $("#template-email_body").val(templatwHtmlWithCss);
});

$("#email-template-form").submit(function(event) {
  event.preventDefault();

  var templatwHtmlWithCss = editor.runCommand("gjs-get-inlined-html");
  var templateHtml = editor.getHtml();
  var templateCss = editor.getCss();

  $("#template-descp").val(templateHtml);
  $("#template-template_css").val(templateCss);
  $("#template-email_body").val(templatwHtmlWithCss);

  $(this).unbind("submit").submit();
});
@endpush
@push('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.9.2/ckeditor.js"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs/dist/grapes.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-mjml/dist/grapesjs-mjml.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-preset-newsletter/dist/grapesjs-preset-newsletter.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-plugin-ckeditor/dist/grapesjs-plugin-ckeditor.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-style-bg/dist/grapesjs-style-bg.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-blocks-flexbox/dist/grapesjs-blocks-flexbox.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-touch/dist/grapesjs-touch.min.js')}}"></script>
<script src="{{asset('assets/plugins/custom/grapesjs/grapesjs-typed/dist/grapesjs-typed.min.js')}}"></script>
@endpush

<?php
$btnsList = [];

if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/prospect/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>'/prospect/import'];
}

$gridViewColumns = getGridViewColumns($moduleTypeId);


$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.full_name'),'data'=>'name','name'=>'name'];
$dtColsArr[]=['title'=>__('common.email'),'data'=>'email','name'=>'email'];
$dtColsArr[]=['title'=>__('common.phone'),'data'=>'phone','name'=>'phone'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];

?>
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('campaign_langs::campaign_langs.list.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'list'=>__('campaign_langs::campaign_langs.list.heading'),
$model->title
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="'listname'" :jsonUrl="'listitem/datatableData/'.$id" :dtColsArr="$dtColsArr" :moduleTypeId="$moduleTypeId" />
  </div>
</div>
@endsection

@extends('layouts.app')
@section('title', __('campaign_langs::campaign_langs.smtp.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'smtp'=>__('campaign_langs::campaign_langs.smtp.heading'),
  __('common.create')
]])
@endsection
@section('content')
{!! Form::model($model, ['method' => 'POST', 'route' => ['smtp.store'], 'files' => true, 'id' => 'form']) !!}
    @include('smtp::smtp._form')
    {!! Form::close() !!}
@endsection

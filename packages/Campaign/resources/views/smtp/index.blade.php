@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/smtp/create', 'method'=>'post'];
}
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', 'Smtp')

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'Smtp'
]])
@endsection

@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <div class="overlay-wrapper">
      @if($results->isNotEmpty())
      <table class="table">
        <thead>
          <tr>
            <th>Sr #</th>
            <th>Type</th>
            <th>Host</th>
            <th>User</th>
            <th>Encryption</th>
            <th>Port</th>
            <th>Default</th>
            <th>Status</th>
            <th width="100">{{__('common.action')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($results as $result)
          @php
          $route = url('smtp/status/'.$result->id);
          if($result->status==0){
            $confrmMsg = __('common.confirmEnable');
          }else{
            $confrmMsg = __('common.confirmDisable');
          }
          @endphp
          <tr id="result_id_{{ $result->id }}">
            <td>{{ $result->id }}</td>
            <td>{{ getSmtpType()[$result->type] }}</td>
            <td>{{ $result->host }}</td>
            <td>{{ $result->username }}</td>
            <td>{{ getEncryptionType()[$result->encryption] }}</td>
            <td>{{ $result->port }}</td>
            <td>{!! getYesNoLabelArr()[$result->default_status] !!}</td>
            <td>{!! getEnableDisableIconWithLink($result->status,$route,$confrmMsg,'grid-table') !!}</td>
            <td>
              <a href="{{ url('/smtp/edit',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon edit' title='{{__('common.update')}}' data-id="{{$result->id}}">
                <i class="text-dark-50 flaticon-edit"></i>
              </a>
              <a href="{{ url('/smtp/delete',['id'=>$result->id]) }}" class='btn btn-sm btn-clean btn-icon act-confirmation' title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <div class="text-center">{{ __('common.noresultfound') }}</div>
      @endif
    </div>
  </div>
</div>
@endsection

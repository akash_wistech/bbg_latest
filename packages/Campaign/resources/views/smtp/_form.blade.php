
<div class="card card-custom multi-tabs">
  <div class="card-body">
   @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('type',__('common.type')) }}
          {{ Form::select('type', [''=>__('common.select')]+getSmtpType(), null,['id'=>'smtp-type','class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('host',__('campaign_langs::campaign_langs.smtp.host')) }}
            {{ Form::text('host', null, ['class'=>'form-control', 'required']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('username',__('common.username')) }}
          {{ Form::text('username', null, ['class'=>'form-control', 'required']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('password',__('common.password')) }}
           {{ Form::text('password', null, array('id' => 'password', "class" => "form-control", 'required')) }}
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('port',__('campaign_langs::campaign_langs.smtp.port')) }}
          {{ Form::text('port', null, ['class'=>'form-control', 'required']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('encryption',__('campaign_langs::campaign_langs.smtp.encryption')) }}
            {{ Form::select('encryption', getEncryptionType(),null ,['class'=>'form-control']) }}
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('mail_from',__('common.mail_from')) }}
          {{ Form::text('mail_from', null, ['class'=>'form-control', 'required']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('default',__('common.default')) }}
            {{ Form::select('default', getYesNoArr(),$model->default_status ,['class'=>'form-control']) }}
        </div>
      </div>
    </div>
     <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('status',__('common.status')) }}
            {{ Form::select('status', getYesNoArr(),$model->status ,['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="alert alert-info">
      <strong>{{__('campaign_langs::campaign_langs.tracking_url')}}</strong>
      <br />
      <span id="trackingInfo">
        {{$model->type>0 ? getSmtpServiceTypesTrackingOptsUrlArr()[$model->type] : __('campaign_langs::campaign_langs.select_type')}}
      </span>
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/smtp') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
@push('jsScripts')
var smtpTrackingOpts={!! json_encode(getSmtpServiceTypesTrackingOptsUrlArr()) !!};

$("body").on("change", "#smtp-type", function () {
  $("#trackingInfo").html(smtpTrackingOpts[$(this).val()]);
});

@endpush

@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/campaign/create', 'method'=>'post'];
}

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.subject'),'data'=>'subject','name'=>'subject'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}
$dtColsArr[]=['title'=>__('campaign_langs::campaign_langs.campaign.from_id'),'data'=>'from_id','name'=>'from_id'];
$dtColsArr[]=['title'=>__('campaign_langs::campaign_langs.campaign.distribution_date'),'data'=>'distribution_date','name'=>'distribution_date'];
$dtColsArr[]=['title'=>__('campaign_langs::campaign_langs.campaign.sent_status'),'data'=>'sent','name'=>'sent'];
$dtColsArr[]=['title'=>__('common.status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];
@endphp
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('campaign_langs::campaign_langs.campaign.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('campaign_langs::campaign_langs.campaign.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="'campaign'" :jsonUrl="'campaign/data-table-data'" :dtColsArr="$dtColsArr" :moduleTypeId="$moduleTypeId" />
  </div>
</div>
@endsection

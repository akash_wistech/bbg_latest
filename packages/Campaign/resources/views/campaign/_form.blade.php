<div class="card card-custom multi-tabs">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('subject','Suject') }}
          {{ Form::text('subject', null,['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('distribution_date','Distribution Date') }}
          <div class="input-group date">
            {{ Form::text('distribution_date', null, ['class'=>'form-control dtpicker', 'autocomplete'=>'off']) }}
            <div class="input-group-append">
              <div class="input-group-text"><i class="la la-calendar-check-o"></i></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('from_id','Send From') }}
          {{ Form::select('from_id', getStaffMemberListArr(), null, ['class'=>'form-control', 'required']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('reply_to','Reply to Email') }}
          {{ Form::text('reply_to', null, ['class'=>'form-control', 'required']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('template_id','Template') }}
          {{ Form::select('template_id', $model->template_id ? getTemplate($model->template_id) : getTemplate(),null ,['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('list_id','List') }}
          {{ Form::select('list_id[]', getListNamesArr(),null ,['class'=>'form-control frmselect2','multiple'=>'multiple']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          {{ Form::label('descp', 'Description') }}
          <div class="tinymce">
            {!! Form::textarea('descp', null, ['class' => 'form-control textEditor tox-target', 'rows'=>10, 'placeholder' => 'Description']) !!}
          </div>
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('smtp_id','Smtp to use') }}
          {{ Form::select('smtp_id[]', getSmtpListArr(), null, ['class'=>'form-control frmselect2','multiple'=>'multiple']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        {{ Form::label('images', 'Drop Attachments Here') }}
        <div class="needsclick dropzone" id="document-dropzone">
        </div>
      </div>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
      <a href="{{ url('/smtp') }}" class="btn btn-default">{{__('common.cancel')}}</a>
    </div>
  </div>
  @push('js')
  <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
  @endpush
  @push('jsScripts')
  tinymce.init({
    selector: '.textEditor',
    menubar: false,
    toolbar: ['styleselect fontselect fontsizeselect | link image',
    'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code',
    'advlist | autolink | lists'],
    plugins : 'advlist autolink link image lists charmap code'
  });
  @endpush
  @push('jsScripts')
  $(".frmselect2").select2({
    allowClear: true,
    width: "100%",
  });
  @endpush

  @push('js')
  <script>
  var uploadedDocumentMap = {}
  $("#document-dropzone").dropzone({
    url: '{{ route('campaign.store_media') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append(
        '<div id="' + response.name + '">' +
        '<input type="hidden" name="document[]" value="' + response.name + '">' +
        '</div>'
      );
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="document[]"][value="' + name + '"]').remove()
    },
    init: function () {
      @if(isset($project) && $project->document)
      var files =
      {!! json_encode($project->document) !!}
      for (var i in files) {
        var file = files[i]
        this.options.addedfile.call(this, file)
        file.previewElement.classList.add('dz-complete')
        $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
      }
      @endif
    }
  });
  </script>
  @endpush

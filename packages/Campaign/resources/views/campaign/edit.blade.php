@extends('layouts.app')
@section('title', __('campaign_langs::campaign_langs.campaign.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'workflow'=>__('campaign_langs::campaign_langs.campaign.heading'),
__('common.update')
]])
@endsection


@section('content')
{!! Form::model($model, ['method' => 'POST', 'route' => ['campaign.update',$model->id], 'files' => true, 'id' => 'form']) !!}
@include('campaign::campaign._form')
{!! Form::close() !!}
@endsection

@extends('layouts.app')
@section('title', __('campaign_langs::campaign_langs.campaign.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'campaign'=>__('campaign_langs::campaign_langs.campaign.heading'),
  __('common.create')
]])
@endsection
@section('content')
{!! Form::model($model, ['method' => 'POST', 'route' => ['campaign.store'], 'files' => true, 'id' => 'form']) !!}
@include('campaign::campaign._form')
{!! Form::close() !!}
@endsection

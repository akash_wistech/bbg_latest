@php
$btnsList = [];

if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/list/create', 'method'=>'post'];
}

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
$dtColsArr[]=['title'=>__('common.contact'),'data'=>'contact','name'=>'contact'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}
$dtColsArr[]=['title'=>__('common.status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>115];
@endphp
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('campaign_langs::campaign_langs.list.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('campaign_langs::campaign_langs.list.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="'listname'" :jsonUrl="'list/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false"  :colorCol="true" :moduleTypeId="$moduleTypeId"  :cbSelection="false" />
  </div>
</div>
@endsection

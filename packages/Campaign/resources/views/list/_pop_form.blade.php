{!! Form::model($model, ['method' => 'POST', 'route' => ['list.store',$model->id], 'class' => 'stl-ajax-submit', 'id' => 'form']) !!}
<div id="lhitms" style="display:none;">
	{{ Form::text('module_type', null, ['id'=>'sel-module-type','class'=>'form-control']) }}
	{{ Form::textarea('module_ids', null, ['id'=>'sel-module-ids','class'=>'form-control']) }}
</div>
<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="form-group">
			{{ Form::label('title',__('common.title')) }}
			{{ Form::text('title', null, ['class'=>'form-control', 'required']) }}
		</div>
	</div>
</div>
<div class="card-footer">
	<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
	<a href="javascript:;" class="btn btn-default" data-dismiss="modal">{{__('common.cancel')}}</a>
</div>
{!! Form::close() !!}

@extends('layouts.app')
@section('title', __('campaign_langs::campaign_langs.list.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'list'=>__('campaign_langs::campaign_langs.list.heading'),
  __('common.create')
]])
@endsection
@section('content')
{!! Form::model($model, ['method' => 'POST', 'route' => ['list.store'], 'files' => true, 'id' => 'form']) !!}
@include('list::list._form')
{!! Form::close() !!}
@endsection

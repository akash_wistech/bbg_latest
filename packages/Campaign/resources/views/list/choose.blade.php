{!! Form::model($model, ['method' => 'POST', 'route' => ['list.itemstore'], 'files' => true, 'id' => 'form','class'=>'stl-ajax-submit']) !!}

<div id="lhitms" style="display:none;">
	{{ Form::text('module_type', null, ['id'=>'sel-module-type','class'=>'form-control']) }}
	{{ Form::textarea('module_ids', null, ['id'=>'sel-module-ids','class'=>'form-control']) }}
</div>

<div class="col-12 col-sm-12 px-0">
	<div class="form-group pt-3">
		{{ Form::label('list_id','Select List to save selection to',['class'=>'required']) }}
		<div class="input-group date">
			{{  Form::select('list_id',[null=>'Please Select'] + getListArr() ,'',['class'=>'form-control','required']); }}
		</div>
	</div>
</div>
<div class="card-footer">
	<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
	<a href="javascript:;" class="btn btn-default" data-dismiss="modal">{{__('common.cancel')}}</a>
</div>
{!! Form::close() !!}

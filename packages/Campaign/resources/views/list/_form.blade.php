<div class="card card-custom multi-tabs">
	<div class="card-body">
		@if ($errors->any())
		<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
			<div>{{ $error }}</div>
			@endforeach
		</div>
		@endif
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="form-group">
					{{ Form::label('title',__('common.title')) }}
					{{ Form::text('title', null, ['class'=>'form-control', 'required']) }}
				</div>
			</div>
		</div>
	</div>
	<div class="card-footer">
		<button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
		<a href="{{ url('/list') }}" class="btn btn-default">{{__('common.cancel')}}</a>
	</div>
</div>

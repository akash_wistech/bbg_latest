<?php
return [
  'mailing_list_id' => 'Mailing List',
  'heading' => 'Custom Fields',
  'search_input_type' => 'Input Type',
  'search_range_from' => 'Range Start',
  'search_range_to' => 'Range End',
  'search_range_interval' => 'Interval',

  'heading' => 'Smtp',
  'search_input_type' => 'Input Type',
  'search_range_from' => 'Range Start',
  'search_range_to' => 'Range End',
  'search_range_interval' => 'Interval',

  'tracking_url' => 'Tracking Url:',
  'select_type' => 'Select type',

  'list' => [
    'heading' => 'Lists',
    'saved' => 'List saved successfully',
    'notsaved' => 'Error while saving List',
  ],
  'smtp' => [
    'heading' => 'SMTP',
    'host' => 'Host',
    'encryption' => 'Encryption',
    'port' => 'Port',
    'saved' => 'Smtp saved successfully',
    'notsaved' => 'Error while saving Smtp',
  ],
  'template' => [
    'heading' => 'Templates',
    'saved' => 'Template saved successfully',
    'notsaved' => 'Error while saving Template',
  ],
  'campaign' => [
    'heading' => 'Campaign',
    'sent_status' => 'Sent',
    'from_id' => 'From',
    'distribution_date' => 'Distribution Date',
    'saved' => 'Campaign saved successfully',
    'notsaved' => 'Error while saving Campaign',
  ]
];

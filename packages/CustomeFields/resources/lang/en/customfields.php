<?php
return [
  'heading' => 'Custom Fields',
  'search_input_type' => 'Input Type',
  'search_range_from' => 'Range Start',
  'search_range_to' => 'Range End',
  'search_range_interval' => 'Interval',
];

@if($customeFields!=null && count($customeFields)>0)
@if($showCard==true)
<div class="card card-custom">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('customfields::customfields.heading')}}</h3>
    </div>
  </div>
  <div class="card-body p-2">
@endif
<div class="d-flex row">
	@foreach($customeFields as $customeField)

	@php
  $inputVal=null;
	if($model->id!=null){
    if(isset($mixed) && $mixed==true){
      $inputVal = getInputFielSavedValue($model,$customeField);
      if($inputVal==''){
        $inputVal = getInputFielSavedValue($model->subModel,$customeField);
      }
    }else{
  		$inputVal = getInputFielSavedValue($model,$customeField);
    }
	}
	@endphp

	@if(in_array($customeField->input_type,['text','vat_number','numberinput','phoneinput','faxinput','websiteinput','emailinput','postalcode']))


	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			@if($customeField->autocomplete_text == 1)
			@if($customeField->predefined_list_id >= 1)
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control predefind', 'placeholder'=>$customeField->hint_text, 'data-id'=>$customeField->predefined_list_id,'autocomplete'))}}
			@else
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control new_options', 'placeholder'=>$customeField->hint_text, 'autocomplete'))}}
			@endif
			@else
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'placeholder'=>$customeField->hint_text))}}
			@endif
		</div>
	</div>

	@endif
  @if($customeField->input_type == 'checkbox' || $customeField->input_type=='radio')
    @if($customeField->predefined_list_id>0)
    @php
    $selectArr=getPredefinedList($customeField->predefined_list_id);
    if($customeField->input_type=='radio'){
      $cbrbName = $inputFieldKey.'['.$customeField->id.']';
    }else{
      $cbrbName = $inputFieldKey.'['.$customeField->id.'][]';
    }
    @endphp
    <div class="col-md-{{$customeField->colum_width}}">
  		<div class="form-group">
  			<label>{{$customeField->title}}</label>
        <div class="{{$customeField->input_type}}-inline">
          @foreach($selectArr as $key=>$val)
          @php
          $checked="";
          if($customeField->input_type=='radio' && $key==$inputVal){
            $checked=' checked="checked"';
          }else{
            if(is_array($inputVal) && in_array($key,$inputVal)){
              $checked=' checked="checked"';
            }
          }
          @endphp
					<label class="{{$customeField->input_type}}">
  					<input type="{{$customeField->input_type}}" name="{{$cbrbName}}" value="{{$key}}"{{$checked}}>
  					<span></span>{{$val}}
          </label>
          @endforeach
				</div>
  		</div>
  	</div>
    @endif
  @endif


	@if(in_array($customeField->input_type,['textarea','address','profile']))
	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			{{Form::textarea($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'placeholder'=>$customeField->hint_text, 'rows'=> ((in_array($customeField->input_type,['address'])) ? '2' : '10') ))}}
		</div>
	</div>

	@endif

	@if($customeField->input_type == 'date' || $customeField->input_type == 'dob')
  @php
  $datepickerOptions = ['format'=>'yyyy-mm-dd','todayHighlight'=>true,'orientation'=>'bottom left','autoclose'=>true];

  if($customeField->dateOptions!=null){
    if($customeField->dateOptions->default_today_date==1 && $inputVal==''){
      $inputVal=date("Y-m-d");
    }
    if($customeField->dateOptions->selection_type==1){//Future
      $datepickerOptions['startDate']='-0d';
    }
    if($customeField->dateOptions->selection_type==2){//Past
      $datepickerOptions['endDate']='-0d';
    }
  }
  @endphp
	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			<div class="input-group date">
				{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('id'=>'cdtp'.$customeField->id,'class' => 'form-control','placeholder'=>$customeField->hint_text,'autocomplete'=>'off','readonly'=>'readonly'))}}
				<div class="input-group-append">
					<span class="input-group-text">
						<i class="la la-calendar-check-o"></i>
					</span>
				</div>
			</div>
		</div>
	</div>
  @push('jsScripts')
  $('#cdtp{{$customeField->id}}').datepicker({!! json_encode($datepickerOptions) !!});
  @endpush
	@endif
	@if($customeField->input_type == 'select')

	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			@if($customeField->selection_type == 2)
			@if($customeField->predefined_list_id > 0)
			{{Form::select($inputFieldKey.'['.$customeField->id.'][]',getPredefinedList($customeField->predefined_list_id),$inputVal,array('class' => 'form-control selectpicker','data-live-search'=>'true','data-selected-text-format'=>'count>2', 'multiple'=>'multiple'))}}
			@else
			{{Form::select($inputFieldKey.'['.$customeField->id.'][]',getMultipleOptions($customeField->id),$inputVal,array('class' => 'form-control selectpicker','data-live-search'=>'true','data-selected-text-format'=>'count>2', 'multiple'=>'multiple'))}}
			@endif
			@else
			@if($customeField->predefined_list_id > 0)
			{{Form::select($inputFieldKey.'['.$customeField->id.']',getPredefinedList($customeField->predefined_list_id),$inputVal,array('class' => 'form-control'))}}
			@else
			{{Form::select($inputFieldKey.'['.$customeField->id.']',getMultipleOptions($customeField->id),$inputVal,array('class' => 'form-control'))}}
			@endif
			@endif
		</div>
	</div>

	@endif
	@endforeach

</div>
@if($showCard==true)
</div>
</div>
@endif
@push('js')
<script type="text/javascript">

// var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
// $(document).ready(function(){
//   $( ".predefind" ).autocomplete({
//     source: function( request, response ) {
//       $.ajax({
//         url:"{{route('custome_field.getPredefinedList')}}",
//         type: 'get',
//         dataType: "json",
//         data: {
//            _token: CSRF_TOKEN,
//            search: request.term
//         },
//         success: function( data ) {
//            response( data );
//         }
//       });
//     },
//     select: function (event, ui) {
//        // Set selection
//        $('#user_serach').val(ui.item.label); // display the selected text
//        $('#user_id').val(ui.item.value); // save selected id to input
//        return false;
//     }
//   });
// });
</script>
@endpush
@endif

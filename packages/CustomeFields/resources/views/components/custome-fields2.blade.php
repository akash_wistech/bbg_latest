@if($customeFields!=null && count($customeFields)>0)
@if($showCard==true)
<div class="card card-custom px-4">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('customfields::customfields.heading')}}</h3>
    </div>
  </div>
  <div class="card-body p-2">
@endif



	@foreach($customeFields as $customeField)
	@php
  $inputVal=null;
	if($model->id!=null){
    if(isset($mixed) && $mixed==true){
      $inputVal = getInputFielSavedValue($model,$customeField);
      if($inputVal==''){
        $inputVal = getInputFielSavedValue($model->subModel,$customeField);
      }
    }else{
  		$inputVal = getInputFielSavedValue($model,$customeField);
    }
	}
	@endphp

  <?php //dd($customeField->input_type); ?>
	@if($customeField->input_type == 'text' || $customeField->input_type == 'numberinput' || $customeField->input_type == 'websiteinput' || $customeField->input_type == 'emailinput')


	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group mt-5  mb-0">
			@if($customeField->autocomplete_text == 1)
			@if($customeField->predefined_list_id >= 1)
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control predefind', 'placeholder'=>$customeField->hint_text, 'data-id'=>$customeField->predefined_list_id,'autocomplete'))}}
			@else
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control new_options', 'placeholder'=>$customeField->hint_text, 'autocomplete'))}}
			@endif
			@else
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'placeholder'=>$customeField->hint_text))}}
			@endif
		</div>
	</div>

	@endif

  

	@if($customeField->input_type == 'textarea')


	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group mt-5 mb-0 ">
			{{Form::textarea($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'rows'=>'1', 'placeholder'=>$customeField->hint_text))}}
		</div>
	</div>

	@endif

	@if($customeField->input_type == 'date')
  @php
  $datepickerOptions = ['format'=>'yyyy-mm-dd','todayHighlight'=>true,'orientation'=>'bottom left','autoclose'=>true];

  if($customeField->dateOptions!=null){
    if($customeField->dateOptions->default_today_date==1 && $inputVal==''){
      $inputVal=date("Y-m-d");
    }
    if($customeField->dateOptions->selection_type==1){//Future
      $datepickerOptions['startDate']='-0d';
    }
    if($customeField->dateOptions->selection_type==2){//Past
      $datepickerOptions['endDate']='-0d';
    }
  }
  @endphp
	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group mt-5 mb-0">
			<label>{{$customeField->title}}</label>
			<div class="input-group date">
				{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('id'=>'cdtp'.$customeField->id,'class' => 'form-control','placeholder'=>$customeField->hint_text,'autocomplete'=>'off','readonly'=>'readonly'))}}
				<div class="input-group-append">
					<span class="input-group-text">
						<i class="la la-calendar-check-o"></i>
					</span>
				</div>
			</div>
		</div>
	</div>
  @push('jsScripts')
  $('#cdtp{{$customeField->id}}').datepicker({!! json_encode($datepickerOptions) !!});
  @endpush
	@endif
	@if($customeField->input_type == 'select')

	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group mt-5 mb-0">
			@if($customeField->selection_type == 2)
			@if($customeField->predefined_list_id > 0)
			{{Form::select($inputFieldKey.'['.$customeField->id.'][]',getPredefinedList($customeField->predefined_list_id),$inputVal,array('class' => 'form-control select2', 'multiple'=>'multiple'))}}
			@else
			{{Form::select($inputFieldKey.'['.$customeField->id.'][]',getMultipleOptions($customeField->id),$inputVal,array('class' => 'form-control select2', 'multiple'=>'multiple'))}}
			@endif
			@else
			@if($customeField->predefined_list_id > 0)
			{{Form::select($inputFieldKey.'['.$customeField->id.']',getPredefinedList($customeField->predefined_list_id),$inputVal,array('class' => 'form-control'))}}
			@else
			{{Form::select($inputFieldKey.'['.$customeField->id.']',getMultipleOptions($customeField->id),$inputVal,array('class' => 'form-control'))}}
			@endif
			@endif
		</div>
	</div>

	@endif
	@endforeach


@if($showCard==true)
</div>
</div>
@endif
@push('js')
<script type="text/javascript">

// var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
// $(document).ready(function(){
//   $( ".predefind" ).autocomplete({
//     source: function( request, response ) {
//       $.ajax({
//         url:"{{route('custome_field.getPredefinedList')}}",
//         type: 'get',
//         dataType: "json",
//         data: {
//            _token: CSRF_TOKEN,
//            search: request.term
//         },
//         success: function( data ) {
//            response( data );
//         }
//       });
//     },
//     select: function (event, ui) {
//        // Set selection
//        $('#user_serach').val(ui.item.label); // display the selected text
//        $('#user_id').val(ui.item.value); // save selected id to input
//        return false;
//     }
//   });
// });
</script>
@endpush
@endif
<?php  //dd('w'); ?>

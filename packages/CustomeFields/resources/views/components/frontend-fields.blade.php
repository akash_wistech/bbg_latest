@if($customeFields!=null && count($customeFields)>0)
	@foreach($customeFields as $customeField)

	@php
  $inputVal=null;
	if($model->id!=null){
    if(isset($mixed) && $mixed==true){
      $inputVal = getInputFielSavedValue($model,$customeField);
      if($inputVal==''){
        $inputVal = getInputFielSavedValue($model->subModel,$customeField);
      }
    }else{
  		$inputVal = getInputFielSavedValue($model,$customeField);
    }
	}
	@endphp

	@if(in_array($customeField->input_type,['text','vat_number','numberinput','phoneinput','faxinput','websiteinput','emailinput']))


	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			@if($customeField->autocomplete_text == 1)
			@if($customeField->predefined_list_id >= 1)
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control predefind', 'placeholder'=>$customeField->hint_text, 'data-id'=>$customeField->predefined_list_id,'autocomplete'))}}
			@else
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control new_options', 'placeholder'=>$customeField->hint_text, 'autocomplete'))}}
			@endif
			@else
			{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'placeholder'=>$customeField->hint_text))}}
			@endif
		</div>
	</div>

	@endif
  @if($customeField->input_type == 'checkbox' || $customeField->input_type=='radio')
    @if($customeField->predefined_list_id>0)
    @php
    $selectArr=getPredefinedList($customeField->predefined_list_id);
    if($customeField->input_type=='radio'){
      $cbrbName = $inputFieldKey.'['.$customeField->id.']';
    }else{
      $cbrbName = $inputFieldKey.'['.$customeField->id.'][]';
    }
    @endphp
    <div class="col-md-{{$customeField->colum_width}}">
  		<div class="form-group">
  			<label>{{$customeField->title}}</label>
        <div class="{{$customeField->input_type}}-inline">
          @foreach($selectArr as $key=>$val)
          @php
          $checked="";
          if($customeField->input_type=='radio' && $key==$inputVal){
            $checked=' checked="checked"';
          }else{
            if(is_array($inputVal) && in_array($key,$inputVal)){
              $checked=' checked="checked"';
            }
          }
          @endphp
					<label class="{{$customeField->input_type}}">
  					<input type="{{$customeField->input_type}}" name="{{$cbrbName}}" value="{{$key}}"{{$checked}}>
  					<span></span>{{$val}}
          </label>
          @endforeach
				</div>
  		</div>
  	</div>
    @endif
  @endif


	@if(in_array($customeField->input_type,['textarea','address','profile']))
	<div class="col-md-{{$customeField->input_type=='address' ? 4 : 12}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
      @if($customeField->input_type=='address')
      {{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'placeholder'=>$customeField->hint_text))}}
      @else
			{{Form::textarea($inputFieldKey.'['.$customeField->id.']',$inputVal,array('class' => 'form-control', 'placeholder'=>$customeField->hint_text))}}
			@endif
		</div>
	</div>

	@endif

	@if($customeField->input_type == 'date')
  @php
  $datepickerOptions = ['format'=>'yyyy-mm-dd','todayHighlight'=>true,'orientation'=>'bottom left','autoclose'=>true];

  if($customeField->dateOptions!=null){
    if($customeField->dateOptions->default_today_date==1 && $inputVal==''){
      $inputVal=date("Y-m-d");
    }
    if($customeField->dateOptions->selection_type==1){//Future
      $datepickerOptions['startDate']='-0d';
    }
    if($customeField->dateOptions->selection_type==2){//Past
      $datepickerOptions['endDate']='-0d';
    }
  }
  @endphp
	<div class="col-md-{{$customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			<div class="input-group date">
				{{Form::text($inputFieldKey.'['.$customeField->id.']',$inputVal,array('id'=>'cdtp'.$customeField->id,'class' => 'form-control','placeholder'=>$customeField->hint_text,'autocomplete'=>'off','readonly'=>'readonly'))}}
				<div class="input-group-append">
					<span class="input-group-text">
						<i class="la la-calendar-check-o"></i>
					</span>
				</div>
			</div>
		</div>
	</div>
  @push('jsScripts')
  $('#cdtp{{$customeField->id}}').datepicker({!! json_encode($datepickerOptions) !!});
  @endpush
	@endif
	@if($customeField->input_type == 'select')

	<div class="col-md-{{$customeField->selection_type == 2 ? 12 : $customeField->colum_width}}">
		<div class="form-group">
			<label>{{$customeField->title}}</label>
			@if($customeField->selection_type == 2)
			@if($customeField->predefined_list_id > 0)
			{{Form::select($inputFieldKey.'['.$customeField->id.'][]',getPredefinedList($customeField->predefined_list_id),$inputVal,array('class' => 'form-control select2','data-live-search'=>'true','data-selected-text-format'=>'count>2', 'multiple'=>'multiple'))}}
			@else
			{{Form::select($inputFieldKey.'['.$customeField->id.'][]',getMultipleOptions($customeField->id),$inputVal,array('class' => 'form-control select2','data-live-search'=>'true','data-selected-text-format'=>'count>2', 'multiple'=>'multiple'))}}
			@endif
			@else
			@if($customeField->predefined_list_id > 0)
			{{Form::select($inputFieldKey.'['.$customeField->id.']',getPredefinedList($customeField->predefined_list_id),$inputVal,array('class' => 'form-control'))}}
			@else
			{{Form::select($inputFieldKey.'['.$customeField->id.']',getMultipleOptions($customeField->id),$inputVal,array('class' => 'form-control'))}}
			@endif
			@endif
		</div>
	</div>

	@endif
	@endforeach
@endif

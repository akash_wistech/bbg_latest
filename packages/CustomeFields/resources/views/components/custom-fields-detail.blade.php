@if($showCard==true)
<div class="card card-custom">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">{{__('customfields::customfields.heading')}}</h3>
    </div>
  </div>
  <div class="card-body">
@endif
    <div class="row">
      @if($customeFields!=null)
      @foreach($customeFields as $customeField)
      @php
      $thisFieldId='input_field_'.$customeField['id'];
      $inputOpts['id']=$thisFieldId;
      $value = getInputFielSavedValueForDetail($model,$customeField);
      @endphp
      <div class="mt-3 col-sm-{{($customeField->colum_width==12 ? $customeField->colum_width : 6)}}">
        <strong>
          {{$customeField->title}}
          @if($customeField['hint_text']!='')
          <a href="javascript:;" class="blue-hint" data-toggle="popover" data-trigger="hover" data-placement="top" title="{{$customeField['title']}}" data-content="{{$customeField['hint_text']}}">
            <i class="fa fa-info-circle"></i>
          </a>
          @endif
          :
        </strong>
        {{$value}}
      </div>
      @endforeach
      @endif
    </div>
@if($showCard==true)
  </div>
</div>
@endif

@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>'','icon'=>'plus','class'=>'success','link'=>'/custome_field/create', 'method'=>'post'];
}
@endphp
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('customfields::customfields.heading'))

@push('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
@endpush
@section('content')
<div class="card card-custom">
  <div class="card card-body">
    <table class="table" id="predefinedlist_table">
      <thead>
        <tr>
          <th>Input Type</th>
          <th>Module</th>
          <th>Name</th>
          <th>Short Name</th>
          <th>Is Required</th>
          <th>Show in Grid</th>
          <th>Status</th>
          <th>Order</th>
          <th></th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>
  </div>
</div>
@endsection
@push('js')
<script src="{{asset('assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script type="text/javascript">
  $('#predefinedlist_table').DataTable({
    processing: true,
    serverSide: true,
    ajax:{
      url: "{{ route('custome_field.datatables') }}",
    },
    order: [[ 7, "asc" ]],
    columns:[
    {
      data: 'input_type',
      name: 'input_type',
    },
    {
      data: 'module',
      name: 'module'
    },
    {
      data: 'name',
      name: 'name'
    },
    {
      data: 'short_name',
      name: 'short_name'
    },
    {
      data: 'is_required',
      name: 'is_required'
    },
    {
      data: 'show_in_grid',
      name: 'show_in_grid'
    },
    {
      data: 'status',
      name: 'status'
    },
    {
      data: 'order',
      name: 'order'
    },
    {
      data: 'action',
      name: 'action',
      orderable: false
    }
    ]
  });
</script>
@endpush

@extends('layouts.app')
@section('title', __('customfields::customfields.heading'))
@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('customfields::customfields.heading'),
]])
@endsection
@section('content')
{!! Form::model($model, ['method' => 'post', 'route' => ['custome_field.store'], 'files' => true, 'id' => 'form']) !!}
@include('customfields::custome_fields._form')
{!! Form::close() !!}
@endsection

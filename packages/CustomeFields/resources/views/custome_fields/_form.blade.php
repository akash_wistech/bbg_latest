<div class="card card-custom">
  <div class="card-body">
    <div class="row">
      <div class="form-group">
        {{ Form::label('module', 'Module') }}
        @foreach(getModuleTypeArr() as $key=>$value)
        @php
        $check = getCustomFieldModuleChecked($model->id,$key);
        @endphp
        <label>
          {!! Form::checkbox('module_type[]', $key, $check).' '.$value !!}
        </label>
        @endforeach
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('input_type', 'Input Type') !!}
          {!! Form::select('input_type', [''=>__('common.select')] + dropDownList(), null, ['class' => 'form-control', 'id'=>'dropdown'])!!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('title', 'Name') !!}
          {!! Form::text('title', null, ['class' => 'form-control'])!!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('short_name', 'Short Name') !!}
          {!! Form::text('short_name', null, ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          {!! Form::label('is_required', 'Is Required') !!}
          {!! Form::select('is_required', dropDownYesNo(), null, ['class' => 'form-control']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('hint_text','Hint') !!}
          {!! Form::text('hint_text', null, ['class' => 'form-control']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          {!! Form::label('sort_order', 'Sort Order') !!}
          {!! Form::text('sort_order', null, ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          {!! Form::label('show_in_grid','Show in Grid') !!}
          {!! Form::select('show_in_grid', dropDownYesNo(), null, ['class' => 'form-control']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          {!! Form::label('colum_width', 'Column Width') !!}
          {!! Form::select('colum_width', columnWidth(), null, ['class' => 'form-control', 'rows'=>4]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 autocomplete_div"{!!$model->input_type!='text' ? ' style="display:none;"' : ''!!}>
        <div class="form-group">
          {!! Form::label('autocomplete_text', 'Autocomplete Text') !!}
          {!! Form::select('autocomplete_text', dropDownYesNo(), null, ['class' => 'form-control', 'rows'=>4]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-3 todaydate_div"{!!$model->input_type!='date' ? ' style="display:none;"' : ''!!}>
        <div class="form-group">
          {!! Form::label('default_today_date', 'Default Today Date') !!}
          {!! Form::select('default_today_date', dropDownYesNo(), null, ['class' => 'form-control']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-3 todaydate_div"{!!$model->input_type!='date' ? ' style="display:none;"' : ''!!}>
        <div class="form-group">
          {!! Form::label('date_selection_type', 'Selection Type') !!}
          {!! Form::select('date_selection_type', dateOptionsArr(), null, ['class' => 'form-control']) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
      <div class="col-md-3 selection_div" {!!$model->input_type == 'dropdown' ? '' : 'style="display:none"'!!}>
        <div class="form-group">
          {!! Form::label('selection_type', 'Selection Type') !!}
          {!! Form::select('selection_type', singleMultiple(), null, ['class' => 'form-control', 'rows'=>4]) !!}
          <div class="help-block with-errors"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        {{Form::label('descp', 'Description')}}
        {{Form::textarea('descp', null, ['class'=> 'form-control', 'cols'=>'12', 'rows'=>'4'])}}
      </div>
    </div>
    <div class="mt-5 radio" {!!in_array($model->input_type,['select','checkbox','radio']) && $model->predefined_list_id >= 0 ? '' : 'style="display:none"'!!}>
      <div class="form-group">
        <div class="radio-inline">
          <label class="radio">
            <input type="radio" name="optradio" onclick="Options('p')" {!!$model->id==null || $model->predefined_list_id=="" || $model->predefined_list_id > 0 ? 'checked="checked"' : ''!!}>
            <span></span>Predefined List
          </label>
          <label class="radio">
            <input type="radio" name="optradio" onclick="Options('c')" {!!$model->id==null || $model->predefined_list_id=="" || $model->predefined_list_id > 0 ? '' : 'checked="checked"'!!}>
            <span></span>Add New Option
          </label>
        </div>
      </div>
    </div>

    <div class="mt-5 predefined_list" {!!in_array($model->input_type,['select','checkbox','radio']) && ($model->id==null || $model->predefined_list_id=="" || $model->predefined_list_id > 0) ? '' : 'style="display: none"'!!}>
      <div class="form-group">
        <label>Pedefined List</label><br>
        @foreach(getPredefinedList() as $key=>$value)
        @php
        $check = false;
        $predefined_list_id = getPredefinedList($model->predefined_list_id);
        @endphp
        @if($predefined_list_id)
        {!! Form::radio('predefined_list_id', $key, true).' '.$value !!}
        @else
        {!! Form::radio('predefined_list_id', $key, $check).' '.$value !!}
        @endif
        @endforeach
      </div>
    </div>
    <div class="row options" {!!in_array($model->input_type,['select','checkbox','radio']) && $model->predefined_list_id <= 0 ? '' : 'style="display: none"'!!}>
      <table name="invoice" id="tbl-items-append" class="table table-bordered tbl">
        <thead>
          <tr>
            <td>{!! Form::label('hint_text','Title') !!} <button style="float:right" type="button" class="btn btn-success add" id="btn-add-more-products"><i class="fas fa-plus-circle mx-2"></i>Add Option(s)</button></td>
            @if($model->predefined_list_id <= 0 && $model->sub_options!=null)
            @foreach($model->sub_options as $options)
            <tr valign="top"><td>{!! Form::text("sub_title[]", $options->title, ["class" => "form-control"]) !!} <a style="float:right"  href="javascript:void(0);" class="btn btn-danger remove"><i class="fas fa-minus-circle mx-2"></i></a></td></tr>
            @endforeach
            @endif
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <div class="adv-card card card-custom{{$model->adv_filter==1 ? '' : ' card-collapse'}} card-border mt-3" data-card="true">
      <div class="card-header">
        <div class="card-title">
          <h3 class="card-label">
            <label>
              {!! Form::checkbox('adv_filter', 1, $model->adv_filter==1 ? true : false, ['id'=>'cbAdvOpts','class'=>'form-group mb-0 lbl-mb-0']).' '.__('common.advance_search') !!}
            </label>
          </h3>
        </div>
        <div class="card-toolbar">
          <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
            <i class="ki ki-arrow-down icon-nm text-white"></i>
          </a>
        </div>
      </div>
      @php
      $searchInputType = '';
      $searchInputTypes = [];
      if($model->input_type!='')$searchInputTypes = CFFilterTypes()[$model->input_type];
      if($model->filterOptions!=null){
        $model->search_input_type = $model->filterOptions->input_type;
        $searchInputType = $model->search_input_type;
        $model->search_range_from = $model->filterOptions->range_from;
        $model->search_range_to = $model->filterOptions->range_to;
        $model->search_range_interval = $model->filterOptions->range_interval;
      }
      @endphp
      <div class="card-body"{!!$model->adv_filter==1 ? '' : ' style="display:none;"'!!}>
        <div class="form-group">
          {!! Form::label('search_input_type', __('customfields::customfields.search_input_type')) !!}
          {!! Form::select('search_input_type', $searchInputTypes, null, ['class' => 'form-control', 'id'=>'srchDropdown'])!!}
        </div>
        <div id="rangInfo" class="{{$searchInputType=='inputrange' ? '' : 'd-none'}}">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                {!! Form::label('search_range_from', __('customfields::customfields.search_range_from')) !!}
                {!! Form::text('search_range_from', null, ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                {!! Form::label('search_range_to', __('customfields::customfields.search_range_to')) !!}
                {!! Form::text('search_range_to', null, ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                {!! Form::label('search_range_interval', __('customfields::customfields.search_range_interval')) !!}
                {!! Form::text('search_range_interval', null, ['class' => 'form-control']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    {!! Form::submit(__('common.save'), ['class' => 'btn btn-success']) !!}
  </div>
</div>
@push('jsScripts')
var customFieldFilterOptions = {!!json_encode(CFFilterTypes())!!};
$("body").on("change", "#srchDropdown", function () {
  var type =this.value;
  if(type=='inputrange'){
    $("#rangInfo").removeClass("d-none");
  }else{
    $("#rangInfo").addClass("d-none");
  }
});
$("body").on("click", "#cbAdvOpts", function () {
  if($(this).prop("checked")){
    $(this).parents(".adv-card").removeClass("card-collapse");
    $(this).parents(".adv-card").find(".card-body").css("display","block");
  }else{
    $(this).parents(".adv-card").addClass("card-collapse");
    $(this).parents(".adv-card").find(".card-body").css("display","none");
  }
});
$('body').on('change', '#dropdown', function() {
  var type =this.value;
  $("#srchDropdown").html("");
  searchInputType_arr = customFieldFilterOptions[type];
  $.each( searchInputType_arr, function( key, value ) {
    $("#srchDropdown").append('<option value=\"'+key+'">'+value+'</option>');
  });
  if (type == 'autocomplete') {
    $('.predefined_list').show();
  }if (type == 'select' || type == 'checkbox' || type == 'radio') {
    $('.radio').show();
    $('.predefined_list').show();
  }if(type=='select'){
    $('.selection_div').show();
  }else{
    $('.selection_div').hide();
  }if(type=='text'){
    $('.autocomplete_div').show();
  }else{
    $('.autocomplete_div').hide();
  }
  if(type=='date'){
    $('.todaydate_div').show();
  }else{
    $('.todaydate_div').hide();
  }
  if (type == 'timerange' || type == 'time' || type == 'daterange' || type == 'date' || type == 'emailinput' || type == 'websiteinput' || type == 'numberinput' || type == 'phoneinput' || type == 'faxinput' || type == 'textarea' || type == 'tinymce') {
    $('.radio').hide();
    $('.predefined_list').hide();
  }
});
$('body').on('click', 'button.add', function() {
  var tbody = $(".tbl tbody");
  tbody.append('<tr valign="top"><td>{!! Form::text("sub_title[]", null, ["class" => "form-control"]) !!} <a style="float:right"  href="javascript:void(0);" class="btn btn-danger remove"><i class="fas fa-minus-circle mx-2"></i></a></td></tr>');
});
$("body").on('click','.remove',function(){
  $(this).parent().parent().remove();
});
function Options(id){
  if (id=='p') {
    $('.predefined_list').show();
    $('.options').hide();
  }if (id=='c') {
    $('.options').show();
    $('.predefined_list').hide();
  }
}
@endpush

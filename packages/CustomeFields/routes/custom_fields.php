<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['web', 'auth'])->group(function () {
	Route::get('/custome_field', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'index'])->name('custome_field');
	Route::get('/custome_field/create', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'create'])->name('custome_field.create');
Route::post('/custome_field/store', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'store'])->name('custome_field.store');
Route::get('/custome_field/edit/{id}', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'edit'])->name('custome_field.edit');
Route::post('/custome_field/update/{id}', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'update'])->name('custome_field.update');
Route::match(['get', 'post'],'/custome_field/delete/{id}', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'destroy'])->name('custome_field.delete');
Route::get('/custome_field/datatables', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'datatables'])->name('custome_field.datatables');
Route::get('/custome_field/getPredefinedList', [Wisdom\CustomeFields\Http\Controllers\CustomFieldsController::class, 'get_predefined_list'])->name('custome_field.getPredefinedList');
});

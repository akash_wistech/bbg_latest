<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldDatetimeInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_datetime_info', function (Blueprint $table) {
            $table->id();
            $table->char('module_type',25)->nullable()->index();
            $table->integer('module_id')->default(0)->nullable()->index();
            $table->integer('input_field_id')->default(0)->nullable()->index();
            $table->text('start_time')->nullable();
            $table->text('start_org_time')->nullable();
            $table->text('end_time')->nullable();
            $table->text('end_org_time')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_datetime_info');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomSubOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_sub_option', function (Blueprint $table) {
            $table->id();
            $table->integer('custom_field_id')->default(0)->nullable()->index();
            $table->string('title',50)->nullable();
            $table->integer('sort_order')->default(0)->nullable()->index();
            $table->tinyInteger('status')->default(1)->nullable()->index();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_sub_option');
    }
}

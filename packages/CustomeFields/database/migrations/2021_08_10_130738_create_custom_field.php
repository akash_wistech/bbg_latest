<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field', function (Blueprint $table) {
            $table->id();
            $table->char('input_type',20)->nullable();
            $table->tinyInteger('is_unique')->default(0)->nullable()->index();
            $table->tinyInteger('show_in_grid')->default(0)->nullable()->index();
            $table->string('title',100)->nullable();
            $table->char('short_name',25)->nullable();
            $table->integer('colum_width')->default(4)->nullable();
            $table->text('hint_text')->nullable();
            $table->text('descp')->nullable();
            $table->tinyInteger('autocomplete_text')->default(0)->nullable();
            $table->integer('predefined_list_id')->default(0)->nullable();
            $table->tinyInteger('is_required')->default(1)->nullable();
            $table->tinyInteger('selection_type')->default(1)->nullable()->comment('1=>Single,2=>Multiple');
            $table->tinyInteger('adv_filter')->default(0)->nullable()->index();
            $table->integer('sort_order')->default(0)->nullable()->index();
            $table->tinyInteger('status')->default(1)->nullable()->index();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->index();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field');
    }
}

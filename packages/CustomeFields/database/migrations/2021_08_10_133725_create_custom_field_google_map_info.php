<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldGoogleMapInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_google_map_info', function (Blueprint $table) {
            $table->id();
            $table->char('module_type',25)->nullable()->index();
            $table->integer('module_id')->default(0)->nullable()->index();
            $table->integer('input_field_id')->default(0)->nullable()->index();
            $table->text('map_location')->nullable();
            $table->string('map_lat',75)->nullable();
            $table->float('map_lat_org',20,15)->nullable();
            $table->string('map_lng',75)->nullable();
            $table->float('map_lng_org',20,15)->nullable();
            $table->text('google_static_image')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_google_map_info');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomeFieldAdvanceSearchOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('custom_field_search_option', function (Blueprint $table) {
          $table->integer('custom_field_id')->default(0)->nullable()->index();
          $table->char('input_type',20)->nullable();
          $table->char('range_from',20)->nullable();
          $table->char('range_to',20)->nullable();
          $table->char('range_interval',20)->nullable();
          $table->integer('sort_order')->default(0)->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_search_option');
    }
}

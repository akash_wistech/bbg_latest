<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldFilterOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_filter_option', function (Blueprint $table) {
            $table->id();
            $table->integer('input_field_id')->default(0)->nullable()->index();
            $table->string('input_type',25)->nullable();
            $table->integer('range_from')->default(0)->nullable();
            $table->integer('range_to')->default(0)->nullable();
            $table->integer('range_interval')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_filter_option');
    }
}

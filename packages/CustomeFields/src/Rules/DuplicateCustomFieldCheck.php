<?php
namespace Wisdom\CustomeFields\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Wisdom\CustomeFields\Models\CustomFieldData;
use App\Models\Opportunity;

class DuplicateCustomFieldCheck implements Rule
{
  public $moduleTypeId;
  public $opmtid;
  public $opmid;
  public $ignore;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($moduleTypeId,$ignore,$opmtid='',$opmid=0)
    {
      $this->moduleTypeId = $moduleTypeId;
      $this->opmtid = $opmtid;
      $this->opmid = $opmid;
      $this->ignore = $ignore;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $passed = true;
      if($this->ignore==0){
        $uniqueInputFields = getUniqueCustomFieldsByModule($this->moduleTypeId);
        if($uniqueInputFields!=null){
          foreach($uniqueInputFields as $uniqueInputField){
            //Check in data field if row exists
            $inputValue = $value[$uniqueInputField['id']];
            // echo $uniqueInputField['title'].' = '.$inputValue.'<br />';
            $query = DB::table((new CustomFieldData)->getTable());
            if($this->moduleTypeId=='opportunity' && $this->opmtid!='' && $this->opmid>0){
              $modelClass = getModelClass()[$this->opmtid];
              $joinedTbl = (new $modelClass)->getTable();
              $query->join((new Opportunity)->getTable(), (new Opportunity)->getTable().'.id', '=', (new CustomFieldData)->getTable().'.module_id');
              $query->where([
                (new CustomFieldData)->getTable().'.module_type' => $this->moduleTypeId,
                (new Opportunity)->getTable().'.module_type' => $this->opmtid,
                (new Opportunity)->getTable().'.module_id' => $this->opmid,
                'input_field_id' => $uniqueInputField['id'],
              ]);
            }else{
              $query->where([
                'module_type' => $this->moduleTypeId,
                'input_field_id' => $uniqueInputField['id'],
              ]);
            }
            $query->orWhere(function($query) use ($inputValue){
                $query->where('input_value', $inputValue)
                      ->where('input_org_value', $inputValue);
            });
            $duplicateRow = $query->first();
            if($duplicateRow!=null){
              $passed = false;
            }
          }
        }
      }
      return $passed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Record already exists, Save again if you want to update old record';
    }
}

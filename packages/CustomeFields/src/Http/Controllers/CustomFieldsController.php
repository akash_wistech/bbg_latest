<?php

namespace Wisdom\CustomeFields\Http\Controllers;

use Wisdom\CustomeFields\Models\CustomField;
use App\Models\Setting;
use Wisdom\CustomeFields\Models\CustomFieldModule;
use Wisdom\CustomeFields\Models\CustomFieldSubOption;
use Illuminate\Http\Request;
use Validator;
use TJGazel\Toastr\Facades\Toastr;
use Auth;
use DataTables;
use App\Http\Controllers\Controller;

class CustomFieldsController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

    return view('customfields::custome_fields.index');
  }
  public function datatables(){
    $data= CustomField::get();
    return DataTables::of($data)
    ->addColumn('action', function($data){

      $edit_url = url('custome_field/edit/'.$data->id);
      $delete_url = url('custome_field/delete/'.$data->id);
      $edit= "<a class='btn btn-sm btn-clean btn-icon edit' href='".$edit_url."' title='Update' data-id='".$data->id."'><i class='text-dark-50 flaticon-edit'></i></a>";
      $cancel= "<a class='btn btn-sm btn-clean btn-icon act-confirmation' href='".$delete_url."' title='Delete' data-id='".$data->id."' data-confirmationmsg='".__('common.deleteconfirmation')."' data-pjxcntr='grid-table'><i class='text-dark-50 flaticon-delete'></i></a>";
      return $edit.'&nbsp;'.$cancel;
    })
    ->addColumn('input_type', function($data){
      return $data->input_type;
    })
    ->addColumn('module', function($data){
      return getCustomFiedAttachedModules($data->id);
    })
    ->addColumn('name', function($data){
      return $data->title;
    })
    ->addColumn('short_name', function($data){
      return $data->short_name;
    })
    ->addColumn('is_required', function($data){
      if ($data->is_required == 1) {
        return '<span class="label label-rounded label-success"><i class="fa fa-check text-white"></i></span>';
      }else{
        return '<span class="label label-rounded label-danger"><i class="fa fa-times text-white"></i></span>';
      }
    })
    ->addColumn('show_in_grid', function($data){
      if ($data->show_in_grid == 1) {
        return '<span class="label label-rounded label-success"><i class="fa fa-check text-white"></i></span>';
      }else{
        return '<span class="label label-rounded label-danger"><i class="fa fa-times text-white"></i></span>';
      }
    })
    ->addColumn('status', function($data){
      if ($data->status == 1) {
        return '<span class="label label-rounded label-success"><i class="fa fa-check text-white"></i></span>';
      }else{
        return '<span class="label label-rounded label-danger"><i class="fa fa-times text-white"></i></span>';
      }
    })
    ->addColumn('order', function($data){
      return $data->sort_order;
    })
    ->rawColumns(['action', 'is_required', 'show_in_grid', 'status', 'module'])
    ->make(true);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $model= new CustomField();
    $model->colum_width=4;
    return view('customfields::custome_fields.create', compact('model'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'title'=>'required',
    ]);
    if ($validator->fails()) {
      Toastr::warning('Please fill all required fileds', 'Warning');
      return redirect()->back();
    }else{
      $data= new CustomField();
      $data->input_type=$request->input_type;
      $data->title=$request->title;
      $data->sort_order=$request->sort_order ?? 0;
      $data->is_unique=$request->is_unique ?? 0;
      $data->show_in_grid=$request->show_in_grid ?? 0;
      $data->is_required=$request->is_required ?? 1;

      $data->default_today_date=$request->default_today_date ?? 0;
      $data->date_selection_type=$request->date_selection_type ?? 0;

      $data->autocomplete_text=$request->autocomplete_text ?? 0;

      $data->short_name=$request->short_name ?? '';
      $data->hint_text=$request->hint_text ?? '';
      $data->colum_width=$request->colum_width ?? 1;
      $data->descp=$request->descp ?? '';
      $data->predefined_list_id=$request->predefined_list_id ?? 0;
      $data->selection_type=$request->selection_type ?? '';
      $data->adv_filter=$request->adv_filter ?? 0;
      $data->search_input_type=$request->search_input_type ?? 'text';
      $data->search_range_from=$request->search_range_from ?? 0;
      $data->search_range_to=$request->search_range_to ?? 0;
      $data->search_range_interval=$request->search_range_interval ?? 0;
      if ($data->save()) {
        CustomFieldModule::where([
          'custom_field_id'=>$data->id,
        ])
        ->whereNotIn('module_type',$request->module_type)->delete();
        foreach ($request->module_type as $key => $value) {
          $moduleDetail=CustomFieldModule::where('custom_field_id',$data->id)->where('module_type',$value)->first();
          if($moduleDetail==null){
            $moduleDetail = new CustomFieldModule();
            $moduleDetail->custom_field_id = $data->id;
            $moduleDetail->module_type = $value;
            $moduleDetail->save();
          }
        }
        if ($request->sub_title) {
          foreach ($request->sub_title as $key => $value) {
            $subOptions = new CustomFieldSubOption();
            $subOptions->custom_field_id = $data->id;
            $subOptions->title = $request->sub_title[$key];
            $subOptions->sort_order = $request->sort_order ?? 0;;
            $subOptions->status = 1;
            $subOptions->created_by=Auth::user()->id;
            $subOptions->save();
          }
        }
        return redirect('custome_field')->with('success', 'custome_fields Added');
      }

    }
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\PredefinedList  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function edit(CustomField $customField, $id)
  {
    $model = CustomField::find($id);
    if($model->input_type=='date' && $model->dateOptions!=null){
      $model->default_today_date = $model->dateOptions->default_today_date;
      $model->date_selection_type = $model->dateOptions->selection_type;
    }
    return view('customfields::custome_fields.edit', compact('model'));

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Models\PredefinedList  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $validator = Validator::make($request->all(), [
      'title'=>'required',
    ]);
    if ($validator->fails()) {
      Toastr::warning('Please fill all required fileds', 'Warning');
      return redirect()->back();
    }else{
      $data=CustomField::find($id);
      $data->input_type=$request->input_type;
      $data->title=$request->title;
      $data->sort_order=$request->sort_order ?? 0;
      $data->is_unique=$request->is_unique ?? 0;
      $data->show_in_grid=$request->show_in_grid ?? 0;
      $data->is_required=$request->is_required ?? 1;

      $data->default_today_date=$request->default_today_date ?? 0;
      $data->date_selection_type=$request->date_selection_type ?? 0;

      $data->autocomplete_text=$request->autocomplete_text ?? 0;
      $data->short_name=$request->short_name ?? '';
      $data->hint_text=$request->hint_text ?? '';
      $data->colum_width=$request->colum_width ?? 1;
      $data->descp=$request->descp ?? '';
      $data->predefined_list_id=$request->predefined_list_id ?? 0;
      $data->selection_type=$request->selection_type ?? '';
      $data->adv_filter=$request->adv_filter ?? 0;
      $data->search_input_type=$request->search_input_type ?? 'text';
      $data->search_range_from=$request->search_range_from ?? 0;
      $data->search_range_to=$request->search_range_to ?? 0;
      $data->search_range_interval=$request->search_range_interval ?? 0;
      $data->updated_at=date("Y-m-d H:i:s");
      if ($data->save()) {
        CustomFieldModule::where([
          'custom_field_id'=>$data->id,
        ])
        ->whereNotIn('module_type',$request->module_type)->delete();
        foreach ($request->module_type as $key => $value) {
          $moduleDetail=CustomFieldModule::where('custom_field_id',$data->id)->where('module_type',$value)->first();
          if($moduleDetail==null){
            $moduleDetail = new CustomFieldModule();
            $moduleDetail->custom_field_id = $data->id;
            $moduleDetail->module_type = $value;
            $moduleDetail->save();
          }
        }
        if ($request->sub_title) {
          foreach ($request->sub_title as $key => $value) {
            $subOptions = new CustomFieldSubOption();
            $subOptions->custom_field_id = $data->id;
            $subOptions->title = $request->sub_title[$key];
            $subOptions->sort_order = $request->sort_order ?? 0;;
            $subOptions->status = 1;
            $subOptions->created_by=Auth::user()->id;
            $subOptions->save();
          }
        }
        return redirect('custome_field')->with('success', 'custome_fields Updated');
        return redirect('custome_field');
      }

    }
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\CustomField  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return CustomField::where('id', $id)->first();
  }
}

<?php
namespace Wisdom\CustomeFields\View\Components;

use Illuminate\View\Component;
use Wisdom\CustomeFields\Models\CustomField;

class FrontendNontaFields extends Component
{
    /**
     * card
     */
    public $card=false;

    /**
     * model
     */
    public $model;

    /**
     * model
     */
    public $isSub=false;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($model,$isSub=false,$card=false)
    {
     $this->card = $card;
     $this->model = $model;
     $this->isSub = $isSub;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
        $customFields = getNonTextAreaColumns($this->model->moduleTypeId);
        $inputFieldKey='input_field';
        if($this->isSub==true)$inputFieldKey=$this->model->moduleTypeId.'[input_field]';
        return view('customfields::components.frontend-fields', [
          'showCard' => $this->card,
          'isSub' => $this->isSub,
          'inputFieldKey' => $inputFieldKey,
          'customeFields' => $customFields,
          'model'=>$this->model
        ]);
    }
}

<?php
namespace Wisdom\CustomeFields\View\Components;

use Illuminate\View\Component;
use Wisdom\CustomeFields\Models\CustomField;

class CustomeFieldDetails extends Component
{
    /**
     * card
     */
    public $card=false;

    /**
     * model
     */
    public $model;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($model,$card=false)
    {
     $this->card = $card;
     $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
        $customFields = getCustomFieldsByModule($this->model->moduleTypeId);
        return view('customfields::components.custom-fields-detail', [
          'showCard' => $this->card,
          'customeFields' => $customFields,
          'model'=>$this->model
        ]);
    }
}

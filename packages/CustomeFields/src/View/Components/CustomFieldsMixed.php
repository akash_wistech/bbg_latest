<?php
namespace Wisdom\CustomeFields\View\Components;

use Illuminate\View\Component;
use Wisdom\CustomeFields\Models\CustomField;

class CustomFieldsMixed extends Component
{
    /**
     * card
     */
    public $card=false;

    /**
     * model
     */
    public $model;

    /**
     * model
     */
    public $subModel;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($model,$subModel=null,$card=false)
    {
     $this->card = $card;
     $this->model = $model;
     $this->subModel = $subModel;
     $this->isSub = false;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
      $modulesArr = [];
      $modulesArr[]=$this->model->moduleTypeId;
      if($this->subModel!=null){
        $modulesArr[]=$this->subModel->moduleTypeId;
      }
        $customFields = getCustomFieldsByModule($modulesArr);
        $inputFieldKey='input_field';
        if($this->isSub==true)$inputFieldKey=$this->model->moduleTypeId.'[input_field]';
        return view('customfields::components.custome-fields', [
          'showCard' => $this->card,
          'isSub' => $this->isSub,
          'mixed' => true,
          'inputFieldKey' => $inputFieldKey,
          'customeFields' => $customFields,
          'model'=>$this->model
        ]);
    }
}

<?php
namespace Wisdom\CustomeFields\Observers;

use Wisdom\CustomeFields\Models\CustomField;
use Wisdom\CustomeFields\Models\CustomFieldFilterOption;
use Wisdom\CustomeFields\Models\CustomFieldsDateOption;
use Illuminate\Support\Facades\DB;

class CustomFieldObserver
{
  /**
  * Handle the CustomField "created" event.
  *
  * @param  \Wisdom\CustomeFields\Models\CustomField  $customField
  * @return void
  */
  public function created(CustomField $customField)
  {
    if ($customField->id) {
      $this->addUpdateFilterOptions($customField);
    }
  }

  /**
  * Handle the CustomField "updated" event.
  *
  * @param  \App\Models\CustomField  $customField
  * @return void
  */
  public function updated(CustomField $customField)
  {
    $this->addUpdateFilterOptions($customField);
  }

  /**
  * Handle the CustomField "deleted" event.
  *
  * @param  \App\Models\CustomField  $customField
  * @return void
  */
  public function deleted(CustomField $customField)
  {
    //
  }

  /**
  * Handle the CustomField "restored" event.
  *
  * @param  \App\Models\CustomField  $customField
  * @return void
  */
  public function restored(CustomField $customField)
  {
    //
  }

  /**
  * Handle the CustomField "force deleted" event.
  *
  * @param  \App\Models\CustomField  $customField
  * @return void
  */
  public function forceDeleted(CustomField $customField)
  {
    //
  }

  public function addUpdateFilterOptions($customField)
  {
    if($customField->input_type=='date'){
      $filterOptions = CustomFieldsDateOption::where('input_field_id',$customField->id)->first();
      if($filterOptions==null){
        $filterOptions = new CustomFieldsDateOption;
        $filterOptions->input_field_id = $customField->id;
        $filterOptions->default_today_date = $customField->default_today_date;
        $filterOptions->selection_type = $customField->date_selection_type;
        $filterOptions->save();
      }else{
        DB::table((new CustomFieldsDateOption)->getTable())
        ->where('input_field_id', $customField->id)
        ->update([
          'default_today_date' => $customField->default_today_date,
          'selection_type' => $customField->date_selection_type,
        ]);
      }
    }
    if($customField->adv_filter==1){
      $filterOptions = CustomFieldFilterOption::where('input_field_id',$customField->id)->first();
      if($filterOptions==null){
        $filterOptions = new CustomFieldFilterOption;
        $filterOptions->input_field_id = $customField->id;
        $filterOptions->input_type = $customField->search_input_type;
        $filterOptions->range_from = $customField->search_range_from;
        $filterOptions->range_to = $customField->search_range_to;
        $filterOptions->range_interval = $customField->search_range_interval;
        $filterOptions->save();
      }else{
        DB::table((new CustomFieldFilterOption)->getTable())
        ->where('input_field_id', $customField->id)
        ->update([
          'input_type' => $customField->search_input_type,
          'range_from' => $customField->search_range_from,
          'range_to' => $customField->search_range_to,
          'range_interval' => $customField->search_range_interval,
        ]);
      }
    }else{
      //Delete
      CustomFieldFilterOption::where('input_field_id',$customField->id)->delete();
    }
  }
}

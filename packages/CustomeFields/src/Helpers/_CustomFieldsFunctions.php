<?php
use App\Models\Setting;
use App\Models\Opportunity;
use Wisdom\CustomeFields\Models\CustomField;
use Wisdom\CustomeFields\Models\CustomFieldModule;
use Wisdom\CustomeFields\Models\CustomFieldData;
use Wisdom\CustomeFields\Models\CustomFieldDataMultiple;
use Wisdom\CustomeFields\Models\CustomFieldSubOption;
use NaeemAwan\PredefinedLists\Models\PredefinedList;
use NaeemAwan\PredefinedLists\Models\PredefinedListDetail;
use Wisdom\CustomeFields\Models\CustomFieldFilterOption;

if (!function_exists('getOriginalTime')) {
  /**
  * formats time for db
  */
  function getOriginalTime($input_time)
  {
    return date("H:i",strtotime($input_time));
  }
}

if (!function_exists('getInputFielSavedValueByCFId')) {
  /**
  * formats time for db
  */
  function getInputFielSavedValueByCFId($model,$id)
  {
    $customField = CustomField::where('id',$id)->first();
    if($customField!=null){
      return getInputFielSavedValueForDetail($model,$customField);
    }
    return "";
  }
}

if (!function_exists('saveModuleNameSetting')) {
  /**
  * formats time for db
  */
  function saveModuleNameSetting($inputValue)
  {
    if($inputValue!=null){
      foreach($inputValue as $key=>$val){
        $setting=Setting::where('config_name',$key)->first();
        if($setting==null){
          $setting=new Setting;
          $setting->config_name=$key;
        }
        $setting->config_value=$val;
        $setting->save();
      }
    }
  }
}

if (!function_exists('getSettingModuleNameField')) {
  /**
  * formats time for db
  */
  function getSettingModuleNameField()
  {
    $html = '';
    $moduleListArr = getModulesDynNameListArr();
    if($moduleListArr!=null){
      foreach($moduleListArr as $mkey=>$mval){
        foreach($mval as $mks1=>$mvs1){
          list($moduleType,$pr) = explode("-",$mks1);
          $customFieldSavedSetting = getSetting($mks1);
          $selectOptions='';
          $customFields = getCustomFieldsByModule($moduleType);
          if($customFields!=null){
            foreach($customFields as $customField){
              $selectOptions.='<option value="'.$customField['id'].'"'.($customFieldSavedSetting==$customField['id'] ? ' selected' : '').'>'.$customField['title'].'</option>';
            }
          }
          $html.='<div class="col-sm-4">';
          $html.='  <div class="form-group">';
          $html.='    <label for="module_name_fld-'.$mks1.'">'.__('app.settings.module_name_fld',['name'=>$mvs1]).'</label>';
          $html.='    <select class="form-control" id="module_name_fld-'.$mks1.'" name="module_name_fld['.$mks1.']">'.$selectOptions.'</select>';
          $html.='  </div>';
          $html.='</div>';
        }
      }
    }
    return $html;
  }
}

if (!function_exists('getOptionTitleById')) {
  /**
  * get title of record by its id
  */
  function getOptionTitleById($id,$customField)
  {
    if($customField['predefined_list_id']!=''){
      $row=getPredefinedListOption($id);
      return $row!=null ? $row->lang->title : '';
    }else{
      $row=getInputSubOption($id);
      return $row!=null ? $row->title : '';
    }
  }
}

if (!function_exists('getServiceTypeList')) {
  /**
  * All predefined lists
  */
  function getServiceTypeList()
  {
    $id = getSetting('service_list_id');
    return PredefinedList::where(['parent_id' => $id, 'status' => 1])->get();
  }
}

if (!function_exists('getServiceTypeListArr')) {
  /**
  * All predefined lists
  */
  function getServiceTypeListArr()
  {
    $serviceTypes = getServiceTypeList();
    $arr = [];
    if($serviceTypes!=null){
      foreach($serviceTypes as $serviceType){
        $arr[$serviceType->id]=$serviceType->lang->title;
      }
    }
    return $arr;
  }
}

if (!function_exists('getPredefinedListOption')) {
  /**
  * All predefined lists
  */
  function getPredefinedListOption($id)
  {
    return PredefinedList::where(['id' => $id, 'status' => 1])->first();
  }
}

if (!function_exists('getInputSubOption')) {
  /**
  * All predefined lists
  */
  function getInputSubOption($id)
  {
    return CustomFieldSubOption::where(['id' => $id, 'status' => 1])->first();
  }
}

if (!function_exists('getModule')) {
  /**
  * return status array
  */
  function getModule($custom_field_id)
  {
    $modules = CustomFieldModule::where('custom_field_id', $custom_field_id)->get()->pluck('module_type');
    foreach ($modules as $value) {
      return '<span class="badge badge-success">'.$value.'</span>';
    }
  }
}

if (!function_exists('getCustomFieldModuleChecked')) {
  /**
  * return status array
  */
  function getCustomFieldModuleChecked($custom_field_id,$moduleTypeId)
  {
    $checked = false;
    $module = CustomFieldModule::where(['custom_field_id'=>$custom_field_id,'module_type'=>$moduleTypeId])->first();
    if($module!=null){
      $checked = true;
    }
    return $checked;
  }
}

if (!function_exists('getCFMultipleIdz')) {
  /**
  * return status array
  */
  function getCFMultipleIdz($model,$customField)
  {
    return CustomFieldDataMultiple::select('option_id')->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id,'input_field_id'=>$customField['id']])->get()->pluck('option_id', 'option_id')->toArray();
  }
}

if (!function_exists('getInputFielSavedValueForDetail')) {
  /**
  * get saved value of input for detail
  * @return string
  */
  function getInputFielSavedValueForDetail($model,$customField)
  {
    $value='';
    if(
      $customField['input_type']=='text' ||
      $customField['input_type']=='autocomplete' ||
      $customField['input_type']=='numberinput' ||
      $customField['input_type']=='websiteinput' ||
      $customField['input_type']=='emailinput' ||
      $customField['input_type']=='date' ||
      $customField['input_type']=='daterange' ||
      $customField['input_type']=='time' ||
      $customField['input_type']=='textarea' ||
      $customField['input_type']=='tinymce' ||
      $customField['input_type']=='radio'
    ){
      $valueRow=CustomFieldData::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        if($customField['input_type']=='autocomplete'){
          $value=$valueRow['input_value'];
        }else{
          if($valueRow['input_value']!=''){
            if($customField['input_type']=='date'){
              $value=formatDate($valueRow['input_value']);
            }elseif($customField['input_type']=='daterange'){
              list($from,$to)=explode(" - ",$valueRow['input_value']);
              $value=Yii::t('app','{from} - {to}',['from'=>formatDate($from),'to'=>formatDate($to)]);
            }elseif($customField['input_type']=='time'){
              $value=date("h:i a",strtotime($valueRow['input_value']));
            }elseif($customField['input_type']=='textarea'){
              $value=nl2br($valueRow['input_value']);
            }else{
              $value=$valueRow['input_value'];
            }
          }
        }
      }
    }
    //Select & Checkbox
    if($customField['input_type']=='select' || $customField['input_type']=='checkbox'){
      if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
        $multipleDataRows=CustomFieldDataMultiple::select(['option_id'])
        ->where([
          'module_type'=>$model->moduleTypeId,
          'module_id'=>$model->id,
          'input_field_id'=>$customField['id']
        ])
        ->get();
        if($multipleDataRows!=null){
          foreach($multipleDataRows as $multipleDataRow){
            $value.=($value!='' ? ', ' : '').getOptionTitleById($multipleDataRow['option_id'],$customField);
          }
        }
      }else{
        $valueRow=CustomFieldData::where([
          'module_type'=>$model->moduleTypeId,
          'module_id'=>$model->id,
          'input_field_id'=>$customField['id']
        ])
        ->first();
        if($valueRow!=null){
          $value=getOptionTitleById($valueRow['input_value'],$customField);
        }
      }
    }
    if($customField['input_type']=='radio'){
      $valueRow=CustomFieldData::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        $value=getOptionTitleById($valueRow['input_value'],$customField);
      }
    }
    if($customField['input_type']=='timerange'){
      $valueRow=CustomFieldDatetimeInfo::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        if($valueRow['start_time']!='')$value=formatTime($valueRow['start_time']);
        if($valueRow['start_time']!='' && $valueRow['end_time']!='')$value.=' - ';
        if($valueRow['end_time']!='')$value.=formatTime($valueRow['end_time']);
      }
    }
    return $value;
  }
}

if (!function_exists('getInputFielSavedValue')) {

  /**
  * get saved value of input
  * @return string
  */
  function getInputFielSavedValue($model,$customField)
  {
    $value='';
    if(
      $customField['input_type']=='text' ||
      $customField['input_type']=='autocomplete' ||
      $customField['input_type']=='numberinput' ||
      $customField['input_type']=='websiteinput' ||
      $customField['input_type']=='emailinput' ||
      $customField['input_type']=='date' ||
      $customField['input_type']=='daterange' ||
      $customField['input_type']=='time' ||
      $customField['input_type']=='textarea' ||
      $customField['input_type']=='tinymce' ||
      $customField['input_type']=='radio'
    ){
      $valueRow=CustomFieldData::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        if($customField['input_type']=='autocomplete'){
          $model->input_field[$customField['id']]['id']=$valueRow['input_org_value'];
          $model->input_field[$customField['id']]['title']=$valueRow['input_value'];
          $value = ['id'=>$valueRow['input_org_value'],'title'=>$valueRow];
        }else{
          $model->input_field[$customField['id']]=$valueRow['input_value'];
          $value = $valueRow['input_value'];
        }
      }
    }
    //Select & Checkbox
    if($customField['input_type']=='select' || $customField['input_type']=='checkbox'){
      if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
        $model->input_field[$customField['id']]=getCFMultipleIdz($model,$customField);
      }else{
        $valueRow=CustomFieldData::where([
          'module_type'=>$model->moduleTypeId,
          'module_id'=>$model->id,
          'input_field_id'=>$customField['id']
        ])
        ->first();
        if($valueRow!=null){
          $model->input_field[$customField['id']]=$valueRow['input_value'];
          $value = $valueRow['input_value'];
        }
      }
    }
    if($customField['input_type']=='timerange'){
      $valueRow=CustomFieldDatetimeInfo::where([
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id,
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        $model->input_field[$customField['id']]['start']=$valueRow['start_time'];
        $model->input_field[$customField['id']]['end']=$valueRow['end_time'];
        $value=['start'=>$valueRow['start_time'],'end'=>$valueRow];
      }
    }
    return $value;
  }
}

if (!function_exists('searchInNameFields')) {
  /**
  * search in name fields
  */
  function searchInNameFields($moduleTypeId,$query,$keyword)
  {
    $customField = CustomField::where('id',getSetting($moduleTypeId))->first();
    if($customField!=null){
      $query->orWhereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){

        if(($customField->input_type=='select' && $customField->selection_type==2) || $customField->input_type=='checkbox' || $customField->input_type=='radio'){
          $query->from((new CustomFieldDataMultiple)->getTable())
          ->selectRaw('module_id')
          ->where('module_type',$moduleTypeId)
          ->where('option_id_value', 'like', '%'.$keyword.'%');
        }else{
          $query->from((new CustomFieldData)->getTable())
          ->selectRaw('module_id')
          ->where('module_type',$moduleTypeId)
          ->where(function ($query) use ($keyword){
            $query->where('input_value', 'like', '%'.$keyword.'%')
            ->orWhere('input_org_value', 'like', '%'.$keyword.'%');
          });
        }
      });
    }
  }
}

if (!function_exists('searchInCustomFields')) {
  /**
  * return status array
  */
  function searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds)
  {

    dd('w');
    $customFields = getCustomFieldsByModule($moduleTypeId);
    $query->where(function($query) use ($customFields,$moduleTypeId,$keyword,$srchFlds){
      if($srchFlds!=null){
        $nn=1;
        foreach($srchFlds as $key=>$val){
          if($nn==1){
            $query->orWhere($val,'like','%'.$keyword.'%');
          }else{
            $query->orWhere($val,'like','%'.$keyword.'%');
          }
          $nn++;
        }
      }
      if($moduleTypeId=='opportunity'){
        //Search In Prospects
        $oppSourceModulesArr = getModulesHavingOpportunity();
        if($oppSourceModulesArr!=null){
          foreach($oppSourceModulesArr as $key=>$val){
            $subQuerymodelClass = getModelClass()[$val];
            $query->orWhere(function($query) use ($val, $subQuerymodelClass, $keyword){
              $query->where('module_type',$val)
              ->whereIn('module_id',function($query) use ($val, $subQuerymodelClass, $keyword){
                $query->from((new $subQuerymodelClass)->getTable())
                ->selectRaw('id')
                ->where('full_name', 'like', '%'.$keyword.'%')
                ->orWhere('email', 'like', '%'.$keyword.'%')
                ->orWhere('phone', 'like', '%'.$keyword.'%')
                ->orWhere('company_name', 'like', '%'.$keyword.'%');
              });
            });
          }
        }
      }

      if($customFields!=null){
        foreach($customFields as $customField){
          $query->orWhereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){
            if(($customField->input_type=='select' && $customField->selection_type==2) || $customField->input_type=='checkbox' || $customField->input_type=='radio'){
              $query->from((new CustomFieldDataMultiple)->getTable())
              ->selectRaw('module_id')
              ->where('module_type',$moduleTypeId)
              ->where('input_field_id',$customField->id)
              ->where('option_id_value', 'like', '%'.$keyword.'%');
            }else{
              $query->from((new CustomFieldData)->getTable())
              ->selectRaw('module_id')
              ->where('module_type',$moduleTypeId)
              ->where('input_field_id',$customField->id)
              ->where(function ($query) use ($keyword){
                $query->where('input_value', 'like', '%'.$keyword.'%')
                ->orWhere('input_org_value', 'like', '%'.$keyword.'%');
              });
            }
          });
        }
      }
    });
  }
}

if (!function_exists('advanceSearchInCustomFields')) {
  /**
  * return status array
  */
  function advanceSearchInCustomFields($moduleTypeId,$query,$input_field)
  {
    $customFields = getCustomFieldsByModule($moduleTypeId);
    if($customFields!=null){
      foreach($customFields as $customField){
        $keyword = isset($input_field[$customField['id']]) ? $input_field[$customField['id']] : '';
        if($keyword!=''){
          if($customField->input_type=='select' || $customField->input_type=='checkbox' || $customField->input_type=='radio'){
            if($customField->input_type=='select' && $customField->selection_type==2 || $customField->input_type=='checkbox'){
              if(is_array($keyword)){
                foreach($keyword as $kwk=>$kwv){
                  $query->orWhereIn('id',function($query) use ($moduleTypeId, $kwv, $customField){
                    $query->from((new CustomFieldDataMultiple)->getTable())
                    ->selectRaw('module_id')
                    ->where('module_type',$moduleTypeId)
                    ->where('option_id_value', 'like', '%'.$kwv.'%');
                  });
                }
              }else{
                $query->orWhereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){
                  $query->from((new CustomFieldDataMultiple)->getTable())
                  ->selectRaw('module_id')
                  ->where('module_type',$moduleTypeId)
                  ->where('option_id_value', 'like', '%'.$keyword.'%');
                });
              }
            }else{
              if(is_array($keyword)){
                foreach($keyword as $kwk=>$kwv){
                  $query->orWhereIn('id',function($query) use ($moduleTypeId, $kwv, $customField){
                    $query->from((new CustomFieldData)->getTable())
                    ->selectRaw('module_id')
                    ->where('module_type',$moduleTypeId)
                    ->where(function ($query) use ($kwv){
                      $query->where('input_value', $kwv);
                    });
                  });
                }
              }else{
                $query->orWhereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){
                  $query->from((new CustomFieldData)->getTable())
                  ->selectRaw('module_id')
                  ->where('module_type',$moduleTypeId)
                  ->where(function ($query) use ($keyword){
                    $query->where('input_value', $keyword);
                  });
                });
              }
            }
          }else{
            $query->orWhereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){
              $query->from((new CustomFieldData)->getTable())
              ->selectRaw('module_id')
              ->where('module_type',$moduleTypeId)
              ->where(function ($query) use ($keyword){
                $query->where('input_value', 'like', '%'.$keyword.'%')
                ->orWhere('input_org_value', 'like', '%'.$keyword.'%');
              });
            });
          }
        }
      }
    }
  }
}
// $query->whereIn('id',function($query) use ($moduleTypeId, $keyword, $customField){
//
// });
if (!function_exists('getCustomFieldsListByModule')) {
  /**
  * return status array
  */
  function getCustomFieldsListByModule($module)
  {
    $customFields = getCustomFieldsByModule($module);
    return $customFields->pluck("title","id")->toArray();
  }
}

if (!function_exists('getCustomFieldsByModule')) {
  /**
  * return status array
  */
  function getCustomFieldsByModule($module)
  {
    return CustomField::where('status',1)
    ->where(function($query) use ($module){
      $query->whereIn('id',function($query) use ($module){
        if(is_array($module)){
          $query->from((new CustomFieldModule)->getTable())
          ->selectRaw('custom_field_id')
          ->whereIn('module_type', $module);
        }else{
          $query->from((new CustomFieldModule)->getTable())
          ->selectRaw('custom_field_id')
          ->where('module_type', $module);
        }
      });
    })
    ->orderBy('sort_order','asc')->get();
  }
}

if (!function_exists('getUniqueCustomFieldsByModule')) {
  /**
  * return status array
  */
  function getUniqueCustomFieldsByModule($module)
  {
    return CustomField::where(['is_unique'=>1,'status'=>1])
    ->where(function($query) use ($module){
      $query->whereIn('id',function($query) use ($module){
        $query->from((new CustomFieldModule)->getTable())
        ->selectRaw('custom_field_id')
        ->where('module_type', $module);
      });
    })
    ->orderBy('sort_order','asc')->get();
  }
}

if (!function_exists('getCustomFilterFieldsByModule')) {
  /**
  * return status array
  */
  function getCustomFilterFieldsByModule($module)
  {
    return CustomField::where('status',1)
    ->where('adv_filter',1)
    ->where(function($query) use ($module){
      $query->whereIn('id',function($query) use ($module){
        $query->from((new CustomFieldModule)->getTable())
        ->selectRaw('custom_field_id')
        ->where('module_type', $module);
      });
    })
    ->orderBy('sort_order','asc')->get();
  }
}

if (!function_exists('getGridViewColumns')) {
  /**
  * columns for grid view for a module
  */
  function getGridViewColumns($type)
  {
    return CustomField::join((new CustomFieldModule)->getTable(),(new CustomFieldModule)->getTable().'.custom_field_id','=',(new CustomField)->getTable().'.id')
    ->where([
      (new CustomFieldModule)->getTable().'.module_type'=>$type,
      (new CustomField)->getTable().'.show_in_grid'=>1,
      (new CustomField)->getTable().'.status'=>1
    ])
    ->orderBy('sort_order','asc')->get();
  }
}

if (!function_exists('getInputListArr')) {
  /**
  * generate selection list for dropdown, checkbox and radio
  * @return array
  */
  function getInputListArr($inputType)
  {
    if($inputType['predefined_list_id']!=null){
      return getPredefinedListOptionsArr($inputType['predefined_list_id']);
    }else{
      return getInputSubOptionListArr($inputType['id']);
    }
  }
}

if (!function_exists('getPredefinedListOptions')) {
  /**
  * All predefined lists
  *
  * @return array
  */
  function getPredefinedListOptions($id)
  {
    return PredefinedList::select([(new PredefinedList)->getTable().'.id','title'])
    ->join((new PredefinedListDetail)->getTable(),(new PredefinedListDetail)->getTable().'.list_id','=',(new PredefinedList)->getTable().'.id')
    ->where(['parent_id' => $id, 'lang'=>App::currentLocale(), 'status' => 1])
    ->orderBy('title','asc')->get();
  }
}

if (!function_exists('getPredefinedListOptionsArr')) {
  /**
  * All predefined for dropdown
  *
  * @return array
  */
  function getPredefinedListOptionsArr($id)
  {
    return PredefinedList::select([(new PredefinedList)->getTable().'.id','title'])
    ->join((new PredefinedListDetail)->getTable(),(new PredefinedListDetail)->getTable().'.list_id','=',(new PredefinedList)->getTable().'.id')
    ->where(['parent_id' => $id, 'lang'=>App::currentLocale(), 'status' => 1])
    ->orderBy('title','asc')->get()->pluck('title','id')->toArray();
  }
}

if (!function_exists('getPredefinedListItemByTitle')) {
  /**
  * Get Predefined Item by title
  *
  * @return array
  */
  function getPredefinedListItemByTitle($listId,$title)
  {
    $id = 0;
    $pdItem = PredefinedList::join((new PredefinedListDetail)->getTable(),(new PredefinedListDetail)->getTable().'.list_id','=',(new PredefinedList)->getTable().'.id')
    ->where(['parent_id'=>$listId,'lang'=>App::currentLocale()])->whereRaw('LOWER(title) = ?',[trim(strtolower($title))])->first();
    if($pdItem!=null){
      $id=$pdItem->id;
    }
    return $id;
  }
}

if (!function_exists('getInputSubOptionList')) {
  /**
  * generate selection list for dropdown, checkbox and radio
  * @return array
  */
  function getInputSubOptionList($id)
  {
    return CustomFieldSubOption::select(['id','title'])
    ->where(['custom_field_id' => $id, 'status' => 1])
    ->orderBy('sort_order','asc')->get();
  }
}

if (!function_exists('getInputSubOptionListArr')) {
  /**
  * generate selection list for dropdown, checkbox and radio
  * @return array
  */
  function getInputSubOptionListArr($id)
  {
    return CustomFieldSubOption::select(['id','title'])
    ->where(['custom_field_id' => $id, 'status' => 1])
    ->orderBy('sort_order','asc')->get()->pluck('title','id')->toArray();
  }
}

if (!function_exists('getGridValue')) {
  /**
  * retuen value for grid column
  */
  function getGridValue($moduleTypeId,$model,$customField)
  {
    $value='';
    if(
      $customField['input_type']=='text' ||
      $customField['input_type']=='autocomplete' ||
      $customField['input_type']=='numberinput' ||
      $customField['input_type']=='websiteinput' ||
      $customField['input_type']=='emailinput' ||
      $customField['input_type']=='date' ||
      $customField['input_type']=='daterange' ||
      $customField['input_type']=='time' ||
      $customField['input_type']=='textarea' ||
      $customField['input_type']=='tinymce' ||
      $customField['input_type']=='radio'
    ){
      $valueRow=CustomFieldData::where([
        'module_type'=>$moduleTypeId,
        'module_id'=>$model->id,
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        if($customField['input_type']=='autocomplete'){
          $value=$valueRow['input_value'];
        }else{
          if($valueRow['input_value']!=''){
            if($customField['input_type']=='date'){
              $value=formatDate($valueRow['input_value']);
            }elseif($customField['input_type']=='daterange'){
              list($from,$to)=explode(" - ",$valueRow['input_value']);
              $value=Yii::t('app','{from} - {to}',['from'=>formatDate($from),'to'=>formatDate($to)]);
            }elseif($customField['input_type']=='time'){
              $value=date("h:i a",strtotime($valueRow['input_value']));
            }elseif($customField['input_type']=='textarea'){
              $value=nl2br($valueRow['input_value']);
            }else{
              $value=$valueRow['input_value'];
            }
          }
        }
      }
    }
    //Select & Checkbox
    if($customField['input_type']=='select' || $customField['input_type']=='checkbox'){
      if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
        $multipleDataRows=CustomFieldDataMultiple::select(['option_id','option_id_value'])
        ->where([
          'module_type'=>$moduleTypeId,
          'module_id'=>$model['id'],
          'input_field_id'=>$customField['id']
        ])
        ->get();
        if($multipleDataRows!=null){
          foreach($multipleDataRows as $multipleDataRow){
            $thisVal = getOptionTitleById($multipleDataRow['option_id'],$customField);
            if($thisVal=='' && $multipleDataRow['option_id_value']!=''){
              $thisVal = $multipleDataRow['option_id_value'];
            }
            if($thisVal=='' && $multipleDataRow['option_id']!=''){
              $thisVal = $multipleDataRow['option_id'];
            }
            $value.=($value!='' ? ', ' : '').$thisVal;
          }
        }
      }else{
        $valueRow=CustomFieldData::where([
          'module_type'=>$moduleTypeId,
          'module_id'=>$model->id,
          'input_field_id'=>$customField['id']
        ])
        ->first();
        if($valueRow!=null){
          $value=getOptionTitleById($valueRow['input_value'],$customField);
        }
      }
    }
    if($customField['input_type']=='radio'){
      $valueRow=CustomFieldData::where([
        'module_type'=>$moduleTypeId,
        'module_id'=>$model['id'],
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        $value=getOptionTitleById($valueRow['input_value'],$customField);
      }
    }
    if($customField['input_type']=='timerange'){
      $valueRow=CustomFieldDatetimeInfo::where([
        'module_type'=>$moduleTypeId,
        'module_id'=>$model['id'],
        'input_field_id'=>$customField['id']
      ])
      ->first();
      if($valueRow!=null){
        if($valueRow['start_time']!='')$value=formatTime($valueRow['start_time']);
        if($valueRow['start_time']!='' && $valueRow['end_time']!='')$value.=' - ';
        if($valueRow['end_time']!='')$value.=formatTime($valueRow['end_time']);
      }
    }
    return $value;
  }
}

if (!function_exists('getCustomerInputTypesByModule')) {
  /**
  * return status array
  */
  function getCustomerInputTypesByModule($type)
  {
    return CustomField::select([
      (new CustomField)->getTable().'.id',
      (new CustomField)->getTable().'.input_type',
      (new CustomField)->getTable().'.title',
      (new CustomField)->getTable().'.colum_width',
      (new CustomField)->getTable().'.hint_text',
      (new CustomField)->getTable().'.autocomplete_text',
      (new CustomField)->getTable().'.predefined_list_id',
      (new CustomField)->getTable().'.selection_type',
      (new CustomField)->getTable().'.is_required',
    ])
    ->join((new CustomFieldModule)->getTable(),(new CustomFieldModule)->getTable().'.custom_field_id', '=', (new CustomField)->getTable().'.id')
    ->where([
      (new CustomFieldModule)->getTable().'.module_type'=>$type,
      (new CustomField)->getTable().'.status'=>1
    ])
    ->orderBY('sort_order','asc')->get();
  }
}

if (!function_exists('saveCustomField')) {
  /**
  * save custom fields info
  */
  function saveCustomField($model,$request,$source='',$duplicate='ignore')
  {
    $customInputFields=$customInputFields=getCustomerInputTypesByModule($model->moduleTypeId);
    if($customInputFields!=null){
      foreach($customInputFields as $customField){
        $canSave=true;

        // if($duplicate=='ignore'){
        //   //Check Unique type
        //   if($customField['is_unique']==1){
        //     $inputValue = isset($model->input_field[$customField['id']]['title']) ? $model->input_field[$customField['id']]['title'] : '';
        //     $inputOrgValue = isset($model->input_field[$customField['id']]['id']) ? $model->input_field[$customField['id']]['id'] : '';
        //     //Check if this fields already exists
        //     $checkDuplicate = CustomFieldData::where([
        //       'module_type'=>$model->moduleTypeId,
        //       'module_id'=>$model->id,
        //       'input_field_id'=>$customField['id'],
        //       'input_value'=>$inputOrgValue,
        //       'input_org_value'=>$inputOrgValue,
        //     ])
        //     ->first();
        //     if($checkDuplicate!=null){
        //       $canSave=false;
        //     }
        //   }
        // }
        if($canSave){
          //Save Simple Data
          $fieldValue=CustomFieldData::where([
            'module_type'=>$model->moduleTypeId,
            'module_id'=>$model->id,
            'input_field_id'=>$customField['id']
          ])
          ->first();
          if($fieldValue==null){
            $fieldValue=new CustomFieldData;
            $fieldValue->company_id=$model->company_id;
            $fieldValue->module_type=$model->moduleTypeId;
            $fieldValue->module_id=$model->id;
            $fieldValue->input_field_id=$customField['id'];
          }
          if($customField['input_type']=='autocomplete'){
            $fieldValue->input_value=isset($model->input_field[$customField['id']]['title']) ? $model->input_field[$customField['id']]['title'] : '';
            $fieldValue->input_org_value=isset($model->input_field[$customField['id']]['id']) ? $model->input_field[$customField['id']]['id'] : '';
          }else{
            if(isset($model->input_field[$customField['id']]) && !is_array($model->input_field[$customField['id']])){
              $fieldValue->input_value=trim($model->input_field[$customField['id']]);
            }
          }
          if($customField['input_type']=='time'){
            $fieldValue->input_org_value=getOriginalTime($fieldValue->input_value);
          }
          if($customField['input_type']=='select' || $customField['input_type']=='radio' || $customField['input_type']=='checkbox'){
            if($source=='import'){
              //Check if its in predefined list.
              $pdItem = PredefinedList::join((new PredefinedListDetail)->getTable(),(new PredefinedListDetail)->getTable().'.list_id','=',(new PredefinedList)->getTable().'.id')
              ->where(['parent_id'=>$customField->predefined_list_id,'lang'=>App::currentLocale()])->whereRaw('LOWER(title) = ?',[trim(strtolower($fieldValue->input_value))])->first();
              if($pdItem==null){
                if($fieldValue->input_value!=''){
                  $pdItem=new PredefinedList;
                  $pdItem->parent_id=$customField->predefined_list_id;
                  $pdItem->status=1;
                  $pdItem->title=['en'=>$fieldValue->input_value];
                  if($pdItem->save()){
                    $fieldValue->input_value=$pdItem->id;
                  }
                }
              }else{
                $fieldValue->input_value=$pdItem->id;
              }
            }
            $fieldValue->input_org_value=getOptionTitleById($fieldValue->input_value,$customField,$model);
          }
          $fieldValue->save();

          //Save Select Multiple values
          if(($customField['input_type']=='select' && $customField['selection_type']==2) || $customField['input_type']=='checkbox'){
            if(isset($model->input_field[$customField['id']]) && $model->input_field[$customField['id']]!=null){
              CustomFieldDataMultiple::where([
                'module_type'=>$model->moduleTypeId,
                'module_id'=>$model->id,
                'input_field_id'=>$customField['id']
              ])
              ->whereNotIn('option_id',$model->input_field[$customField['id']])->delete();
              foreach($model->input_field[$customField['id']] as $key=>$val){
                //Check if its in custom_field_sub_option else add it
                $cfSubOptId = $val;
                $checkIfSubOptionExists = CustomFieldSubOption::where([
                  'custom_field_id'=>$customField['id'],
                  'title'=>trim($val),
                  ])->first();
                  if($checkIfSubOptionExists==null){
                    $newSubOpt = new CustomFieldSubOption;
                    $newSubOpt->custom_field_id = $customField['id'];
                    $newSubOpt->title = $val;
                    $newSubOpt->save();
                    $cfSubOptId = $newSubOpt->id;
                  }
                  $childRow=CustomFieldDataMultiple::where([
                    'company_id'=>$model->company_id,
                    'module_type'=>$model->moduleTypeId,
                    'module_id'=>$model->id,
                    'input_field_id'=>$customField['id'],
                    'option_id'=>$cfSubOptId
                  ])
                  ->first();
                  if($childRow==null){
                    $childRow=new CustomFieldDataMultiple;
                    $childRow->company_id=$model->company_id;
                    $childRow->module_type=$model->moduleTypeId;
                    $childRow->module_id=$model->id;
                    $childRow->input_field_id=$customField['id'];
                  }
                  $childRow->option_id=$cfSubOptId;
                  $childRow->option_id_value=getOptionTitleById($cfSubOptId,$customField,$model);
                  $childRow->save();
                }
              }
            }
            //Save Date Time Info
            if($customField['input_type']=='timerange'){
              $dateTime=CustomFieldDatetimeInfo::where([
                'module_type'=>$model->moduleTypeId,
                'module_id'=>$model->id,
                'input_field_id'=>$customField['id']
              ])
              ->first();
              if($dateTime==null){
                $dateTime=new CustomFieldDatetimeInfo;
                $dateTime->company_id=$model->company_id;
                $dateTime->module_type=$model->moduleTypeId;
                $dateTime->module_id=$model->id;
                $dateTime->input_field_id=$customField['id'];
              }
              $dateTime->start_time=isset($model->input_field[$customField['id']]['start']) ? $model->input_field[$customField['id']]['start'] : '';
              $dateTime->start_org_time=getOriginalTime($dateTime->start_time);
              $dateTime->end_time=isset($model->input_field[$customField['id']]['end']) ? $model->input_field[$customField['id']]['end'] : '';
              $dateTime->end_org_time=getOriginalTime($dateTime->end_time);
              $dateTime->save();
            }

            //Save Google Map
            // if($customField['input_type']=='googlemap'){
            //   $googleMap=CustomFieldGoogleMapInfo::find()
            // 	->where([
            // 		'company_id'=>$model->company_id,
            // 		'module_type'=>$model->moduleTypeId,
            // 		'module_id'=>$model->id,
            // 		'input_field_id'=>$customField['id']
            // 	])
            // 	->one();
            //   if($googleMap==null){
            //     $googleMap=new CustomFieldGoogleMapInfo;
            //     $googleMap->company_id=$model->company_id;
            //     $googleMap->module_type=$model->moduleTypeId;
            //     $googleMap->module_id=$model->id;
            //     $googleMap->input_field_id=$customField['id'];
            //   }
            //   $googleMap->map_location=isset($model->map_location) ? $model->map_location : '';
            //   $googleMap->map_lat=isset($model->map_lat) ? $model->map_lat : '';
            //   $googleMap->map_lng=isset($model->map_lng) ? $model->map_lng : '';
            //   $googleMap->save();
            // }
          }
        }
      }
    }
  }

  if (!function_exists('dropDownYesNo')) {
    /**
    * return status array
    */
    function dropDownYesNo()
    {
      return [
        '1' => 'Yes',
        '0' => 'No',
      ];
    }
  }

  function singleMultiple(){
    return[
      '1' => 'Single',
      '2' => 'Multiple',
    ];
  }
  if (!function_exists('columnWidth')) {
    /**
    * return status array
    */
    function columnWidth()
    {
      return [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
        '11' => '11',
        '12' => '12',
      ];
    }
  }

  if (!function_exists('dropDownList')) {
    /**
    * return status array
    */
    function dropDownList()
    {
      return [
        'text' => 'Text Field',
        // 'autocomplete' => 'Autocomplete',
        'textarea' => 'Text Area',
        // 'tinymce' => 'Text Editor',
        'select' => 'Drop Down',
        'checkbox' => 'Checkbox',
        'radio' => 'Radio Buttons',
        'numberinput' => 'Number Input',
        'websiteinput' => 'Url Input',
        'emailinput' => 'Email Input',
        'date' => 'Date Input',
        // 'daterange' => 'Date Range Input',
        // 'time' => 'Time Input',
        // 'timerange' => 'Time Range Input',
      ];
    }
  }

  if (!function_exists('CFFilterTypes')) {
    /**
    * return status array
    */
    function CFFilterTypes()
    {
      return [
        'text' => [
          'text' => 'Simple'
        ],
        'textarea' => [
          'text' => 'Simple'
        ],
        'select' => [
          'select' => 'Simple',
          'selectpicker' => 'Multi Selection',
        ],
        'checkbox' => [
          'select' => 'Simple',
          'selectpicker' => 'Multi Selection',
        ],
        'radio' => [
          'select' => 'Simple',
          'selectpicker' => 'Multi Selection',
        ],
        'radio' => [
          'select' => 'Simple',
          'selectpicker' => 'Multi Selection',
        ],
        'numberinput' => [
          'numberinput' => 'Simple',
          'inputrange' => 'Range',
        ],
        'websiteinput' => [
          'websiteinput' => 'Simple',
        ],
        'emailinput' => [
          'emailinput' => 'Simple',
        ],
        'date' => [
          'date' => 'Simple',
          'daterange' => 'Range',
        ],
      ];
    }
  }

  function getPredefinedList($parent=0)
  {
    return PredefinedList::select((new PredefinedList)->getTable().'.id',(new PredefinedListDetail)->getTable().'.title')
    ->join((new PredefinedListDetail)->getTable(), (new PredefinedListDetail)->getTable().'.list_id', '=', (new PredefinedList)->getTable().'.id')
    ->where((new PredefinedList)->getTable().'.parent_id', $parent)
    ->get()->pluck('title', 'id')->toArray();
  }

  function getMultipleOptions($id=0)
  {
    return CustomFieldSubOption::select((new CustomFieldSubOption)->getTable().'.id','title')->where('custom_field_id', $id)->get()->pluck('title', 'id')->toArray();
  }

if (!function_exists('getCustomFieldFilterOptions')) {
  /**
  * return filter options row
  */
  function getCustomFieldFilterOptions($id)
  {
    $row = CustomFieldFilterOption::where('input_field_id',$id)->first();
    if($row!=null){
      return $row->toArray();
    }
    return null;
  }
}

if (!function_exists('getCustomFieldInputValue')) {
  /**
  * return filter options row
  */
  function getCustomFieldInputValue($moduleTypeId,$module_id,$id)
  {
    $row = CustomFieldData::where(['module_type'=>$moduleTypeId,'module_id'=>$module_id,'input_field_id'=>$id])->first();
    if($row!=null){
      return $row->input_value;
    }
    return '';
  }
}

if (!function_exists('getCustomFiedAttachedModules')) {
  /**
  * return modules attached to custom field
  */
  function getCustomFiedAttachedModules($id)
  {
    $modulesArr=[];
    $customFieldModules = CustomFieldModule::where('custom_field_id',$id)->get();
    if($customFieldModules!=null){
      foreach($customFieldModules as $customFieldModule){
        $modulesArr[]='<span class="badge badge-success mb-1">'.getModuleTypeArr()[$customFieldModule->module_type].'</span>';
      }
    }
    return implode(" ",$modulesArr);
  }
}

if (!function_exists('getCustomStatsFieldsByModule')) {
  /**
  * return status array
  */
  function getCustomStatsFieldsByModule($module)
  {
    return CustomField::where('status',1)
    ->where('enable_stats',1)
    ->where(function($query) use ($module){
      $query->whereIn('id',function($query) use ($module){
        $query->from((new CustomFieldModule)->getTable())
        ->selectRaw('custom_field_id')
        ->where('module_type', $module);
      });
    })
    ->orderBy('sort_order','asc')->get();
  }
}

if (!function_exists('getCustomStatsFieldsByModuleListArr')) {
  /**
  * return status array
  */
  function getCustomStatsFieldsByModuleListArr($module)
  {
    $statsFieldArray = [];
    $fields = getCustomStatsFieldsByModule($module);
    if($fields!=null){
      foreach($fields as $field){
        $fieldOptions = getInputListArr($field);;

        $statsFieldArray[]=['id'=>$field['id'],'title'=>$field['title'],'options'=>$fieldOptions];
      }
    }
    return $statsFieldArray;
  }
}

if (!function_exists('getCustomFieldStats')) {
  /**
  * return modules attached to custom field
  */
  function getCustomFieldStats($mmodule,$model)
  {
    $modelClass = getModelClass()[$mmodule];
    $statsArr=[];
    $module = $mmodule;
    if($mmodule=='lead')$module = 'opportunity';

    $fields = getCustomStatsFieldsByModule($module);
    if($fields!=null){
      foreach($fields as $field){
        // $totalCount = CustomFieldData::where('module_type',$module)->where('input_field_id',$field['id'])->count('id');
        $query=null;
        $query = $modelClass::whereIn('id',function($query) use ($module,$field){
          $query->select('module_id')
          ->from((new CustomFieldData)->getTable())
          ->where('module_type',$module)
          ->where('input_field_id',$field['id']);
        });
        if($mmodule=='opportunity')$query->where('rec_type','o');
        if($mmodule=='lead')$query->where('rec_type','l');
        //Created At
        if($model->created_at!=null && $model->created_at!=''){
          if(strpos($model->created_at," - ")){
            $query->where(function($query) use($model){
              list($start_date,$end_date)=explode(" - ",$model->created_at);
              $query->where('created_at','>=',$start_date)
              ->where('created_at','<=',$end_date);
            });
          }else{
            $query->where('created_at',$model->created_at);
          }
        }
        //Created By
        if($model->created_by!=null)$query->whereIn('created_by',$model->created_by);
        $totalCount = $query->count('id');
        $data=[];
        //Get Options
        $fieldOptions = getInputListArr($field);
        foreach ($fieldOptions as $foKey=>$foTitle) {
          // $optionCount = CustomFieldData::where(['module_type'=>$module,'input_field_id'=>$field['id'],'input_value'=>$foKey])->count('id');
          $query=null;
          $query = $modelClass::whereIn('id',function($query) use($module,$field,$foKey){
            $query->select('module_id')
            ->from((new CustomFieldData)->getTable())
            ->where('module_type',$module)
            ->where('input_field_id',$field['id'])
            ->where('input_value',$foKey);
          });
          if($mmodule=='opportunity')$query->where('rec_type','o');
          if($mmodule=='lead')$query->where('rec_type','l');
          //Created At
          if($model->created_at!=null && $model->created_at!=''){
            if(strpos($model->created_at," - ")){
              $query->where(function($query) use($model){
                list($start_date,$end_date)=explode(" - ",$model->created_at);
                $query->where('created_at','>=',$start_date)
                ->where('created_at','<=',$end_date);
              });
            }else{
              $query->where('created_at',$model->created_at);
            }
          }
          //Created By
          if($model->created_by!=null)$query->whereIn('created_by',$model->created_by);
          $optionCount = $query->count('id');
          if($optionCount>0){
            $data[]=['label'=>$foTitle.' ('.$optionCount.')', 'data'=>$optionCount];//, 'color'=> "#8950FC"
          }
        }
        if($data!=null){
          $statsArr[]=['id'=>$field['id'],'heading'=>$field['title'],'totalCount'=>$totalCount,'data'=>$data];
        }
      }
    }
    return $statsArr;
  }
}

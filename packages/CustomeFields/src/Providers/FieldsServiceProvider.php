<?php

namespace Wisdom\CustomeFields\Providers;

use Illuminate\Support\ServiceProvider;
use Wisdom\CustomeFields\Models\CustomField;
use Wisdom\CustomeFields\Observers\CustomFieldObserver;
use Wisdom\CustomeFields\View\Components\CustomeFields;
use Wisdom\CustomeFields\View\Components\FrontendNontaFields;
use Wisdom\CustomeFields\View\Components\FrontendOnlytaFields;
use Wisdom\CustomeFields\View\Components\CustomFieldsMixed;
use Wisdom\CustomeFields\View\Components\CustomeFieldDetails;

class FieldsServiceProvider extends ServiceProvider
{
  public function boot()
  {
    CustomField::observe(CustomFieldObserver::class);

    $this->loadRoutesFrom(__DIR__.'/../../routes/custom_fields.php');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'customfields');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'customfields');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

    $this->loadViewComponentsAs('customfields', [
      CustomeFields::class,
      FrontendNontaFields::class,
      FrontendOnlytaFields::class,
      CustomFieldsMixed::class,
      CustomeFieldDetails::class,
    ]);
  }
}

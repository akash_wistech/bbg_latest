<?php

namespace Wisdom\CustomeFields\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomFieldData extends Model
{
	protected $table = 'custom_field_data';
}

<?php

namespace Wisdom\CustomeFields\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModuleCustomFieldsOption extends Model
{
    public $timestamps = false;
    use HasFactory;
}

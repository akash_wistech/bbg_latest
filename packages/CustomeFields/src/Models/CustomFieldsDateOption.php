<?php

namespace Wisdom\CustomeFields\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomFieldsDateOption extends Model
{
	protected $table = 'custom_fields_date_option';
	public $timestamps = false;
}

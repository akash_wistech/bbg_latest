<?php

namespace Wisdom\CustomeFields\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomFieldFilterOption extends Model
{
	protected $table = 'custom_field_filter_option';
	public $timestamps = false;
}

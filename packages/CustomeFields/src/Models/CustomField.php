<?php

namespace Wisdom\CustomeFields\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;

class CustomField extends Model
{
	public $search_input_type,$search_range_from,$search_range_to,$search_range_interval;
	public $default_today_date,$date_selection_type;
	use HasFactory, Blameable, SoftDeletes;
	protected $table = 'custom_field';

	public function module()
	{
		return $this->hasMany(CustomFieldModule::class);
	}

	public function sub_options()
	{
		return $this->belongsTo(CustomFieldSubOption::class, 'custom_field_id');
	}

	/*
	* Filter Options
	*/
	public function filterOptions()
	{
		return $this->hasOne(CustomFieldFilterOption::class,'input_field_id','id');
	}

	/*
	* Filter Options
	*/
	public function dateOptions()
	{
		return $this->hasOne(CustomFieldsDateOption::class,'input_field_id','id');
	}
}

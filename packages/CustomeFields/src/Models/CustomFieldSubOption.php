<?php

namespace Wisdom\CustomeFields\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomFieldSubOption extends Model
{
    use HasFactory;
    protected $table = 'custom_field_sub_option';
}

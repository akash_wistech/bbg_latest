<?php
namespace Naeemawan\ModuleSocialMedia\View\Components;

use Auth;
use Illuminate\View\Component;
use Naeemawan\MultipleEmailPhone\Models\ModuleEmail;
use Naeemawan\MultipleEmailPhone\Models\ModuleNumber;

class ModuleSocialMediaFieldDetails extends Component
{
    /**
     * model
     */
    public $model;

    /**
     * Create the component instance.
     *
     * @param  string  $type
     * @param  string  $message
     * @return void
     */
    public function __construct($model)
    {
     $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
      $links = getModuleSavedSMLinks($this->model);

        return view('namod-socialmedia::components.module-social-media-field-details', [
          'links' => $links,
          'model'=>$this->model
        ]);
    }
}

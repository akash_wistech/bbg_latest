<?php
namespace NaeemAwan\ModuleSocialMedia\View\Components;

use Illuminate\View\Component;

class ModuleSocialMediaFields extends Component
{
  /**
  * card
  */
  public $card=false;

  /**
  * model
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($model,$card=false)
  {
    $this->card = $card;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $links = getModuleSavedSMLinks($this->model);
    if($links!=null){
      foreach($links as $link){
        $sm = getSocialMediaById($link->sm_id);
        $this->model->social_media[$sm->slug] = $link->link;
      }
    }
    return view('namod-socialmedia::components.module-social-media-fields', [
      'showCard' => $this->card,
      'model'=>$this->model,
    ]);
  }
}

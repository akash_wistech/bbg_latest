<?php
use NaeemAwan\ModuleSocialMedia\Models\ModuleSocialMediaLinks;
use NaeemAwan\ModuleSocialMedia\Models\SocialMedia;
use Illuminate\Support\Facades\DB;

if (!function_exists('getSocialMediaList')) {
  /**
  * return list of social medias
  */
  function getSocialMediaList()
  {
    return SocialMedia::where(['status' => 1])->get();
  }
}

if (!function_exists('getSocialMediaById')) {
  /**
  * return list of social medias
  */
  function getSocialMediaById($id)
  {
    return SocialMedia::where(['id'=>$id])->first();
  }
}

if (!function_exists('saveSocialMediaLinks')) {
  /**
  * return list of modules workflow can have
  */
  function saveSocialMediaLinks($model,$request,$type='form')
  {
    if($request->social_media!=null){
      foreach($request->social_media as $key=>$val){
        $model->social_media[$key]=$val;
      }
    }
    $socialMedias = getSocialMediaList();
    if($socialMedias!=null){
      foreach($socialMedias as $socialMedia){
        if($model->social_media[$socialMedia->slug]){
          $childRow=ModuleSocialMediaLinks::where([
						'module_type'=>$model->moduleTypeId,
						'module_id'=>$model->id,
						'sm_id'=>$socialMedia->id
					]);
					if($childRow->exists()){
            DB::table((new ModuleSocialMediaLinks)->getTable())
            ->where('module_type', $model->moduleTypeId)
            ->where('module_id', $model->id)
            ->where('sm_id', $socialMedia->id)
            ->update([
              'link' => $model->social_media[$socialMedia->slug],
            ]);
          }else{
						$childRow=new ModuleSocialMediaLinks;
						$childRow->module_type=$model->moduleTypeId;
						$childRow->module_id=$model->id;
						$childRow->sm_id=$socialMedia->id;
						$childRow->link=$model->social_media[$socialMedia->slug];
						$childRow->save();
					}
        }
      }
    }
  }
}

if (!function_exists('getModuleSavedSMLinks')) {
  /**
  * get saved tags
  */
  function getModuleSavedSMLinks($model)
  {
    return ModuleSocialMediaLinks::where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->get();
  }
}

if (!function_exists('getModuleSocialMediaQuery')) {
  /**
  * get company users query
  */
  function getModuleSocialMediaQuery($id,$moduleTypeId)
  {
    return ModuleSocialMediaLinks::select([
      (new SocialMedia)->getTable().'.title',
      (new ModuleSocialMediaLinks)->getTable().'.link'
    ])
    ->join((new SocialMedia)->getTable(),(new ModuleSocialMediaLinks)->getTable().".sm_id", "=", (new SocialMedia)->getTable().".id")
    ->where('module_type',$moduleTypeId)
    ->where('module_id',$id)
    ->get()->toArray();
  }
}

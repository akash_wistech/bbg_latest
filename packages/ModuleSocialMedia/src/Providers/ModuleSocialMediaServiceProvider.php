<?php

namespace NaeemAwan\ModuleSocialMedia\Providers;

use Illuminate\Support\ServiceProvider;
use NaeemAwan\ModuleSocialMedia\View\Components\ModuleSocialMediaFields;
use NaeemAwan\ModuleSocialMedia\View\Components\ModuleSocialMediaFieldDetails;

class ModuleSocialMediaServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
    $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'namod-socialmedia');
    $this->loadViewsFrom(__DIR__.'/../../resources/views', 'namod-socialmedia');
    $this->publishes([
      __DIR__.'/../../resources/lang' => resource_path('lang/vendor/namod-socialmedia'),
    ],'namod-socialmedia-lang');
    $this->publishes([
      __DIR__.'/../../database/migrations/' => database_path('migrations')
    ], 'namod-socialmedia-migrations');

    $this->loadViewComponentsAs('namod-socialmedia', [
      ModuleSocialMediaFields::class,
      ModuleSocialMediaFieldDetails::class,
    ]);
  }

  /**
  * Make config publishment optional by merging the config from the package.
  *
  * @return  void
  */
  public function register()
  {
  }
}

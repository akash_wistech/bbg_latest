<?php

namespace NaeemAwan\ModuleSocialMedia\Models;
use App\Models\FullTables;

class SocialMedia extends FullTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'social_media';
}

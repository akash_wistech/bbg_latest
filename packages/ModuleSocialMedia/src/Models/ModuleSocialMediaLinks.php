<?php

namespace NaeemAwan\ModuleSocialMedia\Models;
use App\Models\ChildTables;

class ModuleSocialMediaLinks extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'module_social_media_links';
}

@if($showCard==true)
<div class="card card-custom">
  <div class="card-header p-2">
    <div class="card-title">
      <h3 class="card-label">{{__('namod-socialmedia::socialmedia.heading')}}</h3>
    </div>
  </div>
  <div class="card-body p-2">
    @endif
    <div class="row">
      @foreach(getSocialMediaList() as $socialMedia)
      @php
      $msmVal=null;
      @endphp
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('social_media['.$socialMedia->slug.']',$socialMedia->title) }}
          <div class="input-group input-group-md">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="{{$socialMedia->icon}}"></i></span>
            </div>
            {{ Form::text('social_media['.$socialMedia->slug.']', $msmVal, ['class'=>'form-control']) }}
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @if($showCard==true)
  </div>
</div>
@endif

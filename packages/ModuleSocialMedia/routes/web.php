<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\MultipleEmailPhone\Http\Controllers\MultipleEmailPhoneController;


Route::group(['middleware' => ['web','auth']], function () {
  Route::post('/module-action/delete-email/{module}/{id}/{email}', [MultipleEmailPhoneController::class,'deleteEmail']);
  Route::post('/module-action/delete-number/{module}/{id}/{number}', [MultipleEmailPhoneController::class,'deleteNumber']);
});

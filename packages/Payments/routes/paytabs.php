<?php
use Illuminate\Support\Facades\Route;
use NaeemAwan\Payments\Http\Controllers\PayTabsController;

Route::get('/pay-tabs/order/{id}', [PayTabsController::class,'order']);
Route::get('/pay-tabs/success', [PayTabsController::class,'success']);
Route::match(['get','post'],'/pay-tabs/ipn-listener', [PayTabsController::class,'ipnListener'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);
Route::post('/pay-tabs/process/{id}', [PayTabsController::class,'processPayment'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);
Route::post('/pay-tabs/process-order/{id}', [PayTabsController::class,'processOrder'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);
Route::get('/pay-tabs/{id}', [PayTabsController::class,'index']);

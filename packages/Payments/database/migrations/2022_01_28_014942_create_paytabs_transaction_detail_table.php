<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaytabsTransactionDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paytabs_transaction_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('payment_id')->default(0)->nullable()->index();
            $table->char('payment_method',20)->default('online')->nullable();
            $table->char('payment_status',25)->default('pending')->nullable();
            $table->decimal('amount',10,3)->default(0)->nullable();
            $table->string('response_code',255)->nullable();
            $table->timestamp('trans_date');
            $table->string('transdatetime',50)->nullable();
            $table->string('transaction_id',255)->nullable();
            $table->text('stref')->nullable();
            $table->string('currency',100)->nullable();
            $table->text('customer_name')->nullable();
            $table->text('customer_email')->nullable();
            $table->text('customer_phone')->nullable();
            $table->text('first_4_digits')->nullable();
            $table->text('last_4_digits')->nullable();
            $table->text('card_brand')->nullable();
            $table->string('payment_type',100)->nullable();
            $table->text('trans_detail')->nullable();
            $table->string('catrans',50)->nullable();
            $table->string('secure_sign',255)->nullable();
            $table->integer('use_saved_cc_id')->default(0)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paytabs_transaction_detail');
    }
}

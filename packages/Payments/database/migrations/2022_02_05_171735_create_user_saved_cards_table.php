<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSavedCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_saved_cards', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->default(0)->nullable()->index();
            $table->text('customer_name')->nullable();
            $table->text('customer_email')->nullable();
            $table->text('customer_phone')->nullable();
            $table->text('first_4_digits')->nullable();
            $table->text('last_4_digits')->nullable();
            $table->text('card_brand')->nullable();
            $table->text('pt_token')->nullable();
            $table->text('pt_customer_password')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });

        Schema::table('users', function($table) {
            $table->tinyInteger('save_cc')->after('status')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_saved_cards');

        Schema::table('users', function($table) {
            $table->dropColumn('save_cc');
        });
    }
}

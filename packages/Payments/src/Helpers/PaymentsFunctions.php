<?php
// use NaeemAwan\ModuleSocialMedia\Models\ModuleSocialMediaLinks;
// use NaeemAwan\ModuleSocialMedia\Models\SocialMedia;
// use Illuminate\Support\Facades\DB;
//
if (!function_exists('getPayTabsSettings')) {
  /**
  * return paytabs
  */
  function getPayTabsSettings()
  {
    return [
      //New
      'currency'=>'AED',
      'tokenization'=>true,
      'paymentCallBack'=>url('/pay-tabs/ipn-listener'),
      'paymentReturnUrl'=>url('/pay-tabs/success'),
      'paymentRequest'=>'https://secure.paytabs.com/payment/request',
      //Live
      // 'profileId'=>'51506',
      // 'serverKey'=>'SJJNMD6N6J-HZWGBDZZDZ-HNZH9M6WJW',
      // 'clientKey'=>'CRKMTQ-9BMVH9-VT2D99-6GKBQ6',

      //Test
      'profileId'=>'50931',
      'serverKey'=>'SKJNMD6N2W-HZTRTG6MRG-2TW2L296DZ',
      'clientKey'=>'CMKMTQ-9BKKH9-7R7TGN-MVQBDV',
    ];
  }
}

if (!function_exists('getGatewayFee')) {
  /**
  * return service charges for the method
  */
  function getGatewayFee()
  {
    return [
      'paytabs' => ['fee'=>3,'type'=>'%'],
    ];
  }
}

if (!function_exists('getServiceCharges')) {
  /**
  * return service charges for the method
  */
  function getServiceCharges($total,$gateway)
  {
    $serviceCharges = 0;
    $gatewayFee = getGatewayFee()[$gateway];
    $fee = $gatewayFee['fee'];
    $type = $gatewayFee['type'];
    if($type=='%'){
      $serviceCharges = calculatePercentageAmount($total,$fee);
    }else if($type=='fixed'){
      $serviceCharges = $fee;
    }
    return $serviceCharges;
  }
}

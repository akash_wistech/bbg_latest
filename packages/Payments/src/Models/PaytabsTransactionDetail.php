<?php

namespace NaeemAwan\Payments\Models;
use App\Models\FullTables;

class PaytabsTransactionDetail extends FullTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'paytabs_transaction_detail';
}

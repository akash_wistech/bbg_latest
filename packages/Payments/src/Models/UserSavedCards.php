<?php

namespace NaeemAwan\Payments\Models;
use App\Models\FullTables;

class UserSavedCards extends FullTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'user_saved_cards';
}

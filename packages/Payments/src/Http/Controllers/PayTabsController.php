<?php
namespace NaeemAwan\Payments\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Sales\Models\Order;
use Wisdom\Sales\Models\Payments;
use NaeemAwan\Payments\Models\PaytabsTransactionDetail;
use NaeemAwan\Payments\Models\UserSavedCards;
use Illuminate\Support\Facades\Log;

class PayTabsController extends Controller
{
  public $folderName='payments::paytabs';
  public $controllerId='paytabs';

  /**
  * Display a payment form
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request,$id)
  {
    $model = $this->findInvoiceModel($id);
    return view($this->folderName.'.index',compact('model','request'));
  }

  /**
  * Display a payment form for order
  *
  * @return \Illuminate\Http\Response
  */
  public function order(Request $request,$id)
  {
    $model = $this->findOrderModelByOrderId($id);
    return view($this->folderName.'.order',compact('model','request'));
  }

  /**
  * Display a payment form
  *
  * @return \Illuminate\Http\Response
  */
  public function success(Request $request)
  {
    return view($this->folderName.'.success',compact('request'));
  }

  /**
  * handles payment response
  *
  * @return \Illuminate\Http\Response
  */
  public function ipnListener(Request $request)
  {

  		$headers = getallheaders();
      Log::info($headers);
      $paytabsOptions = getPayTabsSettings();
  	  // In case you have multiple profile keys, use Client-Key header to know which server key.
  	  //$clientKey = $headers["Client-Key"];

  	  $requestSignature = '';//$headers["Signature"];
  	  $serverKey = $paytabsOptions['serverKey']; // Example
  	  $requestBody = file_get_contents('php://input');
      Log::info($requestBody);

  	  $signature = hash_hmac('sha256', $requestBody, $serverKey);
  	  if ($signature!=null && $requestSignature!=null && hash_equals($signature,$requestSignature) === TRUE) {

  	  }
  		$data='';
  		$response = json_decode($requestBody, true);
  		if ($response!=null && count($response)>0) {
  			//foreach($response as $key=>$val){
  			//	$data.=$key." => ".$val."\n\n";
  			//}
  			if ($response['response_code'] == "100") {
          $payment = $this->findPaymentModel($response['order_id']);
          $payment->transaction_id = $response['statement_reference'];
          $payment->is_completed = 1;
          $payment->save();
          $payment->updateInvoices();

          $this->savePaytabsResponse($payment,$response);

  				$user=User::where('id',$payment->user_id)->first();
          Log::info('User get');
  				if($user!=null && $user->save_cc==1){
            Log::info('User Found');
  					$ccFullName=(isset($response['customer_full_name'])) ? encryptData($response['customer_full_name'],'pi'.$payment->id) : "";
  					$ccEmail=(isset($response['customer_email'])) ? encryptData($response['customer_email'],'pi'.$payment->id) : "";
  					$ccPhone=(isset($response['customer_phone'])) ? encryptData($response['customer_phone'],'pi'.$payment->id) : "";
            $ccFirst4Digits=(isset($response['first_4_digits'])) ? encryptData($response['first_4_digits'],'pi'.$payment->id) : "";
  					$ccLast4Digits=(isset($response['last_4_digits'])) ? encryptData($response['last_4_digits'],'pi'.$payment->id) : "";
  					$ccCardBrand=(isset($response['card_brand'])) ? encryptData($response['card_brand'],'pi'.$payment->id) : "";
  					$ccPtToken=(isset($response['pt_token'])) ? encryptData($response['pt_token'],'pi'.$payment->id) : "";
  					$ccPtCustomerPassword=(isset($response['pt_customer_password'])) ? encryptData($response['pt_customer_password'],'pi'.$payment->id) : "";
  					if($ccPtToken!="" && $ccPtCustomerPassword!=""){
              Log::info('token and pass found');
  						$saveInfo=UserSavedCc::where([
  							'user_id'=>$user->id,
  							'customer_email'=>$ccEmail,
  							'last_4_digits'=>$ccLast4Digits,
  							'first_4_digits'=>$ccFirst4Digits,
  							'card_brand'=>$ccCardBrand,
  							'pt_token'=>$ccPtToken,
  							'pt_customer_password'=>$ccPtCustomerPassword,
  						])->first();
  						if($saveInfo==null){
                Log::info('new card saving');
  							$saveInfo=new UserSavedCc;
  							$saveInfo->user_id=$user->id;
  							$saveInfo->customer_name=$ccFullName;
  							$saveInfo->customer_email=$ccEmail;
  							$saveInfo->customer_phone=$ccPhone;
  							$saveInfo->last_4_digits=$ccLast4Digits;
  							$saveInfo->first_4_digits=$ccFirst4Digits;
  							$saveInfo->card_brand=$ccCardBrand;
  							$saveInfo->pt_token=$ccPtToken;
  							$saveInfo->pt_customer_password=$ccPtCustomerPassword;
  							if(!$saveInfo->save()){
                  Log::info('Card info not saved');
                  Log::info($payment);
                  Log::info($response);
                  Log::info($saveInfo);
  							}
                Log::info('new card saved');
  						}
  					}
  				}
  			}else{
          $payment = $this->findPaymentModel($response['order_id']);

          $payment->transaction_id = $response['statement_reference'];
          $payment->save();

          $this->savePaytabsResponse($payment,$response);
  			}
  		}
      Log::info('Response Received: '.$data);
  }

  /**
  * Process payment
  *
  * @return \Illuminate\Http\Response
  */
  public function processPayment(Request $request,$id)
  {
    $model = $this->findInvoiceModel($id);
    if($request->isMethod('post') && $request->has('token') && $request->token!=''){
      $paytabsOptions = getPayTabsSettings();

      $invoiceNo=$model->generateInvoiceNumber();
      $invReferenceNumber='INV'.'-'.sprintf('%06d', $invoiceNo);

      DB::table($model->getTable())
      ->where('id', $model->id)
      ->update([
        'reference_number' => $invReferenceNumber,
        'invoice_no' => $invoiceNo,
        'invoice_date' => date("Y-m-d"),
      ]);

      $totalTax = 0;
      $totalDiscount=0;
      $subTotal=0;
      $grandTotal=0;
      if($model->items->isNotEmpty()){
        foreach($model->items as $item){
          $totalDiscount+=$item->discount_amount;
          $itemTaxableAmount = $item->total;
          if($item->tax_id>0){
            $itemTaxableAmount=$item->total+$item->tax_amount;
              $totalTax+=$item->tax_amount;
          }
          $itemTaxableAmount-=$item->discount_amount;
          $subTotal+= $item->total;
          $grandTotal+=$itemTaxableAmount;
        }
      }

      $serviceCharges = getServiceCharges($grandTotal,'paytabs');
      if($serviceCharges>0){
        $grandTotal+=$serviceCharges;
      }

			$finalAmout=$grandTotal;
			$token=$request->token;

      $payment = new Payments;
      $payment->company_id = $model->company_id;
      $payment->user_id = $model->user_id;
      $payment->amount_received = $finalAmout;
      $payment->payment_date = Carbon::today();
      $payment->payment_method = 4;
      $payment->transaction_id = '';
      $payment->note = '';
      $payment->is_completed = 0;

      $payment->module=['invoice'.'-'.$model->id];
      $payment->amount_due=['invoice'=>[$model->id=>$model->grand_total]];
      $payment->discount=['invoice'=>[$model->id=>$totalDiscount]];
      $payment->amount_paid=['invoice'=>[$model->id=>$finalAmout]];

      $payment->save();

			$dataString=[
				'profile_id'=>$paytabsOptions['profileId'],
				'tran_type'=>'sale',
				'tran_class'=>'ecom',
				'cart_description'=>'Payment for Invoice '.$invReferenceNumber,
				'cart_id'=>$payment->rand_code,
				'cart_currency'=>$model->getInvCurrency(),
				'cart_amount'=>$finalAmout,
				//'callback'=>$paytabsOptions['paymentCallBack'],
				'return'=>$paytabsOptions['paymentReturnUrl'],
				'customer_details'=>[
					'name'=>$model->user->full_name,
					'email'=>$model->user->email,
					'street1'=>$model->user->address,
					'city'=>$model->user->city,
					'country'=>'AE',
					'ip'=>'39.49.12.201',//request()->ip(),
				],
				'framed'=>true,
				'hide_shipping'=>true,
				'payment_token'=>$token,
			];
      // Log::info($dataString);

      // dd($dataString);

			$results=curlRequest($dataString);
      // Log::info($results);
			if($results!=null){
				$responseAr=json_decode($results);
				if(isset($responseAr->redirect_url) && $responseAr->redirect_url!=''){
          return redirect($responseAr->redirect_url);
				}else{
			    if($responseAr->code==200 && $responseAr->message=='Invalid payment token'){
            return redirect('/pay-tabs/'.$model->id)->with('error', __('payments::payments.error_response',['message'=>$responseAr->message]));
			    }else{
						if(isset($responseAr->payment_result) && $responseAr->payment_result!=null){

							$paymentResult=$responseAr->payment_result;
							if($paymentResult->response_status=='A'){
                return redirect('/')->with('success', __('payments::payments.success_msg'));
							}else{
                $paytabsDetail = new PaytabsTransactionDetail;
                $paytabsDetail->payment_id = $payment->id;
                $paytabsDetail->payment_method = 'online';
                $paytabsDetail->payment_status = $paymentResult->response_message;
                $paytabsDetail->amount = $order->amount_payable;
                $paytabsDetail->response_code = $paymentResult->response_code;

                $paytabsDetail->trans_date = Carbon::today();
                $paytabsDetail->transdatetime = Carbon::today();
                $paytabsDetail->transaction_id = $result->tran_ref;
                $paytabsDetail->currency = $paymentResult->currency;
                $paytabsDetail->save();
          			return redirect('/pay-tabs/'.$model->id)->with('error', __('payments::payments.error_response',['message'=>$paymentResult->response_message]));
							}
						}
					}
				}
			}else{
				return redirect('/pay-tabs/'.$model->id)->with('error', __('payments::payments.can_not_process'));
			}
			die();
			exit;
		}
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findInvoiceModel($id)
  {
    return Invoices::where('id', $id)->first();
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findOrderModelByOrderId($id)
  {
    return Order::where('order_id', $id)->first();
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findPaymentModel($id)
  {
    return Payments::where('rand_code', $id)->first();
  }

  /**
  * Save paytabs response
  */
  public function savePaytabsResponse($payment,$response)
  {
    $payment_status=(isset($response['response_code'])) ? $response['response_code'] : "";
    $transaction_id=(isset($response['statement_reference'])) ? $response['statement_reference'] : "";
    $paytabsDetail = PaytabsTransactionDetail::where(['payment_id'=>$payment->id,'payment_status'=>$payment_status,'transaction_id'=>$transaction_id])->first();
    if($paytabsDetail==null){
      $paytabsDetail = new PaytabsTransactionDetail;
      $paytabsDetail->payment_id = $payment->id;
      $paytabsDetail->payment_method = 'online';
      $paytabsDetail->payment_status = $payment_status;
      $paytabsDetail->amount = (isset($response['amount'])) ? $response['amount'] : "";
      $paytabsDetail->response_code = (isset($response['detail'])) ? $response['detail'] : "";
      $paytabsDetail->trans_date = (isset($response['datetime'])) ? $response['datetime'] : "";
      $paytabsDetail->transdatetime = (isset($response['datetime'])) ? $response['datetime'] : "";
      $paytabsDetail->transaction_id = $transaction_id;
      $paytabsDetail->currency = (isset($response['currency'])) ? $response['currency'] : "";

      $paytabsDetail->customer_name = (isset($response['customer_full_name'])) ? encryptData($response['customer_full_name'],'pi'.$payment->id) : "";
      $paytabsDetail->customer_email = (isset($response['customer_email'])) ? encryptData($response['customer_email'],'pi'.$payment->id) : "";
      $paytabsDetail->customer_phone = (isset($response['customer_phone'])) ? encryptData($response['customer_phone'],'pi'.$payment->id) : "";
      $paytabsDetail->first_4_digits = (isset($response['first_4_digits'])) ? encryptData($response['first_4_digits'],'pi'.$payment->id) : "";
      $paytabsDetail->last_4_digits = (isset($response['last_4_digits'])) ? encryptData($response['last_4_digits'],'pi'.$payment->id) : "";
      $paytabsDetail->card_brand = (isset($response['card_brand'])) ? encryptData($response['card_brand'],'pi'.$payment->id) : "";

      $paytabsDetail->secure_sign = (isset($response['secure_sign'])) ? $response['secure_sign'] : "";
      if(!$paytabsDetail->save()){
        Log::info('Paytabs response not saved');
        Log::info($payment);
        Log::info($response);
      }
    }
  }
}

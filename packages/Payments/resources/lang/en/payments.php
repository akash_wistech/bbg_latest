<?php
return [
  'service_charges' => 'Service Charges',
  'card_number' => 'Card Number',
  'expiry_mm' => 'MM',
  'expiry_yyyy' => 'YYYY',
  'security_code' => 'Security Code',
  'pay_now' => 'Pay Now',
  'error_response' => 'Payment was :message',
  'can_not_process' => 'Sorry, Payment could not be processed at this time, Please try again later',
  'success_msg' => 'Payment completed successfully',

  //Paytabs
  'expiry_date_pt' => 'Expiry Date (MM/YYYY)',
];

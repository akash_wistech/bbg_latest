@php
$taxArr=getTaxArr();
$paytabsOptions = getPayTabsSettings();

$totalTax = 0;
$totalDiscount=0;
$subTotal=0;
$grandTotal=0;

@endphp
@extends('layouts.guest')
@section('content')

<script src="https://secure.paytabs.com/payment/js/paylib.js"></script>
<style media="screen">

input[type="text"]::placeholder {

            /* Firefox, Chrome, Opera */
            text-align: center;
        }
.statement{
  color:#BDBDBD; font-size:14px;
}
.statement-detail{
  font-weight: 500;
  font-size: 14px;
}

</style>

<div class="bg-white card-body p-5 py-7 rounded border col shadow">

<form action="{{url('/pay-tabs/process/'.$model->id)}}" id="payform" method="post">
  <span id="paymentErrors"></span>
  <div class="d-flex flex-row-reverse justify-content-between">
  <div class="col-5 rounded px-5 py-7   shadow-sm" style="background-color:#FAFAFA;">

    <div class="py-3 mb-10 rounded" style="background-color:#EEEEEE">
      <h1 style="color: #000;"><b>Detail</b></h1>
    </div>


  <div class=" text-left pl-4">

    <div class="row my-4">
      <div class="col">
        <span class="statement">Company</span>
      </div>
      <div class="col">
        <span class="statement-detail">{{ $model->bill_to_company_name }}</span>
      </div>
    </div>


    <?php if ($model->invoice_no==null) { ?>
      <div class="row my-4">
        <div class="col">
          <span class="statement">Proforma Invoice date</span>
        </div>
        <div class="col">
          <span class="statement-detail">{{ formatDate($model->p_invoice_date) }}</span>
        </div>
      </div>
      <div class="row my-4">
        <div class="col">
          Performa Invoice number
        </div>
        <div class="col">
          <span class="statement-detail">{{ $model->p_reference_number }}</span>
        </div>
      </div>

    <?php	} if ($model->invoice_no!=null) {  ?>
      <!-- <h1 style="color: #000;"><b>Invoice</b></h1> -->
      <div class="row my-4">
        <div class="col">
          <span class="statement">Invoice no:</span>
        </div>
        <div class="col">
          <span class="statement-detail">{{ $model->reference_number }}</span>
        </div>
      </div>
      <div class="row my-4">
        <div class="col">
          <span class="statement">Proforma Invoice no:</span>
        </div>
        <div class="col">
          <span class="statement-detail">{{ $model->p_reference_number }}</span>
        </div>
      </div>
        <!-- <div class=" col text-right">
          <span>{{ formatDate($model->invoice_date) }}</span><br>
          <span>{{ formatDate($model->p_invoice_date) }}</span>
        </div> -->
    <?php } ?>



    <div class="row my-4">
      <div class="col">
        <span class="statement">Name</span>
      </div>
      <div class="col">
        <span class="statement-detail">{{ $model->bill_to_name }}</span>
      </div>
    </div>
    @if($model->items->isNotEmpty())
      @foreach($model->items as $item)
      @php
        $totalDiscount+=$item->discount_amount;

        $itemTaxableAmount = $item->total;
        if($item->tax_id>0){
          $itemTaxableAmount=$item->total+$item->tax_amount;
            $totalTax+=$item->tax_amount;
        }
        $itemTaxableAmount-=$item->discount_amount;
        $subTotal+= $item->total;
        $grandTotal+=$itemTaxableAmount;
      @endphp
      <div class="row my-4">
        <div class="col">
          <span class="statement">Membership</span>
        </div>
        <div class="col">
          <span class="statement-detail">{{$item->title}}</span>
        </div>
      </div>
      @endforeach
    @endif



    <div class="row my-4">
      <div class="col">
        <span class="statement">Vat(10%)</span>
      </div>
      <div class="col">
        <span class="statement-detail">{!!formatInvPrice($model,' '.$totalTax)!!}</span>
      </div>
    </div>


    <div class="row my-4">
      <div class="col">
        <span class="statement">{{__('common.discount')}}</span>
      </div>
      <div class="col">
        <span class="statement-detail">{!!formatInvPrice($model,' - '.$totalDiscount)!!}</span>
      </div>
    </div>
    @php
    $serviceCharges = getServiceCharges($grandTotal,'paytabs');
    @endphp
    @if($serviceCharges>0)
    @php
    $grandTotal+=$serviceCharges;
    @endphp

    <div class="row my-4">
      <div class="col">
        <span class="statement">{{__('payments::payments.service_charges')}}</span>
      </div>
      <div class="col">
        <span class="statement-detail">{{formatInputInvPrice($model,$serviceCharges)}}</span>
      </div>
    </div>
    @endif

    <div class="row mt-8 pt-8" style="border-top:1px dashed gray">
      <div class="col">
        <p class="statement">You have to Pay</p>
      </div>
      <div class="col">
          <h3>{{formatInputInvPrice($model,' '.$grandTotal)}}</h3>
      </div>
    </div>


      </div>
</div>

  <div class="col-7 ">
    <div class="">
      <img src="{{ asset('images\paytabs.png') }}" width="120px" alt="">
    </div>
    <div class="text-left p-7 pb-0 border rounded">

      <div class="form-group mb-10">
        <h3>{{__('payments::payments.card_number')}}</h3>
        <p style="font-size:12.5px; color:#BDBDBD">Enter the 16-digiit card number on the card.</p>
        <div class="input-group">
         <div class="input-group-prepend">
          <span class="input-group-text"><img src="{{ asset('images\mc_symbol.svg') }}" width="30px"  alt=""></span>
         </div>
         <input type="text" class="form-control form-control-lg" data-paylib="number" size="20" placeholder="{{__('payments::payments.card_number')}}" />
         <div class="input-group-append">
          <span class="input-group-text  bg-white "><i class="fas fa-check-circle"></i></span>
         </div>
        </div>
       </div>
         <div class="row my-5" >
           <div class="col">
             <h4>CVV NUMBER</h4>
             <p style="font-size:12.5px; color:#BDBDBD">Enter the 3 or 4 digiit number on the card</p>
           </div>
           <div class="form-group col">
             <div class="input-group">
               <input type="text" class="form-control form-control-lg" data-paylib="cvv" size="4" placeholder="327" />
              <div class="input-group-append"><span class="input-group-text"><i class="fas fa-th"></i></span></div>
             </div>
            </div>
         </div>
         <div class="row my-5">
           <div class="col">
             <h4>Expiry Date</h4>
             <p style="font-size:12.5px; color:#BDBDBD">Enter the expiration date of the card</p>
           </div>
           <div class="form-group col">
             <div class="input-group">
                <input type="text" class="form-control form-control-lg rounded" data-paylib="expmonth" size="2" placeholder="{{__('payments::payments.expiry_mm')}}" />
                <span style="font-size:17px;" class="pt-3 px-4">/</span>
                <input type="text" class="form-control form-control-lg rounded" data-paylib="expyear" size="4" placeholder="{{__('payments::payments.expiry_yyyy')}}" />
             </div>
            </div>
         </div>
     </div>
     <div class="col mt-5 px-0">
       <input type="submit" class="btn btn-primary btn-lg col"  value="{{__('payments::payments.pay_now')}}">
     </div>
</div>
</div>
</form>


</div>

@endsection
@push('jsFunc')
var myform = document.getElementById('payform');
paylib.inlineForm({
  'key': '{{$paytabsOptions["clientKey"]}}',
  'form': myform,
  'autosubmit': true,
  'callback': function(response) {
    document.getElementById('paymentErrors').innerHTML = '';
    if (response.error) {
      paylib.handleError(document.getElementById('paymentErrors'), response);
    }
  }
});
@endpush
@push('cssStyles')
.PT_open_popup{opacity:0;}
.scroll-wrapper {
    -webkit-overflow-scrolling: touch;
      overflow-y: scroll;
      /* important:  dimensions or positioning here! */
}
@endpush

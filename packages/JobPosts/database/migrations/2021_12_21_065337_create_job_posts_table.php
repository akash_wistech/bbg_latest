<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('user_type')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('job_title')->nullable();
            $table->string('position')->nullable();
            $table->string('industry')->nullable();
            $table->string('skills')->nullable();
            $table->string('experience')->nullable();
            $table->string('location')->nullable();
            $table->date('posted_on')->nullable();
            $table->date('auto_remove_after')->nullable();
            $table->date('apply_by')->nullable();
            $table->string('short_description')->nullable();
            $table->text('description')->nullable();
            $table->enum('published', ['0', '1']);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}

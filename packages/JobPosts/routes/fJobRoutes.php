<?php

use Illuminate\Support\Facades\Route;



Route::group(['middleware' => ['web','auth']], function () {


	Route::get('jobPosts',[Wisdom\JobPosts\Http\Controllers\frontend\JobPostsController::class,'index']);
	Route::match(['get', 'post'],'jobPosts/create',[Wisdom\JobPosts\Http\Controllers\frontend\JobPostsController::class,'create']);
	Route::match(['get', 'post'],'jobPosts/update/{id}',[Wisdom\JobPosts\Http\Controllers\frontend\JobPostsController::class,'update']);
	Route::match(['get', 'post'],'jobPosts/view/{id}',[Wisdom\JobPosts\Http\Controllers\frontend\JobPostsController::class,'view']);
	Route::match(['get', 'post'],'jobPosts/delete/{id}',[Wisdom\JobPosts\Http\Controllers\frontend\JobPostsController::class,'delete']);

});

<?php

use Illuminate\Support\Facades\Route;
use Wisdom\JobPosts\Http\Controllers\JobPostsController;



Route::group(['middleware' => ['web','auth']], function () {
	Route::get('job-posts',[JobPostsController::class,'index']);
	Route::match(['get', 'post'],'job-posts/create',[JobPostsController::class,'create']);
	Route::match(['get','post'],'job-posts/update/{id}',[JobPostsController::class,'update'])->name('job-posts.update');
	Route::match(['get','post'],'job-posts/delete/{id}',[JobPostsController::class,'destroy'])->name('job-posts.delete');
	Route::match(['get','post'],'job-posts/view/{id}',[JobPostsController::class,'view'])->name('job-posts.view');
	Route::match(['get','post'],'job-posts/data-table-data',[JobPostsController::class,'datatableData']);
});



Route::group(['middleware' => ['web']], function () {
	include('fJobRoutes.php');
});

<?php
return [

	'heading' => 'Job Posts',
	'saved' => 'Saved Successfully',
	'updated' => 'Updated Successfully',
	'id' => 'Id',
	'user_id' => 'Member',
	'user_type' => 'User Type',
	'contact_name' => 'Contact Name',
	'contact_email' => 'Contact Email',
	'contact_phone' => 'Contact Phone',
	'job_title' => 'Job Title',
	'position' => 'Position',
	'industry' => 'Industry',
	'skills' => 'Skills',
	'experience' => 'Experience',
	'location' => 'Location',
	'posted_on' => 'Posted On',
	'auto_remove_after' => 'Auto Remove After',
	'apply_by' => 'Apply By',
	'short_description' => 'Short Description',
	'description' => 'Description',
	'published' => 'Published',
	'publish' => 'Publish',
	'un_publish' => 'Un-Publish',
	'select' => 'Select',
	'created' => 'Created',

	'publish' => 'Publish',
	'un_publish' => 'Un-Publish',

];

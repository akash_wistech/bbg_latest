<?php //dd(getMemberForJobPosts()); ?>

<div class="card card-custom">
  {!! Form::model($model, ['files' => true, 'id' => 'job-form']) !!}
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    {{ Form::text('user_type', 'member', ['class'=>'form-control d-none']) }}
    <div class="row">
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('user_id',__('jobposts::jobposts.user_id')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::select('user_id', [''=>__('common.select')] + getMemberForJobPosts(), $model->user_id, ['class'=>'form-control select2']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('job_title',__('jobposts::jobposts.job_title')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('job_title',  $model->job_title, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('position',__('jobposts::jobposts.position')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('position',  $model->position, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('industry',__('jobposts::jobposts.industry')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('industry',  $model->industry, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('skills',__('jobposts::jobposts.skills')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('skills',  $model->skills, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('experience',__('jobposts::jobposts.experience')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('experience',  $model->experience, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('location',__('jobposts::jobposts.location')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('location',  $model->location, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('posted_on',__('jobposts::jobposts.posted_on')) }}<span class="text-danger"><b>*</b></span>
          <div class="input-group date">
            {{ Form::text('posted_on',  $model->posted_on, ['class'=>'form-control dtpicker', 'readonly']) }}
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar"></i>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('apply_by',__('jobposts::jobposts.apply_by')) }}<span class="text-danger"><b>*</b></span>
          <div class="input-group date">
            {{ Form::text('apply_by',  $model->apply_by, ['class'=>'form-control dtpicker', 'readonly']) }}
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar"></i>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('auto_remove_after',__('jobposts::jobposts.auto_remove_after')) }}<span class="text-danger"><b>*</b></span>
          <div class="input-group date">
            {{ Form::text('auto_remove_after',  $model->auto_remove_after, ['class'=>'form-control dtpicker', 'readonly']) }}
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar"></i>
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('contact_name',__('jobposts::jobposts.contact_name')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('contact_name',  $model->contact_name, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('contact_email',__('jobposts::jobposts.contact_email')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('contact_email',  $model->contact_email, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('contact_phone',__('jobposts::jobposts.contact_phone')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::text('contact_phone',  $model->contact_phone, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-4">
        <div class="form-group">
          {{ Form::label('published',__('jobposts::jobposts.published')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::select('published', [''=>__('jobposts::jobposts.select')] + getJobPostsStatusArr(), $model->published, ['class'=>'form-control']) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('short_description',__('jobposts::jobposts.short_description')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::textarea('short_description',  $model->short_description, ['class'=>'form-control','rows'=>4]) }}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="form-group">
          {{ Form::label('description',__('jobposts::jobposts.description')) }}<span class="text-danger"><b>*</b></span>
          {{ Form::textarea('description',  $model->description, ['class'=>'form-control editor']) }}
        </div>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <div class="col-lg-12">
      <button type="submit" class="btn btn-primary mr-2 mx-2 ">Save</button>
      <a href="{{url('job-posts')}}" class="btn btn-default">Cancel</a>
    </div>
  </div>  
  {!! Form::close() !!}
</div>





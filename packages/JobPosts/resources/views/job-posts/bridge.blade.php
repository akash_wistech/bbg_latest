@extends('layouts.app')


@section('title', __('jobposts::jobposts.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'job-posts'=> __('jobposts::jobposts.heading'),
__('common.create')
]])
@endsection

@section('content')
@include('job-posts::.form')
@endsection

@push('css')
<style>

</style>
@endpush

@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script src="{{asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
	tinymce.init({
		selector: '.editor',
		menubar: false,
		toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
		plugins : 'advlist autolink link image lists charmap code table',
		image_advtab: true ,
	});
</script>
<script>
	$('#job-form').validate({
		rules: {
			user_id: {
				required: true,
			},
			job_title: {
				required: true,
			},
			position: {
				required: true,
			},
			industry: {
				required: true,
			},
			skills: {
				required: true,
			},
			experience: {
				required: true,
			},
			location: {
				required: true,
			},
			contact_name: {
				required: true,
			},
			contact_email: {
				required: true,
				email: true,
			},
			contact_phone: {
				required: true,
			},
			published: {
				required: true,
			},
			short_description: {
				required: true,
			},
			description: {
				required: true,
			}
		}
	});
</script>
@endpush
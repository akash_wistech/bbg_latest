@extends('layouts.app')

@section('title', __('jobposts::jobposts.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'site-settings'=>__('jobposts::jobposts.heading'),
__('common.view')
]])
@endsection

@section('content')
<div class="card card-custom pb-0" data-card="true">
	<div class="card-header">
		<div class="card-title">
			<h3 class="card-label">Information</h3>
		</div>
		<div class="card-toolbar">
			<?php 
			if(checkActionAllowed('update','job-posts'))
			{
				?>
				<a href="{{url('job-posts/update',['id'=>$model->id])}}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="{{__('common.update')}}">
					<i class="text-dark-50 flaticon-edit"></i>
				</a>
				<?php
			}	
			?>	
		</div>
	</div>
	<div class="card-body">
		<div class="row" style="font-size: 14px;">
			<div class="col-4 px-0">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.created')}}:</strong>{{formatDateTime($model->created_at)}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.user_type')}}:</strong>{{$model->user_type}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.contact_phone')}}: </strong>{{$model->contact_phone}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.industry')}}: </strong>{{$model->industry}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.posted_on')}}:</strong>{{$model->posted_on}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.published')}}:</strong>{{getJobPostsStatusArr()[$model->published]}}
				</div>
			</div>
			<div class="col-4">
				<div class="col ">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.job_title')}}:</strong>{{$model->job_title}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.contact_name')}}:</strong>{{$model->contact_name}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.position')}}: </strong>{{$model->position}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.skills')}}: </strong>{{$model->skills}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.apply_by')}}:</strong>{{$model->apply_by}}
				</div>
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.short_description')}}:</strong>{{$model->short_description}}
				</div>
			</div>
			<div class="col-4">
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.user_id')}}:</strong>{{$model->user_id}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.contact_email')}}:</strong>{{$model->contact_email}}
				</div>
				<div class="col">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.experience')}}:</strong>{{$model->experience}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.location')}}:</strong>{{$model->location}}
				</div>
				<div class="col my-4">
					<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.auto_remove_after')}}: </strong>{!! $model->auto_remove_after !!}
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-12">
				<strong class="pr-3" style="text-decoration: underline;">{{__('jobposts::jobposts.description')}}: </strong>{!! $model->description !!}
			</div>
		</div>

	</div>
</div>
@endsection
<?php
$btnsList = [];
if(checkActionAllowed('create','job-posts')){
	$btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'job-posts/create', 'method'=>'post'];
}
$dtColsArr = [];
$advSearchCols=[];
$dtColsArr[]=['title'=>__('jobposts::jobposts.id'),'data'=>'id','name'=>'id','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.user_id'),'data'=>'user_id','name'=>'user_id','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.user_type'),'data'=>'user_type','name'=>'user_type','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.contact_name'),'data'=>'contact_name','name'=>'contact_name','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.contact_email'),'data'=>'contact_email','name'=>'contact_email','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.contact_phone'),'data'=>'contact_phone','name'=>'contact_phone','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.job_title'),'data'=>'job_title','name'=>'job_title','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.position'),'data'=>'position','name'=>'position','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.industry'),'data'=>'industry','name'=>'industry','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.skills'),'data'=>'skills','name'=>'skills','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.experience'),'data'=>'experience','name'=>'experience','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.location'),'data'=>'location','name'=>'location','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.posted_on'),'data'=>'posted_on','name'=>'posted_on','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.auto_remove_after'),'data'=>'auto_remove_after','name'=>'auto_remove_after','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.apply_by'),'data'=>'apply_by','name'=>'apply_by','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.short_description'),'data'=>'short_description','name'=>'short_description','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.description'),'data'=>'description','name'=>'description','orderable'=>false];
$dtColsArr[]=['title'=>__('jobposts::jobposts.published'),'data'=>'published','name'=>'published','orderable'=>false];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at','orderable'=>false];
$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'100'];
?>

@extends('layouts.app',['btnsList'=>$btnsList])



@section('title', __('jobposts::jobposts.heading'))
@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('jobposts::jobposts.heading')
]])
@endsection

@section('content')
<div class="card card-custom">
	<div id="grid-table" class="card card-body">
		<x-data-table-grid :request="$request" :controller="'job-posts'" :cbSelection="false" :exportMenu="true" :jsonUrl="'job-posts/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
	</div>
</div>
@endsection

@extends('frontend.app')
@section('content')

<?php ?>

<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<?php
					if (isset($model) && $model <> null) {
						?>
						<div class="Heading">
							<h3>Job Details</h3>
							<hr/>
							<h4><?= $model->title; ?></h4>
							<div class="row">
								<div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<h3>Details</h3>
									<hr/>
									<p>
										<span class="BoldText">Position:</span> <?= ($model->position); ?>
									</p>
									<p>
										<span class="BoldText">Industry:</span> <?= ($model->industry); ?>
									</p>
									<p>
										<span class="BoldText">Skills:</span> <?= ($model->skills); ?>
									</p>
									<p>
										<span class="BoldText">Experience:</span> <?= ($model->experience); ?>
									</p>
									<p>
										<span class="BoldText">Location:</span> <?= ($model->location); ?>
									</p>
									<p>
										<span class="BoldText">Date Posted:</span> <?= ($model->posted_on); ?>
									</p>
									<p>
										<span class="BoldText">Apply Last Date:</span> <?= ($model->apply_by); ?>
									</p>
								</div>
								<div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<h3>Contact Info</h3>
									<hr/>

									<?php
									if($model->contact_name) {
										?>
										<p>
											<span class="BoldText">Name:</span> <?= ($model->contact_name); ?>
										</p>
										<?php
									}
									if($model->contact_email) {
										// dd($model->contact_email);
										?>
										<p>
											<span class="BoldText">Email:</span>
											<a href="mailto:<?= $model->contact_email ?>"><?= $model->contact_email ?></a>
										</p>
										<?php
									}
									if($model->contact_phone) {
										?>
										<p>
											<span class="BoldText">Phone:</span>
											<a href="mailto:<?= $model->contact_phone ?>"><?= $model->contact_phone ?></a>
										</p>
										<?php
									}
									?>
								</div>
							</div>
						</div>
						<div class="EventDetail">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="DescriptionHeading">
										<h4><?= $model->short_description; ?></h4>
										<?= $model->description; ?>
									</div>
									<div id="share"></div>
								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				<!-- right side bar calendar -->
			</div>
		</div>
	</div>
</section>


@endsection

@push('js')
<script>
	id = "{{$model->id}}";
	title = "{{$model->job_title}}";
	url = "{{url('jobPosts/view')}}/"+id;

	$("#share").jsSocials({
		url : url,
		text: title,
		shareIn: "popup",
		showLabel: false,
		showCount: false,
		shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "whatsapp"]
	});
</script>
@endpush
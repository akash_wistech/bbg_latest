@extends('frontend.app')
@section('content')

<?php  
// dd($model);
$no_image = asset('assets/images/dummy-image-1.jpg');
if ($model<> null) {?>
	@include('frontend._banner',['model'=>$model,'no_image'=>$no_image])
	<?php
}
?>


{!! Form::model($model, ['files' => true, 'id' => 'job-form']) !!}
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
					@if ($errors->any())
					<div class="alert alert-danger my-2">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<div class="col-md-12 ">

						<div class="Heading text-left">
							<h3>Post Job</h3><br/>
						</div>
						{{ Form::text('user_type', 'member', ['class'=>'form-control d-none']) }}
						<div class="row">
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('job_title',__('jobposts::jobposts.job_title')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('job_title',  $newModel->job_title, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('position',__('jobposts::jobposts.position')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('position',  $newModel->position, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('industry',__('jobposts::jobposts.industry')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('industry',  $newModel->industry, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('skills',__('jobposts::jobposts.skills')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('skills',  $newModel->skills, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('experience',__('jobposts::jobposts.experience')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('experience',  $newModel->experience, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('location',__('jobposts::jobposts.location')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('location',  $newModel->location, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('posted_on',__('jobposts::jobposts.posted_on')) }}<span class="text-danger"><b>*</b></span>
									<div class="input-group date">
										{{ Form::text('posted_on',  $newModel->posted_on, ['class'=>'form-control dtpicker', 'readonly']) }}
										
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('apply_by',__('jobposts::jobposts.apply_by')) }}<span class="text-danger"><b>*</b></span>
									<div class="input-group date">
										{{ Form::text('apply_by',  $newModel->apply_by, ['class'=>'form-control dtpicker', 'readonly']) }}
										
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('auto_remove_after',__('jobposts::jobposts.auto_remove_after')) }}<span class="text-danger"><b>*</b></span>
									<div class="input-group date">
										{{ Form::text('auto_remove_after',  $newModel->auto_remove_after, ['class'=>'form-control dtpicker', 'readonly']) }}
										
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('contact_name',__('jobposts::jobposts.contact_name')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('contact_name',  $newModel->contact_name, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('contact_email',__('jobposts::jobposts.contact_email')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('contact_email',  $newModel->contact_email, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('contact_phone',__('jobposts::jobposts.contact_phone')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::text('contact_phone',  $newModel->contact_phone, ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-12 col-sm-4">
								<div class="form-group">
									{{ Form::label('published',__('jobposts::jobposts.published')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::select('published', [''=>__('jobposts::jobposts.select')] + getJobPostsStatusArr(), $newModel->published, ['class'=>'form-control']) }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-12">
								<div class="form-group">
									{{ Form::label('short_description',__('jobposts::jobposts.short_description')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::textarea('short_description',  $newModel->short_description, ['class'=>'form-control','rows'=>4]) }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-sm-12">
								<div class="form-group">
									{{ Form::label('description',__('jobposts::jobposts.description')) }}<span class="text-danger"><b>*</b></span>
									{{ Form::textarea('description',  $newModel->description, ['class'=>'form-control editor']) }}
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
						<button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Post Job</button>
					</div>
				</div>
			</div>
		</div>

	</section>
	{!! Form::close() !!}


	@endsection

	@push('css')
	<link rel="stylesheet" type="text/css" media="all"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/smoothness/jquery-ui.css"    />

	<style>
		form div.required label.control-label:after {
			content: " * ";
			color: #a94442;
		}
	</style>
	@endpush()

	@push('js')
	<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

	<script>
		tinymce.init({
			selector: '.editor',
			menubar: false,
			toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
			plugins : 'advlist autolink link image lists charmap code table',
			image_advtab: true ,
		});
	</script>

	<script>
		$('.dtpicker').datepicker({ 
			dateFormat: 'yy-mm-dd',
			todayHighlight: true,
			orientation: "bottom left",
			todayBtn: "linked",
			clearBtn: true,
			autoclose: true,
		});
	</script>

	<script>
		$('#job-form').validate({
			rules: {
				user_id: {
					required: true,
				},
				job_title: {
					required: true,
				},
				position: {
					required: true,
				},
				industry: {
					required: true,
				},
				skills: {
					required: true,
				},
				experience: {
					required: true,
				},
				location: {
					required: true,
				},
				contact_name: {
					required: true,
				},
				contact_email: {
					required: true,
					email: true,
				},
				contact_phone: {
					required: true,
				},
				published: {
					required: true,
				},
				short_description: {
					required: true,
				},
				description: {
					required: true,
				},
				// posted_on: {
				// 	required: true,
				// },
				// apply_by: {
				// 	required: true,
				// },
				// auto_remove_after: {
				// 	required: true,
				// }
			}
		});
	</script>
	@endpush
@extends('frontend.app')
@section('content')

<?php  
$no_image = asset('assets/images/dummy-image-1.jpg');
if ($model<>null) {?>
	@include('frontend._banner',['model'=>$model,'no_image'=>$no_image])
	<?php
}
?>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="Heading text-center">
					<h3>Job Posts</h3>
				</div>
				<div class="form-group no-more-tables" style=" overflow-x:auto;">
					<a class="Mybtn pull-right" href="{{url('jobPosts/create')}}" style="margin:10px"><i class="fa fa-plus"></i> Post New Job</a>

					<table id="memberEventsRegTable"
					class="table display responsive no-wrap dt-responsive nowrap d-lg-table d-md-table d-sm-table d-table">
					<thead>
						<tr>
							<th>#</th>
							<th>Job Title</th>
							<th>Position</th>
							<th>Experience</th>
							<th>Posted On</th>
							<th>Apply till</th>
							<th>Contact Name</th>
							<th>Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($jobs <> null && count($jobs) > 0) {
							$count = 1;
							foreach ($jobs as $job) {?>
								<tr>
									<td class="text-center"><?= $count++; ?></td>
									<td class="text-center">
										<?= $job->job_title; ?>
									</td>
									<td class="text-center">
										<?= $job->position; ?>
									</td>
									<td class="text-center">
										<?= $job->experience; ?>
									</td>
									<td class="text-center">
										<?= $job->posted_on; ?>
									</td>
									<td class="text-center">
										<?= $job->apply_by; ?>
									</td>
									<td class="text-center">
										<?= $job->contact_name; ?>
									</td>
									<td class="text-center">
										<?= getJobsFendStatus()[$job->published] ?>
									</td>
									<td class="invoicelink text-center">
										<a class="btn-sm Mybtn" href="<?= url('jobPosts/view',['id'=>$job->id]) ?>" target="_blank" title="View Details" style="color:#fff !important" data-method="post"><i class="fa fa-eye"></i></a>
										<a class="btn-sm Mybtn" href="<?= url('jobPosts/update',['id'=>$job->id]) ?>" target="_blank" title="Edit" style="color:#fff !important" data-method="post"><i class="fa fa-pencil"></i></a>
										<a class="btn-sm Mybtn" href="<?= url('jobPosts/delete',['id'=>$job->id]) ?>" target="_blank" title="Delete" style="color:#fff !important" data-method="post"><i class="fa fa-trash"></i></a>
									</td>
								</tr>

								<?php
							}
						}
						?>
					</tbody>
				</table>
				<br>
				<div class="col-md-12 text-center pull-right">
					{{$jobs->links()}}
				</div>
			</div>
		</div>
	</div>
</div>
</section>


@endsection
<?php

namespace Wisdom\JobPosts\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPosts extends Model
{
	use HasFactory;
	public $moduleTypeId = 'job-posts';
}

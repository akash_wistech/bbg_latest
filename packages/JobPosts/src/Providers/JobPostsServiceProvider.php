<?php

namespace Wisdom\JobPosts\Providers;

use Illuminate\Support\ServiceProvider;

class JobPostsServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
		$this->loadViewsFrom(__DIR__ . '/../../resources/views/job-posts', 'job-posts');
		$this->loadViewsFrom(__DIR__ . '/../../resources/views/frontend/job-posts', 'fjob-posts');
		$this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'jobposts');
		$this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
	}
}
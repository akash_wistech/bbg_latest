<?php

namespace Wisdom\JobPosts\Http\Controllers\frontend;

use Wisdom\JobPosts\Models\JobPosts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class JobPostsController extends Controller
{
    public function index(Request $request)
    {
        // dd("hello");
        $jobs = $this->newModel()::where(['user_id'=>Auth::user()->id])->orderBy('posted_on','desc')->paginate(20);
        $model = Contact::where(['id' => Auth::user()->id])->first();
        // dd($jobs);
        return view('fjob-posts::.jobs-posted',compact('jobs','model'));

    }

    public function newModel()
    {
        return new JobPosts;
    }

    public function findModel($id)
    {
        return JobPosts::where('id', $id)->first();
    }

    public function create(Request $request)
    {
        // dd($request->all());
        $newModel = new JobPosts;

        if($request->isMethod('post')){
            // dd("here");
            $validatedData = $request->validate([
                'posted_on' => 'required|date',
                'apply_by' => 'required|date',
                'auto_remove_after' => 'required|date',
            ]);

            $newModel = new JobPosts;
            $newModel->user_id=Auth::user()->id;
            $newModel->user_type=$request->user_type;
            $newModel->contact_name=$request->contact_name;
            $newModel->contact_email=$request->contact_email;
            $newModel->contact_phone=$request->contact_phone;
            $newModel->job_title=$request->job_title;
            $newModel->position=$request->position;
            $newModel->industry=$request->industry;
            $newModel->skills=$request->skills;
            $newModel->experience=$request->experience;
            $newModel->location=$request->location;
            $newModel->posted_on=$request->posted_on;
            $newModel->auto_remove_after=$request->auto_remove_after;
            $newModel->apply_by=$request->apply_by;
            $newModel->short_description=$request->short_description;
            $newModel->description=$request->description;
            $newModel->published=$request->published;
            $newModel->created_by=Auth::user()->id;

            if ($newModel->save()) {
                return redirect('jobPosts');
            }
        }
        $model = Contact::where(['id' => Auth::user()->id])->first();
        return view('fjob-posts::.add_edit_job',['model'=>$model,'newModel'=>$newModel]);
    } 

    public function update(Request $request, JobPosts $jobPosts, $id)
    {
        // dd($id);
        $newModel=JobPosts::where('id',$id)->first();
        if($request->isMethod('post')){
            $validatedData = $request->validate([
                'posted_on' => 'required|date',
                'apply_by' => 'required|date',
                'auto_remove_after' => 'required|date',
            ]);
            $newModel=JobPosts::where('id',$id)->first();
            $newModel->user_id=Auth::user()->id;
            $newModel->user_type=$request->user_type;
            $newModel->contact_name=$request->contact_name;
            $newModel->contact_email=$request->contact_email;
            $newModel->contact_phone=$request->contact_phone;
            $newModel->job_title=$request->job_title;
            $newModel->position=$request->position;
            $newModel->industry=$request->industry;
            $newModel->skills=$request->skills;
            $newModel->experience=$request->experience;
            $newModel->location=$request->location;
            $newModel->posted_on=$request->posted_on;
            $newModel->auto_remove_after=$request->auto_remove_after;
            $newModel->apply_by=$request->apply_by;
            $newModel->short_description=$request->short_description;
            $newModel->description=$request->description;
            $newModel->published=$request->published;
            if ($newModel->save()) {
                return redirect('jobPosts');
            }
        }
        $model = Contact::where(['id' => Auth::user()->id])->first();
        return view('fjob-posts::.add_edit_job',['model'=>$model,'newModel'=>$newModel]);
    }

    public function view(JobPosts $jobPosts, $id)
    {
        $model=JobPosts::where('id',$id)->first();
        return view('fjob-posts::.job_detail',['model'=>$model]);
    }

    public function delete(Request $request, $id)
    {
        $model = $this->findModel($id);
        $model->delete();
        DB::table($model->getTable())
        ->where('id', $id)
        ->update(['deleted_by' => Auth::user()->id]);
        
        return redirect('jobPosts');
    }
}

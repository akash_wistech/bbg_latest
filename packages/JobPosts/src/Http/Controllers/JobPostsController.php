<?php

namespace Wisdom\JobPosts\Http\Controllers;

use Wisdom\JobPosts\Models\JobPosts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class JobPostsController extends Controller
{
    public function index(Request $request)
    {
        // dd("hello");
        $moduleTypeId = $this->newModel()->moduleTypeId;
        return view('job-posts::.index',compact('moduleTypeId','request'));
    }

    public function newModel()
    {
        return new JobPosts;
    }

    public function datatableData(Request $request)
    {
        $moduleTypeId = $this->newModel()->moduleTypeId;
        $query = DB::table($this->newModel()->getTable())
        ->whereNull('deleted_at');


        if($request->search['value']!=''){
          $keyword = $request->search['value'];
          $srchFlds[]='user_type';
          $srchFlds[]='contact_name';
          $srchFlds[]='contact_email';
          $srchFlds[]='contact_phone';
          $srchFlds[]='job_title';
          $srchFlds[]='position';
          $srchFlds[]='industry';
          $srchFlds[]='skills';
          $srchFlds[]='experience';
          $srchFlds[]='location';
          searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
      }

      $countQuery = clone $query;
      $totalRecords = $countQuery->count('id');
      $orderBy = 'id';
      if($request->ob)$orderBy = $request->ob;
      $orderByOrder = 'desc';
      if($request->obo)$orderByOrder = $request->obo;
      $models = $query->offset($request->start)
      ->limit($request->length)
      ->orderBy($orderBy,$orderByOrder)
      ->get();
      $dataArr=[];

      if($models!=null){
        foreach($models as $model){
            $thisData=[];
            $thisData['id']=$model->id;
            $thisData['user_id']=$model->user_id;
            $thisData['user_type']=$model->user_type;
            $thisData['contact_name']=$model->contact_name;
            $thisData['contact_email']=$model->contact_email;
            $thisData['contact_phone']=$model->contact_phone;
            $thisData['job_title']=$model->job_title;
            $thisData['position']=$model->position;
            $thisData['industry']=$model->industry;
            $thisData['skills']=$model->skills;
            $thisData['experience']=$model->experience;
            $thisData['location']=$model->location;
            $thisData['posted_on']=$model->posted_on;
            $thisData['auto_remove_after']=$model->auto_remove_after;
            $thisData['apply_by']=$model->apply_by;
            $thisData['short_description']=$model->short_description;
            $thisData['description']=$model->description;
            $thisData['published']=getStatusIconArr()[$model->published];
            $thisData['cb_col']='';
            $thisData['created_at']=formatDateTime($model->created_at);
            $createdBy = getUserInfo($model->created_by);
            $thisData['created_by']=$createdBy->name;

            $actBtns=[];
            if(checkActionAllowed('view','job-posts')){
                $actBtns[]='
                <a href="'.url('job-posts/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
                <i class="text-dark-50 flaticon-file-2"></i>
                </a>';
            }
            if(checkActionAllowed('update','job-posts')){
                $actBtns[]='
                <a href="'.url('job-posts/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
                <i class="text-dark-50 flaticon-edit"></i>
                </a>';
            }
            if(checkActionAllowed('delete','job-posts')){
                $actBtns[]='
                <a href="'.url('job-posts/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
                <i class="text-dark-50 flaticon-delete"></i>
                </a>';
            }

            $thisData['action_col']=getDatatableActionTemplate($actBtns);
            $dataArr[]=$thisData;
        }
    }


    $dtInputColsArr = [];
    $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
        'columns' => $dtInputColsArr,
        'draw' => (int)$request->draw,
        'length' => (int)$request->length,
        'order' => [
            ['column'=>0,'dir'=>'asc'],
        ],
        'search' => ['value'=>$request->search['value'],'regex'=>false],
        'start' => $request->start,
    ];

    $response = [
        'draw' => (int)$request->draw,
        'input' => $inputArr,
        'recordsTotal' => $totalRecords,
        'recordsFiltered' => $totalRecords,
        'data' => $dataArr,
    ];
    return new JsonResponse($response);
}


public function create(Request $request)
{
        // dd($request->all());
    $model = new JobPosts;
    if($request->isMethod('post')){
        $validatedData = $request->validate([
            'posted_on' => 'required|date',
            'apply_by' => 'required|date',
            'auto_remove_after' => 'required|date',
        ]);
        $model = new JobPosts;
        $model->user_id=$request->user_id;
        $model->user_type=$request->user_type;
        $model->contact_name=$request->contact_name;
        $model->contact_email=$request->contact_email;
        $model->contact_phone=$request->contact_phone;
        $model->job_title=$request->job_title;
        $model->position=$request->position;
        $model->industry=$request->industry;
        $model->skills=$request->skills;
        $model->experience=$request->experience;
        $model->location=$request->location;
        $model->posted_on=$request->posted_on;
        $model->auto_remove_after=$request->auto_remove_after;
        $model->apply_by=$request->apply_by;
        $model->short_description=$request->short_description;
        $model->description=$request->description;
        $model->published=$request->published;
        $model->created_by=Auth::user()->id;
        if ($model->save()) {
            return redirect('job-posts')->with('success', __('jobposts::jobposts.saved'));
        }
    }
    return view('job-posts::.bridge',['model'=>$model]);
}

public function view(JobPosts $jobPosts, $id)
{
    $model=JobPosts::where('id',$id)->first();
    return view('job-posts::.view',compact('model'));
}


public function update(Request $request, JobPosts $jobPosts, $id)
{
    $model=JobPosts::where('id',$id)->first();
    if($request->isMethod('post')){
        $validatedData = $request->validate([
            'posted_on' => 'required|date',
            'apply_by' => 'required|date',
            'auto_remove_after' => 'required|date',
        ]);
        $model=JobPosts::where('id',$id)->first();
        $model->user_id=$request->user_id;
        $model->user_type=$request->user_type;
        $model->contact_name=$request->contact_name;
        $model->contact_email=$request->contact_email;
        $model->contact_phone=$request->contact_phone;
        $model->job_title=$request->job_title;
        $model->position=$request->position;
        $model->industry=$request->industry;
        $model->skills=$request->skills;
        $model->experience=$request->experience;
        $model->location=$request->location;
        $model->posted_on=$request->posted_on;
        $model->auto_remove_after=$request->auto_remove_after;
        $model->apply_by=$request->apply_by;
        $model->short_description=$request->short_description;
        $model->description=$request->description;
        $model->published=$request->published;
        if ($model->save()) {
            return redirect('job-posts')->with('success', __('jobposts::jobposts.updated'));
        }
    }
    return view('job-posts::.bridge',['model'=>$model]);
}

public function findModel($id)
{
    return JobPosts::where('id', $id)->first();
}
}

<?php
use App\Models\User;

if (!function_exists('getJobPostsStatusArr')) {
  function getJobPostsStatusArr()
  {
    return [
      '0' => __('jobposts::jobposts.un_publish'),
      '1' => __('jobposts::jobposts.publish'),
    ];
  }
}


if (!function_exists('getMemberForJobPosts')) {
  function getMemberForJobPosts()
  {
    return User::select(['id','name'])->get()->pluck('name', 'id')->toArray();
    
  }
}

if (!function_exists('getJobsFendStatus')) {
  /**
  * return status array
  */
  function getJobsFendStatus()
  {
    return [
      '0' => '<span class="label label label-danger">'.__('jobposts::jobposts.un_publish').'</span>',
      '1' => '<span class="label label label-success">'.__('jobposts::jobposts.publish').'</span>',
    ];
  }
}
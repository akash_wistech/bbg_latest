<?php

namespace App\Console;

use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Stringable;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      //Send subscription expiry before 30 days
      $schedule->call(function () {
        $now=Carbon::now();


      })->everyMinute()->runInBackground();

      //Update is_active col in subscription where end_date is passed
      //Expiring
      $schedule->call(function () {
        $now=Carbon::now();

        DB::table((new Subscription)->getTable())
        ->where('end_date', '<', $now)
        ->where('is_active',1)
        ->update(['is_active'=>0]);
      })->everyMinute()->runInBackground()
      ->onSuccess(function (Stringable $output) {
        // Log::info("Expiry Artisan command executed.");
      });

      //Update is_active col in subscription where start_date on and is_active=0
      //activating subscriptions
      $schedule->call(function () {
        $now=Carbon::now();

        DB::table((new Subscription)->getTable())
        ->where('start_date', '>=', $now)
        ->where('is_active',0)
        ->update(['is_active'=>1]);
      })->everyMinute()->runInBackground()
      ->onSuccess(function (Stringable $output) {
        // Log::info("Activating Artisan command executed.");
      });
      // Log::info("All");

        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

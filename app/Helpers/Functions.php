<?php
// use Image;
use App\Models\Setting;
use App\Models\Prospect;
use App\Models\Opportunity;
use App\Models\User;
use App\Models\Company;
use NaeemAwan\MultipleEmailPhone\Models\ModuleEmail;
use NaeemAwan\MultipleEmailPhone\Models\ModuleNumber;;
use App\Models\SavedSearchInputValue;
use NaeemAwan\PredefinedLists\Models\PredefinedList;
use NaeemAwan\ModuleProcess\Models\Workflow;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Event\Models\EventFee;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Encryption\Encrypter;
use Umair\Campaign\Models\Template;

//Import
use NaeemAwan\PredefinedLists\Models\PredefinedListDetail;

if (!function_exists('getFilterTagHtml')) {
  /**
  * return filter tag html
  */
  function getFilterTagHtml($inputFld,$filterTag)
  {
    $html = '';
    if(isset($filterTag['value']) && is_array($filterTag['value'])){
      if(count($filterTag['value'])>0){
        $headingAdded=false;
        $heading = '<strong>'.$filterTag['title'].'</strong><br />';
        foreach($filterTag['value'] as $key=>$val){
          if($val!=''){
            $html.=($headingAdded==false ? $heading : '').'<span class="label label-inline label-lg label-light-primary font-weight-bold mb-2">'.$val.' <i class="fa fa-times icon-sm ml-1 remove-filter" data-key="'.$inputFld.'"></i></span>';
            $headingAdded=true;
          }
        }
      }
    }else{
      if(isset($filterTag['value']) && $filterTag['value']!=''){
        $html.=$filterTag['title'].': '.'<span class="label label-inline label-lg label-light-primary font-weight-bold mb-2">'.$filterTag['value'].'</span>';
      }
    }
    return $html;
  }
}

if (!function_exists('getPageSizeAssArr')) {
  /**
  * return page sizes
  */
  function getPageSizeAssArr()
  {
    return [
      25=>25,
      50=>50,
      75=>75,
      100=>100,
      150=>150,
      200=>200,
    ];
  }
}

if (!function_exists('getPageSizeArr')) {
  /**
  * return page sizes
  */
  function getPageSizeArr()
  {
    return [
      25,
      50,
      75,
      100,
      150,
      200,
    ];
  }
}

if (!function_exists('getCompanyVisibilityListArr')) {
  /**
  * return Company visibility options
  */
  function getCompanyVisibilityListArr()
  {
    return [
      '1'=>__('common.visible'),
      '0'=>__('common.hidden'),
    ];
  }
}

if (!function_exists('getSystemLanguages')) {
  /**
  * return all languages
  */
  function getSystemLanguages()
  {
    return [
      'en'=>'English',
      // 'ar'=>'Arabic',
    ];
  }
}

if (!function_exists('getSystemRtlLanguages')) {
  /**
  * return all rtl languages
  */
  function getSystemRtlLanguages()
  {
    return [
      'ar',
    ];
  }
}

if (!function_exists('getYesNoArr')) {
  /**
  * return status array
  */
  function getYesNoArr()
  {
    return [
      '1' => __('common.yes'),
      '0' => __('common.no'),
    ];
  }
}

if (!function_exists('getYesNoLabelArr')) {
  /**
  * return status label
  */
  function getYesNoLabelArr()
  {
    return [
      '1' => '<span class="label label-inline label-success">'.__('common.yes').'</span>',
      '0' => '<span class="label label-inline label-warning">'.__('common.no').'</span>',
    ];
  }
}

if (!function_exists('getStatusArr')) {
  /**
  * return status array
  */
  function getStatusArr()
  {
    return [
      '1' => __('common.enabled'),
      '0' => __('common.disabled'),
    ];
  }
}

if (!function_exists('getStatusArr')) {
  /**
  * return status array
  */
  function getStatusArr()
  {
    return [
      '1' => __('common.enabled'),
      '0' => __('common.disabled'),
    ];
  }
}

if (!function_exists('getStatusConfirmMsg')) {
  /**
  * return status confirm msg array
  */
  function getStatusConfirmMsg()
  {
    return [
      '1' => __('common.confirmDisable'),
      '0' => __('common.confirmEnable'),
    ];
  }
}

if (!function_exists('getStatusIconArr')) {
  /**
  * return status array
  */
  function getStatusIconArr()
  {
    return [
      '0'=>'<span class="label label-rounded label-warning"><i class="fas text-white fa-hourglass"></i></span>',
      '1'=>'<span class="label label-rounded label-success"><i class="fas text-white fa-check"></i></span>',
    ];
  }
}

if (!function_exists('getStatusTextArr')) {
  /**
  * return status text array
  */
  function getStatusTextArr()
  {
    return [
      '0'=>'<span class="label label-inline label-light-warning">'.__('common.in_active').'</span>',
      '1'=>'<span class="label label-inline label-light-success">'.__('common.active').'</span>',
      '2'=>'<span class="label label-inline label-light-primary">'.__('common.invoiced').'</span>',
      '3'=>'<span class="label label-inline label-light-danger">'.__('common.sdeleted').'</span>',
      '4'=>'<span class="label label-inline label-light-info">'.__('common.renewal_required').'</span>',
      '10'=>'<span class="label label-inline label-light-warning">'.__('common.in_active').'</span>',
    ];
  }
}

if (!function_exists('getEnableDisableIcon')) {
  /**
  * return status for a given key
  */
  function getEnableDisableIcon($n = null)
  {
    return getStatusIconArr()[$n];
  }
}

if (!function_exists('getEnableDisableIcon')) {
  /**
  * return status for a given key
  */
  function getEnableDisableIcon($n = null)
  {
    return getStatusIconArr()[$n];
  }
}

if (!function_exists('getEnableDisableIconWithLink')) {
  /**
  * return status for a given key with toggle link
  */
  function getEnableDisableIconWithLink($n,$route,$confrmMsg,$contnr)
  {
    return '<a href="'.url($route).'" class="act-confirmation" data-toggle="tooltip" title="" data-confirmationmsg="'.$confrmMsg.'" data-pjxcntr="'.$contnr.'">'.getStatusIconArr()[$n].'</a>';
  }
}

if (!function_exists('temp_upload_path')) {
  /**
  * return temp upload dir path
  */
  function temp_upload_path()
  {
    return storage_path('temp_uploads');
  }
}

if (!function_exists('image_storage_dir')) {
  /**
  * return storage dir path
  */
  function image_storage_dir()
  {
    return config('image.dir');
  }
}

if (!function_exists('uploadImage')) {
  /**
  * return storage dir path
  */
  function uploadImage($request,$fileInput,$destinationPath)
  {
    $image = $request->file($fileInput) ;
    $fileName = generateName() . '.' . $image->getClientOriginalExtension();

    $img = Image::make($image->getRealPath());

    $img->stream();
    Storage::disk('local')->put($destinationPath.'/'.$fileName, $img, 'public');
    return $fileName;
  }
}

if (!function_exists('getStorageFileUrl')) {
  /**
  * return storage dir path
  */
  function getStorageFileUrl($path = null, $size = 'small')
  {
    if (!$path) {
      return getPlaceholderImg($size);
    }
    if ($size == null) {
      return url("{$path}");
    }
    return url("{$path}?p={$size}");
  }
}


if (!function_exists('getPlaceholderImg')) {
  /**
  * return placeholder image
  */
  function getPlaceholderImg($size = 'small')
  {
    $size = config("image.sizes.{$size}");

    if ($size && is_array($size)) {
      return "https://via.placeholder.com/{$size['w']}x{$size['h']}/eee?text=" . __('app.general.no_img_available');
    }

    return url("images/upload-file.svg");
  }
}

if (!function_exists('imageCachePath')) {
  /**
  * return cache path
  */
  function imageCachePath($path = null)
  {
    $path = config('image.cache_dir') . '/' . $path;
    return str_finish($path, '/');
  }
}

if (!function_exists('getSetting')) {
  /**
  * return storage dir path
  */
  function getSetting($keyword)
  {
    // echo '>'.$i.'<';
    $model = Setting::select('config_value')->where('config_name',$keyword)->first();
    if($model!=null){
      return $model->config_value;
    }
    return 20;
  }
}

if (!function_exists('truncateText')) {
  /**
  * return storage dir path
  */
  function truncateText($str, $limit=50)
  {
    $length = strlen($str);
    return substr($str,0,$limit).($length>$limit ? '...' : '');

  }
}

if (!function_exists('generateName')) {
  /**
  * return random string for name
  */
  function generateName($length=30)
  {
    return str_replace(array(" ","."),"",microtime());
  }
}

if (!function_exists('generateRandomString')) {
  /**
  * return random string
  */
  function generateRandomString($length=30)
  {
    $key = generateTimeId();
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
    }

    return $key;
  }
}


if (!function_exists('generateTimeId')) {
  /**
  * Generate Time Token
  */
  function generateTimeId()
  {
    return str_replace(array(" ","."),"",microtime());
  }
}

if (!function_exists('generateNumberToken')) {
  /**
  * Generate Number Token
  */
  function generateNumberToken($length)
  {
    $key = '';
    $keys = range(1, 9);

    for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
    }

    return $key;
  }
}

if (!function_exists('generateToken')) {
  /**
  * Generate Token
  */
  function generateToken($length)
  {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
    }

    return $key;
  }
}

if (!function_exists('calculatePercentageAmount')) {
  /**
  * Calculate percentage
  */
  function calculatePercentageAmount($total,$percent)
  {
    return ((double)$percent/100)*(double)$total;
  }
}

if (!function_exists('getCurrentControllerAndAction')) {
  /**
  * return current controller action
  */
  function getCurrentControllerAndAction()
  {
    $request = app('request')->route()->getAction();
    $route=app('request')->path();
    $controller = class_basename($request['controller']);
    list($controller, $action) = explode('@', $controller);
    $controller=str_replace("Controller","",$controller);
    $controller=strtolower($controller);
    return ['route'=>$route,'controller'=>$controller,'action'=>$action];
  }
}

if (!function_exists('image_storage_dir')) {
  /**
  * return storage dir path
  */
  function image_storage_dir()
  {
    return config('image.dir');
  }
}

if (!function_exists('getStorageFileUrl')) {
  /**
  * return storage dir path
  */
  function getStorageFileUrl($path = null, $size = 'small')
  {
    if (!$path) {
      return getPlaceholderImg($size);
    }
    if ($size == null) {
      return url("image/{$path}");
    }
    return url("image/{$path}?p={$size}");
  }
}


if (!function_exists('getPlaceholderImg')) {
  /**
  * return placeholder image
  */
  function getPlaceholderImg($size = 'small')
  {
    $size = config("image.sizes.{$size}");

    if ($size && is_array($size)) {
      return "https://via.placeholder.com/{$size['w']}x{$size['h']}/eee?text=" . __('app.general.no_img_available');
    }

    return url("images/upload-file.svg");
  }
}

if (!function_exists('imageCachePath')) {
  /**
  * return cache path
  */
  function imageCachePath($path = null)
  {
    $path = config('image.cache_dir') . '/' . $path;
    return str_finish($path, '/');
  }
}

if (!function_exists('getModulesHavingOpportunity')) {
  /**
  * returns array of modules
  */
  function getModulesHavingOpportunity()
  {
    return [
      'prospect',
      // 'client',
      'contact',
    ];
  }
}

if (!function_exists('getModulesHavingReports')) {
  /**
  * returns array of modules
  */
  function getModulesHavingReports()
  {
    return [
      'prospect',
      // 'client',
      'contact',
      'opportunity',
      // 'lead',
    ];
  }
}

if (!function_exists('getModulesHavingSelection')) {
  /**
  * returns array of modules
  */
  function getModulesHavingSelection()
  {
    return [
      'prospect',
      // 'client',
      'contact',
    ];
  }
}

if (!function_exists('getModuleTypeArr')) {
  /**
  * returns array of modules
  */
  function getModuleTypeArr()
  {
    return [
      'company' =>'Companies',
      'contact' =>'Contacts',
      'prospect' =>'Prospects',
      'opportunity' =>'Opportunities',
      'events' =>'Event',
      'event-member-register' =>'Event Registration Form',
      // 'lead' =>'Leads',
    ];
  }
}

if (!function_exists('getModulesListArr')) {
  /**
  * returns array of Event Status
  */
  function getModulesListArr()
  {
    return [
      'contact' => __('app.general.contacts'),
      'company' => __('app.general.companies'),
      'prospect' => __('app.general.prospects'),
      'opportunity' => __('app.general.opportunities'),
      // 'lead' => __('app.general.leads'),
    ];
  }
}

if (!function_exists('getModulesLabelListArr')) {
  /**
  * returns array of Event Status
  */
  function getModulesLabelListArr()
  {
    return [
      'contact' => __('app.general.contact'),
      'prospect' => __('app.general.prospects'),
      'opportunity' => __('app.general.opportunities'),
      'lead' => __('app.general.leads'),
      'client' => __('app.general.client'),
    ];
  }
}

if (!function_exists('getModulesDynNameListArr')) {
  /**
  * returns array of Event Status
  */
  function getModulesDynNameListArr()
  {
    return [
      'prospect' => [
        'prospect-1'=>__('app.general.prospects_1'),
        'prospect-2'=>__('app.general.prospects_2')
      ],
    ];
  }
}

if (!function_exists('getSourcedModulesArr')) {
  /**
  * returns array of Event Status
  */
  function getSourcedModulesArr()
  {
    return [
      'prospect' => __('app.general.prospects'),
    ];
  }
}

if (!function_exists('getModelClass')) {
  /**
  * returns array of Event Status
  */
  function getModelClass()
  {
    return [
      'prospect' => '\App\Models\Prospect',
      'company' => '\App\Models\Company',
      'contact' => '\App\Models\Contact',
      'opportunity' => '\App\Models\Opportunity',
      'lead' => '\App\Models\Opportunity',
      'invoice' => '\Wisdom\Sales\Models\Invoices',
      'payment' => '\Wisdom\Sales\Models\Payments',
      'list' => '\Umair\Campaign\Models\ListName',
      'listitem' => '\Umair\Campaign\Models\ListName',
      'required-template' => '\Wisdom\Sales\Models\RequiredTemplate',
      'required-document' => '\Wisdom\Sales\Models\RequiredDocument',
      'group' => '\Wisdom\Sales\Models\Group',
      'proposalpage' => '\Wisdom\Sales\Models\Page',
      'proposal_categories' => '\Wisdom\Sales\Models\ProposalCategory',
      'event' => '\Wisdom\Event\Models\Event',
      'events' => '\Wisdom\Event\Models\Event',
    ];
  }
}

if (!function_exists('getModuleSourceNameField')) {
  /**
  * returns array of source table field
  */
  function getModuleSourceNameField()
  {
    return [
      'prospect' => 'full_name',
      'company' => 'title',
      'contact' => 'name',
    ];
  }
}

if (!function_exists('getModelUrlPre')) {
  /**
  * returns array of Event Status
  */
  function getModelUrlPre()
  {
    return [
      'prospect' => 'prospect',
      'company' => 'company',
      'opportunity' =>'opportunity',
      'event' =>'event',
      'events' =>'events',
      'contact' =>'contact',
      // 'lead' =>'lead',
    ];
  }
}

if (!function_exists('actionLogTypesArr')) {
  /**
  * returns array of Event Status
  */
  function actionLogTypesArr()
  {
    return [
      'comment'=>__('actionlog::activity.comment'),
      'action'=>__('actionlog::activity.action'),
      'call'=>__('actionlog::activity.log_call'),
      'timer'=>__('actionlog::activity.log_time'),
      'calendar'=>__('actionlog::activity.calendar'),
      'attachment'=>__('actionlog::activity.attachment'),
    ];
  }
}

if (!function_exists('getModuleRow')) {
  /**
  * returns array of modules
  */
  function getModuleRow($moduleTypeId,$id)
  {
    if ($moduleTypeId) {
      $modelClass = getModelClass()[$moduleTypeId];
      return $modelClass::where('id',$id)->first();
    }else {
      return '';
    }

    // if($moduleTypeId=='prospect'){
    //   return Prospect::where('id',$id)->first();
    // }
  }
}

if (!function_exists('getModuleNameFieldValue')) {
  /**
  * returns array of modules
  */
  function getModuleNameFieldValue($model)
  {
    if(in_array($model->moduleTypeId,['prospect'])){
      return $model->full_name.($model->company_name!='' ? ' - '.$model->company_name : '');
    }else{
      return $model->title;
    }
  }
}

if (!function_exists('shortDate')) {
  /**
  * returns formated short date
  */
  function shortDate($datetime)
  {
    return date("j M",strtotime($datetime));
  }
}

if (!function_exists('formatTime')) {
  /**
  * returns formated short date
  */
  function formatTime($datetime)
  {
    return date("h:i a",strtotime($datetime));
  }
}

if (!function_exists('formatDate')) {
  /**
  * returns formated date
  */
  function formatDate($datetime)
  {
    return date("D, jS M Y",strtotime($datetime));
  }
}

if (!function_exists('formatDateTime')) {
  /**
  * returns formated date time
  */
  function formatDateTime($datetime)
  {
    return date("j M, Y / h:i a",strtotime($datetime));
  }
}

if (!function_exists('calendarDateFormat')) {
  /**
  * returns formated date time
  */
  function calendarDateFormat($date,$inc_time=false)
  {
    return date("Y-m-d".($inc_time==true ? ' H:i:s' : '')."",strtotime($date));
  }
}

if (!function_exists('getDatatableActionTemplate')) {
  /**
  * returns datatable action template
  */
  function getDatatableActionTemplate($actionBtns)
  {
    return implode("",$actionBtns);
		// return '<div class="btn-group flex-wrap">
		// 	<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
		// 		<span class="caret"></span>
		// 	</button>
		// 	<div class="dropdown-menu" role="menu">
		// 		'..'
		// 	</div>
		// </div>';
  }
}

if (!function_exists('convertTime')) {
  /**
  * converts a given date time to ago info string respect to current date time
  */
  function convertTime($time_ago2)
  {
    $time_ago = strtotime($time_ago2);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );

    if($seconds <= 60){// Seconds
      return __('common.justnow');
    }else if($minutes <=60){//Minutes
      if($minutes==1){
        return __('common.one_min_ago');
      }else{
        return __('common.some_minu_ago',['min'=>$minutes]);
      }
    }else if($hours <=24){//Hours
      if($hours==1){
        return __('common.an_hr_ago');
      }else{
        return __('some_hr_ago',['hr'=>$hours]);
      }
    }else{
      return formatDateTime($time_ago2);
    }
  }
}

if (!function_exists('getModuleNameByType')) {
  /**
  * returns datatable action template
  */
  function getModuleNameByType($modelType,$moduleId)
  {
    $html = '';
    // if($modelType=='opportunity'){
    //   $html = $model->title;
    // }
    if($modelType=='prospect'){
      $model = (getModelClass()[$modelType])::where('id',$moduleId)->first();
      if($model!=null){
        $html = $model->full_name.($model->company_name!='' ? ' - '.$model->company_name : '');
      }
    }
    return $html!='' ? ': '.$html : '';
  }
}

if (!function_exists('getModuleListDropdownItems')) {
  /**
  * returns datatable action template
  */
  function getModuleListDropdownItems($moduleTypeId,$layout,$controller)
  {
    $arr = [];
    $urlPre = $controller;
    if($layout==''){
      $arr = getModuleServices($moduleTypeId,$layout,$urlPre,$arr);
    }else{
      $arr = getModuleWorkFlows($moduleTypeId,$layout,$urlPre,$arr);
    }
    return $arr;
  }
}

if (!function_exists('getReverseColor')) {
    /**
     * return reverse color for given color
     */
    function getReverseColor($colorCode)
    {
      $d = 0;
      list($red, $green, $blue) = sscanf($colorCode, "#%02x%02x%02x");
      $l = ( 0.299 * $red + 0.587 * $green + 0.114 * $blue)/255;
      if($l > 0.5)$d=0;
      else $d=255;
      return 'rgb('.$d.','.$d.','.$d.')';
    }
  }

  if (!function_exists('getCurrenciesListArr')) {
    /**
     * return currencies
     */
    function getCurrenciesListArr()
    {
      return [
        1=>'AED',
        2=>'USD',
        3=>'GBP',
      ];
    }
  }

  if (!function_exists('getCurrencyPre')) {
    /**
     * return currency prefix
     */
    function getCurrencyPre()
    {
      return getCurrenciesListArr()[getSetting('currency')].' ';
      // return getSetting('currency_sign').' ';
    }
  }

  if (!function_exists('getDashboardStats')) {
    /**
     * return dashboard stats array
     */
    function getDashboardStats()
    {
      //Check member allowed list types
      $dashboardStats=[];
      //Raw Data
      $prospectsCount = Prospect::count('id');
      $dashboardStats[]=['total'=>$prospectsCount,'title'=>__('app.general.prospects'),'icon'=>'flaticon-layers icon-2x','bgClass'=>'b1g-light-success'];
      //Opportunities
      $opModuleTypeId='opportunity';
      $opportunitiesQuery = Opportunity::where('rec_type','o');
      permissionListTypeFilter($opModuleTypeId,(new Opportunity)->getTable(),$opModuleTypeId,$opportunitiesQuery);
      $opportunitiesCount = $opportunitiesQuery->count('id');
      $dashboardStats[]=['total'=>$opportunitiesCount,'title'=>__('app.general.opportunities'),'icon'=>'flaticon-clipboard icon-2x','bgClass'=>'b1g-light-info'];
      //Leads
      $ldModuleTypeId='opportunity';
      $leadsQuery = Opportunity::where('rec_type','l');
      permissionListTypeFilter($ldModuleTypeId,(new Opportunity)->getTable(),$ldModuleTypeId,$leadsQuery);
      $leadsCount = $leadsQuery->count('id');
      $dashboardStats[]=['total'=>$leadsCount,'title'=>__('app.general.leads'),'icon'=>'flaticon-list-2 icon-2x','bgClass'=>'b1g-light-primary'];
      //Services
      $serviceTypesCount = PredefinedList::where('parent_id',getSetting('service_list_id'))->count('id');
      $dashboardStats[]=['total'=>$serviceTypesCount,'title'=>__('common.service_type'),'icon'=>'flaticon-apps icon-2x','bgClass'=>'b1g-light-warning'];
      //Process
      $workflowCount = WorkFlow::count('id');
      $dashboardStats[]=['total'=>$workflowCount,'title'=>__('common.process'),'icon'=>'flaticon-refresh icon-2x','bgClass'=>'b1g-light-success'];
      if(auth()->user()->user_type==20){
        //Staff
        $staffCount = User::where('user_type','10')->count('id');
        $dashboardStats[]=['total'=>$staffCount,'title'=>__('app.general.staff'),'icon'=>'flaticon2-group icon-2x','bgClass'=>'b1g-light-success'];
      }
      return $dashboardStats;
    }
  }

  if (!function_exists('getSavedSearchInputValues')) {
    /**
     * return saved search input values
     */
    function getSavedSearchInputValues($id)
    {
      return SavedSearchInputValue::where('saved_search_id',$id)->get();
    }
  }


  if (!function_exists('getModuleDuplicateId')) {
  /**
  * return module duplicate id
  */
  function getModuleDuplicateId($moduleTypeId,$request)
  {
    $id = 0;
    $modelClass = getModelClass()[$moduleTypeId];
    $query = DB::table((new $modelClass)->getTable());
    if($moduleTypeId=='prospect'){
      $query->where([
        (new $modelClass)->getTable().'.email' => $request['email'],
        (new $modelClass)->getTable().'.phone' => $request['phone'],
      ]);
    }
    $duplicateRow = $query->first();
    if($duplicateRow!=null){
      $id = $duplicateRow->id;
    }
    return $id;
  }
}

if (!function_exists('getUserDuplicateRow')) {
  /**
  * return user module duplicate id
  */
  function getUserDuplicateRow($module,$request)
  {
    $modelClass = getModelClass()[$module];
    $rowId = $request['id'];
    if($request['emails']!=null && count($request['emails'])>0){
      foreach($request['emails'] as $key=>$val){
        //Check in main row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->where((new $modelClass)->getTable().'.email', $val);
        $query->where('user_type',0);
        $query->whereNull('deleted_at');
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          return $duplicateRow;
        }

        //Check in multi row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->whereIn((new $modelClass)->getTable().'.id', function($query) use ($module,$val){
          $query->select('module_id')
          ->from(with(new ModuleEmail)->getTable())
          ->where('module_type', $module)
          ->where('email', $val);
        });
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          return $duplicateRow;
        }
      }
    }
    if($request['numbers']!=null && count($request['numbers'])>0){
      foreach($request['numbers'] as $key=>$val){
        //Check in main row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->where((new $modelClass)->getTable().'.phone', $val);
        $query->where('user_type',0);
        $query->whereNull('deleted_at');
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          return $duplicateRow;
        }

        //Check in multi row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->whereIn((new $modelClass)->getTable().'.id', function($query) use ($module,$val){
          $query->select('module_id')
          ->from(with(new ModuleNumber)->getTable())
          ->where('module_type', $module)
          ->where('phone', $val);
        });
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          return $duplicateRow;
        }
      }
    }
  }
}

if (!function_exists('DuplicateUserCheckFrontend')) {
  /**
   * return duplicate check
   */
  function DuplicateUserCheckFrontend($module,$data)
  {
    $modelClass = getModelClass()[$module];
    //Check in multi row
    if(isset($data['email'])){
      $query = DB::table((new $modelClass)->getTable());

      $query->where((new $modelClass)->getTable().'.email', $data['email']);
      $query->where('user_type',0);
      $query->whereNull('deleted_at');
      $duplicateRow = $query->first();
      if($duplicateRow!=null){
        return $duplicateRow;
      }
    }

    //Check in multi row
    if(isset($data['phone'])){
      $query = DB::table((new $modelClass)->getTable());

      $query->where((new $modelClass)->getTable().'.phone', $data['phone']);
      $query->where('user_type',0);
      $query->whereNull('deleted_at');
      $duplicateRow = $query->first();
      if($duplicateRow!=null){
        return $duplicateRow;
      }
    }
  }
}

if (!function_exists('getWeekDays')) {
    /**
     * return saved search input values
     */
    function getWeekDays()
    {
      return [
        '0'=>'sunday',
        '1'=>'monday',
        '2'=>'tuesday',
        '3'=>'wednesday',
        '4'=>'thursday',
        '5'=>'friday',
        '6'=>'saturday'
      ];
    }
  }

  if (!function_exists('getWeekDaysArr')) {
    /**
     * return saved search input values
     */
    function getWeekDaysArr()
    {
      $arr = [];
      $weekdays = getWeekDays();
      foreach($weekdays as $key=>$val){
        $arr[$key]=__('common.'.$val);
      }
      return $arr;
    }
  }

  if (!function_exists('getWeekStartEnd')) {
    /**
     * return formatted number string
     */
    function getWeekStartEnd($date)
    {
      // echo $date.'<br />';
      $startDate='';$endDate='';
      $weekStartDay = getSetting('week_start');
      $lastDay = getWeekDays()[$weekStartDay];
      $thisWeekDay = date("w");
      if(date("Y-m-d")==$date && $weekStartDay==$thisWeekDay){
        $startDate = date("Y-m-d");
        $endDate = date("Y-m-d",strtotime("+6 days"));
      }else{
        $ts = strtotime($date);
        $start = strtotime('last '.$lastDay, $ts);
        $startDate = date('Y-m-d', $start);

        $endDate = getWeekDays()[($weekStartDay-1)];
        $endDate = date('Y-m-d', strtotime('next '.$endDate, $start));
      }
      // echo $startDate.' - '.$endDate.'<hr />';
      return [
        $startDate,$endDate
      ];
    }
  }

  if (!function_exists('shortNumberString')) {
    /**
     * return formatted number string
     */
    function shortNumberString($count)
    {
      return $count;
    }
  }

  if (!function_exists('getModuleStats')) {
    /**
     * return stats for module list
     */
    function getModuleStats($controller,$module,$condition)
    {
      $classListArr = getModelClass();
      $statsArr=[];
      if(isset($classListArr[$module])){
        $modelClass = $classListArr[$module];

        $query = DB::table((new $modelClass)->getTable())->whereNull('deleted_at');
        if($condition!=null){
          foreach($condition as $key=>$val){
            $query->where($key,$val);
          }
        }
        $lastWeekQuery = $thisWeekUpdatedQuery = $thisWeekQuery = $thisMonthQuery = $totalCountQuery = $query;

        $total = $totalCountQuery->count('id');

        $monthStartDate = Carbon::parse('first day of this month');
        $monthEndDate = Carbon::parse('last day of this month');
        $totalThisMonth = $thisMonthQuery->whereRaw('MONTH(created_at)=?',[date("m")])->count('id');

        list($thisWeekStart,$thisWeekEnd) = getWeekStartEnd(date("Y-m-d"));
        // echo $thisWeekStart.' - '.$thisWeekEnd;
        $totalThisWeek = $thisWeekQuery->where('created_at','>=',$thisWeekStart)->where('created_at','<=',$thisWeekEnd)->count('id');
        $totalUpdatedThisWeek = $thisWeekUpdatedQuery->where('created_at','>=',$thisWeekStart)->where('created_at','<=',$thisWeekEnd)->count('id');

        $yesterday = date("Y-m-d",strtotime("-1 day",strtotime($thisWeekStart)));
        list($lastWeekStart,$lastWeekEnd) = getWeekStartEnd($yesterday);
        // echo $yesterday.' - '.$thisWeekStart.' / '.$lastWeekStart.' - '.$lastWeekEnd;
        $totalLastWeek = $lastWeekQuery->where('created_at','>=',$lastWeekStart)->where('created_at','<=',$lastWeekEnd)->count('id');

        $statsArr[]=['icon'=>'flaticon-layer','title'=>__('app.stats.total'),'count'=>shortNumberString($total),'url'=>$controller.''];
        $statsArr[]=['icon'=>'flaticon-calendar-2','title'=>__('app.stats.created_this_month'),'count'=>shortNumberString($totalThisMonth),'url'=>$controller.'?created_at='.$monthStartDate.' - '.$monthEndDate];
        $statsArr[]=['icon'=>'flaticon-event-calendar-symbol','title'=>__('app.stats.created_this_week'),'count'=>shortNumberString($totalThisWeek),'url'=>$controller.'?created_at='.$thisWeekStart.' - '.$thisWeekEnd];
        $statsArr[]=['icon'=>'flaticon-calendar-with-a-clock-time-tools','title'=>__('app.stats.created_last_week'),'count'=>shortNumberString($totalLastWeek),'url'=>$controller.'?created_at='.$lastWeekStart.' - '.$lastWeekEnd];
        if($module=='opportunity'){
          // $statsArr[]=['icon'=>'flaticon-calendar-with-a-clock-time-tools','title'=>__('app.stats.updated_this_week'),'count'=>shortNumberString($totalUpdatedThisWeek)];
        }
      }
      return $statsArr;
    }
  }

  if (!function_exists('getModalOpenHtml')) {
  /**
  * return modal opening html
  */
  function getModalOpenHtml($id,$heading)
  {
    $html = '<div class="mmcls modal fade" id="'.$id.'" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">';
    $html.= ' <div class="modal-dialog modal-lg" role="document">';
    $html.= '   <div class="modal-content">';
    $html.= '     <div class="modal-header">';
    $html.= '       <h5 class="modal-title" id="exampleModalLabel">'.$heading.'</h5>';
    $html.= '       <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
    $html.= '         <i aria-hidden="true" class="ki ki-close"></i>';
    $html.= '       </button>';
    $html.= '     </div>';
    $html.= '     <div class="modal-body">';
    return $html;
  }
}

if (!function_exists('getModalCloseHtml')) {
  /**
  * return modal closing html
  */
  function getModalCloseHtml($btns=null)
  {
    $html = '     </div>';
    if($btns!=null){
      $html.= '     <div class="modal-footer">';
      // $html.= '       <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>';
      // $html.= '       <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>';
      $html.= '     </div>';
    }
    $html.= '   </div>';
    $html.= ' </div>';
    $html.= '</div>';
    return $html;
  }
}

if (!function_exists('getUserInfo')) {
  /**
  * return user model by id
  */
  function getUserInfo($id)
  {
    return User::where('id',$id)->first();
  }
}

if (!function_exists('getCompanyInfo')) {
  /**
  * return user model by id
  */
  function getCompanyInfo($id)
  {
    return Company::where('id',$id)->first();
  }
}

if (!function_exists('getCompanyFullName')) {
  /**
  * return user model by id
  */
  function getCompanyFullName($title,$parent_id)
  {
    if($parent_id>0){
      $parentCompant = DB::table((new Company)->getTable())
      ->select(['title','parent_company_id'])
      ->where('id',$parent_id)
      ->whereNull('deleted_at')
      ->first();
      if($parentCompant!=null){
        $parentCompanyName = $parentCompant->title;
        $parentCompanyName = $parentCompanyName.' > '.$title;
        // echo $parentCompanyName.'<br />';
        $finalName = getCompanyFullName($parentCompanyName,$parentCompant->parent_company_id);
        $title = $finalName;
      }
    }
    return $title;
  }
}

if (!function_exists('getContactRoleListArr')) {
  /**
  * All predefined lists
  */
  function getContactRoleListArr()
  {
    $id = getSetting('contactroles_list_id');
    return getPredefinedListItemsArr($id);
  }
}

if (!function_exists('JobTitlesListArr')) {
  /**
  * return user model by id
  */
  function JobTitlesListArr()
  {
    $id = getSetting('jobroles_list_id');
    return getPredefinedListItemsArr($id);
  }
}

if (!function_exists('getSectorsListArr')) {
  /**
  * return Sectors / Industries
  */
  function getSectorsListArr()
  {
    $id = getSetting('sector_industry_list_id');
    return getPredefinedListItemsArr($id);
  }
}

if (!function_exists('getModuleRowDetails')) {
  /**
  * return row details for duplication msg
  */
  function getModuleRowDetails($requestArr,$model,$module)
  {

    $html = '';
    $tempModel = $model;
    $tempModel->moduleTypeId = $module;
    $additionalEmails = getModuleSavedEmails($model);
    $additionalEmailsStr = '';
    if($additionalEmails!=null){
      foreach($additionalEmails as $additionalEmail){
        $additionalEmailsStr .= ($additionalEmailsStr!='' ? ', ' : '').$additionalEmail['email'];
      }
    }
    $additionalNumbers = getModuleSavedNumbers($model);
    $additionalNumbersStr = '';
    if($additionalNumbers!=null){
      foreach($additionalNumbers as $additionalNumber){
        $additionalNumbersStr .= ($additionalNumbersStr!='' ? ', ' : '').$additionalNumber['phone'];
      }
    }
    $companies = getModuleSavedCompanies($model);
    $companiesStr = '';
    if($companies!=null){
      foreach($companies as $company){
        $companiesStr .= ($companiesStr!='' ? ', ' : '').$company->company->title;
      }
    }

    $html.= '';
    $html.= '<table class="table table-stripped mt-3">';
    $html.= ' <tr>';
    $html.= '   <th></th>';
    $html.= '   <th>Existing Record</th>';
    $html.= '   <th>New</th>';
    $html.= ' </tr>';

    $html.= ' <tr>';
    $html.= '   <th><strong>'.__('common.full_name').'</strong></th>';
    $html.= '   <td>'.$model->name.'</td>';
    $html.= '   <td>'.$requestArr['name'].'</td>';
    $html.= ' </tr>';

    $html.= ' <tr>';
    $html.= '   <th><strong>'.__('common.email').'</strong></th>';
    $html.= '   <td>'.$model->email.'</td>';
    $html.= '   <td>'.$requestArr['email'].'</td>';
    $html.= ' </tr>';

    $html.= ' <tr>';
    $html.= '   <th><strong>'.__('common.addition_emails').'</strong></th>';
    $html.= '   <td>'.$additionalEmailsStr.'</td>';
    $html.= '   <td>'.($requestArr['nemail']!=null && count($requestArr['nemail'])>0 ? implode(", ",$requestArr['nemail']) : '').'</td>';
    $html.= ' </tr>';

    $html.= ' <tr>';
    $html.= '   <th><strong>'.__('common.phone').'</strong></th>';
    $html.= '   <td>'.$model->phone.'</td>';
    $html.= '   <td>'.$requestArr['phone'].'</td>';
    $html.= ' </tr>';

    $html.= ' <tr>';
    $html.= '   <th><strong>'.__('common.additional_phones').'</strong></th>';
    $html.= '   <td>'.$additionalNumbersStr.'</td>';
    $html.= '   <td>'.($requestArr['nnumbers']!=null && count($requestArr['nnumbers'])>0 ? implode(", ",$requestArr['nnumbers']) : '').'</td>';
    $html.= ' </tr>';

    $html.= ' <tr>';
    $html.= '   <th><strong>'.__('common.company_name').'</strong></th>';
    $html.= '   <td>'.$companiesStr.'</td>';
    $html.= '   <td>'.($requestArr['companies']!=null && count($requestArr['companies'])>0 ? implode(", ",$requestArr['companies']) : '').'</td>';
    $html.= ' </tr>';

    $html.= '</table>';
    return $html;
  }
}

if (!function_exists('assignProspectInfo')) {
  /**
  * Assign prospect info to model
  */
  function assignProspectInfo($model,$id)
  {
    $prospectInfo = Prospect::where('id',$id)->first();
    if($prospectInfo!=null){
      $model->name = $prospectInfo->full_name;
      $model->email = $prospectInfo->email;
      $model->phone = $prospectInfo->phone;
      if($prospectInfo->company_name!=''){
        $company = Company::where('title',trim($prospectInfo->company_name))->first();
        if($company!=null){
          $model->comp_id[1] = $company->id;
          $model->comp_name[1] = $company->title;
        }
      }

      $customFields = getCustomFieldsByModule($model->moduleTypeId);
      if($customFields!=null){
        foreach($customFields as $customField){
          $prospectInfoCustomFieldVal = getInputFielSavedValue($prospectInfo,$customField);
          $model->input_field[$customField['id']] = $prospectInfoCustomFieldVal;
        }
      }
    }
  }
}

if (!function_exists('emailReplacementsList')) {
  /**
  * return user model by id
  */
  function emailReplacementsList()
  {
    return [
    ];
  }
}

if (!function_exists('getEMById')) {
  /**
  * return Row by Id
  */
  function getEMById($module,$id)
  {
    $classListArr = getModelClass();
    $modelClass = $classListArr[$module];
    return $modelClass::where('id',$id)->first();
  }
}

if (!function_exists('calculateDiscount')) {
  /**
  * return calculated discount
  */
  function calculateDiscount($total,$discountAmt,$discountType)
  {
    $discount = 0;
    if($discountAmt>0){
      if($discountType=='fixed'){
        $discount = $discountAmt;
      }else{
        $discount = $total * ($discountAmt/100);
      }
    }
    return $discount;
  }
}



if (!function_exists('getMonthArr')) {
  /**
  * return status confirm msg array
  */
  function getMonthArr()
  {
    return [
      '01'=>'January',
      '02'=>'February',
      '03'=>'March',
      '04'=>'April',
      '05'=>'May',
      '06'=>'June',
      '07'=>'July ',
      '08'=>'August',
      '09'=>'September',
      '10'=>'October',
      '11'=>'November',
      '12'=>'December',
    ];
  }
}




if (!function_exists('getSect')) {
  /**
  * return status confirm msg array
  */
  function getSect()
  {
    return [
      1  =>  168,
      2  =>  169,
      3  =>  170,
      4  =>  171,
      5  =>  172,
      6  =>  173,
      7  =>  174,
      8  =>  175,
      9  =>  176,
      10  =>  177,
      11  =>  178,
      12  =>  179,
      13  =>  180,
      14  =>  181,
      15  =>  182,
      16  =>  183,
      17  =>  184,
      18  =>  185,
      19  =>  186,
      20  =>  187,
      21  =>  188,
      22  =>  189,
      23  =>  190,
      24  =>  191,
      25  =>  192,
      26  =>  193,
      27  =>  194,
      28  =>  195,
      29  =>  196,
      30  =>  197

    ];
  }
}

if(!function_exists('getPercentageAmount')){
  /**
  * return percentage from amount
  */
  function getPercentageAmount($amt, $total)
  {
    $percent=0;
    if($total>0){
      $count1 = $amt / $total;
      $count2 = $count1 * 100;
      $percent = number_format($count2, 0);
    }
    return $percent;
  }
}

if(!function_exists('validateEmailAddress')){
  /**
  * return if email is valid
  */
  function validateEmailAddress($email)
  {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return false;
    }
    return true;
  }
}

if(!function_exists('encryptData')){
  /**
  * return encrepted string
  */
  function encryptData($data, $key)
  {
    // dd(Config::get('app.cipher'));
    // $newEncrypter = new Encrypter($key, strtolower(Config::get('app.cipher')));
		// return $newEncrypter->encrypt($data);
    return Crypt::encryptString($data);
  }
}

if(!function_exists('deryptData')){
  /**
  * return decrypted string
  */
  function deryptData($data, $key)
  {
    // $newEncrypter = new Encrypter($key, strtolower(Config::get('app.cipher')));
		// return $newEncrypter->decrypt($data);
    return Crypt::decryptString($data);
  }
}

if(!function_exists('getSystemAllowedImageTypes')){
  /**
  * return System allowed image types
  */
  function getSystemAllowedImageTypes()
  {
    return ['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF'];
  }
}

if(!function_exists('getSystemAllowedFileTypes')){
  /**
  * return System allowed image types
  */
  function getSystemAllowedFileTypes()
  {
    return [
      'jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','DOC','docx','DOCX',
      'xls','XLS','xlsx','XLSX','pdf','PDF'
    ];
  }
}

if(!function_exists('cleanString')){
  /**
  * return decrypted string
  */
  function cleanString($str)
  {
    $str = str_replace("'","&#039;",$str);
    $str = str_replace('"',"&quot;",$str);
    return $str;
  }
}

if(!function_exists('curlRequest')){
  /**
  * return decrypted string
  */
  function curlRequest($dataString)
  {
    $paytabOptions = getPayTabsSettings();
    $dataString=json_encode($dataString);
    $headers = array ('Authorization: ' . $paytabOptions['serverKey'], 'Content-Type: application/json');
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, $paytabOptions['paymentRequest']);
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, $dataString);
    $result = curl_exec($ch);
    curl_close ($ch);
    return $result;
  }
}




if (!function_exists('columnchartdata')) {
  /**
  * return page sizes
  */
  function columnchartdata($payment,$month)
  {
    $query=Invoices::where('payment_status',$payment)->whereMonth('p_invoice_date','=',$month)->get();
    return count($query);
  }
}


/*******************************************************************************
Import Functions
*******************************************************************************/
if (!function_exists('getNewSectorId')) {
  /**
  * return new sector / category id
  */
  function getNewSectorId($oldId)
  {
    $newId = 0;
    $sectors_list_id = getSetting('sector_industry_list_id');
    $oldSectorRow = DB::connection('mysql_old')
    ->table('categories')->where('id',$oldId)->first();
    if($oldSectorRow!=null){
      $detailTableName = (new PredefinedListDetail)->getTable();
      $newSectorRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
      ->select((new PredefinedList)->getTable().'.id')
      ->where(['parent_id'=>$sectors_list_id,'lang'=>'en','title'=>trim($oldSectorRow->title)])
      ->first();
      if($newSectorRow==null){
        $newSectorRow = new PredefinedList;
        $newSectorRow->parent_id = $sectors_list_id;
        $newSectorRow->ltitle = ['en'=>trim($oldSectorRow->title)];
        $newSectorRow->descp = ['en'=>trim($oldSectorRow->title)];
        $newSectorRow->type = 'general';
        $newSectorRow->save();
      }
      $newId = $newSectorRow->id;
    }
    return $newId;
  }
}
if (!function_exists('getNewEmiratesId')) {
  /**
  * return new Emirates id
  */
  function getNewEmiratesId($oldId)
  {
    $newId = 0;
    $emirates_id = 49;
    if(in_array($oldId,['-','.']))$oldId='';
    if($oldId!=''){
      $detailTableName = (new PredefinedListDetail)->getTable();
      $newRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
      ->select((new PredefinedList)->getTable().'.id')
      ->where(['parent_id'=>$emirates_id,'lang'=>'en','title'=>trim($oldId)])
      ->first();
      if($newRow==null){
        $newRow = new PredefinedList;
        $newRow->parent_id = $emirates_id;
        $newRow->ltitle = ['en'=>trim($oldId)];
        $newRow->descp = ['en'=>trim($oldId)];
        $newRow->save();
      }
      $newId = $newRow->id;
    }
    return $newId;
  }
}
if (!function_exists('getNewPersonalTitleId')) {
  /**
  * return new Personal Title id
  */
  function getNewPersonalTitleId($oldId)
  {
    $newId = 0;
    $personal_title_list_id = 57;
    $detailTableName = (new PredefinedListDetail)->getTable();
    $newRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
    ->select((new PredefinedList)->getTable().'.id')
    ->where(['parent_id'=>$personal_title_list_id,'lang'=>'en','title'=>trim($oldId)])
    ->first();
    if($newRow==null){
      $newRow = new PredefinedList;
      $newRow->parent_id = $personal_title_list_id;
      $newRow->ltitle = ['en'=>trim($oldId)];
      $newRow->descp = ['en'=>trim($oldId)];
      $newRow->save();
    }
    $newId = $newRow->id;
    return $newId;
  }
}
if (!function_exists('getNewGenderId')) {
  /**
  * return new Gender id
  */
  function getNewGenderId($oldId)
  {
    $genders = [
      1=>67, //Male
      0=>68, //Female
    ];
    return isset($genders[$oldId]) ? $genders[$oldId] : 0;
  }
}
if (!function_exists('getNewCountryCodeId')) {
  /**
  * return new Country Code id
  */
  function getNewCountryCodeId($oldId)
  {
    $newId = 0;
    $country_list_id = getSetting('country_list_id');
    $oldRow = DB::connection('mysql_old')
    ->table('country')->where('id',$oldId)->first();
    if($oldRow!=null){
      $detailTableName = (new PredefinedListDetail)->getTable();
      $newRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
      ->select((new PredefinedList)->getTable().'.id')
      ->where(['parent_id'=>$country_list_id,'lang'=>'en','title'=>trim($oldRow->country_name)])
      ->first();
      if($newRow==null){
        $newRow = new PredefinedList;
        $newRow->parent_id = $country_list_id;
        $newRow->ltitle = ['en'=>trim($oldRow->country_name)];
        $newRow->descp = ['en'=>trim($oldRow->country_name)];
        $newRow->save();
      }
      $newId = $newRow->id;
    }
    return $newId;
  }
}
if (!function_exists('getNewHowDidYouHearUsId')) {
  /**
  * return new How you hear us id
  */
  function getNewHowDidYouHearUsId($oldId)
  {
    $newId = 0;
    $setting_title_list_id = 87;
    $oldRow = DB::connection('mysql_old')
    ->table('referral_sources')->where('id',$oldId)->first();
    if($oldRow!=null){
      $detailTableName = (new PredefinedListDetail)->getTable();
      $newRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
      ->select((new PredefinedList)->getTable().'.id')
      ->where(['parent_id'=>$setting_title_list_id,'lang'=>'en','title'=>trim($oldRow->title)])
      ->first();
      if($newRow==null){
        $newRow = new PredefinedList;
        $newRow->parent_id = $setting_title_list_id;
        $newRow->ltitle = ['en'=>trim($oldRow->title)];
        $newRow->descp = ['en'=>trim($oldRow->title)];
        $newRow->save();
      }
      $newId = $newRow->id;
    }
    return $newId;
  }
}
if (!function_exists('getNewMemberAccountTypeId')) {
  /**
  * return new account type id
  */
  function getNewMemberAccountTypeId($oldId)
  {
    $sysArr = [
      'member' => 'Member',
      'nominee' => 'Nominee',
      'alternate' => 'Alternate',
      'additional' => 'Additional Member',
      'named_associate' => 'Named Associate',
      'platinum associate' => 'Platinum Associate',
      'temporary membership' => 'Temporary Membership',
      'non-resident' => 'Non Resident',
    ];
    $newId = 0;
    $setting_title_list_id = getSetting('contactroles_list_id');
    $oldAccountType = isset($sysArr[$oldId]) ? $sysArr[$oldId] : 0;
    if($oldAccountType!='' && $oldAccountType!=0){
      $detailTableName = (new PredefinedListDetail)->getTable();
      $newRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
      ->select((new PredefinedList)->getTable().'.id')
      ->where(['parent_id'=>$setting_title_list_id,'lang'=>'en','title'=>trim($oldAccountType)])
      ->first();
      if($newRow==null){
        $newRow = new PredefinedList;
        $newRow->parent_id = $setting_title_list_id;
        $newRow->ltitle = ['en'=>trim($oldAccountType)];
        $newRow->descp = ['en'=>trim($oldAccountType)];
        $newRow->save();
      }
      $newId = $newRow->id;
    }
    return $newId;
  }
}
if (!function_exists('getNewMemberJobTitleId')) {
  /**
  * return new job title id
  */
  function getNewMemberJobTitleId($oldId)
  {
    $newId = 0;
    $setting_title_list_id = getSetting('jobroles_list_id');
    $oldJobTitle = $oldId;
    if(in_array($oldJobTitle,['-','.']))$oldJobTitle='';
    if($oldJobTitle!=''){
      $detailTableName = (new PredefinedListDetail)->getTable();
      $newRow = PredefinedList::join($detailTableName, $detailTableName.'.list_id', '=', (new PredefinedList)->getTable().'.id')
      ->select((new PredefinedList)->getTable().'.id')
      ->where(['parent_id'=>$setting_title_list_id,'lang'=>'en','title'=>trim($oldJobTitle)])
      ->first();
      if($newRow==null){
        $newRow = new PredefinedList;
        $newRow->parent_id = $setting_title_list_id;
        $newRow->ltitle = ['en'=>trim($oldJobTitle)];
        $newRow->descp = ['en'=>trim($oldJobTitle)];
        $newRow->save();
      }
      $newId = $newRow->id;
    }
    return $newId;
  }
}
if (!function_exists('getNewMemberShipTypeId')) {
  /**
  * return new Membership Type id
  */
  function getNewMemberShipTypeId($oldId)
  {
    $membershipTypesArr = [
      1=>2, //Business
      2=>4, //Individual
      4=>5, //Business Lite
      5=>3, //Business Advance
      6=>1, //Essential
    ];
    return isset($membershipTypesArr[$oldId]) ? $membershipTypesArr[$oldId] : 0;
  }
}
if (!function_exists('getNewPaymentMethodId')) {
  /**
  * return new payment method
  */
  function getNewPaymentMethodId($oldId)
  {
    $arr = [
      'credit_card'=>3,
      'credit'=>6,
      'bank_transfer'=>1,
      'online'=>4,
      'cash'=>2,
      'cheque'=>5,
    ];
    return isset($arr[$oldId]) ? $arr[$oldId] : 0;
  }
}
if (!function_exists('getNewPaymentStatusId')) {
  /**
  * return new payment status
  */
  function getNewPaymentStatusId($oldId)
  {
    $arr = [
      'paid'=>1,
      'unpaid'=>0,
      'partialy_paid'=>2,
      'cancelled'=>3,
      'refunded'=>4,
    ];
    return $arr[$oldId];
  }
}
if (!function_exists('getNewInvoiceTypeId')) {
  /**
  * return new invoice type
  */
  function getNewInvoiceTypeId($oldId)
  {
    $arr = [
      'member'=>1,
      'event'=>2,
      'renewal'=>3,
      ''=>3,
    ];
    return $arr[$oldId];
  }
}
if (!function_exists('getNewCurrencyId')) {
  /**
  * return new currency id
  */
  function getNewCurrencyId($oldId)
  {
    $arr = [
      'AED'=>1,
      'USD'=>2,
      'GBP'=>3,
    ];
    return $arr[$oldId];
  }
}
if (!function_exists('getFormatedDateTime')) {
  /**
  * return datetime from int to date
  */
  function getFormatedDateTime($datetime)
  {
    return date("Y-m-d H:i:s",$datetime);
  }
}
if (!function_exists('getDateOnlyFromTimestamp')) {
  /**
  * return date from datetime string
  */
  function getDateOnlyFromTimestamp($datetime)
  {
    if($datetime!=null){
      list($date,$time)=explode(" ",$datetime);
      if($date=='0000-00-00')$date=null;
      return $date;
    }
    return null;
  }
}
if (!function_exists('getNewInvoiceItemTypeId')) {
  /**
  * return new invoice item type id
  */
  function getNewInvoiceItemTypeId($oldId)
  {
    $arr = [
      'event'=>'event',
      'membership'=>'service',
      ''=>'service'
    ];
    return $arr[$oldId];
  }
}
if (!function_exists('getNewInvoiceItemId')) {
  /**
  * return new invoice item id
  */
  function getNewInvoiceItemId($oldTypeId,$category,$oldId)
  {
    $newId = 0;
    if($oldTypeId=='event'){
      $newId = $oldId;
    }else{
      if($category=='Membership Joining Fee'){
        $newId = getSetting('joining_fee_id');
      }else{
        $newId = getNewMemberShipTypeId($oldId);
      }
    }
    return $newId;
  }
}
if (!function_exists('importEventFees')) {
  /**
  * save new event fee
  */
  function importEventFees($eventId,$fieldId,$fee)
  {
    $eventFeeRow=EventFee::where(['event_id'=>$eventId,'guest_type_id'=>$fieldId])->first();
    if($eventFeeRow!=null){
      DB::table($eventFeeRow->getTable())
      ->where('event_id', $eventId)
      ->where('guest_type_id', $fieldId)
      ->update([
        'fee' => $fee,
      ]);
    }else{
      $eventFeeRow=new EventFee;
      $eventFeeRow->event_id=$eventId;
      $eventFeeRow->guest_type_id=$fieldId;
      $eventFeeRow->fee=$fee;
      $eventFeeRow->save();
    }
  }
}
if(!function_exists('getNewCategoryId')){
  /**
  * get new category id
  */
  function getNewCategoryId($oldId)
  {
    $arr=[40 => 34];
    return $arr[$oldId];
  }
}
if(!function_exists('getNewNewsCategoryId')){
  /**
  * get new new category id
  */
  function getNewNewsCategoryId($oldId)
  {
    $arr=[
      1=>1614,//Expo
      2=>1615,//Member
      3=>1616//Guest
    ];
    return isset($arr[$oldId]) ? $arr[$oldId] : 0;
  }
}
if(!function_exists('getNewUserId')){
  /**
  * get new user id
  */
  function getNewUserId($oldId)
  {
    $newId=0;
    $result = User::where('old_member_id',$oldId)->withTrashed()->first();
    if($result!=null){
      $newId=$result->id;
    }
    return $newId;
  }
}
if(!function_exists('getNewCompanyId')){
  /**
  * get new company id
  */
  function getNewCompanyId($oldId)
  {
    $newId=0;
    $result = Company::where('old_company_id',$oldId)->withTrashed()->first();
    if($result!=null){
      $newId=$result->id;
    }
    return $newId;
  }
}



if (!function_exists('getProspectInfo')) {
  /**
  * return user model by id
  */
  function getProspectInfo($id)
  {
    return Prospect::where('id',$id)->first();
  }
}




if (!function_exists('sendEmailInvoice')) {
  /**
  * return user model by id
  */
  function sendEmailInvoice($model,$emailattach)
  {


    $template_html='';
      $setting = Setting::where('config_name','invoice_template')->select('config_value')->first();
      if ($setting) {
          $template = Template::where('id',$setting->config_value)->select('email_body')->first();
      }


      if ($template) {

        $template_html= '<div style="width:850px; background-color:#f2f2f2;">'.$template->email_body.'</div>';
        //
        //  // if ($model->invoice_no==null) {
        //    $template_html =  str_replace( '{invoicetype}', 'PROFORMA INVOICE', $template_html);
        //    $template_html =  str_replace( '{invoice_date_type}', 'Performa Invoice Date', $template_html);
        //    $template_html =  str_replace( '{invoice_date}', formatDateTime($model->p_invoice_date), $template_html);
        //    if ($model->p_reference_number!=null) {
        //      $template_html =  str_replace( '{invoice_number_title}', 'Performa Invoice number', $template_html);
        //      $template_html =  str_replace( '{invoice_number}',$model->p_reference_number, $template_html);
        //    }else {
        //      $template_html =  str_replace( '{invoice_number_title}', '', $template_html);
        //      $template_html =  str_replace( '{invoice_number}','', $template_html);
        //    }
        //
           // $template_html =  str_replace( '{performa_date_title}', '', $template_html);
           // $template_html =  str_replace( '{performa_invoice_date}','', $template_html);
           // $template_html =  str_replace( '{performa_invoice_number_title}', '', $template_html);
           // $template_html =  str_replace( '{performa_invoice_number}','', $template_html);

          // }
         //   if ($model->invoice_no!=null) {
         //     $template_html =  str_replace( '{invoicetype}', 'INVOICE', $template_html);
         //     $template_html =  str_replace( '{invoice_date_type}', 'Invoice', $template_html);
         //     $template_html =  str_replace( '{invoice_date}', formatDateTime($model->invoice_date), $template_html);
         //     $template_html =  str_replace( '{invoice_number_title}', 'Invoice number', $template_html);
         //     $template_html =  str_replace( '{invoice_number}',$model->reference_number, $template_html);
         //
         //     $template_html =  str_replace( '{performa_date_title}', 'Proforma Invoice date:', $template_html);
         //     $template_html =  str_replace( '{performa_invoice_date}',$model->p_invoice_date, $template_html);
         //     $template_html =  str_replace( '{performa_invoice_number_title}', 'Proforma Invoice Number:', $template_html);
         //    $template_html =  str_replace( '{performa_invoice_number}',$model->p_reference_number, $template_html);
         // }

         // $template_html =  str_replace( '{client_name}',$model->bill_to_name, $template_html);
         // $template_html =  str_replace( '{client_company}',$model->bill_to_company_name, $template_html);
         // $template_html =  str_replace( '{client_address}','', $template_html);
         //
         // if ($model->bill_to_company_address) {
         //   $template_html =  str_replace( '{client_address}',$model->bill_to_company_address, $template_html);
         //
         // }
         //
         // $taxRows='';
         // $totalTax = 0;
         // $totalDiscount=0;
         // $subTotal=0;
         // $total=0;
         // $invoice_loop='';
         // $taxArr=getTaxArr();
         // if ($model <> null) {
         //   if ($model->items <> null) {
         //       foreach ($model->items as $key => $item) {
         //       $totalDiscount+=$item->discount_amount;
         //       $itemTotal = $item->total;
         //       $itemTaxableAmount = $itemTotal;
         //       if($item->tax_id>0){
         //         $itemTaxableAmount=$itemTotal+$item->tax_amount;
         //           $totalTax+=$item->tax_amount;
                 // $taxRows.='<tr class="tax-item">';
                 // $taxRows.=' <td class="border-0 font-weight-bolder text-left">'.__('common.tax').' ('.$taxArr[$item->tax_id].'%)</td>';
                 // $taxRows.=' <td class="border-0 font-weight-bolder text-right pr-0"><span class="cursign">'.$model->getInvCurrency().'</span>'.$item->tax_amount.'</td>';
                 // $taxRows.='</tr>';
               // }
               // $itemTaxableAmount-=$item->discount_amount;
               // $subTotal+= $item->total;
               // $total+=$itemTaxableAmount;
               //
               //
               // $invoice_loop.='<table style="font-size: 14px; font-weight: normal; width: 100%;" cellpadding="15px">';
               // $invoice_loop.='<tr>';
               // $invoice_loop.='<td>'.$item->title.'</td>';
               // $invoice_loop.='<td>';
               // $invoice_loop.='<span data-toggle="tooltip" title="'.$item->discount_comments.'">'.$item->discount.''.($item->discount_type=='percentage' ? '%' : '') .'</span>';
               // $invoice_loop.=' </td>';
               // $invoice_loop.='<td>';
               // $invoice_loop.=$item->tax_id>0 ? $taxArr[$item->tax_id].'%' : '';
               // $invoice_loop.='</td>';
               // $invoice_loop.='<td style="text-align: right;">'.formatInputInvPrice($model,$itemTaxableAmount).'</td>';
               // $invoice_loop.='</tr>';
               // $invoice_loop.='</table>';
               //   }
               //  }
               // }

               // $template_html =  str_replace( '{invoice_loop}',$invoice_loop, $template_html);
               // $template_html =  str_replace( '{sub_total}',formatInvPrice($model,$subTotal), $template_html);
               // $template_html =  str_replace( '{tax}',formatInvPrice($model,$totalTax), $template_html);
               // $template_html =  str_replace( '{discount}',formatInvPrice($model,$totalDiscount), $template_html);
               // $template_html =  str_replace( '{total}',formatInvPrice($model,$total), $template_html);
               // $template_html =  str_replace('{pay_online}','<a href="'.url('pay-tabs/'.$model->id).'">Pay Online</a><br>', $template_html);
               //
      }


    // $view = View::make('sales::invoices.view_customer_invoice', [ 'model' => $model, 'emailattach'=>$emailattach])->render();

    $view = \View::make('sales::invoices.view_customer_invoiceFend',['model'=>$model,'emailattach'=>'0']);
    $html_content = $view->render();

    // dd($html_content);
    PDF::SetTitle("View Invoice");
    // PDF::SetMargins(10, 30, 10);
    PDF::AddPage();
    PDF::writeHTML($html_content, true, false, true, false, '');
    ob_end_clean();
    // $file =  PDF::Output("invoice".'.pdf' ,'d');

    PDF::Output(public_path('invoices/').$model->id.'invoice.pdf', 'F');
    PDF::reset();
    // if ($template_html=='') {
    //     $template_html=$view;
    // }
    $data = [
      'view' => $template_html,
      'subject'=> 'BBG Invoice',
      'client_name'=> $model->bill_to_name,
      // 'email'=>$model->bill_to_email,
      'email'=>'mushabab10000@gmail.com',
      'id'=>$model->id,
    ];
    if ($model->bill_to_email) {
      \Mail::send([], [],
        function ($message) use ($data)
        {
          $message
          ->from('mushabab10000@gmail.com','British Business Group')
          ->to($data['email'],$data['client_name'])->subject($data['subject']);
          // ->setBody($data['view'], 'text/html');
          $message->attach(public_path('invoices/'.$data['id'].'invoice.pdf'));
        });
    }


  }
}



// Upload funtion by mushabab
if (!function_exists('uploadImageBy')) {
  /**
  * return storage dir path
  */
  function uploadImageInpublic($request,$fileInput,$destinationPath)
  {
    $image = $request->file($fileInput) ;
    $fileName = generateName() . '.' . $image->getClientOriginalExtension();
    $img = Image::make($image->getRealPath());
    $img->stream();
    Storage::disk('public')->put($destinationPath.'/'.$fileName, $img, 'public');
    return $fileName;
  }
}




// Upload funtion by mushabab
if (!function_exists('getCompanyViewTabs')) {
  /**
  * return storage dir path
  */
  function getCompanyViewTabs()
  {
    $company_view_tabs_array =[
      'company-detail-tab'=> [
        'font-aws'=>'far fa-building',
        'name'=>'Company',
        'grid'=>'0',
          ],
      'company-subscription-tab'=> [
        'font-aws'=>'fas fa-wallet',
        'name'=>'Subscriptions',
        'grid'=>'1',
      ],
      'company-overview-tab'=> [
        'font-aws'=>'fas fa-info',
        'name'=>'Overview',
        'grid'=>'0',
      ],
      'company-members-tab'=> [
        'font-aws'=>'far fa-chart-bar',
        'name'=>'Members',
        'grid'=>'1',
      ],
      'company-statements-tab'=> [
        'font-aws'=>'far fa-chart-bar',
        'name'=>'Statements',
        'grid'=>'0',
      ],
      'company-invoices-tab'=> [
        'font-aws'=>'fas fa-file-invoice-dollar',
        'name'=>'Invoices',
        'grid'=>'1',
      ],
      'company-payments-tab'=> [
        'font-aws'=>'fas fa-wallet',
        'name'=>'Payments',
        'grid'=>'1',
      ],
      'company-news-tab'=> [
        'font-aws'=>'fas fa-wallet',
        'name'=>'News',
        'grid'=>'1',
      ],
      'company-offers-tab'=> [
        'font-aws'=>'fas fa-wallet',
        'name'=>'Offers',
        'grid'=>'1',
      ],
      'company-actionlog-tab'=>[
        'font-aws'=>'fas fa-wallet',
        'name'=>'Action Log',
        'grid'=>'1',
      ],

    ];
    return $company_view_tabs_array;
  }
}













//

<?php

use Wisdom\Event\Models\EventFee;
use Wisdom\CustomeFields\Models\CustomField;
use Wisdom\CustomeFields\Models\CustomFieldModule;

// frontend function
if (!function_exists('getCustomFieldsAgaintsCompany')) {
  /**
  * return status confirm msg array
  */
  function getCustomFieldsAgaintsCompany($moduleType, $id)
  {
    $companyCustomFields = DB::table('custom_field_data')->where('module_type',$moduleType)->where('module_id',$id)
    ->leftJoin('custom_field', 'custom_field_data.input_field_id', '=', 'custom_field.id')
    ->select('custom_field_data.input_value','custom_field.title')
    ->pluck('input_value','title')->toArray();
   // dd($companyCustomFields);
    return $companyCustomFields;
  }
}

if (!function_exists('getCustomFieldsAgaintsContact')) {
  /**
  * return status confirm msg array
  */
  function getCustomFieldsAgaintsContact($moduleType, $id)
  {
    $contactCustomFields = $contactCustomFields = DB::table('custom_field_data')->where('module_type',$moduleType)->where('module_id',$id)
    ->leftJoin('custom_field', 'custom_field_data.input_field_id', '=', 'custom_field.id')
    ->select('custom_field_data.input_value','custom_field.title')
    ->pluck('input_value','title')->toArray();
    return $contactCustomFields;
  }
}

if (!function_exists('getAccountTypeArrFend')) {
  /**
  * return status confirm msg array
  */
  function getAccountTypeArrFend()
  {
    return [
      'additional' => 'Additional Member',
      'alternate' => 'Alternate Member',
    ];
  }
}



if (!function_exists('getEventFeesValue')) {
  /**
  * return status confirm msg array
  */
  function getEventFeesValue($event_id, $guest_type_id='')
  {
    $result =  EventFee::where(['event_id'=>$event_id,'guest_type_id'=>$guest_type_id])->first();
    if ($result) {
      return $result->fee;
    }
  }
}



if (!function_exists('getNewsHomePageLabelFend')) {
  function getNewsHomePageLabelFend()
  {
    return [
      '0' => '<span class="badge badge-danger" style="padding: 7px 10px;">No</span>',
      '1' => '<span class="badge badge-success" style="padding: 7px 10px;">Yes</span>',
    ];
  }
}



if (!function_exists('getNewsPublishLabelFend')) {
  function getNewsPublishLabelFend()
  {
    return [
      '0' => '<span class="badge badge-danger" style="padding: 7px 10px;">In-Active</span>',
      '1' => '<span class="badge badge-success" style="padding: 7px 10px;">Active</span>',
    ];
  }
}





if (!function_exists('onlyIntrestedCategories')) {
  function onlyIntrestedCategories($model)
  {
    $isSub = false;
    $card=false;
    $customFields = getCustomFieldOfIntrestedCategories($model->moduleTypeId);
    $inputFieldKey='input_field';
    if($isSub==true)$inputFieldKey=$model->moduleTypeId.'[input_field]';
    return view('customfields::components.custome-fields', [
      'showCard' => $card,
      'isSub' => $isSub,
      'inputFieldKey' => $inputFieldKey,
      'customeFields' => $customFields,
      'model'=>$model
    ]);
  }
}




if (!function_exists('getCustomFieldOfIntrestedCategories')) {
  function getCustomFieldOfIntrestedCategories($module)
  {
    return CustomField::where('status',1)
    ->where('short_name', 'intrested_ctaegories')
    ->where(function($query) use ($module){
      $query->whereIn('id',function($query) use ($module){
        if(is_array($module)){
          $query->from((new CustomFieldModule)->getTable())
          ->selectRaw('custom_field_id')
          ->whereIn('module_type', $module);
        }else{
          $query->from((new CustomFieldModule)->getTable())
          ->selectRaw('custom_field_id')
          ->where('module_type', $module);
        }
      });
    })
    ->orderBy('sort_order','asc')->get();
  }
}


if (!function_exists('getNonTextAreaColumnsSomething')) {
  /**
  * columns for grid view for a module
  */
  function getNonTextAreaColumnsSomething($type)
  {
    // dd($type);
    return CustomField::join((new CustomFieldModule)->getTable(),(new CustomFieldModule)->getTable().'.custom_field_id','=',(new CustomField)->getTable().'.id')

    ->where((new CustomField)->getTable().'.id', 1)

    ->where([
      (new CustomFieldModule)->getTable().'.module_type'=>$type,
      (new CustomField)->getTable().'.status'=>1
    ])
    ->whereNotIn('input_type',['textarea','profile'])
    ->orderBy('sort_order','asc')->get();
  }
}


if (!function_exists('getTextAreaColumnsSomething')) {
  function getTextAreaColumnsSomething($type)
  {
    return CustomField::join((new CustomFieldModule)->getTable(),(new CustomFieldModule)->getTable().'.custom_field_id','=',(new CustomField)->getTable().'.id')
    
    ->where((new CustomField)->getTable().'.id', 8)

    ->where([
      (new CustomFieldModule)->getTable().'.module_type'=>$type,
      (new CustomField)->getTable().'.status'=>1
    ])
    ->whereIn('input_type',['textarea','profile'])
    ->orderBy('sort_order','asc')->get();
  }
}






<?php
namespace App\Models;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;

class Contact extends User implements MustVerifyEmail
{
  use Blameable, SoftDeletes, Notifiable;
  public $prospect_id,$input_field,$manager_id,$emails,$phone_numbers,$social_media;
  public $comp_row_id,$comp_id,$comp_name,$comp_role_id,$comp_role,$comp_job_title_id,$comp_job_title;
  // public $passport_size_pic,$trade_link_proof;
  public $saveToList = true;
  protected $table = 'users';
  public $moduleTypeId = 'contact';

  /**
   * Get the companies for the blog post.
   */
  public function companies()
  {
    return $this->hasMany(ModuleCompany::class, 'company_id', 'module_id')->where('module_type','=', $this->moduleTypeId);
  }

  /**
   * Get primary company
   */
  public function primaryCompany()
  {

    $module_company = ModuleCompany::where('module_id',$this->id)->where('module_type',$this->moduleTypeId)->orderBy('id','asc')->first();
    return $module_company;
    // return $this->hasOne(ModuleCompany::class, 'company_id', 'module_id')->where('module_type','=', $this->moduleTypeId);
  }

  /**
   * Get the primary company
   */
  public function getMainCompanyIdAttribute()
  {
    $id = 0;
    $primaryCompany = $this->primaryCompany();
    if($primaryCompany!=null){
      $id = $primaryCompany->company_id;
    }
    return $id;
  }



  public function checkmembercontact($member, $contact)
  {
    // dd($member);
    return MemberContacts::where(['member_id' => $member, 'contact_id' => $contact])->first();
  }




}

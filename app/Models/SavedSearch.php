<?php
namespace App\Models;

use App;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;

class SavedSearch extends FullTables
{
  use Blameable, SoftDeletes;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'saved_search';

  /**
   * Get the saved input values
   */
  public function inputValues()
  {
      return $this->hasMany(SavedSearchInputValue::class);
  }
}

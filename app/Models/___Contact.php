<?php
namespace App\Models;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;

class Contact extends User
{
  use Blameable, SoftDeletes;
  public $prospect_id,$input_field,$manager_id,$emails,$phone_numbers,$social_media;
  public $comp_row_id,$comp_id,$comp_name,$comp_role_id,$comp_role,$comp_job_title_id,$comp_job_title;
  // public $passport_size_pic,$trade_link_proof;
  protected $table = 'users';
  public $moduleTypeId = 'contact';

  /**
   * Get the companies for the blog post.
   */
  public function companies()
  {
      return $this->hasMany(ModuleCompany::class, 'company_id', 'module_id')->where('module_type','=', $this->moduleTypeId);
  }

    public function checkmembercontact($member, $contact)
  {
    // dd($member);
    return MemberContacts::where(['member_id' => $member, 'contact_id' => $contact])->first();
  }
  
}

<?php
namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use Illuminate\Support\Facades\DB;

class Prospect extends FullTables
{
  use HasFactory, Blameable, SoftDeletes;
  public $input_field,$manager_id;
  public $saveToList = true;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'prospect';
  public $moduleTypeId = 'prospect';

  protected $fillable = [
    'reference_no',
  ];

  public function getFullname()
  {
    $name=$this->first_name;
    if($this->last_name!='')$name.=$this->last_name;
    return $name;
  }
}

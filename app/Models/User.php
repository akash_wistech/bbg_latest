<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, Blameable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
      return "{$this->name}";
    }

    /**
    * Get Address field value
    */
    public function getAddressAttribute()
    {
      $addressField = getCFByType('address',$this->moduleTypeId);
      $address = getInputFielSavedValueByCFId($this,$addressField->id);
      return $address;
    }

    public function getCityAttribute()
    {
      $str='';
      $value = getInputFielSavedValueByCFId($this,14);
      if($value!=null && $value!=''){
        $str = $value;
      }
      return $str;
    }

    /**
    * Get active subscription
    */
    public function activeSubscription()
    {
      return $this->belongsTo(Subscription::class,'active_subscription_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaManager extends Model
{
	use HasFactory;

	public function deleteRecursive($relations = []) {

		foreach ($relations as $relation) {
			if (is_array($this->$relation)) {
				foreach ($this->$relation as $relationItem) {
					$relationItem->deleteRecursive($relations);
				}
			} else {
				if (isset($this->$relation)) {
					$this->$relation->deleteRecursive($relations);
				}
			}
		}

		$this->delete();

		return true;
	}
}

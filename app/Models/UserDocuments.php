<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FullTables;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDocuments extends FullTables
{
  use Blameable, SoftDeletes;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'user_documents';
}

<?php
namespace App\Models;

use App;
use App\Models\ChildTables;

class ProspectToUser extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'prospect_to_user';
}

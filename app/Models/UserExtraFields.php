<?php
namespace App\Models;

use App\Models\ChildTables;

class UserExtraFields extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'user_extra_fields';
}

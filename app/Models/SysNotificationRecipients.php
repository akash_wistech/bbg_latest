<?php
namespace App\Models;

use App;
use App\Models\ChildTables;
use Illuminate\Support\Facades\DB;

class SysNotificationRecipients extends ChildTables
{
  protected $dates = ['read_at'];

  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'sys_notification_recipients';

  protected $fillable = [
    'notification_id',
    'user_id',
    'is_read',
  ];
}

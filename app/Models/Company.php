<?php
namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use Illuminate\Support\Facades\DB;
use App\Traits\Sluggable;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class Company extends FullTables
{
  use HasFactory, Blameable, Sluggable, SoftDeletes;
  public $input_field,$workflow_id,$workflow_stage_id,$manager_id;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'company';
  public $moduleTypeId = 'company';

  protected $fillable = [
    'reference_no',
    'parent_company_id',
    'title',
    'logo',
  ];

  public function getSluggableString()
  {
    return $this->title;
  }

  public function getFullTitleAttribute()
  {
    return "{$this->title}";
  }

  public function getActiveSubscriptionTitleAttribute()
  {
    $title='';
    if($this->activeSubscription!=null && $this->activeSubscription->membershipType && $this->activeSubscription->membershipType->service!=null){
      $title = $this->activeSubscription->membershipType->service->title;
    }
    return $title;
  }

  /**
  * Get active subscription
  */
  public function activeSubscription()
  {
    return $this->belongsTo(Subscription::class,'active_subscription_id');
  }

  /**
  * Get Address field value
  */
  public function getAddressAttribute()
  {
    $addressField = getCFByType('address',$this->moduleTypeId);
    $address = getInputFielSavedValueByCFId($this,$addressField->id);
    return $address;
  }

  /**
  * Get primary contact id
  */
  public function getFirstPrimaryContactIdAttribute()
  {
    $userId = 0;
    $companyPrimaryUsers = getCompanyPrimaryUsersListQuery($this->id,'contact');
    if($companyPrimaryUsers!=null){
      foreach($companyPrimaryUsers as $companyPrimaryUser){
        if($userId==0){
          $userId = $companyPrimaryUser['id'];
        }
      }
    }
    return $userId;
  }

  public function checkmembercompany($member_id, $company_id)
  {
    // dd($member_id);
    return MemberCompanies::where(['member_id' => $member_id, 'member_company' => $company_id])->first();
  }
}

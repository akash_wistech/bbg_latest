<?php
namespace App\Models;

use App;
use App\Models\ChildTables;

class SavedSearchInputValue extends ChildTables
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'saved_search_input_value';
}

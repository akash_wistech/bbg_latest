<?php
namespace App\Models;

use App;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use Illuminate\Support\Facades\DB;

class SysNotifications extends FullTables
{
  use Blameable, SoftDeletes;
  public $user_id;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'sys_notifications';

  protected $fillable = [
    'rec_type',
    'icon',
    'title',
    'url',
    'cls_name',
  ];
}

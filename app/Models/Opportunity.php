<?php
namespace App\Models;

use App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Blameable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FullTables;
use Illuminate\Support\Facades\DB;

class Opportunity extends FullTables
{
  use HasFactory, Blameable, SoftDeletes;
  public $input_field,$workflow_id,$workflow_stage_id,$manager_id;
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'opportunity';
  public $moduleTypeId = 'opportunity';

  protected $fillable = [
    'reference_no',
  ];

  /**
  * Get the service type
  */
  public function serviceType()
  {
    return $this->belongsTo(PredefinedList::class,'service_type');
  }

  /**
  * Get the service type
  */
  public function prospect()
  {
    return $this->belongsTo(Prospect::class,'module_id');
  }

  /**
  * Get the service type
  */
  public function serviceTypeTitle()
  {
    $pdItem = getPredefinedItem($this->service_type);
    return $pdItem->lang->title;
  }

  /**
  * Get the source info
  */
  public function sourceInfo()
  {
    $url = getModelUrlPre()[$this->module_type];
    $url.= '/view/'.$this->module_id;
    $html = getModulesListArr()[$this->module_type];
    $html.= '<a href="'.url($url).'" target="_blank">';
    $html.= getModuleNameByType($this->module_type,$this->module_id);
    $html.= '</a>';
    return $html;
  }

  /**
  * Generates calendar info html
  */
  public function getCalendarInfoHtml($col)
  {
    $html = '';
    $html.= '<div class="mb-3 col-sm-'.$col.'">';
    $html.= '  <strong>'.__('common.title').':</strong> '.$this->title;
    $html.= '</div>';
    $html.= '<div class="mb-3 col-sm-'.$col.'">';
    $html.= '  <strong>'.__('common.service_type').':</strong> '.$this->serviceTypeTitle();
    $html.= '</div>';
    $html.= '<div class="mb-3 col-sm-'.$col.'">';
    $html.= '  <strong>'.__('common.source').':</strong> '.$this->sourceInfo();
    $html.= '</div>';
    $html.= '<div class="mb-3 col-sm-'.$col.'">';
    $html.= '  <strong>'.__('common.expected_close_date').':</strong> '.formatDateTime($this->expected_close_date);
    $html.= '</div>';
    $html.= '<div class="mb-3 col-sm-'.$col.'">';
    $html.= '  <strong>'.__('common.quote_amount').':</strong> '.getCurrencyPre().$this->quote_amount;
    $html.= '</div>';
    if($this->module_type=='prospect'){
      $html.= '<div class="mb-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.email').':</strong> '.$this->prospect->email;
      $html.= '</div>';
      $html.= '<div class="mb-3 col-sm-'.$col.'">';
      $html.= '  <strong>'.__('common.phone').':</strong> '.$this->prospect->phone;
      $html.= '</div>';
    }
    $html.= '<div class="mb-3 col-sm-12 descp-truncate text-truncate">';
    // $html.= '  <strong>'.__('common.descp').':</strong> '.nl2br(truncateText($this->descp,150));
    $html.= '  <strong>'.__('common.descp').':</strong><br />'.nl2br($this->descp).'';
    $html.= '</div>';
    return $html;
  }
}

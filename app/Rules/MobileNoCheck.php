<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Prospect;
use App\Models\Opportunity;

class MobileNoCheck implements Rule
{
  public $request;
  public $model;
  public $customField;
  public $message='Number must be a number';
  /**
 * Create a new rule instance.
 *
 * @return void
 */
public function __construct($request,$model,$customField)
{
    //
    $this->request = $request;
    $this->model = $model;
    $this->customField = $customField;
}

/**
 * Determine if the validation rule passes.
 *
 * @param  string  $attribute
 * @param  mixed  $value
 * @return bool
 */
public function passes($attribute, $value)
{



    if ($this->customField['input_type']=='phoneinput') {
      if (!ctype_digit($this->model->input_field[$this->customField['id']])) {
          $this->message='Phone Number must be in Numeric';
          return false;
      }
      if (ctype_digit($this->model->input_field[$this->customField['id']])) {
        if (strlen($this->model->input_field[$this->customField['id']])!=12) {
          $this->message='Phone Number must contain 12 digits';
          return false;
        }
      }
    }
    if ($this->customField['input_type']=='faxinput') {
      if (!ctype_digit($this->model->input_field[$this->customField['id']])) {
          $this->message='Fax must be in Numeric and contain 12 digits';
          return false;
      }
      if (ctype_digit($this->model->input_field[$this->customField['id']])) {
        if (strlen($this->model->input_field[$this->customField['id']])!=12) {
          $this->message='Fax must be in Numeric and contain 12 digits';
          return false;
        }
      }
    }
    if ($this->customField['input_type']=='vat_number') {
      if (!ctype_digit($this->model->input_field[$this->customField['id']])) {
          $this->message='Vat Number must be in Numeric and contain 12 digits';
          return false;
      }
      if (ctype_digit($this->model->input_field[$this->customField['id']])) {
        if (strlen($this->model->input_field[$this->customField['id']])!=12) {
          $this->message='Vat Number must be in Numeric and contain 12 digits';
          return false;
        }
      }
    }
    if ($this->customField['input_type']=='postalcode') {
      if (preg_match('/[^A-Za-z0-9]/', $this->model->input_field[$this->customField['id']]) ) {
          $this->message='Postal Code must be in Alpabatic and Numeric.';
          return false;
      }
      // if (!preg_match('/[^A-Za-z0-9]/', $this->model->input_field[$this->customField['id']]) ) {
      //   if (strlen($this->model->input_field[$this->customField['id']])!=12) {
      //     $this->message='Postal Code must be in Alpabatic and Numeric with 12 digits';
      //     return false;
      //   }
      // }
    }
  return $this->request->input_field;
}

/**
 * Get the validation error message.
 *
 * @return string
 */
public function message()
{
    return $this->message;
}
}

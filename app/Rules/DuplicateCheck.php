<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Prospect;
use App\Models\Opportunity;

class DuplicateCheck implements Rule
{
  public $moduleTypeId;
  public $req;
  public $ignore;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($moduleTypeId,$req,$ignore)
    {
      $this->moduleTypeId = $moduleTypeId;
      $this->req = $req;
      $this->ignore = $ignore;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $passed = true;
      if($this->ignore==0){
        $modelClass = getModelClass()[$this->moduleTypeId];

        $query = DB::table((new $modelClass)->getTable())->whereNull('deleted_at');
        if($this->moduleTypeId=='prospect'){
          $query->where([
            (new $modelClass)->getTable().'.email' => $this->req->email,
            (new $modelClass)->getTable().'.phone' => $this->req->phone,
          ]);
        }
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          $passed = false;
        }
      }
      return $passed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Record already exists, Save again if you want to update old record';
    }
}

<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DuplicateUserCheckFrontend implements Rule
{
  public $moduleTypeId;
  public $req;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($moduleTypeId,$req)
    {
      $this->moduleTypeId = $moduleTypeId;
      $this->req = $req;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
      $passed = true;
      $duplicateRow = DuplicateUserCheckFrontend($this->moduleTypeId,$this->req);
      if($duplicateRow!=null){
        $passed = false;
      }
      return $passed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('common.duplicate_user_error');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\SavedSearch;
use App\Models\SavedSearchInputValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use Validator;

class AdvanceSearchController extends Controller
{
  /**
  * Save Search
  *
  * @return \Illuminate\Http\Response
  */
  public function saveSearch(Request $request,$module)
  {
    $saveSearch = new SavedSearch;
    $saveSearch->title = $request->search_title;
    $saveSearch->module_type = $module;
    if($saveSearch->save()){
      foreach($request->all() as $key=>$val){
        if($key!='search_title'){
          if(is_array($val)){
            foreach($val as $iKey=>$iVal){
              $inputName = $key.'['.$iKey.']';
              $this->saveItem($saveSearch,$inputName,$iVal);
            }
          }else{
            $this->saveItem($saveSearch,$key,$val);
          }

        }
      }
      return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.saved_search.saved')]]);
    }else{
      return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.saved_search.notsaved'),]]);
    }
  }

  public function saveItem($saveSearch,$key,$val)
  {
    if($val!=''){
      $ssChildRow = new SavedSearchInputValue;
      $ssChildRow->saved_search_id = $saveSearch->id;
      $ssChildRow->input_name = $key;
      $ssChildRow->input_value = $val;
      $ssChildRow->save();
    }
  }

  /**
  * Load Saved Searches
  *
  * @return \Illuminate\Http\Response
  */
  public function savedSearch(Request $request,$module)
  {
    $results = SavedSearch::where('created_by',Auth::user()->id)
    ->where('module_type',$module)
    ->paginate(5);
    // ->paginate(getSetting('grid_page_size'));
    return view('advance-search.index',compact('results','module','request'));
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\PredefinedList  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return SavedSearch::where('id', $id)->first();
  }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use App\Models\Company;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class CompanyController extends Controller
{
  public $folderName='company';
  public $controllerId='company';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    return view($this->folderName.'.index',compact('moduleTypeId','controllerId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    $a=[];
    if ($request->has('id')) {
    $id = (int)$request->input('id');
    $coms = getRowMultipleCompaniesQuery('contact',$id);
    foreach ($coms as $key => $value) {
          $a[]=$value['id'];
    }
    }
      $query = DB::table($this->newModel()->getTable())->where('is_dummy',0)->whereNull('deleted_at');
     if ($a) {
         $query->whereIn('id', $a);
     }
    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);
    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='title';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->title!='')$query->where('title','like','%'.$request->title.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }
    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        $thisData['cb_col']='';
        //Colors column
        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;
        $row = getEMById($moduleTypeId,$model->id);
        $logoSrc=$model->logo;
        if($logoSrc=='')$logoSrc = asset('assets/images/dummy-image.jpg');
        $thisData['logo']='<img src="'.$logoSrc.'" width="30" />';
        $fullTitle = getCompanyFullName($model->title,$model->parent_company_id);
        $thisData['title']=$fullTitle;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        if(function_exists('getSubscriptionInfo')){
          //no of subscriptions & start and end
          $subscriptionStr='';
          $subscriptionInfos = getSubscriptionInfos($model->id);
          if($subscriptionInfos->isNotEmpty()){
            $n=1;
            foreach($subscriptionInfos as $subscriptionInfo){

              $subscriptionStartDate = ($subscriptionInfo['start_date']!='' ? formatDate($subscriptionInfo['start_date']) : '');
              $subscriptionEndDate = ($subscriptionInfo['end_date']!='' ? formatDate($subscriptionInfo['end_date']) : '');

              $subscriptionHtml='';
              $subscriptionHtml.='<span class="label font-weight-bold label-lg label-light-success label-inline p-1 mb-2" style="height:auto;">'.$n.'. '.$subscriptionStartDate.($subscriptionStartDate!='' && $subscriptionEndDate!='' ? ' - ' : '').$subscriptionEndDate.'</span>';
              // $subscriptionStr.=($subscriptionStr!='' ? '<hr />' : '').$subscriptionHtml;
              $subscriptionStr.=$subscriptionHtml;
              $n++;
            }
          }else{
            $futureSubscription = getFutureSubscriptionInfo($model->id);
            if($futureSubscription!=null){
              $subscriptionStartDate = ($futureSubscription['start_date']!='' ? formatDate($futureSubscription['start_date']) : '');
              $subscriptionEndDate = ($futureSubscription['end_date']!='' ? formatDate($futureSubscription['end_date']) : '');

              $subscriptionHtml='';
              $subscriptionHtml.='<span class="label font-weight-bold label-lg label-light-primary label-inline p-1" style="height:auto;">'.$subscriptionStartDate.($subscriptionStartDate!='' && $subscriptionEndDate!='' ? ' - ' : '').$subscriptionEndDate.'</span>';
              $subscriptionStr.=$subscriptionHtml;
            }else{
              $lastExpired = getLastExpiredSubscriptionInfo($model->id);
              if($lastExpired!=null){
                $subscriptionStartDate = ($lastExpired['start_date']!='' ? formatDate($lastExpired['start_date']) : '');
                $subscriptionEndDate = ($lastExpired['end_date']!='' ? formatDate($lastExpired['end_date']) : '');

                $subscriptionHtml='';
                $subscriptionHtml.='<span class="label font-weight-bold label-lg label-light-warning label-inline p-1" style="height:auto;">'.$subscriptionStartDate.($subscriptionStartDate!='' && $subscriptionEndDate!='' ? ' - ' : '').$subscriptionEndDate.'</span>';
                $subscriptionStr.=$subscriptionHtml;
              }
            }
          }
          $thisData['subscriptions'] = $subscriptionStr;
        }
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy!=null ? $createdBy->fullname : '';
        $actBtns=[];
        if(checkActionAllowed('create','subscription') && function_exists('getCompanySubscriptionCount')){
          $subscriptionTooltip=__('nam-subscription::subscription.create');
          $subscriptionPopupHeading=__('nam-subscription::subscription.create_heading_for',['company'=>$fullTitle]);
          $subscriptionCount=getCompanySubscriptionCount($model->id);
          if($subscriptionCount>0){
            // $subscriptionTooltip=__('nam-subscription::subscription.renew');
            // $subscriptionPopupHeading=__('nam-subscription::subscription.renew_heading_for',['company'=>$fullTitle]);
          }
          $actBtns[]='
          <a href="'.url('/subscription/create',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit poplink" data-heading="'.$subscriptionPopupHeading.'" data-toggle="tooltip" title="'.$subscriptionTooltip.'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-layer"></i>
          </a>';
        }
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }

        // active and deative active account
        $actBtns[]='
        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon status-change" data-toggle="tooltip" title="'.(($model->status==1) ? 'Click to UnApprove' : 'Click to Approve').'" data-id="'.$model->id.'" data-url="'.url('/company/changestatus',['id'=>$model->id]).'">
        <i class="'.(($model->status==1) ? 'text-primary far fa-check-square' : 'text-danger fas fa-ban').'"></i>
        </a>';


        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = $this->newModel();
    $model->parent_company_id = 0;
    if($request->isMethod('post')){

      $validator = Validator::make($request->all(),[
        'title'=>'string|required',
        'parent_company_id'=>'integer|nullable',
        'email'=>'email|nullable',
        'vat_number' => 'required|digits:15',
        'visibility'=>'integer|nullable',
        'logo'=>'string|nullable',
        'trade_licence'=>'string|nullable',
        ]);
      if(!$validator->fails()){

        $model= $this->newModel();
        $model->parent_company_id = $request->parent_company_id ?? 0;
        $model->title = trim($request->title);
        $model->logo = trim($request->logo);
        $model->trade_licence = trim($request->trade_licence);
        $model->vat_document = trim($request->vat_document);
        $model->email = trim($request->email);
        $model->vat_number = trim($request->vat_number);
        $model->visibility = $request->visibility;
        if ($model->save()) {
          if($request->hasFile('image')){
            $image = $model->saveImage($request->file('image'), true);
          }
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          saveCustomField($model,$request,'form');
          saveTags($model,$request,'form');
          saveModuleManagerField($model,$request,'form');

          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.company.saved')]]);
          }else{
            return redirect($this->controllerId)->with('success', __('app.company.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.company.notsaved'),]]);
          }else{
            return redirect($this->controllerId)->with('error', __('app.company.notsaved'));
          }
        }
      }else{
        return redirect($this->controllerId.'/create')->withErrors($validator)->withInput($request->all());
      }
    }
    $controllerId = $this->controllerId;
    if($request->ajax()){
      $viewFile = $this->folderName.'._form';
    }else{
      $viewFile = $this->folderName.'.create';
    }
    return view($viewFile,compact('model','controllerId','request'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = $this->findModel($id);
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(),[
        'title'=>'string|required',
        'parent_company_id'=>'integer|nullable',
        'email'=>'email|nullable',
        'vat_number' => 'required|digits:15',
        'visibility'=>'integer|nullable',
        'logo'=>'string|nullable',
        'trade_licence'=>'string|nullable',
        ]);
      $model= $this->findModel($request->id);
      $model->title = trim($request->title);
      $model->logo = trim($request->logo);
      $model->trade_licence = trim($request->trade_licence);
      $model->vat_document = trim($request->vat_document);
      $model->parent_company_id = trim($request->parent_company_id) ?? 0;
      $model->email = trim($request->email);
      $model->vat_number = trim($request->vat_number);
      $model->visibility = $request->visibility;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->hasFile('image')){
          $image = $model->saveImage($request->file('image'), true);
        }
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request);
        saveTags($model,$request,'form');
        saveModuleManagerField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.company.saved'),]]);
        }else{
          return redirect($this->controllerId)->with('success', __('app.company.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.company.notsaved'),]]);
        }else{
          return redirect($this->controllerId)->with('error', __('app.company.notsaved'));
        }
      }
    }

    return view($this->folderName.'.update', [
      'model'=>$model,
      'request'=>$request
    ]);

  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Company;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Company::where('id', $id)->first();
  }

  public function show(Request $request, $id)
  {
    $MembType=null;
    $MemberShipTypeId='';
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $model=Company::where('id',$id)->first();
    $MemberShipTypeId=   Subscription::where('id',$model->active_subscription_id)->select('membership_type_id')->first();
    if($MemberShipTypeId!=null){
     $MembType = DB::table('membership_type')->where('id',$MemberShipTypeId)->select('description')->first();

    }

    return view('company.view',[
      'model'=>$model,
      'moduleTypeId'=>$moduleTypeId,
      // 'members'=>$members,
      // 'invoices'=>$invoices,
      // 'payments'=>$payments,
      // 'subscriptions'=>$subscriptions,
      'MembType'=>$MembType,
      'MemberShipTypeId'=>$MemberShipTypeId,
      'request'=>$request,

    ]);

  }

  public function changestatus(Request $request, $id)
  {
    $model = $this->findModel($id);
    if ($model->status==1) {
        $status =0;
    }
    if ($model->status==0) {
        $status=1;
    }
    $model->status=$status;
    if ($model->update()) {
      return new JsonResponse(['success'=>['heading'=>__('success'),'msg'=>__('Status has been changed')]]);
    }
    return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('Status is not working.')]]);
  }

}

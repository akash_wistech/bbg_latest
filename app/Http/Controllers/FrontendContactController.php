<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Company;
use App\Models\CustomFieldData;
use App\Models\MemberContacts;
use App\Rules\DuplicateUserCheck;

class FrontendContactController extends Controller
{
	public function newModel()
	{
		return new Contact;
	}

	public function findModel($id)
	{
		return Contact::where('id', $id)->first();
	}



	public function MemberDirectory(Request $request, $value='')
	{
		$search = '';
		if ($request->has('search')) {
			$search = $request['search'];
		}

		$members = Contact::where(['status'=>1])->whereNotIn('id', [Auth::user()->id])->orderBy('name','asc')->paginate(20);
		$model = $this->findModel(Auth::user()->id);

		return view('frontend.contact.members-directory',compact('members','model','search'));
	}

	public function MemberDetails($id='')
	{
		$member = $this->findModel($id);
		$model = $this->findModel(Auth::user()->id);
		return view('frontend.contact.member-details',compact('member','model'));
	}







	public function CompanyMembers($company_id='')
	{
		$showComapnyInfo = true;
		$company = Company::where(['id'=>$company_id])->first();
		$model = $this->findModel(Auth::user()->id);
		$members = getCompanyUsersListQuery($company_id,'contact');
// dd($members);

		return view('frontend.contact.company-members',compact('company','model','members','showComapnyInfo'));
	}









	public function AddMemberContact(Request $request)
	{
		if($request->isMethod('post')){


			if ($request->post('type') == "add") {

				$model = new MemberContacts;
				$model->member_id = Auth::user()->id;
				$model->contact_id = $request->post('id');

				if ($model->save()) {
					echo 1;
				} else {
					print_r($model->getErrors());
				}
			} 
			else {
				if (MemberContacts::where(['contact_id'=>$request->post('id'),'member_id'=>Auth::user()->id])->delete()) {
					echo 1;
				}
			}
		}
	}



}

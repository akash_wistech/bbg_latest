<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\SettingsForm;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;

class SystemController extends Controller
{
  /**
  * System Settings
  *
  * @return \Illuminate\Http\Response
  */
  public function settings(Request $request)
  {
    $model = new SettingsForm;

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'grid_page_size' => 'required|integer',
      ]);

      if ($model->save()) {
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('app.general.success'),'msg'=>__('app.setting.saved'),]]);
        }else{
          return redirect('system/settings')->with('success', __('app.settings.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('app.general.error'),'msg'=>__('app.setting.notsaved'),]]);
        }else{
          return redirect('system/settings')->with('error', __('app.settings.notsaved'));
        }
      }
    }

    $settings=Setting::get();
    foreach($settings as $record){
      $colName = $record['config_name'];
      if(in_array($colName,['show_in_committe_members'])){
        $model->$colName=explode(",",$record['config_value']);
      }else{
        $model->$colName=$record['config_value'];
      }
      
    }

    return view('system.setting',['model'=>$model]);
  }
}

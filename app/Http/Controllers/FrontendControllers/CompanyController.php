<?php
namespace App\Http\Controllers\FrontendControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use App\Models\Company;
use App\Models\Contact;
use App\Models\MemberCompanies;

class CompanyController extends Controller
{

  public function newModel()
  {
    return new Company;
  }

  public function findModel($id)
  {
    return Company::where('id', $id)->first();
  }

  public function CompanyDirectory(Request $request)
  {
    $model='';
    $search = '';

    if ($request->has('search')) {
      $search = $request['search'];
    }

    $companies = $this->newModel()::where(['visibility'=>1])
    ->when($search, function ($query, $search){
      return $query->where('title', 'like', '%' . $search . '%');
    })
    ->orderBy('title','asc')
    ->paginate(20);

    if (Auth::check()) {

      $model = Contact::where(['id'=>Auth::user()->id])->first();
    }
    
    return view('frontend.company.company-directory',compact('companies','model','search'));
  }

  public function AddMemberCompany(Request $request)
  {
    if($request->isMethod('post')){


      if ($request->post('type') == "add") {

        $model = new MemberCompanies;
        $model->member_id = Auth::user()->id;
        $model->member_company = $request->post('id');

        if ($model->save()) {
          echo 1;
        } else {
          print_r($model->getErrors());
        }
      } 
      else {
        if (MemberCompanies::where(['member_company'=>$request->post('id'),'member_id'=>Auth::user()->id])->delete()) {
          echo 1;
        }
      }
    }
  }

  

}

<?php

namespace App\Http\Controllers\FrontendControllers;

use Illuminate\Http\Request;
use Aws\Credentials\Credentials;
use Aws\Signature\SignatureV4;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Company;
use Auth;
use Illuminate\Support\Facades\DB;

use NaeemAwan\CMS\Models\ViewFromChair;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    // $this->middleware('auth');
  }

  public function frontendindex(Request $request)
  {

    $search='';
    if ($request->has('search')) {
      $search = $request->search;
      $search_events = DB::table('events')
      ->where('title','like','%'.$search.'%')
      ->get();
      // dd($search_events);

      $search_news = DB::table('news')
      ->where('title','like','%'.$search.'%')
      ->get();
      // dd($search_news);
      return view('frontend.site.search_main',compact('search_events','search_news','search'));

    }else{
     $chairmna_message = ViewFromChair::where(['display_on_home'=>1])->orderBy('date', 'asc')->first();
     return view('frontend.home',compact('chairmna_message'));
   }

 }

 public function SearchExperties(Request $request)
 {
  $search = $request->search_request_experties;

  $filterData = Contact::join((new ModuleCompany)->getTable(), (new ModuleCompany)->getTable().'.module_id','=',(new Contact)->getTable().'.id')
  ->join((new Company)->getTable(), (new ModuleCompany)->getTable().'.company_id','=',(new Company)->getTable().'.id')

  ->where((new ModuleCompany)->getTable().'.module_type','=','contact')
  ->where(function($query) use ($search){
    $query->orWhere(function($query) use($search){
      $query->where((new Contact)->getTable().'.name','LIKE','%'.$search.'%')
      ->orWhere((new Contact)->getTable().'.email','LIKE','%'.$search.'%');
    })
    ->orWhere(function($query) use($search){
      $query->where((new Company)->getTable().'.title','LIKE','%'.$search.'%')
      ->orWhere((new Company)->getTable().'.slug','LIKE','%'.$search.'%');
    });
  })
  ->paginate(30);

  // dd($filterData);

  return view('frontend.site.search_experties',compact('filterData','search'));
}

public function contactus(Request $request)
{
    // dd($request->body);
  if($request->isMethod('post')){
    $validatedData = $request->validate([
      'name' => 'required|string',
      'email' => 'required|string',
      'subject' => 'required|string',
      'body' => 'required|string',
      'g-recaptcha-response' => 'recaptcha',
    ]);

    $details = [
      'title' => 'Mail from Wisdom IT Solution',
      'name' => $request->name,
      'email' => $request->email,
      'subject' => $request->subject,
      'body' => $request->body,
    ];

    \Mail::to('meharusama0011@gmail.com')->send(new \App\Mail\Mailer($details));



    return view('frontend.site.contact-us');
  }

  return view('frontend.site.contact-us');
}


public function MemberDashboard(Request $request)
{
  $model = Contact::where(['id'=>Auth::user()->id])->first();

  return view('frontend.contact.member-dashboard',compact('model'));
}


public function MemberLogin(Request $request)
{
  return view('frontend.site.login');
}


}

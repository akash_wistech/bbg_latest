<?php

namespace App\Http\Controllers\FrontendControllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Prospect;
use App\Models\Company;
use App\Models\CustomFieldData;
use App\Models\MemberContacts;
use App\Rules\DuplicateUserCheck;
use Wisdom\Event\Models\EventSubscription;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\Payments;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use Wisdom\Sales\Models\ModuleInvoice;

class ContactController extends Controller
{
	public function newModel()
	{
		return new Contact;
	}

	public function findModel($id)
	{
		return Contact::where('id', $id)->first();
	}



	public function MemberDirectory(Request $request, $value='')
	{
		if (Auth::check()) {
			$search = '';
			if ($request->has('search')) {
				$search = $request['search'];
			}

			$members = Contact::where(['status'=>1])->whereNotIn('id', [Auth::user()->id])->orderBy('name','asc')->paginate(20);
		// dd($members);
			$model = $this->findModel(Auth::user()->id);
		// dd($model);

			return view('frontend.contact.members-directory',compact('members','model','search'));
		}else{
			return redirect('member/login');
		}


	}

	public function MemberDetails($id='')
	{
		$member = $this->findModel($id);
		// dd($member);
		$model = $this->findModel(Auth::user()->id);
		// dd($model);
		return view('frontend.contact.member-details',compact('member','model'));
	}







	public function CompanyMembers(Request $request)
	{
		$company_id = '';
		$members = '';
		$company = '';
		$showComapnyInfo = false;

		if ($request->has('id')) {
			$company_id = $request['id'];
			$members = getCompanyUsersListQuery($company_id,'contact');
			// dd($members);
			$company = Company::where(['id'=>$company_id])->first();
			$showComapnyInfo = true;

		}else{
			// dd("else");
		}
		$model = '';
		if (Auth::check()) {
			$model = $this->findModel(Auth::user()->id);
		}


		return view('frontend.contact.company-members',compact('company','model','members','showComapnyInfo'));
	}


	public function AddAdditionalMember(Request $request)
	{
		// dd($request->all());
		$model = $this->newModel();
		if($request->isMethod('post')){
			$validator = Validator::make($request->all(), [
				'full_name'=>'required|string',
				'phone'=>'required',
				'email'=>'required|email',
				'emails.*'=>'required|email',
			]);

			if(!$validator->fails()){
				// dd("here");
				$jobTitleId = $request->designation_id;
				$model = new Contact;
				$model->name = $request->full_name;
				$model->email = $request->email;
				$model->phone = $request->phone;
				$model->image = trim($request->image);
				$model->passport_copy = trim($request->passport_copy);
				$model->residence_visa = trim($request->residence_visa);
				$model->permission_group_id = getSetting('clients_permission_group');
				$model->status = 10;

				if ($model->save()) {
					if($request->input_field!=null){
						foreach($request->input_field as $key=>$val){
							$model->input_field[$key]=$val;
						}
					}

					saveMultipleEmailNumber($model,$request,'form');
					saveCustomField($model,$request,'form');


					$record = Contact::where(['id'=>Auth()->user()->id])->first();
					$companyId = $record->main_company_id;
					// dd($companyId);



					// $modelCompany=ModuleCompany::where(['module_type'=>$record->moduleTypeId,'module_id'=>$record->id,'company_id'=>$companyId])->first();
					// // dd($modelCompany);
					// if($modelCompany==null){
					// 	dd("here");
					$modelCompany=new ModuleCompany;
					$modelCompany->module_type=$record->moduleTypeId;
					$modelCompany->module_id=$model->id;
					$modelCompany->company_id=$companyId;
					$modelCompany->role_id=0;
					$modelCompany->job_title_id=$jobTitleId;
					$modelCompany->save();
					// }else{
					// 	dd("not nulll");
					// }


					if($request->ajax()){
						return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('nam-subscription::membership_type.saved'),]]);
					}else{
						return redirect('company/members/?id='.$companyId)->with('success', __('nam-subscription::membership_type.saved'));
					}
				}else{
					if($request->ajax()){
						return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.notsaved'),]]);
					}else{
						return redirect('member/add-additional')->withErrors($validator)->withInput($request->all())->with('error', __('nam-subscription::membership_type.notsaved'));
					}
				}
			}else{
				// dd("fails here");
				if($request->ajax()){
					return new JsonResponse(['errors' => $validator->errors(),'error'=>['heading'=>__('common.error'),'msg'=>__('nam-subscription::membership_type.notsaved'),]]);
				}else{
					return redirect('member/add-additional')->withErrors($validator)->withInput($request->all());
				}
			}

		}
		return view('frontend.contact.additional_form',['model'=>$model]);
	}






	public function AddMemberContact(Request $request)
	{
		if($request->isMethod('post')){


			if ($request->post('type') == "add") {

				$model = new MemberContacts;
				$model->member_id = Auth::user()->id;
				$model->contact_id = $request->post('id');

				if ($model->save()) {
					echo 1;
				} else {
					print_r($model->getErrors());
				}
			}
			else {
				if (MemberContacts::where(['contact_id'=>$request->post('id'),'member_id'=>Auth::user()->id])->delete()) {
					echo 1;
				}
			}
		}
	}

	public function actionMemberContacts()
	{
		$model = $this->findModel(Auth::user()->id);
		return view('frontend.contact.membership-contacts',compact('model'));
	}

	public function actionChangePassword()
	{
		$model= new Contact;
		return view('frontend.contact.change_password',compact('model'));
	}


	public function EditEmailSubscriptions(Request $request)
	{
		$model = $this->findModel(Auth::user()->id);

		return view('frontend.contact._edit_email_subscriptions',compact('model'));
	}

	public function EventsRegistered()
	{
		$model = $this->findModel(Auth::user()->id);
		// dd(Auth::user()->id);
		$events = EventSubscription::where(['user_id'=>Auth::user()->id,'user_type'=>'member'])
		// ->orWhere('registered_by',Auth::user()->id)
		->orderBy('id','desc')
		->paginate(10);
		// dd($events);
		return view ('frontend.contact.events_registered',compact('model','events'));
	}

	public function MemberInvoices(Request $request)
	{
		$model = $this->findModel(Auth::user()->id);
		$invoices = Invoices::where(['user_id'=>Auth::user()->id])
		->orWhereIn('id',function($query){
			$query->select('invoice_id')
		    ->from(with(new ModuleInvoice)->getTable())
		    ->where('module_type', (new EventSubscription)->moduleTypeId)
		    ->whereIn('module_id', function($query){
		    	$query->select('id')
			    ->from(with(new EventSubscription)->getTable())
			    ->where('registered_by', Auth::user()->id)
			    ->orWhere(function($query){
			    	$query->where('user_type','member')
			    	->where('user_id',Auth::user()->id);
			    });
		    });
		})
		->orderBy('id','desc')->paginate(15);
		// dd($invoices);
		return view ('frontend.contact.member_invoices',compact('model','invoices'));
	}

	public function MemberPayments(Request $request)
	{
		$model = $this->findModel(Auth::user()->id);
		$payments = Payments::where(['user_id'=>Auth::user()->id])->orderBy('id','desc')->paginate(15);
		// dd($payments);
		return view ('frontend.contact.member_payments',compact('model','payments'));
	}

	public function MemberStatements(Request $request)
	{
		$model = $this->findModel(Auth::user()->id);

		$invoices = Invoices::where('user_id',Auth::user()->id)->whereNotNull('invoice_date');
		$invoices->select([
			'id',
			'reference_number',
			DB::raw("'invoice' as type"),
			'invoice_date as date',
			'grand_total',
			'due_date'
		]);
		// dd($invoices);

		$payments = Payments::where('user_id',Auth::user()->id)
		->select([
			'id',
			DB::raw("'' as reference_number"),
			DB::raw("'payment' as type"),
			'payment_date as date',
			'amount_received',
			DB::raw("'' as due_date"),
		]);
		// dd($payments);

		$statements = $invoices->union($payments)->orderBy('date', 'asc');
		$statements = $statements->paginate(50);
		// dd($statements);

		$totalInvoiceAmount = $invoices->sum('grand_total');
		$totalPaymentAmount = $payments->sum('amount_received');
		// dd($totalInvoiceAmount.','.$totalPaymentAmount);

		$TotalBalence = $totalInvoiceAmount-$totalPaymentAmount;
		// dd($TotalBalence);


		return view ('frontend.contact.member_statement',compact('model','statements','TotalBalence'));
	}



	public function CompanyAddMember(Request $request)
	{

	}


	public function MemberSubAjax(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'first_name' => 'required|string',
			'last_name' => 'required|string',
			'email' => ['required', 'string', 'email', 'max:255', 'unique:prospect'],
		]);

    // Check validation failure
		if ($validator->fails()) {
			$text = "";
			$errors = $validator->errors()->getMessages();
			foreach ($errors as $key => $val) {
				// dd($val[0]);
				$text .= $val[0] . "<br/>";
			}
			return new JsonResponse(['type' => 'error', 'msg' => $val]);
		}

    // Check validation success
		if ($validator->passes()) {
			$model = new Prospect;
			$model->full_name = $request->post('first_name').' '.$request->post('last_name');
			$model->email = $request->post('email');
			if ($model->save()) {
				if(function_exists('subscribeUser')){
					subscribeUser($model);
				}
				return new JsonResponse(['type' => 'success', 'msg' => "You are now subscribed to receive email updates, special offers and announcements from the BBG!"]);
			}
		}
	}

	public function actionProfile(Request $request)
	{
		// dd($request->input_field);

		$model = $this->findModel(Auth::user()->id);
		$categories = getPredefinedListOptionsArr(getSetting('sector_industry_list_id'));

		$CustomFieldModel = $this->newModel();
		// dd($CustomFieldModel->moduleTypeId);

		if($request->isMethod('post')){

			$CustomFieldModel = $this->findModel(Auth::user()->id);

			if($request->input_field!=null){
				foreach($request->input_field as $key=>$val){
					$CustomFieldModel->input_field[$key]=$val;
				}
				$savedAll = saveCustomField($CustomFieldModel,$request,'form');
				if ($savedAll) {
					return view ('frontend.contact.myprofile',compact('model','categories','CustomFieldModel'));
				}
			}else{
				// dd("null");
			}

		}


		return view ('frontend.contact.myprofile',compact('model','categories','CustomFieldModel'));
	}

}

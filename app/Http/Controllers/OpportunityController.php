<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Opportunity;
use App\Models\CustomFieldData;
use NaeemAwan\StaffManager\Models\ModuleManager;
use Wisdom\CustomeFields\Rules\DuplicateCustomFieldCheck;

class OpportunityController extends Controller
{
  public $folderName='opportunity';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = (new Opportunity)->moduleTypeId;
    return view('opportunity.index',compact('moduleTypeId','request'));

    // return view('opportunity.kanban',compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = (new Opportunity)->moduleTypeId;
    $query = DB::table((new Opportunity)->getTable())
    // ->where('rec_type','o')
    ->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,(new Opportunity)->getTable(),'opportunity',$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->created_by!=null)$query->whereIn('created_by',$request->created_by);
    if($request->assign_to!=null){
      $query->whereIn('id',function($query) use ($moduleTypeId, $request){
        $query->select('module_id')
        ->from((new ModuleManager)->getTable())
        ->whereIn('staff_id',$request->assign_to)
        ->where('module_type',$moduleTypeId);
      });
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->title!='')$query->where('title','like','%'.$request->title.'%');
    if($request->service_type!='')$query->where('service_type','like','%'.$request->service_type.'%');
    if($request->source!='')$query->where('source','like','%'.$request->source.'%');
    if($request->expected_close_date!='')$query->where('expected_close_date','like','%'.$request->expected_close_date.'%');
    if($request->amount!='')$query->where('amount','like','%'.$request->amount.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();

    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        //Colors column
        $thisData['cb_col']='';

        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;

        $thisData['title']=$model->title;
        $pdItem = getPredefinedItem($model->service_type);
        $thisData['service_type']=$pdItem->lang->title;
        $thisData['module_type']=getModulesListArr()[$model->module_type].getModuleNameByType($model->module_type,$model->module_id);

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }

        $thisData['expected_close_date']=formatDate($model->expected_close_date);
        $thisData['amount']=getCurrencyPre().$model->quote_amount;
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/opportunity/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/opportunity/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/opportunity/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = new Opportunity;
    $di = $request->di ?? 0;
    if($request->isMethod('post')){
      // $module_type = $request->old('module_type');
      $validator = Validator::make($request->all(), [
        'title'=>'required|string',
        'service_type'=>'required|integer',
        'module_type'=>'required|string',
        'module_id'=>'required|integer',
        'expected_close_date'=>'string|nullable',
        'quote_amount'=>'numeric|nullable',
        'descp'=>'string|nullable',
        'duplicatecheck'=>new DuplicateCustomFieldCheck($model->moduleTypeId,$request->di,$request->module_type,$request->module_id)
      ]);
      if ($validator->fails()) {
        $errors = $validator->errors();
        if($errors->first('duplicatecheck')){
          return redirect('opportunity/create?di=1')->withErrors($validator)->withInput($request->all());
        }else{
          return redirect('opportunity/create')->withErrors($validator)->withInput($request->all());
        }
      }

      $diId = 0;
      if($di==1){
        $diId = getModuleDuplicateId($model->moduleTypeId,$request);
      }
      if($diId>0){
        $model = $this->findModel($diId);
      }else{
        $model= new Opportunity();
      }

      $model->rec_type = 'o';
      $model->title = $request->title;
      $model->service_type = $request->service_type;
      $model->module_type = $request->module_type;
      $model->module_id = $request->module_id;
      $model->module_keyword = $request->module_keyword;
      $model->expected_close_date = $request->expected_close_date;
      $model->quote_amount = $request->quote_amount;
      $model->descp = $request->descp;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->save()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request,'form');
        saveModuleManagerField($model,$request,'form');
        saveWorlflowField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.opportunity.saved')]]);
        }else{
          return redirect('opportunity')->with('success', __('app.opportunity.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.opportunity.notsaved'),]]);
        }else{
          return redirect('opportunity')->with('error', __('app.opportunity.notsaved'));
        }
      }
    }
    return view('opportunity.create',compact('model','di'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function createFor(Request $request,$module,$id)
  {
    $model = new Opportunity;
    $di = $request->di ?? 0;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required|string',
        'service_type'=>'required|integer',
        'expected_close_date'=>'string|nullable',
        'quote_amount'=>'numeric|nullable',
        'descp'=>'string|nullable',
        'duplicatecheck'=>new DuplicateCustomFieldCheck($model->moduleTypeId,$request->di,$module,$id)
      ]);
      if ($validator->fails()) {
        $errors = $validator->errors();

        if($errors->first('duplicatecheck')){
          return redirect('opportunity/create-for/'.$module.'/'.$id.'?di=1')->withErrors($validator)->withInput($request->all());
        }else{
          return redirect('opportunity/create-for/'.$module.'/'.$id.'')->withErrors($validator)->withInput($request->all());
        }
      }

      $diId = 0;
      if($di==1){
        $diId = getModuleDuplicateId($model->moduleTypeId,$request,$module,$id);
      }

      if($diId>0){
        $model = $this->findModel($diId);
      }else{
        $model= new Opportunity();
      }
      $subModelName='';
      $subModel = (getModelClass()[$module])::where('id',$id)->first();
      if($module=='prospect'){
        $subModelName = $subModel->full_name.($subModel->company_name!='' ? ' - '.$subModel->company_name : '');
      }
      $model->title = $request->title;
      $model->service_type = $request->service_type;
      $model->module_type = $module;
      $model->module_id = $id;
      $model->module_keyword = $subModelName;
      $model->expected_close_date = $request->expected_close_date;
      $model->quote_amount = $request->quote_amount;
      $model->descp = $request->descp;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->save()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request,'form');
        saveModuleManagerField($model,$request,'form');
        saveWorlflowField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.opportunity.saved')]]);
        }else{
          return redirect('opportunity')->with('success', __('app.opportunity.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.opportunity.notsaved'),]]);
        }else{
          return redirect('opportunity')->with('error', __('app.opportunity.notsaved'));
        }
      }
    }
    return view('opportunity._pop_form',compact('model','di'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Opportunity  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = Opportunity::where('id', $id)->first();

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required',
        'descp'=>'nullable|string',
      ]);

      $model= Opportunity::find($request->id);
      $model->title = $request->title;
      $model->service_type = $request->service_type;
      $model->module_type = $request->module_type;
      $model->module_id = $request->module_id;
      $model->module_keyword = $request->module_keyword;
      $model->expected_close_date = $request->expected_close_date;
      $model->quote_amount = $request->quote_amount;
      $model->descp = $request->descp;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request);
        saveModuleManagerField($model,$request);
        saveWorlflowField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.opportunity.saved'),]]);
        }else{
          return redirect('opportunity')->with('success', __('app.opportunity.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.opportunity.notsaved'),]]);
        }else{
          return redirect('opportunity')->with('error', __('app.opportunity.notsaved'));
        }
      }
    }

    return view('opportunity.update', [
      'model'=>$model,
    ]);

  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Opportunity  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Opportunity::where('id', $id)->first();
  }
}

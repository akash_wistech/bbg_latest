<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AwsSesController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    // $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index(Request $request)
  {
    // Log::info($request->all());
    $postBody = file_get_contents('php://input');
    Log::info($postBody);
    Log::info('------------------------------------------------------------------------------');
    // JSON decode the body to an array of message data
    $message = json_decode($postBody, true);
    Log::info($message);
    Log::info('------------------------------------------------------------------------------');
    if ($message) {
        // Do something with the data
        Log::info($message['Message']);
        // echo $message['Message'];
    }

  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    * detail view of model
    *
    * @return \Illuminate\Http\Response
    */
    public function show(Request $request,$id)
    {
      $model = $this->findModel($id);
      return view($this->folderName.'.view', compact('model','request'));
    }

    /**
    * Update status of model
    *
    * @return \Illuminate\Http\Response
    */
    public function status(Request $request, $id)
    {
      $model = $this->findModel($id);
      if($model->status==0){
        $status=1;
      }else{
        $status=0;
      }
      DB::table($model->getTable())
      ->where('id', $model->id)
      ->update(['status' => $status]);
      return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.statusUpdated'),]]);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
      // dd("here");
      $model = $this->findModel($id);
      $model->delete();
      DB::table($model->getTable())
      ->where('id', $id)
      ->update(['deleted_by' => Auth::user()->id]);
      return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('common.deleted'),]]);
    }
  }

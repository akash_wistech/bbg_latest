<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ChartsForm;

class ReportController extends Controller
{
  /**
  * Reports
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request,$module='prospect')
  {
    $model = new ChartsForm;
    if($request->input('created_at'))$model->created_at=$request->input('created_at');
    if($request->input('created_by'))$model->created_by=$request->input('created_by');
    return view('report.charts',['model'=>$model,'module'=>$module,'request'=>$request]);
  }
}

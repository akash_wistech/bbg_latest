<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\SettingsForm;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use App\Models\Company;
use NaeemAwan\MultipleEmailPhone\Models\ModuleEmail;
use NaeemAwan\MultipleEmailPhone\Models\ModuleNumber;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class SuggestionController extends Controller
{
  /**
  * Module Lookup
  *
  * @return \Illuminate\Http\Response
  */
  public function moduleLookUp(Request $request,$module)
  {


    $customFldModuleArr=['prospect'];
    $output_arrays=[];
    $moduleClassName = getModelClass()[$module];
    dd($moduleClassName);
    $query = DB::table((new $moduleClassName)->getTable());
    if($request->input('query')){
      $keyword=$request->input('query');
      if($module=='prospect'){
        $query->where('full_name','like','%'.$keyword.'%')
        ->orWhere('company_name','like','%'.$keyword.'%');
      }
      if($module=='contact'){
        $query->where('user_type',0)
        ->where(function($query) use ($keyword){
          $query->where('name','like','%'.$keyword.'%')
          ->orWhereIn('id', function($query) use ($keyword){
              $query->select('module_id')
              ->from(with(new ModuleCompany)->getTable())
              ->whereIn('company_id', function($query) use ($keyword){
                  $query->select('id')
                  ->from(with(new Company)->getTable())
                  ->where('title','like','%'.$keyword.'%');
              });
          });
        });
      }
      if($module=='company'){
        $query->where('title','like','%'.$keyword.'%');
      }
    }
    $results = $query->limit(20)->get();

    if($results!=null){
      foreach($results as $result){
        if($module=='prospect'){
          $title = $result->full_name.($result->company_name!='' ? ' - '.$result->company_name.'' : '');
        }
        if($module=='contact'){
          $title = $result->name;
          if(function_exists('getRowMultipleCompaniesQuery')){
            $companyInfo = '';
            $contactCompanies = getRowMultipleCompaniesQuery($module,$result->id);
            if($contactCompanies!=null){
              foreach($contactCompanies as $contactCompany){
                if($companyInfo!='')$companyInfo.=' & ';
                $companyInfo.= $contactCompany->title;
              }
            }
            if($companyInfo!='')$title.=' ('.$companyInfo.')';
          }
        }
        if($module=='company'){
          $title = $result->title;
        }
        $output_arrays[] = [
          'data'=>$result->id,
          'value'=>trim($title),
          'full_value'=>trim($title),
        ];
      }
    }
    return new JsonResponse(['suggestions'=>$output_arrays]);
  }

  /**
  * Company Lookup
  *
  * @return \Illuminate\Http\Response
  */
  public function companyLookUp(Request $request)
  {
    $output_arrays=[];
    $query = DB::table((new Company)->getTable());
    $query->select(['id','title','parent_company_id']);
    if($request->input('query')){
      $keyword=$request->input('query');
      $query->where('title','like','%'.$keyword.'%');
    }
    $results = $query->limit(10)->get();
    if($results!=null){
      foreach($results as $result){
        $fullTitle = getCompanyFullName($result->title,$result->parent_company_id);
        $output_arrays[] = [
          'data'=>$result->id,
          'value'=>trim($result->title),
          'full_value'=>trim($fullTitle),
        ];
      }
    }
    return new JsonResponse(['suggestions'=>$output_arrays]);
  }

  /**
  * Suggestions
  *
  * @return \Illuminate\Http\Response
  */
  public function generalSearch(Request $request,$keyword)
  {
    $kwquery='';
    if($request->input('query')){
      $kwquery=$request->input('query');
    }
    $query = generateSearchQuery($keyword,$kwquery);

    $results = $query->limit(20)->get();

    if($results!=null){
      foreach($results as $result){
        $title = $result->full_name.($result->company_name!='' ? ' - '.$result->company_name.'' : '');
        $output_arrays[] = [
          'data'=>$result->id,
          'value'=>trim($title),
        ];
      }
    }
    return new JsonResponse(['suggestions'=>$output_arrays]);
  }






  /**
  * search all the job roles
  * @return mixed
  */
  public function actionJobRoles($query=null)
  {
    $this->checkLogin();
    $listId=Yii::$app->appHelperFunctions->getSetting('role_list_id');
    header('Content-type: application/json');
    $output_arrays=[];
    $results=PredefinedList::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['parent'=>$listId,'status'=>1],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search all the job title
  * @return mixed
  */
  public function actionJobTitles($query=null)
  {
    $this->checkLogin();
    $listId=Yii::$app->appHelperFunctions->getSetting('job_title_list_id');
    header('Content-type: application/json');
    $output_arrays=[];
    $results=PredefinedList::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['parent'=>$listId,'status'=>1],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * check module duplication
  * @return mixed
  */
  public function moduleDuplicationCheck(Request $request,$module)
  {
    //Check Main Row
    $modelClass = getModelClass()[$module];
    $rowId = $request->id;
    $crequest['name']=$request->input('name');
    $crequest['email']=$request->input('email');
    $crequest['nemail']=$request->input('nemail');
    $crequest['phone']=$request->input('phone');
    $crequest['nnumbers']=$request->input('nnumbers');
    $crequest['companies']=$request->input('companies');

    if($request->input('emails')!=null && count($request->input('emails'))>0){
      foreach($request->input('emails') as $key=>$val){
        //Check in main row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->where((new $modelClass)->getTable().'.email', $val);
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          $recDetails = getModuleRowDetails($crequest,$duplicateRow,$module);
          return new JsonResponse([
            'error'=>[
              'heading' => __('common.error'),
              'msg' => '<div style="line-height:25px;;">'.__('common.duplication_email_error_msg_1',['email'=>$val]).'<br />'.__('common.duplication_email_error_msg_2',['details'=>$recDetails]).'</div>',
              'duplicate_id'=>$duplicateRow->id,
            ]
          ]);
        }

        //Check in multi row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->whereIn((new $modelClass)->getTable().'.id', function($query) use ($module,$val){
          $query->select('module_id')
          ->from(with(new ModuleEmail)->getTable())
          ->where('module_type', $module)
          ->where('email', $val);
        });
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          $recDetails = getModuleRowDetails($crequest,$duplicateRow,$module);
          return new JsonResponse([
            'error'=>[
              'heading' => __('common.error'),
              'msg' => '<div style="line-height:25px;;">'.__('common.duplication_email_error_msg_1',['email'=>$val]).'<br />'.__('common.duplication_email_error_msg_2',['details'=>$recDetails]).'</div>',
              'duplicate_id'=>$duplicateRow->id,
            ]
          ]);
        }
      }
    }
    if($request->input('numbers')!=null && count($request->input('numbers'))>0){
      foreach($request->input('numbers') as $key=>$val){
        //Check in main row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->where((new $modelClass)->getTable().'.phone', $val);
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          $recDetails = getModuleRowDetails($crequest,$duplicateRow,$module);
          return new JsonResponse([
            'error'=>[
              'heading' => __('common.error'),
              'msg' => '<div style="line-height:25px;;">'.__('common.duplication_number_error_msg_1',['number'=>$val]).'<br />'.__('common.duplication_number_error_msg_2',['details'=>$recDetails]).'</div>',
              'duplicate_id'=>$duplicateRow->id,
            ]
          ]);
        }

        //Check in multi row
        $query = DB::table((new $modelClass)->getTable());
        if($rowId!='')$query->where('id','!=',$rowId);
        $query->whereIn((new $modelClass)->getTable().'.id', function($query) use ($module,$val){
          $query->select('module_id')
          ->from(with(new ModuleNumber)->getTable())
          ->where('module_type', $module)
          ->where('phone', $val);
        });
        $duplicateRow = $query->first();
        if($duplicateRow!=null){
          $recDetails = getModuleRowDetails($crequest,$duplicateRow,$module);
          return new JsonResponse([
            'error'=>[
              'heading' => __('common.error'),
              'msg' => '<div style="line-height:25px;;">'.__('common.duplication_number_error_msg_1',['number'=>$val]).'<br />'.__('common.duplication_number_error_msg_2',['details'=>$recDetails]).'</div>',
              'duplicate_id'=>$duplicateRow->id,
            ]
          ]);
        }
      }
    }
  }



  public function moduleLookUpCompany(Request $request){

        $output_arrays=[];
        $moduleClassName = getModelClass()['company'];
        $query = DB::table((new $moduleClassName)->getTable());
        if($request->input('query')){
          $keyword=$request->input('query');
            $query->where('title','like','%'.$keyword.'%');
        }

        $query->whereIn('id',function($query){
          $query->select('company_id')->from(with(new Subscription)->getTable())
          ->whereDate('end_date', '>=', date("Y-m-d"));
        });




        $results = $query->limit(20)->get();

        if($results!=null){
          foreach($results as $result){
              $title = $result->title;
            $output_arrays[] = [
              'data'=>$result->id,
              'value'=>trim($title),
              'full_value'=>trim($title),
            ];
          }
        }
        return new JsonResponse(['suggestions'=>$output_arrays]);


  }




}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Aws\Credentials\Credentials;
use Aws\Signature\SignatureV4;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;
use Wisdom\Sales\Models\Payments;
use Wisdom\Event\Models\Event;
use NaeemAwan\ModuleSubscription\Models\MembershipType;
use NaeemAwan\ModuleSubscription\Models\Subscription;
use Auth;


class HomeController extends Controller
{
  public $PaymentByMonth;
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    if(Auth::user()->user_type==0){
      return redirect('/frontend');
    }
    $dashboardStats = getDashboardStats();

    //pie chart
    // $series=[];
    // $labels=[];
    // $groups=MembershipType::where('status',1)->get();
    // $total=Subscription::get();
    //
    // $countcomp=count($total);
    //     foreach ($groups as $key => $value) {
    //       $companies=Subscription::where('membership_type_id',$value->id)->get();
    //       $member_count=(count($companies)/$countcomp)*100;
    //       $series[$key]=(int)$member_count;
    //        $labels[$key]=$value->service->title.' - '.count($companies);
    //     }
  $subscription=Subscription::leftJoin('membership_type', 'subscription.membership_type_id', '=', 'membership_type.id')
  ->leftJoin('predefined_list_detail', 'membership_type.service_id', '=', 'predefined_list_detail.list_id')
  ->select( \DB::raw('count(*) as total'),'predefined_list_detail.title','subscription.membership_type_id')
  ->where('membership_type.status',1)->groupBy('subscription.membership_type_id')->get();
  $labels= $subscription->pluck('title')->toArray();
  $series= $subscription->pluck('total')->toArray();
  $activeSubscription = array_sum($series);

    //column chart for invoices
        $barData=[];
       $a = [];
         $b =  [];
         $c=  [];
         $textxaxis="Created per month";
         $aname= "Paid";
         $bname="Unpaid";
         $cname="Partially Paid";
         $months=['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'];

    foreach ($months as $key => $value) {
           $a[].=columnchartdata(1,$key+1);
            $b[].=columnchartdata(0,$key+1);
             $c[].=columnchartdata(2,$key+1);
         }

         $a = array_map('intval', $a);
         $b = array_map('intval', $b);
         $c = array_map('intval', $c);
         $barData=array(array("name"=>$aname,"data"=>$a),array("name"=>$bname,"data"=>$b),array("name"=>$cname,"data"=>$c));


        //column chart for event types
        $EventTypesPredefinedlistID= 1617;
        $Event_Types = getPredefinedListItemsArr($EventTypesPredefinedlistID);
        foreach ($Event_Types as $key => $value) {
          $eventTypeID[] = $key;
          $types[] = $value;
        }
        $Events = Event::whereIn('type',$eventTypeID)
        ->select( \DB::raw('count(*) as total'))
        ->groupBy('type')->get()->pluck('total')->toArray();
        $data=array(array("name"=>"created","data"=>$Events));






// Subscription Payments
$payments=[];
$payments=array(array("name"=>"Total Amount","data"=>getPaymentsByYear()['2021']));
$pay_months=['Dec','Nov', 'Oct', 'Sep', 'Aug', 'Jul', 'Jun', 'May', 'Apr', 'Mar','Feb','Jan'];

// Subscription Payments
$eventpayments=[];
$eventpayments=array(array("name"=>"Total Amount","data"=>getPaymentsByYear([2])['2021']));

// dd($eventpayments);

// yaerly Payments
    $yearly_payments=[];
    $years=['2019','2020','2021','2022','2023'];
    foreach ($years as $key => $value) {
      $payment=Payments::whereYear('payment_date', '=', $value)->get();
      $amount=0;
      foreach ($payment as $key => $value) {
        $amount=$amount+$value->amount_received;
      }
      $yearly_payments[]=(int)$amount;
    }
    $yearly_payments=array(array("name"=>"Total Amount","data"=>$yearly_payments));


    return view('home',compact('dashboardStats','series','labels','barData','textxaxis','months','types','data','payments','pay_months','yearly_payments','years','activeSubscription','eventpayments'));
  }


  public function yearwisepayments($year){
    $payments=array(array("name"=>"Total Amount","data"=>getPaymentsByYear()[$year]));
    return response()->json(['payments'=>$payments, 'year'=>'year']);
  }


public function eventyearwisepayments($year){
  $eventpayments=array(array("name"=>"Total Amount","data"=>getPaymentsByYear([2])[$year]));
  return response()->json(['payments'=>$eventpayments, 'year'=>'year']);
}


  public function practice(){
    return view('practice.index');
  }
















  // function signRequest(ServerRequestInterface $request, string $accessKeyId, string $secretAccessKey): ServerRequestInterface
  // {
  //     $signature = new SignatureV4('execute-api', 'af-south-1');
  //     $credentials = new Credentials($accessKeyId, $secretAccessKey);
  //     $request = $signature->signRequest($request, $credentials);
  //     return $request;
  // }
  //
  // function awsTest(ServerRequestInterface $request)
  // {
  //   $signedReq = $this->signRequest($request,'AKIA55D5DNTBPQ25INCH','u4RtIMPYLKrAEM5WGvfHVhKm68VIHLoPA8r7WirG');
  //   echo '<pre>';print_r($signedReq);echo '</pre>';
  //   die();
  //   $curl = curl_init();
  //
  //   curl_setopt_array($curl, array(
  //   CURLOPT_URL => 'https://api.shiplogic.com/rates',
  //   CURLOPT_RETURNTRANSFER => true,
  //   CURLOPT_ENCODING => '',
  //   CURLOPT_MAXREDIRS => 10,
  //   CURLOPT_TIMEOUT => 0,
  //   CURLOPT_FOLLOWLOCATION => true,
  //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  //   CURLOPT_CUSTOMREQUEST => 'POST',
  //   CURLOPT_POSTFIELDS =>'{
  //     "collection_address": {
  //         "company": "uAfrica.com",
  //         "street_address": "116 Lois Avenue",
  //         "local_area": "Newlands, Pretoria",
  //         "city": "City of Tshwane Metropolitan Municipality",
  //         "zone": "Gauteng",
  //         "country": "ZA",
  //         "code": "0181",
  //         "lat": -25.7863272,
  //         "lng": 28.277583
  //     },
  //     "delivery_address": {
  //         "company": "Bambolini Nursery School",
  //         "street_address": "Midas Avenue",
  //         "local_area": "Olympus AH, Pretoria",
  //         "city": "City of Tshwane Metropolitan Municipality",
  //         "zone": "Gauteng",
  //         "country": "ZA",
  //         "code": "0081",
  //         "lat": -25.80665579999999,
  //         "lng": 28.334732
  //     },
  //     "parcels": [
  //         {
  //             "submitted_length_cm": 42.5,
  //             "submitted_width_cm": 38.5,
  //             "submitted_height_cm": 5.5,
  //             "submitted_weight_kg": 3
  //         }
  //     ],
  //     "account_id": 188,
  //     "declared_value": 1500,
  //     "opt_in_rates": [97],
  //     "opt_in_time_based_rates": [77],
  //     "collection_min_date": "2021-05-21",
  //     "delivery_min_date": "2021-05-21"
  //   }',
  //   CURLOPT_HTTPHEADER => array(
  //     'Authorization: AWS4-HMAC-SHA256 Credential=AKIA55D5DNTBPQ25INCH/20211013/af-south-1/execute-api/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=8b579066153b7dc9903caf1d02022ad4b70d2d6a7b865e73c49846a41bb351de',
  //     'X-Amz-Content-Sha256: beaead3198f7da1e70d03ab969765e0821b24fc913697e929e726aeaebf0eba3',
  //     'X-Amz-Date: 20211013T091313Z'
  //   ),
  //   ));
  //   $response = curl_exec($curl);
  //
  //   curl_close($curl);
  //   print_r($response);
  // }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Aws\Credentials\Credentials;
use Aws\Signature\SignatureV4;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;

use NaeemAwan\CMS\Models\ViewFromChair;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    $dashboardStats = getDashboardStats();
    return view('home',compact('dashboardStats'));
  }

  // function signRequest(ServerRequestInterface $request, string $accessKeyId, string $secretAccessKey): ServerRequestInterface
  // {
  //     $signature = new SignatureV4('execute-api', 'af-south-1');
  //     $credentials = new Credentials($accessKeyId, $secretAccessKey);
  //     $request = $signature->signRequest($request, $credentials);
  //     return $request;
  // }
  //
  // function awsTest(ServerRequestInterface $request)
  // {
  //   $signedReq = $this->signRequest($request,'AKIA55D5DNTBPQ25INCH','u4RtIMPYLKrAEM5WGvfHVhKm68VIHLoPA8r7WirG');
  //   echo '<pre>';print_r($signedReq);echo '</pre>';
  //   die();
  //   $curl = curl_init();
  //
  //   curl_setopt_array($curl, array(
  //   CURLOPT_URL => 'https://api.shiplogic.com/rates',
  //   CURLOPT_RETURNTRANSFER => true,
  //   CURLOPT_ENCODING => '',
  //   CURLOPT_MAXREDIRS => 10,
  //   CURLOPT_TIMEOUT => 0,
  //   CURLOPT_FOLLOWLOCATION => true,
  //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  //   CURLOPT_CUSTOMREQUEST => 'POST',
  //   CURLOPT_POSTFIELDS =>'{
  //     "collection_address": {
  //         "company": "uAfrica.com",
  //         "street_address": "116 Lois Avenue",
  //         "local_area": "Newlands, Pretoria",
  //         "city": "City of Tshwane Metropolitan Municipality",
  //         "zone": "Gauteng",
  //         "country": "ZA",
  //         "code": "0181",
  //         "lat": -25.7863272,
  //         "lng": 28.277583
  //     },
  //     "delivery_address": {
  //         "company": "Bambolini Nursery School",
  //         "street_address": "Midas Avenue",
  //         "local_area": "Olympus AH, Pretoria",
  //         "city": "City of Tshwane Metropolitan Municipality",
  //         "zone": "Gauteng",
  //         "country": "ZA",
  //         "code": "0081",
  //         "lat": -25.80665579999999,
  //         "lng": 28.334732
  //     },
  //     "parcels": [
  //         {
  //             "submitted_length_cm": 42.5,
  //             "submitted_width_cm": 38.5,
  //             "submitted_height_cm": 5.5,
  //             "submitted_weight_kg": 3
  //         }
  //     ],
  //     "account_id": 188,
  //     "declared_value": 1500,
  //     "opt_in_rates": [97],
  //     "opt_in_time_based_rates": [77],
  //     "collection_min_date": "2021-05-21",
  //     "delivery_min_date": "2021-05-21"
  //   }',
  //   CURLOPT_HTTPHEADER => array(
  //     'Authorization: AWS4-HMAC-SHA256 Credential=AKIA55D5DNTBPQ25INCH/20211013/af-south-1/execute-api/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=8b579066153b7dc9903caf1d02022ad4b70d2d6a7b865e73c49846a41bb351de',
  //     'X-Amz-Content-Sha256: beaead3198f7da1e70d03ab969765e0821b24fc913697e929e726aeaebf0eba3',
  //     'X-Amz-Date: 20211013T091313Z'
  //   ),
  //   ));
  //   $response = curl_exec($curl);
  //
  //   curl_close($curl);
  //   print_r($response);
  // }

  // public function frontendindex()
  // {
  //   $chairmna_message = ViewFromChair::where(['display_on_home'=>1])->orderBy('date', 'asc')->first();
  //   // dd($chairmna_message);

  //   return view('frontend.home',compact('chairmna_message'));
  // }

  // public function contactus()
  // {

  //   return view('frontend.site.contact-us');
  // }

  // public function eventsubscribe()
  // {

  //   return view('frontend.news.subscribe');
  // }


  



}

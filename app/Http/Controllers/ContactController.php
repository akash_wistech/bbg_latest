<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Company;
use App\Models\CustomFieldData;
use App\Rules\DuplicateUserCheck;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\InvoiceItems;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use NaeemAwan\ModuleSubscription\Models\Subscription;

class ContactController extends Controller
{
  public $folderName='contact';
  public $controllerId='contact';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view($this->folderName.'.index',compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;

    $a=[];


    $query = DB::table($this->newModel()->getTable())->where(['user_type'=>0])->whereNull('deleted_at');

    if ($request->has('id')) {
      $id = (int)$request->input('id');
      $coms = getCompanyUsersListQuery($id,'contact');
      foreach ($coms as $key => $value) {
        $a[]=$value['id'];
      }
      $query->whereIn('id', $a);
    }
    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='name';
      $srchFlds[]='email';
      $srchFlds[]='phone';
      $srchFlds[]='bill_to_name';
      $srchFlds[]='bill_to_company';
      $srchFlds[]='bill_to_address';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->name!='')$query->where('name','like','%'.$request->name.'%');
    if($request->company_name!=''){
      $companyName=$request->company_name;
      $query = searchInMultipleCompanies($query,$moduleTypeId,$companyName);
    };
    if($request->email!='')$query->where('email','like','%'.$request->email.'%');
    if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
    if($request->bill_to_name!='')$query->where('bill_to_name','like','%'.$request->bill_to_name.'%');
    if($request->bill_to_company!='')$query->where('bill_to_company','like','%'.$request->bill_to_company.'%');
    if($request->bill_to_address!='')$query->where('bill_to_address','like','%'.$request->bill_to_address.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];

    $statusArr = getStatusTextArr();
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        //Colors column
        $thisData['cb_col']='';

        $usertype='contact';
        $userType=  \DB::table('subscription_users')->where('user_id',$model->id)->first();
        if ($userType) {
          $usertype=getPredefinedListItemsArr(2)[$userType->user_type];
          $usertype= '<span class="label label-inline label-light-info font-weight-bold">'.$usertype.'</span>';

        // dd($usertype);
        }

        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['image']=getImageTag($model->image);
        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;
        $thisData['name']=$model->name;
        $companyNames = getRowMultipleCompanies($moduleTypeId,$model->id);
        $thisData['company_name']=$companyNames;
        $thisData['email']='<a href="mailto:'.$model->email.'">'.$model->email.'</a>';
        $thisData['user_type']=$usertype;
        $thisData['phone']=$model->phone;
        $thisData['bill_to_name']=$model->bill_to_name;
        $thisData['bill_to_company']=$model->bill_to_company;
        $thisData['bill_to_address']=$model->bill_to_address;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        $thisData['status']=(isset($statusArr[$model->status]) ? $statusArr[$model->status] : '--');
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy!=null ? $createdBy->fullname : '';
        $actBtns=[];
        // if(checkActionAllowed('create','opportunity')){
        //   $actBtns[]='
        //   <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        //   <i class="text-dark-50 flaticon-add-circular-button"></i>
        //   </a>';
        // }
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }


        // active and deative active account
        // $actBtns[]='
        // <a href="javascript:;" class="btn btn-sm btn-clean btn-icon status-change" data-toggle="tooltip" title="'.(($model->status==1) ? 'Click to UnApprove' : 'Click to Approve').'" data-id="'.$model->id.'" data-url="'.url('/contact/changestatus',['id'=>$model->id]).'">
        // <i class="'.(($model->status==1) ? 'text-primary far fa-check-square' : 'text-danger fas fa-ban').'"></i>
        // </a>';


        // Send Message
        $actBtns[]='
        <a href="'.url('contact/sendMessageTemplate/'.$model->id).'"
        class="btn btn-sm btn-clean btn-icon poplink"  data-id="'.$model->id.'" data-invoicetype="performa_invoice" data-heading="Send Message" >
        <i class="far fa-envelope" data-toggle="tooltip" title="Send Message"></i>
        </a>';

        $activedeactivemsg = 'Are you sure you want to deactivate this?';
        if($model->status==0)$activedeactivemsg = 'Are you sure you want to active this?';
        $actBtns[]='
        <a href="'.url('/'.$controllerId.'/status',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-pjxcntr="grid-table" data-confirmationmsg="'.$activedeactivemsg.'" data-toggle="tooltip" title="'.(($model->status==1) ? 'Click to UnApprove' : 'Click to Approve').'" data-id="'.$model->id.'">
        <i class="'.(($model->status==1) ? 'text-primary far fa-check-square' : 'text-danger fas fa-ban').'"></i>
        </a>';


        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = $this->newModel();
    if($request->input('pid')!=''){
      $model->prospect_id = $request->input('pid');
      assignProspectInfo($model,$request->input('pid'));
    }
    $duplicateModel = null;
    $di = $request->di ?? 0;
    if($di>0){
      $duplicateModel = $this->findModel($di);
    }
    if($request->isMethod('post')){
      $emailsArr = $request->emails;
      $emailsArr[] = $request->email;
      $numbersArr = $request->phone_numbers;
      $numbersArr[] = $request->phone;
      $crequest=['id'=>$model->id,'emails'=>$emailsArr,'numbers'=>$numbersArr];

      $duplicateModel = getUserDuplicateRow($model->moduleTypeId,$crequest);

      $validator = Validator::make($request->all(), [
        'full_name'=>'required|string',
        'email'=>'required|string|email',
        'emails.*'=>'string|email',
        'phone'=>'required|string',
        'duplicatecheck'=>new DuplicateUserCheck($model->moduleTypeId,$crequest)
      ]);
      if(!$validator->fails()){
        $diId = 0;
        //
        // if($diId>0){
        //   $model = $this->findModel($diId);
        // }else{
        $model= $this->newModel();
        // }
        $model->name = trim($request->full_name);
        $model->email = trim($request->email);
        $model->phone = trim($request->phone);

        $model->bill_to_name = $request->bill_to_name;
        $model->bill_to_company = $request->bill_to_company;
        $model->bill_to_address = $request->bill_to_address;
        if(isset($request->pid) && $request->pid>0){
          $model->prospect_id = $request->pid;
        }
        $model->permission_group_id = getSetting('clients_permission_group');
        $model->image = trim($request->image);
        $model->passport_copy = trim($request->passport_copy);
        $model->residence_visa = trim($request->residence_visa);
        // $model->passport_size_pic = trim($request->passport_size_pic);
        if ($model->save()) {

          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          saveMultipleCompany($model,$request,'form');
          saveMultipleEmailNumber($model,$request,'form');
          saveCustomField($model,$request,'form');
          saveSocialMediaLinks($model,$request,'form');
          saveTags($model,$request,'form');
          saveModuleManagerField($model,$request,'form');

          // die();
          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.contact.saved')]]);
          }else{
            return redirect($this->controllerId)->with('success', __('app.contact.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.contact.notsaved'),]]);
          }else{
            return redirect($this->controllerId)->with('error', __('app.contact.notsaved'));
          }
        }
      }else{
        $errors = $validator->errors();
        if($errors->first('duplicatecheck')){
          return redirect($this->controllerId.'/create?pid='.$request->pid.'&di='.$duplicateModel->id)->withErrors($validator)->withInput($request->all());
        }else{
          return redirect($this->controllerId.'/create')->withErrors($validator)->withInput($request->all());
        }
      }
    }
    return view($this->folderName.'.create',compact('model','duplicateModel','di'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = $this->findModel($id);

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'full_name'=>'string',
        'email'=>'string|email',
        'phone'=>'string',
      ]);

      $model= $this->findModel($request->id);
      $model->name = trim($request->full_name);
      $model->email = trim($request->email);
      $model->phone = trim($request->phone);
      $model->image = trim($request->image);
      $model->passport_copy = trim($request->passport_copy);
      $model->residence_visa = trim($request->residence_visa);
      $model->bill_to_name = $request->bill_to_name;
      $model->bill_to_company = $request->bill_to_company;
      $model->bill_to_address = $request->bill_to_address;
      // $model->passport_size_pic = trim($request->passport_size_pic);
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveMultipleCompany($model,$request,'form');
        saveMultipleEmailNumber($model,$request,'form');
        saveCustomField($model,$request);
        saveSocialMediaLinks($model,$request,'form');
        saveTags($model,$request,'form');
        saveModuleManagerField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.contact.saved'),]]);
        }else{
          return redirect($this->controllerId)->with('success', __('app.contact.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.contact.notsaved'),]]);
        }else{
          return redirect($this->controllerId)->with('error', __('app.contact.notsaved'));
        }
      }
    }

    return view($this->folderName.'.update', [
      'model'=>$model,
    ]);

  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Contact;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Contact::where('id', $id)->first();
  }
  public function show(Request $request, $id)
  {

    $model=Contact::where('id',$id)->first();
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $company = getRowMultipleCompaniesQuery($model->moduleTypeId,$model->id);
    $membersCustomFields = DB::table('custom_field_data')->where('module_type',$moduleTypeId)->where('module_id',$model->id)
    ->leftJoin('custom_field', 'custom_field_data.input_field_id', '=', 'custom_field.id')
    ->select('custom_field_data.input_value','custom_field.title')
    ->pluck('input_value','title')->toArray();
    $nonTextAreaColumns=getNonTextAreaColumns($model->moduleTypeId);
    $textAreaColumns=getTextAreaColumns($model->moduleTypeId);



    if($model->passport_copy != '' && $model->passport_copy != null){
      if(str_contains($model->passport_copy, '.pdf')){
        $passportCopyImg =asset('images/pdf_sample_img.png');
      }else{
        $passportCopyImg=$model->passport_copy;
      }
    }else{
      $passportCopyImg = asset('assets/images/dummy-image.jpg');
    }



    if($model->residence_visa != '' && $model->residence_visa != null){
      if(str_contains($model->residence_visa, '.pdf')){
        $residenceVisaImg =asset('images/pdf_sample_img.png');
      }else{
        $residenceVisaImg=$model->residence_visa;
      }
    }else{
      $residenceVisaImg = asset('assets/images/dummy-image.jpg');
    }

    $account_type_id =  DB::table('subscription_users')->where('user_id',$model->id)->select('user_type')->first();

    if ($account_type_id) {
      $account_type_id=getPredefinedListItemsArr(2)[$account_type_id->user_type];
    }else {
      $account_type_id='Not Available';
    }





    return view('contact.view',[
      'model'=>$model,
      'moduleTypeId'=>$moduleTypeId,
      'company'=>$company,
      'membersCustomFields'=>$membersCustomFields,
      'nonTextAreaColumns'=>$nonTextAreaColumns,
      'textAreaColumns'=>$textAreaColumns,
      'passportCopyImg'=>$passportCopyImg,
      'residenceVisaImg'=>$residenceVisaImg,
      'account_type_id'=>$account_type_id,
      'request'=>$request,

    ]);

  }



  public function ImportExport(){

    $c1 = DB::table('accounts_old')->get();
    //
    // foreach($c1 as $record){
    //
    //     dd($record->user_name);
    //
    //     $contact = new Contact ;
    //
    //     $contact->old_id = $record->user_name;
    //     $contact->reference_no = $record->user_name;
    //
    //
    //     $contact->user_name = $record->title;
    //     $contact->name = $record->first_name.' '.$record->last_name;
    //     $contact->user_name = $record->designation;
    //     $contact->email = $record->email;
    //     $contact->user_name = $record->secondry_email;
    //     $contact->phone = $record->phone_number;
    //     $contact->user_name = $record->gender;
    //     $contact->user_name = $record->country;
    //     $contact->user_name = $record->country_code;
    //     $contact->user_name = $record->city;
    //     $contact->user_name = $record->nationality;
    //     $contact->user_name = $record->address;
    //     $contact->user_name = $record->company;
    //     $contact->user_name = $record->group_id;
    //     $contact->user_name = $record->account_type;
    //     $contact->status = $record->status;
    //     $contact->user_name = $record->invoiced;
    //     $contact->user_name = $record->registeration_date;
    //     $contact->user_name = $record->expiry_date;
    //     $contact->user_name = $record->last_renewal;
    //     $contact->image = $record->picture;
    //
    //
    //     $contact->user_name = $record->paymentCycle;
    //     $contact->user_name = $record->occasional_updates;
    //     $contact->user_name = $record->newsletter;
    //     $contact->user_name = $record->inforamtion_source;
    //     $contact->user_name = $record->vat_number;
    //     $contact->user_name = $record->linkedin;
    //     $contact->user_name = $record->twitter;
    //     $contact->user_name = $record->user_industry;
    //     $contact->user_name = $record->mem_invoice_gen;
    //     $contact->user_name = $record->renw_invoice_gen;
    //
    //
    //     // $contact->user_name = $record->honourary;
    //     // $contact->user_name = $record->committee;
    //     // $contact->user_name = $record->focus_chair;
    //     // $contact->user_name = $record->charity;
    //     // $contact->user_name = $record->invoice_renewal;
    //     // $contact->user_name = $record->bbg_membershipid;
    //     // $contact->user_name = $record->old_membershipid;
    //     // $contact->user_name = $record->lastpasswordchange;
    //     // $contact->user_name = $record->lastvisit;
    //     // $contact->user_name = $record->delete_request;
    //     // $contact->user_name = $record->approved;
    //     // $contact->user_name = $record->login_first;
    //     // $contact->user_name = $record->user_type_relation;
    //
    //     $contact->user_name = $record->howDidYouHear;
    //     $contact->user_name = $record->tellUsAboutYourself;
    //     $contact->user_name = $record->purposeOfJoining;
    //     $contact->user_name = $record->referred_by;
    //
    // }




    //
    //
    //
    //
    //
    // dd($c1);
    //



  }

  public function checkDuplicaton($lastid=null){


    if ($lastid>0 && Session::get('issueFound')) {
      $issueFound=Session::get('issueFound');
    }else {
      $issueFound=0;
    }


    $c1 = DB::table('account_company')
    ->where('id','>',$lastid)
    ->get()->take(100);
      // dd($c1);
    if (count($c1)<=0) {
      echo $issueFound; die();
    }
    foreach($c1 as $record){

      $lastid=$record->id;
      $oldExpiry='';
      $members = DB::table('accounts_old')->where('company',$record->id)->get();

      foreach ($members as $key => $member) {
        if($oldExpiry!='' && $oldExpiry!=$member->expiry_date){
          $issueFound++;
        }
        $oldExpiry = $member->expiry_date;
      }
    }


    Session::put('issueFound', $issueFound);
    sleep(1);
    echo '<script>window.location.href="'.url('/contact/check/'.$lastid).'"</script>';


  }



  public function ImportExport2(Request $request,$lastid=null){



    $c1 = DB::table('account_company')->where('id','>',$lastid)
    ->get()->take(10);
    foreach($c1 as $record){
      $lastid++;
      $company = new Company;
      $company->old_company_id = $record->id;
      $company->title = $record->name;
      $company->logo = $record->logo;
      $company->vat_number = $record->vat_number;
      $company->visibility = $record->is_active;
      $company->show_in_directory = $record->show_in_directory;
      $company->save();
      $member = DB::table('accounts_old')->where('company',$company->old_company_id)->where('account_type','nominee')->first();


      if (isset($member->expiry_date) && $member->expiry_date<>null) {
        $subscriptions = new   Subscription;
        $subscriptions->membership_type_id = $member->group_id;
        $subscriptions->company_id  = $company->id;
        $subscriptions->start_date = $member->registeration_date;
        $subscriptions->end_date = $member->expiry_date;
        $totalMonths = getDataInDates('m',$member->registeration_date,$member->expiry_date);
        $totalDays = getDataInDates('d',$member->registeration_date,$member->expiry_date);
        $subscriptions->no_of_months = round($totalMonths);
        $subscriptions->no_of_days = round($totalDays);
        $subscriptions->activated =1;
        $subscriptions->save();



        $company->active_subscription_id = $subscriptions->id;
        // $members = DB::table('accounts_old')->where('company',$record->id)->get();
        $members = DB::table('accounts_old')->where('company',$company->old_company_id)->get();
        foreach ($members as $key => $member) {

          $invoices_old = DB::table('old_invoices')->where('user_id','!=',null)->where('user_id',$member->id)
          ->where('invoice_related_to','member')->where('invoice_category','Membership Registration')
          ->first();

          if ($invoices_old) {

            $invoice = new Invoices;
            $invoice->old_invoice_id= $invoices_old->invoice_id;
            if ($invoice->payment_status=='paid' && $invoice->payment_status=='partialy_paid' ) {
              // $invoice->reference_number = $invoices_old->reference_number;
              // $invoice->invoice_no  = $invoices_old->invoice_no ;
              $invoice->invoice_date = $invoices_old->invoice_date;
            }
            else {
              // $invoice->p_reference_number = $invoices_old->p_reference_number;
              // $invoice->p_invoice_no  = $invoices_old->p_invoice_no ;
              $invoice->p_invoice_date = $invoices_old->invoice_date;
            }

            // $invoice->payment_status  = $invoices_old->payment_status ;
            $invoice->sub_total = $invoices_old->subtotal;
            $invoice->grand_total = $invoices_old->total;
            $invoice->client_note = $invoices_old->note;
            $invoice->user_id  = $invoices_old->user_id;
            $invoice->company_id  = $company->id;
            $invoice->invoice_type = ($invoices_old->invoice_related_to=='Member')? 0 : 1  ;
            $invoice->currency_id  = 1 ;
            $invoice->discount_calculation = 'aftertax';
            $invoice->is_recurring  = 0 ;
            if($invoice->save()){

              $invoices_old_items = DB::table('old_invoice_items')->where('invoice_id',$invoice->old_invoice_id)
              ->where('invoice_related_to','membership')->get();

              foreach ($invoices_old_items as $key => $value) {
              //
              // dd($value->invoice_id);
                $invoices_items = new InvoiceItems;
                $invoices_items->invoice_id = $value->invoice_id;
                $invoices_items->item_type  = 'service';
                $invoices_items->description  = $value->invoice_category;
                $invoices_items->qty   = 1;
                $invoices_items->total   = $value->amount;
                $invoices_items->save();

              }
            }
          }

          // dd($invoice);

          $contact = new Contact;
          $contact->old_id = $member->id;
          $contact->name = $member->first_name.' '.$member->last_name;
          $contact->test_email = $member->email;
          $contact->phone = $member->phone_number;
          $contact->gender = $member->gender;
          $contact->status = $member->status;
          $contact->invoiced = (($member->invoiced<>null)? $member->invoiced :null) ;
          $contact->image = $member->picture;
              // $contact->created_at = $member->created_at;
              // $contact->updated_at = $member->updated_at;
          $contact->newsletter = $member->newsletter;
          $contact->phone = $member->phone_number;
          $contact->user_type_relation = $member->user_type_relation;
          if($contact->save()){
              // dd($contact);
            $contact->input_field[20] = $member->title;
            $contact->input_field[9] = $member->designation;
            $contact->input_field[12] = $member->country_code;
            $contact->input_field[11] = $member->city;
            $contact->input_field[10] = $member->nationality;
            $contact->input_field[13] = $member->address;
            $contact->input_field[11] = $member->city;
            $contact->input_field[24] = $member->vat_number;
            $contact->input_field[14] = $member->linkedin;
            $contact->input_field[15] = $member->twitter;
            $contact->input_field[21] = $member->howDidYouHear;
            $contact->input_field[16] = $member->tellUsAboutYourself;
            $contact->input_field[17] = $member->purposeOfJoining;
            $contact->input_field[22] = $member->referred_by;
            $contact->input_field[23] = $member->account_type;
            saveCustomField($contact,$request);
            $module_company = new  ModuleCompany;
            $module_company->module_type = 'contact';
            $module_company->module_id  = $contact->id;
              // This is commented because is cannot be worked with old id
              // $module_company->module_id  = $member->id;
            $module_company->company_id  =$company->id;
            $module_company->is_primary =1;
            $module_company->save();
          }
          // It saves users document
          $documents = DB::table('membership_documents')->where('account_id',$member->id)->where('account_id','!=',null)->first();
          if ($documents->trade_licence<>null) {
            $company->trade_licence = $documents->trade_licence;
            break;
          }
          // It saves invoices against company from member
        }
        if($company->save()){
          $company->input_field[1][] = getSect()[$record->category];
          $company->input_field[2] = $record->url;
          $company->input_field[3] = $record->phonenumber;
          $company->input_field[4] = $record->fax;
          $company->input_field[5] = $record->postal_code;
          $company->input_field[6] = $record->emirates_number;
          $company->input_field[7] = $record->address;
          $company->input_field[8] = $record->about_company;
          saveCustomField($company,$request);
          // dd($record->name);
        }
      }
    }
    sleep(1);
    echo '<script>window.location.href="'.url('/contact/import2/'.$lastid).'"</script>';

  }


  public function changestatus(Request $request, $id)
  {
    $model = $this->findModel($id);
    if ($model->status==1) {
      $status =0;
    }
    if ($model->status==0) {
      $status=1;
    }
    $model->status=$status;
    if ($model->update()) {
      return new JsonResponse(['success'=>['heading'=>__('success'),'msg'=>__('Status has been changed')]]);
    }
    return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('Status is not working')]]);
  }

  public function sendMessageTemplate(Request $request, $id)
  {
    $model = $this->findModel($id);
    return view('contact.send_message',
      [
        'model' => $model,
      ]);
  }

  public function sendMessage(Request $request)
  {
    $data = [
      'view' => $request->post('message'),
      'subject'=> $request->post('subject'),
      'client_name'=> $request->post('name'),
      'email'=> 'mushabab10000@gmail.com',
    ];
    \Mail::send([], [],
      function ($message) use ($data)
      {
        $message
        ->from('mushabab10000@gmail.com','Mushabab Alam')
        ->to($data['email'],$data['client_name'])->subject($data['subject'])->setBody($data['view'], 'text/html');;
      });
    return true;

  }




}

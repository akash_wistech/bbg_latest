<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Prospect;
use App\Http\Resources\ProspectResource;
use App\Models\CustomFieldData;
use App\Http\Requests\ProspectImportForm;
use App\Rules\DuplicateCheck;

class ProspectController extends Controller
{
  public $folderName='prospect';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    // $query = DB::table($this->newModel()->getTable());
    // $query->whereNotIn('id',function($query) use ($moduleTypeId){
    //   $query->from('module_manager')
    //   ->selectRaw('module_id')
    //   ->where('module_type',$moduleTypeId);
    // });
    //
    // $results = $query->limit(25)->get();
    // if($results!=null){
    //   foreach($results as $result){
    //     DB::table('module_manager')->insert([
    //         'module_type' => $moduleTypeId,
    //         'module_id' => $result->id,
    //         'staff_id' => $result->created_by,
    //     ]);
    //   }
    // }
    return view('prospect.index',compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'prospect',$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='full_name';
      $srchFlds[]='company_name';
      $srchFlds[]='email';
      $srchFlds[]='phone';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->full_name!='')$query->where('full_name','like','%'.$request->full_name.'%');
    if($request->company_name!='')$query->where('company_name','like','%'.$request->company_name.'%');
    if($request->email!='')$query->where('email','like','%'.$request->email.'%');
    if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        //Colors column
        $thisData['cb_col']='';

        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;
        $thisData['full_name']=$model->full_name;
        $thisData['email']=$model->email;
        $thisData['phone']=$model->phone;
        $thisData['company_name']=$model->company_name;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        $thisData['created_at']=formatDateTime($model->created_at);

        $name = '';
        $createdBy = getUserInfo($model->created_by);
        if($createdBy!=null)$name = $createdBy->fullname;
        $thisData['created_by']=$name;
        $actBtns=[];
        if(checkActionAllowed('create','opportunity')){
          $actBtns[]='
          <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-add-circular-button"></i>
          </a>';
        }
        if(checkActionAllowed('convert','prospect')){
          $actBtns[]='
          <a href="'.url('/contact/create?pid='.$model->id).'" class="btn btn-sm btn-clean btn-icon view" target="_blank" data-toggle="tooltip" title="'.__('common.convert_to_client').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-customer"></i>
          </a>';
        }
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/prospect/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/prospect/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/prospect/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * return all rows for api
  *
  * @return \Illuminate\Http\Response
  */
  public function apiData()
  {
    // $userId = auth('api')->user()->id;

    $query = Prospect::whereNull('deleted_at')
    ->orderBy('updated_at','desc')
    ->paginate(20);

    // return ProspectResource::collection($query)->ownerUser($userId)->response();
    return ProspectResource::collection($query)->response();
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = $this->newModel();
    $di = $request->di ?? 0;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'full_name'=>'required|string',
        'company_name'=>'string',
        'email'=>'string|email',
        'phone'=>'string',
        'duplicatecheck'=>new DuplicateCheck($model->moduleTypeId,$request,$request->di)
        // 'input_field'=>new DuplicateCustomFieldCheck($model->moduleTypeId,$request->di)
      ]);
      if(!$validator->fails()){
        $diId = 0;
        $crequest=['email'=>$request->email,'phone'=>$request->phone];
        if($di==1){
          $diId = getModuleDuplicateId($model->moduleTypeId,$crequest);
        }
        if($diId>0){
          $model = $this->findModel($diId);
        }else{
          $model= $this->newModel();
        }
        $model->full_name = $request->full_name;
        $model->company_name = $request->company_name;
        $model->email = $request->email;
        $model->phone = $request->phone;
        if ($model->save()) {
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          saveCustomField($model,$request,'form');
          saveTags($model,$request,'form');
          saveModuleManagerField($model,$request,'form');

          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.prospect.saved')]]);
          }else{
            return redirect('prospect')->with('success', __('app.prospect.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.prospect.notsaved'),]]);
          }else{
            return redirect('prospect')->with('error', __('app.prospect.notsaved'));
          }
        }
      }else{
        $errors = $validator->errors();
        if($errors->first('duplicatecheck')){
          return redirect('prospect/create?di=1')->withErrors($validator)->withInput($request->all());
        }else{
          return redirect('prospect/create')->withErrors($validator)->withInput($request->all());
        }
      }
    }
    return view('prospect.create',compact('model','di'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function import(Request $request)
  {
    $model = new ProspectImportForm;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
      ]);
      $moduleTypeId = $this->newModel()->moduleTypeId;
      $customFields = getCustomFieldsByModule($moduleTypeId);

      $csvFile = temp_upload_path().'/'.$request->csv_file_name;
      $handle = fopen($csvFile, "r");

      $crequest = [];//$request;
      $saved = 0;
      $savedUnique = 0;
      $replaced = 0;

      $replaceExisting = $request->duplicate_replace;

      $i=1;
      while (($fileop = fgetcsv($handle, 1000, ",")) !== false){
        if($i>1){
          $email = $fileop[$request->input_map_field['static']['email']];
          $phone = $fileop[$request->input_map_field['static']['phone']];

          $crequest=['email'=>$email,'phone'=>$phone];
          $diId = 0;
          $diId = getModuleDuplicateId($moduleTypeId,$crequest);
          //Update
          if($replaceExisting==1 && $diId>0){
            $prospect = $this->findModel($diId);
          }else{
            $prospect = $this->newModel();
          }
          $prospect->full_name = $fileop[$request->input_map_field['static']['full_name']];
          $prospect->company_name = $fileop[$request->input_map_field['static']['company_name']];
          $prospect->email = $email;
          $prospect->phone = $phone;
          if($prospect->save()){
            $saved++;
            if($replaceExisting==1 && $diId>0){
              $replaced++;
            }else{
              $savedUnique++;
            }
            if($customFields!=null){
              foreach($customFields as $customField){
                if(isset($request->input_map_field[$customField->id])){
                  $crequest['input_field'][$customField->id] = $fileop[$request->input_map_field[$customField->id]];
                  $prospect->input_field[$customField->id] = $fileop[$request->input_map_field[$customField->id]];
                }
              }
            }
            $mtrequest=[];
            if(isset($request->input_map_field['static']['public_tags'])){
              $mtrequest['public_tags'] = $fileop[$request->input_map_field['static']['public_tags']];
            }
            if(isset($request->input_map_field['static']['private_tags'])){
              $mtrequest['private_tags'] = $fileop[$request->input_map_field['static']['private_tags']];
            }
            $matrequest=[];
            if(isset($request->input_map_field['static']['assign_to'])){
              $matrequest['assign_to'] = $fileop[$request->input_map_field['static']['assign_to']];
              saveImportAssignedField($prospect,$matrequest);
            }

            saveCustomField($prospect,$crequest,'import');
            saveTags($prospect,$mtrequest,'import');
            saveModuleManagerField($prospect,$request);
          }
        }
        $i++;
      }
      // die();
      $msg = __('app.prospect.imported',['total'=>$saved,'unique'=>$savedUnique,'replaced'=>$replaced]);
      return redirect('prospect')->with('success', $msg);
    }
    return view('prospect.import',compact('model'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function upload(Request $request)
  {
    $allowedFileTypes=[
      'csv','CSV'
    ];
    $moduleTypeId = (new Prospect)->moduleTypeId;
    if ($request->hasFile('file')) {
      $image = $request->file('file');
      if (in_array($image->getClientOriginalExtension(),$allowedFileTypes)) {
        $content = file_get_contents($request->file('file'));
        if (preg_match('/\<\?php/i', $content)) {
        }else{
          $csvFileName = generateName().'.'.$image->getClientOriginalExtension();
          $image->move(temp_upload_path(),$csvFileName);
          $columnsArr=[];
          $csvFile = temp_upload_path().'/'.$csvFileName;
          $handle = fopen($csvFile, "r");
          $fileop = fgetcsv($handle, 1000, ",");
          $tableHtml ='<table class="table">';
          $tableHtml.=' <thead>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>Field</td>';
          $tableHtml.='     <td>Map With</td>';
          $tableHtml.='   </tr>';
          $tableHtml.=' </thead>';
          $tableHtml.=' <tbody>';
          $selOptsHtml = '';
          $selOptsHtml.='<option value="">'.__('common.select').'</option>';
          foreach ($fileop as $key => $value) {
            $columnsArr[$key]=$value;
            $selOptsHtml.='<option value="'.$key.'">'.$value.'</option>';
          }

          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.full_name').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][full_name]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.company_name').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][company_name]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.email').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][email]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.phone').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][phone]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          //Loop custome fields
          $customFields = getCustomFieldsByModule($moduleTypeId);
          if($customFields!=null){
            foreach($customFields as $customField){
              $tableHtml.='   <tr>';
              $tableHtml.='     <td>'.$customField->title.'</td>';
              $tableHtml.='     <td><select name="input_map_field['.$customField->id.']" class="form-control">'.$selOptsHtml.'</td>';
              $tableHtml.='   </tr>';
            }
          }

          $tableHtml.= getCustomTagsImportInputFields($selOptsHtml);
          $tableHtml.= getAssignedToImportInputFields($selOptsHtml);

          $tableHtml.=' </tbody>';
          $tableHtml.='</table>';
          return new JsonResponse([
            'success'=>[
              'name'=>$csvFileName,
              'columns'=>$columnsArr,
              'tableHtml'=>$tableHtml,
            ]
          ]);
        }
      }
    }
    exit;
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Prospect  $prospect
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = Prospect::where('id', $id)->first();

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'full_name'=>'string',
        'company_name'=>'string',
        'email'=>'string|email',
        'phone'=>'string',
        // 'title'=>'required',
        // 'descp'=>'nullable|string',
        // 'status'=>'required|integer',
      ]);

      $model= Prospect::find($request->id);
      $model->full_name = $request->full_name;
      $model->company_name = $request->company_name;
      $model->email = $request->email;
      $model->phone = $request->phone;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request);
        saveTags($model,$request,'form');
        saveModuleManagerField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.prospect.saved'),]]);
        }else{
          return redirect('prospect')->with('success', __('app.prospect.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.prospect.notsaved'),]]);
        }else{
          return redirect('prospect')->with('error', __('app.prospect.notsaved'));
        }
      }
    }

    return view('prospect.update', [
      'model'=>$model,
    ]);

  }

  /**
  * Create a model
  *
  * @param  \App\Models\Prospect  $prospect
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Prospect;
  }

  /**
  * Fix old rows
  *
  * @param  \App\Models\Prospect  $prospect
  * @return \Illuminate\Http\Response
  */
  public function fixOldrows(Request $request)
  {
    $lastid = $request->lastid;
    if($lastid=='')$lastid=0;
    $query = DB::table($this->newModel()->getTable())
    ->where('id','>',$lastid)
    ->whereNull('deleted_at');

    $nameCFId = 2;
    $emailCFId = 4;
    $phoneCFId = 5;
    $companyCFId = 3;

    $moduleTypeId = $this->newModel()->moduleTypeId;

    $prospects = $query->orderBy('id','asc')->limit(25)->get();
    if($prospects!=null && count($prospects)>0){
      foreach($prospects as $prospect){
        $lastid = $prospect->id;

        $fullnameValue = '';
        $fullnameValue = getCustomFieldInputValue($moduleTypeId,$prospect->id,$nameCFId);
        $emailValue = '';
        $emailValue = getCustomFieldInputValue($moduleTypeId,$prospect->id,$emailCFId);
        $phoneValue = '';
        $phoneValue = getCustomFieldInputValue($moduleTypeId,$prospect->id,$phoneCFId);
        $companyValue = '';
        $companyValue = getCustomFieldInputValue($moduleTypeId,$prospect->id,$companyCFId);

        DB::table($this->newModel()->getTable())
        ->where('id', $prospect->id)
        ->update([
          'full_name' => $fullnameValue,
          'email' => $emailValue,
          'phone' => $phoneValue,
          'company_name' => $companyValue,
        ]);
      }
      sleep(1);
      echo '<script>';
      echo 'window.location.href="'.url('prospect/fix-old-data?lastid='.$lastid).'"';
      echo '</script>';
      echo '<a href="'.url('prospect/fix-old-data?lastid='.$lastid).'">Next</a>';
    }else{
      echo "<strong>Finished</strong>";
    }
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Prospect  $prospect
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Prospect::where('id', $id)->first();
  }
}

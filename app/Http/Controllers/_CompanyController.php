<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;
use App\Models\Company;

class CompanyController extends Controller
{
  public $folderName='company';
  public $controllerId='company';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;
    return view($this->folderName.'.index',compact('moduleTypeId','controllerId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $controllerId = $this->controllerId;

    $query = DB::table($this->newModel()->getTable())->whereNull('deleted_at');
    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='title';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->name!='')$query->where('title','like','%'.$request->name.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        $thisData['cb_col']='';
        //Colors column
        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;
        $row = getEMById($moduleTypeId,$model->id);
        $logoSrc=getStorageFileUrl(optional($row->featuredImage)->path, 'logo');
        $thisData['logo']='<img src="'.$logoSrc.'" width="30" />';
        $fullTitle = getCompanyFullName($model->title,$model->parent_company_id);
        $thisData['title']=$fullTitle;

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request)
  {
    $model = $this->newModel();
    $model->parent_company_id = 0;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required|string',
        'parent_company_id'=>'integer|nullable',
        'email'=>'email|nullable',
        'vat_number'=>'string|nullable',
        'visibility'=>'integer|nullable',
      ]);
      if(!$validator->fails()){
        $model= $this->newModel();
        $model->parent_company_id = $request->parent_company_id ?? 0;
        $model->title = trim($request->title);
        $model->email = trim($request->email);
        $model->vat_number = trim($request->vat_number);
        $model->visibility = $request->visibility;
        if ($model->save()) {
          if($request->hasFile('image')){
            $image = $model->saveImage($request->file('image'), true);
          }
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          saveCustomField($model,$request,'form');
          saveTags($model,$request,'form');
          saveModuleManagerField($model,$request,'form');

          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.company.saved')]]);
          }else{
            return redirect($this->controllerId)->with('success', __('app.company.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.company.notsaved'),]]);
          }else{
            return redirect($this->controllerId)->with('error', __('app.company.notsaved'));
          }
        }
      }else{
        return redirect($this->controllerId.'/create')->withErrors($validator)->withInput($request->all());
      }
    }
    $controllerId = $this->controllerId;
    if($request->ajax()){
      $viewFile = $this->folderName.'._form';
    }else{
      $viewFile = $this->folderName.'.create';
    }
    return view($viewFile,compact('model','controllerId','request'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = $this->findModel($id);

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'string|required',
        'parent_company_id'=>'integer|nullable',
        'email'=>'email|nullable',
        'vat_number'=>'string|nullable',
        'visibility'=>'integer|nullable',
      ]);

      $model= $this->findModel($request->id);
      $model->title = trim($request->title);
      $model->parent_company_id = trim($request->parent_company_id) ?? 0;
      $model->email = trim($request->email);
      $model->vat_number = trim($request->vat_number);
      $model->visibility = $request->visibility;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->hasFile('image')){
          $image = $model->saveImage($request->file('image'), true);
        }
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request);
        saveTags($model,$request,'form');
        saveModuleManagerField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.company.saved'),]]);
        }else{
          return redirect($this->controllerId)->with('success', __('app.company.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.company.notsaved'),]]);
        }else{
          return redirect($this->controllerId)->with('error', __('app.company.notsaved'));
        }
      }
    }

    return view($this->folderName.'.update', [
      'model'=>$model,
      'request'=>$request
    ]);

  }

  /**
  * Create a model
  *
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Company;
  }

  /**
  * Find a model based on the primary key
  *
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Company::where('id', $id)->first();
  }
  public function show(Request $request, $id)
{
  $moduleTypeId = $this->newModel()->moduleTypeId;
  $model=Company::where('id',$id)->first();
  // $invoices = Invoice::where('company_id',$id)->get()->toArray();
  // $payments = Payment::where('company_id',$id)->get()->toArray();
  // $subscriptions = Subscription::where('company_id',$id)->get()->toArray();

  // $members = Membership::where('company_id',$id)->get()->toArray();

  return view('company.view',[
    'model'=>$model,
    'moduleTypeId'=>$moduleTypeId,
    // 'members'=>$members,
    // 'invoices'=>$invoices,
    // 'payments'=>$payments,
    // 'subscriptions'=>$subscriptions,
    'request'=>$request,

  ]);

}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Prospect;
use App\Models\Opportunity;
use App\Models\CustomFieldData;
use App\Http\Requests\LeadImportForm;
use App\Rules\DuplicateCheck;

class LeadController extends Controller
{
  public $folderName='opportunity';
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    return view('lead.index',compact('moduleTypeId','request'));
  }

  /**
  * returns data for datatable
  *
  * @return \Illuminate\Http\Response
  */
  public function datatableData(Request $request)
  {
    $moduleTypeId = $this->newModel()->moduleTypeId;
    $query = DB::table($this->newModel()->getTable())->where('rec_type','l')->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),'opportunity',$query);

    if($request->search['value']!=''){
      $keyword = $request->search['value'];
      $srchFlds[]='reference_no';
      $srchFlds[]='title';
      $srchFlds[]='descp';
      searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
    }

    if($request->created_at!=''){
      if(strpos($request->created_at," - ")){
        $query->where(function($query) use($request){
          list($start_date,$end_date)=explode(" - ",$request->created_at);
          $query->where('created_at','>=',$start_date)
          ->where('created_at','<=',$end_date);
        });
      }else{
        $query->where('created_at',$request->created_at);
      }
    }
    if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
    if($request->title!='')$query->where('title','like','%'.$request->title.'%');
    if($request->service_type!='')$query->where('service_type','like','%'.$request->service_type.'%');
    if($request->module_type!='')$query->where('module_type','like','%'.$request->module_type.'%');
    if($request->expected_close_date!='')$query->where('expected_close_date','like','%'.$request->expected_close_date.'%');
    if($request->quote_amount!='')$query->where('quote_amount','like','%'.$request->quote_amount.'%');
    if($request->descp!='')$query->where('descp','like','%'.$request->descp.'%');
    if($request->input_field!=''){
      advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
    }

    $countQuery = clone $query;

    $totalRecords = $countQuery->count('id');

    $gridViewColumns=getGridViewColumns($moduleTypeId);

    $orderBy = 'id';
    if($request->ob)$orderBy = $request->ob;
    $orderByOrder = 'desc';
    if($request->obo)$orderByOrder = $request->obo;
    $models = $query->offset($request->start)
    ->limit($request->length)
    ->orderBy($orderBy,$orderByOrder)
    ->get();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData=[];
        //Colors column
        $thisData['cb_col']='';

        $colorArr = getActivityColors($moduleTypeId,$model->id);
        $thisData['cs_col']=implode("",$colorArr);

        $thisData['id']=$model->id;
        $thisData['reference_no']=$model->reference_no;

        $thisData['title']=$model->title;
        $pdItem = getPredefinedItem($model->service_type);
        $thisData['service_type']=$pdItem->lang->title;
        $thisData['module_type']=getModulesListArr()[$model->module_type].getModuleNameByType($model->module_type,$model->module_id);

        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
          }
        }

        $thisData['expected_close_date']=formatDate($model->expected_close_date);
        $thisData['amount']=getCurrencyPre().$model->quote_amount;
        $thisData['created_at']=formatDateTime($model->created_at);
        $createdBy = getUserInfo($model->created_by);
        $thisData['created_by']=$createdBy->fullname;
        $actBtns=[];
        if(checkActionAllowed('view')){
          $actBtns[]='
          <a href="'.url('/lead/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-file-2"></i>
          </a>';
        }
        if(checkActionAllowed('update')){
          $actBtns[]='
          <a href="'.url('/lead/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
          <i class="text-dark-50 flaticon-edit"></i>
          </a>';
        }
        if(checkActionAllowed('delete')){
          $actBtns[]='
          <a href="'.url('/lead/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
          <i class="text-dark-50 flaticon-delete"></i>
          </a>';
        }
        $thisData['action_col']=getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }
    $dtInputColsArr = [];
    // $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
    // $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
      }
    }
    $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
    $inputArr = [
      'columns' => $dtInputColsArr,
      'draw' => (int)$request->draw,
      'length' => (int)$request->length,
      'order' => [
        ['column'=>0,'dir'=>'asc'],
      ],
      'search' => ['value'=>$request->search['value'],'regex'=>false],
      'start' => $request->start,
    ];

    $response = [
      'draw' => (int)$request->draw,
      'input' => $inputArr,
      'recordsTotal' => $totalRecords,
      'recordsFiltered' => $totalRecords,
      'data' => $dataArr,
    ];
    return new JsonResponse($response);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create(Request $request,$module='prospect',$module_id=0)
  {
    $model = $this->newModel();
    $subModel = getModelClass()[$module];
    $model->subModel=new $subModel;
    $di = $request->di ?? 0;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required|string',
        'full_name'=>'required|string',
        'company_name'=>'nullable|string',
        'email'=>'required|string',
        'phone'=>'required|string',
        'service_type'=>'required|integer',
        'expected_close_date'=>'string|nullable',
        'quote_amount'=>'numeric|nullable',
        'descp'=>'string|nullable',
        'duplicatecheck'=>new DuplicateCheck($model->subModel->moduleTypeId,$request,$request->di)
      ]);
      if(!$validator->fails()){
        $diId = 0;
        $crequest=['email'=>$request->email,'phone'=>$request->phone];
        if($di==1){
          $diId = getModuleDuplicateId($model->subModel->moduleTypeId,$crequest);
        }
        if($diId>0){
          $subModel = $subModel::where('id',$diId)->first();
        }else{
          $subModel = new $subModel;
        }

        if($subModel->moduleTypeId=='prospect'){
          $subModel->full_name = $request->full_name;
          $subModel->company_name = $request->company_name;
          $subModel->email = $request->email;
          $subModel->phone = $request->phone;
          $subModel->save();
          foreach($request->input_field as $key=>$val){
            $subModel->input_field[$key]=$val;
          }
          saveCustomField($subModel,$request);
          saveModuleManagerField($subModel,$request);
          $module_id=$subModel->id;
        }

        $model= $this->newModel();
        $model->rec_type = 'l';
        $model->title = $request->title;
        $model->service_type = $request->service_type;
        $model->module_type = $module;
        $model->module_id = $module_id;
        $model->expected_close_date = $request->expected_close_date;
        $model->quote_amount = $request->quote_amount;
        $model->descp = $request->descp;
        if ($model->save()) {
          if($request->input_field!=null){
            foreach($request->input_field as $key=>$val){
              $model->input_field[$key]=$val;
            }
          }
          saveCustomField($model,$request,'form');
          saveModuleManagerField($model,$request,'form');
          saveWorlflowField($model,$request);
          if($request->ajax()){
            return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.lead.saved')]]);
          }else{
            return redirect('lead')->with('success', __('app.lead.saved'));
          }
        }else{
          if($request->ajax()){
            return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.lead.notsaved'),]]);
          }else{
            return redirect('lead')->with('error', __('app.lead.notsaved'));
          }
        }
      }else{
        $errors = $validator->errors();
        if($errors->first('duplicatecheck')){
          return redirect('lead/create?di=1')->withErrors($validator)->withInput($request->all());
        }else{
          return redirect('lead/create')->withErrors($validator)->withInput($request->all());
        }
      }
    }
    return view('lead.create',compact('model','di'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Models\Opportunity  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $model = Opportunity::where('id', $id)->first();
    $subModel = getModelClass()[$model->module_type];
    $subModel = $subModel::where('id',$model->module_id)->first();
    $model->subModel=$subModel;
    $model->full_name = $subModel->full_name;
    $model->company_name = $subModel->company_name;
    $model->email = $subModel->email;
    $model->phone = $subModel->phone;

    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
        'title'=>'required',
        'full_name'=>'required|string',
        'company_name'=>'nullable|string',
        'email'=>'required|string',
        'phone'=>'required|string',
        'descp'=>'nullable|string',
      ]);


      if($subModel->moduleTypeId=='prospect'){
        $subModel->full_name = $request->full_name;
        $subModel->company_name = $request->company_name;
        $subModel->email = $request->email;
        $subModel->phone = $request->phone;
        $subModel->updated_at = date("Y-m-d H:i:s");
        $subModel->save();
        foreach($request->input_field as $key=>$val){
          $subModel->input_field[$key]=$val;
        }
        saveCustomField($subModel,$request);
        saveModuleManagerField($subModel,$request);
      }

      $model= Opportunity::find($request->id);
      $model->title = $request->title;
      $model->service_type = $request->service_type;
      $model->expected_close_date = $request->expected_close_date;
      $model->quote_amount = $request->quote_amount;
      $model->descp = $request->descp;
      $model->updated_at = date("Y-m-d H:i:s");
      if ($model->update()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveCustomField($model,$request);
        saveModuleManagerField($model,$request);
        saveWorlflowField($model,$request);
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.lead.saved'),]]);
        }else{
          return redirect('lead')->with('success', __('app.lead.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.lead.notsaved'),]]);
        }else{
          return redirect('lead')->with('error', __('app.lead.notsaved'));
        }
      }
    }

    return view('lead.update', [
      'model'=>$model,
    ]);

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function import(Request $request)
  {
    $model = new LeadImportForm;
    if($request->isMethod('post')){
      $validator = Validator::make($request->all(), [
      ]);
      $moduleTypeId = $this->newModel()->moduleTypeId;
      $customFields = getCustomFieldsByModule($moduleTypeId);
      $serviceTypeListId = getSetting('service_list_id');

      $csvFile = temp_upload_path().'/'.$request->csv_file_name;
      $handle = fopen($csvFile, "r");

      $crequest = [];//$request;

      $i=1;
      while (($fileop = fgetcsv($handle, 1000, ",")) !== false){
        if($i>1){
          $prospect = new Prospect;
          $prospect->save();
          foreach($request->input_map_field['prospect'] as $key=>$val){
            if(isset($request->input_map_field['prospect'][$key])){
              $cprequest['input_field'][$key] = $fileop[$request->input_map_field['prospect'][$key]];
              $prospect->input_field[$key]=$cprequest['input_field'][$key];
            }
          }
          saveCustomField($prospect,$cprequest,'import');
          saveModuleManagerField($prospect,$request);

          $serviceType = $fileop[$request->input_map_field['static']['service_type']];
          $serviceTypeId = getPredefinedListItemByTitle($serviceTypeListId,$serviceType);

          $opportunity = $this->newModel();
          $opportunity->rec_type = 'l';
          $opportunity->title = $fileop[$request->input_map_field['static']['title']];
          $opportunity->service_type = $serviceTypeId;
          $opportunity->expected_close_date = date("Y-m-d",strtotime($fileop[$request->input_map_field['static']['expected_close_date']]));
          $opportunity->quote_amount = $fileop[$request->input_map_field['static']['quote_amount']];
          $opportunity->descp = $fileop[$request->input_map_field['static']['descp']];
          $opportunity->module_type=$prospect->moduleTypeId;
          $opportunity->module_id=$prospect->id;
          if($opportunity->save()){
            if($customFields!=null){
              foreach($customFields as $customField){
                if(isset($request->input_map_field['opportunity'][$customField->id])){
                  $crequest['input_field'][$customField->id] = $fileop[$request->input_map_field['opportunity'][$customField->id]];
                  $opportunity->input_field[$customField->id] = $fileop[$request->input_map_field['opportunity'][$customField->id]];
                }
              }
            }
            saveCustomField($opportunity,$crequest,'import');
            saveModuleManagerField($opportunity,$request);
          }
        }
        $i++;
      }
      die();
      return redirect('lead')->with('success', __('app.lead.imported'));
    }
    return view('lead.import',compact('model'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function upload(Request $request)
  {
    $allowedFileTypes=[
      'csv','CSV'
    ];
    $moduleTypeId = $this->newModel()->moduleTypeId;
    if ($request->hasFile('file')) {
      $image = $request->file('file');
      if (in_array($image->getClientOriginalExtension(),$allowedFileTypes)) {
        $content = file_get_contents($request->file('file'));
        if (preg_match('/\<\?php/i', $content)) {
        }else{
          $csvFileName = generateName().'.'.$image->getClientOriginalExtension();
          $image->move(temp_upload_path(),$csvFileName);
          $columnsArr=[];
          $csvFile = temp_upload_path().'/'.$csvFileName;
          $handle = fopen($csvFile, "r");
          $fileop = fgetcsv($handle, 1000, ",");
          $tableHtml ='<table class="table">';
          $tableHtml.=' <thead>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>Field</td>';
          $tableHtml.='     <td>Map With</td>';
          $tableHtml.='   </tr>';
          $tableHtml.=' </thead>';
          $tableHtml.=' <tbody>';
          $selOptsHtml = '';

          $selOptsHtml.='<option value="">'.__('common.select').'</option>';
          foreach ($fileop as $key => $value) {
            $columnsArr[$key]=$value;
            $selOptsHtml.='<option value="'.$key.'">'.$value.'</option>';
          }

          //Opportunity Fields
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.title').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][title]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.service_type').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][service_type]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.expected_close_date').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][expected_close_date]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.quote_amount').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][quote_amount]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';
          $tableHtml.='   <tr>';
          $tableHtml.='     <td>'.__('common.descp').'</td>';
          $tableHtml.='     <td><select name="input_map_field[static][descp]" class="form-control">'.$selOptsHtml.'</td>';
          $tableHtml.='   </tr>';

          //Loop custom fields
          $customFields = getCustomFieldsByModule($moduleTypeId);
          if($customFields!=null){
            foreach($customFields as $customField){
              $tableHtml.='   <tr>';
              $tableHtml.='     <td>'.$customField->title.'</td>';
              $tableHtml.='     <td><select name="input_map_field[opportunity]['.$customField->id.']" class="form-control">'.$selOptsHtml.'</td>';
              $tableHtml.='   </tr>';
            }
          }

          //Loop custom fields
          $customFields = getCustomFieldsByModule('prospect');
          if($customFields!=null){
            foreach($customFields as $customField){
              $tableHtml.='   <tr>';
              $tableHtml.='     <td>'.$customField->title.'</td>';
              $tableHtml.='     <td><select name="input_map_field[prospect]['.$customField->id.']" class="form-control">'.$selOptsHtml.'</td>';
              $tableHtml.='   </tr>';
            }
          }

          $tableHtml.=' </tbody>';
          $tableHtml.='</table>';
          return new JsonResponse([
            'success'=>[
              'name'=>$csvFileName,
              'columns'=>$columnsArr,
              'tableHtml'=>$tableHtml,
            ]
          ]);
        }
      }
    }
    exit;
  }

  /**
  * Create a model
  *
  * @param  \App\Models\Opportunity  $opportunity
  * @return \Illuminate\Http\Response
  */
  public function newModel()
  {
    return new Opportunity;
  }

  /**
  * Find a model based on the primary key
  *
  * @param  \App\Models\Opportunity  $predefinedList
  * @return \Illuminate\Http\Response
  */
  public function findModel($id)
  {
    return Opportunity::where('id', $id)->first();
  }
}

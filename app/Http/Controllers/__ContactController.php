<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Company;
use App\Models\CustomFieldData;
use App\Models\MemberContacts;
use App\Rules\DuplicateUserCheck;

class ContactController extends Controller
{
  public $folderName='contact';
  public $controllerId='contact';
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index(Request $request)
{
  $moduleTypeId = $this->newModel()->moduleTypeId;
  return view($this->folderName.'.index',compact('moduleTypeId','request'));
}

/**
* returns data for datatable
*
* @return \Illuminate\Http\Response
*/
public function datatableData(Request $request)
{
  $moduleTypeId = $this->newModel()->moduleTypeId;
  $controllerId = $this->controllerId;
  $a=[];


  $query = DB::table($this->newModel()->getTable())->where(['user_type'=>0])->whereNull('deleted_at');

  if ($request->has('id')) {
    $id = (int)$request->input('id');
    $coms = getCompanyUsersListQuery($id,'contact');
    foreach ($coms as $key => $value) {
      $a[]=$value['id'];
    }
    $query->whereIn('id', $a);
  }


  permissionListTypeFilter($moduleTypeId,$this->newModel()->getTable(),$controllerId,$query);

  if($request->search['value']!=''){
    $keyword = $request->search['value'];
    $srchFlds[]='reference_no';
    $srchFlds[]='name';
    $srchFlds[]='email';
    $srchFlds[]='phone';
    searchInCustomFields($moduleTypeId,$query,$keyword,$srchFlds);
  }

  if($request->created_at!=''){
    if(strpos($request->created_at," - ")){
      $query->where(function($query) use($request){
        list($start_date,$end_date)=explode(" - ",$request->created_at);
        $query->where('created_at','>=',$start_date)
        ->where('created_at','<=',$end_date);
      });
    }else{
      $query->where('created_at',$request->created_at);
    }
  }
  if($request->reference_no!='')$query->where('reference_no','like','%'.$request->reference_no.'%');
  if($request->name!='')$query->where('name','like','%'.$request->name.'%');
  if($request->company_name!=''){
    $companyName=$request->company_name;
    $query = searchInMultipleCompanies($query,$moduleTypeId,$companyName);
  };
  if($request->email!='')$query->where('email','like','%'.$request->email.'%');
  if($request->phone!='')$query->where('phone','like','%'.$request->phone.'%');
  if($request->input_field!=''){
    advanceSearchInCustomFields($moduleTypeId,$query,$request->input_field);
  }

  $countQuery = clone $query;

  $totalRecords = $countQuery->count('id');

  $gridViewColumns=getGridViewColumns($moduleTypeId);

  $orderBy = 'id';
  if($request->ob)$orderBy = $request->ob;
  $orderByOrder = 'desc';
  if($request->obo)$orderByOrder = $request->obo;
  $models = $query->offset($request->start)
  ->limit($request->length)
  ->orderBy($orderBy,$orderByOrder)
  ->get();
  $dataArr=[];
  if($models!=null){
    foreach($models as $model){
      $thisData=[];
//Colors column
      $thisData['cb_col']='';

      $colorArr = getActivityColors($moduleTypeId,$model->id);
      $thisData['cs_col']=implode("",$colorArr);

      $thisData['id']=$model->id;
      $thisData['reference_no']=$model->reference_no;
      $thisData['name']=$model->name;
      $companyNames = getRowMultipleCompanies($moduleTypeId,$model->id);
      $thisData['company_name']=$companyNames;
      $thisData['email']=$model->email;
      $thisData['phone']=$model->phone;

// if($gridViewColumns!=null){
//   foreach($gridViewColumns as $gridViewColumn){
//     $thisData['col_'.$gridViewColumn['id']]=getGridValue($moduleTypeId,$model,$gridViewColumn);
//   }
// }
      $thisData['created_at']=formatDateTime($model->created_at);
      $createdBy = getUserInfo($model->created_by);
      $thisData['created_by']=$createdBy->fullname;
      $actBtns=[];
      if(checkActionAllowed('create','opportunity')){
        $actBtns[]='
        <a href="'.url('/opportunity/create-for',['module'=>$moduleTypeId,'id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon poplink" data-heading="'.__('app.general.create_opportunity').'" data-toggle="tooltip" title="'.__('app.general.create_opportunity').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-add-circular-button"></i>
        </a>';
      }
      if(checkActionAllowed('view')){
        $actBtns[]='
        <a href="'.url('/'.$controllerId.'/view',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon view" data-toggle="tooltip" title="'.__('common.view').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-file-2"></i>
        </a>';
      }
      if(checkActionAllowed('update')){
        $actBtns[]='
        <a href="'.url('/'.$controllerId.'/update',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="'.__('common.update').'" data-id="'.$model->id.'">
        <i class="text-dark-50 flaticon-edit"></i>
        </a>';
      }
      if(checkActionAllowed('delete')){
        $actBtns[]='
        <a href="'.url('/'.$controllerId.'/delete',['id'=>$model->id]).'" class="btn btn-sm btn-clean btn-icon act-confirmation" data-toggle="tooltip" title="'.__('common.delete').'" data-id="'.$model->id.'" data-confirmationmsg="'.__('common.deleteconfirmation').'" data-pjxcntr="grid-table">
        <i class="text-dark-50 flaticon-delete"></i>
        </a>';
      }
      $thisData['action_col']=getDatatableActionTemplate($actBtns);
      $dataArr[]=$thisData;
    }
  }
  $dtInputColsArr = [];
// $dtInputColsArr[]=['data'=>'id','name'=>'ID','searchable'=>true,'orderable'=>true];
// $dtInputColsArr[]=['data'=>'reference_no','name'=>'reference_no','searchable'=>true,'orderable'=>true];
  if($gridViewColumns!=null){
    foreach($gridViewColumns as $gridViewColumn){
      $dtInputColsArr[]=['data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id'],'searchable'=>true,'orderable'=>true];
    }
  }
  $dtInputColsArr[]=['data'=>'action_col','name'=>'action_col','searchable'=>false,'orderable'=>false];
  $inputArr = [
    'columns' => $dtInputColsArr,
    'draw' => (int)$request->draw,
    'length' => (int)$request->length,
    'order' => [
      ['column'=>0,'dir'=>'asc'],
    ],
    'search' => ['value'=>$request->search['value'],'regex'=>false],
    'start' => $request->start,
  ];

  $response = [
    'draw' => (int)$request->draw,
    'input' => $inputArr,
    'recordsTotal' => $totalRecords,
    'recordsFiltered' => $totalRecords,
    'data' => $dataArr,
  ];
  return new JsonResponse($response);
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create(Request $request)
{
  $model = $this->newModel();
  if($request->input('pid')!=''){
    $model->prospect_id = $request->input('pid');
    assignProspectInfo($model,$request->input('pid'));
  }
  $duplicateModel = null;
  $di = $request->di ?? 0;
  if($di>0){
    $duplicateModel = $this->findModel($di);
  }
  if($request->isMethod('post')){
    $emailsArr = $request->emails;
    $emailsArr[] = $request->email;
    $numbersArr = $request->phone_numbers;
    $numbersArr[] = $request->phone;
    $crequest=['id'=>$model->id,'emails'=>$emailsArr,'numbers'=>$numbersArr];

    $duplicateModel = getUserDuplicateRow($model->moduleTypeId,$crequest);

    $validator = Validator::make($request->all(), [
      'full_name'=>'required|string',
      'email'=>'string|email',
      'emails.*'=>'string|email',
      'phone'=>'string',
      'duplicatecheck'=>new DuplicateUserCheck($model->moduleTypeId,$crequest)
    ]);
    if(!$validator->fails()){
      $diId = 0;
//
// if($diId>0){
//   $model = $this->findModel($diId);
// }else{
      $model= $this->newModel();
// }
      $model->name = trim($request->full_name);
      $model->email = trim($request->email);
      $model->phone = trim($request->phone);
      if(isset($request->pid) && $request->pid>0){
        $model->prospect_id = $request->pid;
      }
      $model->permission_group_id = getSetting('clients_permission_group');
      if ($model->save()) {
        if($request->input_field!=null){
          foreach($request->input_field as $key=>$val){
            $model->input_field[$key]=$val;
          }
        }
        saveMultipleCompany($model,$request,'form');
        saveMultipleEmailNumber($model,$request,'form');
        saveCustomField($model,$request,'form');
        saveTags($model,$request,'form');
        saveModuleManagerField($model,$request,'form');

// die();
        if($request->ajax()){
          return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.contact.saved')]]);
        }else{
          return redirect($this->controllerId)->with('success', __('app.contact.saved'));
        }
      }else{
        if($request->ajax()){
          return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.contact.notsaved'),]]);
        }else{
          return redirect($this->controllerId)->with('error', __('app.contact.notsaved'));
        }
      }
    }else{
      $errors = $validator->errors();
      if($errors->first('duplicatecheck')){
        return redirect($this->controllerId.'/create?pid='.$request->pid.'&di='.$duplicateModel->id)->withErrors($validator)->withInput($request->all());
      }else{
        return redirect($this->controllerId.'/create')->withErrors($validator)->withInput($request->all());
      }
    }
  }
  return view($this->folderName.'.create',compact('model','duplicateModel','di'));
}

/**
* Show the form for editing the specified resource.
*
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
  $model = $this->findModel($id);

  if($request->isMethod('post')){
    $validator = Validator::make($request->all(), [
      'full_name'=>'string',
      'email'=>'string|email',
      'phone'=>'string',
    ]);

    $model= $this->findModel($request->id);
    $model->name = trim($request->full_name);
    $model->email = trim($request->email);
    $model->phone = trim($request->phone);
    $model->updated_at = date("Y-m-d H:i:s");
    if ($model->update()) {
      if($request->input_field!=null){
        foreach($request->input_field as $key=>$val){
          $model->input_field[$key]=$val;
        }
      }
      saveMultipleCompany($model,$request,'form');
      saveMultipleEmailNumber($model,$request,'form');
      saveCustomField($model,$request);
      saveTags($model,$request,'form');
      saveModuleManagerField($model,$request);
      if($request->ajax()){
        return new JsonResponse(['success'=>['heading'=>__('common.success'),'msg'=>__('app.contact.saved'),]]);
      }else{
        return redirect($this->controllerId)->with('success', __('app.contact.saved'));
      }
    }else{
      if($request->ajax()){
        return new JsonResponse(['error'=>['heading'=>__('common.error'),'msg'=>__('app.contact.notsaved'),]]);
      }else{
        return redirect($this->controllerId)->with('error', __('app.contact.notsaved'));
      }
    }
  }

  return view($this->folderName.'.update', [
    'model'=>$model,
  ]);

}

/**
* Create a model
*
* @return \Illuminate\Http\Response
*/
public function newModel()
{
  return new Contact;
}

/**
* Find a model based on the primary key
*
* @return \Illuminate\Http\Response
*/
public function findModel($id)
{
  return Contact::where('id', $id)->first();
}
public function show(Request $request, $id)
{


  $model=Contact::where('id',$id)->first();
  $moduleTypeId = $this->newModel()->moduleTypeId;

// $membershipDocument = MembershipDocuments::where('member_id',$id)->first();
// $invoices = Invoice::where('company_id',$model->company_id)->get()->toArray();
// $payments = Payment::where('company_id',$model->company_id)->get()->toArray();
// $subscriptions = Subscription::where('company_id',$model->company_id)->get()->toArray();
//

  return view('contact.view',[
    'model'=>$model,
    'moduleTypeId'=>$moduleTypeId,
// 'membershipDocument'=>$membershipDocument,
// 'invoices'=>$invoices,
// 'payments'=>$payments,
// 'subscriptions'=>$subscriptions,
    'request'=>$request,

  ]);

}





}

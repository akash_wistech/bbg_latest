<?php

namespace App\Http\Controllers;

use App\Models\MediaManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Session;


class MediaManagerController extends Controller
{
    // protected $paginationTheme = 'bootstrap';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $parent_id=null, $target=null, $thumb=null)
    {
        $refreshUrl = \Request::getRequestUri();
        $viewData = $this->prepareData($request);
        // echo "<pre>"; print_r($viewData); echo "</pre>"; die();
        echo view('media-manager.modal-box',compact('viewData','refreshUrl'));
        
        
    }

    public function prepareData($request)
    {
        //  $url = \Request::getRequestUri();
        // echo "<pre>"; print_r($url); echo "</pre>"; die();
        if ($request['page']!='') {
            $params['page'] = $request['page'];
        }
        if ($request['parent_id']!='') {
            $params['parent_id'] = $request['parent_id'];
        }
        if ($request['name']) {
            $params['name'] = $request['name'];
        }
        if ($request['target']) {
            $params['target'] = $request['target'];
        }
        if ($request['thumb']) {
            $params['thumb'] = $request['thumb'];
        }
        // echo "<pre>"; print_r($params); echo "</pre>"; die();
        $dataProvider = $this->search($params);
        // echo "<pre>"; print_r($dataProvider); echo "</pre>"; die();
        $data['parent'] = 0;
        $data['target'] = null;
        $data['thumb'] = null;

        if ($request['target']) {
            $data['target'] = $request['target'];
        }

        if ($request['thumb']) {
            $data['thumb'] = $request['thumb'];
        }

        $viewData = [
            'data' => $data,
            'dataProvider' => $dataProvider,
        ];

        return $viewData;





    }

    public function search($params=null)
    {
        // echo "<pre>"; print_r($params); echo "</pre>"; die();
        $parent_id = 0;
        $page='';
        $folder_name ='';
        $name = '';

        if (isset($params['page']) && $params['page']!='') {
            $page=$params['page'];
            if(Session::get('parent_id')){
                $parent_id=Session::get('parent_id');
            }
        }
        if (isset($params['parent_id']) && $params['parent_id']>0) {
            $parent_id =  $params['parent_id'];
            Session::put('parent_id', $parent_id);
            $findFolderName = DB::table('media_managers')
            ->where('id', '=',  ($parent_id) ? $parent_id : 0)
            ->first();
            // dd($findFolderName->name);
            $folder_name = $findFolderName->name;
        }else{
            Session::put('parent_id', 0);
        }
        if (isset($params['name']) && $params['name']!='') {
            $name = $params['name'];
        }
        $query = DB::table('media_managers')
        ->where('parent_id', '=',  ($parent_id) ? $parent_id : 0)
        ->where('name','like','%'.$name.'%')
        ->orderBy('type','asc')->paginate(15);
        $data = [
            'query' => $query,
            'parent_id' => $parent_id,
            'name' => $name,
            'folder_name' => $folder_name,
        ];
        Session::put('parent_id', $parent_id);
        return $data;
    }


    public function upload(Request $request)
    {
        // echo $request['parent_id']; die(); 
        $json = [];
        // Make sure we have the correct directory
        if ($request['parent_id'] && $request['parent_id']>0) { 
            $currentDir = MediaManager::find($request['parent_id']);
            // echo "greater than zero"; // echo "<pre>"; print_r($currentDir); echo "</pre>"; die();
            $parent_id = $currentDir->id;
            $path = $currentDir->path . '/';
        } else {
            // echo "less than zero"; die();
            $directory = null;
            $parent_id = 0;
            $path = '';
        }

        if (!$json) {
           $file = $request->file('file'); 
           // print_r($file->getClientOriginalName()); die(); 
           if($request->file->getSize() > 1048576){
            $json['error'] = 'Uploading file size is too large (Max 1MB)';
        }

        if (!empty($file->getClientOriginalName())) {

            $filename = basename(html_entity_decode($file->getClientOriginalName(), ENT_QUOTES, 'UTF-8'));
            // echo $filename; die();
            $extension = $file->getClientOriginalExtension();
            // echo $extension; die();
            $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // echo $name; die();

        }

    }

    if (!$json) {
        // echo "hello not json"; die();

        $uniqueName = trim((str_replace(' ', '', $name) . uniqid()));
        $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' . $extension;
        // $uploadObject = Yii::$app->get('s3bucket')->upload($path . $randomName, $file->tempName);
        $uploadObject = $file->move(public_path('images'), $randomName);

        $link = request()->getHost();
        
        $href = "http://".$link."/images/".$randomName;
        // dd($href);

        if ($uploadObject) {

                // check if CDN host is available then upload and get cdn URL.

            // if (Yii::$app->get('s3bucket')->cdnHostname) {
            //     $url = Yii::$app->get('s3bucket')->getCdnUrl($path . $randomName);
            // } else {
            //     $url = Yii::$app->get('s3bucket')->getUrl();
            // }

            // Save Data into Database
            $data = [
                'name' => $file->getClientOriginalName(), 
                'parent_id' => $parent_id, 
                'type' => 'file', 
                'href' => $href,//'https://d2a5i90cjeax12.cloudfront.net/event-images/BBG_GOLF2021610bb05207e59.jpg', 
                'path' => $path . $randomName,
            ];
            $this->saveObject($data);
            $json['success'] = "File Uploaded Successfully";
        } else {
            $json['error'] = "Something Went Wrong";
        }
    }
    return new JsonResponse($json);


}




public function folder(Request $request)
{

       // echo "<pre>"; print_r($request->all()); echo "</pre>";
    $json = [];
        // Make sure we have the correct directory
    $parent_id = 0;
       // echo $request->data['folder'];echo "<br>";
        // echo "parent_id: ".$request['parent_id'];
        // die();

    if ($request['parent_id']) {
        $parent_id = $request['parent_id'];
    }
        // Sanitize the folder name
    $folder = str_replace(['../', '..\\', '..'], '', basename(html_entity_decode($request->data['folder'], ENT_QUOTES, 'UTF-8')));
        // print_r($folder); die();

        // Validate the filename length
        // if ((Utf8Helper::utf8_strlen($folder) < 3) || (Utf8Helper::utf8_strlen($folder) > 128)) {
        //     $json['error'] = "Something Went Wrong";
        // }

    $isExist = DB::table('media_managers')
    ->where('parent_id', '=', $parent_id)
    ->where('name', '=', $folder)
    ->first();
        // echo "isExist: "; print_r($isExist); die();

        // Check if directory already exists or not
    if ($isExist !== null) {
        $json['error'] = "Something Went Wrong";
    }else{
    }

    if (!$json) {
            // echo"parent_id in json".$parent_id; die();
        $parent = MediaManager::find($parent_id);
            // echo "<pre>"; print_r($parent); echo "</pre>"; die();

        $path = $folder;
        if ($parent !== null) {
            $path = $parent->path . '/' . $folder;
        }

            // Save Data into Database
        $data = ['name' => $folder, 'parent_id' => $parent_id, 'type' => 'folder', 'path' => $path];
        $this->saveObject($data);

        $json['success'] = "Created SuccessFully";
    }

    return new JsonResponse($json);
}


public function saveObject($data)
{
        // echo "<pre>"; print_r($data); echo "</pre>"; die();

    $folderObj = new MediaManager();
    $folderObj->parent_id = $data['parent_id'];
    $folderObj->type = $data['type'];
    $folderObj->name = $data['name'];
    $folderObj->path = $data['path'];
    $folderObj->href = isset($data['href']) && $data['href']!='' ? $data['href'] : '' ;

    return $folderObj->save();
}



public function delete(Request $request)
{
    // print_r($request['path']); die();
    $json = [];

    if ($request['path']) {
        $paths = $request['path'];
    } else {
        $paths = [];
    }

    if (!$json) {
            // Loop through each path
        foreach ($paths as $path) {
            $mediaObject = MediaManager::find($path);
            // echo "<pre>"; print_r($mediaObject); echo "</pre>"; die();

                /**
                 * If path is just a file delete it also delete it from AWS
                 * */
                if ($mediaObject->type == 'file') {
                    // Yii::$app->get('s3bucket')->delete($mediaObject->path);
                    $fileObj = $mediaObject->delete();

                    if (!$fileObj) {
                        return $fileObj->getErrors();
                    }
                } elseif ($mediaObject->type == 'folder') {
                    // Yii::$app->get('s3bucket')->delete($mediaObject->path);
                    $mediaObject->deleteRecursive(['files', 'folders']);
                }
            }

            $json['success'] = "Delete Successfully";
        }

        return new JsonResponse($json);




    }


    public function uploadFrontend(Request $request)
    {
        // echo $request['parent_id']; die(); 
        $json = [];

        // Make sure we have the correct directory
        $currentDir = MediaManager::find(193);
        $parent_id = $currentDir->id;
        $path = $currentDir->path . '/';

        if (!$json) {
           $file = $request->file('file'); 
           // print_r($file->getClientOriginalName()); die(); 
           if($request->file->getSize() > 1048576){
            $json['error'] = 'Uploading file size is too large (Max 1MB)';
        }

        if (!empty($file->getClientOriginalName())) {

            $filename = basename(html_entity_decode($file->getClientOriginalName(), ENT_QUOTES, 'UTF-8'));
            // echo $filename; die();
            $extension = $file->getClientOriginalExtension();
            // echo $extension; die();
            $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // echo $name; die();

        }

    }

    if (!$json) {
        // echo "hello not json"; die();

        $uniqueName = trim((str_replace(' ', '', $name) . uniqid()));
        $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' . $extension;
        // $uploadObject = Yii::$app->get('s3bucket')->upload($path . $randomName, $file->tempName);
        $uploadObject = $file->move(public_path('images'), $randomName);

         $link = request()->getHost();
        
        $href = "http://".$link."/images/".$randomName;

        if ($uploadObject) {

                // check if CDN host is available then upload and get cdn URL.

            // if (Yii::$app->get('s3bucket')->cdnHostname) {
            //     $url = Yii::$app->get('s3bucket')->getCdnUrl($path . $randomName);
            // } else {
            //     $url = Yii::$app->get('s3bucket')->getUrl();
            // }

            // Save Data into Database
            $data = [
                'name' => $file->getClientOriginalName(), 
                'parent_id' => $parent_id, 
                'type' => 'file', 
                'href' => $href,//'https://d2a5i90cjeax12.cloudfront.net/event-images/BBG_GOLF2021610bb05207e59.jpg', 
                'path' => $path . $randomName,
            ];
            $this->saveObject($data);
            $json = $data;
            $json['success'] = "File Uploaded Successfully";
        } else {
            $json['error'] = "Something Went Wrong";
        }
    }
    return new JsonResponse($json);
}








}

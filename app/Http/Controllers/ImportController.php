<?php
namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\Company;
use App\Models\Contact;
use App\Models\UserDocuments;
use App\Models\UserExtraFields;

use App\Models\Prospect;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\InvoiceItems;
use Wisdom\Sales\Models\ModuleInvoice;
use Wisdom\Sales\Models\Payments;
use NaeemAwan\Payments\Models\PaytabsTransactionDetail;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use NaeemAwan\ModuleSubscription\Models\MembershipType;
use NaeemAwan\ModuleSubscription\Models\Subscription;
use NaeemAwan\ModuleSubscription\Models\SubscriptionUsers;
use Wisdom\Event\Models\Event;
use Wisdom\Event\Models\EventSubscription;
use Wisdom\News\Models\News;

class ImportController extends Controller
{
  /**
  * Import Companies Data
  *
  * @return \Illuminate\Http\Response
  */
  public function importCompanies(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $oldCompanies = DB::connection('mysql_old')
    ->table('account_company')
    // ->where('id',852)
    ->where('id','>',$lastid)
    ->orderBy('id','asc')
    ->get()->take(25);
    if($oldCompanies->isNotEmpty()){
      foreach($oldCompanies as $oldCompany){
        $lastid = $oldCompany->id;
        //Save Company Info
        $trade_licence = '';
        $active_subscription_id = 0;
        $startDate = '';
        $endDate = '';
        $memberShipTypeId = 0;
        $oldExpiry='';
        $haveIssue = false;

        $company = new Company;
        $company->old_company_id = $oldCompany->id;
        $company->title = $oldCompany->name;
        $company->logo = $oldCompany->logo;
        $company->vat_number = $oldCompany->vat_number;
        $company->visibility = $oldCompany->show_in_directory;
        $company->status = $oldCompany->is_active;
        if($company->save()){
          //Save Custom Fields
          $sectorId = getNewSectorId($oldCompany->category);
          $emiratesId = $oldCompany->emirates_number;//getNewEmiratesId($oldCompany->emirates_number);
          // echo 'Company = '.$company->id;
          // echo 'Sector = '.$sectorId;
          // echo '<br />';
          // echo 'Emirates = '.$emiratesId;
          // echo '<hr />';
          // die();
          if($sectorId>0){
            $company->input_field[1] = [$sectorId];
          }
          $company->input_field[2] = $oldCompany->url;
          $company->input_field[3] = $oldCompany->phonenumber;
          $company->input_field[4] = $oldCompany->fax;
          $company->input_field[5] = $oldCompany->postal_code;
          $company->input_field[6] = $emiratesId;
          $company->input_field[7] = $oldCompany->address;
          $company->input_field[8] = $oldCompany->about_company;
          saveCustomField($company,$request,'form');
          //Get Members and save them
          $oldCompanyMembersCount = DB::connection('mysql_old')
          ->table('accounts')
          ->where('company',$oldCompany->id)->count();
          echo 'Total Member = '.$oldCompanyMembersCount.'<br />';
          //Get Members and save them
          $oldCompanyMembers = DB::connection('mysql_old')
          ->table('accounts')
          ->where('company',$oldCompany->id)
          ->orderBy('id','asc')->get();
          if($oldCompanyMembers->isNotEmpty()){
            $userSubscriptionData = [];
            foreach($oldCompanyMembers as $oldCompanyMember){
              $contact = $this->saveContactInfo($oldCompanyMember,$company,$request);

              $userSubscriptionType = getNewMemberAccountTypeId($oldCompanyMember->account_type);
              if(in_array($userSubscriptionType,getSubscriptionAllowedUserTypes())){
                $userSubscriptionData[$userSubscriptionType][] = $contact->id;
              }

              //If nominee get subscription dates
              if($oldCompanyMember->account_type=='nominee' && $startDate=='' && $endDate==''){
                $startDate = $oldCompanyMember->registeration_date;
                $endDate = $oldCompanyMember->expiry_date;
                $memberShipTypeId = getNewMemberShipTypeId($oldCompanyMember->group_id);
              }
              if($oldCompanyMembersCount==1 && $oldCompanyMember->account_type!='nominee'){
                $startDate = $oldCompanyMember->registeration_date;
                $endDate = $oldCompanyMember->expiry_date;
                $memberShipTypeId = getNewMemberShipTypeId($oldCompanyMember->group_id);
              }
              //Loop to check if memberships have issues.
              if($oldExpiry!='' && $oldExpiry!=$oldCompanyMember->expiry_date){
                $haveIssue=true;
              }
              $oldExpiry = $oldCompanyMember->expiry_date;

              $this->saveContactDocuments($oldCompanyMember,$contact);

              //Get Latest Document for trade license
              if($oldCompanyMember->account_type=='nominee'){
                $oldNomineeDocuments = DB::connection('mysql_old')
                ->table('membership_documents')
                ->where('account_id',$oldCompanyMember->id)
                ->orderBy('id','desc')->get();
                if($oldNomineeDocuments->isNotEmpty()){
                  foreach($oldNomineeDocuments as $oldNomineeDocument){
                    if($oldNomineeDocument->trade_licence!=null && $oldNomineeDocument->trade_licence!='' && $trade_licence==''){
                      $trade_licence = $oldNomineeDocument->trade_licence;
                    }
                  }
                }
              }

              $this->saveFinancialData($oldCompanyMember,$company,$contact);
            }
          }
          //Create Subscription
          if($startDate!='' && $endDate!='' && $memberShipTypeId>0){
            $subscription = $this->saveSubscriptionData($company,$startDate,$endDate,$memberShipTypeId,$userSubscriptionData);
            $active_subscription_id = $subscription->id;
          }

          $company->active_subscription_id = $active_subscription_id;
          $company->trade_licence = $trade_licence;
          if($haveIssue==true){
            $company->have_issue = 1;
          }
          $company->update();
          echo '<hr />';

          DB::connection('mysql_old')->table('account_company')
          ->where('id', $oldCompany->id)->update(['done' => 1]);
        }else{
          echo "Error saving company";
          die();
        }
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-companies').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-individuals').'"</script>';
    }
  }

  /**
  * Import Individuals Data
  *
  * @return \Illuminate\Http\Response
  */
  public function importIndividuals(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $oldIndividualMembers = DB::connection('mysql_old')
    ->table('accounts')
    ->where('company',0)
    ->where('id','>',$lastid)
    ->orderBy('id','asc')
    ->get()->take(25);
    if($oldIndividualMembers->isNotEmpty()){
      foreach($oldIndividualMembers as $oldIndividualMember){
        $lastid = $oldIndividualMember->id;

        $trade_licence = '';
        $active_subscription_id = 0;
        $startDate = '';
        $endDate = '';
        $memberShipTypeId = 0;
        $oldExpiry='';
        $haveIssue = false;

        $company = new Company;
        $company->title = 'N/A';
        $company->visibility = 0;
        $company->status = 1;
        $company->is_dummy = 1;
        if($company->save()){
          $userSubscriptionData = [];
          $contact = $this->saveContactInfo($oldIndividualMember,$company,$request);

          $userSubscriptionData[getNomineeUserTypeId()][] = $contact->id;
          //If nominee get subscription dates
          if($startDate=='' && $endDate==''){
            $startDate = $oldIndividualMember->registeration_date;
            $endDate = $oldIndividualMember->expiry_date;
            $memberShipTypeId = getNewMemberShipTypeId($oldIndividualMember->group_id);
          }
          //Loop to check if memberships have issues.
          if($oldExpiry!='' && $oldExpiry!=$oldIndividualMember->expiry_date){
            $haveIssue=true;
          }
          $oldExpiry = $oldIndividualMember->expiry_date;

          $this->saveContactDocuments($oldIndividualMember,$contact);

          //Get Latest Document for trade license
          $oldNomineeDocuments = DB::connection('mysql_old')
          ->table('membership_documents')
          ->where('account_id',$oldIndividualMember->id)
          ->orderBy('id','desc')->get();
          if($oldNomineeDocuments->isNotEmpty()){
            foreach($oldNomineeDocuments as $oldNomineeDocument){
              if($oldNomineeDocument->trade_licence!=null && $oldNomineeDocument->trade_licence!='' && $trade_licence==''){
                $trade_licence = $oldNomineeDocument->trade_licence;
              }
            }
          }

          $this->saveFinancialData($oldIndividualMember,$company,$contact);

          //Create Subscription
          if($startDate!='' && $endDate!='' && $memberShipTypeId>0){
            $subscription = $this->saveSubscriptionData($company,$startDate,$endDate,$memberShipTypeId,$userSubscriptionData);
            $active_subscription_id = $subscription->id;
          }

          $company->active_subscription_id = $active_subscription_id;
          $company->trade_licence = $trade_licence;
          if($haveIssue==true){
            $company->have_issue = 1;
          }
          $company->update();
        }
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-individuals').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-event-subscriptions').'"</script>';
    }
  }

  /**
  * Import Event Subscriptions
  *
  * @return \Illuminate\Http\Response
  */
  public function importEventSubscriptions(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $events = Event::where('id','>',$lastid)
    ->orderBy('id','asc')
    ->get()->take(25);
    if($events->isNotEmpty()){
      foreach($events as $event){
        $lastid = $event->id;
        //Update Fees
        importEventFees($event->id,'member',$event->member_fee);
        importEventFees($event->id,'guest',$event->nonmember_fee);
        importEventFees($event->id,'committee',$event->committee_fee);
        importEventFees($event->id,'honourary',$event->honourary_fee);
        importEventFees($event->id,'platinum-sponsor',$event->sponsor_fee);
        importEventFees($event->id,'focus-chair',$event->focus_chair_fee);
        importEventFees($event->id,'speaker',0);
        importEventFees($event->id,'speakers-guest',0);
        importEventFees($event->id,'VIP',0);
        importEventFees($event->id,'business-associate',0);
        importEventFees($event->id,'foc-multiplier',0);
        importEventFees($event->id,'foc-other',0);

        //Process guests / subscriptions
        $oldEventSubscriptions = DB::connection('mysql_old')
        ->table('event_subscriptions')
        ->where('event_id',$event->id)
        ->orderBy('id','asc')->get();
        if($oldEventSubscriptions->isNotEmpty()){
          foreach($oldEventSubscriptions as $oldEventSubscription){
            if($oldEventSubscription->user_type=='member'){
              $newUser = Contact::where('old_member_id',$oldEventSubscription->user_id)->first();
              if($newUser!=null){
                $newEventSubscription = new EventSubscription;
                $newEventSubscription->old_user_id = $oldEventSubscription->user_id;
                $newEventSubscription->user_id = $newUser->id;
                $newEventSubscription->company_id = $newUser->main_company_id;
                $newEventSubscription->user_type = $oldEventSubscription->user_type;
                $newEventSubscription->event_id = $oldEventSubscription->event_id;
                $newEventSubscription->fee_paid = $oldEventSubscription->focGuest==1 ? 0 : $oldEventSubscription->fee_paid;
                $newEventSubscription->focGuest = $oldEventSubscription->focGuest;
                $newEventSubscription->diet_option = $oldEventSubscription->diet_option;
                $newEventSubscription->diet_option_description = $oldEventSubscription->diet_option_description;
                $newEventSubscription->other_diet_option_description = $oldEventSubscription->other_diet_option_description;
                $newEventSubscription->vaccinated = $oldEventSubscription->vaccinated;
                $newEventSubscription->user_type_relation = getNewMemberAccountTypeId($oldEventSubscription->user_type_relation);
                $newEventSubscription->vaccinated = $oldEventSubscription->vaccinated;
                $newEventSubscription->walkin = $oldEventSubscription->walkin;
                $newEventSubscription->userNote = $oldEventSubscription->userNote;
                $newEventSubscription->attended = $oldEventSubscription->attended;
                $newEventSubscription->status = $oldEventSubscription->status;
                $newEventSubscription->cancel_request = $oldEventSubscription->cancel_request;
                // $newEventSubscription->registered_by = getNewUserId($oldEventSubscription->registered_by);
                $newEventSubscription->save();

                DB::table($newEventSubscription->getTable())
                ->where('id', $newEventSubscription->id)->update(['created_by'=>$newUser->id]);

                //Check and update invoice link
                $this->saveModuleInvoice($oldEventSubscription->gen_invoice,$newEventSubscription);
              }
            }elseif($oldEventSubscription->user_type=='guest'){
              $prospect = Prospect::where('email',$oldEventSubscription->email)->orWhere('phone',$oldEventSubscription->mobile)->first();
              if($prospect==null){
                $prospect = new Prospect;
              }
              $prospect->email = $oldEventSubscription->email;
              $prospect->phone = $oldEventSubscription->mobile;
              $prospect->full_name = $oldEventSubscription->firstname.' '.$oldEventSubscription->lastname;
              $prospect->company_name = $oldEventSubscription->company;
              $jobTitleId = getNewMemberJobTitleId($oldEventSubscription->designation);
              $prospect->input_field[20] = $jobTitleId;
              if($prospect->save()){
                saveCustomField($prospect,$request,'form');
              }else{
                die("Error, ".$oldEventSubscription->email." Prospect not saved");
              }
              $newEventSubscription = new EventSubscription;
              $newEventSubscription->old_user_id = $oldEventSubscription->user_id;
              $newEventSubscription->user_id = $prospect->id;
              $newEventSubscription->company_id = 0;
              $newEventSubscription->user_type = $oldEventSubscription->user_type;
              $newEventSubscription->event_id = $oldEventSubscription->event_id;
              $newEventSubscription->userNote = $oldEventSubscription->userNote;
              $newEventSubscription->attended = $oldEventSubscription->attended;
              $newEventSubscription->fee_paid = $oldEventSubscription->focGuest==1 ? 0 : $event->nonmember_fee;
              $newEventSubscription->status = $oldEventSubscription->status;
              $newEventSubscription->cancel_request = $oldEventSubscription->cancel_request;
              $newEventSubscription->focGuest = $oldEventSubscription->focGuest;
              $newEventSubscription->user_type_relation = $oldEventSubscription->user_type_relation;
              $newEventSubscription->diet_option = $oldEventSubscription->diet_option;
              $newEventSubscription->diet_option_description = $oldEventSubscription->diet_option_description;
              $newEventSubscription->other_diet_option_description = $oldEventSubscription->other_diet_option_description;
              $newEventSubscription->walkin = $oldEventSubscription->walkin;
              $newEventSubscription->vaccinated = $oldEventSubscription->vaccinated;
              $newEventSubscription->refered_by = getNewUserId($oldEventSubscription->registered_by);
              $newEventSubscription->registered_by = getNewUserId($oldEventSubscription->registered_by);
              $newEventSubscription->save();

              DB::table($newEventSubscription->getTable())
              ->where('id', $newEventSubscription->id)->update(['created_by'=>getNewUserId($oldEventSubscription->registered_by)]);

              //Check and update invoice link
              $this->saveModuleInvoice($oldEventSubscription->gen_invoice,$newEventSubscription);
            }

            DB::connection('mysql_old')->table('event_subscriptions')
            ->where('id', $oldEventSubscription->id)->update(['done' => 1]);
          }
        }


        DB::table('events')
        ->where('id', $event->id)->update(['currency'=>getNewCurrencyId($event->old_currency),'done' => 1]);
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-event-subscriptions').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
      echo '<script>window.location.href="'.url('import-old-member-companies').'"</script>';
    }
  }

  /**
  * Import Member Companies
  *
  * @return \Illuminate\Http\Response
  */
  function importMemberCompanies(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $memberCompanies = DB::connection('mysql_old')->table('member_companies')
    ->where('id','>',$lastid)
    ->orderBy('id','asc')
    ->take(25)->get();
    if($memberCompanies->isNotEmpty()){
      foreach($memberCompanies as $memberCompany){
        $lastid = $memberCompany->id;

        $newUserId = getNewUserId($memberCompany->member_id);
        $newCompanyId = getNewCompanyId($memberCompany->member_company);
        if($newUserId>0 && $newCompanyId>0){
          $alreadyRow = DB::table('member_companies')
          ->where(['member_id'=>$newUserId,'member_company'=>$newCompanyId])
          ->first();
          if($alreadyRow==null){
            DB::table('member_companies')->insert([
                'member_id' => $newUserId,
                'member_company' => $newCompanyId,
                'created_at' => $memberCompany->date_added,
                'updated_at' => $memberCompany->date_added,
            ]);
          }

          DB::connection('mysql_old')->table('member_companies')
          ->where('id', $memberCompany->id)->update(['done' => 1]);
        }
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-member-companies').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
      echo '<script>window.location.href="'.url('import-old-member-contacts').'"</script>';
    }
  }

  /**
  * Import Member Contacts
  *
  * @return \Illuminate\Http\Response
  */
  function importMemberContacts(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $memberContacts = DB::connection('mysql_old')->table('member_contacts')
    ->where('id','>',$lastid)
    ->orderBy('id','asc')
    ->take(25)->get();
    if($memberContacts->isNotEmpty()){
      foreach($memberContacts as $memberContact){
        $lastid = $memberContact->id;

        $newUserId = getNewUserId($memberContact->member_id);
        $newContactId = getNewUserId($memberContact->contact_id);
        if($newUserId>0 && $newContactId>0){
          $alreadyRow = DB::table('member_contacts')
          ->where(['member_id'=>$newUserId,'contact_id'=>$newContactId])
          ->first();
          if($alreadyRow==null){
            DB::table('member_contacts')->insert([
                'member_id' => $newUserId,
                'contact_id' => $newContactId,
                'created_at' => $memberContact->date_added,
                'updated_at' => $memberContact->date_added,
            ]);
          }

          DB::connection('mysql_old')->table('member_contacts')
          ->where('id', $memberContact->id)->update(['done' => 1]);
        }
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-member-contacts').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
      echo '<script>window.location.href="'.url('import-old-member-offers').'"</script>';
    }
  }

  /**
  * Import Member Offers
  *
  * @return \Illuminate\Http\Response
  */
  function importMemberOffers(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $memberOffers = DB::connection('mysql_old')->table('member_offers')
    ->where('id','>',$lastid)
    ->orderBy('id','asc')
    ->take(25)->get();
    if($memberOffers->isNotEmpty()){
      foreach($memberOffers as $memberOffer){
        $lastid = $memberOffer->id;


        $offerRelId = null;
        if($memberOffer->offer_rel=='company'){
          $offerRelId = getNewCompanyId($memberOffer->offer_rel);
        }
        if($memberOffer->offer_rel=='member'){
          $offerRelId = getNewUserId($memberOffer->offer_rel);
        }

        DB::table('member_offers')->insert([
            'category_id' => getNewCategoryId($memberOffer->category_id),
            'title' => $memberOffer->title,
            'image' => $memberOffer->image,
            'short_description' => $memberOffer->short_description,
            'description' => $memberOffer->description,
            'sort_order' => $memberOffer->sort_order,
            'is_active' => $memberOffer->is_active,
            'offer_rel' => $memberOffer->offer_rel,
            'offer_rel_id' => $offerRelId,
            'created_at' => $memberOffer->created_at,
        ]);

        DB::connection('mysql_old')->table('member_offers')
        ->where('id', $memberOffer->id)->update(['done' => 1]);
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-member-offers').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
      echo '<script>window.location.href="'.url('import-old-news').'"</script>';
    }
  }

  /**
  * Import News
  *
  * @return \Illuminate\Http\Response
  */
  function importNews(Request $request)
  {
    $lastid = 0;
    if($request->has('lastid'))$lastid = $request->input('lastid');
    $oldNews = DB::connection('mysql_old')->table('news')
    ->where('id','>',$lastid)
    ->orderBy('id','asc')
    ->take(25)->get();
    if($oldNews->isNotEmpty()){
      foreach($oldNews as $oldNew){
        $lastid = $oldNew->id;

        $news = new News;
        $news->title=$oldNew->title;
        $news->category=getNewNewsCategoryId($oldNew->category);
        $news->image=$oldNew->image;
        $news->short_description=$oldNew->short_description;
        $news->description=$oldNew->description;
        $news->news_type=$oldNew->title;
        $news->title=$oldNew->title;
        $news->image=$oldNew->image;
        $news->link=$oldNew->link;
        $news->isPublished=$oldNew->isPublished;
        $news->homepage_show=$oldNew->homepage_show;
        $news->sort_order=$oldNew->sort_order;
        $news->date=$oldNew->created_at;
        $news->related_to=$oldNew->related_to;

        $newsRelId = 0;
        if($oldNew->related_to=='company'){
          $newsRelId = getNewCompanyId($oldNew->rel_id);
        }
        if($oldNew->related_to=='member'){
          $newsRelId = getNewUserId($oldNew->rel_id);
        }

        $news->rel_id=$newsRelId;
        $news->save();

        DB::connection('mysql_old')->table('news')
        ->where('id', $news->id)
        ->update([
          'created_at'=>$oldNew->created_at,
          'updated_at'=>$oldNew->updated_at
        ]);

        DB::connection('mysql_old')->table('news')
        ->where('id', $oldNew->id)->update(['done' => 1]);
      }
      sleep(2);
      echo '<script>window.location.href="'.url('import-old-news').'?lastid='.$lastid.'"</script>';
    }else{
      echo "<strong>Finished</strong>";
    }
  }

  /**
  * Saving contact main information
  */
  public function saveContactInfo($oldCompanyMember,$company,$request)
  {
    $contact = new Contact;
    $contact->old_member_id = $oldCompanyMember->id;
    $contact->old_parent_id = $oldCompanyMember->parent_id;
    $contact->user_name = $oldCompanyMember->user_name;
    $contact->password_hash = $oldCompanyMember->password_hash;
    $contact->user_type = 0;
    $contact->permission_group_id = getSetting('clients_permission_group');
    $contact->parent_id = 0;
    $contact->name = $oldCompanyMember->first_name.' '.$oldCompanyMember->last_name;
    $contact->email = $oldCompanyMember->email;
    if(validateEmailAddress($oldCompanyMember->secondry_email))$contact->emails[1] = $oldCompanyMember->secondry_email;
    $contact->email = $oldCompanyMember->email;
    $contact->phone = $oldCompanyMember->phone_number;
    $contact->image = $oldCompanyMember->picture;
    $contact->approved = $oldCompanyMember->approved;
    $status=$oldCompanyMember->status;
    if($status==3){  //Deleted
      $contact->deleted_at = date("Y-m-d H:i:s");
      $contact->deleted_by = 1;
    }
    if(in_array($status,[2,4]))$status=1;
    $contact->status = $status;
    $contact->newsletter = $oldCompanyMember->newsletter;
    $contact->referred_by = $oldCompanyMember->referred_by;

    $contact->comp_id[1] = $company->id;
    $contact->comp_name[1] = $company->name;
    $accounttypeId = getNewMemberAccountTypeId($oldCompanyMember->account_type);
    $contact->comp_role_id[1] = $accounttypeId;
    $jobTitleId = getNewMemberJobTitleId($oldCompanyMember->designation);
    $contact->comp_job_title_id[1] = $jobTitleId;
    $contact->save();

    saveMultipleEmailNumber($contact,$request,'form');

    saveMultipleCompanyImport($contact,$request,'form');

    $personalTitleId = getNewPersonalTitleId($oldCompanyMember->title);
    $contact->input_field[9] = $personalTitleId;
    $contact->input_field[10] = getNewGenderId($oldCompanyMember->gender);
    $memberSectorIds = [];
    $mainSectorId = getNewSectorId($oldCompanyMember->user_industry);
    if($mainSectorId>0)$memberSectorIds[]=$mainSectorId;
    $oldMemberSectors = DB::connection('mysql_old')
    ->table('interested_categories')
    ->where('member_id',$oldCompanyMember->id)->get();
    if($oldMemberSectors->isNotEmpty()){
      foreach($oldMemberSectors as $oldMemberSector){
        $oldSectorId = getNewSectorId($oldMemberSector->category_id);
        if($oldSectorId!='' && $oldSectorId>0)$memberSectorIds[]=$oldSectorId;
      }
    }

    $contact->input_field[11] = $memberSectorIds;

    $contact->input_field[12] = $oldCompanyMember->nationality;
    $contact->input_field[13] = getNewCountryCodeId($oldCompanyMember->country_code);
    $contact->input_field[14] = $oldCompanyMember->city;
    $contact->input_field[15] = $oldCompanyMember->vat_number;
    $contact->input_field[16] = getNewHowDidYouHearUsId($oldCompanyMember->howDidYouHear);
    $contact->input_field[17] = $oldCompanyMember->address;
    $contact->input_field[18] = $oldCompanyMember->tellUsAboutYourself;
    $contact->input_field[19] = $oldCompanyMember->purposeOfJoining;
    $contact->social_media = ['linkedin'=>$oldCompanyMember->linkedin,'twitter'=>$oldCompanyMember->twitter];
    saveCustomField($contact,$request,'form');
    saveSocialMediaLinks($contact,$request,'form');

    //Saving Extra Fields
    $contactExtraFields = new UserExtraFields;
    $contactExtraFields->user_id = $contact->id;
    $contactExtraFields->delete_request = $oldCompanyMember->delete_request;
    $contactExtraFields->registeration_date = $oldCompanyMember->registeration_date;
    $contactExtraFields->expiry_date = $oldCompanyMember->expiry_date;
    $contactExtraFields->last_renewal = ($oldCompanyMember->last_renewal=='0000-00-00' ? NULL : $oldCompanyMember->last_renewal);
    $contactExtraFields->user_industry = $oldCompanyMember->user_industry;
    $contactExtraFields->mem_invoice_gen = $oldCompanyMember->mem_invoice_gen;
    $contactExtraFields->renw_invoice_gen = $oldCompanyMember->renw_invoice_gen;
    $contactExtraFields->honourary = $oldCompanyMember->honourary;
    $contactExtraFields->committee = $oldCompanyMember->committee;
    $contactExtraFields->focus_chair = $oldCompanyMember->focus_chair;
    $contactExtraFields->charity = $oldCompanyMember->charity;
    $contactExtraFields->sticky = $oldCompanyMember->sticky;
    $contactExtraFields->sponsor = $oldCompanyMember->sponsor;
    $contactExtraFields->invoice_renewal = $oldCompanyMember->invoice_renewal;
    $contactExtraFields->bbg_membershipid = $oldCompanyMember->bbg_membershipid;
    $contactExtraFields->old_membershipid = $oldCompanyMember->old_membershipid;
    $contactExtraFields->social_type = $oldCompanyMember->socialType;
    $contactExtraFields->social_id = $oldCompanyMember->socialID;
    $contactExtraFields->verification_code = $oldCompanyMember->verification_code;
    $contactExtraFields->phone_verification = $oldCompanyMember->phone_verification;
    $contactExtraFields->payment_cycle = $oldCompanyMember->paymentCycle;
    $contactExtraFields->occasional_updates = $oldCompanyMember->occasional_updates;
    $contactExtraFields->inforamtion_source = $oldCompanyMember->inforamtion_source;
    $contactExtraFields->company = $oldCompanyMember->company;
    $contactExtraFields->group_id = $oldCompanyMember->group_id;
    $contactExtraFields->account_type = $oldCompanyMember->account_type;
    $contactExtraFields->user_type_relation = $oldCompanyMember->user_type_relation;
    $contactExtraFields->status = $oldCompanyMember->status;
    $contactExtraFields->save();
    return $contact;
  }

  /**
  * Saving contact documents
  */
  public function saveContactDocuments($oldCompanyMember,$contact)
  {
    $passportCopyImg = '';
    $residenceVisaImg = '';
    //Saving Documents
    $oldMemberDocuments = DB::connection('mysql_old')
    ->table('membership_documents')
    ->where('account_id',$oldCompanyMember->id)
    ->orderBy('id','asc')->get();
    if($oldMemberDocuments!=null){
      foreach($oldMemberDocuments as $oldMemberDocument){
        $contactDocument = new UserDocuments;
        $contactDocument->user_id = $contact->id;
        $contactDocument->passport_copy = $oldMemberDocument->passport_copy;
        $contactDocument->residence_visa = $oldMemberDocument->residence_visa;
        $contactDocument->passport_size_pic = $oldMemberDocument->passport_size_pic;
        $contactDocument->trade_link_proof = $oldMemberDocument->trade_link_proof;
        $contactDocument->save();
        if($oldMemberDocument->passport_copy!=null && $oldMemberDocument->passport_copy!='')$passportCopyImg = $oldMemberDocument->passport_copy;
        if($oldMemberDocument->residence_visa!=null && $oldMemberDocument->residence_visa!='')$residenceVisaImg = $oldMemberDocument->residence_visa;


        DB::connection('mysql_old')->table('membership_documents')
        ->where('id', $oldMemberDocument->id)->update(['done' => 1]);
      }
    }

    DB::table($contact->getTable())
    ->where('id', $contact->id)
    ->update([
      'passport_copy' => $passportCopyImg,
      'residence_visa' => $residenceVisaImg,
      'created_at' => getFormatedDateTime($oldCompanyMember->created_at),
      'updated_at' => getFormatedDateTime($oldCompanyMember->updated_at)
    ]);
  }

  /**
  * Saving financial data
  */
  public function saveFinancialData($oldCompanyMember,$company,$contact)
  {
    $memberShiptypeNames=getMembershipTypeNames();
    //Get Invoices
    $oldInvoices = DB::connection('mysql_old')
    ->table('invoices')
    ->where('user_id',$oldCompanyMember->id)
    ->orderBy('invoice_id','asc')->get();
    if($oldInvoices->isNotEmpty()){
      foreach($oldInvoices as $oldInvoice){
        $oldInvoiceCreatedAt='';
        $oldInvoiceUpdatedAt='';

        $invoice = new Invoices;
        $invoice->old_invoice_id = $oldInvoice->invoice_id;
        $invoice->company_id = $company->id;
        $invoice->user_id = $contact->id;
        $invoice->invoice_type = getNewInvoiceTypeId($oldInvoice->invoice_related_to);
        $invoice->payment_status = getNewPaymentStatusId($oldInvoice->payment_status);

        if(in_array($oldInvoice->payment_status,['paid','partialy_paid'])){
          $invoice->p_invoice_date = $oldInvoice->invoice_date;
          $invoice->invoice_date = $oldInvoice->invoice_date;
          $invoice->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($invoice->invoice_date))));
        }else{
          $invoice->p_invoice_date = $oldInvoice->invoice_date;
          $invoice->due_date = date("Y-m-d",strtotime("-1 day",strtotime("+1 month",strtotime($invoice->p_invoice_date))));
        }

        // $invoice->sub_total = $oldInvoice->subtotal;
        // $invoice->grand_total = $oldInvoice->total;
        $invoice->admin_note = $oldInvoice->note;
        $invoice->currency_id  = 1 ;
        $invoice->discount_calculation = 'aftertax';
        $invoice->is_recurring  = 0 ;

        $item_type = [];
        $item_id = [];
        $title = [];
        $descp = [];
        $qty = [];
        $rate = [];
        $discount = [];
        $discount_type = [];
        $discount_comments = [];
        $tax_id = [];
        $oldInvoiceItemId = [];
        $oldInvoiceItems = DB::connection('mysql_old')
        ->table('invoice_items')
        ->where('invoice_id',$oldInvoice->invoice_id)
        ->orderBy('id','asc')->get();
        if($oldInvoiceItems->isNotEmpty()){
          foreach($oldInvoiceItems as $oldInvoiceItem){
            echo '<hr>';
            $itemTypeId = getNewInvoiceItemTypeId($oldInvoiceItem->invoice_related_to);
            $item_type[] = $itemTypeId;
            $item_id[] = getNewInvoiceItemId($itemTypeId,$oldInvoiceItem->invoice_category,$oldInvoiceItem->invoice_rel_id);
            $title[] = $oldInvoiceItem->invoice_category;
            $descp[] = '';
            $qty[] = 1;
            $rate[] = $oldInvoiceItem->subtotal;
            $tax_id[] = 1;//5%
            $oldInvoiceItemId[] = $oldInvoiceItem->id;

            $thisItemDiscount=0;
            $thisItemOldId=null;
            $thisDiscountComments='';
            //Renewal negative Adjustment
            $renewalAdjustments = DB::connection('mysql_old')
            ->table('invoice_adjustments')->where('invoice_id',$oldInvoice->invoice_id)->where('type','-')
            ->orderBy('id','asc')->get();
            if($renewalAdjustments->isNotEmpty()){
              foreach($renewalAdjustments as $renewalAdjustment){
                $thisDiscountComments='';
                if($oldInvoiceItem->invoice_related_to=='event'){
                  $thisDiscountComments=$renewalAdjustment->reason;
                  $thisItemDiscount = getPercentageAmount($renewalAdjustment->adjustment,$oldInvoiceItem->subtotal);

                  DB::connection('mysql_old')->table('invoice_adjustments')
                  ->where('id', $renewalAdjustment->id)->update(['done' => 1]);
                }else{
                  // echo $oldInvoiceItem->invoice_category.'';
                  // echo '<pre>';
                  // print_r($memberShiptypeNames);
                  // echo '</pre>';
                  // if(in_array(trim($oldInvoiceItem->invoice_category),$memberShiptypeNames)){
                  //   echo '<br>!!!FOUND!!!<br><br>';
                  // }else{;
                  //   echo '<br>!!!NOT FOUND!!!<br><br>';
                  // }
                  // echo 'Invoice  = '.$oldInvoice->invoice_id.' /  '.$oldInvoiceItem->invoice_category.' && '.$renewalAdjustment->reason;
                  if(
                    (
                      strpos(strtolower($oldInvoiceItem->invoice_category),'joining fee') !== false ||
                      in_array(trim($oldInvoiceItem->invoice_category),getMembershipTypeNames())
                    ) &&
                    (
                      strpos(strtolower($renewalAdjustment->reason),'renewal') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'bcb') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'downgrade') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'downgrading') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'upgrade') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'membership') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'member') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'returning') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'joined fee') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'referral') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'joining') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'vat invoice is incorrect') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'individual rate') !== false ||
                      strpos(strtolower($renewalAdjustment->reason),'offer') !== false/* ||
                      (
                        strpos(strtolower($oldInvoiceItem->invoice_category),'joining fee') !== false &&
                        in_array(trim($renewalAdjustment->reason),getMembershipTypeNames())
                      )*/
                    )
                  ){
                    // echo ' / Discounting<br />';
                    //Its Renewal / Joining discount
                    $thisItemDiscount = getPercentageAmount($renewalAdjustment->adjustment,$oldInvoiceItem->subtotal);

                    $thisDiscountComments=$renewalAdjustment->reason;
                    DB::connection('mysql_old')->table('invoice_adjustments')
                    ->where('id', $renewalAdjustment->id)->update(['done' => 1]);
                  }else{
                    // echo "In here??";
                    if(
                      strpos(strtolower($oldInvoice->invoice_category),'renewal')!==false ||
                      strpos(strtolower($oldInvoice->invoice_category),'registration')!==false
                    ){
                      //Rnewal
                      // echo ' / Discounting<br />';
                      //Its Renewal / Joining discount
                      $thisItemDiscount = getPercentageAmount($renewalAdjustment->adjustment,$oldInvoiceItem->subtotal);

                      $thisDiscountComments=$renewalAdjustment->reason;
                      DB::connection('mysql_old')->table('invoice_adjustments')
                      ->where('id', $renewalAdjustment->id)->update(['done' => 1]);
                    }
                    // echo ' >>> In Else <br />';
                    // if(){
                    //
                    // }
                  }
                }
              }
              $thisItemOldId = $renewalAdjustment->id;
            }

            //Other negative Adjustments
            $discount[] = $thisItemDiscount;
            $discount_type[] = 'percentage';
            $discount_comments[] = $thisDiscountComments;
            $tax_id[] = 1;//5%
            $oldInvoiceItemId[] = $thisItemOldId;

            $oldInvoiceCreatedAt=$oldInvoiceItem->created_date;
            $oldInvoiceUpdatedAt=$oldInvoiceItem->created_date;
            if($oldInvoiceCreatedAt=='' && $oldInvoiceItem->invoice_date!='')$oldInvoiceCreatedAt = $oldInvoiceItem->invoice_date;
            if($oldInvoiceUpdatedAt=='' && $oldInvoiceItem->invoice_date!='')$oldInvoiceUpdatedAt = $oldInvoiceItem->invoice_date;


            DB::connection('mysql_old')->table('invoice_items')
            ->where('id', $oldInvoiceItem->id)->update(['done' => 1]);
          }
        }
        // if($oldInvoice->invoice_id==153)die();
        // if($oldInvoice->invoice_id==3477)die('');

        $positiveAdjustments = DB::connection('mysql_old')
        ->table('invoice_adjustments')
        ->where('invoice_id',$oldInvoice->invoice_id)
        ->where('type','+')
        ->orderBy('id','asc')->get();
        if($positiveAdjustments->isNotEmpty()){
          foreach($positiveAdjustments as $positiveAdjustment){
            $item_type[] = 'addition';//Or Addon
            $item_id[] = 0;
            $title[] = $positiveAdjustment->reason;
            $descp[] = '';
            $qty[] = 1;
            $rate[] = $positiveAdjustment->adjustment;
            $discount[] = 0;
            $discount_type[] = 'percentage';
            $discount_comments[] = '';
            $tax_id[] = 1;//5%
            $oldInvoiceItemId[] = $positiveAdjustment->id;


            DB::connection('mysql_old')->table('invoice_adjustments')
            ->where('id', $positiveAdjustment->id)->update(['done' => 1]);
          }
        }

        $invoice->item_type=$item_type;
        $invoice->item_id=$item_id;
        $invoice->title=$title;
        $invoice->descp=$descp;
        $invoice->qty=$qty;
        $invoice->rate=$rate;
        $invoice->discount=$discount;
        $invoice->discount_type=$discount_type;
        $invoice->discount_comments=$discount_comments;
        $invoice->tax_id=$tax_id;
        $invoice->old_invoice_item_id=$oldInvoiceItemId;

        $invoice->save();
        if($oldInvoiceCreatedAt!='' && $oldInvoiceUpdatedAt!=''){
          DB::table($invoice->getTable())
          ->where('id', $invoice->id)
          ->update([
            'created_at' => $oldInvoiceCreatedAt,
            'updated_at' => $oldInvoiceUpdatedAt
          ]);
        }

        //Get Invoice Payments
        $oldInvoicePayments = DB::connection('mysql_old')
        ->table('payments')
        ->where('invoice_id',$oldInvoice->invoice_id)
        ->orderBy('invoice_id','asc')->get();
        if($oldInvoicePayments->isNotEmpty()){
          foreach($oldInvoicePayments as $oldInvoicePayment){
            $payment = new Payments;
            $payment->old_payment_id = $oldInvoicePayment->id;
            $payment->company_id = $company->id;
            $payment->user_id = $contact->id;
            $payment->amount_received = $oldInvoicePayment->amount;
            $payment->payment_date = getDateOnlyFromTimestamp($oldInvoicePayment->payment_date);
            $payment->payment_method = getNewPaymentMethodId($oldInvoicePayment->payment_method);
            $payment->transaction_id = $oldInvoicePayment->transaction_id;
            $payment->note = $oldInvoicePayment->detail;
            $payment->is_completed = ($oldInvoicePayment->status=='done' ? 1 : 0);

            $payment->module=['invoice'.'-'.$invoice->id];
            $payment->amount_due=['invoice'=>[$invoice->id=>$invoice->grand_total]];
            $payment->discount=['invoice'=>[$invoice->id=>0]];
            $payment->amount_paid=['invoice'=>[$invoice->id=>$oldInvoicePayment->amount]];

            $payment->save();

            if($oldInvoicePayment->payment_method=='online'){
              $responseText=$oldInvoicePayment->response_code;
              $oldPTResponse = DB::connection('mysql_old')
              ->table('payment_responses')
              ->where('invoice_id',$oldInvoice->invoice_id)
              ->orderBy('id','desc')->first();
              if($oldPTResponse!=null){
                $responseText = $oldPTResponse->response_text;
              }
              $paytabsDetail = new PaytabsTransactionDetail;
              $paytabsDetail->payment_id = $payment->id;
              $paytabsDetail->payment_method = $oldInvoicePayment->payment_method;
              $paytabsDetail->payment_status = $oldInvoicePayment->status;
              $paytabsDetail->amount = $oldInvoicePayment->amount;
              $paytabsDetail->response_code = $responseText;
              $paytabsDetail->trans_date = getDateOnlyFromTimestamp($oldInvoicePayment->payment_date);
              $paytabsDetail->transdatetime = $oldInvoicePayment->payment_date;
              $paytabsDetail->transaction_id = $oldInvoicePayment->transaction_id;
              $paytabsDetail->currency = $oldInvoicePayment->currency;

              $paytabsDetail->customer_name = encryptData($oldInvoicePayment->customer_name,'pi'.$payment->id);
              $paytabsDetail->customer_email = encryptData($oldInvoicePayment->email,'pi'.$payment->id);
              $paytabsDetail->customer_phone = encryptData($oldInvoicePayment->phone_num,'pi'.$payment->id);
              $paytabsDetail->first_4_digits = encryptData($oldInvoicePayment->first_4_digits,'pi'.$payment->id);
              $paytabsDetail->last_4_digits = encryptData($oldInvoicePayment->last_4_digits,'pi'.$payment->id);
              $paytabsDetail->card_brand = encryptData($oldInvoicePayment->card_brand,'pi'.$payment->id);

              $paytabsDetail->secure_sign = $oldInvoicePayment->secure_sign;
              $paytabsDetail->save();
            }


            DB::connection('mysql_old')->table('payments')
            ->where('id', $oldInvoicePayment->id)->update(['done' => 1]);
          }
        }else{
          //Save Dummy Payment
          if($invoice->payment_status==1){
            $payment = new Payments;
            $payment->old_payment_id = 0;
            $payment->company_id = $company->id;
            $payment->user_id = $contact->id;
            $payment->amount_received = $invoice->grand_total;
            $payment->payment_date = $invoice->invoice_date;
            $payment->payment_method = 2;
            $payment->transaction_id = '';
            $payment->note = 'No payment found, adding manual entry';
            $payment->is_completed = 1;

            $payment->module=['invoice'.'-'.$invoice->id];
            $payment->amount_due=['invoice'=>[$invoice->id=>$invoice->grand_total]];
            $payment->discount=['invoice'=>[$invoice->id=>0]];
            $payment->amount_paid=['invoice'=>[$invoice->id=>$invoice->grand_total]];

            $payment->save();
          }
        }


        DB::connection('mysql_old')->table('invoices')
        ->where('invoice_id', $oldInvoice->invoice_id)->update(['done' => 1]);
      }
    }
    //Get Payments


    DB::connection('mysql_old')->table('accounts')
    ->where('id', $oldCompanyMember->id)->update(['done' => 1]);
  }

  /**
  * Saving subscription
  */
  public function saveSubscriptionData($company,$startDate,$endDate,$memberShipTypeId,$userSubscriptionData)
  {
    $memberShipTypeRow = MembershipType::where('id',$memberShipTypeId)->first();
    $subscription = new Subscription;
    $subscription->membership_type_id = $memberShipTypeId;
    $subscription->company_id  = $company->id;
    $subscription->start_date = $startDate;
    $subscription->end_date = $endDate;
    $totalMonths = getDataInDates('m',$startDate,$endDate);
    $totalDays = getDataInDates('d',$startDate,$endDate);
    $subscription->no_of_months = round($totalMonths);
    $subscription->no_of_days = round($totalDays);
    $subscription->no_of_nominee = $memberShipTypeRow->no_of_nominee;
    $subscription->no_of_additional = $memberShipTypeRow->no_of_additional;
    $subscription->no_of_named_associate = $memberShipTypeRow->no_of_named_associate;
    $subscription->no_of_alternate = $memberShipTypeRow->no_of_alternate;
    $subscription->no_of_news_per_month = $memberShipTypeRow->no_of_news_per_month;
    $subscription->activated =1;
    $subscription->save();
    //Save users to subscriptions
    if($userSubscriptionData!=null){
      //User Types loop
      foreach($userSubscriptionData as $key=>$typeUsers){
        if($typeUsers!=null){
          foreach($typeUsers as $idx=>$typeUserId){
            $subscriptionUser = new SubscriptionUsers;
            $subscriptionUser->subscription_id = $subscription->id;
            $subscriptionUser->user_type = $key;
            $subscriptionUser->user_id = $typeUserId;
            $userInfo = Contact::where('id',$typeUserId)->withTrashed()->first();
            if($userInfo!=null && $userInfo->deleted_at!=null){
              $subscriptionUser->deleted_at = $userInfo->deleted_at;
            }
            $subscriptionUser->save();

            DB::table((new User)->getTable())
            ->where('id', $typeUserId)->update(['active_subscription_id' => $subscription->id]);
          }
        }
      }
    }
    //Check and link invoice
    $subscriptionInvoice = Invoices::where(['company_id'=>$company->id,'invoice_type'=>1])->orderBy('id','desc')->first();
    if($subscriptionInvoice!=null){
      $invoiceModule = ModuleInvoice::where('module_type',$subscription->moduleTypeId)
      ->where('module_id',$subscription->id)
      ->where('invoice_id',$subscriptionInvoice->id)
      ->first();
      if($invoiceModule==null){
        $invoiceModule = new ModuleInvoice;
        $invoiceModule->module_type = $subscription->moduleTypeId;
        $invoiceModule->module_id = $subscription->id;
        $invoiceModule->invoice_id = $subscriptionInvoice->id;
        $invoiceModule->save();
      }
    }
    return $subscription;
  }

  /**
  * Saving module invoice link
  */
  public function saveModuleInvoice($oldInvoiceId,$module)
  {
    if($oldInvoiceId>0){
      $newInvoice = Invoices::where('old_invoice_id',$oldInvoiceId)->first();
      if($newInvoice!=null){
        $invoiceModule = ModuleInvoice::where('module_type',$module->moduleTypeId)
        ->where('module_id',$module->id)
        ->where('invoice_id',$newInvoice->id)
        ->first();
        if($invoiceModule==null){
          $invoiceModule = new ModuleInvoice;
          $invoiceModule->module_type = $module->moduleTypeId;
          $invoiceModule->module_id = $module->id;
          $invoiceModule->invoice_id = $newInvoice->id;
          $invoiceModule->save();
        }
      }
    }
  }
}

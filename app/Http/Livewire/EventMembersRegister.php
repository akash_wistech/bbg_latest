<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Wisdom\Event\Models\EventSubscription;
use Illuminate\Http\Request;

class EventMembersRegister extends Component
{

  public $model,$event_id,$i;
  public $GuestCount;
  public $html='1';
  public $html2='hello';
  public $inputs=[];
  public $member_id=0;
  public $name,$user_id;
  // public $arr2=array();


  public function mount($model,$event_id){
    $this->model = $model;
    $this->event_id = $event_id;

  

    // dd($this->user_id);

  }

  function getMemberDetailOnCompanyId($as){

    dd($as);
  }



  function GetGuestForm($GuestCount=0){
    $GuestCount = $GuestCount + 1;
    $this->GuestCount = $GuestCount;
    array_push($this->inputs ,$GuestCount);
    $model= new EventSubscription;
    // $this->member_id=0;
    //  if (isset($request->post('data')['member_id']) && $request->post('data')['member_id'] <>null) {
    //   $member_id= $request->post('data')['member_id'];
    // }
  }

  function Remove($GuestCount){
    unset($this->inputs[$GuestCount]);
  }
  private function resetInputFields(){
      $this->name = '';
      // $this->user_id = '';
  }

    public function render()
    {
        return view('livewire.event-members-register',[
          'model'=>$this->model,
          'event_id'=>$this->event_id,
          'GuestCount'=>$this->GuestCount,
          'inputs'=>$this->inputs,
          'member_id'=>$this->member_id,
        ]);
    }
}

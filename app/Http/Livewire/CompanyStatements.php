<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Wisdom\Sales\Models\Invoices;
use Wisdom\Sales\Models\Payments;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CompanyStatements extends Component
{

  public $state=[];
  public $model,$totalInvoiceAmount,$totalPaymentAmount,$TotalBalence,$statements;
  public $type='this_month';
  public $email,$cc;
  public $start_date,$end_date;
  public  $short_description= '
  Dear <b> {company_name} </b>,<br><br>

  Its been a great experience working with you.<br>

  Attached with this email is a list of all transactions for the period between {statement_from} to {statement_to} . <br>

  For your information your account balance due is total: {statement_balance_due}<br>

  Please contact us if you need more information.<br>

  Kind Regards,<br>
  {email_signature}<br>
  ';

  public function openModal()
  {
    $this->dispatchBrowserEvent('show-form');
  }


  public function mount($model){
    $this->model = $model;
    $invoices = $this->getInvoices();
    $payments = $this->getPayments();
    $this->totalInvoiceAmount = $invoices->sum('grand_total');
    $this->totalPaymentAmount = $payments->sum('amount_received');
    $this->TotalBalence = $this->totalInvoiceAmount-$this->totalPaymentAmount;
    if ($this->type=='this_month') {
      $invoices->whereMonth('invoice_date', Carbon::now()->month);
      $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
      $payments->whereMonth('payment_date', Carbon::now()->month);
      $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
      $today = \Carbon\Carbon::now(); //Current Date and Time
      $this->start_date = \Carbon\Carbon::parse($today)->firstOfMonth()->toDateString();
      $this->end_date =    \Carbon\Carbon::parse($today)->endOfMonth()->toDateString();

    }
    $this->statements = $invoices->union($payments)->orderBy('date', 'ASC');
    $this->statements = $this->statements->get()->toArray();

    // dd($this->cc);
    // dd($this->totalInvoiceAmount);
  }

  public function modaldata()
  {

      // dd("gffghfgh");



    $this->short_description=  str_replace("{company_name}",$this->model->title,$this->short_description);
    $this->short_description =  str_replace("{statement_balance_due}",'AED '.$this->TotalBalence,$this->short_description);
    $this->short_description =  str_replace("{statement_from}",$this->start_date,$this->short_description);
    $this->short_description =  str_replace("{statement_to}",$this->end_date,$this->short_description);
    $this->short_description =  str_replace("{email_signature}","British Business Group",$this->short_description);

    // dd($this->short_description);

    $data = [
      'view' => $this->short_description,
      'subject'=> 'BBG Statement Invoice',
      'client_name'=> $this->model->title,
      'email'=> 'mushabab10000@gmail.com',
    ];
    \Mail::send([], [],
      function ($message) use ($data)
      {
        $message
        ->from('mushabab10000@gmail.com','Mushabab Alam')
        ->to($data['email'],$data['client_name'])->subject($data['subject'])->setBody($data['view'], 'text/html');;
      });

    $this->dispatchBrowserEvent('hide-form',['message' => 'Email Send Sussessfully!']);
  }


  public function getInvoices(){
    $invoices = Invoices::where('company_id',$this->model->id)->whereNotNull('invoice_date');
    $invoices->select([
      'id',
      'reference_number',
      DB::raw("'invoice' as type"),
      'invoice_date as date',
      'grand_total as total',
      'due_date'
    ]);
    return $invoices;
  }

  public function getPayments(){
    $payments = Payments::where('company_id',$this->model->id)
    ->select([
      'id',
      DB::raw("'' as reference_number"),
      DB::raw("'payment' as type"),
      'payment_date as date',
      'amount_received as total',
      DB::raw("'' as due_date"),
    ]);
    return $payments;
  }




  public function getInvoiceStatement($type){

    $this->type=$type;
    $invoices = $this->getInvoices();
    $payments = $this->getPayments();
    if ($type=='all') {
      $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
    }
    if ($type=='today') {
      $invoices->whereDate('invoice_date', Carbon::today());
      $payments->whereDate('payment_date', Carbon::today());
      $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
    }
    if ($type=='this_week') {
      $invoices->whereBetween('invoice_date',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
      $payments->whereBetween('payment_date',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
      $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
    }
    if ($type=='this_month') {
        $today = \Carbon\Carbon::now(); //Current Date and Time
        $this->start_date = \Carbon\Carbon::parse($today)->firstOfMonth()->toDateString();
        $this->end_date =    \Carbon\Carbon::parse($today)->endOfMonth()->toDateString();
        $invoices->whereMonth('invoice_date', Carbon::now()->month);
        $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
        $payments->whereMonth('payment_date', Carbon::now()->month);
        $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
        $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
      }
      if ($type=='last_month') {
        $invoices->whereMonth('invoice_date', '=', Carbon::now()->subMonth()->month);
        $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
        $payments->whereMonth('payment_date', '=', Carbon::now()->subMonth()->month);
        $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
        $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
      }
      if ($type=='this_year') {
        $invoices->whereBetween('invoice_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
        $payments->whereBetween('payment_date',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()]);
        $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
      }
      if ($type=='last_year') {
        $invoices->whereYear('invoice_date', now()->subYear()->year);
        $payments->whereYear('payment_date', now()->subYear()->year);
        $this->statements = $payments->union($invoices)->orderBy('date', 'ASC');
      }

      $this->statements = $this->statements->get()->toArray();
    }


    public function render()
    {
      return view('livewire.company-statements',[
        'model'=>$this->model,
        'type'=>$this->type,
        'totalInvoiceAmount'=>$this->totalInvoiceAmount,
        'totalPaymentAmount'=>$this->totalPaymentAmount,
        'TotalBalence'=>$this->TotalBalence,
        'statements'=>$this->statements,
        'email'=>$this->email,
        'cc'=>$this->cc,
        'short_description'=>$this->short_description,
        'start_date'=>$this->start_date,
        'end_date'=>$this->end_date,
      ]);
    }
  }

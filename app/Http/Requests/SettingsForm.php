<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;
use App\Models\Setting;

class SettingsForm
{
  public $grid_page_size,$service_list_id,$contactroles_list_id,$jobroles_list_id,$currency_sign;
  public $module_name_fld,$clients_permission_group;
  public $company_address;
  public $app_name,$app_url,$app_owner,$admin_email,$from_email,$event_email,$membership_email,$app_vat,$telephone,$fax,$address,$location,$about,$invoice_payment_info,$currency ,$cancel_grace_period,$geo_code,$meta_title,$app_logo,$meta_tag,$meta_tag_description,$footer_logo,$copyright_text,$points_on_spent,$points_of_spent,$min_redeem_amount,$smtp_email,$smtp_username,$smtp_password,$smtp_hash,$smtp_port,$youtube,$youtube_icon,$twitter,$twitter_icon,$facebook,$facebook_icon,$linkedin,$linkedin_icon,$pinterest,$pinterest_icon,$instagram,$instagram_icon,$grid_size_width,$grid_size_height,$listing_size_width,$listing_size_height,$additional_image_size_width,$additional_image_size_height,$popup_size_width,$popup_size_height,$page_banner_size_width,$page_banner_size_height,$home_banner_size_width,$home_banner_size_height,$howDidYouHear,$personal_titles;
  public $from,$to,$create_program,$starting_from,$separate_revenue,$current_year_revenue,$next_year_revenue,$create_program_value,$separate_revenue_value,$categories_list_id,$show_in_committe_members,$business_team_list,$member_news_id,$expo_2020_id,$guest_articles_id,$future_vision_collective_list;


  /**
  * save the settings data.
  */
  public function save()
  {
    $post=Request::post();
    foreach($post as $key=>$val){
      if($key!='_token'){
        if($key=='module_name_fld'){
          saveModuleNameSetting($val);
        }else{
          $setting=Setting::where('config_name',$key)->first();
          if($setting==null){
            $setting=new Setting;
            $setting->config_name=$key;
          }
          if(is_array($val)){
            $setting->config_value=implode(",",$val);;
          }else{
            $setting->config_value=$val;
          }
          $setting->save();
        }
      }
    }
    return true;
  }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProspectCollection extends ResourceCollection
{
  protected $ownerUser;

  public function ownerUser($value){
    $this->ownerUser = $value;
    return $this;
  }

  /**
  * Transform the resource collection into an array.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return array
  */
  public function toArray($request)
  {
    return $this->collection->map(function(ProspectResource $resource) use($request){
      return $resource->ownerUser($this->ownerUser)->toArray($request);
    })->all();
  }
}

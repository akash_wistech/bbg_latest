<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ProspectResource extends JsonResource
{
  protected $ownerUser;

  public function ownerUser($value){
    $this->ownerUser = $value;
    return $this;
  }
  /**
  * Transform the resource into an array.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return array
  */
  public function toArray($request)
  {
    $jsonArray = [
      'id'=> $this->id,
      'reference_no'=> $this->reference_no,
      'full_name'=> $this->full_name,
      'email'=> $this->email,
      'phone'=> $this->phone,
      'company_name'=> $this->company_name,
      // $this->mergeWhen($this->ownerUser==$this->sender_id, [
      //   'sender'=>$this->receiver->name,
      // ]),
    ];
    $gridViewColumns=getGridViewColumns($this->moduleTypeId);
    if($gridViewColumns!=null){
      foreach($gridViewColumns as $gridViewColumn){
        $colName = trim(strtolower($gridViewColumn['title']));
        $colName = str_replace(" ","_",$colName);
        $jsonArray[$colName] = getGridValue($this->moduleTypeId,$this,$gridViewColumn);
      }
    }

    $jsonArray['created_at'] = Carbon::parse($this->created_at)->toDateTimeString();
    $jsonArray['updated_at'] = Carbon::parse($this->updated_at)->toDateTimeString();
    return $jsonArray;
  }

  public static function collection($resource){
    return new ProspectCollection($resource);
  }
}

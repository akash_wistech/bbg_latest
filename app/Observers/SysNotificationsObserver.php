<?php
namespace App\Observers;

use App\Models\SysNotifications;
use App\Models\SysNotificationRecipients;
use Illuminate\Support\Facades\DB;

class SysNotificationsObserver
{
  /**
  * Handle the SysNotifications "created" event.
  *
  * @param  \NaeemAwan\SysNotifications\Models\SysNotifications  $sysNotification
  * @return void
  */
  public function created(SysNotifications $sysNotification)
  {
    if ($sysNotification->id) {
      if($sysNotification->user_id!=null){
        foreach($sysNotification->user_id as $key=>$val){
          $checkAlready = SysNotificationRecipients::where(['notification_id'=>$sysNotification->id,'user_id'=>$val]);
          if(!$checkAlready->exists()){
            $recipient = new SysNotificationRecipients;
            $recipient->notification_id = $sysNotification->id;
            $recipient->user_id = $val;
            $recipient->save();
          }
        }
      }
    }
  }

  /**
  * Handle the SysNotifications "updated" event.
  *
  * @param  \App\Models\SysNotifications  $sysNotification
  * @return void
  */
  public function updated(SysNotifications $sysNotification)
  {
    //
    if($sysNotification->user_id!=null){
      foreach($sysNotification->user_id as $key=>$val){
        $checkAlready = SysNotificationRecipients::where(['notification_id'=>$sysNotification->id,'user_id'=>$val]);
        if(!$checkAlready->exists()){
          $recipient = new SysNotificationRecipients;
          $recipient->notification_id = $sysNotification->id;
          $recipient->user_id = $val;
          $recipient->save();
        }
      }
    }
  }

  /**
  * Handle the SysNotifications "deleted" event.
  *
  * @param  \App\Models\SysNotifications  $sysNotification
  * @return void
  */
  public function deleted(SysNotifications $sysNotification)
  {
    //
  }

  /**
  * Handle the SysNotifications "restored" event.
  *
  * @param  \App\Models\SysNotifications  $sysNotification
  * @return void
  */
  public function restored(SysNotifications $sysNotification)
  {
    //
  }

  /**
  * Handle the SysNotifications "force deleted" event.
  *
  * @param  \App\Models\SysNotifications  $sysNotification
  * @return void
  */
  public function forceDeleted(SysNotifications $sysNotification)
  {
    //
  }
}

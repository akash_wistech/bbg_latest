<?php
namespace App\Observers;

use App\Models\Prospect;
use Illuminate\Support\Facades\DB;

class ProspectObserver
{
  /**
  * Handle the Prospect "created" event.
  *
  * @param  \NaeemAwan\Prospects\Models\Prospect  $prospect
  * @return void
  */
  public function created(Prospect $prospect)
  {
    if ($prospect->id) {
      $reference='RD'.'-'.date('y').'-'.sprintf('%03d', $prospect->id);

      DB::table($prospect->getTable())
      ->where('id', $prospect->id)
      ->update(['reference_no' => $reference]);
    }
  }

  /**
  * Handle the Prospect "updated" event.
  *
  * @param  \App\Models\Prospect  $prospect
  * @return void
  */
  public function updated(Prospect $prospect)
  {
    //
  }

  /**
  * Handle the Prospect "deleted" event.
  *
  * @param  \App\Models\Prospect  $prospect
  * @return void
  */
  public function deleted(Prospect $prospect)
  {
    //
  }

  /**
  * Handle the Prospect "restored" event.
  *
  * @param  \App\Models\Prospect  $prospect
  * @return void
  */
  public function restored(Prospect $prospect)
  {
    //
  }

  /**
  * Handle the Prospect "force deleted" event.
  *
  * @param  \App\Models\Prospect  $prospect
  * @return void
  */
  public function forceDeleted(Prospect $prospect)
  {
    //
  }
}

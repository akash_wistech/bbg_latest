<?php
namespace App\Observers;

use App\Models\Contact;
use App\Models\ProspectToUser;
use App\Models\UserDocuments;
use Illuminate\Support\Facades\DB;

class ContactObserver
{
  /**
  * Handle the Contact "created" event.
  *
  * @param  \NaeemAwan\Contacts\Models\Contact  $contact
  * @return void
  */
  public function created(Contact $contact)
  {
    if ($contact->id) {
      if($contact->prospect_id>0){
        $prospectRow = ProspectToUser::where('prospect_id',$contact->prospect_id)->where('user_id',$contact->id)->first();
        if($prospectRow==null){
          $prospectRow=new ProspectToUser;
          $prospectRow->prospect_id = $contact->prospect_id;
          $prospectRow->user_id = $contact->id;
          $prospectRow->save();
        }
      }
      $this->updateDocuments($contact);
      $reference='CT'.'-'.date('y').'-'.sprintf('%03d', $contact->id);

      DB::table($contact->getTable())
      ->where('id', $contact->id)
      ->update(['reference_no' => $reference]);
    }
  }

  /**
  * Handle the Contact "updated" event.
  *
  * @param  \App\Models\Contact  $contact
  * @return void
  */
  public function updated(Contact $contact)
  {
    $this->updateDocuments($contact);
  }

  /**
  * Handle the Contact "deleted" event.
  *
  * @param  \App\Models\Contact  $contact
  * @return void
  */
  public function deleted(Contact $contact)
  {
    //
  }

  /**
  * Handle the Contact "restored" event.
  *
  * @param  \App\Models\Contact  $contact
  * @return void
  */
  public function restored(Contact $contact)
  {
    //
  }

  /**
  * Handle the Contact "force deleted" event.
  *
  * @param  \App\Models\Contact  $contact
  * @return void
  */
  public function forceDeleted(Contact $contact)
  {
    //
  }

  /**
  * Handle the Contact documents
  *
  * @param  \App\Models\Contact  $contact
  * @return void
  */
  public function updateDocuments($contact)
  {
    if(
      $contact->passport_copy!='' ||
      $contact->residence_visa!=''
      // $contact->passport_size_pic!=''
    ){
      $contactDocuments = new UserDocuments;
      $contactDocuments->user_id = $contact->id;
      $contactDocuments->passport_copy = $contact->passport_copy;
      $contactDocuments->residence_visa = $contact->residence_visa;
      // $contactDocuments->passport_size_pic = $contact->passport_size_pic;
      $contactDocuments->save();
    }
  }
}

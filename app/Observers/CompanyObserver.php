<?php
namespace App\Observers;

use App\Models\Company;
use Illuminate\Support\Facades\DB;

class CompanyObserver
{
  /**
  * Handle the Company "created" event.
  *
  * @param  \NaeemAwan\Companys\Models\Company  $company
  * @return void
  */
  public function created(Company $company)
  {
    if ($company->id) {
      $reference='COM'.'-'.date('y').'-'.sprintf('%03d', $company->id);

      DB::table($company->getTable())
      ->where('id', $company->id)
      ->update(['reference_no' => $reference]);
    }
  }

  /**
  * Handle the Company "updated" event.
  *
  * @param  \App\Models\Company  $company
  * @return void
  */
  public function updated(Company $company)
  {
    //
  }

  /**
  * Handle the Company "deleted" event.
  *
  * @param  \App\Models\Company  $company
  * @return void
  */
  public function deleted(Company $company)
  {
    //
  }

  /**
  * Handle the Company "restored" event.
  *
  * @param  \App\Models\Company  $company
  * @return void
  */
  public function restored(Company $company)
  {
    //
  }

  /**
  * Handle the Company "force deleted" event.
  *
  * @param  \App\Models\Company  $company
  * @return void
  */
  public function forceDeleted(Company $company)
  {
    //
  }
}

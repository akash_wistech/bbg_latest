<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\View\Components\DataTableGrid;
use App\View\Components\ColorCodeModal;
use App\View\Components\AdvanceSearchModal;
use App\View\Components\KanbanBoard;
use App\Models\Company;
use App\Observers\CompanyObserver;
use App\Models\Contact;
use App\Observers\ContactObserver;
use App\Models\Prospect;
use App\Observers\ProspectObserver;
use App\Models\Opportunity;
use App\Observers\OpportunityObserver;
use App\Models\SysNotifications;
use App\Observers\SysNotificationsObserver;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
  /**
  * Register any application services.
  *
  * @return void
  */
  public function register()
  {
        // Ondemand Img manupulation
    $this->app->singleton(\League\Glide\Server::class, function($app)
    {
      $filesystem = $app->make(\Illuminate\Contracts\Filesystem\Filesystem::class);

      return \League\Glide\ServerFactory::create([
        'response' => new \League\Glide\Responses\LaravelResponseFactory(app('request')),
        'driver' => config('image.driver'),
        'presets' => config('image.sizes'),
        'source' => $filesystem->getDriver(),
        'cache' => $filesystem->getDriver(),
        'cache_path_prefix' => config('image.cache_dir'),
                    'base_url' => 'image', //Don't change this value
                  ]);
    }
  );
  }

  /**
  * Bootstrap any application services.
  *
  * @return void
  */
  public function boot()
  {
   Paginator::useBootstrap();
   Company::observe(CompanyObserver::class);
   Contact::observe(ContactObserver::class);
   Prospect::observe(ProspectObserver::class);
   Opportunity::observe(OpportunityObserver::class);
   SysNotifications::observe(SysNotificationsObserver::class);
    //
   Blade::component('data-table-grid', DataTableGrid::class);
   Blade::component('kanban-board', KanbanBoard::class);
   Blade::component('color-code-modal', ColorCodeModal::class);
   Blade::component('advance-search-modal', AdvanceSearchModal::class);
 }
}

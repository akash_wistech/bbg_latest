<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ColumnChart extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $series=[];
    public $xaxis=[];
    public $textxaxis='';
    public $text;
    public $color;
    public $width;
    public $year;
    public function __construct($series,$textxaxis,$xaxis,$text,$color,$width,$year)
    {
        $this->series=$series;
        $this->textxaxis=$textxaxis;
        $this->xaxis=$xaxis;
        $this->text=$text;
        $this->color=$color;
        $this->width=$width;
        $this->year=$year;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.column-chart');
    }
}

<?php
namespace App\View\Components;
use Auth;
use Illuminate\View\Component;

class FlotChart extends Component
{
  public $id='';
  public $type='';
  public $heading='';
  public $label='';
  public $data=[];
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($id,$type,$heading='',$label=false,$data)
  {
    $this->id=$id;
    $this->type=$type;
    $this->heading=$heading;
    $this->label=$label;
    $this->data=$data;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $wid='pc'.$this->id;
    $data = $this->data;

    $chartTypeArr = ['pie' => ['show'=>true]];
    if($this->type=='bar'){
      $chartTypeArr = ['bar' => ['show'=>true,'barWidth'=>'0.6','align'=>'center']];

      $barData = [];
      $categoriesArr = [];
      foreach($data as $dataAr){
        $barData[]=[$dataAr['label'],$dataAr['data']];
        $categoriesArr[]=$dataAr['label'];
      }
      $data = [$barData];

      $flotChartOptions['xaxis'] = [
        'mode'=>'categories',
        'categories'=>$categoriesArr,
				// 'showTicks'=>false,
				// 'gridLines'=>false
      ];
      // $flotChartOptions['bars'] = [
      //   'horizontal'=>true,
			// 	'barWidth'=>2,
			// 	'lineWidth'=>0, // in pixels
			// 	'shadowSize'=>0,
			// 	'align'=>'left'
      // ];
      // $flotChartOptions['grid'] = [
      //   'tickColor' => '#eee',
      //   'borderColor' => '#eee',
      //   'borderWidth' => 1,
      // ];

    }

    $flotChartOptions['series'] = $chartTypeArr;

    if($this->type=='pie'){
      if($this->label==true){
        $flotChartOptions['series']['pie']['label']=[
          'show'=>true,
          'radius'=>1,
          'background'=>['opacity'=>'0.8'],
        ];
      }
      $flotChartOptions['legend'] = [
        'show' => false
      ];
    }

    $data = json_encode($data);

    return view('components.flot_chart',[
      'id' => $wid,
      'type' => $this->type,
      'heading' => $this->heading,
      'label' => $this->label,
      'data' => $data,
      'flotChartOptions' => json_encode($flotChartOptions),
    ]);
  }
}

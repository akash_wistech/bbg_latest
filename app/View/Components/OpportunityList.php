<?php
namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Opportunity;
use Illuminate\Support\Facades\DB;

class OpportunityList extends Component
{
  /**
  * model.
  *
  * @var string
  */
  public $model;

  /**
  * Create the component instance.
  *
  * @param  string  $model
  * @return void
  */
  public function __construct($model)
  {
    $this->module_type = $model->moduleTypeId;
    $this->model = $model;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $model = new Opportunity;
    $modelName = (new Opportunity)->getTable();

    $module_type = $moduleTypeId = $model->moduleTypeId;

    $query = DB::table($modelName)->where('module_type',$this->module_type)
    ->where('module_id',$this->model->id)
    ->whereNull('deleted_at');

    permissionListTypeFilter($moduleTypeId,$modelName,'opportunity',$query);

    $countQuery = clone $query;
    $results = $query->orderBy('created_at','desc')->simplePaginate(25);

    $totalCount = $countQuery->count('id');

    return view('components.opportunity-list',compact('module_type','results','totalCount'));
  }
}

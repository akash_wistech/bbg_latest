<?php
namespace App\View\Components;
use Auth;
use Illuminate\View\Component;

class DataTableGrid extends Component
{
  public $saveToList=false;
  public $sid='';
  public $showStats=true;
  public $columnsBtn=true;
  public $request;
  public $moduleTypeId;
  public $advSearch=false;
  public $advSrchColArr=[];
  public $colorCol=false;
  public $exportMenu=false;
  public $cbSelection=false;
  public $jsonUrl;
  public $dtColsArr;
  public $controller;

  public $exportMenuLinks = [
    [
      'extend'=>'print',
      'text'=>'<span class="navi-icon">
      <i class="la la-print"></i>
      </span>
      <span class="navi-text">Print</span>',
      'titleAttr'=> 'Print',
    ],
    [
      'extend'=>'copyHtml5',
      'text'=>'<span class="navi-icon">
      <i class="la la-copy"></i>
      </span>
      <span class="navi-text">Copy</span>',
      'titleAttr'=> 'Copy',
    ],
    [
      'extend'=>'excelHtml5',
      'text'=>'<span class="navi-icon">
      <i class="la la-file-excel-o"></i>
      </span>
      <span class="navi-text">Excel</span>',
      'titleAttr'=> 'Excel',
    ],
    [
      'extend'=>'csvHtml5',
      'text'=>'<span class="navi-icon">
      <i class="la la-file-text-o"></i>
      </span>
      <span class="navi-text">CSV</span>',
      'titleAttr'=> 'CSV',
    ],
    [
      'extend'=>'pdfHtml5',
      'text'=>'<span class="navi-icon">
      <i class="la la-file-pdf-o"></i>
      </span>
      <span class="navi-text">PDF</span>',
      'titleAttr'=> 'PDF',
      'orientation'=> 'landscape',
      'pageSize'=> 'LEGAL'
    ],
  ];
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($sid='',$request,$moduleTypeId,$controller,$jsonUrl,$dtColsArr,$advSrchColArr=[],$showStats=true,$advSearch=false,$exportMenu=false,$colorCol=false,$cbSelection=false,$columnsBtn=true)
  {
    $this->sid=$sid;
    $this->showStats=$showStats;
    $this->controller=$controller;
    $this->request=$request;
    $this->moduleTypeId=$moduleTypeId;
    $this->cbSelection=$cbSelection;
    $this->advSearch=$advSearch;
    $this->advSrchColArr=$advSrchColArr;
    $this->colorCol=$colorCol;
    $this->exportMenu=$exportMenu;
    $this->jsonUrl=$jsonUrl;
    $this->dtColsArr=$dtColsArr;
    $this->columnsBtn=$columnsBtn;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $modelClassList = getModelClass();
    $modelClass = (isset($modelClassList[$this->moduleTypeId]) ? (new $modelClassList[$this->moduleTypeId]) : null);
    if($modelClass!=null && $modelClass->saveToList==true)$this->saveToList=true;

    $dtOptions=[];
    $filterTags = [];
    $finalColsArr = [];
    $buttonsArr=[];
    $showBtns='';


    $staffMembersList = getStaffMemberListArr();

    $userDBVal = Auth::user()->dt_col_visibility;
    $userDBVal = json_decode($userDBVal,true);
    $moduleVisibleColsArr = isset($userDBVal[$this->moduleTypeId]) ? $userDBVal[$this->moduleTypeId] : [];


    if($this->cbSelection==true){
      $finalColsArr[]=[
        'data'=>'cb_col',
        'title'=>'',
        'name'=>'cb_col',
        'visible'=>true,
        'width'=>'50',
      ];
    }

    if($this->dtColsArr!=null){
      foreach($this->dtColsArr as $colArr){
        $visibility = true;
        if($this->columnsBtn!=false && $moduleVisibleColsArr!=null){
          $visibility = ((in_array($colArr['name'],$moduleVisibleColsArr) ? true : false));
        }
        $finalColsArr[]=[
          'data'=>$colArr['data'],
          'title'=>(isset($colArr['title']) ? $colArr['title'] : ''),
          'name'=>$colArr['name'],
          'visible'=>$visibility,
          // 'className'=>($this->columnsBtn==false ? 'noVis' : ''),
          'width'=>(isset($colArr['width']) ? $colArr['width'] : ''),
        ];
      }
    }

    $dtOptions['language'] = [
      'search'=>'',
      'lengthMenu'=>"_MENU_ Per Page",
      'processing'=>"Hello World"
    ];
    $dtOptions['classes'] = ['sLengthSelect'=>'form-control form-control-sm mr-auto'];
    $dtOptions['stateSave'] = true;
    $dtOptions['processing'] = true;
    $dtOptions['serverSide'] = true;
    $dtOptions['ordering'] = false;
    $dtOptions['lengthMenu'] = getPageSizeArr();
    $dtOptions['pageLength'] = getSetting('grid_page_size');
    $dtOptions['columns'] = $finalColsArr;

    if($this->columnsBtn==true){
      $showBtns='B';
      $buttonsArr[]=[
        'extend'=>'colvis',
        'columns'=>':not(.noVis)',
        'collectionLayout'=>'two-column',
        'className'=>'btn btn-sm btn-light-info mx-1',
        'text'=>'Columns'
      ];
    }
    if($this->advSearch==true){
      $showBtns='B';
      $buttonsArr[] = [
        'className'=>'btn btn-sm btn-light-success advance-search-popup mx-1',
        'text'=>'Advance Search'
      ];
    }
    if($this->colorCol==true){
      // $buttonsArr[] = [
      //   'className'=>'btn btn-sm btn-primary colorcodes-popup',
      //   'text'=>'Colors'
      // ];
    }

    if($this->exportMenu==true){
      $buttonsArr[] = [
        'extend'=> 'collection',
        'text'=> '<span class="svg-icon svg-icon-md">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24" />
        <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
        <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
        </g>
        </svg>
        </span>Export',
        'className'=>'font-weight-bold mx-1',
        'buttons'=> $this->exportMenuLinks,
      ];
    }

    $dtOptions['responsive']=true;

    if($this->cbSelection==true){
      if($this->saveToList==true){
        $buttonsArr[] = [
          'extend'=> 'collection',
          'text'=> 'Save to List',
          'className'=>'btn btn-success font-weight-bold mx-1',
          'buttons'=> [
            [
              'text'=>'<li class="dropdown-item py-0">
              <a href="'.url("list/createForm").'" class="poplink" data-heading="Create New List" >
              Create New List
              </a>
              </li>',
              'className'=>'cust1',
            ],
            [
              'text'=>'<li class="dropdown-item py-0">
              <a href="'.url('list/choose').'" class="poplink selected-values" data-heading="Select List">
              Select List
              </a>
              </li>',
            ],
          ]
        ];
      }

      $dtOptions["select"]=['style'=>'multi','selector'=>'td:first-child .checkable'];
      $dtOptions["columnDefs"]=[
        [
          'orderable'=>false,
          'className'=>'noVis select-checkbox',
          'targets'=>0
        ],
        [
          'orderable'=>false,
          'targets'=>-1
        ],
      ];
    }


    $dtOptions['buttons']=$buttonsArr;

    $params = $this->request;
    $oparams = '';
    if($this->jsonUrl!=false){
      $searchParams=[];
      if($this->request->created_at!=''){
        $createdAtVal=[];
        if(strpos($this->request->created_at," - ")){
          list($start_date,$end_date)=explode(" - ",$this->request->created_at);

          $createdAtVal[] = formatDate($start_date).' - '.formatDate($end_date);
        }else{
          $createdAtVal[] = formatDate($this->request->created_at);
        }
        $filterTags['created_at']=['title'=>__('common.created_at'),'value'=>$createdAtVal];
        $searchParams['created_at']=$this->request->created_at;
      }
      if($this->request->created_by!=null){
        $createdByVal=[];
        if(is_array($this->request->created_by)){
          foreach($this->request->created_by as $key=>$val){
            $createdByVal[]= $staffMembersList[$val];
          }
        }else{
          $createdByVal[] = $staffMembersList[$this->request->created_by];
        }
        $filterTags['created_by']=['title'=>__('common.created_by'),'value'=>$createdByVal];
        $searchParams['created_by']=$this->request->created_by;
      }
      if($this->request->assign_to!=null){
        $assignedToVal=[];
        if(is_array($this->request->assign_to)){
          foreach($this->request->assign_to as $key=>$val){
            $assignedToVal[]= $staffMembersList[$val];
          }
        }else{
          $assignedToVal[] = $staffMembersList[$this->request->assign_to];
        }
        $filterTags['assign_to']=['title'=>__('common.assign_to'),'value'=>$assignedToVal];
        $searchParams['assign_to']=$this->request->assign_to;
      }
      if($this->request->reference_no!=''){
        $filterTags['reference_no']=['title'=>__('common.reference_no'),'value'=>$this->request->reference_no];
        $searchParams['reference_no']=$this->request->reference_no;
      }
      if($this->advSrchColArr!=null){
        foreach($this->advSrchColArr as $sk=>$sinputField){
          $nameKey = $sinputField['name'];
          $searchParams[$nameKey]=$this->request->$nameKey;

          $advSrcColVal=[];
          if(isset($this->request->$nameKey) && $this->request->$nameKey!=''){
            if(is_array($this->request->$nameKey)){
              foreach($this->request->$nameKey as $key=>$val){
                if(isset($sinputField['subopts']) && $sinputField['subopts']!=null){
                  $advSrcColVal[]= $sinputField['subopts'][$val];
                }else{
                  $advSrcColVal[]= $val;
                }
              }
            }else{
              if($this->request->$nameKey!=''){
                if(isset($sinputField['subopts']) && $sinputField['subopts']!=null){
                  $advSrcColVal[] = $sinputField['subopts'][$this->request->$nameKey];
                }else{
                  if(isset($sinputField['input_type']) && $sinputField['input_type']=='date'){
                    $advSrcColVal[] = formatDate($this->request->$nameKey);
                  }else{
                    $advSrcColVal[] = $this->request->$nameKey;
                  }
                }
              }
            }
            $filterTags[$nameKey]=['title'=>$sinputField['title'],'value'=>$advSrcColVal];
          }
        }
      }
      if($params['input_field']!=null){
        foreach($params['input_field'] as $key=>$val){
          $searchParams['input_field'][$key]=$val;

          if (function_exists('getCFSearchFilterTags')) {
            $filterTags = getCFSearchFilterTags($key,$val,$filterTags);
          }
        }
      }
      $dtOptions["ajax"]=[
        'url'=>url($this->jsonUrl),
        'data'=>$searchParams
      ];
    }

    $inputFields=getCustomFilterFieldsByModule($this->moduleTypeId);

    $moduleVisibleCols = '[';
    if($moduleVisibleColsArr!=null){
      $n=1;
      foreach($moduleVisibleColsArr as $key=>$val){
        $moduleVisibleCols.=($n>1 ? ',' : '').$val;
        $n++;
      }
    }
    $moduleVisibleCols.=']';

    $dtOptions['dom'] = "<'row'<'col-sm-6'".$showBtns."f><'col-sm-6 text-right'l>><'table-responsive1't><'row'<'col-sm-6'i><'col-sm-6'p>>";

    if($this->showStats==true){

      $condition=[];
      if($this->controller=='opportunity')$condition['rec_type']='o';
      if($this->controller=='lead')$condition['rec_type']='l';
      if($this->controller=='contact')$condition['user_type']=0;

      $statsArr = getModuleStats($this->controller,$this->moduleTypeId,$condition);

      if($this->controller=='company')$statsArr=[];


    }else{
      $statsArr=[];
    }

    return view('components.data-table-grid',[
      'sid' => $this->sid,
      'advSearch' => $this->advSearch,
      'advSrchColArr' => $this->advSrchColArr,
      'colorCol' => $this->colorCol,
      'inputFields' => $inputFields,
      'moduleTypeId' => $this->moduleTypeId,
      'dtOptions' => $dtOptions,
      'searchParams' => $params,
      'moduleVisibleCols' => $moduleVisibleCols,
      'statsArr' => $statsArr,
      'filterTags' => $filterTags,
      'staffMembersList' => $staffMembersList,
    ]);
  }
}

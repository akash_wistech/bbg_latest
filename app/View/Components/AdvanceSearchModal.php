<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AdvanceSearchModal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $request;
    public $advSearch;
    public $advSrchColArr;
    public $sid;
    public $moduleTypeId;
    public $inputFields;
    public $searchParams;
    public $staffMembersList;

    public function __construct($request,$advSearch,$advSrchColArr,$sid,$moduleTypeId,$inputFields,$searchParams)
    {
        $this->request=$request;
        $this->advSearch=$advSearch;
        $this->advSrchColArr=$advSrchColArr;
        $this->sid=$sid;
        $this->moduleTypeId=$moduleTypeId;
        $this->inputFields=$inputFields;
        $this->searchParams=$searchParams;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
      $this->staffMembersList = getStaffMemberListArr();
        return view('components.advance-search-modal');
    }
}

<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PieChart extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $series=[];
    public $labels=[];
    public $id;
    public function __construct($series,$labels,$id)
    {
        //
        $this->series=$series;
        $this->labels=$labels;
        $this->id=$id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.pie-chart');
    }
}

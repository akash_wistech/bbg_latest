<?php
namespace App\View\Components;
use Auth;
use Illuminate\View\Component;
use App\Models\SysNotifications;
use App\Models\SysNotificationRecipients;

class TopBarNotifications extends Component
{
  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $myId = Auth::id();
    $mainTable = (new SysNotifications)->getTable();
    $recipientTable = (new SysNotificationRecipients)->getTable();
    $query = SysNotifications::join($recipientTable, $recipientTable.'.notification_id', '=', $mainTable.'.id')
    ->where($recipientTable.'.user_id',$myId);

    $countQuery = clone $query;
    $results = $query->orderBy('created_at','desc')->simplePaginate(10);

    $unreadCount = $countQuery->where($recipientTable.'.is_read',0)->count('id');

    return view('components.top-bar-notifications',[
      'results'=>$results,
      'unreadCount'=>$unreadCount,
    ]);
  }
}

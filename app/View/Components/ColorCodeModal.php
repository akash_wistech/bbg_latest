<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ColorCodeModal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $colorCol;

    public function __construct($colorCol)
    {
        $this->colorCol=$colorCol;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.color-code-modal');
    }
}

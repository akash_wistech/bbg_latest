<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Contact;
use Auth;

class ContactBanner extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $model = Contact::where(['id'=>Auth::user()->id])->first();
        $no_image = asset('assets/images/dummy-image-1.jpg');
        return view('components.contact-banner',compact('model','no_image'));
    }
}

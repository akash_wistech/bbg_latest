<?php
namespace App\View\Components;
use Auth;
use Illuminate\View\Component;

class AmChart extends Component
{
  public $id='';
  public $type='';
  public $heading='';
  public $label='';
  public $data=[];
  /**
  * Create the component instance.
  *
  * @param  string  $type
  * @param  string  $message
  * @return void
  */
  public function __construct($id,$type,$heading='',$label=false,$data)
  {
    $this->id=$id;
    $this->type=$type;
    $this->heading=$heading;
    $this->label=$label;
    $this->data=$data;
  }

  /**
  * Get the view / contents that represent the component.
  *
  * @return \Illuminate\View\View|\Closure|string
  */
  public function render()
  {
    $wid='pc'.$this->id;
    $data = $this->data;
    $chartOptions['type'] = $this->type;
    $chartOptions['theme'] = 'light';
    $chartOptions['dataProvider'] = $data;

    if($this->type=='serial'){
      $chartOptions['valueAxes'] = [[
          "gridColor"=>"#FFFFFF",
          "gridAlpha"=>0.2,
          "dashLength"=>0
      ]];
      // $chartOptions['gridAboveGraphs'] = true;
      $chartOptions['startDuration'] = 1;
      $chartOptions['graphs'] = [[
        "fillAlphas"=>0.8,
        "lineAlpha"=>0.2,
        "type"=>"column",
        "valueField"=>"data"
      ]];
      $chartOptions['chartCursor'] = [
        "categoryBalloonEnabled"=>false,
        "cursorAlpha"=>0,
        "zoomable"=>false
      ];
      $chartOptions['categoryField'] = "label";

      $chartOptions['categoryAxis'] = [
          "gridPosition"=>"start",
          "gridAlpha"=>0,
          "tickPosition"=>"start",
          "tickLength"=>20
      ];
    }

    if($this->type=='pie'){
      $chartOptions['valueField']='data';
      $chartOptions['titleField']='label';
      $chartOptions['balloon']=['fixedPosition'=>true];
    }

    $chartOptions['export'] = ["enabled"=>false];

    return view('components.am_chart',[
      'id' => $wid,
      'type' => $this->type,
      'heading' => $this->heading,
      'chartOptions' => json_encode($chartOptions),
    ]);
  }
}

@extends('layouts.app')
@section('title', __('app.general.dashboard'))
@section('content')
<div class="row ">


  @if($dashboardStats!=null)
  <div class="col-sm-6 d-none">
    <div class="row">
      @foreach($dashboardStats as $dbsk=>$statItem)
      <div class="col-12 col-sm-4">
        <div class="card card-custom{{(isset($statItem['bgClass']) ? ' '.$statItem['bgClass'] : '')}} gutter-b" style="height: 150px">
          <div class="card-body">
            <span class="svg-icon svg-icon-3x svg-icon-success">
              <i class="{{$statItem['icon']}}"></i>
            </span>
            <div class="text-dark font-weight-bolder font-size-h2 mt-3">{{$statItem['total']}}</div>
            <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">{{$statItem['title']}}</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  @endif
</div>

<?php

$event_count = \Wisdom\Event\Models\Event::whereNull('deleted_at')->count();
$upcoming_event_count = \Wisdom\Event\Models\Event::whereDate('event_startDate', '>', date("Y-m-d"))->whereNull('deleted_at')->count();
$time = strtotime(date("Y/m/d"));
$one_month = date("Y-m-d", strtotime("+1 month", $time));
$upcoming_renewal_count = \NaeemAwan\ModuleSubscription\Models\Subscription::whereDate('end_date', '>', date("Y-m-d"))->whereDate('end_date', '<', $one_month)->whereNull('deleted_at')->count();
// dd($upcoming_renewal_count);


$invoiced = \Wisdom\Sales\Models\Invoices::whereNotNull('sent_date_time')->whereNull('deleted_at')->count();


// dd($invoiced);

 ?>
 <style>
.top-card-hei-m{
height: 115px;
margin-left: 7px;
margin-right: 7px;
 }
 </style>

  <div class="row mt-5 mt-lg-0">
  <!--Top three sections and Event Calendar  -->
  <div class="data-in-row col-lg-7 col-12">
    <!-- Top Three Main Sections for stats -->
    <div class="row px-2">
      <!-- Companies Stats -->
      <div class="card col-md col-xs-12 top-card-hei-m my-md-0 my-3">
        <div class="card-body  px-xl-7 p-4 pt-8 pb-4">
          <div class="media">
            <div class="media-body overflow-hidden">
              <a href="{{ url('subscription') }}" class=" mb-3" style="font-size:14px; color:#9E9E9E">Active Subscription</a>
              <h3 class="mb-0 mt-3">{{ $activeSubscription }}</h3>
            </div>
            <div class="text-primary"><i class="icon-xl la la-building-o icon-2x " style="color:#0277BD"></i></div>
          </div>
        </div>
       </div>
      <!-- Evnets Stats -->
      <div class="card col-md col-xs-12 top-card-hei-m my-md-0 my-3">
        <div class="card-body  px-xl-7 p-4 pt-8 pb-4">
          <div class="media">
            <div class="media-body overflow-hidden">
              <a  href="{{ url('event') }}" class=" mb-3" style="font-size:14px; color:#9E9E9E">Total Events</a>
              <h3 class="mb-0 mt-3">{{ $event_count }}</h3>
            </div>
            <div class="text-primary"><i class="icon-xl la la-calendar icon-2x " style="color:#0277BD"></i></div>
          </div>
        </div>
      </div>
      <!-- Upcoming Evnets Stats -->
      <div class="card col-md col-xs-12 top-card-hei-m my-md-0 my-3">
        <div class="card-body  px-xl-7 p-4 pt-8 pb-4">
          <div class="media">
            <div class="media-body overflow-hidden">
              <a  href="{{ url('event') }}" class=" mb-3" style="font-size:14px; color:#9E9E9E">Upcoming Events</a>
              <h3 class="mb-0 mt-3">{{ $upcoming_event_count }}</h3>
            </div>
            <div class="text-primary"><i class="icon-xl la la-calendar-check icon-2x " style="color:#0277BD"></i></div>
          </div>
        </div>
        </div>
    </div>
    <!--Monthly Revenue of Subscriptions  -->
    <div class="col-12 px-0 mt-4 mb-5" >
     <x-column-chart :series="$payments" :year="'true'" :color="'#AC1EB3'" :width="'55%'" :textxaxis="'Monthly Revenue of Subscriptions'" :xaxis="$pay_months" text="Payments" />

    </div>
  </div>

<!-- MembershipType and Two small section with invoice and upcoming_renewal Stats -->
<div class="col-lg-5 col-12">
  <!--MemberShip Types Pie Chart -->
   <div class="" style="">
     <x-pie-chart :series="$series" :labels="$labels" :id="'MemberShip'"/>
   </div>

  <!--Two Small Section of Stats on the right under MemberShip Types chart  -->
  <div class="row">
    <!--Invoiced Stats Box  -->
    <div class="col-sm-6 col-12">
    <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
    	<div class="card-body">
    		<span class="svg-icon svg-icon-2x svg-icon-info ">
    		<i class="icon-xl la la-archive text-primary"></i>
    		</span>
    		<span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $invoiced }}</span>
    		<a target="_blank" href="{{ url('invoices?type=invoiced') }}" class="font-weight-bold text-muted font-size-sm">Unpaid Invoices</a>
    	</div>
    </div>
    </div>
    <!--Upcoming Renewal  -->
    <div class="col-sm-6 col-12">
    <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(assets/media/svg/shapes/abstract-1.svg)">
    	<div class="card-body">
    		<span class="svg-icon svg-icon-2x svg-icon-info">
    		<i class="icon-xl la la-envelope-open-text text-primary"></i>
    		</span>
    		<span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $upcoming_renewal_count }}</span>
    		<a href="{{ url('subscription?type=upcoming') }}"  class="font-weight-bold text-muted font-size-sm">Upcoming Renewal</a>
    	</div>
    </div>
    </div>
  </div>
  <!-- End of Two small Section of Stats -->
</div>
</div>


















<?php

// use Wisdom\Event\Models\Event;
$events = \Wisdom\Event\Models\Event::whereDate('event_startDate', '>', date("Y-m-d"))->whereNull('deleted_at')->get();

// dd($events);


 ?>


<div class="row mb-5 ">

  <div class="col-lg-4 col-12">

    <div class="row px-md-4 mb-md-0 pb-4">
			<!--begin::List Widget 14-->
			<div class="col-lg-12 col-md-6 px-lg-0 pr-md-4 pl-md-0">
			<div class="card card-custom"  style="min-height:400px">
				<!--begin::Header-->
				<div class="card-header border-0 px-7">
					<h3 class="card-title font-weight-bolder text-dark">Upcoming Events</h3>
				</div>
				<!--end::Header-->
				<!--begin::Body-->
				<div class="card-body pt-2 px-7 pb-0">

<?php



foreach ($events as $key => $event) { ?>
					<!--begin::Item-->
					<div class="d-flex flex-wrap align-items-center mb-10">
						<!--begin::Symbol-->
						<div class="symbol symbol-50 symbol-2by3 flex-shrink-0 mr-4">
							<div class="symbol-label">
                <img src="{{ $event->image }}" alt=""     height='65px'  width='75px'>
              </div>
						</div>
						<!--end::Symbol-->
						<!--begin::Title-->
						<div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3" style="width:200px">
							<a href="{{url('/event/view',['id'=>$event->id])}}" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">{{ $event->title }}</a>
              <span class="text-muted font-weight-bold font-size-sm my-1">{{ formatDate($event->event_startDate).' | '.$event->event_startTime }}</span>
						</div>
						<!--end::Title-->
					</div>
					<!--end::Item-->
        <?php } ?>

				</div>
				<!--end::Body-->
			</div>
			</div>
			<!--end::List Widget 14-->

      <div class="col-xl-12 col-md-6 px-lg-0 pl-md-4 pr-md-0 mt-lg-3 pt-md-0 pt-4">
        <x-column-chart :series="$data" :year="'false'" :color="'#1d355f'" :width="'36px'" :xaxis="$types" :text="'events'" :textxaxis="'Total no of events'"/>
      </div>

		</div>
		</div>

    <div class="col-lg-8">
      <x-actionlog-full-calendar/>
  </div>




</div>

<?php //dd($yearly_payments); ?>

<div class="row px-0 pb-4">


 <div class="col">
<x-column-chart :series="$eventpayments" :year="'true'" :color="'#AC1EB3'" :width="'55%'" :textxaxis="'Monthly Revenue of Events'" :xaxis="$pay_months" text="EventsPayments" />
</div>


</div>


<div class="row">
  <div class="col-md-6 col-12">
 <x-column-chart :series="$yearly_payments" :year="'false'" :color="'#1EB359'" :width="'36px'" :textxaxis="$textxaxis" :xaxis="$years" text="Yearly_Payments" />
</div>
  <div class="col-md-6 col-12 pl-md-0 pt-md-0 pt-4">
    <x-column-chart :series="$barData" :year="'false'" :color="'#1BC5BD'" :width="'55%'" :textxaxis="$textxaxis" :xaxis="$months" text="Invoices" />
  </div>

  </div>





<?php

use Wisdom\Event\Models\Event;

$events = Event::get();




 ?>










 <!-- column chart -->
 <div class=" mt-3 d-none">
   <!--begin::Card-->
   <div class="card card-custom gutter-b col-12">
     <div class="card-body p-0">
       <p class="card-label p-6" style="font-size:18px">Profit Analytics</p>
       <!--begin::Chart-->
       <div id="chart_12" class="d-flex justify-content-center"></div>
       <!--end::Chart-->
     </div>
   </div>
   <!--end::Card-->
 </div>

<!--div class="card card-custom card-stretch gutter-b">
  <div class="card-body pt-2 pb-0 mt-n3">
  </div>
</div-->
@endsection


@push('js')

<script src="{{ asset('assets/js/pages/features/charts/apexcharts.js')}}"></script>



@endpush

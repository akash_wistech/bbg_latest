@extends('layouts.app')
@section('title', __('app.prospect.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'prospect'=>__('app.prospect.heading'),
'prospect/update/'.$model->id=>__('common.update_rec',['name'=>$model->reference_no]),
__('common.view')
]])
@endsection
@section('content')



	<div class="container border rounded mt-4 py-5 px-0">
		<div class="d-flex px-0">
		<div class="col-3 border-right rounded bg-white py-4 px-6">
			<!--begin::Details-->
			<div class="d-flex mt-6">
				<!--begin: Pic-->
				<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
					<div class="symbol symbol-50 symbol-lg-90">
						<img src="{{ ($model->image<>null) ? $model->image : asset('images/default-placeholder.png') }}" alt="image" />
					</div>
					<div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
						<span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
					</div>
				</div>
				<!--end::Pic-->
				<!--begin::Info-->
				<div class="flex-grow-1">
					<!--begin::Title-->
					<div class="d-flex justify-content-between flex-wrap mt-1">
						<div class="d-flex mr-3">
							<p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->name }}</p>
							<?php if($model->status==1){  ?>
							<a href="#">
								<i class="flaticon2-correct text-success font-size-h5"></i>
							</a>
							<?php } ?>
						</div>
					</div>
					<!--end::Title-->
					<!--begin::Content-->
					<div class="d-flex flex-wrap justify-content-between mt-1">
						<div class="d-flex flex-column flex-grow-1 pr-8">
							<div class="d-flex flex-wrap ">

								<?php if (isset($membersCustomFields['Designation']) && $membersCustomFields['Designation']<>null ) { ?>
								<p class="text-dark-50 text-hover-primary font-weight-bold">
								<i class="flaticon2-calendar-3 mr-1"></i>{{ $membersCustomFields['Designation'] }}</p>
								<?php } ?>

							</div>
						</div>
					</div>
					<!--end::Content-->
				</div>
				<!--end::Info-->
			</div>
			<!--end::Details-->


			<div class="pb-7 mt-4">

			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">{{ __('common.reference_no:') }}</span>
				<a href="#" class="text-muted text-hover-primary">{{ $model->reference_no }}</a>
			</div>

      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">{{ __('common.full_name:') }}</span>
				<span class="text-muted">{{ $model->full_name }}</span>
			</div>

			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">{{ __('common.company_name:') }}</span>
				<span class="text-muted">{{ $model->company_name }}</span>
			</div>
      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">{{ __('common.phone:') }}</span>
				<span class="text-muted">{{ $model->phone }}</span>
			</div>
      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">{{ __('common.email:') }}</span>
				<span class="text-muted">{{ $model->email }}</span>
			</div>
			</div>


			<ul class="nav nav-tabs  mb-5  d-flex flex-column navi navi-hover navi-active navi-accent border-bottom-0">
			    <li class="navi-item my-1">
			        <a class="navi-link active" data-toggle="tab" href="#overview_grid">
			            <span class="navi-icon pr-5">
			            <i class="fas fa-info-circle"></i>
			            </span>
			            <span class="navi-text font-size-h6 ">Overview</span>
			        </a>
			    </li>
			     <li class="navi-item my-1">
			        <a class="navi-link" data-toggle="tab" href="#opportunity_grid">
			            <span class="navi-icon pr-4"><i class="fab fa-unity"></i></span>
			            <span class="navi-text font-size-h6 ">Opportunities </span>
			        </a>
			    </li>
          <li class="navi-item my-1">
             <a class="navi-link" data-toggle="tab" href="#attachment_grid">
                 <span class="navi-icon pr-6"><i class="far fa-file-pdf"></i></span>
                 <span class="navi-text font-size-h6 ">Attachments</span>
             </a>
         </li>
         <li class="navi-item my-1">
            <a class="navi-link" data-toggle="tab" href="#actions_grid">
                <span class="navi-icon pr-6"><i class="fas fa-clipboard-check"></i></span>
                <span class="navi-text font-size-h6 ">Actions</span>
            </a>
        </li>
			</ul>
			</div>


			<div class="tab-content col-9 rounded pr-0" id="myTabContent">


  			  <div class="tab-pane show active fade bg-white rounded" id="overview_grid" role="tabpanel" aria-labelledby="kt_tab_pane_3" style="min-height:550px">
            <div class="card card-custom shadow-none">
             <div class="card-header">
              <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-chat-1 text-primary"></i>
                </span>
               <h3 class="card-label">
                Prospect Information
               </h3>
              </div>
             </div>
             <div class="card-body p-5">
               <section class="card card-custom card-border mb-3" data-card="true">
                 <header class="card-header">
                   <div class="card-title">
                     <h3 class="card-label">{{ __('common.info') }}</h3>
                   </div>
                   <div class="card-toolbar">
                     <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
                       <i class="ki ki-arrow-down icon-nm"></i>
                     </a>
                   </div>
                 </header>

                 <div class="card-body">
                   <div class="row">
                     <div class="col-sm-6">
                       <strong>{{ __('common.created:') }}</strong> {{ formatDateTime($model->created_at) }}
                     </div>
                     <div class="col-sm-6">
                       <strong>{{ __('common.reference_no:') }}</strong> {{ $model->reference_no }}
                     </div>
                   </div>
                   <div class="row">
                     <div class="mt-3 col-sm-6">
                       <strong>{{ __('common.full_name:') }}</strong> {{ $model->full_name }}
                     </div>
                     <div class="mt-3 col-sm-6">
                       <strong>{{ __('common.company_name:') }}</strong> {{ $model->company_name }}
                     </div>
                   </div>
                   <div class="row">
                     <div class="mt-3 col-sm-6">
                       <strong>{{ __('common.email:') }}</strong> {{ $model->email }}
                     </div>
                     <div class="mt-3 col-sm-6">
                       <strong>{{ __('common.phone:') }}</strong> {{ $model->phone }}
                     </div>
                   </div>
                   <x-customfields-custome-field-details :model="$model"/>
                   <x-customtags-custom-tags-field-details :model="$model"/>
                 </div>
               </section>
             </div>
            </div>
  				</div>


          <!-- Opportunities -->
          <div class="tab-pane fade bg-white rounded" id="opportunity_grid" role="tabpanel" aria-labelledby="kt_tab_pane_3" style="min-height:550px">
            <div class="card card-custom shadow-none">
             <div class="card-header">
              <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-chat-1 text-primary"></i>
                </span>
               <h3 class="card-label">
                Opportunities
               </h3>
              </div>
             </div>
             <div class="card-body p-5">
                  <x-opportunity-list :model="$model"/>
             </div>
            </div>
         </div>


          <!-- Attachments -->
          <div class="tab-pane fade bg-white rounded" id="attachment_grid" role="tabpanel" aria-labelledby="kt_tab_pane_3" style="min-height:550px">
            <div class="card card-custom shadow-none">
             <div class="card-header">
              <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-chat-1 text-primary"></i>
                </span>
               <h3 class="card-label">
                Attachments
               </h3>
              </div>
             </div>
             <div class="card-body p-5">
                  <x-actionlog-attachment :model="$model"/>
             </div>
            </div>
         </div>


         <!-- Actions -->
         <div class="tab-pane fade bg-white rounded" id="actions_grid" role="tabpanel" aria-labelledby="kt_tab_pane_3" style="min-height:550px">
           <div class="card card-custom shadow-none">
            <div class="card-header">
             <div class="card-title">
               <span class="card-icon">
                   <i class="flaticon2-chat-1 text-primary"></i>
               </span>
              <h3 class="card-label">
               Actions
              </h3>
             </div>
            </div>
            <div class="card-body p-5">
                 <x-actionlog-activity :model="$model"/>
            </div>
           </div>
        </div>



				</div>
			</div>
		</div>


























@endsection

@extends('layouts.app')
@section('title', __('app.lead.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'lead'=>__('app.lead.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('lead._form')
@endsection

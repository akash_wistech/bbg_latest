{!! Form::model($model, ['files' => true]) !!}
<input type="hidden" name="duplicatecheck" value="" />
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif

    @if(isset($di))
    <input type="hidden" name="di" value="{{$di}}">
    @endif
    <div class="row">
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('reference_no',__('common.reference_no')) }}
          {{ Form::text('reference_no', __('common.autogenerated'), ['class'=>'form-control', 'readonly'=>'readonly']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('full_name',__('common.full_name')) }}
          {{ Form::text('full_name', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('company_name',__('common.company_name')) }}
          {{ Form::text('company_name', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('email',__('common.email')) }}
          {{ Form::text('email', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('phone',__('common.phone')) }}
          {{ Form::text('phone', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('title',__('common.title')) }}
          {{ Form::text('title', null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('service_type',__('common.service_type')) }}
          {{ Form::select('service_type', [''=>__('common.select')] + getPredefinedListOptionsArr(getSetting('service_list_id')), null, ['class'=>'form-control']) }}
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('expected_close_date',__('common.expected_close_date')) }}
          <div class="input-group date">
            {{ Form::text('expected_close_date', null, ['class'=>'form-control dtpicker', 'autocomplete'=>'off']) }}
            <div class="input-group-append">
              <div class="input-group-text"><i class="la la-calendar-check-o"></i></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <div class="form-group">
          {{ Form::label('quote_amount',__('common.quote_amount')) }}
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">{{getSetting('currency_sign')}}</div>
            </div>
            {{ Form::text('quote_amount', $model->id==null && $model->quote_amount=='' ? 0 : $model->quote_amount, ['class'=>'form-control']) }}
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      {{ Form::label('descp',__('common.descp')) }}
      {{ Form::textarea('descp', null, ['class'=>'form-control', 'rows'=>4]) }}
    </div>
    <x-customfields-custom-fields-mixed :model="$model" :subModel="$model->subModel" :card="true"/>
      <x-staffmanager-module-manager-field :model="$model"/>
      <x-workflow-workflow-fields card="true" :model="$model"/>
    </div>
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
      <a href="{{ url('/lead') }}" class="btn btn-default">{{__('common.cancel')}}</a>
    </div>
  </div>
  {!! Form::close() !!}
  @push('css')
  <link href="{{asset('assets/plugins/custom/jquery-autocomplete/css/jquery.autocomplete.css')}}" rel="stylesheet" type="text/css" />
  @endpush()
  @push('js')
  <script src="{{asset('assets/plugins/custom/jquery-autocomplete/js/jquery.autocomplete.js')}}"></script>
  @endpush()
  @push('jsScripts')
  var moduleTitles = {!!json_encode(getModulesLabelListArr())!!}
  $("body").on("change", "#oppr-module_type",function(e){
  if($(this).val()!=""){
  $("#lookup-input").data('ds',"{{url('suggestion/module-lookup')}}/"+$(this).val());
  $("#lookup-input").data('mtype',$(this).val());
  $("#module_type-lookup").find("label").html(moduleTitles[$(this).val()]);
  $("#module_type-lookup").removeClass("d-none");
}else{
$("#module_type-lookup").addClass("d-none");
}
});
@endpush()

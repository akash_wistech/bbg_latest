@extends('layouts.app')
@section('title', __('app.lead.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'lead'=>__('app.lead.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('lead._form')
@endsection

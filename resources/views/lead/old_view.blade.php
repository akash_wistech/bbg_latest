@extends('layouts.app')
@section('title', __('app.opportunity.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'opportunity'=>__('app.opportunity.heading'),
'opportunity/update/'.$model->id=>__('common.update_rec',['name'=>$model->reference_no]),
__('common.view')
]])
@endsection

@section('content')
@include('opportunity._view_detail')
@endsection

@php
  $gridViewColumns = getGridViewColumns($moduleTypeId);
  $dtColsArr = [];
  $advSearchCols = [];
  $dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
  $dtColsArr[]=['title'=>__('common.reference_no'),'data'=>'reference_no','name'=>'reference_no'];

  $dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
  $advSearchCols[]=['title'=>__('common.title'),'name'=>'title'];

  $dtColsArr[]=['title'=>__('common.service_type'),'data'=>'service_type','name'=>'service_type'];
  $advSearchCols[]=['title'=>__('common.service_type'),'name'=>'service_type','input_type'=>'select','subopts'=>getPredefinedListOptionsArr(getSetting('service_list_id'))];

  $dtColsArr[]=['title'=>__('common.module_type'),'data'=>'module_type','name'=>'module_type'];
  $advSearchCols[]=['title'=>__('common.module_type'),'name'=>'module_type','input_type'=>'select','subopts'=>getSourcedModulesArr()];

  if($gridViewColumns!=null){
    foreach($gridViewColumns as $gridViewColumn){
      $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
    }
  }
  $dtColsArr[]=['title'=>__('common.expected_close_date'),'data'=>'expected_close_date','name'=>'expected_close_date'];
  $advSearchCols[]=['title'=>__('common.expected_close_date'),'name'=>'expected_close_date','input_type'=>'date'];

  $dtColsArr[]=['title'=>__('common.quote_amount'),'data'=>'amount','name'=>'amount'];
  $advSearchCols[]=['title'=>__('common.quote_amount'),'name'=>'quote_amount','input_type'=>'number'];

  $dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
  $dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

  $dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>85];
@endphp
<div id="grid-table" class="card card-body">
  <x-data-table-grid :request="$request" :controller="$controller" :jsonUrl="$controller.'/data-table-data'" :advSrchColArr="$advSearchCols" :dtColsArr="$dtColsArr" :advSearch="true" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
</div>

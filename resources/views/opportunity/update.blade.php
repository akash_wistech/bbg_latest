@extends('layouts.app')
@section('title', __('app.opportunity.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'opportunity'=>__('app.opportunity.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('opportunity._form')
@endsection

@php
$controller='opportunity';
$btnsList = [];
$btnlistDropdownItems = getModuleListDropdownItems($moduleTypeId,'',$controller);
$btnsList[] = ['label'=>__('common.list_view'),'icon'=>'bars','class'=>'success','link'=>'','subopts'=>$btnlistDropdownItems];

$btnlistDropdownItems = getModuleListDropdownItems($moduleTypeId,'kanban',$controller);
$btnsList[] = ['label'=>__('common.board_view'),'icon'=>'th-large','class'=>'success','link'=>'','subopts'=>$btnlistDropdownItems];

if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/opportunity/create', 'method'=>'post'];
}
$breadcrumbs = [];
$serviceType='';
if($request->service_type!=''){
  $wfItem = getPredefinedItem($request->service_type);
  if($wfItem!=null)$serviceType=$wfItem->lang->title;
  $breadcrumbs = [
    '/opportunity'=>__('app.opportunity.heading'),
    $serviceType
  ];
}else{
  $breadcrumbs = [
    __('app.opportunity.heading')
  ];
}
@endphp
@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('app.opportunity.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
@endsection


@section('content')
<div class="card card-custom">
  @if($request->layout=='kanban')
    @include('opportunity._kanban')
  @elseif($request->layout=='funnel')
    @include('opportunity._funnel')
  @else
    <!--div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          @if($serviceType!='')
            {{$serviceType}}
          @else
            {{__('common.all')}}
          @endif
        </h3>
      </div>
    </div-->
    @include('opportunity._grid')
  @endif
</div>
@endsection

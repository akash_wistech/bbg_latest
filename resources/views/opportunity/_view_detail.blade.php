<section class="card card-custom with-activity">
  <header class="card-header">
    <div class="card-title">
      <h3 class="card-label">{{ $model->reference_no }}</h3>
    </div>
    @if(checkActionAllowed('update'))
    <div class="card-toolbar">
      <a href="{{ url('/opportunity/update',['id'=>$model->id]) }}" class="btn btn-icon btn-sm btn-hover-light-primary">
        <i class="flaticon-edit-1"></i>
      </a>
    </div>
    @endif
  </header>
  <div class="card-body multi-cards multi-tabs">
    <section class="card card-custom card-border mb-3" data-card="true">
      <header class="card-header">
        <div class="card-title">
          <h3 class="card-label">{{ __('common.info') }}</h3>
        </div>
        <div class="card-toolbar">
          <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
            <i class="ki ki-arrow-down icon-nm"></i>
          </a>
        </div>
      </header>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-6">
            <strong>{{ __('common.created:') }}</strong> {{ formatDateTime($model->created_at) }}
          </div>
          <div class="col-sm-6">
            <strong>{{ __('common.reference_no:') }}</strong> {{ $model->reference_no }}
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-6">
            <strong>{{ __('common.service_type:') }}</strong> {{ $model->serviceTypeTitle() }}
          </div>
          <div class="col-sm-6">
            <strong>{{ __('common.source:') }}</strong> {!! $model->sourceInfo() !!}
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-6">
            <strong>{{ __('common.expected_close_date:') }}</strong> {{ formatDateTime($model->expected_close_date) }}
          </div>
          <div class="col-sm-6">
            <strong>{{ __('common.quote_amount:') }}</strong> {{ getCurrencyPre().$model->quote_amount }}
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-12">
            <strong>{{ __('common.title:') }}</strong> {{ $model->title }}
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-sm-12">
            <strong>{{ __('common.descp:') }}</strong> {{ nl2br($model->descp) }}
          </div>
        </div>
      </div>
    </section>
    <x-customfields-custome-field-details :card="true" :model="$model"/>
    <x-actionlog-attachment :model="$model"/>
    <x-actionlog-activity :model="$model"/>
    <x-actionlog-activity-history :model="$model"/>
  </div>
</section>

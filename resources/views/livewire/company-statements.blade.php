<div class="">
<h4 class="border-bottom p-4" style="background-color:#f9fafc;">Statements</h4>

<style>
table.statement {
  border-collapse: collapse;
  width: 100%;
}
.statement td,.statement th {
  border: 1px solid #EEEEEE;
  text-align: left;
  padding: 8px;
}
tr:nth-child(even) {
  background-color: #f9f9f9;
}
</style>


<?php

$members =getCompanyUsersListQuery($model->id,'contact');

$emailArr='<option value="select">Select</option>';
foreach ($members as $key => $member) {
  $emailArr.='<option value="'.$member['email'].'">'.$member['email'].'</option>';
}
?>



<!-- Modal-->
<div class="modal fade" id="sendEmailform" wire:ignore>
    <div class="modal-dialog modal-lg" role="document" id='block11'>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Account Summary</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
              <div class="col mx-auto text-center" wire:loading >
                <div class="spinner-border mx-auto" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            <form class="" method="post" wire:submit.prevent="modaldata">


              <div class="">
                {{ Form::label('email',__('Email to'),['class'=>'required']) }}
                <select type="text" class="form-control" wire:model="email" >
                  {!! $emailArr !!}
                </select>

              </div>
              <div class="mt-7">
                {{ Form::label('cc',__('CC'),['class'=>'required']) }}
                <input type="text" wire:model='cc' class='form-control'>
              </div>
              <div class="form-group mt-7">
                {{ Form::label('short_description',__('Preview Email Template')) }}
                {{ Form::textarea('short_description', $short_description, ['class'=>'form-control','rows'=>'10','wire:model'=>'short_description']) }}
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button  type="submit" class="btn btn-primary send-email font-weight-bold">Send</button>
            </div>
            </form>
        </div>
    </div>
</div>


 <div class="card-body">
   <!-- Top Options Such as Filter and invoice Pdf -->
   <div class="row">
     <div class="col-4">
       <select class="form-control"  wire:change="getInvoiceStatement($event.target.value)" id='statement_filter' name="statement_filter">
         <option value="today">Today</option>
         <option value="this_week">This Week</option>
         <option value="this_month" selected>This Month</option>
         <option value="last_month">Last Month</option>
         <option value="this_year">This Year</option>
         <option value="last_year">Last Year</option>
       </select>
     </div>
     <div class="ml-auto col-4">
       <div class="row">
         <a target="_blank" href="{{ url('invoices/make-pdf-statement/'.$model->id.'/'.$type.'/bbg') }}" class=" ml-auto mr-1 btn btn-secondary bg-gray-100"  data-toggle="tooltip" title="Print"> <i class="icon-lg la la-print"></i></a>
         <a  href="{{ url('invoices/make-pdf-statement/'.$model->id.'/'.$type.'/download' ) }}" class=" mx-1 btn btn-secondary bg-gray-100" data-toggle="tooltip" title="View Pdf"> <i class="icon-lg la la-file-pdf"></i></a>
         <button class=" mx-1 btn btn-secondary bg-gray-100" type="button" name="button" wire:click.prevent="openModal" ><span data-toggle="tooltip" title="Send to Email"><i class="icon-lg la la-envelope"></i></span></button>
       </div>
     </div>
   </div>
   <!-- Statement Heading with Company Name -->
   <div class="my-6">
     <h4 style="font-weight: 400;  line-height: 1.1;">Customer Statement For <span>{{ $model->title }}</span> </h4>
   </div>
   <!-- Invoice Statement -->
   <div class="card-body shadow-sm">
     <!--BBG InFormation -->
     <div class="row px-4">
       <div class="ml-auto text-right">
         <h5>British Business Group</h5>
         <p>British Embassy Compound<br>Al Seef Road<br>Bur Dubai<br>United Arab Emirates<br>P.O. Box 9333    </p>
       </div>
     </div>
     <!-- Custom Divider -->
     <div class=" border-bottom"></div>

     <!-- Starting InFormation of Account Summary -->
     <div class="row px-4">
       <!-- Customer InFormation -->
       <div class="">
         <br><span>To:</span>
         <br><b>{{ $model->title }}</b>
         <br><span>70316 Hudson Road</span>
         <br><span>North Velda Maine</span>
         <br><span>WF 61724</span>
       </div>
       <!--Account Summary From and TO -->
       <div class="ml-auto col-4">
         <br>
         <div class="text-right">
           <h3>Account Summary</h3>
           <!-- <p><?php  //echo $this->start_date.' to '.$this->end_date ?></p> -->
         </div>
         <div class="border-top">
             <div class="row py-1">
               <div>Beginning Balance:</div>
               <div class="ml-auto"> AED 0.00</div>
             </div>

             <?php
             $totalBalence1 =0;
             $inv_stat=0;
             $pay_stat=0;
             foreach ($statements as $key => $statement) {
               if ($statement['type']=='invoice') {
                 $inv_stat = $inv_stat+$statement['total'];
                 $totalBalence1 = $totalBalence1+$statement['total'];
               }
               if ($statement['type']=='payment') {
                 $pay_stat = $pay_stat+$statement['total'];
                 $totalBalence1 = $totalBalence1-$statement['total'];
               }
             }
                ?>
             <div class="row py-1">
               <div>Invoiced Amount:</div>
               <div class="ml-auto">AED {{ $inv_stat }}</div>
             </div>
             <div class="row py-1">
               <div>Amount Paid:</div>
               <div class="ml-auto">AED {{ $pay_stat }}</div>
             </div>
             <div class="row py-1 border-top ">
               <div>Balance Due:</div>
               <div class="ml-auto">AED {{ number_format((float)$totalBalence1, 2, '.', ''); }}</div>
             </div>
         </div>
       </div>
     </div>
     <!-- <div class="text-center my-12">
       <b>Showing all invoices and payments between <?php //echo $this->start_date.' and '.$this->end_date ?></b>
     </div> -->

    <!--Invoice Table -->
    <table class="statement">
    <tr>
      <th>Date</th>
      <th>Details</th>
      <th>Amount</th>
      <th>Payments</th>
      <th>Balance</th>
    </tr>
    <?php
    $totalBalence =0;
    foreach ($statements as $key => $statement) {

      if ($statement['type']=='invoice') {
        $totalBalence = $totalBalence+$statement['total'];
      }
      if ($statement['type']=='payment') {
        $totalBalence = $totalBalence-$statement['total'];
        $invoicesToPayment = \Wisdom\Sales\Models\Invoices::where('id',$statement['id'])->select('reference_number')->first();
      }   ?>
      <tr>
        <td>{{ $statement['date'] }}</td>
        <td>{{ (($statement['type']=='invoice')? ucwords($statement['type']).' ('.$statement['reference_number'].') due to - '.$statement['due_date'] : ucwords($statement['type'].' to invoice '.$invoicesToPayment['reference_number']) ) }}</td>
        <td>{{ (($statement['type']=='invoice')? $statement['total'] : '') }}</td>
        <td>{{ (($statement['type']=='payment')? $statement['total'] : '') }}</td>
        <td>{{ number_format((float)$totalBalence, 2, '.', ''); }}</td>
      </tr>
  <?php } ?>
      <tr>
        <td></td>
        <td></td>
        <td><b>Balance Due</b></td>
        <td></td>
        <td>{{ $totalBalence }}</td>
      </tr>
    </table>
   </div>
  </div>

</div>

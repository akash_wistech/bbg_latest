@php
$usersList=[];
$companyName='';
if($model->company!=null){
  $companyName =$model->company->title;
  $usersList = getCompanyUsersList($model->company_id);
}
$invoiceRef = $model->reference_number;

$compInputOpts = [];
if($model->id!=null){
  $compInputOpts['class']='form-control';
  $compInputOpts['readonly']='readonly';
}else{
  $compInputOpts['class']='autocomplete-company form-control';
  $compInputOpts['data-ds']=url('suggestion/company');
  $compInputOpts['data-mtype']=($model->module_type!='' ? $model->module_type : '');
  $compInputOpts['data-fld']='company_id';
  $compInputOpts['data-module']='contact';
  $compInputOpts['data-oldsval']=$model->user_id;
  $compInputOpts['data-sfld']='user_id';
}
@endphp

	<div class="d-flex flex-column-fluid">
		<div class="container">
			<div class="card card-custom">
				<div class="card-body p-0">
			{!! Form::model($model, ['files' => true,'id'=>'kt_form']) !!}
				<div class="card-body">
					@if ($errors->any())
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
						<div>{{ $error }}</div>
						@endforeach
					</div>
					@endif
					<div class="row px-0">
						<div class="col-12">
              <div class="row">
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('company_id',__('common.company'),['class'=>'required']) }}
                    {!! Form::text('customer_name', $companyName,$compInputOpts) !!}
                    {{ Form::hidden('company_id', $model->company_id, ['id'=>'company_id','class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('user_id',__('common.user_id'),['class'=>'required']) }}
                    {!! Form::select('user_id',[], $model->user_id, ['id'=>'user_id1', 'class' => 'form-control comeMembers','required'=>'true']) !!}

                    <select class="form-control" wire:change="getMemberDetailOnCompanyId($event.target.value)"  id='user_id' name="user_id">
                      <option value="select">Select</option>
                    </select>
                  </div>
                </div>
                <input type="hidden" id='event-id' name="event_id" value="{{ $event_id }}">
                </div>
                <!-- if a person add a member , It comes here -->
                <div class="members_here">
                </div>

                <div class="row my-5 mt-15">
                  <div class="col-9"></div>
                  <div class="col-3 ">
                    <div class="ml-auto" style="float:right">
                      <button type="button" class="btn btn-sm btn-light-primary add_guest_21"  wire:click="GetGuestForm({{ $GuestCount }})">Add Guest</button>
                    </div>
                  </div>
                </div>
                <!-- if a person add guest, it comes here -->
                <div id="section_invite_guest">
                  <div id="guestInformation">
                    @foreach($inputs as $key => $value)
                    <div class="card card-custom remove_guest_1 my-10 guest-member-parent parent-card col-12" style="padding-left:60px">
                    <div class="position-absolute " style="left:0px; top:0; bottom:0; width:70px; background-color:#EEEEEE;">
                    <h4 class="text-center mt-39 font-weight">Guest</h4>
                    </div>
                    <div class="card-body  row">
                    <div class="col-11">
                    <div class="row">
                    <div class="col-4">
                    <input class="form-control" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][user_id]" type="text"   value="{{ $member_id }}"/>
                    <!-- <input class="form-control" wire:model="name.0" placeholder="Name" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][name]" type="text"/> -->
                    <input class="form-control" wire:model="name.{{ $value }}" placeholder="Name"  type="text"/>
                    </div>
                    <div class="col-4">
                    <div class="form-group">
                    <input class="form-control js-user-email" placeholder="Email" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][email]" type="text">
                    </div>
                    </div>
                    <div class="col-4">
                    <input class="form-control" placeholder="mobile" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][mobile]" type="text" >
                    </div>
                    <div class="col-4">
                    <input class="form-control mt-7" placeholder="Company" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][company]" type="text" >
                    </div>
                    <div class="col-4">
                    <input class="form-control mt-7" placeholder="Designation" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][designation]" type="text">
                    </div>
                    <div class="col-4 mt-7">
                    <select class="form-control" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][user_type_relation]">
                    <option value="" selected="selected">Select User Type</option>
                    <?php
                    $userTypesList = getUserTypeRelation();
                    if($userTypesList!=null){
                      foreach($userTypesList as $key2=>$val){ ?>
                        <option value="{{$key}}">{{ $val }}</option>
                      <?php } } ?>

                    </select>
                    </div>
                    </div>
                    <div class="row mt-7">
                    <div class="form-group col-2 px-0 pt-4">
                    <div class="checkbox-inline col-12">
                    <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                    <input type="checkbox" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][focGuest]" value="1">
                    <span></span>
                    Foc Guest
                    </label>
                    </div>
                    </div>
                    <div class="form-group col-2 px-0 pt-4">
                    <div class="checkbox-inline col-12">
                    <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                    <input class="diet-option-box" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][diet_option]" type="checkbox" value="1">
                    <span></span>
                    Diet Option
                    </label>
                    </div>
                    </div>
                    <div class="form-group col-4 px-0 pt-4">
                    <div class="checkbox-inline col-12">
                    <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                    <input type="checkbox" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][vaccinated]" value="1">
                    <span></span>
                    Fully vaccinated against COVID-19
                    </label>
                    </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-4 diet-option-description-select" style="display: none;">
                    <div class="form-group">
                    <select class="form-control diet-select-options" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][diet_option_description]"><option value="" selected="selected">Select Dietry Option...</option><option value="Standard">Standard</option><option value="Vegetarian">Vegetarian</option><option value="other">Other Dietary</option></select>
                    </div>
                    </div>
                    <div class="col-9 other-diet-option-description" style="display: none;">
                    <div class="form-group">
                    <textarea class="form-control " placeholder="Write Description" rows="3" name="guestinfo[{{ $member_id }}][{{$GuestCount}}][other_diet_option_description]" cols="50"></textarea>
                    </div>
                    </div>
                    </div>
                    <div class="row">
                    {{ getCustomFieldsMultiple($model,$isSub=false,$card=false,$this->event_id); }}
                    </div>
                    </div>
                    <div class="col-1">
                    <div class="col pr-0 mt-25"><span class="btn btn-light-danger pl-6 pr-5 font-weight-bold"  wire:click="Remove({{$key}})" ><i class="far fa-trash-alt"></i></span>
                    </div>
                    </div>
                    </div>
                    </div>
                    @endforeach


                  </div>
                </div>
            </div>
        </div>
					</div>
					<div class="card-footer border-0">
						<button type="submit" class="btn btn-success">{{__('common.save')}}</button>
						<a href="{{ url('/event') }}" class="btn btn-default">{{__('common.cancel')}}</a>
					</div>
					 {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>

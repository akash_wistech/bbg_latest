@extends('frontend.app')
@section('content')

<?php
use App\Models\MemberContacts;
$no_image = asset('assets/images/dummy-image-1.jpg');

// dd($model->main_company_id);
// dd($company->id);
?>

<x-contact-banner/>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
						<h3><?= ($company <> null) ? $company->title . " Members" : "Company Members" ;?></h3>
					</div>
					<?php
					if (Auth::check() && $model->main_company_id==$company->id) {?>
						
						<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2" style="padding: 20px;">
							<a href="{{url('/member/add-additional')}}" class="Mybtn"><i class="fa fa-plus px-1"></i>Add Member</a>
						</div>
						<?php
					}
					?>
				</div>
				<div class="row PaddingTopBtm30px" style="padding: 20px;">
					<?php
					if ($showComapnyInfo) {
						if ($company<>null) {
							$nonTextAreaColumnsForCompany=getNonTextAreaColumns($company->moduleTypeId);
							$textAreaColumnsForCompany=getTextAreaColumns($company->moduleTypeId);
							// dd($textAreaColumnsForCompany);
							?>
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
								<h2>
									Company Information
									<?php 
									if (Auth::check()) {?>
										<span class="float-right">
											<a href="#" class="d-none"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a>
										</span>
										<?php 
									}
									?>

								</h2>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
								<div class="row PaddingTopBtm30px">
									<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
										<img height="200" src="<?= ($company <> null && $company->logo <> "") ? $company->logo : $no_image; ?>" class="img-fluid" alt="">
									</div>
									<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
										<div class="profile">
											<?php 
											// getCustomFieldsAgaintsCompany('contact', $company->id)
											?>
											<h2><?= ($company <> null) ? $company->title : ""; ?></h2>
											<p>
												@if($nonTextAreaColumnsForCompany->isNotEmpty())
												@foreach($nonTextAreaColumnsForCompany as $nonTextAreaColumn)
												@php
												$value=getInputFielSavedValueForDetail($company,$nonTextAreaColumn);
												if(is_array($value))$value = implode(", ",$value);
												@endphp
												<strong>{{$nonTextAreaColumn['title']}}</strong> - {{ ($value!=null)? $value : 'N\A' }} <br>
												@endforeach
												@endif
											</p>

											<p>
												@if($textAreaColumnsForCompany->isNotEmpty())
												@foreach($textAreaColumnsForCompany as $textAreaColumn)
												@php
												$value=getInputFielSavedValueForDetail($company,$textAreaColumn);
												if(is_array($value))$value = implode(", ",$value);
												@endphp
												<h2>{{ $textAreaColumn['title'] }}</h2>
												<p >{!! $value !!}</p>
												@endforeach
												@endif
											</p>
										</div>
									</div>
								</div>
							</div>
							<?php
						}
					}
					?>


					<?php
					if (Auth::check()) {
						?>
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading" id="CompanyMembers">
							<h2>
								Members Information
							</h2>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<br/>
							<div class="row PaddingTopBtm30px">
								<?php
								if ($members <> null) {


									foreach ($members as $key => $member) {
										$isContact = MemberContacts::where(['member_id' => Auth::user()->id, 'contact_id' => $member['id']])->first();

										?>

										<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
											<div class="directorymember">
												<div class="row">
													<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
														<img src="<?= ($member['image']!=null) ? $member['image'] : $no_image ?>"
														class="img-fluid image_150" alt="">
													</div>
													<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
														<div class="directory" style="min-height: 120px;">
															<span class="pull-right memberContactIcon"
															id="memberContact<?= $member['id']; ?>">
															<?php
															if($isContact <> null){?>
																<a href="javascript:;" class="add_member_contact add-false" id="<?= $member['id'] ?>" title="Remove From Contact" style="color: #bd1f2f !important;">
																	<i class="fa fa-user-times" aria-hidden="true"></i>
																</a>
																<?php 
															}else{
																?>
																<a href="javascript:;" class="add_member_contact add-true" id="<?= $member['id'] ?>" title="Add to Contact" style="color: #228B22 !important;">
																	<i class="fa fa-user-plus" aria-hidden="true"></i>
																</a>
																<?php
															}
															if ($member['created_by'] == $model->id) {
																?>	
																<a  href="{{url('membershipFend/update/'.$member['id'])}}" id="<?= $member['id'] ?>" class="update_contact_profile" title="Update Profile" style="color: #428bca !important;padding-left: 10px;"><i class="fa fa-pencil" aria-hidden="true"></i></a>
																<?php
															}
															?>

														</span>
														<h2><?= $member['name'] ?></h2>
														<?php
														$companies = getModuleSavedCompaniesByModuleId($member['id'],'contact');
																// dd($companies);
														foreach ($companies as  $company) {
															if ($company<>null) {
																// dd($company->id);
																if($company->company!=null){
																	$companyName = $company->company->title;
																			// dd($companyName);
																	$designationRow = getPredefinedItem($company->job_title_id);
																	$designation='';
																	if($designationRow!=null && $designationRow->lang!=null)$designation=$designationRow->title;
																			// dd($designation);
																	?>
																	<h3>
																		<?= $companyName; ?><br>
																		<?= $designation; ?><br>
																	</h3>
																</div>
																<span class="EventDetail">
																	<a href="<?= url('/member/detail',['id'=>$member['id']]) ?>">View Detail</a>
																</span>

																<span class="EventDetail">
																	<a href="{{url('company/members?id='.$company->id)}}">Company Detail</a>
																</span>
																<?php
															}
														}
													}?>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							} else {
								?>
								<div class="col-md-12">
									<div class="col-md-12 alert alert-danger">No Record Found</div>
								</div>
								<?php
							}
							?>
						</div>

						<?php
					}
					?>
					<br>
				</div>
				<div class="col-md-12 text-center">

				</div>
			</div>
		</div>
	</section>


	@endsection

	@push('css')
	<style>
		.TopMostSearch1 {
			margin-top: 20px;
		}
	</style>
	@endpush


	@push('js')
	<script>
		$("body").find("a.add_member_contact").click(function(e) {
			e.preventDefault();  
			$(this).prop("disabled", true);

			var memberId = $(this).attr("id");
			var type = "add";  
// console.log(type);

if( $(this).hasClass("add-false")){
	type = "remove"; 
// console.log(type);
}

$.ajax({
	url: "/account/add-member-contact",
	type: "POST",
	data: {
		"_token": "{{ csrf_token() }}",
		id : memberId, type : type
	}, 
	success : function(res){ 

		if(res == 1){ 
			console.log("type");
			console.log(type);
			console.log(res);
			if(type == "add"){ 
				$("#"+memberId+".add_member_contact").removeClass("add-true");
				$("#"+memberId+".add_member_contact").addClass("add-false");  
				$("#"+memberId+".add_member_contact").css("color","#bd1f2f","!important"); 

				$("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
				$("#memberContact"+memberId+ " .fa").addClass("fa-user-times");

				$(this).attr("title", "Add to contact");

			} else{ 
				$("#"+memberId+".add_member_contact").removeClass("add-false");
				$("#"+memberId+".add_member_contact").addClass("add-true"); 
				$("#"+memberId+".add_member_contact").css("color","#228B22","!important"); 

				$("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
				$("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 

				$(this).attr("title", "Remove from contact");
			}              

		} 
	},
	error : function(data){
		console.log(data);
	}
}); 


e.stopImmediatePropagation(); 
return false;
});
</script>
@endpush
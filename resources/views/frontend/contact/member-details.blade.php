<?php  
$no_image = asset('assets/images/dummy-image-1.jpg');

?>

@extends('frontend.app')
@section('content')

<x-contact-banner/>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h3>Member Details</h3>
					</div>

				</div>
				<div class="row PaddingTopBtm30px" style="padding: 20px;">

					<?php
					if ($member <> null) {
						// dd($member->moduleTypeId);
						$nonTextAreaColumns=getNonTextAreaColumns($member->moduleTypeId);
						
						?>
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
							<h2>Personal Information</h2>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
							<div class="row PaddingTopBtm30px">
								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">

									

									<img src="<?= ($member->image <> null) ? $member->image : $no_image; ?>"
									class="img-fluid" alt="">
								</div>
								<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
									<div class="profile">


										<h2><?= ($member <> null) ? $member->name : ""; ?></h2>
										<?php 
										$companies = getModuleSavedCompanies($member);
										// dd($companies);
										foreach ($companies as  $company) {
											if ($company<>null) {
												if($company->company!=null){
													$companyName = $company->company->title;
													// dd($companyName);
													$designationRow = getPredefinedItem($company->job_title_id);
													$designation='';
													if($designationRow!=null && $designationRow->lang!=null)$designation=$designationRow->title;
													// dd($designation);
													?>
													
													<h3><?= $designation ?></h3>


													<?php
												}
											}
										}
										?>



										<p>
											@if($nonTextAreaColumns->isNotEmpty())
											@foreach($nonTextAreaColumns as $nonTextAreaColumn)
											@php
											$value=getInputFielSavedValueForDetail($member,$nonTextAreaColumn);
											if(is_array($value))$value = implode(", ",$value);
											@endphp
											<strong>{{$nonTextAreaColumn['title']}}</strong> - {{ ($value!=null)? $value : 'N\A' }} <br>
											@endforeach
											@endif
											
											<?php
											$socialMediaLinks = getModuleSocialMediaQuery($member->id,$member->moduleTypeId);
											foreach ($socialMediaLinks as $socialMediaLink) {
												// dd($socialMediaLink);
												?>
												<strong><?=$socialMediaLink['title']?></strong>
												- <a href="<?=$socialMediaLink['link']?>"><?=$socialMediaLink['link']?></a><br>
												
												<?php										
											}
											?>
											
										</p>
									</div>
								</div>
							</div>

						</div>


						<?php  
						$companies = getRowMultipleCompaniesQuery('contact',$member->id);
						if ($companies<>null && count($companies)>0) {?>	
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
								<h2>
									Company Information
									<span class="float-right">
										<a href="#" class="d-none"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a>
									</span>
								</h2>
							</div>
							<?php
							
							foreach ($companies as $company) {
								$nonTextAreaColumnsForCompany=getNonTextAreaColumns($company->moduleTypeId);
								$textAreaColumnsForCompany=getTextAreaColumns($company->moduleTypeId);
								?>
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
									<div class="row PaddingTopBtm30px">
										<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
											<img src="<?= ($company <> null && !empty($company->logo)) ? $company->logo : $no_image ?>"
											class="img-fluid" alt="">
										</div>
										<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
											<div class="profile">
												<h2>
													<a href="<?= url('/company/members?id='.$company->id) ?>"> <?= ($company <> null) ? $company->title : ""; ?></a>
												</h2>

												<p>
													@if($nonTextAreaColumnsForCompany->isNotEmpty())
													@foreach($nonTextAreaColumnsForCompany as $nonTextAreaColumn)
													@php
													$value=getInputFielSavedValueForDetail($company,$nonTextAreaColumn);
													if(is_array($value))$value = implode(", ",$value);
													@endphp
													<strong>{{$nonTextAreaColumn['title']}}</strong> - {{ ($value!=null)? $value : 'N\A' }} <br>
													@endforeach
													@endif
												</p>
												
												<p>
													@if($textAreaColumnsForCompany->isNotEmpty())
													@foreach($textAreaColumnsForCompany as $textAreaColumn)
													@php
													$value=getInputFielSavedValueForDetail($company,$textAreaColumn);
													if(is_array($value))$value = implode(", ",$value);
													@endphp
													<h2>{{ $textAreaColumn['title'] }}</h2>
													<p >{!! $value !!}</p>
													@endforeach
													@endif
												</p>
											</div>
										</div>
									</div>
								</div>
								<?php
							}

						}

					}  else {
						?>
						<div class="col-md-12">
							<div class="col-md-12 alert alert-danger">No Record Found</div>
						</div>
						<?php
					}
					?>


				</div>
				<br>
			</div>
		</div>
	</div>
</section>



@endsection

@push('css')
<style>
	.TopMostSearch1 {
		margin-top: 20px;
	}
</style>
@endpush



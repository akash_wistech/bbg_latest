@extends('frontend.app')
@section('content')

<x-contact-banner/>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<h3>Payment History</h3>
				<br>
				<table class="table table-striped table-responsive">
					<thead>

						<tr>
							<th>Reference Number</th>
							<th>Name</th>
							<th>Company</th>
							<th>Amount</th>
							<th>Payment Method</th>
							<th>Completed</th>
							<th>Payment Date</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($payments<>null) {
							foreach ($payments as $payment) {
								// dd($payment->company->title);
								?>
								<tr>
									<th scope="row"><?= $payment->reference_number; ?></th>
									<td><?= $payment->user->name; ?></td>
									<td><?= $payment->company->title; ?></td>
									<td><?= $payment->amount_received; ?></td>
									<td><?= getPaymentMehtodLabelArr()[$payment->payment_method]; ?></td>
									<td><?= $payment->is_completed==1 ? '<span class="label font-weight-bold label-lg label-light-success label-inline"><i class="fa fa-check"></i></span>' : '<span class="label label-light-primary label-inline"><i class="fa fa-times"></i></span>'; ?></td>
									<td><?= formatDate($payment->payment_date); ?></td>
								</tr>
								<?php
							}
						}
						?>

					</tbody>
				</table>
				<br>
				<div class="col-md-12 text-center pull-right">
					{{$payments->links()}}
				</div>
			</div>

		</div>
	</div>
</section>

@endsection

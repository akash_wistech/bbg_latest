@extends('frontend.app')
@section('content')
<section class="MainArea">
	<div class="container LeftArea">

		<div class="row">
			<div class="col-3"></div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 Heading" style="min-height: 300px">
				{!! Form::model($model, ['files' => true]) !!}
				<div class="col-md-12 col-md-offset-3 text-center">
					<h3>Update Password</h3>
					<br>
					<div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">
						{{ Form::password('currentPassword',['autofocus'=>true, 'placeholder'=>'Enter Your Current Passowrd']) }}
					</div>

					<div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">						
						{{ Form::password('password',['autofocus'=>true, 'placeholder'=>'Enter new password']) }}
					</div>

					<div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5">
						{{ Form::password('repassword',[ 'autofocus'=>true, 'placeholder'=>'Re-enter new passowrd']) }}
					</div>

					<div class="col-md-12 col-sm-12 input-style-1 b-50 type-2 color-5 my-3">
						<a href="" class="mybutn btn btn-default btn-circle">Update Password</a>
					</div>
					<br> <br> <br>
				</div>

				{!! Form::close() !!}
			</div>
			<div class="col-3"></div>
		</div>

	</div>
</section>
@endsection
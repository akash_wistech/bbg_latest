<?php
use Wisdom\Event\Models\Event;
use Carbon\Carbon;
use Wisdom\News\Models\News;
use NaeemAwan\MultipleCompany\Models\ModuleCompany;
use Wisdom\JobPosts\Models\JobPosts;
use Wisdom\Sales\Models\Invoices;



$no_image = asset('assets/images/dummy-image-1.jpg');

$totalPostedJobs = JobPosts::where(['user_id'=>$model->id])->count();
$totalPublishedJobs = JobPosts::where(['user_id'=>$model->id, 'published'=>'1'])->count();
// dd($totalPublishedJobs);

$unpaidInvoices = Invoices::where(['user_id'=>$model->id,'payment_status'=>0])->count();
// dd($unpaidInvoices);
$partialyPaidInvoices = Invoices::where(['user_id'=>$model->id,'payment_status'=>2])->count();
// dd($partialyPaidInvoices);


//Commment by Mushabab
//
// //2022
// $totalEventThisYear = Event::whereNull('events.deleted_at')
// ->whereBetween('events.event_startDate',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()])
// ->get();
//
// $JoinedThisYear = Event::whereNull('events.deleted_at')
// ->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')
// ->where('event_subscriptions.user_id',$model->id)
// ->whereBetween('events.event_startDate',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()])
// ->groupBy('event_subscriptions.event_id')
// ->get();
//
//
// $EventInvoicesLastYear = \Wisdom\Event\Models\Event::whereNull('events.deleted_at')
// ->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')
// ->where('event_subscriptions.user_id',$model->id)
// ->whereBetween('events.event_startDate',[Carbon::now()->startOfYear(),Carbon::now()->endOfYear()])
// ->select('event_subscriptions.id');
//
//
// $query = DB::table('invoices')->whereNull('deleted_at');
// $query2= \DB::table('module_invoice')->whereIn('module_id',$EventInvoicesLastYear)->where('module_type','event-member-register')->select('invoice_id');
// $query->where(function($query) use($EventInvoicesLastYear,$query2){
// 	$query->whereIn('id',$query2);
// })->select('id');
//
// $paymentsThisYear = DB::table('payment_items')->whereIn('module_id',$query)->where('module_type','invoice')->sum('amount_paid');
//
//
// $user_id = $model->id;
// $ThisYearNews = News::where(function($query) use ($user_id){
// 	$query->where('related_to','member')
// 	->whereIn('rel_id',function($query) use($user_id){
// 		$query->select('module_id')
// 		->from(with(new ModuleCompany)->getTable())
// 		->where('module_type','contact')
// 		->where('module_id', $user_id);
// 	});
// })
//
// ->count('id');
//
//
// // 2021
// $JoindedLastYear = \Wisdom\Event\Models\Event::whereNull('events.deleted_at')
// ->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')
// ->where('event_subscriptions.user_id',$model->id)
// ->whereYear('events.event_startDate', now()->subYear()->year)
// ->groupBy('event_subscriptions.event_id')
// ->get();
//
// $totalEventLastYear = \Wisdom\Event\Models\Event::whereNull('events.deleted_at')
// ->whereYear('events.event_startDate', now()->subYear()->year)
// ->get();
//
// // dd($model->id);
//
// $EventInvoicesThisYear = \Wisdom\Event\Models\Event::whereNull('events.deleted_at')
// ->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')
// ->where('event_subscriptions.user_id',$model->id)
// ->whereYear('events.event_startDate', now()->subYear()->year)
// ->select('event_subscriptions.id');
//
// $query = DB::table('invoices')->whereNull('deleted_at');
// $query2= \DB::table('module_invoice')->whereIn('module_id',$EventInvoicesThisYear)->where('module_type','event-member-register')->select('invoice_id');
// $query->where(function($query) use($EventInvoicesThisYear,$query2){
// 	$query->whereIn('id',$query2);
// })->select('id');
//
//
//
// $paymentsLastYear = DB::table('payment_items')->whereIn('module_id',$query)->where('module_type','invoice')->sum('amount_paid');
// // dd($paymentsLastYear);
//
// $LastYearNews = News::where(function($query) use ($user_id){
// 	$query->where('related_to','member')
// 	->whereIn('rel_id',function($query) use($user_id){
// 		$query->select('module_id')
// 		->from(with(new ModuleCompany)->getTable())
// 		->where('module_type','contact')
// 		->where('module_id', $user_id);
// 	});
// })
// ->where(function($query){
// 	$query->whereYear('created_at','2021');
// })
// ->count('id');
//
//
// // 2020
// $JoindedTwoYearAgo = Event::whereNull('events.deleted_at')
// ->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')
// ->where('event_subscriptions.user_id',$model->id)
// ->whereYear('events.event_startDate', '2020')
// ->groupBy('event_subscriptions.event_id')
// ->get();
//
// $totalEventTwoYearAgo = Event::whereNull('events.deleted_at')
// ->whereYear('events.event_startDate', '2020')
// ->get();
//
//
// $EventInvoicesTwoYearAgo = \Wisdom\Event\Models\Event::whereNull('events.deleted_at')
// ->join('event_subscriptions', 'events.id', '=', 'event_subscriptions.event_id')
// ->where('event_subscriptions.user_id',$model->id)
// ->whereYear('events.event_startDate', '2020')
// ->select('event_subscriptions.id');
//
// $query = DB::table('invoices')->whereNull('deleted_at');
// $query2= \DB::table('module_invoice')->whereIn('module_id',$EventInvoicesTwoYearAgo)->where('module_type','event-member-register')->select('invoice_id');
// $query->where(function($query) use($EventInvoicesTwoYearAgo,$query2){
// 	$query->whereIn('id',$query2);
// })->select('id');
//
// $paymentsTwoYearAgo = DB::table('payment_items')->whereIn('module_id',$query)->where('module_type','invoice')->sum('amount_paid');
//
// $TwoYearsAgoNews = News::where(function($query) use ($user_id){
// 	$query->where('related_to','member')
// 	->whereIn('rel_id',function($query) use($user_id){
// 		$query->select('module_id')
// 		->from(with(new ModuleCompany)->getTable())
// 		->where('module_type','contact')
// 		->where('module_id', $user_id);
// 	});
// })
// ->where(function($query){
// 	$query->whereYear('created_at','2022');
// })
// ->count('id');
//
// $totalEventThisYear = count($totalEventThisYear);
// $JoinedThisYear = count($JoinedThisYear);
// $JoindedLastYear = count($JoindedLastYear);
// $totalEventLastYear = count($totalEventLastYear);
// $JoindedTwoYearAgo = count($JoindedTwoYearAgo);
// $totalEventTwoYearAgo = count($totalEventTwoYearAgo);

//End Commment by Mushabab


$model = App\Models\Contact::where(['id'=>Auth::user()->id])->first();
    // dd($model->active_subscription_id);

$totalNews = News::whereIn('created_by', function($query) use ($model){
	$query->select('user_id')
	->from(with(new NaeemAwan\ModuleSubscription\Models\SubscriptionUsers)->getTable())
	->where('subscription_id', $model->active_subscription_id);
})->count();


?>

@extends('frontend.app')
@section('content')

		<x-contact-banner/>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="card-body">
			<h3 class="pt-3">Statistics</h3>

			<div class="row px-2">
				<div class="pr-3 col-3">
				<div class="shadow p-4 rounded my-4 bg-white">
					<h3 class="py-2 ">Total Posted Jobs</h3>
					<div class="d-flex align-items-center flex-wrap">
						<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
							<div class="d-flex flex-column text-dark-75">
								<span class="font-weight-bolder font-size-h5">
									<span class="text-dark-50 font-weight-bold">{{ $totalPostedJobs }}</span>
								</div>
							</div>
						</div>
					</div>
				</div>

				  <div class="px-3 col-3 d-none">
					<div class="shadow p-4 rounded my-4 bg-white">
						<h3 class="py-2 ">Total Published Jobs</h3>
						<div class="d-flex align-items-center flex-wrap">
							<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
								<div class="d-flex flex-column text-dark-75">
									<span class="font-weight-bolder font-size-h5">
										<span class="text-dark-50 font-weight-bold">{{$totalPublishedJobs}}</span>
									</div>
								</div>
							</div>
						</div>
						</div>

						<div class="px-3 col-3">
						<div class="shadow p-4 rounded my-4 bg-white">
							<h3 class="py-2 ">Total News</h3>
							<div class="d-flex align-items-center flex-wrap">
								<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
									<div class="d-flex flex-column text-dark-75">
										<span class="font-weight-bolder font-size-h5">
											<span class="text-dark-50 font-weight-bold">{{$totalNews}}</span>
										</div>
									</div>
								</div>
							</div>
							</div>

							<div class="px-3 col-3">
							<div class="shadow p-4 rounded my-4 bg-white">
								<h3 class="py-2 ">Unpaid Invoices</h3>
								<div class="d-flex align-items-center flex-wrap">
									<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
										<div class="d-flex flex-column text-dark-75">
											<span class="font-weight-bolder font-size-h5">
												<span class="text-dark-50 font-weight-bold">{{$unpaidInvoices}}</span>
											</div>
										</div>
									</div>
								</div>
								</div>

								<div class="pl-3 col-3">
								<div class="shadow p-4 rounded my-4 bg-white">
									<h3 class="py-2 ">Partially Paid Invoices</h3>
									<div class="d-flex align-items-center flex-wrap">
										<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
											<div class="d-flex flex-column text-dark-75">
												<span class="font-weight-bolder font-size-h5">
													<span class="text-dark-50 font-weight-bold">{{$partialyPaidInvoices}}</span>
												</div>
											</div>
										</div>
									</div>
							</div>
							</div>

					    <?php
					    $company_id= $model->main_company_id;
					    $user_id= $model->id;
					    $totalevent = getTotalEventsByUser();
					    $eventByCompany = getTotalEventsByUser($user_id);
					    $eventByCompanyAttended = getTotalEventsByUser($user_id,1);
					    foreach ($totalevent as $key => $value) {
					     $event_s_paysumArr[$key]['sum']=getJoinedSum($user_id,$key);
					     $event_s_paysumArr[$key]['event_by_year']=$value;
					     $event_s_paysumArr[$key]['register_event']=0;
					     $event_s_paysumArr[$key]['attended_event']=0;
					     $event_s_paysumArr[$key]['news_uploaded']=getNewsByYear($company_id,$key);
					     if (array_key_exists($key, $eventByCompany)) {
					       $event_s_paysumArr[$key]['register_event']=$eventByCompany[$key];
					     }
					     if (array_key_exists($key, $eventByCompanyAttended)) {
					       $event_s_paysumArr[$key]['attended_event']=$eventByCompanyAttended[$key];
					     }
					     ?>
					     <!-- 2022 -->
					     <div class="my-5 shadow-sm p-4 rounded bg-white">
					      <h2 class="py-2 font-weight-bold">{{ $key }}</h2>
					      <div class="d-flex align-items-center flex-wrap">
					       <!--begin: Item-->
					       <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
					         <span class="mr-4">
					           <i class="flaticon-confetti icon-2x text-primary font-weight-bold"></i>
					         </span>
					         <div class="d-flex flex-column text-dark-75">
					           <span class="font-weight-bolder font-size-sm">Registered Events</span>
					           <span class="font-weight-bolder font-size-h5">
					             <span class="text-dark-50 font-weight-bold">{{ $event_s_paysumArr[$key]['register_event'].'/'.$value }}</span>
					           </div>
					         </div>
					         <!--end: Item-->
					         <!--begin: Item-->
					         <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
					           <span class="mr-4">
					             <i class="flaticon-confetti icon-2x text-primary font-weight-bold"></i>
					           </span>
					           <div class="d-flex flex-column text-dark-75">
					             <span class="font-weight-bolder font-size-sm">Attended Events</span>
					             <span class="font-weight-bolder font-size-h5">
					               <span class="text-dark-50 font-weight-bold">{{ $event_s_paysumArr[$key]['attended_event'].'/'.$value }}</span>
					             </div>
					           </div>
					           <!--end: Item-->
					           <!--begin: Item-->
					           <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
					             <span class="mr-4">
					               <i class="flaticon-piggy-bank icon-2x text-primary font-weight-bold"></i>
					             </span>
					             <div class="d-flex flex-column text-dark-75">
					               <span class="font-weight-bolder font-size-sm">Events Amount Paid</span>
					               <span class="font-weight-bolder font-size-h5">
					                 <span class="text-dark-50 font-weight-bold pr-3">AED</span>{{ getJoinedSum($user_id,$key) }}</span>
					               </div>
					             </div>
					             <!--end: Item-->
					             <!--begin: Item-->
					             <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
					               <span class="mr-4">
					                 <i class="flaticon-pie-chart icon-2x text-primary font-weight-bold"></i>
					               </span>
					               <div class="d-flex flex-column text-dark-75">
					                 <span class="font-weight-bolder font-size-sm">News Published</span>
					                 <span class="font-weight-bolder font-size-h5">
					                   <span class="text-dark-50 font-weight-bold">{{ getNewsByYear($company_id,$key); }}</span>
					                 </div>
					               </div>
					               <!--end: Item-->
					                 </div>
					               </div>
					             <?php } ?>
					           </div>
									</div>
								</section>

@endsection

@extends('frontend.app')
@section('content')
<x-contact-banner/>
{!! Form::model($model, ['files' => true]) !!}
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
					<div class="col-md-12 ">
						<div class="Heading text-left">
							<h3>Edit my email subscriptions</h3><br/>
						</div>
					</div>

					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-1">
						<label class="checkbox checkbox-lg">
							{{ Form::checkbox('events_news', '1', false,  ['class' => 'larger']) }}
							<span class="label">Events</span>
						</label>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 my-4">
						<label class="checkbox checkbox-lg">
							{{ Form::checkbox('subscriptions', '1', false,  ['class' => 'larger']) }}
							<span class="label">Weekly E-Newsletter</span>
						</label>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<label class="checkbox checkbox-lg">
							{{ Form::checkbox('special_offers', '1', false,  ['class' => 'larger']) }}
							<span class="label">Special Offers & Announcements</span>
						</label>
					</div>
					<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right my-4">
						<a href="" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Update Subscriptions</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
{!! Form::close() !!}
@endsection
@push('css')
<style>
	form div.required label.control-label:after {
		content: " * ";
		color: #a94442;
	}

	input.larger {
		width: 25px;
		height: 25px;
	}
</style>
@endpush




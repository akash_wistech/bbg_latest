@extends('frontend.app')
@section('content')

<x-contact-banner/>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<h3>Invoice History</h3>
				<br>
				<table class="table table-striped table-responsive">
					<thead>

						<tr>
							<th>Reference Number</th>
							<th>Name</th>
							<th>Company</th>
							<th>Sub Total</th>
							<th>Grand Total</th>
							<th>Pyment Status</th>
							<th>Invoice Date</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						if ($invoices<>null) {
							foreach ($invoices as $invoice) {
								// dd($invoice);
								// dd($invoice->company->title);
								?>
								<tr>
									<th scope="row">
										<?= $invoice->reference_no; ?>
										<a target="_blank" href="{{url('invoices/pdf/'.$invoice->id)}}" class=""><i class="fa fa-file-pdf-o"></i></a>
									</th>
									<td><?= $invoice->bill_to_name; ?></td>
									<td><?= $invoice->bill_to_company_name; ?></td>
									<td><?= $invoice->sub_total; ?></td>
									<td><?= $invoice->grand_total; ?></td>
									<td><?= getPaymentStatusLabelArr()[$invoice->payment_status] ?></td>
									<td><?= formatDate($invoice->final_invoice_date); ?></td>

									<td class="invoicelink">
										@if($invoice->payment_status!=1)
										<a href="{{url('pay-tabs/'.$invoice->id)}}">Pay Online</a>
										@endif
									</td>
								</tr>
								<?php
							}
						}
						?>

					</tbody>
				</table>
				<br>
				<div class="col-md-12 text-center pull-right">
					{{$invoices->links()}}
				</div>
			</div>

		</div>
	</div>
</section>

@endsection

<?php  
// dd("here");
$no_image = asset('assets/images/dummy-image-1.jpg');

?>

@extends('frontend.app')
@section('content')

<x-contact-banner/>



<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-7">
						<h3>members directory</h3>
					</div>
					<div class="col-12 col-sm-12 col-md-4 col-md-offset-1 col-lg-4 col-xl-4">
						{!! Form::open(array('url' => '/membership/directory', 'method' => 'get', 'id' => 'member-searcher','enableClientScript' => false)) !!}
						<div class="TopMostSearch1">
							<div class="input-group">
								{{ Form::text('search', $search, ['class'=>'form-control','placeholder'=>'I’m looking for....']) }}
								<span class="input-group-btn">
									<button class="MySearchBtn btn btn-default" type="submit">
										<i class="fa fa-search" aria-hidden="true"></i>
									</button>
								</span>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" class="text-center">
						<ul class="pagination flex-wrap">
							<li class="page-item">
								<a class="page-link" href="{{url('/membership/directory')}}">
									All
								</a>
							</li>
							<?php
							$letters = range('A', 'Z');
// dd($letters);  

							foreach ($letters as $letter) {
								?>
								<li class="page-item">
									<a class="page-link" href=" {{url('/membership/directory?search=').$letter}} ">
										<?= $letter; ?>
									</a>
								</li>
								<?php
							}
							?>
						</ul>
					</div>
				</div>



				<div class="row PaddingTopBtm30px">
					<?php
					if ($members <> null) {
						foreach ($members as $member) {
							$isContact = $member->checkmembercontact(Auth::user()->id, $member->id);
							?>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
								<div class="directorymember">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
											<img src="<?= ($member->image!=null) ? $member->image : $no_image ?>"
											class="img-fluid image_150" alt="">
										</div>
										<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
											<div class="directory" style="min-height: 120px;">
												<span class="pull-right memberContactIcon"
												id="memberContact<?= $member->id; ?>">
												<?php
												if($isContact <> null){?>
													<a href="javascript:;" class="add_member_contact add-false" id="<?= $member->id ?>" title="Remove From Contact" style="color: #bd1f2f !important;">
														<i class="fa fa-user-times" aria-hidden="true"></i>
													</a>
													<?php 
												}else{
													?>
													<a href="javascript:;" class="add_member_contact add-true" id="<?= $member->id ?>" title="Add to Contact" style="color: #228B22 !important;">
														<i class="fa fa-user-plus" aria-hidden="true"></i>
													</a>
													<?php
												}
												?>
											</span>

											

											<h2><?= $member->name ?></h2>

											<?php 
											$companies = getModuleSavedCompanies($member);
											foreach ($companies as  $company) {
												if ($company<>null) {
													if($company->company!=null){
														$companyName = $company->company->title;
														$designationRow = getPredefinedItem($company->job_title_id);
														$designation='';
														if($designationRow!=null && $designationRow->lang!=null)$designation=$designationRow->title;
														?>
														<h3>
															<?= $companyName; ?><br>
															<?= $designation; ?><br>
														</h3>
														<?php
													}
												}
											}
											?>

										</div>
										<span class="EventDetail">
											<a href="<?= url('/member/detail',['id'=>$member->id]) ?>">View Detail</a>
										</span>
										<?php if ($companies<>null && count($companies)>0) {
											foreach ($companies as $company) {
												?>
												<span class="EventDetail">
													<a href="<?= url('/company/members?id='.$company->company_id) ?>">Company Detail</a>
												</span>
												<?php
											} //die();
										}  ?>

									</div>
								</div>
							</div>
						</div>
						<?php
					}
				} else {
					?>
					<div class="col-md-12">
						<div class="col-md-12 alert alert-danger">No Record Found</div>
					</div>
					<?php
				}
				?>
			</div>



			<br>
		</div>
		<div class="col-md-12 text-center">
			{{$members->links()}}
		</div>
	</div>
</div>
</section>

@endsection

@push('css')
<style>
	.TopMostSearch1 {
		margin-top: 20px;
	}
</style>
@endpush



@push('js')
<script>
	$("body").find("a.add_member_contact").click(function(e) {
		e.preventDefault();  
		$(this).prop("disabled", true);

		var memberId = $(this).attr("id");
		var type = "add";  
// console.log(type);

if( $(this).hasClass("add-false")){
	type = "remove"; 
// console.log(type);
}

$.ajax({
	url: "/account/add-member-contact",
	type: "POST",
	data: {
		"_token": "{{ csrf_token() }}",
		id : memberId, type : type
	}, 
	success : function(res){ 

		if(res == 1){ 
			console.log("type");
			console.log(type);
			console.log(res);
			if(type == "add"){ 
				$("#"+memberId+".add_member_contact").removeClass("add-true");
				$("#"+memberId+".add_member_contact").addClass("add-false");  
				$("#"+memberId+".add_member_contact").css("color","#bd1f2f","!important"); 

				$("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
				$("#memberContact"+memberId+ " .fa").addClass("fa-user-times");

				$(this).attr("title", "Add to contact");

			} else{ 
				$("#"+memberId+".add_member_contact").removeClass("add-false");
				$("#"+memberId+".add_member_contact").addClass("add-true"); 
				$("#"+memberId+".add_member_contact").css("color","#228B22","!important"); 

				$("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
				$("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 

				$(this).attr("title", "Remove from contact");
			}              

		} 
	},
	error : function(data){
		console.log(data);
	}
}); 


e.stopImmediatePropagation(); 
return false;
});
</script>
@endpush








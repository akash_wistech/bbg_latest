<?php 
$no_image = asset('assets/images/dummy-image-1.jpg');
?>
@extends('frontend.app')
@section('content')
{!! Form::model($model, ['files' => true]) !!}

<style>
	.input-group-text{
		padding: 15px;
	}
</style>
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
					<div class="col-md-12 ">
						<div class="Heading text-left">
							<h3>Additional Member</h3><br/>
						</div>
						<div class="row">

							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
								{{ Form::label('full_name',__('common.full_name')) }}
								{{ Form::text('full_name', null, ['class'=>'form-control']) }}
							</div>

							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">								
								{{ Form::label('email',__('common.email')) }}
								{{ Form::text('email', null, ['class'=>'form-control']) }}
							</div>

							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
								{{ Form::label('phone',__('common.phone')) }}
								{{ Form::text('phone', null, ['class'=>'form-control']) }}
							</div>

							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
								{{ Form::label('emails[1]', __('nam-subscription::membership_type.secondry_email'))}}<span class="text-danger">*</span>
								{{ Form::text('emails[1]', null, ['class'=> 'form-control'])}}
							</div>

							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4">
								{{ Form::label('bill_to_name',__('common.bill_to_name')) }}
								{{ Form::text('bill_to_name', null, ['class'=>'form-control']) }}
							</div>

							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
								{{ Form::label('bill_to_company',__('common.bill_to_company')) }}
								{{ Form::text('bill_to_company', null, ['class'=>'form-control']) }}
							</div>

							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
								{{ Form::label('bill_to_address',__('common.bill_to_address')) }}
								{{ Form::textarea('bill_to_address', null, ['class'=>'form-control', 'rows'=>'3']) }}
							</div>

							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
								{{ Form::label('designation_id', __('nam-subscription::membership_type.designation'))}}
								{{ Form::select('designation_id', [''=>__('Select')]+JobTitlesListArr(), null, ['class'=> 'form-control'])}}
							</div>
							<x-customfields-frontend-nonta-fields :model="$model"/>
						</div>
						<div class="row">							
							<x-customfields-frontend-onlyta-fields :model="$model"/>
						</div>

						<div class="row">
							<x-namod-socialmedia-module-social-media-fields :model="$model"/>
						</div>

						<div class="row">
							<x-customtags-custom-tags-fields :model="$model"/>
						</div>

						<div class="row">
							<x-staffmanager-module-manager-field :model="$model"/>
						</div>

						<div class="row">
							<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
								<label class="docs_upload_label control-label" for="signupform-profile_image">Profile Image.</label>
								<div class="form-group">
									<a href="javascript:;"
									id="upload-document1"
									onclick="uploadAttachment(1)"
									data-toggle="tooltip"
									class="img-thumbnail"
									title="Upload Document">
									<i></i>
									<img src="<?= $no_image ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
									<br/>
								</a>
								<p class="image_upload_label">click Image to upload</p>
								{{ Form::hidden('image', '', array('id' => 'input-attachment1', 'maxlength' => true)) }}
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
							<label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of passport</label>
							<div class="form-group">
								<a href="javascript:;"
								id="upload-document2"
								onclick="uploadAttachment(2)"
								data-toggle="tooltip"
								class="img-thumbnail"
								title="Upload Document">
								<i></i>
								<img src="<?= $no_image ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
								<br/>
							</a>
							<p class="image_upload_label">click Image to upload</p>
							{{ Form::hidden('passport_copy', '', array('id' => 'input-attachment2', 'maxlength' => true)) }}
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
						<label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of residence visa</label>
						<div class="form-group">
							<a href="javascript:;"
							id="upload-document3"
							onclick="uploadAttachment(3)"
							data-toggle="tooltip"
							class="img-thumbnail"
							title="Upload Document">
							<i></i>
							<img src="<?= $no_image ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
							<br/>
						</a>
						<p class="image_upload_label">click Image to upload</p>
						{{ Form::hidden('residence_visa', '', array('id' => 'input-attachment3', 'maxlength' => true)) }}
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3" style="display: none;">
					<label class="docs_upload_label control-label" for="signupform-trade_licence">Passport size photograph (upload as JPEG)</label>
					<div class="form-group">
						<a href="javascript:;"
						id="upload-document4"
						onclick="uploadAttachment(4)"
						data-toggle="tooltip"
						class="img-thumbnail"
						title="Upload Document">
						<i></i>
						<img src="<?= $no_image ?>" width="100" alt="" title="" data-placeholder="no_image.png" />
						<br/>
					</a>
					<p class="image_upload_label">click Image to upload</p>
					{{ Form::hidden('passport_size_pic', '', array('id' => 'input-attachment4', 'maxlength' => true)) }}
				</div>
			</div>
		</div>



	</div>
	<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
		<button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Add Member</button>
	</div>
</div>
</div>
</div>
</div>
</section>
{!! Form::close() !!}
@endsection
@push('css')
<style>
	form div.required label.control-label:after {
		content: " * ";
		color: #a94442;
	}
</style>
@endpush

@push('js')
<script>
	var uploadAttachment = function (attachmentId) {

		$('#form-upload').remove();
		$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;">{{ csrf_field() }}<input type="file" name="file" value="" /></form>');
		$('#form-upload input[name=\'file\']').trigger('click');
		
		if (typeof timer != 'undefined') {
			clearInterval(timer);
		}
		timer = setInterval(function() {
			if ($('#form-upload input[name=\'file\']').val() != '') {
				clearInterval(timer);
				$.ajax({
					url: 'file-manager/uploadFrontend?parent_id=1',
					type: 'post',
					dataType: 'json',
					data: new FormData($('#form-upload')[0]),
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: function() {
						$('#upload-document' + attachmentId + ' img').hide();
						$('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
						$('#upload-document' + attachmentId).prop('disabled', true);
					},
					complete: function() {
						$('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
						$('#upload-document' + attachmentId).prop('disabled', false);
						$('#upload-document' + attachmentId + ' img').show();
					},
					success: function(json) {
						if (json['error']) {
							alert(json['error']);
						}

						if (json['success']) {
							console.log(json);
							$('#upload-document' + attachmentId + ' img').prop('src', json.href);
							$('#input-attachment' + attachmentId).val(json.href);
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});

			}
		}, 500);
	}
</script>
@endpush
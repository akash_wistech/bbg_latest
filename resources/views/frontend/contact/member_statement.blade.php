@extends('frontend.app')
@section('content')

<x-contact-banner/>

<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading ">
				<h3>Statements</h3>
				<br>
				<table class="table table-striped table-responsive">
					<thead>

						<tr>
							<th>Date</th>
							<th>Details</th>
							<th>Amount</th>
							<th>Payments</th>
							<th>Balance</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($statements as $key => $statement) {
							// dd($statement);
							if ($statement['type']=='invoice') {
								$TotalBalence = $TotalBalence+$statement['grand_total'];
							}
							if ($statement['type']=='payment') {
								$TotalBalence = $TotalBalence-$statement['grand_total'];
								$invoicesToPayment = \Wisdom\Sales\Models\Invoices::where('id',$statement['id'])->select('reference_number')->first();
							}   ?>
							<tr>
								<td><?= $statement['date'] ?></td>
								<td><?= (($statement['type']=='invoice')? ucwords($statement['type']).' ('.$statement['reference_number'].') due to - '.$statement['due_date'] : ucwords($statement['type'].' to invoice '.$invoicesToPayment['reference_number']) ) ?></td>
								<td><?= (($statement['type']=='invoice')? $statement['grand_total'] : '') ?></td>
								<td><?= (($statement['type']=='payment')? $statement['grand_total'] : '') ?></td>
								<td><?= number_format((float)$TotalBalence, 2, '.', ''); ?></td>
							</tr>
						<?php } ?>

						<tr>
							<td></td>
							<td></td>
							<td><b>Balance Due</b></td>
							<td></td>
							<td><?= $TotalBalence ?></td>
						</tr>
						
					</tbody>
				</table>
				<br>
				<div class="col-md-12 text-center pull-right">
					{{$statements->links()}}
				</div>
			</div>

		</div>
	</div>
</section>

@endsection
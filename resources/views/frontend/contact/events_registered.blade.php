<?php
use Wisdom\Sales\Models\Invoices;
use App\Models\Contact;
?>
@extends('frontend.app')
@section('content')

<x-contact-banner/>


<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="Heading text-center">
					<h3 class="pb-4">Events Registered</h3>
				</div>
				<div class="form-group no-more-tables" style=" overflow-x:auto;">
					<table id="memberEventsRegTable" class="table">
					<thead class="text-left">
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Event Title</th>
							<th>Event Date</th>
							<th>Registration Status</th>
							<th>Amount</th>
							<th>Invoice</th>
							<th>Payment Status</th>

						</tr>
					</thead>
					<tbody>
						<?php
						if ($events <> null) {
							$count = 1;
							foreach ($events as $event) {
								if ( GetInvoiceIdByEventSubscribe($event)!=null) {
								// dd(GetInvoiceIdByEventSubscribe($event));
								$user = Contact::where(['id'=>$event->user_id])->first();
								// dd($user);
								// dd($event->payment_status);
								$event_detials = null;
								if ($event->eventData <> null) {
									// dd("here");
									$event_detials = $event->eventData;
									// echo "<pre>"; print_r($event_detials); echo "</pre>"; die();
								}
								$invoices = null;
								// dd($event->fee_paid);
								if ($event->fee_paid > 0) {
									// dd($event->gen_invoice);


										$invoices_id=\Wisdom\Sales\Models\ModuleInvoice::where(['module_type'=>$event->moduleTypeId, 'module_id'=>$event->id])->select('invoice_id')->first();
											$invoices = Invoices::find($invoices_id['invoice_id']);
								}
								// dd($invoices);
								$EventSubscriptionInvoice = GetInvoiceIdByEventSubscribe($event);
								?>

								<tr>
									<td class="text-center"><?= $count++; ?></td>
									<td class="text-center">
										<?= ($user<>null)?$user->name: ' ' ?>
									</td>
									<td class="text-left"><?= ($event_detials <> null && $event_detials) ? $event_detials->title : ''; ?></td>
									<td class="text-center"><?= ($event_detials <> null && $event_detials) ? formatDate($event_detials->event_startDate) : ''; ?></td>
									<td class="text-center">
										<span class="text-center"><?= ucwords($event->status); ?></span>
									</td>
									<td class="text-center">
										<span class="pull-right text-center pricebox">

											<?=  (($EventSubscriptionInvoice!=null) ? $EventSubscriptionInvoice->grand_total : 'N/A'  )?></span>
									</td>
									<td class="invoicelink text-center">
										<a href="{{ (($EventSubscriptionInvoice!=null) ? url('/invoices/pdf/'.$EventSubscriptionInvoice->id) : 'N/A'  )   }}" title="View Invoice" target="_blank">view</a>
									</td>
									<td>Payment stats</td>

								</tr>

								<?php
							}
						}
					}
						?>
					</tbody>
				</table>
				<br>
				<div class="col-md-12 text-center pull-right">
					{{$events->links()}}

				</div>
			</div>
		</div>
	</div>
</div>
</section>


@endsection

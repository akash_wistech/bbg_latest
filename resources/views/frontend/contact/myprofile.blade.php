@extends('frontend.app')
@section('content')

<?php
// dd($membershipType->service->title);

// $userType = NaeemAwan\ModuleSubscription\Models\SubscriptionUsers::where(['user_id'=>$model->id])->first();
// dd($userType);
// $usertype=getPredefinedListItemsArr(2)[$userType->user_type];
// dd($usertype);

// dd($CustomFieldModel->moduleTypeId);


$no_image = asset('assets/images/dummy-image-1.jpg');
if ($model<>null) {
	$contactCustomFields = getCustomFieldsAgaintsContact('contact', $model->id);
}
$membershipType = NaeemAwan\ModuleSubscription\Models\MembershipType::where(['id'=>$model->active_subscription_id])->first();
$nonTextAreaColumns=getNonTextAreaColumns($CustomFieldModel->moduleTypeId);
?>


<x-contact-banner/>


<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<h3>My Profile</h3>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
					<h2>Personal Information
						<span class="float-right">
							<a href="" class="d-none"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
						</span>
					</h2>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
					<div class="row PaddingTopBtm30px">
						<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
							<img src="<?= ($model->image) ? $model->image : $no_image; ?>"
							class="img-fluid" alt="">
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
							<div class="profile">
								<h2><?= ($model <> null) ? $model->name : ""; ?></h2>
								<h3><?= ($model <> null) ? $model->designation : ""; ?></h3>
								<p>
									<strong>Email</strong>
									- <?= ($model<>null ? '<a href="mailto:'.$model->email.'">'.$model->email.'</a>' :'') ?>
									<br>
									<strong>Contact Number</strong>
									- <?= ($model<>null ? '<a href="tel:'.$model->phone.'">'.$model->phone.'</a>' :'') ?>
									<br>

									@if($nonTextAreaColumns->isNotEmpty())
									@foreach($nonTextAreaColumns as $nonTextAreaColumn)
									@php
									$value=getInputFielSavedValueForDetail($model,$nonTextAreaColumn);
									if(is_array($value))$value = implode(", ",$value);
									@endphp
									<strong>{{$nonTextAreaColumn['title']}}</strong> - {{ ($value!=null)? $value : 'N\A' }} <br>
									@endforeach
									@endif

									<?php
									$socialMediaLinks = getModuleSocialMediaQuery($model->id,$model->moduleTypeId);
									foreach ($socialMediaLinks as $socialMediaLink) {
										// dd($socialMediaLink);
										?>
										<strong><?=$socialMediaLink['title']?></strong>
										- <a href="<?=$socialMediaLink['link']?>"><?=$socialMediaLink['link']?></a>
										<br>
										<?php
									}
									?>



									<strong>Company Type</strong> - legal <br/>
								</p>
							</div>
						</div>
						<div class="profile col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
							<p>
								<strong>Status</strong>
								- <?= getStatusTextArr()[$model->status]; ?>
								<br>

								<strong>Group</strong>
								-  <?= ($membershipType)<>null ? $membershipType->service->title : '' ?>
								<br>
								<!-- <strong>Account Type</strong>
								-  <?php //echo $usertype ?>
								<br> -->
								<!-- <strong>Registration Date</strong>
								- Tue, 30 Nov, 2004
								<br>

								<strong>Expiry Date</strong>
								- Mon, 06 Jun, 2022
								<br>

								<strong>Last Renewal Date</strong>
								- Sun, 06 Jun, 2021
								<br> -->

							</p>
						</div>
					</div>
				</div>
				<?php
				$companies = getRowMultipleCompaniesQuery('contact',$model->id);
				// dd($companies);
				if ($companies<>null && count($companies)>0) {
					?>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
						<h2>
							Company Information
							<span class="float-right">
								<a href="/membership/update" class="d-none"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit </a>
							</span>
						</h2>
					</div>

					<?php
					foreach ($companies as $company) {
						// dd($company);
						$nonTextAreaColumnsForCompany=getNonTextAreaColumns($company->moduleTypeId);
						$textAreaColumnsForCompany=getTextAreaColumns($company->moduleTypeId);
						// dd($nonTextAreaColumnsForCompany);
						?>
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
							<div class="row PaddingTopBtm30px">
								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
									<img src="<?= ($company <> null && !empty($company->logo)) ? $company->logo : $no_image; ?> " class="img-fluid" alt="">
								</div>
								<div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
									<div class="profile">
										<h2><?= $company->title ?></h2>
										<p>
											@if($nonTextAreaColumnsForCompany->isNotEmpty())
											@foreach($nonTextAreaColumnsForCompany as $nonTextAreaColumn)
											@php
											$value=getInputFielSavedValueForDetail($company,$nonTextAreaColumn);
											if(is_array($value))$value = implode(", ",$value);
											@endphp
											<strong>{{$nonTextAreaColumn['title']}}</strong> - {{ ($value!=null)? $value : 'N\A' }} <br>
											@endforeach
											@endif
										</p>

										<p>
											@if($textAreaColumnsForCompany->isNotEmpty())
											@foreach($textAreaColumnsForCompany as $textAreaColumn)
											@php
											$value=getInputFielSavedValueForDetail($company,$textAreaColumn);
											if(is_array($value))$value = implode(", ",$value);
											@endphp
											<h2>{{ $textAreaColumn['title'] }}</h2>
											<p >{!! $value !!}</p>
											@endforeach
											@endif
										</p>
									</div>
								</div>
							</div>
						</div>
						<?php
					}
				}
				?>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading d-none ">
					<h2>Interests
                        <!--<span class="float-right">
                            <a href="#">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                            </a>
                        </span>-->
                    </h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                	<div class="row PaddingTopBtm30px">
                		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                			<div class="profile text-left">
                				{!! Form::model($CustomFieldModel, ['files' => true]) !!}

                				<div class="col-md-12">

                					<?php echo onlyIntrestedCategories($CustomFieldModel); ?>


                				</div>
                				<div class="col-md-12 ">

                					<?php
                					//if ($categories <> null && count($categories) > 0) {
                						// dd("here");
                						//foreach ($categories as $key => $value) {
                					?>
                					<!-- <div class="col-md-4 col-xl-4 form-group" style="float: left;">
                						<label class="label_checkbox_class"
                						for="interested_category_<?php //echo $key ?>">
                						<?php //echo $value ?>
                						<input name="interested_category[]"
                						id="interested_category_<?php //echo $key ?>" class="styled"
                						value="<?php //echo $key ?>" class="category_checkbox"
                						type="checkbox"/>
                						<span class="checkmark"></span>
                					</label>
                				</div> -->
                				<?php
                					//}
                				//}
                				?>
                			</div>

                			<div class="col-md-12 d-none" style="margin-top: 50px;float: right;">
                				<button type="submit" class="Mybtn pull-right">Update</button>
                			</div>
                			{!! Form::close() !!}
                		</div>
                	</div>
                </div>
            </div>

            <br>
        </div>

    </div>
</div>
</section>

@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="/theme/bbg/resources/css/checkbox-style.css" />
<style>
	.checkbox{
		margin-right: 50px;
	}
</style>
@endpush

@extends('frontend.app') @section('content')
<x-contact-banner/>
<section class="MainArea">
	<div class="container LeftArea">
		<div class="row ">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading ">
				<h3>member’s contact</h3>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
						<h2>Personal Contact Information</h2>
					</div>
				</div>

				
				<div class="row">
					<?php
					use App\Models\MemberContacts;
					$contacts = App\Models\MemberContacts::where(['member_id'=>Auth::user()->id])->get();

					// dd($contacts);

					if ($contacts<>null) {
						$no_image = asset('assets/images/dummy-image-1.jpg');
						foreach ($contacts as $contact) {
							// dd($contact);
							$user = App\Models\Contact::where(['id'=>$contact->contact_id])->first();
							$isContact = MemberContacts::where(['member_id' => Auth::user()->id, 'contact_id' => $contact->contact_id])->first();

							?>

							<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 px-4">
								<div class="PaddingTopBtm30px darkbg row mx-4">
									<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
										<img src="<?= ($user->image <> "" ) ? $user->image : $no_image; ?>"
										class="img-fluid" alt="Image">
									</div>
									<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
										<div class="profile">
											<span class="pull-right memberContactIcon"
											id="memberContact<?= $user->id; ?>">
											<?php
											if($isContact <> null){?>
												<a href="javascript:;" class="add_member_contact add-false" id="<?= $user->id; ?>" title="Remove From Contact" style="color: #bd1f2f !important;">
													<i class="fa fa-user-times" aria-hidden="true"></i>
												</a>
												<?php 
											}else{
												?>
												<a href="javascript:;" class="add_member_contact add-true" id="<?= $user->id; ?>" title="Add to Contact" style="color: #228B22 !important;">
													<i class="fa fa-user-plus" aria-hidden="true"></i>
												</a>
												<?php
											}

											?>

										</span>
										<h2><a href="<?= url('/member/detail',['id'=>$user->id]) ?>"><?= $user->name; ?></a></h2>

										<?php 

										$companies = getModuleSavedCompaniesByModuleId($contact->id,'contact');
													// dd($companies);
										foreach ($companies as  $company) {
											if ($company<>null) {
												if($company->company!=null){
													$companyName = $company->company->title;
																			// dd($companyName);
													$designationRow = getPredefinedItem($company->job_title_id);
													$designation='';
													if($designationRow!=null && $designationRow->lang!=null)$designation=$designationRow->title;
																			// dd($designation);
													?>
													<h3><?= $designation; ?> at <?= $companyName; ?></h3>
													<p>
														<strong>Designation</strong> - <?= $designation; ?> <br>
														<strong>Company Name</strong> - <?= substr(trim($companyName), 0, 40); ?> <br>
														<?php
													}
												}
											}

											?>														
											<strong>Mobile Number</strong> - <?= $user->phone; ?> <br>
											<strong>Email Address</strong> - <?= $user->email ?> <br>
										</p>
									</div>
								</div>
							</div>
						</div>

						<?php
					}
				}
				?>
				<br>
			</div>
		</div>
	</div>
</div>
</section>
@endsection

@push('css')
<style>
	.darkbg {
		background: #e2e2e2;
		min-height: 270px;
		margin: 10px 0px;
	}
</style>
@endpush

	@push('js')
	<script>
		$("body").find("a.add_member_contact").click(function(e) {
			e.preventDefault();  
			$(this).prop("disabled", true);

			var memberId = $(this).attr("id");
			var type = "add";  
// console.log(type);

if( $(this).hasClass("add-false")){
	type = "remove"; 
// console.log(type);
}

$.ajax({
	url: "/account/add-member-contact",
	type: "POST",
	data: {
		"_token": "{{ csrf_token() }}",
		id : memberId, type : type
	}, 
	success : function(res){ 

		if(res == 1){ 
			console.log("type");
			console.log(type);
			console.log(res);
			if(type == "add"){ 
				$("#"+memberId+".add_member_contact").removeClass("add-true");
				$("#"+memberId+".add_member_contact").addClass("add-false");  
				$("#"+memberId+".add_member_contact").css("color","#bd1f2f","!important"); 

				$("#memberContact"+memberId+ " .fa").removeClass("fa-user-plus");    
				$("#memberContact"+memberId+ " .fa").addClass("fa-user-times");

				$(this).attr("title", "Add to contact");

			} else{ 
				$("#"+memberId+".add_member_contact").removeClass("add-false");
				$("#"+memberId+".add_member_contact").addClass("add-true"); 
				$("#"+memberId+".add_member_contact").css("color","#228B22","!important"); 

				$("#memberContact"+memberId+ " .fa").addClass("fa-user-plus");    
				$("#memberContact"+memberId+ " .fa").removeClass("fa-user-times"); 

				$(this).attr("title", "Remove from contact");
			}              

		} 
	},
	error : function(data){
		console.log(data);
	}
}); 


e.stopImmediatePropagation(); 
return false;
});
</script>
@endpush
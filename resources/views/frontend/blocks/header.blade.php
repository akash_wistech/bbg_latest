<header id="topHeaderSection" style="margin-bottom:40px;">
    <div id="fixedTopNav">
        <div class="TopMost">
            <div class="container">
                <div class="row hidden-lg-up">
                    <div class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a href="#" title="Member Login" data-original-title="Member Login" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-user usericonsize" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>


                    <div class="col-1 col-sm-1 col-md-1  display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a target="_blank" href="/news/subscribe-newsletter"><i class="fa fa-envelope usericonsize" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-1 col-sm-1 col-md-1  display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a id="search_box_on_mobile" class="request_experties" href="#" data-toggle="modal" data-target="#myModalSearchOnMobile"><i class="fa fa-search-plus usericonsize" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>

                    <div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-3 display-block">

                    </div>
                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                        <div class="MembershipBtn PaddingTopBtm">
                            <a href="/site/apply-membership">apply</a>
                        </div>
                    </div>

                </div>
                <!--- ########################## -->

                <!-- start header -->
                <div class="row hidden-md-down">
                    <?php 
                    if (!Auth::check()) {?>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                            <div class="MembershipBtn PaddingTopBtm">
                                <a href="{{url('applyformembership')}}">apply for membership</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-8 col-sm-8 col-md-4 col-lg-2 col-xl-2 display-block">
                        <div class="MemberLogin PaddingTopBtm">
                           <?php
                           if (Auth::check()) {
                            $model = App\Models\Contact::where(['id'=>Auth::user()->id])->first();
                            ?>
                            <a href="{{url('account/profile')}}" data-toggle="tooltip" title="<?= $model->name; ?>">
                                <i class="fa fa-user-o usericonsize" aria-hidden="true" ></i> 
                                Welcome, <?php echo strlen($model->name) > 5 ? substr($model->name,0,6)."..." : $model->name;?>
                            </a>
                            <?php
                        } else {
                            ?>
                            <a href="#" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-user usericonsize" aria-hidden="true"></i> Member Login
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <?php
                if (Auth::check()) {
                    ?>
                    <div class="col-4 col-sm-4 col-md-4 col-lg-2 col-xl-2 display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a href="{{url('/fend/logout')}}"  data-method="post"><i class="fa  fa-sign-out usericonsize" aria-hidden="true"></i> Logout</a>          
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 col-xl-2 display-block">
                        <div class="MemberLogin PaddingTopBtm">
                            <a id="request_experties" class="request_experties" href="#" data-toggle="modal" data-target="#myModalRequestExpertise">
                                <i class="fa fa-search usericonsize" aria-hidden="true"></i> Request Expertise
                            </a>
                        </div>
                    </div>
                    <?php 
                }
                ?>

                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3 display-block">
                    <div class="MemberLogin PaddingTopBtm">
                        <a target="_blank" href="{{url('subscribe-newsletter')}}"><i class="fa fa-envelope" aria-hidden="true"></i> Subscribe to updates</a>
                    </div>
                </div>

                <div class="hidden-md-down col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3 display-block">
                    <form id="searcher" class="" action="{{url('/')}}" method="get">    <div class="TopMostSearch">
                        <div class="input-group">
                            <input type="search" name="search" class="form-control" placeholder="Find News and Events ..."  style="height:34px; width: 200px;">
                            <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <!-- header end -->
        </div>
    </div>

    <!-- sub header -->
    <div class="SiteHeader">
        <div class="container">
            <div class="row">
                <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 ">
                    <a href="{{route('frontend')}}"><img class="MyAppLogo" src="https://d2a5i90cjeax12.cloudfront.net/BBG_NEW_WEBSITE_PLACEHOLDER_RGB_200x60px5f6a012c5ec34.jpg" alt="BBG-Dubai"></a>
                </div>
                <div class="hidden-md-down col-lg-9 col-xl-9">
                    <nav class="float-left">
                        <ul id="menu">

                            <li>
                                <a class="prett" href="{{url('/slug-pages?slug=about-bbg')}}">About BBG</a>
                                <ul class="menus">
                                    <li class="">
                                        <a class="home" href="{{route('sponsors-fend')}}">OUR PARTNERS AND SPONSORS</a>
                                    </li>
                                    <li class="">
                                        <a class="home" href="{{route('board-members-info')}}">OUR BOARD AND INDUSTRY LEADS</a>
                                    </li>
                                    <li class="">
                                        <a class="home" href="{{route('operations-info')}}">OUR BUSINESS TEAM</a>
                                    </li>
                                    <li class="">
                                        <a class="home" href="{{url('/future-vision-collective')}}">OUR FUTURE VISION COLLECTION</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a class="prett" href="{{url('/slug-pages?slug=about-bbg-membership')}}">Membership</a>
                                <ul class="menus">
                                    <li class="">
                                        <a class="home" href="{{url('/slug-pages?slug=about-bbg-membership')}}">ABOUT BBG MEMBERSHIP</a>
                                    </li>

                                    <?php
                                    if (!Auth::check()) {?>
                                        <li class="">
                                            <a class="home" href="{{url('applyformembership')}}">JOIN US</a>
                                        </li>
                                        <?php
                                    } ?>


                                    <li class="">
                                        <a class="home" href="{{url('membership/offers')}}">MEMBER OFFERS</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a class="prett" href="{{route('event-fend')}}">Events</a>
                                <ul class="menus">
                                    <li class="">
                                        <a class="home" href="{{route('event-fend')}}">UPCOMING EVENTS</a>
                                    </li>
                                    <li class="">
                                        <a class="home" href="{{url('event-fend?type=past')}}">PAST EVENTS</a>
                                    </li>
                                </ul>

                            </li>
                            <li>
                                <a class="prett" href="{{url('/membership/directory')}}">Directory</a>
                                <ul class="menus">
                                    <li class="">
                                        <a class="home" href="{{url('/membership/directory')}}">MEMBERS DIRECTORY</a>
                                    </li>
                                    <li class="">
                                        <a class="home" href="{{url('/company/directory')}}">COMPANY DIRECTORY</a>
                                    </li>
                                    <li class="">
                                        <a class="home" href="{{url('/slug-pages?slug=gcc-bbg-links')}}">GCC BBG CONTACT DETAILS </a>
                                    </li>


                                </ul>

                            </li>

                            <li>
                                <a class="prett" href="{{route('news-fend')}}">News</a>
                                <ul class="menus">
                                    <li><a class="home" href="{{url('news-fend?type='.getSetting('member_news_id'))}}">MEMBER NEWS</a></li>
                                    <li><a class="home" href="{{url('news-fend?type='.getSetting('expo_2020_id'))}}">EXPO 2020</a></li>
                                    <li><a class="home" href="{{url('news-fend?type='.getSetting('guest_articles_id'))}}">GUEST ARTICLES</a></li>
                                    <li class=""><a class="home" href="{{url('/view-from-chair-detail')}}">VIEW FROM CHAIR</a></li>


                                </ul>

                            </li><li>
                                <a class="" href="/contact-us">Contact Us</a>
                                <ul class="menus">

                                </ul>

                            </li></ul></nav>
                        </div>


                        <div class="col-8 col-sm-8 col-md-8 hidden-lg-up">
                            <div id="mySidenav" class="sidenav">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>

                                <a id="coll1" href="#">About BBG +</a>
                                <div class="submenu-1" style="display: none" id="coll1-a">
                                    <a href="/sponsors">OUR PARTNERS AND SPONSORS</a>
                                    <a href="/site/board-members-info">OUR BOARD AND INDUSTRY LEADS</a>
                                    <a href="/site/operations-info">OUR BUSINESS TEAM</a>
                                </div>
                                <a id="coll2" href="#">Membership +</a>
                                <div class="submenu-1" style="display: none" id="coll2-a">
                                    <a href="/pages/view-by-seo-url?seo_url=About BBG Membership">ABOUT BBG MEMBERSHIP</a>
                                    <a href="/apply/membership">JOIN US</a><a href="/membership/offers">MEMBER OFFERS</a>
                                </div>
                                <a id="coll3" href="#">Events +</a>
                                <div class="submenu-1" style="display: none" id="coll3-a">
                                    <a href="/events">UPCOMING EVENTS</a>
                                    <a href="/events?type=past">PAST EVENTS</a>
                                </div>
                                <a id="coll4" href="#">Directory +</a>
                                <div class="submenu-1" style="display: none" id="coll4-a">
                                    <a href="/membership/directory">MEMBERS DIRECTORY</a>
                                    <a href="/company/directory">COMPANY DIRECTORY</a>
                                    <a href="/pages/view-by-seo-url?seo_url=GCC BBG">GCC BBG CONTACT DETAILS </a>
                                </div>
                                <a id="coll5" href="#">News +</a>
                                <div class="submenu-1" style="display: none" id="coll5-a">
                                    <a class="home" href="/news?category=2">MEMBER NEWS</a>
                                    <a class="home" href="/news?category=1">EXPO 2020</a>
                                    <a class="home" href="/news?category=3">GUEST ARTICLES</a>
                                    <a href="/site/view-from-chair">VIEW FROM CHAIR</a>
                                </div>
                                <a href="{{route('contact-us')}}">Contact Us</a>
                            </div>
                            <div class="text-right">
                                <span style="font-size:30px;cursor:pointer;" onclick="openNav()">☰</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- The Modal -->
                <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Members Login</h4>
                            </div>
                            {{ Form::open(array('url' => '/fend/login')) }}
                            <div class="modal-body">
                                <input type="hidden" name="_csrf-frontend" value="l--IFh00HfOHneArVG7hbu-nzOlAbN5kZ2d0hRtYd_Xiu-58UVhTtujpomIwApQnmM6igxUUkg84Hh7NKQoSog==">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="form-group">
                                            <input type="email" required="" id="accountloginform_user" class="form-control" name="email" autofocus="" placeholder="Please Enter Email" aria-required="true">
                                            <p class="help-block help-block-error"></p>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" required="" id="accountloginform_password" class="form-control" name="password" placeholder="Enter your password" aria-required="true">
                                        </div>
                                        <div id="ajax_login_errors" class="alert hidden row"></div>
                                        <button type="submit" id="submitModelLogin" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o" name="LOGIN TO YOUR ACCOUNT">Sign In
                                        </button>
                                        <a href="/site/request-password-reset">
                                            <button class="Mybtn " type="button">Forgot Password</button>
                                        </a>
                                    </div>
                                </div>                 
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary secondary_button" data-dismiss="modal" style="background: #bd1f2f !important;color: #ffffff !important;background-color: #bd1f2f !important;border-color: #bd1f2f !important;">Close</button>
                            </div>
                            {{ Form::close() }}

                        </div>
                    </div>
                </div>

                <!-- The Modal for request expertise-->
                <div class="modal fade" id="myModalRequestExpertise" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Request Expertise</h4>
                                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form id="model_request_experties" class="c_model_request_experties" action="{{url('search-experties')}}" method="get">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                            <div class="form-group">
                                                <input type="text" required="" id="search_request_experties" class="form-control" name="search_request_experties" autofocus="" placeholder="Keyword Search e.g Consultancy, IT, Director Communications etc.." aria-required="true">
                                                <p class="help-block help-block-error"></p>
                                            </div>
                                            <button type="submit" id="submitModelSearchExperties" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Search Expertise
                                            </button>
                                        </div>
                                    </div>
                                </form>                    </div>
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary secondary_button" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="myModalSearchOnMobile" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Search News &amp; Events</h4>
                                    <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 " id="searchMobile">
                                        <form id="searcher" class="" action="/" method="get">
                                            <div class="TopMostSearch" style="padding-top: 4px;">
                                                <div class="input-group">
                                                    <input type="search" name="search" class="form-control" value="" placeholder="Find News and Events ...">

                                                </div>
                                                <div id="ajax_login_errors" class="alert hidden row"></div>
                                                <br>
                                                <button class="Mybtn  btn btn-default" type="submit">
                                                    Search <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>

                                            </div>
                                        </form>                            </div>
                                    </div>
                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary secondary_button" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end sub header -->
                </div>
            </header>
            <div style="clear: both"></div>








<!-- comment this and then event calender not working -->
<!-- <script src="{{asset('theme/bbg/resources/js/jquery-1.12.4.js')}}"></script> -->

<!-- add this and jquery not defined  but work registeration event-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<script src="{{asset('theme/bbg/resources/js/bootstrap.js')}}"></script>

<script src="{{asset('theme/bbg/resources/js/imported/DateTimePicker.min.js')}}"></script>
<script src="{{asset('/theme/bbg/resources/miSlider/js/mislider.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jcarousel/0.3.7/jquery.jcarousel-autoscroll.min.js"></script>
<script src="{{asset('theme/bbg/resources/calender/tempust.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/jquery.flexisel.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/swal.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/waiting.js')}}"></script>
<script src="{{asset('theme/bbg/resources/DataTables/datatables.min.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/header_form_submit.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/footer.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/events_registeration.js?v=1.0')}}"></script>
<script src="{{asset('//platform.linkedin.com/in.js')}}"></script>
<script src="https://platform.twitter.com/widgets.js"></script>
<script src="{{asset('theme/bbg/resources/Monthly-master/js/monthly.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/jssocials.js')}}"></script>
<script src="{{asset('theme/bbg/resources/js/jquery.flexisel.js')}}"></script>
<script src="https://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
<script src="{{asset('theme/bbg/resources/js/multislider.js')}}"></script>



<script>
	var uploadAttachment = function (attachmentId) {
		$('#form-upload').remove();
		$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" value="" /></form>');

		$('#form-upload input[name=\'file\']').trigger('click');

		if (typeof timer != 'undefined') {
			clearInterval(timer);
		}

		timer = setInterval(function () {
			if ($('#form-upload input[name=\'file\']').val() != '') {
				clearInterval(timer);

				$.ajax({
					url: '{{url('member/file-manager/uploadFrontend')}}?parent_id=1',
					type: 'post',
					dataType: 'json',
					data: new FormData($('#form-upload')[0]),
					cache: false,
					contentType: false,
					processData: false,
					beforeSend: function () {
						$('#upload-document' + attachmentId + ' img').hide();
						$('#upload-document' + attachmentId + ' i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
						$('#upload-document' + attachmentId).prop('disabled', true);
					},
					complete: function () {
						$('#upload-document' + attachmentId + ' i').replaceWith('<i></i>');
						$('#upload-document' + attachmentId).prop('disabled', false);
						$('#upload-document' + attachmentId + ' img').show();
					},
					success: function (json) {
						if (json['error']) {
							alert(json['error']);
						}

						if (json['success']) {
							console.log(json);
							$('#upload-document' + attachmentId + ' img').prop('src', json.href);
							$('#input-attachment' + attachmentId).val(json.href);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				});
			}
		}, 500);
	}
</script>
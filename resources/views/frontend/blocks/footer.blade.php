<footer>
  <section class="footerarea">
    <div class="container">
      <div class="row footerwidgets">
        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
          <div class="column1">
            <h2>Contact</h2>
            <p>Telephone: <?= getSetting('telephone') ? getSetting('telephone') : ''; ?><br>
              Email: <a href="mailto:<?= getSetting('admin_email') ? getSetting('admin_email') : ''; ?>"><?= getSetting('admin_email') ? getSetting('admin_email') : ''; ?></a><br>
            </p>
            <br>
            <p>
              <a style="background: #cd2c47" target="_blank" class="Mybtn" href="/blog-posts">
                Help <i class="fa fa-question-circle" aria-hidden="true"></i>
              </a>
            </p>
            <br>
            <p>
              <a target="_blank" class="Mybtn" href="{{url('/slug-pages?slug=privacy-policy')}}" style="background: #cd2c47; margin-top: 10px;">Privacy Policy</a>
            </p>
            <br>
            <p>
              <script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
              <span class="IN-widget" data-lnkd-debug="<script type=&quot;IN/FollowCompany+init&quot; data-id=&quot;1043402&quot; data-counter=&quot;right&quot;></script>" style="display: inline-block; line-height: 1; vertical-align: bottom; padding: 0px; margin: 0px; text-indent: 0px;"><iframe id="{&quot;xdOrigin&quot;:&quot;http://bbg_frontend.local&quot;,&quot;xdChannel&quot;:&quot;38700163-78dc-47dd-a8bb-64827eeed0fe&quot;,&quot;framework&quot;:&quot;@linkedin/xdoor-sdk&quot;,&quot;version&quot;:&quot;0.1.156&quot;,&quot;debug&quot;:false}" name="{&quot;xdOrigin&quot;:&quot;http://bbg_frontend.local&quot;,&quot;xdChannel&quot;:&quot;38700163-78dc-47dd-a8bb-64827eeed0fe&quot;,&quot;framework&quot;:&quot;@linkedin/xdoor-sdk&quot;,&quot;version&quot;:&quot;0.1.156&quot;,&quot;debug&quot;:false}" src="https://www.linkedin.com/pages-extensions/FollowCompany?id=1043402&amp;counter=right&amp;xdOrigin=http%3A%2F%2Fbbg_frontend.local&amp;xdChannel=38700163-78dc-47dd-a8bb-64827eeed0fe&amp;xd_origin_host=http%3A%2F%2Fbbg_frontend.local" class="IN-widget IN-widget--iframe" scrolling="no" allowtransparency="true" frameborder="0" border="0" width="1" height="1" style="vertical-align: bottom; width: 146px; height: 23.8889px;"></iframe></span>
            </p>
            <br>
          </div>
        </div>

        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
          <div class="column1">
            <h2>Address</h2>
            <p><?= getSetting('company_address') ? nl2br(getSetting('company_address')) : ''; ?></p>
          </div>
          <span class="socialmedia">
            <?php
            if (getSetting('facebook') && !empty(getSetting('facebook'))) {
              ?>
              <a href="<?= getSetting('facebook'); ?> ">
                <i class="fa fa-facebook-square" aria-hidden="true"></i>
              </a>

              <?PHP
            }
            ?>
            <?php
            if (getSetting('linkedin') && !empty(getSetting('linkedin'))) {
              ?>
              <a href="<?= getSetting('linkedin'); ?> ">
                <i class="fa fa-linkedin-square" aria-hidden="true"></i>
              </a>

              <?PHP
            }
            ?>
            <?php
            if (getSetting('twitter') && !empty(getSetting('twitter'))) {
              ?>
              <a href="<?= getSetting('twitter'); ?> ">
                <i class="fa fa-twitter-square" aria-hidden="true"></i>
              </a>

              <?PHP
            }
            ?>

            <?php
            if (getSetting('instagram') && !empty(getSetting('instagram'))) {
              ?>
              <a href="<?= getSetting('instagram'); ?> ">
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </a>

              <?PHP
            }
            ?>

            <?php
            if (getSetting('youtube') && !empty(getSetting('youtube'))) {
              ?>
              <a href="<?= getSetting('youtube'); ?> ">
                <i class="fa fa-youtube-square" aria-hidden="true"></i>
              </a>

              <?PHP
            }
            ?>

            <?php
            if (getSetting('pinterest') && !empty(getSetting('pinterest'))) {
              ?>
              <a href="<?= getSetting('pinterest'); ?> ">
                <i class="fa fa-pinterest-square" aria-hidden="true"></i>
              </a>

              <?PHP
            }
            ?>

          </span>
        </div>



        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
          <div class="column1">
            <h2>Join our mailing list</h2>
            <p>Get the latest updates</p>
            <div class="input-group width90percent">
              <input type="text" id="email_subscribe_f_name" name="f_name" class="form-control subscribeEmail" placeholder="Enter Your First Name..."><br>


            </div>
            <div class="input-group width90percent">
              <input type="text" id="email_subscribe_l_name" name="l_name" class="form-control subscribeEmail" placeholder="Enter Your Last Name..."><br>


            </div>
            <div class="input-group width90percent Sendwidth90percent">
              <input type="email" id="email_subscribe_footer" name="email" class="form-control subscribeEmail" placeholder="Enter Your Email Address..">
              <span class="input-group-btn">
                <button class="MySearchBtn btn btn-default" id="subscribeNewsletter" type="button">
                  <i class="fa fa-paper-plane" aria-hidden="true"></i>
                </button>
              </span>
            </div>
          </div>
        </div>




      </div>
    </div>
  </section>
  <section class="copyrights">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 CopyR">
          <p>
            CopyRights © 2021 - 2022  British Business Group . All Right Reserved. <br>
            <a href="https://wistech.biz/" target="_blank">Designed &amp; Developed By</a> : <a href="http://800Wisdom.ae/" target="_blank">800Wisdom.ae</a>                    </p>
          </div>
        </div>
      </div>
    </section>
  </footer>
  <div id="fb-root"></div>
  <div class="mmcls modal fade" id="general-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body modalContent">
        </div>
      </div>
    </div>
  </div>
  <div class="mmcls modal fade" id="secondary-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body modalContent">
        </div>
      </div>
    </div>
  </div>

  @push('css')
  <style>
    .Sendwidth90percent{
      width: 90%!important;
    }
    .MySearchBtn{
      margin-left: 0px !important;
    }
    @media screen and (max-width: 767px) {
      .Sendwidth90percent{
        width: 93%!important;
      }
    }
    @media screen and (max-width: 420px) {
      .Sendwidth90percent{
        width: 90%!important;
      }
    }
    @media screen and (max-width: 360px) {
      .Sendwidth90percent{
        width: 89%!important;
      }
    }
    @media screen and (max-width: 320px) {
      .Sendwidth90percent{
        width: 87%!important;
      }
    }
  </style>
  @endpush()


  @push('js')
  <script>
    $('body').find('#subscribeNewsletter').click(function (e) {

      $(this).prop('disabled', true);
      var email_data = $(document).find('#email_subscribe_footer').val();
      var Fname = $(document).find('#email_subscribe_f_name').val()
      var Lname = $(document).find('#email_subscribe_l_name').val()

      if ( email_data == '') {

        swal({
          title: 'Error!',
          text: 'Please enter email address!',
          type: 'error',
          timer: 5000,
          confirmButtonText: 'OK'
        });
        $('#subscribeNewsletter').prop('disabled', false);
        return false;
      }

      if(!isEmail(email_data)){
        swal({
          title: 'Error!',
          text: 'Please enter correct email address!',
          type: 'error',
          timer: 5000,
          confirmButtonText: 'OK'
        });
        $('#subscribeNewsletter').prop('disabled', false);
        return false;
      }

      $.ajax({

        url: 'Mem/SubscribeAjax',
        type: 'POST',
        data : {email : email_data, first_name : Fname, last_name : Lname},

        success: function (data) {

        // var data = JSON.parse(data);
        console.log(data);

        if(data.type == 'error'){
          swal({
            title: 'Error!',
            text: data.msg,
            type: 'error',
            timer: 5000,
            confirmButtonText: 'OK'
          });
        }

        if (data.type=="success") {
         $(document).find('#email_subscribe_footer').val('')
         $(document).find('#email_subscribe_f_name').val('')
         $(document).find('#email_subscribe_l_name').val('')


         swal({
          title: 'Success!',
          text: data.msg,
          type: 'success',
          timer: 5000,
          confirmButtonText: 'OK'
        });

       }
       $('#subscribeNewsletter').prop('disabled', false)
     },
     error: function (e) {

      swal({
        title: 'Error!',
        text: 'Registration could not be completed!',
        type: 'error',
        timer: 5000,
        confirmButtonText: 'OK'
      });

      $('#subscribeNewsletter').prop('disabled', false);
    }
  });

      $('#subscribeNewsletter').prop('disabled', false);
      e.stopImmediatePropagation();
      return false;
    });


    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
  </script>
  @endpush

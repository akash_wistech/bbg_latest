<?php  
use Wisdom\Event\Models\EventSubscription;

$upcoming_events = DB::table('events')
->where('event_startDate', '>=', date('Y-m-d'))
->where('active', '=', '1')
->orderBy('event_startDate', 'asc')
->limit(5)
->get();

// dd($upcoming_events);

$past_events = DB::table('events')
->where('event_startDate', '<', date('Y-m-d'))
->where('active', '=', '1')
->orderBy('event_startDate', 'desc')
->limit(5)
->get();

$data = [];

if ($past_events <> null) {

	foreach ($past_events as $event) {
// dd($event);

		$color = "#1f3760";
		$getSubscription = null;
		if (Auth::check()) {
			$getSubscription = EventSubscription::where(['user_id' => Auth::user()->id, 'event_id' => $event->id])->first();
			if($getSubscription <> null){
				$color = "#cd2c47";
			}
		}

		$data[] = [
			'id' => $event->id,
			'name' => $event->title,
			'startdate' => $event->event_startDate,
			'enddate' => "",
			'starttime' => "",
			'endtime' => "",
			'color' => $color,
			'url' => "/events/event-details/".$event->id,
		];

// dd($data);
	}
}



if ($upcoming_events <> null) {

	foreach ($upcoming_events as $event) {
		$color = "#1f3760";
		$getSubscription = null;
		if (Auth::check()) {
			$getSubscription = EventSubscription::where(['user_id' => Auth::user()->id, 'event_id' => $event->id])->first();
			if($getSubscription <> null){
				$color = "#cd2c47";
			}
		}

		$data[] = [

			'id' => $event->id,
			'name' => $event->title,
			'startdate' => $event->event_startDate,
			'enddate' => "",
			'starttime' => "",
			'endtime' => "",
			'color' => $color,
			'url' => "/events/event-details/".$event->id,
		];

// dd($data);
	}
}



$upcomingEvents = $upcoming_events;
$pastEvents = $past_events;
$sampleEvents = \GuzzleHttp\json_encode(['monthly' => $data]);

// dd($sampleEvents);

?>


<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
	<div class="SideHeading">
		<h3><i class="fa fa-calendar" aria-hidden="true"></i> Events Calendar</h3>
	</div>
	<div class="SideCalender">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="EventCalender">
					<!--<div id="tempusts"></div>-->
					<!--<div id="output"><div>-->
						<div class="monthly" id="mycalendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
		<div class="SideHeading">
			<h3><i class="fa fa-bullhorn" aria-hidden="true"></i> Upcoming events</h3>
		</div>
		<div class="SideCalender">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<?php $events_data = [];
					if (count($upcomingEvents) > 0) {
						foreach ($upcomingEvents as $event) {
							$start_date = $event->event_startDate;
							$events_data[$start_date] = "<a href='/events/event-details/".$event->id."' style='color:#fff;' >" . $event->title . "</a>";


							$data[] = [
								'id' => $event->id,
								'name' => $event->title,
								'startdate' => $event->event_startDate,
								'enddate' => "",
								'starttime' => $event->event_startTime,
								'endtime' => $event->event_endTime,
								'url' => "/events/event-details/" . $event->id,
							];

							?>
							<a href="/events/event-details/<?= $event->id; ?>">
								<div class="UpcomingEvent1">
									<h2><?= $event->title; ?></h2>
									<h3>
										<b><?= formatDate($event->event_startDate); ?></b>
									</h3>
									<p><?= substr($event->short_description, 0, 90) . '...'; ?></p>
								</div>
							</a>
							<?php
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>


	@push('js')
	<script>
		$(document).ready(function(){

			var sampleEvents = [];

			$('#mycalendar').monthly({
				mode: 'event',
				dataType: 'json',
				events: {!!$sampleEvents!!},
				stylePast : true,
				weekStart : 'Sun',

            // jsonUrl: 'events.json',
            // target : '#mycalendar'
        });

			if($('#mycalendar .monthly-header').length > 1){
				$('#mycalendar .monthly-header:nth-child(2)').remove();
				$('#mycalendar .monthly-day-title-wrap:nth-child(2)').remove();
				$('#mycalendar .monthly-day-wrap:nth-child(2)').remove();
				$('#mycalendar .monthly-event-list:nth-child(2)').remove();
			}

			$('body').find('.monthly-prev').trigger('click');
			$('body').find('.monthly-reset').trigger('click');
		}); 
	</script>
	@endpush
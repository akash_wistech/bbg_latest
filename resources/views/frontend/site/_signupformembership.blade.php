@extends('frontend.app')
@section('content')

<form id="login-form" class="f-login-form" action="/site/signup/?membership=6" method="post" enctype="multipart/form-data">
	<input type="hidden" name="_csrf-frontend" value="lLtycyL3xslkb7uO5bjt6AhBof_caNaoI35FWyzqd-vh7xQZbpuIjAsb-ceB1JihfyjPlYkQmsN8By8THrgSvA=="><!--? /*= TopBannerWidget::widget(); */ ?-->
	<section class="MainArea">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
						<div class="col-md-12">
						</div>
						<div class="col-md-12 ">
							<div class="Heading text-left">
								<h3>Personal Information</h3><br>
							</div>
							<div class="row">

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-title required">
										<label class="control-label" for="signupform-title">Title</label>
										<select id="signupform-title" class="form-control" name="SignupForm[title]" aria-required="true">
											<option value="">Select ...</option>
											<option value="Mr">Mr</option>
											<option value="Ms">Ms</option>
											<option value="Miss">Miss</option>
											<option value="Dr">Dr</option>
											<option value="Mrs">Mrs</option>
											<option value="HMCG">HMCG</option>
											<option value="H.E">H.E</option>
											<option value="Sir">Sir</option>
										</select>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-first_name required">
										<label class="control-label" for="signupform-first_name">First Name</label>
										<input type="text" id="signupform-first_name" class="form-control" name="SignupForm[first_name]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-last_name required">
										<label class="control-label" for="signupform-last_name">Last Name</label>
										<input type="text" id="signupform-last_name" class="form-control" name="SignupForm[last_name]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-phone_number required">
										<label class="control-label" for="signupform-phone_number">Mobile</label>
										<input type="text" id="signupform-phone_number" class="form-control" name="SignupForm[phone_number]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-email required">
										<label class="control-label" for="signupform-email">Email</label>
										<input type="text" id="signupform-email" class="form-control" name="SignupForm[email]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-country_code">
										<label class="control-label" for="signupform-country_code">Your Office Based Country</label>
										<select id="signupform-country_code" class="form-control" name="SignupForm[country_code]">
											<option value="">Select ...</option>
											<option value="228">United Arab Emirates</option>
											<option value="229">United Kingdom</option>
											<option value="247">Other</option>
										</select>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-city">
										<label class="control-label" for="signupform-city">Your Office Based City</label>
										<select id="signupform-city" class="form-control" name="SignupForm[city]">
											<option value="">Select ...</option>
											<option value="Abu Dhabi">Abu Dhabi</option>
											<option value="Dubai">Dubai</option>
											<option value="Ajman">Ajman</option>
											<option value="Sharjah">Sharjah</option>
											<option value="Fujairah">Fujairah</option>
											<option value="Ras Al Khaimah">Ras Al Khaimah</option>
											<option value="Umm Al Quwain">Umm Al Quwain</option>
											<option value="Other">Other</option>
										</select>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="row">
										<div id="other_country" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden">
											<div class="form-group field-signupform-other_country">
												<label class="control-label" for="signupform-other_country">Please specify Country Name</label>
												<input type="text" id="signupform-other_country" class="form-control" name="SignupForm[other_country]">

												<p class="help-block help-block-error"></p>
											</div>                                    
										</div>
										<div id="other_city" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 hidden">
											<div class="form-group field-signupform-other_city">
												<label class="control-label" for="signupform-other_city">Please specify City Name</label>
												<input type="text" id="signupform-other_city" class="form-control" name="SignupForm[other_city]">

												<p class="help-block help-block-error"></p>
											</div>                                    
										</div>
									</div>

								</div>

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="form-group field-signupform-address">
										<label class="control-label" for="signupform-address">Physical Mailing Address</label>
										<input type="text" id="signupform-address" class="form-control" name="SignupForm[address]">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-secondry_email">
										<label class="control-label" for="signupform-secondry_email">Secondary Email</label>
										<input type="text" id="signupform-secondry_email" class="form-control" name="SignupForm[secondry_email]">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-designation required">
										<label class="control-label" for="signupform-designation">Corporate Title</label>
										<input type="text" id="signupform-designation" class="form-control" name="SignupForm[designation]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

									<div class="form-group field-signupform-nationality">
										<label class="control-label" for="signupform-nationality">Nationality</label>
										<select id="signupform-nationality" class="form-control" name="SignupForm[nationality]">
											<option value="">Select ...</option>
											<option value="Afghanistan">Afghanistan</option>
											<option value="Albania">Albania</option>
											<option value="Algeria">Algeria</option>
											<option value="American Samoa">American Samoa</option>
											<option value="Andorra">Andorra</option>
											<option value="Angola">Angola</option>
											<option value="Anguilla">Anguilla</option>
											<option value="Antarctica">Antarctica</option>
											<option value="Antigua and Barbuda">Antigua and Barbuda</option>
											<option value="Argentina">Argentina</option>
											<option value="Armenia">Armenia</option>
											<option value="Aruba">Aruba</option>
											<option value="Australia">Australia</option>
											<option value="Austria">Austria</option>
											<option value="Azerbaijan">Azerbaijan</option>
											<option value="Bahamas">Bahamas</option>
											<option value="Bahrain">Bahrain</option>
											<option value="Bangladesh">Bangladesh</option>
											<option value="Barbados">Barbados</option>
											<option value="Belarus">Belarus</option>
											<option value="Belgium">Belgium</option>
											<option value="Belize">Belize</option>
											<option value="Benin">Benin</option>
											<option value="Bermuda">Bermuda</option>
											<option value="Bhutan">Bhutan</option>
											<option value="Bolivia">Bolivia</option>
											<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
											<option value="Botswana">Botswana</option>
											<option value="Bouvet Island">Bouvet Island</option>
											<option value="Brazil">Brazil</option>
											<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
											<option value="Brunei Darussalam">Brunei Darussalam</option>
											<option value="Bulgaria">Bulgaria</option>
											<option value="Burkina Faso">Burkina Faso</option>
											<option value="Burundi">Burundi</option>
											<option value="Cambodia">Cambodia</option>
											<option value="Cameroon">Cameroon</option>
											<option value="Canada">Canada</option>
											<option value="Cape Verde">Cape Verde</option>
											<option value="Cayman Islands">Cayman Islands</option>
											<option value="Central African Republic">Central African Republic</option>
											<option value="Chad">Chad</option>
											<option value="Chile">Chile</option>
											<option value="China">China</option>
											<option value="Christmas Island">Christmas Island</option>
											<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
											<option value="Colombia">Colombia</option>
											<option value="Comoros">Comoros</option>
											<option value="Congo">Congo</option>
											<option value="Cook Islands">Cook Islands</option>
											<option value="Costa Rica">Costa Rica</option>
											<option value="Croatia (Hrvatska)">Croatia (Hrvatska)</option>
											<option value="Cuba">Cuba</option>
											<option value="Cyprus">Cyprus</option>
											<option value="Czech Republic">Czech Republic</option>
											<option value="Denmark">Denmark</option>
											<option value="Djibouti">Djibouti</option>
											<option value="Dominica">Dominica</option>
											<option value="Dominican Republic">Dominican Republic</option>
											<option value="East Timor">East Timor</option>
											<option value="Ecuador">Ecuador</option>
											<option value="Egypt">Egypt</option>
											<option value="El Salvador">El Salvador</option>
											<option value="Equatorial Guinea">Equatorial Guinea</option>
											<option value="Eritrea">Eritrea</option>
											<option value="Estonia">Estonia</option>
											<option value="Ethiopia">Ethiopia</option>
											<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
											<option value="Faroe Islands">Faroe Islands</option>
											<option value="Fiji">Fiji</option>
											<option value="Finland">Finland</option>
											<option value="France">France</option>
											<option value="France, Metropolitan">France, Metropolitan</option>
											<option value="French Guiana">French Guiana</option>
											<option value="French Polynesia">French Polynesia</option>
											<option value="French Southern Territories">French Southern Territories</option>
											<option value="Gabon">Gabon</option>
											<option value="Gambia">Gambia</option>
											<option value="Georgia">Georgia</option>
											<option value="Germany">Germany</option>
											<option value="Ghana">Ghana</option>
											<option value="Gibraltar">Gibraltar</option>
											<option value="Guernsey">Guernsey</option>
											<option value="Greece">Greece</option>
											<option value="Greenland">Greenland</option>
											<option value="Grenada">Grenada</option>
											<option value="Guadeloupe">Guadeloupe</option>
											<option value="Guam">Guam</option>
											<option value="Guatemala">Guatemala</option>
											<option value="Guinea">Guinea</option>
											<option value="Guinea-Bissau">Guinea-Bissau</option>
											<option value="Guyana">Guyana</option>
											<option value="Haiti">Haiti</option>
											<option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
											<option value="Honduras">Honduras</option>
											<option value="Hong Kong">Hong Kong</option>
											<option value="Hungary">Hungary</option>
											<option value="Iceland">Iceland</option>
											<option value="India">India</option>
											<option value="Isle of Man">Isle of Man</option>
											<option value="Indonesia">Indonesia</option>
											<option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
											<option value="Iraq">Iraq</option>
											<option value="Ireland">Ireland</option>
											<option value="Israel">Israel</option>
											<option value="Italy">Italy</option>
											<option value="Ivory Coast">Ivory Coast</option>
											<option value="Jersey">Jersey</option>
											<option value="Jamaica">Jamaica</option>
											<option value="Japan">Japan</option>
											<option value="Jordan">Jordan</option>
											<option value="Kazakhstan">Kazakhstan</option>
											<option value="Kenya">Kenya</option>
											<option value="Kiribati">Kiribati</option>
											<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
											<option value="Korea, Republic of">Korea, Republic of</option>
											<option value="Kosovo">Kosovo</option>
											<option value="Kuwait">Kuwait</option>
											<option value="Kyrgyzstan">Kyrgyzstan</option>
											<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
											<option value="Latvia">Latvia</option>
											<option value="Lebanon">Lebanon</option>
											<option value="Lesotho">Lesotho</option>
											<option value="Liberia">Liberia</option>
											<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
											<option value="Liechtenstein">Liechtenstein</option>
											<option value="Lithuania">Lithuania</option>
											<option value="Luxembourg">Luxembourg</option>
											<option value="Macau">Macau</option>
											<option value="Macedonia">Macedonia</option>
											<option value="Madagascar">Madagascar</option>
											<option value="Malawi">Malawi</option>
											<option value="Malaysia">Malaysia</option>
											<option value="Maldives">Maldives</option>
											<option value="Mali">Mali</option>
											<option value="Malta">Malta</option>
											<option value="Marshall Islands">Marshall Islands</option>
											<option value="Martinique">Martinique</option>
											<option value="Mauritania">Mauritania</option>
											<option value="Mauritius">Mauritius</option>
											<option value="Mayotte">Mayotte</option>
											<option value="Mexico">Mexico</option>
											<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
											<option value="Moldova, Republic of">Moldova, Republic of</option>
											<option value="Monaco">Monaco</option>
											<option value="Mongolia">Mongolia</option>
											<option value="Montenegro">Montenegro</option>
											<option value="Montserrat">Montserrat</option>
											<option value="Morocco">Morocco</option>
											<option value="Mozambique">Mozambique</option>
											<option value="Myanmar">Myanmar</option>
											<option value="Namibia">Namibia</option>
											<option value="Nauru">Nauru</option>
											<option value="Nepal">Nepal</option>
											<option value="Netherlands">Netherlands</option>
											<option value="Netherlands Antilles">Netherlands Antilles</option>
											<option value="New Caledonia">New Caledonia</option>
											<option value="New Zealand">New Zealand</option>
											<option value="Nicaragua">Nicaragua</option>
											<option value="Niger">Niger</option>
											<option value="Nigeria">Nigeria</option>
											<option value="Niue">Niue</option>
											<option value="Norfolk Island">Norfolk Island</option>
											<option value="Northern Mariana Islands">Northern Mariana Islands</option>
											<option value="Norway">Norway</option>
											<option value="Oman">Oman</option>
											<option value="Pakistan">Pakistan</option>
											<option value="Palau">Palau</option>
											<option value="Palestine">Palestine</option>
											<option value="Panama">Panama</option>
											<option value="Papua New Guinea">Papua New Guinea</option>
											<option value="Paraguay">Paraguay</option>
											<option value="Peru">Peru</option>
											<option value="Philippines">Philippines</option>
											<option value="Pitcairn">Pitcairn</option>
											<option value="Poland">Poland</option>
											<option value="Portugal">Portugal</option>
											<option value="Puerto Rico">Puerto Rico</option>
											<option value="Qatar">Qatar</option>
											<option value="Reunion">Reunion</option>
											<option value="Romania">Romania</option>
											<option value="Russian Federation">Russian Federation</option>
											<option value="Rwanda">Rwanda</option>
											<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
											<option value="Saint Lucia">Saint Lucia</option>
											<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
											<option value="Samoa">Samoa</option>
											<option value="San Marino">San Marino</option>
											<option value="Sao Tome and Principe">Sao Tome and Principe</option>
											<option value="Saudi Arabia">Saudi Arabia</option>
											<option value="Senegal">Senegal</option>
											<option value="Serbia">Serbia</option>
											<option value="Seychelles">Seychelles</option>
											<option value="Sierra Leone">Sierra Leone</option>
											<option value="Singapore">Singapore</option>
											<option value="Slovakia">Slovakia</option>
											<option value="Slovenia">Slovenia</option>
											<option value="Solomon Islands">Solomon Islands</option>
											<option value="Somalia">Somalia</option>
											<option value="South Africa">South Africa</option>
											<option value="South Georgia South Sandwich Islands">South Georgia South Sandwich Islands</option>
											<option value="Spain">Spain</option>
											<option value="Sri Lanka">Sri Lanka</option>
											<option value="St. Helena">St. Helena</option>
											<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
											<option value="Sudan">Sudan</option>
											<option value="Suriname">Suriname</option>
											<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
											<option value="Swaziland">Swaziland</option>
											<option value="Sweden">Sweden</option>
											<option value="Switzerland">Switzerland</option>
											<option value="Syrian Arab Republic">Syrian Arab Republic</option>
											<option value="Taiwan">Taiwan</option>
											<option value="Tajikistan">Tajikistan</option>
											<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
											<option value="Thailand">Thailand</option>
											<option value="Togo">Togo</option>
											<option value="Tokelau">Tokelau</option>
											<option value="Tonga">Tonga</option>
											<option value="Trinidad and Tobago">Trinidad and Tobago</option>
											<option value="Tunisia">Tunisia</option>
											<option value="Turkey">Turkey</option>
											<option value="Turkmenistan">Turkmenistan</option>
											<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
											<option value="Tuvalu">Tuvalu</option>
											<option value="Uganda">Uganda</option>
											<option value="Ukraine">Ukraine</option>
											<option value="United Arab Emirates">United Arab Emirates</option>
											<option value="United Kingdom">United Kingdom</option>
											<option value="United States">United States</option>
											<option value="United States minor outlying islands">United States minor outlying islands</option>
											<option value="Uruguay">Uruguay</option>
											<option value="Uzbekistan">Uzbekistan</option>
											<option value="Vanuatu">Vanuatu</option>
											<option value="Vatican City State">Vatican City State</option>
											<option value="Venezuela">Venezuela</option>
											<option value="Vietnam">Vietnam</option>
											<option value="Virgin Islands (British)">Virgin Islands (British)</option>
											<option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
											<option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
											<option value="Western Sahara">Western Sahara</option>
											<option value="Yemen">Yemen</option>
											<option value="Yugoslavia">Yugoslavia</option>
											<option value="Zaire">Zaire</option>
											<option value="Zambia">Zambia</option>
											<option value="Zimbabwe">Zimbabwe</option>
											<option value="Other">Other</option>
										</select>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>
								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-company_type">
										<label class="control-label" for="signupform-company_type">Category</label>
										<select id="signupform-company_type" class="form-control" name="SignupForm[company_type]">
											<option value="1">Animal Welfare</option>
											<option value="2">Automotive</option>
											<option value="3">Aviation</option>
											<option value="4">Banking/Finance</option>
											<option value="5">Business Services</option>
											<option value="6">Charity</option>
											<option value="7">Marketing/Advertising/ Communications</option>
											<option value="8">Construction/ Engineering/Procurement</option>
											<option value="9">Education/Training</option>
											<option value="10">Energy/Oil &amp; Gas</option>
											<option value="11">Events/Entertainment</option>
											<option value="12">Interior Design/ Arts</option>
											<option value="13">Hospitality/ Food and Beverage/ Tourism</option>
											<option value="14">Waste Services/ Green Initiatives/ Recycling/ Environment </option>
											<option value="15">HR/Recruitment</option>
											<option value="16">Healthcare/Well-being</option>
											<option value="19">Insurance</option>
											<option value="20">IT/ Digital/ Technology</option>
											<option value="21">Legal</option>
											<option value="22">Leisure/Sports</option>
											<option value="23">Logistics</option>
											<option value="24">Manufacturing</option>
											<option value="25">Office Equipment/Services</option>
											<option value="26">Project Management/ Facilities Management </option>
											<option value="27">Real Estate</option>
											<option value="28">Retail/Trade</option>
											<option value="29">Security/Safety</option>
											<option value="40">Conference and Exhibitions</option>
											<option value="42">Goverment</option>
											<option value="43">Consulting </option>
											<option value="44">usama cat</option>
										</select>

										<p class="help-block help-block-error"></p>
									</div>                                
								</div>
								<div class="col-6 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-howdidyouhear">
										<label class="control-label" for="signupform-howdidyouhear">How did you hear about us?</label>
										<select id="signupform-howdidyouhear" class="form-control" name="SignupForm[howDidYouHear]" maxlength="">
											<option value="">Select ...</option>
											<option value="2">BBG Event</option>
											<option value="3">Administrative Board Member/Focus Chair/Business Team</option>
											<option value="4">Past or Present BBG Member Referral</option>
											<option value="5">Scottish Dev Institute/Invest Northern Ireland/Welsh Gov</option>
											<option value="6">British Centres for Business (BCB)</option>
											<option value="7">Social Media</option>
											<option value="8">Online Search</option>
											<option value="9">BBG MVP</option>
										</select>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>
								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-referred_by">
										<label class="control-label" for="signupform-referred_by">Referred By</label>
										<input type="text" id="signupform-referred_by" class="form-control" name="SignupForm[referred_by]">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="form-group field-signupform-tellusaboutyourself">
										<label class="control-label" for="signupform-tellusaboutyourself">Tell us about yourself in a few words</label>
										<textarea id="signupform-tellusaboutyourself" class="form-control" name="SignupForm[tellUsAboutYourself]" row="10" placeholder="E.g. educated in the UAE and studied in the Netherlands I have lived, worked and studied in a cosmopolitan environment. Through this experience I was able to achieve a wide-ranging background. Through my travels I was able to gain knowledge in the following areas public speaking, event management, creative writing, marketing and production."></textarea>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="form-group field-signupform-purposeofjoining">
										<label class="control-label" for="signupform-purposeofjoining">Reasons for joining the BBG</label>
										<textarea id="signupform-purposeofjoining" class="form-control" name="SignupForm[purposeOfJoining]" row="5" placeholder="E.g. having the ability to request for personal introductions to other BBG Members, without the usual awkwardness when approaching new contacts"></textarea>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

							</div>
							<div class="Heading text-left">
								<h3>Upload Required Documents</h3><br>
							</div>
							<div class="row">

								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
									<label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of
									passport</label>
									<div class="form-group">
										<a href="javascript:;" id="upload-document2" onclick="uploadAttachment(2)" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">
											<i></i>
											<img src="https://d2a5i90cjeax12.cloudfront.net/default-placeholder5a66caaf3a8a3.png" width="100" alt="" title="" data-placeholder="no_image.png">
											<br>
										</a>
										<p class="image_upload_label">click Image to upload</p>
										<div class="form-group field-input-attachment2">

											<input type="hidden" id="input-attachment2" class="form-control" name="SignupForm[passport_copy]" maxlength="">

											<p class="help-block help-block-error"></p>
										</div>                                
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
									<label class="docs_upload_label control-label" for="signupform-trade_licence">Copy of
									residence visa</label>
									<div class="form-group">
										<a href="javascript:;" id="upload-document3" onclick="uploadAttachment(3)" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">
											<i></i>
											<img src="https://d2a5i90cjeax12.cloudfront.net/default-placeholder5a66caaf3a8a3.png" width="100" alt="" title="" data-placeholder="no_image.png">
											<br>
										</a>
										<p class="image_upload_label">click Image to upload</p>
										<div class="form-group field-input-attachment3">

											<input type="hidden" id="input-attachment3" class="form-control" name="SignupForm[residence_visa]" maxlength="">

											<p class="help-block help-block-error"></p>
										</div>                                
									</div>
								</div>
								<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
									<label class="docs_upload_label control-label" for="signupform-trade_licence">Profile
									Picture (upload as JPEG)</label>
									<div class="form-group">
										<a href="javascript:;" id="upload-document4" onclick="uploadAttachment(4)" data-toggle="tooltip" class="img-thumbnail" title="Upload Document">
											<i></i>
											<img src="https://d2a5i90cjeax12.cloudfront.net/default-placeholder5a66caaf3a8a3.png" width="100" alt="" title="" data-placeholder="no_image.png">
											<br>
										</a>
										<p class="image_upload_label">click Image to upload</p>
										<div class="form-group field-input-attachment4">

											<input type="hidden" id="input-attachment4" class="form-control" name="SignupForm[passport_size_pic]" maxlength="">

											<p class="help-block help-block-error"></p>
										</div>                                
									</div>
								</div>

							</div>

							<div class="Heading text-left">
								<h3>BBG Profile Info</h3><br>
							</div>
							<div class="row">
								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-user_name required">
										<label class="control-label" for="signupform-user_name">User Name</label>
										<input type="text" id="signupform-user_name" class="form-control" name="SignupForm[user_name]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>
								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-password required">
										<label class="control-label" for="signupform-password">Password</label>
										<input type="password" id="signupform-password" class="form-control" name="SignupForm[password]" aria-required="true">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="form-group field-signupform-repassword">
										<label class="control-label" for="signupform-repassword">Retype password</label>
										<input type="password" id="signupform-repassword" class="form-control" name="SignupForm[repassword]">

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="form-group field-signupform-verifycode required">
										<label class="control-label" for="signupform-verifycode">Verify Code</label>
										<div class="row"><div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><input type="text" id="signupform-verifycode" class="form-control" name="SignupForm[verifyCode]"></div><div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6"><img id="signupform-verifycode-image" src="/site/captcha?v=61ab425d8aff5" alt=""></div></div>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>


								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="form-group field-signupform-newsletter">

										<input type="hidden" name="SignupForm[newsletter]" value="0"><label><input type="checkbox" id="signupform-newsletter" class="cbx-loading" name="SignupForm[newsletter]" value="1" data-krajee-checkboxx="checkboxX_639902a2"> I want to subscribe to the BBG Newsletter.</label>

										<p class="help-block help-block-error"></p>
									</div>                            
								</div>

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="form-group field-signupform-terms required">

										<input type="hidden" name="SignupForm[terms]" value="0"><label><input type="checkbox" id="signupform-terms" class="cbx-loading" name="SignupForm[terms]" value="1" data-krajee-checkboxx="checkboxX_639902a2"> I agree to the BBG Terms and Conditions .</label>

										<p class="help-block help-block-error"></p>
									</div>                                
									<span>
										<a target="_blank" href="/pages/view-by-seo-url?seo_url=terms-and-conditions" style="font-size: 10px;font-weight: 800;font-style: italic;">Read Terms &amp; Condition here</a>
									</span>
								</div>

							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
							<button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o" name="LOGIN TO YOUR ACCOUNT">Sign Up</button>                    </div>
						</div>
					</div>
				</div>

			</div>
		</section>
	</form>

	@endsection


@push('css')
	form div.required label.control-label:after {
		content: " * ";
		color: #a94442;
	}

	textarea#signupform-tellusaboutyourself {
		min-height: 100px;
	}

	textarea#signupform-purposeofjoining {
		min-height: 100px;
	}

	textarea#signupform-purposeofjoining::-webkit-input-placeholder,
	textarea#signupform-tellusaboutyourself::-webkit-input-placeholder {
		font-size: 14px;
	}
@endpush
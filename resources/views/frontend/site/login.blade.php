@extends('frontend.app')
@section('content')


<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">

				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 loginFormstyle paddingRightLeft ">
					<!-- login toast after login -->
					<div class="row ">
						<div class="col-12 col-sm-12">
							<div class="EventText alert  alert-info" style="padding-top: 20px">
								<p>This area of our web site is for individual or company members of the BBG. If you would like to know more about becoming a member, please email us at <a class="member_link" href="mailto:info@bbgdxb.com" target="_top">info@bbgdxb.com</a> or go to <a class="member_link" href="{{url('applyformembership')}}">Membership</a></p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-3"></div>

						<div class="col-md-6 col-md-offset-12 loginFormstyle">

							<div class="Heading text-center">
								<h3>Member Login</h3>
							</div>
							{{ Form::open(array('url' => '/fend/login')) }}
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="SiteText">
										<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
											<input class="form-control" placeholder="Please Enter Email" autofocus="" name="email" type="text">
											<input type="password" id="accountloginform-password" class="form-control" name="password" placeholder="Enter your password" aria-required="true">

											<button type="submit" class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Sign In</button>
											<button class="Mybtn" style="padding:10px;">Forget Password</button>
										</div>
									</div>
								</div>
							</div>
							{{ Form::close() }}
						</div>
						
						<div class="col-3"></div>
					</div>
					
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>

@endsection

@push('css')
<style>
	a.member_link:hover{
		color: #1F3760 !important;
		font-weight: bold;
	}
	
</style>
@endpush
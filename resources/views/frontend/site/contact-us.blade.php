@extends('frontend.app')
@section('content')
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft ">
                    <div class="Heading">
                        <h3>Contact Us</h3>
                    </div>
                    <div class="contactus PaddingTopBtm">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <h3>British Business Group Dubai & Northern Emirates</h3>
                                <p>
                                    <?= getSetting('company_address') ? nl2br(getSetting('company_address')) : ''; ?><br>
                                    <b>Tel:</b> <a
                                    href="tel:<?= getSetting('telephone') ? getSetting('telephone') : ''; ?>">
                                    <?= getSetting('telephone') ? getSetting('telephone') : ''; ?> <br>
                                </a>

                                <b>Email:</b> <a
                                href="tel:<?= getSetting('admin_email') ? getSetting('admin_email') : ''; ?>">
                                <?= getSetting('admin_email') ? getSetting('admin_email') : ''; ?>
                            </a>
                        </p>
                        <p>
                            <strong>Social Media:</strong> <br>
                            <?= getSetting('facebook') ? '<a class="fontsize24" target="_blank" href="' . getSetting('facebook') . '">' . getSetting('facebook_icon') . '</a>' : ''; ?>
                            <?= getSetting('twitter') ? '<a class="fontsize24" target="_blank" href="' . getSetting('twitter') . '">' . getSetting('twitter_icon') . '</a>' : ''; ?>
                            <?= getSetting('linkedin') ? '<a class="fontsize24" target="_blank" href="' . getSetting('linkedin') . '">' . getSetting('linkedin_icon') . '</a>' : ''; ?>
                            <?= getSetting('instagram') ? '<a class="fontsize24" target="_blank" href="' . getSetting('instagram') . '">' . getSetting('instagram_icon') . '</a>' : ''; ?>
                            <?= getSetting('pinterest') ? '<a class="fontsize24" target="_blank" href="' . getSetting('pinterest') . '">' . getSetting('pinterest_icon') . '</a>' : ''; ?>
                        </p>
                                <!--<p>
                                    <strong>Timings:</strong> <br>
                                    <?/*= (isset($siteSetting['about'])) ? $siteSetting['about'] : ''; */?>
                                </p>-->
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <iframe width="100%" height="300" frameborder="1" scrolling="no" marginheight="20" marginwidth="20"
                                src="https://maps.google.com/maps?q=<?= getSetting('location') ? nl2br(getSetting('location')) : ''; ?>&sensor=false&hl=en;z=14&amp;output=embed"></iframe>
                            </div>
                        </div>
                        <div class="Heading">
                            <h3>Feedback Form</h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                          @foreach ($errors->all() as $error)
                          <div>{{ $error }}</div>
                          @endforeach
                      </div>
                      @endif
                      <div class="row">
                        {!! Form::open(array('url' => 'contact-us', 'enableClientScript' => false)) !!}
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 feedbackform">
                            <h2>Please fill in the following form specifying your requirement. We will get back to
                            you shortly.</h2>

                            {{ Form::text('name', '', ['class'=>'form-control','placeholder'=>'Please enter name']) }}

                            {{ Form::text('email', '', ['class'=>'form-control','placeholder'=>'Please enter email']) }}

                            {{ Form::text('subject', '', ['class'=>'form-control','placeholder'=>'Please enter subject']) }}

                            {{ Form::textarea('body', '', ['class'=>'form-control', 'rows' => 6, 'placeholder' => 'Please enter message', 'autofocus' => true]) }}
                            <br>
                            {!! htmlFormSnippet() !!} 
                            
                            {!! Form::submit('Send Email', ['class' => 'Mybtn pull-right my-4']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

@push('css')
<style>
    <style>
    .fontsize24{
        font-size: 24px !important;
    }
</style>
</style>
@endpush
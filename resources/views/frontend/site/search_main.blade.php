@extends('frontend.app')
@section('content')
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="f-login-content loginFormstyle paddingRightLeft">
					<div class="Heading text-center">
						<h3>Search Results</h3>
					</div>
					<div class="alert alert-info">
						Search Results for <strong>"<?= $search; ?>"</strong>
						<!--<strong>( Results)</strong>-->
					</div>
				</div>
				<h2 class="my-4">Events
					<small><i class="fa fa-angle-double-right"></i> (<?= count($search_events);?>)</small>
				</h2>
				<div id="events" class="list-view searchResult">
					<?php 
					if (count($search_events)>0) {

						foreach ($search_events as $search_event) { 
							// dd("here");
							?>
							<div class="view">
								<a href="{{url('events/event-details/'.$search_event->id)}}"><?= $search_event->title; ?></a>
								<p><?= strlen($search_event->short_description) > 250 ? substr($search_event->short_description,0,250)."..." : $search_event->short_description;  ?></p>

							</div>
							<?php 
						}

					} else{ 
						?>
						<div class="items">
							<div class="alert alert-danger">No results found.</div>
						</div>
						<?php 
					}
					?>

				</div>
				<div style="clear: both;"></div>
				<h2 class="my-4">News
					<small><i class="fa fa-angle-double-right"></i> (<?= count($search_news);?>)</small>
				</h2>
				<div id="news" class="list-view searchResult">
					<?php 
					if (count($search_news)>0) {
						foreach ($search_news as $news) { 
							// dd($news);
							?>
							<div class="view">
								<a href="{{url('news-details/'.$news->id)}}"><?= $news->title; ?></a>
								<p><?= strlen($news->short_description) > 250 ? substr($news->short_description,0,250)."..." : $news->short_description;  ?></p>

							</div>
							<?php 
						}
					}
					else{
						?>
						<div class="items">
							<div class="alert alert-danger">No results found.</div>
						</div>
						<?php
					}
					?>
				</div>

			</div>

			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>
@endsection

@push('css')
<style>
	.searchResult .view a {
		font-weight: bold;
	}
	#events,#news{
		background: #ffffff;
	}

	.searchResult .view {
		padding: 5px 5px 10px;
		margin-bottom: 10px;
		border-bottom: dashed 1px #ccc;
		-webkit-transition: background 0.2s;
		transition: background 0.2s;
	}
</style>
@endpush
<?php
$share_url = "";
$title = "";
$no_image = asset('assets/images/dummy-image-1.jpg');
?>


@extends('frontend.app')
@section('content')

<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<?php
				if (isset($single_message) && $single_message <> null) {
					// dd("not null");
					$share_url = "view-from-chair-detail?id=" . $single_message->id;
					$title = $single_message->title;
					?>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
						<div class="Heading">
							<h3><?= $single_message->title; ?></h3>
							<!--  <h4 style="text-transform: initial !important;"><?/*= $title; */?></h4>-->
							<p>
								<span class="BoldText">Date:</span> <?= formatDate($single_message->date); ?>
							</p>
						</div>
						<div class="EventDetail">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<img src="<?= ($single_message->image) ? $single_message->image : $no_image; ?>"
									class="img-fluid" alt="<?= $single_message->title; ?>">
								</div>

								<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
									<div class="DescriptionHeading"><br/> <br/> <br/>
										<h4><?= $single_message->short_description; ?></h4>
										<?= $single_message->description; ?>
									</div>
									<div id="share"></div>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
				?>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<?php
					if (isset($chariman_messages) && $chariman_messages <> null && count($chariman_messages) > 0) {
						foreach ($chariman_messages as $chariman_messages) {
							?>
							<div class="Event1">
								<div class="row">
									<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
										<img src="<?= ($chariman_messages->image) ? $chariman_messages->image : $no_image; ?>" class="img-fluid" alt="">
									</div>
									<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
										<div class="EventText">
											<h3><i class="fa fa-calendar"
												aria-hidden="true"></i> <?= formatDate($chariman_messages->date); ?>
											</h3>
											<h4><?= $chariman_messages->title; ?></h4>
											<p><?= $chariman_messages->short_description; ?></p>
											<span class="EventDetail">
												<a href="<?= url('view-from-chair-detail?id='.$chariman_messages->id) ?>" class="Mybtn pull-left" type="button">Read Detail</a>
											</span>
										</div>
									</div>
								</div>
							</div>
							<?php
						}
					} else {
						?>
						<div class="alert alert-warning">There isn't any news yet,you hear soon</div>
						<?php
					}
					?>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>

@endsection

@push('js')
<script>
	share_url = "{{$share_url}}";
	title = "{{$title}}";

	$("#share").jsSocials({
		url : share_url,
		text: title,
		shareIn: "popup",
		showLabel: false,
		showCount: false,
		shares: ["email", "twitter", "facebook", "googleplus", "linkedin"]
	});
</script>
@endpush
@extends('frontend.app') @section('content')
<?php $no_image = asset('assets/images/dummy-image-1.jpg'); ?>
<section class="MainArea">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
					<div class="Heading">
						<h3>Search Results</h3>
					</div>
					<hr/>
					<div class="committee text-center">
						<div class="row">
							<?php
							if ($filterData <> null && count($filterData)>0) {
								foreach ($filterData as $member) {
									// dd($member);
									$designationRow = getPredefinedItem($member->job_title_id);
									$designation='';
									if($designationRow!=null && $designationRow->lang!=null)$designation=$designationRow->title;
									

									$nontextareafields = getNonTextAreaColumnsSomething($member->moduleTypeId);
									// dd($nontextareafields);
									?>
									<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
										<img src="<?= ($member->image<>null) ? $member->image : $no_image; ?>"
										class="committee_staff_image img-fluid"
										alt="<?= $member->name; ?>">
										<div class="name_and_postion">
											<h5>
												<a href="{{url('member/detail/'.$member->module_id)}}"><?= $member->name ?></a>
											</h5>
											<p><strong><?= substr($designation, 0, 40); ?></strong></p>
											<h6><a href="{{url('company/members?id='.$member->company_id)}}" style="color:#cd2c47;"><?= substr($member->title, 0, 40); ?></a></h6>
										</div>
										<h3 class="commitee_staff_email">
											<i class="fa fa-envelope"
											aria-hidden="true"></i> 
											<a href="mailto:<?= ($member->email<>null)?$member->email:'N/A' ?>"><?= ($member->email<>null)?$member->email:'N/A' ?></a>
										</h3>
									</div>
									<?php
								}
							}else{?>
								<div class="items">
									<div class="alert alert-danger">No results found.</div>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					{{$filterData->links()}}
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
				@include('frontend.blocks.RightSideBarWidget')
			</div>
		</div>
	</div>
</section>
@endsection

@push('css')
<style>
	.searchResult .view a {
		font-weight: bold;
	}
	#events,#news{
		background: #ffffff;
	}
	.searchResult .view {
		padding: 5px 5px 10px;
		margin-bottom: 10px;
		border-bottom: dashed 1px #ccc;
		-webkit-transition: background 0.2s;
		transition: background 0.2s;
	}
</style>
@endpush
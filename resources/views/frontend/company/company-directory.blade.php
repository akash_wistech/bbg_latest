@extends('frontend.app')
@section('content')


<?php  
$no_image = asset('assets/images/dummy-image-1.jpg');
if (Auth::check()) {?>
	<x-contact-banner/>
	<?php
}
?>



<section class="MainArea">
	<div class="container LeftArea">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
				<div class="row">
					<div class="col-4 ">
						<h3>Company directory</h3>
					</div>
					<div class="col-12 col-sm-12 col-md-7 col-md-offset-1 col-lg-7 col-xl-7 ">
						{!! Form::open(array('url' => '/company/directory', 'method' => 'get', 'id' => 'company-searcher','enableClientScript' => false)) !!}
						<div class="TopMostSearch1 row">
							<!-- <div class="col-md-3"></div> -->
							<div class="col-md-7">
								<div class="input-group">
									{{ Form::select('user_id', [''=>__('common.select')] + getPredefinedListItemsArr(getSetting('categories_list_id')), '', ['class'=>'form-control']) }}
								</div>
							</div>
							<div class="col-md-5">
								<div class="input-group">
									<input type="search" name="search" value="<?=$search?>" class="form-control" placeholder="I’m looking for...." style="height: 29px;">
									<span class="input-group-btn">
										<button class="MySearchBtn btn btn-default" type="submit" style="height: 29px;">
											<i class="fa fa-search" aria-hidden="true"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<ul class="pagination flex-wrap">
							<li class="page-item">
								<a class="page-link" href="/company/directory">
									All
								</a>
							</li> 
							<?php
							$letters = range('A', 'Z');
							foreach ($letters as $letter) {
								?>
								<li class="page-item">
									<a class="page-link" href=" {{url('/company/directory?search=').$letter}} ">
										<?= $letter; ?>
									</a>
								</li>
								<?php
							}
							?>
						</ul>
					</div>
				</div>
				<div class="row PaddingTopBtm30px">
					<?php
					if ($companies <> null && count($companies)>0) {
						foreach ($companies as $company) {
							if ($company<>null) {
								$nonTextAreaColumnsForCompany = getNonTextAreaColumnsSomething($company->moduleTypeId);								
								$textAreaColumnsForCompany = getTextAreaColumnsSomething($company->moduleTypeId);
								$isContact='';
								if (Auth::check()) {
									$isContact = $company->checkmembercompany(Auth::user()->id, $company->id);
								}
								?>
								<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
									<div class="directorymember">
										<div class="row">
											<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
												<img  src="<?= ( !empty($company->logo)) ? $company->logo : $no_image; ?> " class="img-fluid image_150" alt="">
											</div>
											<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
												<div class="directory">
													<span class="pull-right memberContactIcon"
													id="memberContact<?= $company->id; ?>">
													<?php
													if (Auth::check()) {
														if ($isContact <> null) {?>
															<a href="javascript:;" class="add_member_contact add-false" id="<?= $company->id ?>" title="Remove" style="color: #bd1f2f !important;"><i class="fa fa-user-times" aria-hidden="true"></i></a>
															<?php
														} else {?>
															<a href="javascript:;" class="add_member_contact add-true" id="<?= $company->id ?>" title="Add to contact" style="color: #228B22 !important;"><i class="fa fa-user-plus" aria-hidden="true"></i></a>
															<?php
														}
													}
													
													?>

												</span>
												<h2><?= trim($company->title); ?></h2>

												<h3>
													<i class="fa fa-map-marker"></i> <?= ($company->address <> null) ? substr(trim($company->address), 0, 40). " ..." : '' ?>
													<br/>
													@if($nonTextAreaColumnsForCompany->isNotEmpty())
													@foreach($nonTextAreaColumnsForCompany as $nonTextAreaColumn)
													@php
													$value=getInputFielSavedValueForDetail($company,$nonTextAreaColumn);
													if(is_array($value))$value = implode(", ",$value);
													@endphp
													<i class="fa fa-tags"></i><span class="mx-2" style='color: #bd1f2f !important'>{{ ($value!=null)? $value : 'N\A' }}</span>
													@endforeach
													@endif
												</h3>

												@if($textAreaColumnsForCompany->isNotEmpty())
												@foreach($textAreaColumnsForCompany as $nonTextAreaColumn)
												@php
												$value=getInputFielSavedValueForDetail($company,$nonTextAreaColumn);
												if(is_array($value))$value = implode(", ",$value);
												@endphp
												<p style="text-decoration: none;"><?= ($value <> null) ? substr(trim($value), 0, 60). " ..."  : '' ?></p>
												@endforeach
												@endif

												<span class="EventDetail" >
													<a href="<?= url('/company/members?id='.$company->id) ?>">Read More...</a></span>
													<?php
													if (Auth::check()) {
														?>
														<span class="EventDetail"><a
															href="<?= url('/company/members?id='.$company->id) ?>">View Members</a></span>
															<?php
														}
														?>
													</div>
												</div>
											</div>
										</div>
									</div>

									<?php
								}
							}
							?>
							<div class="col-md-12 text-center">
								{{$companies->links()}}

							</div>
							<?php
						} else {
							?>
							<div class="col-md-12">
								<div class="col-md-12 alert alert-danger">No Record Found</div>
							</div>
							<?php
						}
						?>
					</div>

					<br>
				</div>
			</div>
		</div>
	</section>

	@endsection

	@push('css')
	<style>
		.TopMostSearch1{
			margin-top: 20px;
		}
		.directorymember{
			background: #e2e2e2 !important;
			padding: 10px;
			margin-bottom: 25px;
			height: 230px;
		}
	</style>
	@endpush


	@push('js')
	<script>
		$("body").find("a.add_member_contact").click(function(e) {

			e.preventDefault();  

			$(this).prop("disabled", true);

			var companyId = $(this).attr("id");
			var type = "add";  

			if( $("#"+companyId+".add_member_contact").hasClass("add-false")){
				type = "remove"; 
			}

			$.ajax({

				url: "/account/add-member-company",
				type: "POST",
				data: { 
					"_token": "{{ csrf_token() }}",
					id : companyId, type : type 
				}, 
				success : function(res){ 

					if(res == 1){ 
						console.log("type");
						console.log(type);
						console.log(res);
						if(type == "add"){ 
							$("#"+companyId+".add_member_contact").removeClass("add-true");
							$("#"+companyId+".add_member_contact").addClass("add-false");  
							$("#"+companyId+".add_member_contact").css("color","#bd1f2f","!important"); 

							$("#memberContact"+companyId+ " .fa").removeClass("fa-user-plus");    
							$("#memberContact"+companyId+ " .fa").addClass("fa-user-times");

							$(this).attr("title", "Add to contact");

						} else{ 
							$("#"+companyId+".add_member_contact").removeClass("add-false");
							$("#"+companyId+".add_member_contact").addClass("add-true"); 
							$("#"+companyId+".add_member_contact").css("color","#228B22","!important"); 

							$("#memberContact"+companyId+ " .fa").addClass("fa-user-plus");    
							$("#memberContact"+companyId+ " .fa").removeClass("fa-user-times"); 

							$(this).attr("title", "Remove from contact");
						}              

					} 
				},
				error : function(data){
					console.log(data);
				}
			}); 
		});
	</script>
	@endpush
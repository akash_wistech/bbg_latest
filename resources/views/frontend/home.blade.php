@extends('frontend.app')
@section('content')
<!-- chairman sestion -->
<?php
if(isset($chairmna_message) && $chairmna_message <> null) {
    $no_image = asset('assets/images/dummy-image-1.jpg');
    ?>
    <section class="MainArea chairmanmessage">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                    <div class="CMessage">
                        <h2><?= ($chairmna_message <> null) ? $chairmna_message->title : ''; ?></h2>
                        <p><?= ($chairmna_message <> null) ? $chairmna_message->short_description : ''; ?></p>
                        <a href="<?= url('view-from-chair-detail?id='.$chairmna_message->id,) ?>" class="Mybtn pull-left" type="button">Read More...</a>
                        <!--Add buttons to initiate auth sequence and sign out-->
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2" style="padding-top: 5px;">
                    <img src="<?= ($chairmna_message->image) ? $chairmna_message->image : $no_image; ?>" class="img-fluid img100 johnsImg" alt="">
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>

<!-- end chairman section -->

@include('frontend.blocks.eventswidget')
@include('frontend.blocks.newsletterwidget')

<!-- twitter feed section -->
<section class="socialfeeds">
    <div class="container">

        <div class="row">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="fbplugin">
                        <h2>Tweets by BBG Dubai</h2>
                    </div>
                </div>

            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 social_feeds force-overflow" id="style-3">
                <div class="force-overflow" id="style-3">
                    <a class="twitter-timeline" href="https://twitter.com/BBGOnline">Tweets by BBG Dubai</a>
                </div>
            </div>

        </div>
        <br/><br/><br/>
    </div>
</section>
<!-- end twitter feed section -->



@endsection

@push('css')
<style>
    .EventSwiper {
        padding-bottom: 35px;
    }

    .socialfeeds {
        background: #ffffff;
    }

    #style-3::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar {
        width: 6px;
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar-thumb {
        background-color: #000000;
    }

    .force-overflow {
        min-height: 450px;
    }

    @media screen and (max-width: 575px) {
        .johnsImg{
            width: 50% !important;
        }
    }

    @media screen and (max-width: 767px) {
        .johnsImg{
            width: 50% !important;
        }
    }

</style>
@endpush

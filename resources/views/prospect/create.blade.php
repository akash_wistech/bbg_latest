@extends('layouts.app')
@section('title', __('app.prospect.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'prospect'=>__('app.prospect.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('prospect._form')
@endsection

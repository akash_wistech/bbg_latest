@extends('layouts.app')
@section('title', __('app.prospect.importheading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'prospect'=>__('app.prospect.importheading'),
__('common.import')
]])
@endsection
@section('content')
{!! Form::model($model, ['id'=>'importForm']) !!}
<div class="d-none"><input type="hidden" id="csv_file_name" name="csv_file_name" value="" /></div>
<div class="card card-custom">
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
      @endforeach
    </div>
    @endif

    <div class="form-group nichng">
      {{ Form::label('file_name',__('common.csv_file')) }}
      <div class="input-group file-input">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="fas fa-file-csv"></i>
          </span>
        </div>
        <div class="custom-file">
          {{ Form::file('file_name', ['id'=>'uploadcsv','class'=>'form-control custom-file-input', 'accept'=>'.csv']) }}
          <label class="custom-file-label text-truncate" for="image">{{ __('common.choose_file') }}</label>
        </div>
      </div>
    </div>
    <label>
      {!! Form::radio('duplicate_replace', 1, true).' '.__('common.replace_existing_records') !!}
    </label>
    <label>
      {!! Form::radio('duplicate_replace', 0, false).' '.__('common.ignore_duplicate') !!}
    </label>
    <div id="mapFields">
    </div>
  </div>
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">{{__('common.save')}}</button>
    <a href="{{ url('/prospect') }}" class="btn btn-default">{{__('common.cancel')}}</a>
  </div>
</div>
{!! Form::close() !!}
@endsection

@push('jsScripts')
$("body").on("change", "#uploadcsv", function (e) {
  var file_data = $(this).prop('files')[0];
  var form_data = new FormData();
  form_data.append('file', file_data);
  $.ajax({
    url: '{{url('prospect/upload')}}',
    dataType: 'json',
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    success: function(response){
      $("#csv_file_name").val(response["success"]["name"]);
      $("#mapFields").html(response["success"]["tableHtml"]);
    }
  });
});
$("body").on("click", "#cbDupR", function () {
  if($(this).prop("checked")){
    $("#dupreptxt").html("{{__('common.replace_existing_records')}}");
  }else{
    $("#dupreptxt").html("{{__('common.ignore_duplicate')}}");
  }
});
@endpush

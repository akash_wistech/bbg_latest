@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/prospect/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>'/prospect/import'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.reference_no'),'data'=>'reference_no','name'=>'reference_no'];
$dtColsArr[]=['title'=>__('common.full_name'),'data'=>'full_name','name'=>'full_name'];
$advSearchCols[]=['title'=>__('common.full_name'),'name'=>'full_name'];
$dtColsArr[]=['title'=>__('common.email'),'data'=>'email','name'=>'email'];
$advSearchCols[]=['title'=>__('common.email'),'name'=>'email'];
$dtColsArr[]=['title'=>__('common.phone'),'data'=>'phone','name'=>'phone'];
$advSearchCols[]=['title'=>__('common.phone'),'name'=>'phone'];
$dtColsArr[]=['title'=>__('common.company_name'),'data'=>'company_name','name'=>'company_name'];
$advSearchCols[]=['title'=>__('common.company_name'),'name'=>'company_name'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}

$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
@endphp

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('app.prospect.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('app.prospect.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table1" class="card card-body">
    <x-data-table-grid :request="$request" :showStats="false" :controller="'prospect'" :cbSelection="true" :exportMenu="true" :jsonUrl="'prospect/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="true" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>
@endsection

@extends('layouts.app')
@section('title', __('app.prospect.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'prospect'=>__('app.prospect.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('prospect._form')
@endsection

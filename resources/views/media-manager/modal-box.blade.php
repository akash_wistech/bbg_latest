<?php
use Illuminate\Http\Request;

// echo "<pre>"; print_r($viewData['dataProvider']['folder_name']); echo "</pre>"; echo "<br>"; 
// echo "<pre>"; print_r("viewData dataProvider parent_is: ".$viewData['dataProvider']['parent_id']); echo "<br>"; 
// echo "<pre>"; print_r($viewData['data']['target']); echo "</pre>";  echo "<br>";
// echo "<pre>"; print_r($viewData['data']['thumb']); echo "</pre>";  //die();

?>

<div class="modal-dialog modal-xl" data-backdrop="static">
	<div class="modal-content p-0 b-0">
		<div class="modal-header">
			<?php
			if ($viewData['dataProvider']['folder_name']!='') {?>
				<h5 class="modal-title" id="exampleModalLabel"><span class="">{{$viewData['dataProvider']['folder_name']}}</span> </h5>
				<?php
			} else{?>
				<h5 class="modal-title" id="exampleModalLabel">Media Manager </h5>
				<?php
			}
			?>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<i aria-hidden="true" class="ki ki-close"></i>
			</button>
		</div>
		<div class="card card-custom card-stretch example example-compact" id="kt_page_stretched_card">
			<div class="card-header pb-0 mb-0 "  style="">
				<div class="col">
					<div class="row mt-6 ">
						<div class="col-6">
							<div class="form-group">
								<div class="input-group">
									<input type="text" name="search" value="<?= $viewData['dataProvider']['name'] ?>" class="form-control" placeholder="Search...">
									<div class="input-group-append">
										<button type="button" id="button-search" class="btn btn-secondary"data-toggle="tooltip" title="Search">
											<i class="fa flaticon-search"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<div class="col-3"></div>
					</div>
				</div>
				<div class="card-toolbar mx-4">
					<div class="example-tools justify-content-center">
						<a href="{{ url('file-manager?parent_id='.$viewData['data']['parent'].'&&target='.$viewData['data']['target'].'&&thumb='.$viewData['data']['thumb']) }}" data-toggle="tooltip" title="Parent" id="button-parent" type="button"  class="btn btn-icon btn-primary  ml-2">
							<i class="fa flaticon2-left-arrow"></i>
						</a>
						<a href="{{$refreshUrl}}" data-toggle="tooltip" title="Refresh" id="button-refresh" type="button" class="btn btn-icon btn-secondary  ml-2">
							<i class="fa flaticon2-refresh-1"></i>
						</a>
						<button data-toggle="tooltip" title="Upload" id="button-upload" type="button" class="btn btn-icon btn-warning  ml-2">
							<i class="fa flaticon-upload"></i>
						</button>
						<button data-toggle="tooltip" title="New Folder" id="button-folder" type="button" class="btn btn-icon btn-info  ml-2">
							<i class="fa flaticon-folder"></i>
						</button>
						<button data-toggle="tooltip" title="Delete" id="button-delete" type="button" class="btn btn-icon btn-danger  ml-2">
							<i class="fa flaticon2-trash"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="card-body pb-0 mb-0 px-0 pr-2">
				<div class="card-scroll" style="height: 420px;">
					<div class="row px-0">
						<?php 
						$resources = $viewData['dataProvider']['query'];
						foreach ($resources as $resource) { 
							?>
							<div class="col-sm-3 media-manager my-4 px-0">
								<!-- folder section -->
								<?php if ($resource->type == 'folder') { ?>
									<div class="text-center">
										<div class="">
											<a href="{{ url('file-manager?parent_id='.$resource->id.'&&target='.$viewData['data']['target'].'&&thumb='.$viewData['data']['thumb']) }}" class="directory" href="" style="vertical-align: middle">
												<i class="fa flaticon2-folder fa-5x"></i>
											</a>
										</div>
										<div class="">
											<label class="">
												<input type="checkbox" name="path[]" value="<?= $resource->id; ?>">
												<span></span>
												<?php echo substr($resource->name, 0, 20); ?>
											</label>
										</div>
									</div>
								<?php } ?>
								<!-- end  -->
								<!-- file section start -->
								<?php if ($resource->type == 'file') { ?>
									<div class="text-center">
										<div>
											<a href="javascript:;" class="thumbnail">
												<img class="media-image" 
												src="<?= $resource->href; ?>" 
												alt="<?= $resource->name; ?>" 
												title="<?= $resource->name; ?>" 
												width="180" />
											</a>
										</div>
										<div class="py-2">
											<label>
												<input type="checkbox" name="path[]" value="<?= $resource->id; ?>" />
												<?php echo substr($resource->name, 0, 10); ?>
											</label>
										</div>
									</div>
								<?php } ?>
								<!-- end -->
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="pt-4 mx-8 d-flex justify-content-end pagination">
				<?php echo $resources->withQueryString()->links() ?>
			</div>
		</div>
	</div>
</div>

<?php 
// echo $viewData['data']['thumb']; echo "<br>";
// echo $viewData['data']['target']; die();
?>

<!-- pagination code start -->
<script>
	$('.pagination a').on('click', function(e) {
		e.preventDefault();
		$('#modal-image').load($(this).attr('href'));
	});
</script>
<!-- end -->

<!-- assignment -->
<script>
	var parent_id = "{{ $viewData['dataProvider']['parent_id']!=null && $viewData['dataProvider']['parent_id']>0 ? $viewData['dataProvider']['parent_id'] : 0 }}";
	var thumb_id = "{{ $viewData['data']['thumb'] }}";
	var target_id = "{{ $viewData['data']['target'] }}";
	// console.log("str");
	console.log(parent_id);
	console.log(thumb_id);
	console.log(target_id);
	// console.log("ennd");
</script>
<!-- end -->

<!-- click on image  code start -->
<script>
	$('a.thumbnail').on('click', function(e) {
		e.preventDefault();
		$("a[data-thumbid=\""+thumb_id+"\"]").find('img').attr('src', $(this).find('img').attr('src'));
		$("input[data-targetid=\""+target_id+"\"]").attr('value', $(this).find('img').attr('src'));
		$('#modal-image').modal('hide');
	});
</script>
<!-- end -->

<!-- parent button code -->
<script>
	$('#button-parent').on('click', function(e) {
		e.preventDefault();
		$('#modal-image').load($(this).attr('href'));
	});
</script>
<!-- end parent button code -->

<!-- start refresh button code -->
<script>
	$('#button-refresh').on('click', function(e) {
		e.preventDefault();

		$('#modal-image').load($(this).attr('href'));
	});
</script>
<!-- end refresh button code -->

<!-- click on folder code -->
<script>
	$('a.directory').on('click', function(e) {
		e.preventDefault();
		// alert("helloww");
		$('#modal-image').load($(this).attr('href'));
	});
</script>
<!-- end click on folder code -->

<!-- make new folder -->
<script>
	$('#button-folder').popover({
		html: true,
		container: 'body',
		placement: 'bottom',
		trigger: 'click',
		title: 'Folder Name',
		sanitize: false,
		content: function() {
			html  = '<div class="input-group input-group-sm pop-class">';
			html += '<input type="text" name="folder" value="" placeholder="Folder Name..." class="form-control">';
			html += '<div class="input-group-append"><button type="button" title="New Folder" id="button-create" class="btn btn-secondary"><i class="fs flaticon2-plus-1"></i></button></div>';
			html += '</div>';
			return html;
			
		}
	});

	$('#button-folder').on('shown.bs.popover', function() {
		$('#button-create').on('click', function() {
			var folder = $('input[name=\'folder\']').val();
			console.log(folder);
			data = {folder:folder};
			$.ajax({
				url: 'file-manager/folder?parent_id='+parent_id,
				type: 'post',
				dataType: 'json',
				data: {
					"_token": "{{ csrf_token() }}",
					"data": data,
				},
				beforeSend: function() {
					$('#button-create').prop('disabled', true);
				},
				complete: function() {
					$('#button-create').prop('disabled', false);
				},
				success: function(json) {
					if (json['error']) {
						swal({
							title: 'Opps!',
							type: 'error',
							text: json['error']
						});
					}
					if (json['success']) {
						$('#button-folder').popover('hide', function() {
							$('.popover').remove();
						});
						swal({
							title: 'Done!',
							type: 'success',
							text: json['success']
						});
						$('#button-refresh').trigger('click');
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		});
	});
</script>
<!-- end make new folder -->

<!-- upload file code -->
<script>
	$('#button-upload').on('click', function() {
		$('#form-upload').remove();
		$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;">{{ csrf_field() }}<input type="file" name="file" value="" /></form>');
		$('#form-upload input[name=\'file\']').trigger('click');
		
		if (typeof timer != 'undefined') {
			clearInterval(timer);
		}
		timer = setInterval(function() {
			if ($('#form-upload input[name=\'file\']').val() != '') {
				clearInterval(timer);
				$.ajax({
					url: 'file-manager/upload?parent_id='+parent_id,
					method: 'post',
					data: new FormData($('#form-upload')[0]),
					dataType: 'JSON',
					contentType: false,
					cache: false,
					processData: false,
					beforeSend: function() {
						$('#button-upload i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
						$('#button-upload').prop('disabled', true);
					},
					complete: function() {
						$('#button-upload i').replaceWith('<i class="fa fa-upload"></i>');
						$('#button-upload').prop('disabled', false);
					},
					success: function(json) {
						if (json['error']) {
							swal({
								title: 'Opps!',
								type: 'error',
								text: json['error']
							});
						}
						if (json['success']) {
							swal({
								title: 'Success!',
								type: 'success',
								text: json['success'],
							});
							$('#button-refresh').trigger('click');
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}

				});
			}
		}, 500);
	});
</script>
<!-- end upload code -->

<!-- delete code start -->
<script>
	$("#button-delete").click(function(e) {
		var limits = $('input[name^=\'path\']:checked');
		if (limits.length>0) {
			Swal.fire({
				title: "Are you sure?",
				text: "You wont be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true
			}).then(function(result) {
				if (result.value) {
					$.ajax({
						url: 'file-manager/delete',
						type: 'post',
						dataType: 'json',
						data: $('input[name^=\'path\']:checked'),
						beforeSend: function() {
							$('#button-delete').prop('disabled', true);
						},
						complete: function() {
							$('#button-delete').prop('disabled', false);
						},
						success: function(json) {
							if (json['error']) {
								swal({
									title: 'Opps!',
									type: 'error',
									text: json['error'],
								});
							}
							if (json['success']) {
								swal({
									title: 'Success!',
									type: 'success',
									text: json['success'],
								});

								$('#button-refresh').trigger('click');
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				} else if (result.dismiss === "cancel") {
					Swal.fire(
						"Cancelled",
						"Your imaginary file is safe :)",
						"error"
						)
				}
			});
		}else{
			Swal.fire(
				"Opps!",
				"Plese Select Atleat One File!",
				"error"
				)
		}

	});
</script>
<!-- delete code end -->

<!-- search code start -->
<script>
	$('input[name=\'search\']').on('keydown', function(e) {
		if (e.which == 13) {
			$('#button-search').trigger('click');
		}
	});
	$('#button-search').on('click', function(e) {
		var url = 'file-manager?parent_id='+parent_id;
		var filter_name = $('input[name=\'search\']').val();
		if (filter_name) {
			url += '&name=' + encodeURIComponent(filter_name);
		}
		<?php if (isset( $viewData['data']['thumb']) && !empty( $viewData['data']['thumb'])) { ?>
			url += '&thumb=' + thumb_id;
		<?php } ?>

		<?php if (isset( $viewData['data']['target'] ) && !empty($viewData['data']['target'])) { ?>
			url += '&target=' + target_id;
		<?php } ?>
		$('#modal-image').load(url);
	});
</script>
<!-- search code end -->




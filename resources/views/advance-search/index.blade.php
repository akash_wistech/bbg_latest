@extends('layouts.blank')

@section('content')
<div class="card card-custom">
  <div id="grid-table-advsrc" class="card card-body">
    <div class="overlay-wrapper">
      <table class="table mb-0">
        <thead>
          <tr>
            <th>{{__('common.title')}}</th>
            <th width="100">{{__('common.action')}}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($results as $result)
          @php

          $urlPre = getModelUrlPre()[$module];
          $urlSearchParams='';
          $ssInputVals = getSavedSearchInputValues($result->id);
          if($ssInputVals!=''){
            foreach($ssInputVals as $ssInputVal){
              $urlSearchParams.=($urlSearchParams!='' ? '&' : '').$ssInputVal->input_name.'='.$ssInputVal->input_value;
            }
          }

          $searcUrl = $urlPre.($urlSearchParams!='' ? '?' : '').$urlSearchParams;
          @endphp
          <tr>
            <td>{{$result->title}}</td>
            <td>
              <a href="{{url($searcUrl)}}" class="btn btn-sm btn-clean btn-icon" data-id="{{$result->id}}" data-toggle="tooltip" data-title="{{(__('app.saved_search.use_this'))}}">
                <i class="text-dark-50 flaticon-search"></i>
              </a>
              <a href="{{ url('saved-search/delete/'.$result->id)}}" class='btn btn-sm btn-clean btn-icon act-confirmation' data-toggle="tooltip" title='{{__('common.delete')}}' data-id="{{$result->id}}" data-confirmationmsg="{{__('common.deleteconfirmation')}}" data-pjxcntr="grid-table-advsrc">
                <i class="text-dark-50 flaticon-delete"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {{$results->links('vendor.pagination.default')}}
    </div>
  </div>
</div>
@endsection
@push('jsScripts')
$(document).pjax('#grid-table-advsrc .pagination a', '#grid-table-advsrc',{push:false})
$('[data-toggle="tooltip"]').tooltip();
@endpush

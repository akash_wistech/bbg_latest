@if (! empty($breadcrumbs))
<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 mb-2 font-size-lg">
  <li class=" text-muted">
    <a href="{{ route('dashboard') }}" class="text-white">
      {{__('app.general.home')}}
    </a>
  </li>
  @foreach ($breadcrumbs as $link => $label)

    @if (is_int($link) && ! is_int($label))
    <li class=" ">
      <i class="text-white-50 flaticon2-right-arrow pl-4"></i>
      <span style="padding-top:1px; padding-left:3px; color:white">{{ $label }}</span>
    </li>
    @else
    <li class=" text-white">
      <a href="{{ url($link) }}" class="text-white">
        <i class="text-white-50 flaticon2-right-arrow pl-4"></i>
        {{ $label }}
      </a>
    </li>
    @endif
  @endforeach
</ul>
@endif

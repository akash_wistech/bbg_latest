@extends('layouts.app')
@section('title', __('app.settings.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('app.settings.heading')
]])
@endsection

<?php
$show_in_committe = getSetting('staff_categories_list');
?>

@section('content')


<div class="card card-custom">
  {!! Form::model($model, ['files' => true,'id'=>'kt_form']) !!}
  <!--begin::Header-->

  <!--end::Header-->
  <!--begin::Body-->
  <div class="card-body">
    <div class="tab-content pt-3">
      <!--begin::Tab Pane-->
      <div class="tab-pane active" id="kt_builder_extras">
        <div class="row">
          <div class="col-2">
            <!--begin::Tab Inner-->
            <ul class="nav nav-bold nav-light-primary nav-pills flex-column" data-remember-tab="tab_extra_id">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#config"><b>CONFIGURATION</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#general"><b>GENERAL</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#data"><b>DATA</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#loyalty"><b>LOYALTY</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#email-setup"><b>EMAIL SETUP</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#social-data"><b>SOCIAL DATA</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#image-sizes"><b>IMAGE SIZES</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#zoho"><b>ZOHO</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#frontend"><b>Frontend</b></a>
              </li>
            </ul>
            <!--end::Tab Inner-->
          </div>
          <div class="col-10">
            <!--begin::Tab Content Inner-->
            <div class="tab-content mt-5">

              <div class="tab-pane active" id="config">
                <div class="row">
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('grid_page_size', __('app.settings.grid_page_size')) !!}
                      <div class="input-group igb mb-3">
                        {{ Form::select('grid_page_size', getPageSizeAssArr(), null, ['class'=>'form-control']) }}
                        <div class="input-group-append">
                          <span class="input-group-text ctv">{{__('app.settings.per_page')}}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('contactroles_list_id', __('app.settings.contactroles_list')) !!}
                      {{ Form::select('contactroles_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('jobroles_list_id', __('app.settings.jobroles_list')) !!}
                      {{ Form::select('jobroles_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('staffjobroles_list_id', __('Staff Job Roles')) !!}
                      {{ Form::select('staffjobroles_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('country_list_id', __('app.settings.country_list')) !!}
                      {{ Form::select('country_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('service_list_id', __('app.settings.service_list')) !!}
                      {{ Form::select('service_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('sector_industry_list_id', __('app.settings.sector_industry_list_id')) !!}
                      {{ Form::select('sector_industry_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('clients_permission_group', __('app.settings.clients_permission_group')) !!}
                      {{ Form::select('clients_permission_group', getAdminGroupList(), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('news_categories_list', __('app.settings.news_categories_list')) !!}
                      {{ Form::select('news_categories_list', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('howDidYouHear', __('app.settings.howDidYouHear')) !!}
                      {{ Form::select('howDidYouHear', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('personal_titles', __('app.settings.personal_titles')) !!}
                      {{ Form::select('personal_titles', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('staff_categories_list', __('app.settings.staff_categories_list')) !!}
                      {{ Form::select('staff_categories_list', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('business_team_list', __('app.settings.business_team_list')) !!}
                      {{ Form::select('business_team_list', getPredefinedListOptionsArr($show_in_committe), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('future_vision_collective_list', __('app.settings.future_vision_collective_list')) !!}
                      {{ Form::select('future_vision_collective_list', getPredefinedListOptionsArr($show_in_committe), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('categories_list_id', __('app.settings.categories_list_id')) !!}
                      {{ Form::select('categories_list_id', getPredefinedListOptionsArr(0), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('joining_fee_id', __('app.settings.joining_fee')) !!}
                      {{ Form::select('joining_fee_id', getPredefinedListOptionsArr(getSetting('service_list_id')), null, ['class'=>'form-control']) }}
                    </div>
                  </div>



                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('member_news_id', __('app.settings.member_news_id')) !!}
                      {{ Form::select('member_news_id', getPredefinedListOptionsArr(getSetting('news_categories_list')), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('expo_2020_id', __('app.settings.expo_2020_id')) !!}
                      {{ Form::select('expo_2020_id', getPredefinedListOptionsArr(getSetting('news_categories_list')), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('guest_articles_id', __('app.settings.guest_articles_id')) !!}
                      {{ Form::select('guest_articles_id', getPredefinedListOptionsArr(getSetting('news_categories_list')), null, ['class'=>'form-control']) }}
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('proforma_default', __('app.settings.proforma_default')) !!}
                      {{ Form::select('proforma_default', getYesNoArr(), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('invoice_template', __('sales::invoices.invoice_template')) !!}
                      {{ Form::select('invoice_template', getTemplatesforInvoices(), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  @if(function_exists('getProposalPagesListArr'))
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('invoice_terms_content', __('sales::invoices.invoice_terms_content')) !!}
                      {{ Form::select('invoice_terms_content', getProposalPagesListArr(), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  @endif
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('show_in_committe_members', __('app.settings.show_in_committe_members')) !!}
                      {{ Form::select('show_in_committe_members[]', getPredefinedListOptionsArr($show_in_committe), null, ['class'=>'form-control select2','multiple' => 'true',]) }}
                    </div>
                  </div>
                  @if(function_exists('getListNamesArr'))
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('mailing_list_id', __('campaign_langs::campaign_langs.mailing_list_id')) !!}
                      {{ Form::select('mailing_list_id', getListNamesArr(), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  @endif

                </div>
              </div>



              <!--begin::Tab Pane Inner-->
              <div class="tab-pane" id="general">
                <div class="row">
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('app_name',__('app.settings.app_name')) }}
                      {{ Form::text('app_name', $model->app_name, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('currency',__('app.settings.currency_sign')) }}
                      {{ Form::select('currency', getCurrenciesListArr(), $model->currency, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {!! Form::label('week_start', __('app.settings.week_start')) !!}
                      {{ Form::select('week_start', getWeekDaysArr(), null, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('app_url',__('app.settings.app_url')) }}
                      {{ Form::text('app_url', $model->app_url, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('app_owner',__('app.settings.app_owner')) }}
                      {{ Form::text('app_owner', $model->app_owner, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('admin_email',__('app.settings.admin_email')) }}
                      {{ Form::text('admin_email', $model->admin_email, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('from_email',__('app.settings.from_email')) }}
                      {{ Form::text('from_email',  $model->from_email, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('event_email',__('app.settings.event_email')) }}
                      {{ Form::text('event_email',  $model->event_email, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('membership_email',__('app.settings.membership_email')) }}
                      {{ Form::text('membership_email', $model->membership_email, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('app_vat',__('app.settings.app_vat')) }}
                      {{ Form::text('app_vat',  $model->app_vat, ['class'=>'form-control']) }}
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('telephone',__('app.settings.telephone')) }}
                      {{ Form::text('telephone',  $model->telephone, ['class'=>'form-control']) }}
                    </div>
                  </div>


                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('fax',__('app.settings.fax')) }}
                      {{ Form::text('fax', $model->fax, ['class'=>'form-control']) }}
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('location',__('app.settings.location')) }}
                      {{ Form::textarea('location', $model->location, ['class'=>'form-control', 'rows' => 2]) }}
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('company_address',__('app.settings.company_address')) }}
                      {{ Form::textarea('company_address', $model->company_address, ['class'=>'form-control', 'rows' => 4]) }}
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('about',__('app.settings.about')) }}
                      {{ Form::textarea('about', $model->about, ['class'=>'form-control editor']) }}
                    </div>
                  </div>


                  <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                      {{ Form::label('invoice_payment_info',__('app.settings.invoice_payment_info')) }}
                      {{ Form::textarea('invoice_payment_info', $model->invoice_payment_info, ['class'=>'form-control editor']) }}
                    </div>
                  </div>
                </div>

              </div>
              <!--end::Tab Pane Inner-->

              <!--begin::Tab Pane Inner-->
              <div class="tab-pane" id="data">
                <div class="row px-0">
                  <div class="col-md-4 col-lg-5 px-0">
                    <div class="col-12">
                      <div class="form-group">
                        {{ Form::label('geo_code',__('app.settings.geo_code')) }}
                        {{ Form::text('geo_code', $model->geo_code, ['class'=>'form-control']) }}
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        {{ Form::label('meta_title',__('app.settings.meta_title')) }}
                        {{ Form::text('meta_title', $model->meta_title, ['class'=>'form-control']) }}
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-lg-5  px-0 ">

                    <div class="col-12">
                      <div class="form-group">
                        {{ Form::label('cancel_grace_period',__('app.settings.cancel_grace_period')) }}
                        {{ Form::text('cancel_grace_period', $model->cancel_grace_period, ['class'=>'form-control']) }}
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        {{ Form::label('meta_tag',__('app.settings.meta_tag')) }}
                        {{ Form::text('meta_tag', $model->meta_tag, ['class'=>'form-control']) }}
                      </div>
                    </div>
                  </div>
                  <!-- image here -->
                  <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-4">
                    <div id="images" class="form-group pb-0 mb-2">
                      <a href="" data-thumbid="thumb-image-app-logo" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
                        <img src="{{ $model->app_logo != '' && $model->app_logo <> null ? $model->app_logo : asset('assets/images/dummy-image.jpg') }}"
                        alt="" width="145" height="125" title=""
                        data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
                      </a>
                      {{ Form::text('app_logo', $model->app_logo, ['maxlength' => true, 'data-targetid' => 'input-image-app-logo','class'=>'form-control d-none']) }}
                    </div>
                    <span class="label label-xl label-muted font-weight-bold label-inline mx-8">{{__('app.settings.app_logo')}}</span>
                  </div>
                  <!-- image end -->
                </div>

                <div class="row px-0">
                  <div class="col-md-4 col-lg-10 px-0">


                    <div class="col-12">
                      <div class="form-group">
                        {{ Form::label('meta_tag_description',__('app.settings.meta_tag_description')) }}
                        {{ Form::textarea('meta_tag_description', $model->meta_tag_description, ['class'=>'form-control','rows'=>6]) }}
                      </div>
                    </div>
                  </div>


                  <!-- image here -->
                  <div class="col-xs-12 col-md-2 d-flex align-items-end flex-column pt-4">
                    <div id="images" class="form-group pb-0 mb-2">
                      <a href="" data-thumbid="thumb-image-footer-logo" data-toggle="image" class="img-thumbnail" data-content=""  width="20">
                        <img src="{{ $model->footer_logo != '' && $model->footer_logo <> null ? $model->footer_logo : asset('assets/images/dummy-image.jpg') }}"
                        alt="" width="145" height="125" title=""
                        data-placeholder="{{asset('assets/images/dummy-image.jpg')}}"/>
                      </a>
                      {{ Form::text('footer_logo', $model->footer_logo, ['maxlength' => true, 'data-targetid' => 'input-image-footer-logo','class'=>'form-control d-none']) }}
                    </div>
                    <span class="label label-xl label-muted font-weight-bold label-inline mx-8">{{__('app.settings.footer_logo')}}</span>
                  </div>
                  <!-- image end -->
                </div>

                <div class="row">
                  <div class="col-10">
                    <div class="form-group">
                      {{ Form::label('copyright_text',__('app.settings.copyright_text')) }}
                      {{ Form::textarea('copyright_text', $model->copyright_text, ['class'=>'form-control','rows'=>6]) }}
                    </div>
                  </div>
                </div>
              </div>

              <!--end::Tab Pane Inner-->
              <!--begin::Tab Pane Inner-->
              <div class="tab-pane" id="loyalty">
                <div class="row mx-4">
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('points_on_spent',__('app.settings.points_on_spent')) }}
                      {{ Form::text('points_on_spent', $model->points_on_spent, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('points_of_spent',__('app.settings.points_of_spent')) }}
                      {{ Form::text('points_of_spent', $model->points_of_spent, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('min_redeem_amount',__('app.settings.min_redeem_amount')) }}
                      {{ Form::text('min_redeem_amount', $model->min_redeem_amount, ['class'=>'form-control']) }}
                    </div>
                  </div>
                </div>
              </div>
              <!--end::Tab Pane Inner-->
              <!--begin::Tab Pane Inner-->
              <div class="tab-pane" id="email-setup">
                <div class="row mx-4">
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('smtp_email',__('app.settings.smtp_email')) }}
                      {{ Form::text('smtp_email', $model->smtp_email, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('smtp_username',__('app.settings.smtp_username')) }}
                      {{ Form::text('smtp_username', $model->smtp_username, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                      {{ Form::label('smtp_password',__('app.settings.smtp_password')) }}
                      {{ Form::text('smtp_password', $model->smtp_password, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('smtp_hash',__('app.settings.smtp_hash')) }}
                      {{ Form::select('smtp_hash',['ssl' => 'Ssl', 'tls' => 'Tls'], $model->smtp_hash, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('smtp_port',__('app.settings.smtp_port')) }}
                      {{ Form::text('smtp_port', $model->smtp_port, ['class'=>'form-control']) }}
                    </div>
                  </div>
                </div>
              </div>
              <!--end::Tab Pane Inner-->
              <!--begin::Tab Pane Inner-->
              <div class="tab-pane" id="social-data">
                <div class="row mx-4">
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('youtube',__('app.settings.youtube')) }}
                      {{ Form::text('youtube', $model->youtube, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('youtube_icon',__('app.settings.youtube_icon')) }}
                      {{ Form::text('youtube_icon', $model->youtube_icon, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('twitter',__('app.settings.twitter')) }}
                      {{ Form::text('twitter', $model->twitter, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('twitter_icon',__('app.settings.twitter_icon')) }}
                      {{ Form::text('twitter_icon', $model->twitter_icon, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('facebook',__('app.settings.facebook')) }}
                      {{ Form::text('facebook', $model->facebook, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('facebook_icon',__('app.settings.facebook_icon')) }}
                      {{ Form::text('facebook_icon', $model->facebook_icon, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('linkedin',__('app.settings.linkedin')) }}
                      {{ Form::text('linkedin', $model->linkedin, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('linkedin_icon',__('app.settings.linkedin_icon')) }}
                      {{ Form::text('linkedin_icon', $model->linkedin_icon, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('pinterest',__('app.settings.pinterest')) }}
                      {{ Form::text('pinterest', $model->pinterest, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('pinterest_icon',__('app.settings.pinterest_icon')) }}
                      {{ Form::text('pinterest_icon', $model->pinterest_icon, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('instagram',__('app.settings.instagram')) }}
                      {{ Form::text('instagram', $model->instagram, ['class'=>'form-control']) }}
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                      {{ Form::label('instagram_icon',__('app.settings.instagram_icon')) }}
                      {{ Form::text('instagram_icon', $model->instagram_icon, ['class'=>'form-control']) }}
                    </div>
                  </div>
                </div>
              </div>
              <!--end::Tab Pane Inner-->
              <!--begin::Tab Pane Inner-->
              <div class="tab-pane" id="image-sizes">
               <div class="row mx-4">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('grid_size_width',__('app.settings.grid_size_width')) }}
                    {{ Form::text('grid_size_width', $model->grid_size_width, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('grid_size_height',__('app.settings.grid_size_height')) }}
                    {{ Form::text('grid_size_height', $model->grid_size_height, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('listing_size_width',__('app.settings.listing_size_width')) }}
                    {{ Form::text('listing_size_width', $model->listing_size_width, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('listing_size_height',__('app.settings.listing_size_height')) }}
                    {{ Form::text('listing_size_height', $model->listing_size_height, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('additional_image_size_width',__('app.settings.additional_image_size_width')) }}
                    {{ Form::text('additional_image_size_width', $model->additional_image_size_width, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('additional_image_size_height',__('app.settings.additional_image_size_height')) }}
                    {{ Form::text('additional_image_size_height', $model->additional_image_size_height, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('popup_size_width',__('app.settings.popup_size_width')) }}
                    {{ Form::text('popup_size_width', $model->popup_size_width, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('popup_size_height',__('app.settings.popup_size_height')) }}
                    {{ Form::text('popup_size_height', $model->popup_size_height, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('page_banner_size_width',__('app.settings.page_banner_size_width')) }}
                    {{ Form::text('page_banner_size_width', $model->page_banner_size_width, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('page_banner_size_height',__('app.settings.page_banner_size_height')) }}
                    {{ Form::text('page_banner_size_height', $model->page_banner_size_height, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('home_banner_size_width',__('app.settings.home_banner_size_width')) }}
                    {{ Form::text('home_banner_size_width', $model->home_banner_size_width, ['class'=>'form-control']) }}
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    {{ Form::label('home_banner_size_height',__('app.settings.home_banner_size_height')) }}
                    {{ Form::text('home_banner_size_height', $model->home_banner_size_height, ['class'=>'form-control']) }}
                  </div>
                </div>
              </div>
            </div>
            <!--end::Tab Pane Inner-->
            <!--begin::Tab Pane Inner-->
            <div class="tab-pane" id="zoho">
              <div class="row mx-8">
                <div class="col-xs-12 col-sm-2 py-4 mx-4">
                  <div class="form-group">
                    Financial Year:
                  </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                  <div class="form-group">
                    {{ Form::select('from', getMonthArr(), $model->from, ['class'=>'form-control']) }}
                    <span class="form-text text-muted mx-2">From.</span>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                  <div class="form-group">
                    {{ Form::select('to', getMonthArr(), $model->to, ['class'=>'form-control']) }}
                    <span class="form-text text-muted mx-2">To.</span>
                  </div>
                </div>
              </div>

              <div class="row mx-8">
                <div class="col-xs-12 col-sm-2 py-4 mx-4">
                  <div class="form-group">
                    Starting From:
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group">
                    <div class="input-group date">
                      {{ Form::text('starting_from',  $model->starting_from, ['class'=>'form-control dtpicker', 'readonly']) }}
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="la la-calendar"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row mx-8">
                <div class="col-xs-12 col-sm-12">
                 <div class="form-group">
                  <label class="checkbox checkbox-outline checkbox-success">
                    {{ Form::checkbox('create_program', '', $model->create_program_value==1 ? true : false, ['class' => 'statring-from', 'data-id' => 'create-program-box'] )}}
                    <span class="mx-3"></span>Create Program Invoice First Starting From</label>
                    {{ Form::text('create_program_value', $model->create_program_value, ['class'=>'form-control create-program-value d-none']) }}
                  </div>
                </div>
              </div>



              <div class="row mx-8">
                <div class="col-xs-12 col-sm-12">
                 <div class="form-group">
                  <label class="checkbox checkbox-outline checkbox-success">
                    {{ Form::checkbox('separate_revenue', '', $model->separate_revenue_value==1 ? true : false,['class' => 'statring-from', 'data-id' => 'separate-revenue-box'] )}}
                    <span class="mx-3"></span>Separate Revenue Based On Financial Years.</label>
                    {{ Form::text('separate_revenue_value', $model->separate_revenue_value, ['class'=>'form-control separate-revenue-value d-none']) }}
                  </div>
                </div>
              </div>


              <div class="row mx-8">
                <div class="col-xs-12 col-sm-2 py-4 mx-4">
                  <div class="form-group">
                    Current Year Revenue:
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group">
                    {{ Form::text('current_year_revenue', $model->current_year_revenue, ['class'=>'form-control']) }}
                  </div>
                </div>
              </div>

              <div class="row mx-8">
                <div class="col-xs-12 col-sm-2 mx-4">
                  <div class="form-group">
                    Next Year Revenue:
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group">
                    {{ Form::text('next_year_revenue', $model->next_year_revenue, ['class'=>'form-control']) }}
                  </div>
                </div>
              </div>
            </div>
            <!--end::Tab Pane Inner-->


            <div class="tab-pane active" id="frontend">
              <div class="col-sm-4">
                <div class="form-group">
                  {!! Form::label('frontend_invoice_status', __('sales::invoices.frontend_invoice_status')) !!}
                  {{ Form::select('frontend_invoice_status', ['0'=>'Select', '1'=>'Nominee', '10'=>'All members'], null, ['class'=>'form-control']) }}
                </div>
              </div>
            </div>


          </div>
          <!--begin::Tab Pane Inner-->
        </div>
        <!--end::Tab Content Inner-->
      </div>
    </div>
  </div>
  <!--end::Tab Pane-->
</div>

<div class="card-footer border-0">
  {!! Form::submit(__('common.save'), ['class' => 'btn btn-success']) !!}
</div>
{!! Form::close() !!}
</div>
<!--end::Body-->
@endsection



@push('css')
@endpush
@push('js')
<script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}"></script>
<script>
  tinymce.init({
    selector: '.editor',
    menubar: false,
    toolbar: ['styleselect fontselect fontsizeselect | link image table responsivefilemanager ', 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code','advlist | autolink | lists'],
    plugins : 'advlist autolink link image lists charmap code table',
    image_advtab: true ,
  });
</script>


<script>
 $("[data-id=\'separate-revenue-box\']").click(function(){
  if($(this).prop("checked") == true){
    $('.separate-revenue-value').val(1);
  }
  else if($(this).prop("checked") == false){
    $('.separate-revenue-value').val(0);
  }
});

 $("[data-id=\'create-program-box\']").click(function(){
  if($(this).prop("checked") == true){
    $('.create-program-value').val(1);
  }
  else if($(this).prop("checked") == false){
    $('.create-program-value').val(0);
  }
});
</script>
@endpush

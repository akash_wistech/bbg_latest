<div class="card card-custom card-border mb-3 card-collapse" data-card="true">
  <div class="card-header">
    <h3 class="card-title">{{ __('app.opportunity.heading') }} ({{$totalCount}})</h3>
		<div class="card-toolbar">
			<a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Toggle Card">
				<i class="ki ki-arrow-down icon-nm"></i>
			</a>
		</div>
  </div>
  <div class="card-body p-2" style="display:none;">
    <div id="oppr-lv-container">
      @if($results!=null)
      <div id="oppr-list-view" class="mCustomScrollbar" data-mcs-theme="dark">
        <table class="card-comments table table-border table-striped">
          <tr>
            <th>{{__('common.title')}}</th>
            <th>{{__('common.descp')}}</th>
            <th>{{__('common.created_by')}}</th>
            <th>{{__('common.created_at')}}</th>
          </tr>
          @foreach($results as $model)
          @php
          $createdBy = getUserInfo($model->created_by);
          $urlPre = 'opportunity';
          if($model->rec_type=='l')$urlPre = 'lead';
          $url = $urlPre.'/view/'.$model->id;
          @endphp
          <tr class="alh-item" data-key="{{$model->id}}">
            <td><a href="{{url($url)}}" target="_blank"><strong>{{$model->title}}</strong></a></td>
            <td>{{$model->descp}}</td>
            <td>{{$createdBy->fullname}}</td>
            <td>{{formatDateTime($model->created_at)}}</td>
          </tr>
          @endforeach
        </table>
    		<div class="al-pager d-none">
          {{ $results->links('vendor.pagination.custom-simple') }}
        </div>
        <div id="oppr-spinner" class="text-center">
          <div class="btn btn-secondary spinner spinner-dark spinner-right">
            <span>{{__('common.loading')}}</span>
          </div>
        </div>
      </div>
      @else
      <div class="alert aler-info">
        {{__('common.noresultfound')}}
      </div>
      @endif
    </div>
  </div>
</div>
@push('jsFunc')
function reloadOpprList(){
  var opprIL=false;

  let alhIAS = new InfiniteAjaxScroll('#oppr-list-view .card-comments', {
    scrollContainer: '#oppr-list-view',
    logger: false,
    item: '.alh-item',
    next: '#oppr-list-view a.pg-next',
    pagination: '#oppr-list-view .al-pager',
    negativeMargin: 0,
    spinner: document.getElementById('oppr-spinner'),
  });

  alhIAS.on('next', function(event) {
    opprIL=true;
  });
  alhIAS.on('appended', function(event) {
    opprIL=false;
  });

  $("#oppr-list-view").mCustomScrollbar({
    scrollbarPosition: "outside",
    autoHideScrollbar: true,
    callbacks:{
      onTotalScroll: function(){
        if(opprIL==false)alhIAS.next();
      }
    }
  });

}
@endpush
@push('jsScripts')
reloadOpprList();
@endpush

<style>
#oppr-list-view{height: 200px;}
.att-img-block{
margin: 1px 2px;
display: inline-block;
padding: 1px;
border:1px solid #ccc;
}
.alh-toolbar a.btn{padding:1px;}
</style>

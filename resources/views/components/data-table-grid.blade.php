@if(isset($statsArr) && $statsArr!=null)
<div class="d-flex align-items-center flex-wrap">
	@foreach($statsArr as $statItem)
  <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
		<span class="mr-4">
			<i class="{{$statItem['icon']}} icon-2x text-muted font-weight-bold"></i>
		</span>
		<div class="d-flex flex-column text-dark-75">
			<span class="font-weight-bolder font-size-sm">{{$statItem['title']}}</span>
			<span class="font-weight-bolder font-size-h5">
  			{{$statItem['count']}}
      </span>
      @if(isset($statItem['url']) && $statItem['url']!='')
      <a href="{{$statItem['url']}}" class="text-primary font-weight-bolder">{{__('common.view')}}</a>
      @endif
		</div>
	</div>
  @endforeach
</div>
<div class="separator separator-solid my-7"></div>
@endif
@if($filterTags!=null)
<h3>{{__('common.search_filters')}}</h3>
<div class="d-flex flex-wrap mb-5">
	@foreach($filterTags as $inputFld=>$filterTag)
	<div class="block-inline mr-5">
		@if(isset($filterTag['value']))
			{!!getFilterTagHtml($inputFld,$filterTag)!!}
		@else
			@foreach($filterTag as $idx=>$filterTagAr)
				@if(isset($filterTagAr['value']))
				{!!getFilterTagHtml('input_field%5B'.$idx.'%5D',$filterTagAr)!!}
				@endif
			@endforeach
		@endif
	</div>
	@endforeach
	<div class="separator separator-solid my-7"></div>
</div>
@endif
<div class="overlay-wrapper">
  <table class="table" id="dt-list{{$sid}}">
  </table>
</div>
<x-advance-search-modal :advSearch="$advSearch" :sid="$sid" :moduleTypeId="$moduleTypeId" :inputFields="$inputFields" :searchParams="$searchParams" :request="$request" :staffMembersList="$staffMembersList" :advSrchColArr="$advSrchColArr" />
<x-color-code-modal :colorCol="$colorCol" />


@push('cssStyles')
#dt-list_filter{display:inline-block}
#dt-list_filter .form-control {
  background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgICB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgICB2ZXJzaW9uPSIxLjEiICAgaWQ9InN2ZzQ0ODUiICAgdmlld0JveD0iMCAwIDIxLjk5OTk5OSAyMS45OTk5OTkiICAgaGVpZ2h0PSIyMiIgICB3aWR0aD0iMjIiPiAgPGRlZnMgICAgIGlkPSJkZWZzNDQ4NyIgLz4gIDxtZXRhZGF0YSAgICAgaWQ9Im1ldGFkYXRhNDQ5MCI+ICAgIDxyZGY6UkRGPiAgICAgIDxjYzpXb3JrICAgICAgICAgcmRmOmFib3V0PSIiPiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+ICAgICAgICA8ZGM6dHlwZSAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4gICAgICAgIDxkYzp0aXRsZT48L2RjOnRpdGxlPiAgICAgIDwvY2M6V29yaz4gICAgPC9yZGY6UkRGPiAgPC9tZXRhZGF0YT4gIDxnICAgICB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwLC0xMDMwLjM2MjIpIiAgICAgaWQ9ImxheWVyMSI+ICAgIDxnICAgICAgIHN0eWxlPSJvcGFjaXR5OjAuNSIgICAgICAgaWQ9ImcxNyIgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNjAuNCw4NjYuMjQxMzQpIj4gICAgICA8cGF0aCAgICAgICAgIGlkPSJwYXRoMTkiICAgICAgICAgZD0ibSAtNTAuNSwxNzkuMSBjIC0yLjcsMCAtNC45LC0yLjIgLTQuOSwtNC45IDAsLTIuNyAyLjIsLTQuOSA0LjksLTQuOSAyLjcsMCA0LjksMi4yIDQuOSw0LjkgMCwyLjcgLTIuMiw0LjkgLTQuOSw0LjkgeiBtIDAsLTguOCBjIC0yLjIsMCAtMy45LDEuNyAtMy45LDMuOSAwLDIuMiAxLjcsMy45IDMuOSwzLjkgMi4yLDAgMy45LC0xLjcgMy45LC0zLjkgMCwtMi4yIC0xLjcsLTMuOSAtMy45LC0zLjkgeiIgICAgICAgICBjbGFzcz0ic3Q0IiAvPiAgICAgIDxyZWN0ICAgICAgICAgaWQ9InJlY3QyMSIgICAgICAgICBoZWlnaHQ9IjUiICAgICAgICAgd2lkdGg9IjAuODk5OTk5OTgiICAgICAgICAgY2xhc3M9InN0NCIgICAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgwLjY5NjQsLTAuNzE3NiwwLjcxNzYsMC42OTY0LC0xNDIuMzkzOCwyMS41MDE1KSIgICAgICAgICB5PSIxNzYuNjAwMDEiICAgICAgICAgeD0iLTQ2LjIwMDAwMSIgLz4gICAgPC9nPiAgPC9nPjwvc3ZnPg==);
  background-repeat: no-repeat;
  background-color: #fff;
  background-position: 0px 5px !important;
  padding-left: 20px;
}
@endpush
@push('jsScripts')
initPageScripts{{$sid}}();
$('body').on('click', '.remove-filter', function () {
  inptFld = $(this).data("key");
	fixedQueryString = removeFromUrlVars(inptFld);
	myApp.block('#grid-table', {
	 overlayColor: '#000000',
	 state: 'danger',
	 message: "{{__('common.please_wait')}}"
	});
	window.location.href="{{url()->current()}}?"+fixedQueryString;
});
@endpush
@push('jsFunc')
var dttable = null;
function initPageScripts{{$sid}}()
{
	var headerCallback{{$sid}} =
	 function(thead, data, start, end, display) {
	        thead.getElementsByTagName('th')[0].innerHTML = `
	      <label class="checkbox checkbox-single checkbox-solid checkbox-primary mb-0">
	          <input type="checkbox" value="" class="group-checkable"/>
	          <span></span>
	      </label>`;
	      }

	var columnDefs{{$sid}}=[{
	          targets: 0,
	          orderable: false,
	          render: function(data, type, full, meta) {
	            return `
	            <label class="checkbox checkbox-single checkbox-primary mb-0">
	                <input type="checkbox" value="`+data+`" class="checkable"/>
	                <span></span>
	            </label>`;
	          },
	        },];

	var dom{{$sid}}='Bfrtip';

	$('[data-toggle="tooltip"], .tooltip').tooltip("hide");

	var  dtOptions{{$sid}} = {!!json_encode($dtOptions)!!};
	var cbSelection{{$sid}} = {{ json_encode($cbSelection) }};
	if (cbSelection{{$sid}} === true){
	dtOptions{{$sid}}.headerCallback=headerCallback{{$sid}};
	dtOptions{{$sid}}.columnDefs=columnDefs{{$sid}};
	}

if (!$.fn.dataTable.isDataTable('#dt-list{{$sid}}')){
	dttable{{$sid}} = $('#dt-list{{$sid}}').DataTable(dtOptions{{$sid}});


	@if($columnsBtn!=false)
	dttable{{$sid}}.on( 'column-visibility.dt', function ( e, settings, column, state ) {
	  var all_columns = dttable{{$sid}}.settings().init().columns;
	  var colsArr = [];
	  for (var i in all_columns) {
	    if (dttable{{$sid}}.column(all_columns[i].name + ':name').visible()) {
	      colsArr.push(all_columns[i].name);
	    }
	  }
	 data = {module_type: "{{$moduleTypeId}}", cols_arr: colsArr}
	 $.ajax({
	   url: "{{url('save-dt-cols')}}",
	   type: "post",
	   data: data,
	   dataType: "json",
	   success: function(data) {
	     console.log(data);
	   },
	   error: bbAlert
	 });
	});




	$('.col-sm-6:eq(0)').addClass('col-sm-7');
	$('.col-sm-6:eq(1)').addClass('col-sm-5');
	$('.col-sm-7:eq(0)').removeClass('col-sm-6');
	$('.col-sm-5:eq(0)').removeClass('col-sm-6');


	  dttable{{$sid}}.on('change', '.group-checkable', function() {
	      var set = $(this).closest('table').find('td:first-child .checkable');
	      var checked = $(this).is(':checked');

	      $(set).each(function() {
	        if (checked) {
	          $(this).prop('checked', true);
	          dttable{{$sid}}.rows($(this).closest('tr')).select();
	        }
	        else {
	          $(this).prop('checked', false);
	          dttable{{$sid}}.rows($(this).closest('tr')).deselect();
	        }
	      });
	    });
	@endif

	$('#dt-list{{$sid}}')
	.on('init.dt', function (){
	  $('[data-toggle="tooltip"]').tooltip();
	});
}
}
@endpush


@if($saveToList==true)

@push('jsScripts')
$("body").on("submit", "form.stl-ajax-submit", function (e) {
  console.log("Here?");
  e.preventDefault();
  frm = $(this);
  frmId = frm.attr("id");
  pjxContainer = 'grid-table';
  blockContainer = frmId;

  if(blockContainer!=''){
    myApp.block('#'+blockContainer, {
      overlayColor: '#000000',
      state: 'danger',
      message: "{{__('common.please_wait')}}"
    });
  }

  var rowIds = $.map(dttable.rows('.selected').data(), function (item) {
		  return item.id
  });
  $("#sel-module-type").val("{{$moduleTypeId}}");
  $("#sel-module-ids").val(rowIds.join(","));
	
  frm.find('input[type=submit]').attr("disabled",true);

  var formData = new FormData(frm[0]);
  $.ajax({
    url: frm.attr("action"),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    success: function (response) {
      if(blockContainer!='')myApp.unblock('#'+blockContainer);
      frm.find('input[type=submit]').attr("disabled",false);
      if(response['success']){
        $("#general-modal").modal("hide");
        toastr.success(response["success"]["msg"], response["success"]["heading"]);
      }else{
        if(response['errors']){
          response['errors'].each(function(key,val) {
            toastr.success(val, "{{__('common.error')}}");
          });
        }else{
          if(response['error']){
            toastr.error(response['error']['msg'], response['error']['heading']);
          }else{
            $("#general-modal").find(".modalContent").html(response);
          }
        }
      }
    },
    error: function(xhr, ajaxOptions, thrownError){
      var obj = xhr.responseJSON.errors;
      $.each(obj, function(key,value) {
        toastr.error(value, "{{__('common.error')}}");
      });
    }
  });
  return false;
});
@endpush
@endif

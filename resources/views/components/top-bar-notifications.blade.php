<div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
  <div class="btn btn-icon btn-clean btn-dropdown btn-lg mr-1 pulse pulse-primary" id="kt_quick_panel_toggle">
    <span class="svg-icon svg-icon-xl svg-icon-primary">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <path d="M17,12 L18.5,12 C19.3284271,12 20,12.6715729 20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 C4,12.6715729 4.67157288,12 5.5,12 L7,12 L7.5582739,6.97553494 C7.80974924,4.71225688 9.72279394,3 12,3 C14.2772061,3 16.1902508,4.71225688 16.4417261,6.97553494 L17,12 Z" fill="#000000"></path>
          <rect fill="#000000" opacity="0.3" x="10" y="16" width="4" height="4" rx="2"></rect>
        </g>
      </svg>
    </span>
    @if($unreadCount>0)
    <span class="pulse-ring"></span>
    @endif
  </div>
</div>

<div id="kt_quick_panel" class="offcanvas offcanvas-right pt-5 pb-10">
  <div class="offcanvas-header offcanvas-header-navs d-flex align-items-center justify-content-between mb-5">
    <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary flex-grow-1 px-10" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_notifications">{{__('common.notifications')}}</a>
			</li>
		</ul>
    <div class="offcanvas-close mt-n1 pr-5">
      <a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_panel_close">
        <i class="ki ki-close icon-xs text-muted"></i>
      </a>
    </div>
  </div>
  <div class="offcanvas-content px-10">
    <div class="tab-content">
      <div class="tab-pane fade pt-2 pr-5 mr-n5 scroll ps active show ps--active-y" id="kt_quick_panel_notifications" role="tabpanel">
        <div class="navi navi-icon-circle navi-spacer-x-0">
          @if($results!=null)
          @foreach($results as $result)
          <a href="{{url($result->url)}}" class="navi-item">
            <div class="navi-link rounded">
              <div class="symbol symbol-50 mr-3">
                <div class="symbol-label">
                  <i class="flaticon-{{$result->icon}} text-{{$result->cls_name}} icon-lg"></i>
                </div>
              </div>
              <div class="navi-text">
                <div class="font-weight-bold font-size-lg">{{$result->title}}</div>
                <div class="text-muted">{{__('common.created_at_with_dt',['datetime'=>formatDate($result->created_at)])}}</div>
              </div>
            </div>
          </a>
          @endforeach
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

<div class="card card-custom gutter-b mb-3">
  @if($heading!='')
  <div class="card-header">
		<div class="card-title">
			<h3 class="card-label">{{$heading}}</h3>
		</div>
	</div>
  @endif
  <div class="card-body">
    <div id="{{$id}}" style="height: 300px;"></div>
  </div>
</div>

@push('jsScripts')
var data{{$id}} = {!!$data!!};

var flotChartOptions{{$id}} = {!!$flotChartOptions!!}
@if($type=='pie' && $label==true)
flotChartOptions{{$id}}.series.pie.label.formatter = function(label, series){
  return '<div style=\"font-size:8pt;text-align:center;padding:2px;color:white;\">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
}
@endif
$.plot($("#{{$id}}"), data{{$id}}, flotChartOptions{{$id}});
@endpush

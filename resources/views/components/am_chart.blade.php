<div class="card card-custom card-border gutter-b mb-3">
  @if($heading!='')
  <div class="card-header">
		<div class="card-title">
			<h3 class="card-label">{{$heading}}</h3>
		</div>
	</div>
  @endif
  <div class="card-body">
    <div id="{{$id}}" style="height: 500px;"></div>
  </div>
</div>

@push('jsScripts')
var chart{{$id}} = AmCharts.makeChart("{{$id}}", {!!$chartOptions!!});
@endpush

@if($advSearch==true)

@push('jsFunc')
function saveAdvSearch{{$sid}}(trigSrch)
{
  _targetContainer = "advanceSearchModal{{$sid}}";
  frm = $("#adv-search-form{{$sid}}");

  myApp.block('#'+_targetContainer, {
   overlayColor: '#000000',
   state: 'danger',
   message: "{{__('common.please_wait')}}"
  });

  var data = frm.serialize();
  $.ajax({
    url: "{{url('advance-search/save/'.$moduleTypeId)}}",
    type: 'POST',
    data: data,
    success: function (response) {
      if(response['success']){
        toastr.success(response["success"]["msg"], response["success"]["heading"]);
        if(trigSrch==1){
          frm.submit();
        }
      }else{
        swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error"});
      }
      myApp.unblock('#'+_targetContainer);
    },
    error: bbAlert
  });
}
@endpush
@push('jsScripts')
$('body').on('click', '.advance-search-popup', function () {
  $('#advanceSearchModal').modal();
});
$("body").on("click",".save-search",function(e){
  $("#save-search-title{{$sid}}").removeClass("d-none");
  $(this).addClass("d-none");
});
$("body").on("click",".btn-ss-cancel",function(e){
  $("#save-search-title{{$sid}}").addClass("d-none");
  $(".save-search").removeClass("d-none");
});
$("body").on("click",".savets",function(e){
  saveAdvSearch{{$sid}}(0);
});
$("body").on("click",".savetsns",function(e){
  saveAdvSearch{{$sid}}(1);
});
@endpush
<div class="modal fade" id="advanceSearchModal{{$sid}}" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
  <div class="modal-dialog modal-dialog-cent1ered modal-lg" role="document">
    <div class="modal-content">
      {!! Form::open(['method'=>'get','id'=>'adv-search-form'.$sid]) !!}
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('common.advance_search')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i aria-hidden="true" class="ki ki-close"></i>
        </button>
      </div>
      <div class="modal-body">
        @if($inputFields!=null)
        <div class="row">
          @php
          $created_atVal = isset($searchParams['created_at']) ? $searchParams['created_at'] : '';
          @endphp
          <div class="col-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('created_at',__('common.created_at')) }}
              <div class="input-group cadtrpicker">
                {{ Form::text('created_at', $created_atVal, ['class'=>'form-control', 'readonly'=>'readonly']) }}
                <div class="input-group-append">
									<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
								</div>
              </div>
            </div>
          </div>
					@push('jsScripts')
					initDateRangePicker(".cadtrpicker");
					@endpush
          @php
					$createdByValue=[];
          $createdByValue = isset($searchParams['created_by']) ? $searchParams['created_by'] : '';
          @endphp
          <div class="col-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('created_by',__('common.created_by')) }}
              {{Form::select('created_by[]',$staffMembersList,$createdByValue,['class'=>'form-control selectpicker','data-selected-text-format'=>'count>3','multiple'=>'multiple'])}}
            </div>
          </div>
          @php
					$assignedToValue=[];
          $assignedToValue = isset($searchParams['assign_to']) ? $searchParams['assign_to'] : '';
          @endphp
          <div class="col-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('assign_to',__('common.assign_to')) }}
              {{Form::select('assign_to[]',$staffMembersList,$assignedToValue,['class'=>'form-control selectpicker','data-selected-text-format'=>'count>3','multiple'=>'multiple'])}}
            </div>
          </div>
          @php
          $selectedValue = isset($searchParams['reference_no']) ? $searchParams['reference_no'] : '';
          @endphp
          <div class="col-12 col-sm-6">
            <div class="form-group">
              {{ Form::label('reference_no',__('common.reference_no')) }}
              {{ Form::text('reference_no', $selectedValue, ['class'=>'form-control']) }}
            </div>
          </div>
          @if($advSrchColArr!=null)
          @foreach($advSrchColArr as $sk=>$sinputField)
          @php
          $selectedValue = isset($searchParams[$sinputField['name']]) ? $searchParams[$sinputField['name']] : '';
          $inputClasses = 'form-control';
          if(isset($sinputField['input_type'])){
            if($sinputField['input_type']=='date')$inputClasses.=' dtpicker';
            if($sinputField['input_type']=='number')$inputClasses.=' numeric';
          }
          @endphp
          <div class="col-12 col-sm-6">
            <div class="form-group">
              {{ Form::label($sinputField['name'],$sinputField['title']) }}
              @if(isset($sinputField['input_type']) && $sinputField['input_type']=='select')
              @php
              $selectOptsArr = $sinputField['subopts'];
              @endphp
              {{Form::select($sinputField['name'],[''=>__('common.select')] + $selectOptsArr,$selectedValue,['class'=>$inputClasses])}}
              @else
              {{ Form::text($sinputField['name'], $selectedValue, ['class'=>$inputClasses,'autocomplete'=>'off']) }}
              @endif
            </div>
          </div>
          @endforeach
          @endif
          @foreach($inputFields as $inputField)
            @php
            $selectedValue = isset($searchParams['input_field'][$inputField['id']]) ? $searchParams['input_field'][$inputField['id']] : '';
            @endphp
            <div class="col-12 col-sm-6">
            	<div class="form-group">
                <label>{{$inputField->title}}</label>
                @if(
                  $inputField['input_type']=='textarea' ||
                  $inputField['input_type']=='tinymce' ||
                  $inputField['input_type']=='text' ||
                  $inputField['input_type']=='qtyField' ||
                  $inputField['input_type']=='phoneinput' ||
                  $inputField['input_type']=='faxinput' ||
                  $inputField['input_type']=='numberinput' ||
                  $inputField['input_type']=='websiteinput' ||
                  $inputField['input_type']=='emailinput' ||
                  $inputField['input_type']=='date' ||
                  $inputField['input_type']=='autocomplete'
                )
                  @php
                    $clsNames='form-control';
                    $inputOpts['id']='srchtxt-'.$inputField['id'];
                    $inputOpts['maxlength']=true;
                    $inputOpts['placeholder']=$inputField->hint_text;
                    $inputOpts['autocomplete'] = 'off';
                    if($inputField['input_type']=='qtyField' || $inputField['input_type']=='numberinput')$clsNames.=' numeric-field';

                    $filterOptions = getCustomFieldFilterOptions($inputField['id']);
                    if($filterOptions!=null){
                      if($filterOptions['input_type']=='date'){
                        $clsNames.=' dtpicker';
                      }
                    }

                    $inputOpts['class']=$clsNames;
                  @endphp
                  @if($inputField['input_type']=='date')
									@if($filterOptions['input_type']=='daterange')
									@push('jsScripts')
									initDateRangePicker("#input-field{{$inputOpts['id']}}");
									@endpush
									@endif
                  <div id="input-field{{$inputOpts['id']}}" class="input-group{{$filterOptions['input_type']!=null && $filterOptions['input_type']=='daterange' ? ' dtrpicker' : ''}}">
                    {{Form::text('input_field['.$inputField->id.']',$selectedValue,$inputOpts)}}
                    <div class="input-group-append">
    									<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
    								</div>
                  </div>

                  @else
                    @if($inputField['input_type']=='numberinput')
                      @php
                      $rangeFrom = $filterOptions['range_from'];
                      $rangeTo = $filterOptions['range_to'];
                      if($selectedValue!=""){
                        list($rangeFrom,$rangeTo)=explode(";",$selectedValue);
                      }
                      @endphp
                      @if($filterOptions['input_type']!=null && $filterOptions['input_type']=='inputrange')
                      @push('jsScripts')
                      var incomeRageSlider=$("#srchtxt-{{$inputField['id']}}");
                      incomeRageSlider.ionRangeSlider({
                      	type: "double",
                      	skin: "round",
                      	min: {{$filterOptions['range_from']}},
                      	max: {{$filterOptions['range_to']}},
                      	step: {{$filterOptions['range_interval']}},
                      	prefix: "",
                      	prettify_separator: ",",
                      	from: {{$rangeFrom}},
                      	to: {{$rangeTo}},
                      });
                      @endpush
                      @endif
                    @endif
                    {{Form::text('input_field['.$inputField->id.']',$selectedValue,$inputOpts)}}
                  @endif

                @elseif(
                  $inputField['input_type']=='select' ||
                  $inputField['input_type']=='checkbox' ||
                  $inputField['input_type']=='radio'
                )
                  @php
										$selectOptions=[];
									  $selectArr=getInputListArr($inputField);
										$filterOptions = getCustomFieldFilterOptions($inputField['id']);
										$clsNames='form-control';

										if($filterOptions['input_type']=='selectpicker'){
											$clsNames.=' selectpicker';
											$selectOptions['multiple']='multiple';
											$selectOptions['data-selected-text-format']='count>3';
											$fldName = 'input_field['.$inputField->id.'][]';
										}else{
											$selectArr = [''=>__('common.select')] + $selectArr;
											$fldName = 'input_field['.$inputField->id.']';
										}

                    $selectOptions['class']=$clsNames;

                  @endphp
                  {{Form::select($fldName,$selectArr,$selectedValue,$selectOptions)}}
                @endif
              </div>
            </div>
          @endforeach
        </div>
        @endif
        <div id="save-search-title{{$sid}}" class="d-none card card-custom card-fit card-border">
					<div class="card-header py-0 p-2" style="min-height:20px;">
						<div class="card-title m-0">
							<h3 class="card-label">
                {{__('common.save_search_as')}}
              </h3>
						</div>
						<div class="card-toolbar m-0">
              <a href="javascript:;" class="btn btn-hover-light-primary btn-sm btn-icon btn-ss-cancel" data-toggle="tooltip" data-title="{{__('common.cancel')}}">
  							<i class="fa fa-times"></i>
  						</a>
						</div>
					</div>
					<div class="card-body p-3">
            <div class="form-group">
  						<div class="input-group">
  							<input name="search_title" type="text" class="form-control" placeholder="{{__('common.save_search_as')}}">
  							<div class="input-group-append">
  								<a class="btn btn-success savets" type="button" data-toggle="tooltip" data-title="{{__('common.save')}}"><i class="fa fa-save"></i></a>
  								<a class="btn btn-info savetsns" type="button" data-toggle="tooltip" data-title="{{__('common.save_and_search')}}"><i class="fa fa-search"></i></a>
  							</div>
  						</div>
  					</div>
          </div>
				</div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary font-weight-bold">{{__('common.search')}}</button>
          <a href="javascript:;" class="btn btn-light-success font-weight-bold save-search">{{__('common.save_search')}}</a>
          <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{__('common.cancel')}}</button>
        </div>
        <center><h3>OR</h3></center>
        <center><a href="javascript:;" class="btn btn-success load-modal" data-url="{{url('/saved-search/'.$moduleTypeId)}}" data-heading="{{__('app.saved_search.use_saved_search')}}">{{__('app.saved_search.use_saved_search')}}</a></center>

      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endif

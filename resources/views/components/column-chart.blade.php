
        <div class="card card-custom gutter-b">



<div class="d-flex justify-content-between">

<div class="">
  <?php if ($text=='events') { ?>
    <h3 class="card-label pt-7 pl-5">Type of Events ( Since 2014 )</h3>
  <?php } else if ($text=='Payments') { ?>
    <h3 class="card-label pt-7 change_since pl-5">Subscription Revenue Since(2021 to 2022 )</h3>
  <?php } else if ($text=='EventsPayments') { ?>
    <h3 class="card-label pt-7 change_since_event pl-5">Events Revenue Since(2021 to 2022 )</h3>
<?php  }else{ ?>

  <h3 class="card-label pt-7 pl-5"><?php echo str_replace( '_', ' ', $text); ?></h3>
<?php } ?>
</div>



            <?php    if($text=='Payments'){ ?>
                <div class="dropdown pt-7 pr-7" >
                    <a href="#" class="btn btn-light btn-sm font-size-sm font-weight-bolder dropdown-toggle text-dark-75" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SelectYear</a>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                      <ul class="navi navi-hover">
                        <?php foreach (getPaymentYear() as $key => $year) { ?>
                        <li class="navi-item">
                            <a id="{{ $year }}" class="navi-link todo-item<?= $text ?>">{{$year }}
                            </a>
                          </li>
                        <?php } ?>
                      <ul>
                        @push("js")
                        <script type="text/javascript">
                            $("body").on("click", ".todo-item<?= $text ?>", function (e) {
                                var i= $(this).attr('id');
                                var j= parseInt(i)+1;

                                $('.change_since').html('Subscription Revenue Since('+i+' to '+j+' )');
                                var url<?= $text ?>="/paymentyear/"+i;
                                $.ajax({
                                    url:"<?= url('paymentyear/')?>"+"/"+i ,
                                    type: "POST",
                                    dataType: "json",
                                    success: function(response) {
                                        console.log(response['payments']);
                                        chart<?= $text ?>.updateSeries(response['payments']);
                                        // _demo3<?= $text ?>(response);
                                }
                            });
                        });
                        </script>
                        @endpush

                    </div>
                  </div>
                <?php }if ($text=='EventsPayments'){?>

                  <div class="dropdown pt-7 pr-7" >
                      <a href="#" class="btn btn-light btn-sm font-size-sm font-weight-bolder dropdown-toggle text-dark-75" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SelectYear</a>
                      <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        <ul class="navi navi-hover">
                          <?php foreach (getPaymentYear() as $key => $year) { ?>
                          <li class="navi-item">
                              <a id="{{ $year }}" class="navi-link todo-item<?= $text ?>">{{$year }}
                              </a>
                            </li>
                          <?php } ?>
                        <ul>
                          @push("js")
                          <script type="text/javascript">
                              $("body").on("click", ".todo-item<?= $text ?>", function (e) {
                                  var i= $(this).attr('id');
                                  var j= parseInt(i)+1;

                                  $('.change_since_event').html('Event Revenue Since('+i+' to '+j+' )');
                                  var url<?= $text ?>="/paymentyear/"+i;
                                  $.ajax({
                                      url:"<?= url('paymentyear/event')?>"+"/"+i ,
                                      type: "POST",
                                      dataType: "json",
                                      success: function(response) {
                                          console.log(response['payments']);
                                          chart<?= $text ?>.updateSeries(response['payments']);
                                          // _demo3<?= $text ?>(response);
                                  }
                              });
                          });
                          </script>
                          @endpush

                      </div>
                    </div>
              <?php  } ?>
</div>




        	<div class="card-body" style="min-height: 250px;">
            	<div id="{{$text}}"></div>
        	</div>
    	</div>
@push("js")
<script type="text/javascript">
var chart<?= $text ?>;
var h<?= $text ?>={!!json_encode($series)!!};
var primary = '#6993FF';
var success = '#1BC5BD';
var info = '#8950FC';
var warning = '#FFA800';
var danger = '#F64E60';
var color_arrays<?= $text ?>=["<?= $color ?>",'#F93C22', warning];
var _demo3<?= $text ?> = function (seriesdata) {
    const apexChart<?= $text ?> = "#<?= $text ?>";
        var options<?= $text ?> = {
            series: seriesdata,
            chart: {
                type: 'bar',
                height: 290
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: "<?= $width ?>",
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: <?php echo json_encode($xaxis); ?>,
            },
            yaxis: {
                title: {
                    text: "<?= $textxaxis ?>"
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return  val
                    }
                }
            },
            colors: color_arrays<?= $text ?>
        };
        chart<?= $text ?> = new ApexCharts(document.querySelector(apexChart<?= $text ?>), options<?= $text ?>);
        chart<?= $text ?>.render();
}
jQuery(document).ready(function () {
    _demo3<?= $text ?>(h<?= $text ?>);
});
</script>

@endpush

@if($colorCol==true)
<div class="modal fade" id="colorCodeModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
  <div class="modal-dialog modal-dialog-cent1ered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{__('common.color_legends')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i aria-hidden="true" class="ki ki-close"></i>
        </button>
      </div>
      <div class="modal-body">
        @foreach(getColorLegends() as $key=>$colorItem)
        <table class="table">
          <tr>
            <td width="100" align="left"><span class="label label-dot label-xl" style="background:{{$colorItem['color_code']}};" data-toggle="tooltip" data-original-title="{{$colorItem['title']}}" data-placement="top"></span></td>
            <td align="left">{{$colorItem['title']}}</td>
          </tr>
        </table>
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{__('common.cancel')}}</button>
      </div>
    </div>
  </div>
</div>
@push('jsScripts')
$('body').on('click', '.colorcodes-popup', function () {
  $('#colorCodeModal').modal();
});
@endpush
@endif

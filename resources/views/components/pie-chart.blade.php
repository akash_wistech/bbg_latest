
	<div class="card card-custom gutter-b">
		<div class="card-body p-0">
			<p class="card-label pl-6 pt-6" style="font-size:18px">{{$id}} Types</p>
			<div id="{{$id}}" class="d-flex justify-content-center">
			</div>
		</div>
	</div>


<?php //dd($series); ?>




@push("js")
<script type="text/javascript">

var primary = '#6993FF';
var success = '#1BC5BD';
var info = '#8950FC';
var warning = '#FFA800';
var danger = '#F64E60';



jQuery(document).ready(function () {
const apexChart<?= $id ?> = "#<?= $id ?>";
		var options<?= $id ?> = {
			series: <?php echo json_encode($series); ?>,
			chart: {
				width: 430,
				height: 337,
				type: 'donut',
			},
			labels:<?php echo json_encode($labels); ?>,
			responsive: [{
				breakpoint: 480,
				options: {
					chart: {
						width: 300
					},
					legend: {
						position: 'bottom'
					}
				}
			}],
			colors: [primary ,success, warning,danger],
			dataLabels: {
		enabled: false,
	},
	plotOptions: {
    pie: {
       customScale: 0.9,
      donut: {
        labels: {
          show: true,
        }
      },
      dataLabels: {
     position: 'top'
   }
    }
  },

	legend: {
					show: true,
					width:400,
					position: 'bottom',
					fontSize: '10px',
					offsetX: 0,
      offsetY: 0,
					formatter: function(seriesName, opts) {
					 return [ "<span class=''>" ,seriesName, "-<b style='font-size:10px'>", opts.w.globals.series[opts.seriesIndex], "</b></span>"]
			 },
			 markers: {
					 width: 10,
					 height: 10,

				 },
				 itemMargin: {
						 horizontal: 15,
						 vertical: 0
				 },
				}

		};

		var chart<?= $id ?> = new ApexCharts(document.querySelector(apexChart<?= $id ?>), options<?= $id ?>);
		chart<?= $id ?>.render();
	});
</script>
@endpush

<?php
if ($model<>null) {
	$companies = getRowMultipleCompaniesQuery('contact',$model->id);
	$companyId = $model->main_company_id;
	?>
	<section class="bannerBG usertopdetail">
		<div class="container">
			<div class="row">
				<?php
				if ($companies<>null && count($companies)>0) {
					foreach ($companies as $company) {

						$nonTextAreaColumnsForCompany=getNonTextAreaColumns($company->moduleTypeId);
						?>
						<div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 Tcenter">
							<img src="<?= (!empty($model->image)) ? $model->image : $no_image; ?>"
							class="hidden-md-down img-fluid PaddingTopBtm30px" alt="">
						</div>
						<div class="hidden-md-down col-6 col-sm-6 col-md-8 col-lg-8 col-xl-8 Tcenter">
							<div style="position: unset !important; padding-top: 25px;" class="BannerText">
								<h1><?= $model->name; ?></h1>
								<p class="text-white">
									@if($nonTextAreaColumnsForCompany->isNotEmpty())
									@foreach($nonTextAreaColumnsForCompany as $nonTextAreaColumn)
									@php
									$value=getInputFielSavedValueForDetail($company,$nonTextAreaColumn);
									if(is_array($value))$value = implode(", ",$value);
									@endphp
									<strong>{{$nonTextAreaColumn['title']}}</strong> - {{ ($value!=null)? $value : 'N\A' }} <br>
									@endforeach
									@endif
								</p>
							</div>
						</div>
						<?php
					}
				}
				?>


				<div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 Tcenter">
					<div class="editprofilebtn">
						<a href="{{url('/membership/change-password')}}" class="d-none"><i class="fa fa-lock mx-2" aria-hidden="true"></i>Change Password</a><br/>
						<a href="{{url('/membershipFend/update',['id'=>$model->id])}}"><i class="fa fa-user mx-2" aria-hidden="true"></i>Edit Profile</a><br />
						<a href="javascript:;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out mx-2" aria-hidden="true"></i>Logout</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</div>
				</div>
				<div class="col-6  hidden-lg-up">
					<div id="mySidenav1" class="sidenav">
						<a href="javascript:void(0)" class="closebtn" onclick="closeNav1()">&times;</a>

						<a href="">Renew Membership</a>
						<a href="" class="d-none">My Email Subscriptions</a>
						<a href="">Company Members</a>
						<a href="">Upgrade Membership</a>
						<a href="">Payment History</a>
						<a href="">My Contacts</a>
						<a href="">Events Registered</a>
						<a href="" class="jobsPortalBtn pull-left">Jobs Portal</a>

					</div>
					<div class="text-right">
						<span style="color:#fff;font-size:30px;cursor:pointer;" onclick="openNav1()">&#9776;</span>
					</div>

				</div>
			</div>
			<div class="row whiteline text-right">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div class="hidden-md-down topnav1" id="myTopnav">

						<div class="d-flex" style="justify-content: space-between;">
							<div class="bg-dark">
								<a href="{{url('jobPosts')}}" class="jobsPortalBtn">Jobs Portal</a>
							</div>

							<div>
								<!-- <a href="">Renew Membership</a> -->
								<a href="{{url('/member/edit-email-subscriptions')}}" class="d-none">My Email Subscriptions</a>
								<a href="{{url('/company/members?id='.$companyId)}}">Company Members</a>
								<!-- <a href="">Upgrade Membership</a> -->

								<a href="{{url('/account/member-contacts')}}">My Contacts</a>
								<a href="{{url('member/statements')}}">Statements</a>
								<a href="{{url('member/payments')}}">Payments</a>
								<a href="{{url('member/invoices')}}">Invoices</a>
								<a href="{{url('events/registrations')}}">Events Registered</a>
								<a href="{{url('GetMyCompanyNews')}}">News</a>
								<a href="{{url('member/dashboard')}}">Dashboard</a>
							</div>
						</div>



					</div>

				</div>
			</div>
		</section>
		<?php
	}
	?>


	@push('css')
	<style>
		.topnav1 a {
			padding: 14px 10px;
		}
	</style>
	@endpush


	@push('js')
	<script>
		function openNav1() {
			document.getElementById("mySidenav1").style.width = "100%";
			document.getElementById("main").style.marginLeft = "100%";
		}

		function closeNav1() {
			document.getElementById("mySidenav1").style.width = "0";
			document.getElementById("main").style.marginLeft = "0";
		}
	</script>
	@endpush

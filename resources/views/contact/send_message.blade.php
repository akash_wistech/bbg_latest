
{!! Form::model($model, ['files' => true]) !!}
<div class="row">
  <div class="col">
    <div class="form-group d-none">
      {{ Form::label('name',__('common.name')) }}
      {{ Form::text('name', null, ['class'=>'form-control form-control-solid','id'=>'nameforMessage']) }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col">
    <div class="form-group">
      {{ Form::label('email',__('common.email')) }}
      {{ Form::text('email', null, ['class'=>'form-control form-control-solid','id'=>'emailforMessage']) }}
    </div>
  </div>
</div>

<div class="row">
  <div class="col">
    <div class="form-group">
      {{ Form::label('subject',__('Subject'),['class'=>'required']) }}
      {{ Form::text('subject', null, ['class'=>'form-control','id'=>'subject']) }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col">
    <div class="form-group">
      {{ Form::label('message',__('Message'),['class'=>'required']) }}
      {{ Form::textarea('message', null, ['class'=>'form-control','id'=>'message']) }}
    </div>
  </div>
</div>
<button type="button" class="btn btn-default btn-danger" id="send_message">Send</button>
<button type="button" class="btn btn-default btn-danger" data-dismiss="modal" id="closeModalBox">Close</button>
{!! Form::close() !!}

<script>

$("body").on("click", "#send_message", function (e) {

var name,email,subject,message;

name = $('#nameforMessage').val();
email = $('#emailforMessage').val();
subject = $('#subject').val();
message = $('#message').val();

if (subject) {

if (message) {
KTApp.block('#general-modal .modal-dialog', {});
  $.ajax({
      url: "/contact/sendMessage",
      type: "POST",
      data: {name,email,subject,message},
      success: function (res) {
         KTApp.unblock('#general-modal .modal-dialog');
         $('#general-modal').modal('toggle');
           Swal.fire("Done!", "Email has sent successfully.", "success");
      },

  });
}else {
  Swal.fire("Error!", "Message must be filled.", "error");
}

}else {
  Swal.fire("Error!", "Subject must be filled.", "error");
}

});

</script>

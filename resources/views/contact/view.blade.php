@extends('layouts.app')
@section('title', __('app.member.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'membership'=> __('app.member.heading'),
  __('common.create')
]])
@endsection
@section('content')

<style>

.info-gray-box{
background-color:#f8f8f8;
min-height:280px;
border-radius:10px;
}
.text-gray-for-box{
color:#8e8c8c;
}
</style>




	<div class="container border rounded mt-4 py-5 px-0">
		<div class="d-flex px-0">
		<div class="col-3 border-right rounded bg-white py-4 px-6">
			<!--begin::Details-->
			<div class="d-flex mt-6">
				<!--begin: Pic-->
				<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
					<div class="symbol symbol-50 symbol-lg-90">
						<img src="{{ ($model->image<>null) ? $model->image : asset('images/default-placeholder.png') }}" alt="image" />
					</div>
					<div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
						<span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
					</div>
				</div>
				<!--end::Pic-->
				<!--begin::Info-->
				<div class="flex-grow-1">
					<!--begin::Title-->
					<div class="d-flex justify-content-between flex-wrap mt-1">
						<div class="d-flex mr-3">
							<p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->name }}</p>
							<?php if($model->status==1){  ?>
							<a>
								<i class="flaticon2-correct text-success font-size-h5"></i>
							</a>
							<?php } ?>
						</div>
					</div>
					<!--end::Title-->
					<!--begin::Content-->
					<div class="d-flex flex-wrap justify-content-between mt-1">
						<div class="d-flex flex-column flex-grow-1 pr-8">
							<div class="d-flex flex-wrap ">

								<?php if (isset($membersCustomFields['Designation']) && $membersCustomFields['Designation']<>null ) { ?>
								<p class="text-dark-50 text-hover-primary font-weight-bold">
								<i class="flaticon2-calendar-3 mr-1"></i>{{ $membersCustomFields['Designation'] }}</p>
								<?php } ?>

							</div>
						</div>
					</div>
					<!--end::Content-->
				</div>
				<!--end::Info-->
			</div>
			<!--end::Details-->


			<div class="pb-7 mt-4">
			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Account Type:</span>
				<a  class="text-gray-for-box text-hover-primary">{{ $account_type_id }}</a>
			</div>

      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Reference No:</span>
				<span class="text-gray-for-box">{{ ($model->reference_no!=null)? $model->reference_no : 'N/A';  }}</span>
			</div>

			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Phone:</span>
				<span class="text-gray-for-box">{{ $model->phone }}</span>
			</div>
      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Email:</span>
				<span class="text-gray-for-box">{{ $model->email }}</span>
			</div>
			</div>


			<ul class="nav nav-tabs  mb-5  d-flex flex-column navi navi-hover navi-active navi-accent border-bottom-0">
			    <li class="navi-item my-1">
			        <a class="navi-link active" data-toggle="tab" href="#user_personal_information">
			            <span class="navi-icon pr-2">
			            	<span class="svg-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Code\Info-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24"/>
                          <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                          <rect fill="#000000" x="11" y="10" width="2" height="7" rx="1"/>
                          <rect fill="#000000" x="11" y="7" width="2" height="2" rx="1"/>
                      </g>
                    </svg><!--end::Svg Icon-->
                  </span>
			           </span>
			          <span class="navi-text font-size-h6 ">Personal Information</span>
			        </a>
			    </li>
			     <li class="navi-item my-1">
			        <a class="navi-link" data-toggle="tab" href="#company_tab">
			            <span class="navi-icon pr-4"><i class="far fa-building"></i></span>
			            <span class="navi-text font-size-h6 ">Company</span>
			        </a>
			    </li>
			    <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#invoices_tab">
			            <span class="navi-icon pr-4"><i class="fas fa-file-invoice-dollar"></i></span>
			            <span class="navi-text font-size-h6">Invoices</span>
			        </a>
			    </li>
			     <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#payment_tab">
			            <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
			            <span class="navi-text font-size-h6">Payment</span>
			        </a>
			    </li>
          <li class="navi-item my-1">
             <a class="navi-link mx-0" data-toggle="tab" href="#tab-actions">
                 <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
                 <span class="navi-text font-size-h6">Actions</span>
             </a>
         </li>
         <li class="navi-item my-1">
             <a class="navi-link mx-0" data-toggle="tab" href="#tab-attachments">
                 <span class="navi-icon pr-4"><i class="fas fa-file-invoice-dollar"></i></span>
                 <span class="navi-text font-size-h6">{{__('common.attachments')}}</span>
             </a>
         </li>
			</ul>
			</div>


	<div class="tab-content col-9 rounded pr-0" id="myTabContent">
      <!-- User Personal Information First Tab 1 -->
			<div class="tab-pane fade show active bg-white rounded" id="user_personal_information" role="tabpanel" aria-labelledby="user_personal_information">
					<div class="card card-custom">
					 <div class="card-header">
					  <div class="card-title">
	            <span class="card-icon">
	               <i class="far fa-user-circle icon-xl text-primary"></i>
	            </span>
					   <h3 class="card-label">Personal Information</h3>
					  </div>
            <!-- User Edit Button  -->
					   <div class="card-toolbar">
					    <a href="{{ url('contact/update/'.$model->id) }}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="" data-original-title="Update">
							<i class="text-dark-50 flaticon-edit"></i>
						  </a>
					   </div>
					 </div>
					 <div class="card-body">
            <!-- company view gary -->
            <div class="p-7 info-gray-box">
            <div class="row mb-2">
              <!--begin::Info-->
              <div class="col-6 col-md-3">
                <div class="mb-8 d-flex flex-column">
                  <span class="text-dark font-weight-bolder mb-2">Reference No</span>
                  <span class="text-gray-for-box font-size-lg" style="">{{ $model->reference_no }}</span>
                </div>
              </div>
              <div class="col-6 col-md-3">
                <div class="mb-8 d-flex flex-column">
                  <span class="text-dark font-weight-bolder mb-2">Name</span>
                  <span class="text-gray-for-box  font-size-lg">{{ $model->user_name }}</span>
                </div>
              </div>
              <div class="col-6 col-md-3">
                <div class="mb-8 d-flex flex-column">
                  <span class="text-dark font-weight-bolder mb-2">Email</span>
                  <span class="text-gray-for-box  font-size-lg">{{ ($model->email!=null) ? $model->email : 'N\A' }}</span>
                </div>
              </div>
              <div class="col-6 col-md-3">
                <div class="mb-8 d-flex flex-column">
                  <span class="text-dark font-weight-bolder mb-2">Phone</span>
                  <span class="text-gray-for-box  font-size-lg">{{($model->phone!=null)? $model->phone:"N\A" }}</span>
                </div>
              </div>
              @if($nonTextAreaColumns->isNotEmpty())
              @foreach($nonTextAreaColumns as $nonTextAreaColumn)
              @php
              $value=getInputFielSavedValueForDetail($model,$nonTextAreaColumn);
              if(is_array($value))$value = implode(", ",$value);
              @endphp
                  <div class="col-6 col-md-3 ">
                    <div class="mb-8 d-flex flex-column">
                      <span class="text-dark font-weight-bolder mb-2">{{$nonTextAreaColumn['title']}}</span>
                      <span class="text-gray-for-box font-size-lg" style="word-break: break-all;">{{ ($value!=null)? $value : 'N\A' }}</span>
                    </div>
                  </div>
              @endforeach
             @endif
        	   <!--end::Info-->
						</div>
						</div>
          <!--End company view gary -->
          <!-- Tell Us About Yourself and Purpose of joining of a user -->
            <div class="row px-0">
              <div class="col-6">
                @if($textAreaColumns->isNotEmpty())
                @foreach($textAreaColumns as $textAreaColumn)
                @php
                $value=getInputFielSavedValueForDetail($model,$textAreaColumn);
                if(is_array($value))$value = implode(", ",$value);
                @endphp
                <div class="col px-0 mt-10">
                  <h5 class="mb-4">{{ $textAreaColumn['title'] }}</h5>
                  <span style="text-align: justify; text-justify:auto;">{{ ($value!=null)? $value : 'N\A' }}</span>
                </div>
              @endforeach
             @endif
            </div>
              <!-- Documents of User  -->
              <div class="col-6 pl-20 text-center">
                <h3 class="my-10 text-left">Documents</h3>
              <div class="d-flex align-items-center text-align-center">
                <div class="mr-4">
                  <!--begin::Pic-->
                  <div class="text-center">
                    <img src="{{ $passportCopyImg }}" class="rounded mt-3" style="width:150px" alt="image">
                  </div>
                  <span class="label label-xl label-muted font-weight-bold label-inline mt-6">Passport Copy</span>
                </div>
                <div class="mx-4">
                  <!--begin::Pic-->
                  <div class="">
                    <img src="{{ $residenceVisaImg  }}" class="rounded mt-3" style="width:150px" alt="image">
                  </div>
                  <span class="label label-xl label-muted font-weight-bold label-inline mt-6">Residence VISA</span>
                </div>
              </div>
            </div>
            </div>
					 </div>
					</div>
			 	</div>

       <!-- Company Information First Tab 2 -->
			 <div class="tab-pane fade bg-white rounded" id="company_tab" role="tabpanel" aria-labelledby="company_information" style="min-height:550px">
			    	<div class="card card-custom">
					 <div class="card-header">
						  <div class="card-title">
		            <span class="card-icon">
		              <i class="flaticon2-chat-1 text-primary"></i>
		            </span>
						   <h3 class="card-label">Company Information</h3>
						  </div>
		        <div class="card-toolbar">
		        </div>
					 </div>
           <div class="card-body p-0 shadow-none">
             @include('contact.company_grid',['request'=>$request,'model'=>$model])
          </div>
					</div>
				</div>

        <!-- Invoices Information First Tab 3 -->
			  <div class="tab-pane fade bg-white rounded" id="invoices_tab" role="tabpanel" aria-labelledby="invoices_tab" style="min-height:550px">
          <div class="card card-custom shadow-none">
           <div class="card-header">
              <div class="card-title">
                  <span class="card-icon">
                      <i class="flaticon2-chat-1 text-primary"></i>
                  </span>
               <h3 class="card-label">
                Invoices
               </h3>
              </div>
           </div>
           <div class="card-body p-0">
              @include('contact.invoices_grid',['request'=>$request,'model'=>$model])
           </div>
          </div>
				</div>

        <!-- Payments Information First Tab 4 -->
        <div class="tab-pane fade bg-white rounded" id="payment_tab" role="tabpanel" aria-labelledby="kt_tab_pane_4">
         <div class="card card-custom shadow-none">
          <div class="card-header">
           <div class="card-title">
             <span class="card-icon">
               <i class="flaticon2-chat-1 text-primary"></i>
             </span>
             <h3 class="card-label">
               Payments
             </h3>
           </div>
         </div>
         <div class="card-body">
          <x-salescom-payments-list :req="$request" :url="'payments/data-table-data?user_id='.$model->id" />
          </div>
        </div>
      </div>

        <!-- Actions tab section -->
        <div class="tab-pane fade bg-white rounded"  style="min-height:670px" role="tabpanel" aria-labelledby="tab-actions" id="tab-actions">
          <div class="card card-custom shadow-none">
           <div class="card-header">
            <div class="card-title">
              <span class="card-icon">
                  <i class="flaticon2-chat-1 text-primary"></i>
              </span>
             <h3 class="card-label">
              Action Log
             </h3>
            </div>
           </div>
           <div class="card-body">
             <x-actionlog-activity-history :model="$model"/>
           </div>
          </div>
        </div>


        <!-- Actions tab section -->
        <div class="tab-pane fade bg-white rounded"  style="min-height:670px" role="tabpanel" aria-labelledby="tab-attachments" id="tab-attachments">
          <div class="card card-custom shadow-none">
           <div class="card-header">
            <div class="card-title">
              <span class="card-icon">
                  <i class="flaticon2-chat-1 text-primary"></i>
              </span>
             <h3 class="card-label">
              Attachments
             </h3>
            </div>
           </div>
           <div class="card-body">
             <x-actionlog-attachment :model="$model"/>
           </div>
          </div>
        </div>



			</div>
		</div>
	</div>

  <x-actionlog-activity :model="$model"/>
@endsection

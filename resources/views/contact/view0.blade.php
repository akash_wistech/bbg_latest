
@extends('layouts.app')
@section('title', __('membership_lang::membership.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'membership'=> __('membership_lang::membership.heading'),
  __('common.create')
]])
@endsection
@section('content')


	<div class="container border rounded mt-4 py-5 px-0">

		<div class="d-flex px-0">
		<div class="col-3 border-right rounded bg-white py-4 px-6">
			<!--begin::Details-->
			<div class="d-flex mt-6">
				<!--begin: Pic-->
				<div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
					<div class="symbol symbol-50 symbol-lg-90">
						<img src="{{ ($model->image<>null) ? $model->image : asset('images/default-placeholder.png') }}" alt="image" />
					</div>
					<div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
						<span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
					</div>
				</div>
				<!--end::Pic-->
				<!--begin::Info-->
				<div class="flex-grow-1">
					<!--begin::Title-->
					<div class="d-flex justify-content-between flex-wrap mt-1">
						<div class="d-flex mr-3">
							<p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->first_name.' '.$model->last_name }}</p>
							<?php if($model->status==1){  ?>
							<a href="#">
								<i class="flaticon2-correct text-success font-size-h5"></i>
							</a>
							<?php } ?>
						</div>
					</div>
					<!--end::Title-->
					<!--begin::Content-->
					<div class="d-flex flex-wrap justify-content-between mt-1">
						<div class="d-flex flex-column flex-grow-1 pr-8">
							<div class="d-flex flex-wrap ">

								<?php if ($model->designation) { ?>
								<p class="text-dark-50 text-hover-primary font-weight-bold">
								<i class="flaticon2-calendar-3 mr-1"></i>{{$model->designation}}</p>
								<?php } ?>

							</div>
						</div>
					</div>
					<!--end::Content-->
				</div>
				<!--end::Info-->
			</div>
			<!--end::Details-->


			<div class="pb-7 mt-4">

			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Account Type:</span>
				<a href="#" class="text-muted text-hover-primary">{{ $model->account_type }}</a>
			</div>

      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Nationality:</span>
				<span class="text-muted">{{ $model->nationality }}</span>
			</div>

			<div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Phone:</span>
				<span class="text-muted">{{ $model->phone }}</span>
			</div>
      <div class="d-flex align-items-center justify-content-between mb-2">
				<span class="font-weight-bold mr-2">Email:</span>
				<span class="text-muted">{{ $model->email }}</span>
			</div>
			</div>


			<ul class="nav nav-tabs  mb-5  d-flex flex-column navi navi-hover navi-active navi-accent border-bottom-0">
			    <li class="navi-item my-1">
			        <a class="navi-link active" data-toggle="tab" href="#kt_tab_pane_2">
			            <span class="navi-icon pr-2">
			            	<span class="svg-icon"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo1\dist/../src/media/svg/icons\Code\Info-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
        <rect fill="#000000" x="11" y="10" width="2" height="7" rx="1"/>
        <rect fill="#000000" x="11" y="7" width="2" height="2" rx="1"/>
    </g>
</svg><!--end::Svg Icon--></span>
			            </span>
			            <span class="navi-text font-size-h6 ">Personal Information</span>
			        </a>
			    </li>
			     <li class="navi-item my-1">
			        <a class="navi-link" data-toggle="tab" href="#kt_tab_pane_1">
			            <span class="navi-icon pr-4"><i class="far fa-building"></i></span>
			            <span class="navi-text font-size-h6 ">Company</span>
			        </a>
			    </li>
			    <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#kt_tab_pane_3">
			            <span class="navi-icon pr-4"><i class="fas fa-file-invoice-dollar"></i></span>
			            <span class="navi-text font-size-h6">Invoices</span>
			        </a>
			    </li>
			     <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#kt_tab_pane_4">
			            <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
			            <span class="navi-text font-size-h6">Payment</span>
			        </a>
			    </li>
			    <!-- <li class="navi-item my-1">
			        <a class="navi-link mx-0" data-toggle="tab" href="#kt_tab_pane_5">
			            <span class="navi-icon pr-1"><i class="fas fa-wallet"></i></span>
			            <span class="navi-text font-size-h6">Subscriptions</span>
			        </a>
			    </li> -->
			</ul>
			</div>


			<div class="tab-content col-9 rounded pr-0" id="myTabContent">
			<div class="tab-pane fade show active bg-white rounded" id="kt_tab_pane_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
					<div class="card card-custom">
					 <div class="card-header">
					  <div class="card-title">
					            <span class="card-icon">
					                <i class="flaticon2-chat-1 text-primary"></i>
					            </span>
					   <h3 class="card-label">
					    Personal Information

					   </h3>
					  </div>
					        <div class="card-toolbar">
					        <a href="{{ url('membership/update/'.$model->id) }}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="" data-original-title="Update">
							<i class="text-dark-50 flaticon-edit"></i>
						    </a>
					        </div>
					 </div>
					 <div class="card-body">
					 	<div class="row" style="font-size: 14px;">
							<div class="col-6 px-0">
								<div class="col">
									<strong class="pr-3" >Company: </strong>{{$model->company->name}}
								</div>
								<div class="col my-2">
									<strong class="pr-3">Nationality:</strong>{{ $model->nationality }}
								</div>
                <!-- <div class="col my-2">
									<strong class="pr-3">Vat Number:</strong>
								</div> -->
								<div class="col">
									<strong class="pr-3">City:</strong>{{ $model->city }}
								</div>
								<div class="col my-2">
									<strong class="pr-3" >Country:</strong>{{ getPredefinedListItemsArr(2)[$model->country_code]}}
								</div>

								<div class="col">
									<strong class="pr-3">Address:</strong>{{$model->address}}
								</div>
							</div>

              <div class="col-6 pl-15">
                <h5 class="mb-4 pl-3">Social Network</h5>
                <div class="col py-1">
									<strong class="pr-3" >Linkedin: </strong>{{$model->linkedin}}
								</div>
                <div class="col py-1">
									<strong class="pr-3" >Twitter: </strong>{{$model->twitter}}
								</div>
              </div>
						</div>



            <div class="row px-0">
              <div class="col-6 mt-10">
                <!-- <div class="col px-0">
                  <h5 class="mb-4">How Did You Hear</h5>

                </div> -->
                <div class="col px-0 mt-10">
                  <h5 class="mb-4">Tell Us About Yourself</h5>
                  <span style="text-align: justify; text-justify:auto;">{{$model->tellUsAboutYourself}}</span>
                </div>
                <div class="col px-0 mt-6">
                  <h5 class="mb-4">Purpose Of Joining</h5>
                  {{$model->purposeOfJoining}}
                </div>
              </div>

            <div class="col-6 pl-20">
              <h3 class=" mt-10">Documents</h3>
            <div class="d-flex align-items-center text-align-center">
              <div class="mr-4">
                <!--begin::Pic-->
                <div class="">
                  <img src="{{ ($model->company->logo!=null) ? $model->company->logo : asset('images/default-placeholder.png')  }}" class="rounded mt-3" style="width:150px" alt="image">
                </div>
                <span class="label label-xl label-muted font-weight-bold label-inline mt-6 ml-3">Passport Copy</span>
              </div>
              <div class="mx-4">
                <!--begin::Pic-->
                <div class="">
                  <img src="{{ ($model->company->logo!=null) ? $model->company->logo : asset('images/default-placeholder.png')  }}" class="rounded mt-3" style="width:150px" alt="image">
                </div>
                <span class="label label-xl label-muted font-weight-bold label-inline mt-6 ml-3">Residence VISA</span>
              </div>
            </div>
          </div>



          </div>



					 </div>
					</div>
				</div>



						 <div class="tab-pane fade bg-white rounded" id="kt_tab_pane_1" role="tabpanel" aria-labelledby="kt_tab_pane_2" style="min-height:550px">

						    	<div class="card card-custom">
								 <div class="card-header">
								  <div class="card-title">
								            <span class="card-icon">
								                <i class="flaticon2-chat-1 text-primary"></i>
								            </span>
								   <h3 class="card-label">
								    Company Information

								   </h3>
								  </div>
								        <div class="card-toolbar">
								            <a href="{{ url('membership/update/'.$model->id) }}" class="btn btn-sm btn-clean btn-icon edit" data-toggle="tooltip" title="" data-original-title="Update">
											<i class="text-dark-50 flaticon-edit"></i>
										    </a>
								        </div>
								 </div>
								 <div class="card-body">

									<!--begin::Content-->
									<div class="d-flex flex-wrap">
										<div class="mr-12 d-flex flex-column mb-7">
											<span class="d-block font-weight-bold mb-4">Category</span>
											<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{ getPredefinedListItemsArr(4)[$model->company->category] }}</span>
										</div>
										<div class="mr-12 d-flex flex-column mb-7">
											<span class="d-block font-weight-bold mb-4">Phone No</span>
											<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{ $model->company->phonenumber }}</span>
										</div>
										<div class="mr-12 d-flex flex-column mb-7">
											<span class="d-block font-weight-bold mb-4">Fax</span>
											<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{ $model->company->fax }}</span>
										</div>
										<div class="mr-12 d-flex flex-column mb-7">
											<span class="d-block font-weight-bold mb-4">Postal Code</span>
											<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{ $model->company->postal_code }}</span>
										</div>
										<div class="mr-12 d-flex flex-column mb-7">
											<span class="d-block font-weight-bold mb-4">Emirate</span>
											<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{ $model->company->emirates_number }}</span>
										</div>
										<div class="mr-12 d-flex flex-column mb-7">
											<span class="d-block font-weight-bold mb-4">Website</span>
											<span class="btn btn-light-primary btn-sm font-weight-bold btn-upper btn-text">{{ $model->company->url }}</span>
										</div>
									</div>
									<!--end::Content-->
									<!--begin::Text-->
									<h5>About Company</h5>
									<p class="mb-7 mt-3"> <i class="fas fa-audio-description" ></i>
										{{ $model->company->about_company }}
									</p>
									<!--end::Text-->
								  </div>
								</div>
							</div>



			  <div class="tab-pane fade bg-white rounded" id="kt_tab_pane_3" role="tabpanel" aria-labelledby="kt_tab_pane_3" style="min-height:550px">

        <div class="card card-custom shadow-none">
				 <div class="card-header">
				  <div class="card-title">
            <span class="card-icon">
                <i class="flaticon2-chat-1 text-primary"></i>
            </span>
				   <h3 class="card-label">
				    Invoices
				   </h3>
				  </div>
				 </div>
				 <div class="card-body p-0 ">
            @include('membership::membership.invoices_grid',['request'=>$request])
				 </div>
				</div>
				</div>

				<div class="tab-pane fade bg-white rounded" id="kt_tab_pane_4" role="tabpanel" aria-labelledby="kt_tab_pane_4" style="min-height:550px">
          <div class="card card-custom shadow-none">
					 <div class="card-header">
					  <div class="card-title">
					            <span class="card-icon">
					                <i class="flaticon2-chat-1 text-primary"></i>
					            </span>
					   <h3 class="card-label">
					    Payments
					   </h3>
					  </div>
					 </div>
					 <div class="card-body p-0">
					        @include('membership::membership.payment_grid',['request'=>$request])
					 </div>
					</div>
				</div>



				<!-- <div class="tab-pane fade bg-white rounded" id="kt_tab_pane_5" role="tabpanel" aria-labelledby="kt_tab_pane_4" style="min-height:550px">
          <div class="card card-custom shadow-none">
					 <div class="card-header">
					  <div class="card-title">
	            <span class="card-icon">
	                <i class="flaticon2-chat-1 text-primary"></i>
	            </span>
					   <h3 class="card-label">
					    Subscription
					   </h3>
					  </div>
					 </div>
					 <div class="card-body p-0">
					   include('company::company.subscription_grid',['subscriptions'=>$subscriptions,'request'=>$request])
					 </div>
					</div>
				</div> -->


				</div>
			</div>
		</div>


























@endsection

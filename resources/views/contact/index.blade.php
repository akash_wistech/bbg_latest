<?php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'primary','link'=>'/contact/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>'/contact/import'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('Profile Image'),'data'=>'image','name'=>'image','orderable'=>false];
$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.name'),'data'=>'name','name'=>'name'];
$advSearchCols[]=['title'=>__('common.name'),'name'=>'name'];
$dtColsArr[]=['title'=>__('common.company_name'),'data'=>'company_name','name'=>'company_name'];
$advSearchCols[]=['title'=>__('common.company_name'),'name'=>'company_name'];
$dtColsArr[]=['title'=>__('common.email'),'data'=>'email','name'=>'email'];
$dtColsArr[]=['title'=>__('User Type'),'data'=>'user_type','name'=>'user_type'];
$advSearchCols[]=['title'=>__('common.email'),'name'=>'email'];
$dtColsArr[]=['title'=>__('common.phone'),'data'=>'phone','name'=>'phone'];

$dtColsArr[]=['title'=>__('Bill To Name'),'data'=>'bill_to_name','name'=>'bill_to_name'];
$dtColsArr[]=['title'=>__('Bill To Company'),'data'=>'bill_to_company','name'=>'bill_to_company'];
$dtColsArr[]=['title'=>__('Bill To Address'),'data'=>'bill_to_address','name'=>'bill_to_address'];
$advSearchCols[]=['title'=>__('common.phone'),'name'=>'phone'];
$advSearchCols[]=['title'=>__('Bill To Name'),'name'=>'bill_to_name'];
$advSearchCols[]=['title'=>__('Bill To Company'),'name'=>'bill_to_company'];
$advSearchCols[]=['title'=>__('Bill To Address'),'name'=>'bill_to_address'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}

$dtColsArr[]=['title'=>__('common.status'),'data'=>'status','name'=>'status'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
?>

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('app.member.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('app.member.heading')
]])
@endsection


@section('content')
<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="'contact'" :jsonUrl="'contact/data-table-data'" :dtColsArr="$dtColsArr" :advSearch="true" :exportMenu="true" :advSrchColArr="$advSearchCols" :cbSelection="true" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>
@endsection

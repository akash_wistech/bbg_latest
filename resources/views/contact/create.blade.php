@extends('layouts.app')
@section('title', __('app.member.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  'contact'=>__('app.member.heading'),
  __('common.create')
]])
@endsection
@section('content')
@include('contact._form')
@endsection

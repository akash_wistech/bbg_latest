@php


$company_id=0;
 foreach ($company as $key => $value){
   $company_id=$value['id'];
 }


$btnsList = [];

$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];
$advSearchCols = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('Membership Type'),'data'=>'membership_type_id','name'=>'membership_type_id'];
$dtColsArr[]=['title'=>__('Company'),'data'=>'company_id','name'=>'company_id'];
$dtColsArr[]=['title'=>__('Reference No'),'data'=>'reference_no','name'=>'reference_no'];
$dtColsArr[]=['title'=>__('Start Date'),'data'=>'start_date','name'=>'start_date'];
$dtColsArr[]=['title'=>__('End Date'),'data'=>'end_date','name'=>'end_date'];
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
@endphp


<div class="card card-custom">
  <div id="grid-table3" class="card card-body">
    <x-data-table-grid :sid="3"  :request="$request" :showStats="false" :controller="'subscription'" :jsonUrl="'subscription/data-table-data?company_id='.$company_id" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>

@php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'company/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>'company/import'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.color_col'),'data'=>'cs_col','name'=>'cs_col'];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.logo'),'data'=>'logo','name'=>'logo'];
$dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
$advSearchCols[]=['title'=>__('common.name'),'name'=>'title'];
@endphp
<!-- if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
} -->
@php
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
$moduleTypeId='';
@endphp


<div class="card card-custom shadow-none border-0">
  <div id="grid-table1" class="card card-body shadow-none border-0">
    <x-data-table-grid :sid="1" :request="$request" :controller="'company'" :jsonUrl="'company/data-table-data?id='.$model->id" :dtColsArr="$dtColsArr" :advSearch="true" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>

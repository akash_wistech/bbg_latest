<div class="d-flex mt-6">
  <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
    <div class="symbol symbol-50 symbol-lg-90">
      <img src="{{ ($model->logo<>null) ? $model->logo : asset('images/default-placeholder.png') }}" alt="image" />
    </div>
  </div>
  <div class="flex-grow-1">
    <div class="d-flex justify-content-between flex-wrap mt-1">
      <div class="d-flex mr-3">
        <p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->title }}</p>
        @if($model->visibility==1)
        <i class="flaticon2-correct text-success font-size-h5"></i>
        @endif
      </div>
    </div>
    <div class="d-flex flex-wrap justify-content-between mt-1">
      <div class="d-flex flex-column flex-grow-1 pr-8">
        <div class="d-flex flex-wrap ">
          <p class="text-dark-50 text-hover-primary font-weight-bold">
            <span class="text-muted font-weight-bold">
              {!! $model->active_subscription_title !!}
            </span>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="pb-7 mt-4">
  <!-- 	<div class="d-flex align-items-center justify-content-between mb-2">
  <span class="font-weight-bold mr-2">Membership Type:</span>
  <a href="#" class="text-muted text-hover-primary"><?php //echo getMemberType($model->membership_type_id) ?></a>
</div> -->
<div class="d-flex align-items-center justify-content-between mb-2">
  <span class="font-weight-bold mr-2">Sector:</span>
  <a href="#" class="text-muted text-hover-primary">{{ ((isset($companyCustomFields['Sector & Industries']) && $companyCustomFields['Sector & Industries'] !=null ) ? $companyCustomFields['Sector & Industries'] : '') }}</a>
</div>
<div class="d-flex align-items-center justify-content-between mb-2">
  <span class="font-weight-bold mr-2">Email:</span>
  <span class="text-muted">{{ $model->email }}</span>
</div>
<div class="d-flex align-items-center justify-content-between mb-2">
  <span class="font-weight-bold mr-2">Phone:</span>
  <span class="text-muted">{{((isset($companyCustomFields['Phone']) && $companyCustomFields['Phone'] !=null ) ? $companyCustomFields['Phone'] : '') }}</span>
</div>
</div>

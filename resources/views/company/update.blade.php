@extends('layouts.app')
@section('title', __('app.company.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'company'=>__('app.company.heading'),
__('common.update')
]])
@endsection

@section('content')
  @include('company._form')
@endsection

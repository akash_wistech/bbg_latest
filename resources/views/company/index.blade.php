<?php
$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'primary','link'=>$controllerId.'/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>$controllerId.'/import'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);
$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.logo'),'data'=>'logo','name'=>'logo'];
$dtColsArr[]=['title'=>__('common.title'),'data'=>'title','name'=>'title'];
$advSearchCols[]=['title'=>__('TItle'),'name'=>'title'];
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
}
if(function_exists('getSubscriptionInfo')){
$dtColsArr[]=['title'=>__('common.subscriptions'),'data'=>'subscriptions','name'=>'subscriptions'];
//$dtColsArr[]=['title'=>__('common.subscription_end_date'),'data'=>'end_date','name'=>'end_date'];
}
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];

?>

@extends('layouts.app',['btnsList'=>$btnsList])
@section('title', __('app.company.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
__('app.company.heading')
]])
@endsection
@section('content')


<?php
use NaeemAwan\ModuleSubscription\Models\Subscription;
use App\Models\Company;

$companies = Company::whereNull('deleted_at')
->where('is_dummy',0)
->count();

$essential = Subscription::where('membership_type_id',1)->count();
$business = Subscription::where('membership_type_id',2)->count();
$business_advance = Subscription::where('membership_type_id',3)->count();

$totalActiveMembers= $essential+$business+$business_advance;
 ?>

<div class="col-md-6 col-xl-12 mb-5 px-0">
  <div class="card card-statistics">
    <!----><!---->
    <div class="card-header pb-0 d-flex justify-content-space-between border-0">
      <h4 class="card-title pb-0">Statistics</h4>
      <!-- <p class="card-text font-small-2  mb-0 float-right ml-auto pb-0"> Updated 1 month ago </p> -->
    </div>
    <div class="card-body statistics-body pt-0">
      <!----><!----><div class="row">

      <div class="col-xl-3 mb-2 mb-xl-0 col-sm-7">
        <div class="media">
          <div class="align-self-start bg-light-info position-relative mr-5" style="width: 55px; height: 55px;  border-radius:50%">
          <span class="position-absolute" style="left: 26%;top: 27%;">
            <span class="b-avatar-custom text-info" >
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trending-up"><polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline><polyline points="17 6 23 6 23 12"></polyline></svg>
            </span>
          </span>
          </div>
          <div class="media-body my-auto">
            <h4 class="font-weight-bolder mb-0"> {{ $totalActiveMembers }} </h4>
            <p class="card-text font-small-3 mb-0"> Total Number of Head Counts </p>
          </div>
        </div>
      </div>


      <div class="col-xl-3 mb-2 mb-xl-0 col-sm-7">
        <div class="media">
          <div class="align-self-start bg-light-success position-relative mr-5" style="width: 55px; height: 55px;  border-radius:50%">
          <span class="position-absolute" style="left: 27.5%;top: 27%;">
            <span class="b-avatar-custom text-success" >
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
            </span>
          </span>
          </div>
          <div class="media-body my-auto">
            <h4 class="font-weight-bolder mb-0"> {{ $essential }} </h4>
            <p class="card-text font-small-3 mb-0"> Essential </p>
          </div>
        </div>
      </div>


      <div class="col-xl-3 mb-2 mb-xl-0 col-sm-7">
        <div class="media">
          <div class="align-self-start bg-light-danger position-relative mr-5" style="width: 55px; height: 55px;  border-radius:50%">
          <span class="position-absolute" style="left: 28%;top: 27%;">
            <span class="b-avatar-custom text-danger" >
              <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line>
              </svg>
            </span>
          </span>
          </div>
          <div class="media-body my-auto">
            <h4 class="font-weight-bolder mb-0">{{ $business }} </h4>
            <p class="card-text font-small-3 mb-0"> Business </p>
          </div>
        </div>
      </div>


      <div class="col-xl-3 mb-2 mb-xl-0 col-sm-7">
        <div class="media">
          <div class="align-self-start bg-light-primary position-relative mr-5" style="width: 55px; height: 55px;  border-radius:50%">
          <span class="position-absolute" style="left: 23%;top: 24%;">
            <span class="b-avatar-custom text-primary" >
              <i class="la la-group text-primary pr-3" style="font-size: 2.3rem !important"></i>
              </svg>
            </span>
          </span>
          </div>
          <div class="media-body my-auto">
            <h4 class="font-weight-bolder mb-0">{{ $business_advance }} </h4>
            <p class="card-text font-small-3 mb-0"> Business Advance </p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!----><!---->
</div>
</div>

<div class="card card-custom">
  <div id="grid-table" class="card card-body">
    <x-data-table-grid :request="$request" :controller="$controllerId" :jsonUrl="$controllerId.'/data-table-data'" :cbSelection="true" :dtColsArr="$dtColsArr" :advSearch="true" :exportMenu="true" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
  </div>

</div>

@endsection

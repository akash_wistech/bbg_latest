     <table style="font-size:11px;">
       <tr>
         <td colspan="14"></td>
         <td colspan="5" style="text-align: right;">
           <br><br><b>British Business Group</b>
           <br>British Embassy Compound
           <br>Al Seef Road
           <br>Bur Dubai
           <br>United Arab Emirates
           <br>P.O. Box 9333
         </td>
       </tr>
      </table>
       <table>
         <br><br>
       <tr>
         <td colspan="10" style="font-size:10px;">
           <br><span>To:</span>
           <br><b>{{ $model->title }}</b>
           <br><span>70316 Hudson Road</span>
           <br><span>North Velda Maine</span>
           <br><span>WF 61724</span>
         </td>
         <td colspan="4" style="text-align: right;">
           <span style="font-weight:bold; font-size:14.5px;">Account&nbsp;&nbsp;Summary</span>
           <span style="font-size:11px; color:gray;" >2022-02-01 To 2022-02-28</span>
         </td>
       </tr>
     </table>

     <table style="font-size:10px;" cellpadding="3">
       <tr >
         <td colspan="15"></td>
         <td colspan="5" style=" border-top:1px solid black">Beginning Balance:</td>
         <td colspan="4"  style=" border-top:1px solid black; text-align: right;">AED 0.00</td>
       </tr>
       <tr>
         <td colspan="15"></td>
         <td colspan="5">Invoiced Amount:</td>
         <td colspan="4"  style=" text-align: right;">AED {{ $totalInvoiceAmount }}</td>
       </tr>
       <tr>
         <td colspan="15"></td>
         <td colspan="5">Amount Paid:</td>
         <td colspan="4"  style=" text-align: right;">AED {{ $totalPaymentAmount }}</td>
       </tr>
       <tr>
         <td colspan="15"></td>
         <td colspan="5">Balance Due:</td>
         <td colspan="4"  style=" text-align: right;">AED {{ $TotalBalence }}</td>
       </tr>
     </table>

     <style>
     tr.as th{
       border: 1px solid #EEEEEE;
       text-align: left;
       padding: 8px;
       font-size: 10px;
     }
     tr.as td{
       border: 1px solid #EEEEEE;
       text-align: left;
       padding: 8px;
       font-size: 10px;
     }
     </style>
   <br><br>
    <!--Invoice Table -->
    <table style="border-collapse: collapse; width: 100%;" cellpadding="6">
    <tr class="as">
      <th colspan="5">Date</th>
      <th colspan="10">Details</th>
      <th colspan="3">Amount</th>
      <th colspan="3">Payments</th>
      <th colspan="3">Balance</th>
    </tr>
    <?php
    $totalBalence =0;
    $n=0;
    foreach ($statements as $key => $statement) {
      if ($statement['type']=='invoice') {
        $totalBalence = $totalBalence+$statement['total'];
      }
      if ($statement['type']=='payment') {
        $totalBalence = $totalBalence-$statement['total'];
        $invoicesToPayment = \Wisdom\Sales\Models\Invoices::where('id',$statement['id'])->select('reference_number')->first();
      }   ?>
      <tr class="as" style="<?= ($n%2==0)? 'background-color: #f9f9f9;' : '' ?>">
        <td colspan="5">{{ $statement['date'] }}</td>
        <td colspan="10">{{ (($statement['type']=='invoice')? ucwords($statement['type']).' ('.$statement['reference_number'].') due to - '.$statement['due_date'] : ucwords($statement['type'].' to invoice '.$invoicesToPayment['reference_number']) ) }}</td>
        <td colspan="3">{{ (($statement['type']=='invoice')? $statement['total'] : '') }}</td>
        <td colspan="3">{{ (($statement['type']=='payment')? $statement['total'] : '') }}</td>
        <td colspan="3">{{ $totalBalence }}</td>
      </tr>
  <?php $n++; } ?>
      <tr>
        <td colspan="15"></td>
        <td colspan="6"><b>Balance Due</b></td>
        <td colspan="3">{{ $totalBalence }}</td>
      </tr>
    </table>

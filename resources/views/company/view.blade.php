@extends('layouts.app')
@section('title', __('Company'))
@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
'membership'=> __('Company'),
__('common.create')
]])
@endsection
@section('content')
<div class="container border rounded mt-4 py-5 px-0">
  <div class="d-flex flex-md-row flex-column  px-0">
    <div class="col-xl-3 col-md-4 col-12 border-right rounded bg-white py-4 px-6 ">
     <!--begin::Details-->
     <div class="d-flex mt-6">
      <!--begin: Pic-->
      <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
       <div class="symbol symbol-50 symbol-lg-90">
        <img src="{{ ($model->logo<>null) ? $model->logo : asset('images/default-placeholder.png') }}" alt="image" />
      </div>
      <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
        <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
      </div>
    </div>
    <!--end::Pic-->
    <!--begin::Info-->
    <div class="flex-grow-1">
     <!--begin::Title-->
     <div class="d-flex justify-content-between flex-wrap mt-1">
      <div class="d-flex mr-3">
       <p class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3 mb-0">{{ $model->title }}</p>
       <a href="{{ url('company/update/'.$model->id) }}">
        <i class="flaticon-edit-1 text-info font-size-h5"></i>
      </a>
      <?php if($model->status==1){  ?>
       <a href="#" class="ml-2">
        <i class="flaticon2-correct text-success font-size-h5"></i>
      </a>
    <?php } ?>
  </div>
</div>
<!--end::Title-->
<!--begin::Content-->
<div class="d-flex flex-wrap justify-content-between mt-1">
  <div class="d-flex flex-column flex-grow-1 pr-8">
   <div class="d-flex flex-wrap ">

    <?php if ($model->address) { ?>
      <p class="text-dark-50 text-hover-primary font-weight-bold">
        <span class="text-muted font-weight-bold"><i class="flaticon2-placeholder mr-2 font-size-lg"></i>
          {!! $model->address !!}</span></p>


        <?php } ?>

      </div>
    </div>
  </div>
  <!--end::Content-->
</div>
<!--end::Info-->
</div>
<!--end::Details-->

      <!-- Side Bar Data -->
      <div class="pb-7 mt-4">
         <div class="d-flex align-items-center justify-content-between mb-2">
          <span class="font-weight-bold mr-2">Membership Number:</span>
          <a href="#" class="text-muted text-hover-primary">{{ ($model->reference_no!=null)? $model->reference_no : 'N\A' }}</a>
        </div>
        <div class="d-flex align-items-center justify-content-between mb-2">
          <span class="font-weight-bold mr-2">Membership Type:</span>
          <span class="text-muted">{!! (($MembType!=null)? $MembType->description : 'N\A') !!}</span>
        </div>
      </div>


      <!-- Tab Links -->
      <ul class="nav nav-tabs  mb-5  d-flex flex-md-column flex-row navi navi-hover navi-active navi-accent border-bottom-0">
        <?php foreach (getCompanyViewTabs() as $link => $value) {   ?>
          <li class="navi-item col-md-12 col-6 px-0 my-1">
           <a class="navi-link {{ (($link=='company-detail-tab') ? 'active' : '') }}" data-toggle="tab" href="{{ '#'.$link }}">
             <span class="navi-icon pr-4"><i class="{{ $value['font-aws'] }}"></i></span>
             <span class="navi-text font-size-h6 ">{{ $value['name'] }}</span>
           </a>
         </li>
        <?php  }  ?>
      </ul>

    </div>

    <!-- Tab Views -->
    <div class="tab-content col-xl-9 col-md-8 col-12 mt-md-0 mt-4 pl-md-5 pl-0  rounded pr-0" id="myTabContent" >
      <?php foreach (getCompanyViewTabs() as $link => $value) {   ?>
        <div class="tab-pane fade {{ (($link=='company-detail-tab') ? 'active show' : '') }} px-0 bg-white rounded" id="{{$link}}" role="tabpanel" aria-labelledby="{{$link}}" style="min-height:900px">
          <?php if ($value['grid']=='1') { ?>
            <div class="card card-custom shadow-none ">
             <div class="card-header">
              <div class="card-title">
                <span class="card-icon">
                  <i class="{{$value['font-aws']}}"></i>
                </span>
                <h3 class="card-label">
                  {{ $value['name'] }}
                </h3>
              </div>
            </div>
            <div class="">
             @include('company.tabs.'.$link,['model'=>$model])
            </div>
            </div>
         <?php  }else { ?>
           @include('company.tabs.'.$link,['model'=>$model])
         <?php } ?>

        </div>
      <?php  }  ?>
    </div>

  </div>

</div>

<x-actionlog-activity :model="$model"/>

@endsection

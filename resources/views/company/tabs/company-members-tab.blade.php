@php

$btnsList = [];
if(checkActionAllowed('create')){
  $btnsList[] = ['label'=>__('common.create'),'icon'=>'plus','class'=>'success','link'=>'/contact/create', 'method'=>'post'];
}
if(checkActionAllowed('import')){
  $btnsList[] = ['label'=>__('common.import'),'icon'=>'file-import','class'=>'primary','link'=>'/contact/import'];
}
$gridViewColumns = getGridViewColumns($moduleTypeId);

$dtColsArr = [];

$dtColsArr[]=['title'=>__('common.color_col'),'data'=>'cs_col','name'=>'cs_col'];
$dtColsArr[]=['title'=>__('common.id'),'data'=>'id','name'=>'id'];
$dtColsArr[]=['title'=>__('common.name'),'data'=>'name','name'=>'name'];
$advSearchCols[]=['title'=>__('common.name'),'name'=>'name'];
$dtColsArr[]=['title'=>__('common.company_name'),'data'=>'company_name','name'=>'company_name'];
$advSearchCols[]=['title'=>__('common.company_name'),'name'=>'company_name'];
$dtColsArr[]=['title'=>__('common.email'),'data'=>'email','name'=>'email'];
$dtColsArr[]=['title'=>__('User Type'),'data'=>'user_type','name'=>'user_type'];
$advSearchCols[]=['title'=>__('common.email'),'name'=>'email'];
$dtColsArr[]=['title'=>__('common.phone'),'data'=>'phone','name'=>'phone'];
$advSearchCols[]=['title'=>__('common.phone'),'name'=>'phone'];
@endphp

<!-- if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $dtColsArr[]=['title'=>$gridViewColumn['title'],'data'=>'col_'.$gridViewColumn['id'],'name'=>'col_'.$gridViewColumn['id']];
  }
} -->

@php
$dtColsArr[]=['title'=>__('common.created_at'),'data'=>'created_at','name'=>'created_at'];
$dtColsArr[]=['title'=>__('common.created_by'),'data'=>'created_by','name'=>'created_by'];

$dtColsArr[]=['title'=>__('common.action'),'data'=>'action_col','name'=>'action_col','orderable'=>false,'width'=>'85'];
$moduleTypeId = '';

@endphp

<div class="card card-custom shadow-none border-0">
  <div id="grid-table1" class="card card-body shadow-none border-0">
    <x-data-table-grid :sid="1" :request="$request" :controller="'contact'" :jsonUrl="'contact/data-table-data?id='.$model->id" :dtColsArr="$dtColsArr" :advSearch="false" :advSrchColArr="$advSearchCols" :colorCol="true" :moduleTypeId="$moduleTypeId"/>
  </div>
</div>

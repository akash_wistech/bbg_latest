
<?php
if($model->trade_licence != '' && $model->trade_licence != null){
  if(str_contains($model->trade_licence, '.pdf')){
    $trade_licence =asset('images/pdf_sample_img.png');
  }else{
    $trade_licence=$model->trade_licence;
  }
}else{
  $trade_licence = asset('assets/images/dummy-image.jpg');
}



if($model->vat_document != '' && $model->vat_document != null){
  if(str_contains($model->vat_document, '.pdf')){
    $vat_document =asset('images/pdf_sample_img.png');
  }else{
    $vat_document=$model->vat_document;
  }
}else{
  $vat_document = asset('assets/images/dummy-image.jpg');
}

$nonTextAreaColumns=getNonTextAreaColumns($model->moduleTypeId);
$textAreaColumns=getTextAreaColumns($model->moduleTypeId);
?>


<div class="card-body">
 <div class="card-body rounded pb-0" style="background-color:#f8f8f8;">
   <div class="row">
     <div class="col-6 col-md-3">
       <div class="mb-8 d-flex flex-column">
         <span class="text-dark font-weight-bolder mb-2">Reference No</span>
         <span class="text-muted  font-size-lg">{{ ($model->reference_no!=null)? $model->reference_no : 'N\A' }}</span>
       </div>
     </div>
     <div class="col-6 col-md-3">
       <div class="mb-8 d-flex flex-column">
         <span class="text-dark font-weight-bolder mb-2">Title</span>
         <span class="text-muted  font-size-lg">{{ ($model->title!=null)? $model->title : 'N\A' }}</span>
       </div>
     </div>
     <div class="col-6 col-md-3">
       <div class="mb-8 d-flex flex-column">
         <span class="text-dark font-weight-bolder mb-2">Email</span>
         <span class="text-muted  font-size-lg">{{ ($model->email!=null)? $model->email : 'N\A' }}</span>
       </div>
     </div>
     <div class="col-6 col-md-3">
       <div class="mb-8 d-flex flex-column">
         <span class="text-dark font-weight-bolder mb-2">Vat Number</span>
         <span class="text-muted  font-size-lg">{{ ($model->vat_number!=null)? $model->vat_number : 'N\A' }}</span>
       </div>
     </div>
     <div class="col-6 col-md-3">
       <div class="mb-8 d-flex flex-column">
         <span class="text-dark font-weight-bolder mb-2">Parent Company</span>
         <span class="text-muted  font-size-lg">{{ ($model->parent_company_id!=0)? $model->parent_company_id : 'N\A' }}</span>
       </div>
     </div>
     @if($nonTextAreaColumns->isNotEmpty())
     @foreach($nonTextAreaColumns as $nonTextAreaColumn)
     @php
     $value=getInputFielSavedValueForDetail($model,$nonTextAreaColumn);
     if(is_array($value))$value = implode(", ",$value);
     @endphp
     <div class="col-6 col-md-3 ">
       <div class="mb-8 d-flex flex-column">
        <span class="text-dark font-weight-bolder mb-2">{{$nonTextAreaColumn['title']}}</span>
        <span class="text-muted font-size-lg" style="word-break: break-all;">{{ ($value!=null)? $value : 'N\A' }}</span>
      </div>
    </div>
    @endforeach
    @endif
  </div>
</div>

<!-- Company Description and Images -->
<div class="row  mt-8">
 <div class="col-xl-3 col-md-4 d-flex flex-md-column flex-row">
   <div class="mb-3 mx-md-0 mx-5">
     <div class="">
       <img src="{{ $trade_licence  }}" class="rounded mt-3 shadow-sm" style="width:120px" alt="image">
     </div>
     <span class="label label-xl label-muted font-weight-bold label-inline mt-3">Trade Licence</span>
   </div>
   <div class="mb-3 mx-md-0 mx-5">
     <div class="">
       <img src="{{ $vat_document }}" class="rounded mt-3 shadow-sm" style="width:120px" alt="image">
     </div>
     <span class="label label-xl label-muted font-weight-bold label-inline mt-3">{{__('VAT Document')}}</span>
   </div>
 </div>

 <div class="col-xl-9 col-md-8 col-12 mt-3">
   <div class="row">
     @if($textAreaColumns->isNotEmpty())
     @foreach($textAreaColumns as $textAreaColumn)
     @php
     $value=getInputFielSavedValueForDetail($model,$textAreaColumn);
     if(is_array($value))$value = implode(", ",$value);
     @endphp
     <div class="">
       <h5>{{ $textAreaColumn['title'] }}</h5>
       <p class="mb-7">{!! $value !!}</p>
     </div>
     @endforeach
     @endif
   </div>
 </div>
</div>

</div>

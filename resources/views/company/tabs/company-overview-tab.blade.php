<div class="card-body">
  <?php
  $company_id= $model->id;
  $totalevent = getTotalEvents();
  $eventByCompany = getTotalEvents($company_id);
  $eventByCompanyAttended = getTotalEvents($company_id,1);
  foreach ($totalevent as $key => $value) {
   $event_s_paysumArr[$key]['sum']=getJoinedSum($company_id,$key);
   $event_s_paysumArr[$key]['event_by_year']=$value;
   $event_s_paysumArr[$key]['register_event']=0;
   $event_s_paysumArr[$key]['attended_event']=0;
   $event_s_paysumArr[$key]['news_uploaded']=getNewsByYear($company_id,$key);
   if (array_key_exists($key, $eventByCompany)) {
     $event_s_paysumArr[$key]['register_event']=$eventByCompany[$key];
   }
   if (array_key_exists($key, $eventByCompanyAttended)) {
     $event_s_paysumArr[$key]['attended_event']=$eventByCompanyAttended[$key];
   }
   ?>
   <!-- 2022 -->
   <div class="my-5 shadow-sm p-4 rounded">
    <h3 class="py-2">{{ $key }}</h3>
    <div class="d-flex align-items-center flex-wrap">
     <!--begin: Item-->
     <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
       <span class="mr-4">
         <i class="flaticon-confetti icon-2x text-primary font-weight-bold"></i>
       </span>
       <div class="d-flex flex-column text-dark-75">
         <span class="font-weight-bolder font-size-sm">Registered Events</span>
         <span class="font-weight-bolder font-size-h5">
           <span class="text-dark-50 font-weight-bold">{{ $event_s_paysumArr[$key]['register_event'].'/'.$value }}</span>
         </div>
       </div>
       <!--end: Item-->
       <!--begin: Item-->
       <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
         <span class="mr-4">
           <i class="flaticon-confetti icon-2x text-primary font-weight-bold"></i>
         </span>
         <div class="d-flex flex-column text-dark-75">
           <span class="font-weight-bolder font-size-sm">Attended Events</span>
           <span class="font-weight-bolder font-size-h5">
             <span class="text-dark-50 font-weight-bold">{{ $event_s_paysumArr[$key]['attended_event'].'/'.$value }}</span>
           </div>
         </div>
         <!--end: Item-->
         <!--begin: Item-->
         <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
           <span class="mr-4">
             <i class="flaticon-piggy-bank icon-2x text-primary font-weight-bold"></i>
           </span>
           <div class="d-flex flex-column text-dark-75">
             <span class="font-weight-bolder font-size-sm">Events Amount Paid</span>
             <span class="font-weight-bolder font-size-h5">
               <span class="text-dark-50 font-weight-bold pr-3">AED</span>{{ getJoinedSum($company_id,$key) }}</span>
             </div>
           </div>
           <!--end: Item-->
           <!--begin: Item-->
           <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
             <span class="mr-4">
               <i class="flaticon-pie-chart icon-2x text-primary font-weight-bold"></i>
             </span>
             <div class="d-flex flex-column text-dark-75">
               <span class="font-weight-bolder font-size-sm">News Published</span>
               <span class="font-weight-bolder font-size-h5">
                 <span class="text-dark-50 font-weight-bold">{{ getNewsByYear($company_id,$key); }}</span>
               </div>
             </div>
             <!--end: Item-->
               </div>
             </div>
           <?php } ?>
         </div>

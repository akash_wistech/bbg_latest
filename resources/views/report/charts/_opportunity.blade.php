@php
$module='opportunity';
$workflows = getModuleWorkFlowsListArr($module);
@endphp
@if($workflows!=null)
@foreach($workflows as $workflow)
<div class="col-lg-6">
  @php
  $workflowStats = getWorkflowStatsData($module,$workflow,$model);
  @endphp
  <x-am-chart :type="'pie'" :id="$module.'wfs'.$workflow->id" :heading="$workflow->title" :label="true" :data="$workflowStats"/>
</div>
@endforeach
@endif

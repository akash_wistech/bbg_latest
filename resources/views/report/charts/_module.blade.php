@php
$moduleLabels = getModulesLabelListArr();
@endphp
@foreach(getModulesHavingReports() as $key=>$val)
<div class="card card-custom card-border mb-3" data-card="true">
  <div class="card-header">
		<div class="card-title">
			<h3 class="card-label">{{$moduleLabels[$val]}}</h3>
		</div>
    <div class="card-toolbar">
      <a href="javascript:;" class="btn btn-icon btn-sm btn-hover-light-primary" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" data-original-title="{{ __('common.toggle') }}">
        <i class="ki ki-arrow-down icon-nm"></i>
      </a>
    </div>
	</div>
  <div class="card-body">
    <div class="row">
      @php
      $customFieldStats = getCustomFieldStats($val,$model);
      @endphp
      @if($customFieldStats!=null)
        @foreach($customFieldStats as $customFieldStat)
        <div class="col-lg-6">
          @if(count($customFieldStat['data'])>8)
          <x-am-chart :type="'pie'" :id="$val.'cfs'.$customFieldStat['id']" :heading="$customFieldStat['heading']" :label="true" :data="$customFieldStat['data']"/>
          @else
          <x-am-chart :type="'pie'" :id="$val.'cfs'.$customFieldStat['id']" :heading="$customFieldStat['heading']" :label="true" :data="$customFieldStat['data']"/>
          @endif
        </div>
        @endforeach
      @endif
      @include('report.charts._'.$val)
    </div>
  </div>
</div>
@endforeach

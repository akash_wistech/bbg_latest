@php
$staffMembersList = getStaffMemberListArr();
$created_atVal=null;
$createdByValue=null;
@endphp
<div class="card card-custom card-border mb-3">
  <div class="card-body">
    {!! Form::model($model, ['method'=>'get']) !!}
    <div class="row">
      <div class="col-sm-4">
        <div class="form-group">
          {{ Form::label('created_at',__('common.created_at')) }}
          <div class="input-group dtrpicker">
            {{ Form::text('created_at', $created_atVal, ['class'=>'form-control', 'readonly'=>'readonly']) }}
            <div class="input-group-append">
              <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          {{ Form::label('created_by',__('common.created_by')) }}
          {{Form::select('created_by[]',$staffMembersList,$createdByValue,['class'=>'form-control selectpicker','data-selected-text-format'=>'count>3','multiple'=>'multiple'])}}
        </div>
      </div>
      <div class="col-sm-2 pt-8">
        <button type="submit" class="btn btn-primary font-weight-bold">{{__('common.filter')}}</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>

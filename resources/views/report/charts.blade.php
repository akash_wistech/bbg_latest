@extends('layouts.app')
@section('title', __('app.reports.heading'))

@section('breadcrumbs')
@include('layouts.blocks.breadcrumbs', ['breadcrumbs' => [
  __('app.reports.heading'),
  __('app.reports.heading_for',['mdoule'=>getModulesLabelListArr()[$module]])
]])
@endsection

@section('content')
<div id="filtered-charts">
  @include('report.charts._filter')
</div>
@include('report.charts._module')
@endsection
@push('js')
<script src="//www.amcharts.com/lib/3/amcharts.js"></script>
<script src="//www.amcharts.com/lib/3/serial.js"></script>
<script src="//www.amcharts.com/lib/3/radar.js"></script>
<script src="//www.amcharts.com/lib/3/pie.js"></script>
<script src="//www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js"></script>
@endpush

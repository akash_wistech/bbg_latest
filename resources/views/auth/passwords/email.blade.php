@extends('layouts.guest')

@section('content')
<div class="login-forgot">
  <div class="mb-10">
    <h3>{{ __('Reset Password') }}</h3>
    <div class="text-muted font-weight-bold">{{ __('Enter your email to reset your password') }}</div>
  </div>
  <form method="POST" action="{{ route('password.email') }}">
      @csrf
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="form-group mb-10">
      <input id="email" type="email" class="form-control form-control-solid h-auto py-4 px-8 @error('email') is-invalid @enderror" name="email" placeholder="{{ __('Email') }}" value="{{ old('email') }}" required autocomplete="email" autofocus>

      @error('email')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
      @enderror
    </div>
    <div class="form-group d-flex flex-wrap flex-center mt-10">
      <button class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-2">{{ __('Send Password Reset Link') }}</button>
      <a href="{{route('login')}}" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">{{ __('Cancel') }}</a>
    </div>
  </form>
</div>
@endsection

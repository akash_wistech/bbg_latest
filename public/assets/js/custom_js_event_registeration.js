
      $(document).ready(function() {
          var GuestCount=1;
          var memberCount=1;
          var eventId = $('#event-id').val();
          var frontend = 'frontend-form';
          var companyName = $('.autocomplete-company').val();
          var company_id = $("#company_id").val();


          var CurrentLoggedInUser= <?= ((\Auth::user()!=null)? \Auth::user()->id : 0)  ?>
          // This add guest button comes when we add company
          $("body").on("click",'.add_guest_1', function(){
              GuestCount++;
              _this= $(this);
              var member_id= $(this).data('id');
              var guestType= 'guest';
              var html = '';
              var frontend = 'frontend-form';
              var eventId = $('#event-id').val();
              data = {eventId,member_id, GuestCount,guestType,frontend};
              $.ajax({
                  url:"<?php echo url('event/get-guest-form')?>",
                  type:"POST",
                  data: {
                    "_token": "<?php echo  csrf_token() ?>",
                    data:data,
                },
                success:function (data) {
                   $(_this).parents('.tr_remove').find('.add_guest_here_1').append(data);

               }
           });
          });

          // Add guest button
          $("body").on("click",'.add_guest_2', function(){
            $('.member-selection-my-company').addClass('d-none');
            $('.member-selection-other-company').addClass('d-none');
              GuestCount++;
              guestType= 'guest';
              var frontend = 'frontend-form';
              var html = '';
              var eventId = $('#event-id').val();
              data = {eventId,GuestCount,guestType,frontend};
              $.ajax({
                url:"<?= url('event/get-guest-form')?>",
                type:"POST",
                data: {
                  "_token": "<?= csrf_token() ?>",
                  data:data,
              },
              success:function (data) {
                 $('#guestInformation').append(data);
                 }
             });
            });




          //It runs when we select member from user list
          $('body').on('change','.comeMembers', function(){
              memberid= $(this).val();
              getmemberDetials(memberid);

          });

          // Remove member
          $("body").on("click", '.remove_div', function(){
              $(this).parents('.tr_remove').addClass('d-none');
              $("#primary_contact option[value='"+$(this).data("id")+"']").remove();
          });
          // Remove Guest
          $("body").on("click", '.remove_guest', function(){
              $(this).parents('.remove_guest_1').addClass('d-none');
          });

          // Guest Diet First Option
          $('body').on('click', '.diet-option-box', function() {
              if($(this).prop("checked") == true){
                if ($(this).parents('.parent-card').find('.diet-select-options').val()=='other') {
                  $(this).parents('.parent-card').find('.other-diet-option-description').show();
              }
              $(this).parents('.parent-card').find('.diet-option-description-select').show();
              }
            else if($(this).prop("checked") == false){
                $(this).parents('.parent-card').find('.diet-option-description-select').hide();
                $(this).parents('.parent-card').find('.other-diet-option-description').hide();
                }
          });

          // After checked diet_option , we select diet , this function will work
          $('body').on('change', '.diet-select-options', function() {
          if($(this).val()=='other'){
              $(this).parents('.parent-card').find('.other-diet-option-description').show();
          }else{
              $(this).parents('.parent-card').find('.other-diet-option-description').hide();
          }
          });

        $('body').on('click', '.diet-option-box-member', function() {
              if($(this).prop("checked") == true){
                  $(this).parents('.tr_remove').find('.diet-option-description-select-member').show();
                  $('#user_type_relation').addClass('mt-4');
              }
              else if($(this).prop("checked") == false){
                  $(this).parents('.tr_remove').find('.diet-option-description-select-member').hide();
                  $('#user_type_relation').removeClass('mt-4');
              }
            });



         // New Script
        //EventRegisterationType
        $("body").on("click",'.add-registeration-type', function(){
            GuestCount++;
            Registerationtype= $(this).data("usertype");
            data = {Registerationtype,CurrentLoggedInUser,memberCount,frontend};
            memberCount++;
            // eventId
            if (Registerationtype=='myself') {
            if (CurrentLoggedInUser!=0) {
              getmemberDetials(CurrentLoggedInUser);
            }
          }
          if (Registerationtype=='contactofmycompany') {
            $('.member-selection-my-company').removeClass('d-none');
            $('.member-selection-other-company').addClass('d-none');
            getCompanyWithMembers(CurrentLoggedInUser);
          }
          if (Registerationtype=='contactofothercompany') {
            $('.member-selection-my-company').addClass('d-none');
            $('.member-selection-other-company').removeClass('d-none');
          }
        });


        //Get Member EventsDetails
        function getmemberDetials(memberid){
            data = {member_id:memberid,memberCount,eventId,frontend};
            memberCount++;
            $.ajax({
                url:"<?= url('event/get-event-registeration-type')?>",
                type:"POST",
                data: {
                    "_token": "<?= csrf_token() ?>",
                    data: data,
                },
                success:function (data) {
                  console.log(data);
                    $('.members_here').append(data.html);
                    if (data.status=='error') {
                      swal({
                             title: 'Error!',
                             text: data.msg,
                             type:data.status,
                             confirmButtonText: 'OK'
                           });
                    }
                    $('.dummy-member-box').addClass('d-none');
                }
            });
        }

        // Add Current user company name wit members
        function getCompanyWithMembers(memberid){
          data = {memberid};
          $.ajax({
              url:"<?= url('/event/get-company-wiht-members/')?>",
              type:"POST",
              data: {
                  "_token": "<?= csrf_token() ?>",
                  data: data,
              },
              success:function (data) {
                console.log(data['html']);
                  $('#mycompany').val(data['companyName']);
                  $('#mycompanyuser').html(data['html']);
              }
          });
        }
      });

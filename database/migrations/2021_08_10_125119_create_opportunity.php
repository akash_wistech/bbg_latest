<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity', function (Blueprint $table) {
            $table->id();
            $table->char('rec_type',1)->default('o')->nullable();
            $table->string('reference_no',100)->nullable();
            $table->tinyInteger('source')->default(1)->nullable();
            $table->string('title',100)->nullable();
            $table->integer('service_type')->default(0)->nullable();
            $table->char('module_type',25)->nullable();
            $table->integer('module_id')->default(0)->nullable();
            $table->text('module_keyword')->nullable();
            $table->date('expected_close_date')->nullable();
            $table->decimal('quote_amount', $precision = 10, $scale = 2)->nullable();
            $table->text('descp')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->index('source');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpiryReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expiry_reasons', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->text('reason')->nullable();
            $table->date('expiry_start_date')->nullable();
            $table->date('expiry_end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expiry_reasons');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->text('path')->nullable();
            $table->text('name')->nullable();
            $table->string('extension',255)->nullable();
            $table->string('size',255)->default(0)->nullable();
            $table->integer('order')->default(0)->nullable();
            $table->tinyInteger('featured')->nullable();
            $table->unsignedInteger('imageable_id')->default(0)->nullable();
            $table->string('imageable_type',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}

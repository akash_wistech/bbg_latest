<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;

Route::get('/company', [CompanyController::class,'index']);
Route::get('/company/data-table-data', [CompanyController::class,'datatableData']);
Route::match(['get', 'post'], '/company/create', [CompanyController::class,'create']);
Route::match(['get', 'post'], '/company/update/{id}', [CompanyController::class,'update']);
// Route::get('/company/view/{id}', [CompanyController::class,'show']);
Route::post('/company/delete/{id}', [CompanyController::class,'destroy']);

Route::match(['get','post'],'company/view/{id}',[CompanyController::class, 'show']);


// By Mushabab
Route::match(['get', 'post'], '/company/changestatus/{id}', [CompanyController::class,'changestatus']);

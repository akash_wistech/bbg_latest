<?php

use Illuminate\Support\Facades\Route;

use NaeemAwan\CMS\Http\Controllers\SponsorsController;
use NaeemAwan\CMS\Http\Controllers\CommitteMembersController;
use NaeemAwan\CMS\Http\Controllers\ViewFromChairController;
use Wisdom\News\Http\Controllers\NewsController;
use Wisdom\Event\Http\Controllers\EventController;

// Frontend Routes

Route::match(['get', 'post'], 'contact-us', [App\Http\Controllers\FrontendControllers\HomeController::class, 'contactus'])->name('contact-us');

Route::match(['get', 'post'], 'search-experties', [App\Http\Controllers\FrontendControllers\HomeController::class, 'SearchExperties'])->name('search-experties');



// Frontend Contact Routes
Route::match(['get', 'post'], '/membership/directory', [App\Http\Controllers\FrontendControllers\ContactController::class,'MemberDirectory']);
Route::match(['get', 'post'], '/account/add-member-contact', [App\Http\Controllers\FrontendControllers\ContactController::class,'AddMemberContact']);
Route::match(['get', 'post'], '/member/detail/{id}', [App\Http\Controllers\FrontendControllers\ContactController::class,'MemberDetails']);
Route::match(['get', 'post'], '/company/members', [App\Http\Controllers\FrontendControllers\ContactController::class,'CompanyMembers']);
Route::match(['get', 'post'], '/member/edit-email-subscriptions', [App\Http\Controllers\FrontendControllers\ContactController::class,'EditEmailSubscriptions']);
Route::match(['get', 'post'], '/member/add-additional', [App\Http\Controllers\FrontendControllers\ContactController::class,'AddAdditionalMember']);

//Frontend Company Routes
Route::match(['get', 'post'], '/company/directory', [App\Http\Controllers\FrontendControllers\CompanyController::class,'CompanyDirectory']);
Route::match(['get', 'post'], '/account/add-member-company', [App\Http\Controllers\FrontendControllers\CompanyController::class,'AddMemberCompany']);


//frontend routes
Route::match(['get', 'post'], '/account/member-contacts', [App\Http\Controllers\FrontendControllers\ContactController::class,'actionMemberContacts']);
Route::match(['get', 'post'], '/membership/change-password', [App\Http\Controllers\FrontendControllers\ContactController::class,'actionChangePassword']);



Route::match(['get', 'post'], 'events/registrations', [App\Http\Controllers\FrontendControllers\ContactController::class,'EventsRegistered']);
Route::match(['get', 'post'], 'member/invoices', [App\Http\Controllers\FrontendControllers\ContactController::class,'MemberInvoices']);
Route::match(['get', 'post'], 'member/payments', [App\Http\Controllers\FrontendControllers\ContactController::class,'MemberPayments']);
Route::match(['get', 'post'], 'member/statements', [App\Http\Controllers\FrontendControllers\ContactController::class,'MemberStatements']);
Route::match(['get', 'post'], 'company/addmember', [App\Http\Controllers\FrontendControllers\ContactController::class,'CompanyAddMember']);




Route::match(['post','get'], 'member/file-manager/uploadFrontend', [App\Http\Controllers\MediaManagerController::class, 'uploadFrontend']);




Route::match(['post','get'], 'member/file-manager/uploadFrontend', [App\Http\Controllers\MediaManagerController::class, 'uploadFrontend']);


Route::match(['post','get'], 'member/dashboard', [App\Http\Controllers\FrontendControllers\HomeController::class, 'MemberDashboard']);

Route::match(['post','get'], 'member/login', [App\Http\Controllers\FrontendControllers\HomeController::class, 'MemberLogin']);


Route::match(['post','get'], 'Mem/SubscribeAjax', [App\Http\Controllers\FrontendControllers\ContactController::class, 'MemberSubAjax']);


Route::match(['post','get'], 'account/profile', [App\Http\Controllers\FrontendControllers\ContactController::class, 'actionProfile']);

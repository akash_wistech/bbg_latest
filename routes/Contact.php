<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;

Route::get('/contact', [ContactController::class,'index']);
Route::get('/contact/data-table-data', [ContactController::class,'datatableData']);
Route::match(['get', 'post'], '/contact/create', [ContactController::class,'create']);
Route::match(['get', 'post'], '/contact/update/{id}', [ContactController::class,'update']);
Route::get('/contact/view/{id}', [ContactController::class,'show']);
Route::post('/contact/delete/{id}', [ContactController::class,'destroy']);

Route::get('/contact/import', [ContactController::class,'ImportExport']);
Route::get('/contact/import2/{id}', [ContactController::class,'ImportExport2']);

Route::get('/contact/check/{id}', [ContactController::class,'checkDuplicaton']);


// By Mushabab
Route::match(['get', 'post'], '/contact/status/{id}', [ContactController::class,'status']);
Route::match(['get', 'post'], '/contact/sendMessageTemplate/{id}', [ContactController::class,'sendMessageTemplate']);
Route::match(['get', 'post'], '/contact/sendMessage/', [ContactController::class,'sendMessage']);

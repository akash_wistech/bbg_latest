<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/image/{path}', [App\Http\Controllers\ImageController::class, 'showimage'])->where('path', '.*')->name('image.show');
// Route::get('/sign-aws', [App\Http\Controllers\HomeController::class, 'signRequest']);
// Route::get('/test-api', [App\Http\Controllers\HomeController::class, 'awsTest']);

Route::get('/', [App\Http\Controllers\FrontendControllers\HomeController::class, 'frontendindex'])->name('frontend');
Route::match(['get', 'post'], '/fend/login', [App\Http\Controllers\FrontendControllers\LoginController::class,'login']);
Route::match(['get', 'post'], '/fend/logout', [App\Http\Controllers\FrontendControllers\LoginController::class,'logout']);

Route::group(['middleware' => ['web','auth']], function () {

  include('FrontendRoutes.php');
  Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
  Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
  Route::match(['post','get'], '/system/settings', [App\Http\Controllers\SystemController::class, 'settings'])->name('system.settings');

  include('AdvanceSearch.php');
  include('Prospect.php');
  include('Company.php');
  include('Contact.php');
  include('Opportunity.php');
  include('Lead.php');

// By Mushsabab
  Route::get('/suggestion/company', [App\Http\Controllers\SuggestionController::class, 'moduleLookUpCompany']);
  Route::get('/practice', [App\Http\Controllers\HomeController::class, 'practice']);
  //By Noor
  Route::match(['get','post'],'/paymentyear/{year}', [App\Http\Controllers\HomeController::class, 'yearwisepayments']);
  Route::match(['get','post'],'/paymentyear/event/{year}', [App\Http\Controllers\HomeController::class, 'eventyearwisepayments']);



  Route::match(['post','get'],'/report', [App\Http\Controllers\ReportController::class, 'index'])->name('reports');
  Route::match(['post','get'],'/report/{module}', [App\Http\Controllers\ReportController::class, 'index'])->name('reportsModule');
  Route::get('/suggestion/module-lookup/{module}', [App\Http\Controllers\SuggestionController::class, 'moduleLookUp'])->name('modulelookup');
  Route::get('/suggestion/company-lookup', [App\Http\Controllers\SuggestionController::class, 'companyLookUp'])->name('generallookup');
  Route::post('/suggestion/check-duplication/{module}', [App\Http\Controllers\SuggestionController::class, 'moduleDuplicationCheck'])->name('moduledupchk');
});
Route::get('/aws-ses-listner', [App\Http\Controllers\AwsSesController::class, 'index'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);


Route::match(['post','get'], 'file-manager', [App\Http\Controllers\MediaManagerController::class, 'index']);
Route::match(['post','get'], 'file-manager/upload', [App\Http\Controllers\MediaManagerController::class, 'upload']);
Route::match(['post','get'], 'file-manager/folder', [App\Http\Controllers\MediaManagerController::class, 'folder']);
Route::match(['post','get'], 'file-manager/delete', [App\Http\Controllers\MediaManagerController::class, 'delete']);


Route::get('import-old-companies', [App\Http\Controllers\ImportController::class, 'importCompanies']);
Route::get('import-old-individuals', [App\Http\Controllers\ImportController::class, 'importIndividuals']);
Route::get('import-old-event-subscriptions', [App\Http\Controllers\ImportController::class, 'importEventSubscriptions']);
Route::get('import-old-member-companies', [App\Http\Controllers\ImportController::class, 'importMemberCompanies']);
Route::get('import-old-member-contacts', [App\Http\Controllers\ImportController::class, 'importMemberContacts']);
Route::get('import-old-member-offers', [App\Http\Controllers\ImportController::class, 'importMemberOffers']);
Route::get('import-old-news', [App\Http\Controllers\ImportController::class, 'importNews']);


Route::get('/clear', function() {
  \Illuminate\Support\Facades\Artisan::call('cache:clear');
  \Illuminate\Support\Facades\Artisan::call('config:clear');
  \Illuminate\Support\Facades\Artisan::call('config:cache');
  \Illuminate\Support\Facades\Artisan::call('view:clear');
  return "Cleared!";
});

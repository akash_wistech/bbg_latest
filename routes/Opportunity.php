<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OpportunityController;

Route::get('/opportunity', [OpportunityController::class,'index']);
Route::get('/opportunity/data-table-data', [OpportunityController::class,'datatableData']);
Route::match(['get', 'post'], '/opportunity/create', [OpportunityController::class,'create']);
Route::match(['get', 'post'], '/opportunity/create-for/{module}/{id}', [OpportunityController::class,'createFor']);
Route::match(['get', 'post'], '/opportunity/update/{id}', [OpportunityController::class,'update']);
Route::get('/opportunity/view/{id}', [OpportunityController::class,'show']);
Route::post('/opportunity/delete/{id}', [OpportunityController::class,'destroy']);

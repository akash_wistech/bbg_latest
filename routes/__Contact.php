<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;

Route::get('/contact', [ContactController::class,'index']);
Route::get('/contact/data-table-data', [ContactController::class,'datatableData']);
Route::match(['get', 'post'], '/contact/create', [ContactController::class,'create']);
Route::match(['get', 'post'], '/contact/update/{id}', [ContactController::class,'update']);
Route::get('/contact/view/{id}', [ContactController::class,'show']);
Route::post('/contact/delete/{id}', [ContactController::class,'destroy']);










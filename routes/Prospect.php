<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProspectController;

Route::get('/prospect', [ProspectController::class,'index']);
Route::get('/prospect/fix-old-data', [ProspectController::class,'fixOldrows']);
Route::get('/prospect/data-table-data', [ProspectController::class,'datatableData']);
Route::match(['get', 'post'], '/prospect/create', [ProspectController::class,'create']);
Route::match(['get', 'post'], '/prospect/import', [ProspectController::class,'import']);
Route::post('/prospect/upload', [ProspectController::class,'upload']);
Route::match(['get', 'post'], '/prospect/update/{id}', [ProspectController::class,'update']);
Route::get('/prospect/view/{id}', [ProspectController::class,'show']);
Route::post('/prospect/delete/{id}', [ProspectController::class,'destroy']);

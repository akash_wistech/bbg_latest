<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/image/{path}', [App\Http\Controllers\ImageController::class, 'showimage'])->where('path', '.*')->name('image.show');
// Route::get('/sign-aws', [App\Http\Controllers\HomeController::class, 'signRequest']);
// Route::get('/test-api', [App\Http\Controllers\HomeController::class, 'awsTest']);

Route::group(['middleware' => ['auth']], function () {
  Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
  Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
  Route::match(['post','get'], '/system/settings', [App\Http\Controllers\SystemController::class, 'settings'])->name('system.settings');

  include('AdvanceSearch.php');
  include('Prospect.php');
  include('Company.php');
  include('Contact.php');
  include('Opportunity.php');
  include('Lead.php');
  include('FrontendRoutes.php');
  Route::match(['post','get'],'/report', [App\Http\Controllers\ReportController::class, 'index'])->name('reports');
  Route::match(['post','get'],'/report/{module}', [App\Http\Controllers\ReportController::class, 'index'])->name('reports');
  Route::get('/suggestion/module-lookup/{module}', [App\Http\Controllers\SuggestionController::class, 'moduleLookUp'])->name('modulelookup');
  Route::get('/suggestion/company-lookup', [App\Http\Controllers\SuggestionController::class, 'companyLookUp'])->name('generallookup');
  Route::post('/suggestion/check-duplication/{module}', [App\Http\Controllers\SuggestionController::class, 'moduleDuplicationCheck'])->name('moduledupchk');
});
Route::get('/aws-ses-listner', [App\Http\Controllers\AwsSesController::class, 'index'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);


Route::match(['post','get'], 'file-manager', [App\Http\Controllers\MediaManagerController::class, 'index']);
Route::match(['post','get'], 'file-manager/upload', [App\Http\Controllers\MediaManagerController::class, 'upload']);
Route::match(['post','get'], 'file-manager/folder', [App\Http\Controllers\MediaManagerController::class, 'folder']);
Route::match(['post','get'], 'file-manager/delete', [App\Http\Controllers\MediaManagerController::class, 'delete']);




Route::get('/clear', function() {
  \Illuminate\Support\Facades\Artisan::call('cache:clear');
  \Illuminate\Support\Facades\Artisan::call('config:clear');
  \Illuminate\Support\Facades\Artisan::call('config:cache');
  \Illuminate\Support\Facades\Artisan::call('view:clear');
  return "Cleared!";
});

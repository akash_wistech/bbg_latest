<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LeadController;

Route::get('/lead', [LeadController::class,'index']);
Route::get('/lead/data-table-data', [LeadController::class,'datatableData']);
Route::match(['get', 'post'], '/lead/create', [LeadController::class,'create']);
Route::match(['get', 'post'], '/lead/import', [LeadController::class,'import']);
Route::post('/lead/upload', [LeadController::class,'upload']);
Route::match(['get', 'post'], '/lead/update/{id}', [LeadController::class,'update']);
Route::get('/lead/view/{id}', [LeadController::class,'show']);
Route::post('/lead/delete/{id}', [LeadController::class,'destroy']);

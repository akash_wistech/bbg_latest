<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdvanceSearchController;

Route::post('/advance-search/save/{module}', [AdvanceSearchController::class,'saveSearch']);
Route::get('/saved-search/data-table-data', [AdvanceSearchController::class,'savedSearchDatatable']);
Route::get('/saved-search/{module}', [AdvanceSearchController::class,'savedSearch']);
Route::post('/saved-search/delete/{id}', [AdvanceSearchController::class,'destroy']);
